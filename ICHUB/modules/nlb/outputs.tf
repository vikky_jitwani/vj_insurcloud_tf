output "load_balancer_name" {
  value = "${aws_lb.main.name}"
}

output "load_balancer_dns_name" {
  value = "${aws_lb.main.dns_name}"
}

output "load_balancer_zone_id" {
  value = "${aws_lb.main.zone_id}"
}

output "fully_qualified_domain_name" {
  value = "${element(concat(aws_route53_record.main_nlb.*.fqdn, list("")), 0)}"
}