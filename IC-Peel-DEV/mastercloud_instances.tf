# #=======================================================================
# # DEV01
# #=======================================================================
######### GUIDEWIRE ##############
resource "aws_instance" "ICPEEL_DEV01_GUIDEWIRE_SUITE" {
    ami = "${var.ami_id_amazonlinux2base}"
    vpc_security_group_ids = [
        "${aws_security_group.SG_GUIDEWIRE_DEV.id}",
        "${aws_security_group.SG_JENKINS_GUIDEWIRE_HEALTH_CHECK.id}",
        "${aws_security_group.SG_AVIATRIX_SSH.id}",
        "${aws_security_group.SG_JENKINS_CLIENT_SSH.id}",
        "${aws_security_group.SG_MSSQL_DB.id}"
    ]
    subnet_id = "${var.peel-dev-private-1a-subnet}"
    instance_type = "r5a.xlarge"
    key_name    = "PeelGuidewire"
    associate_public_ip_address = false
    iam_instance_profile = "CMS-SSMInstanceRole"
	ebs_optimized = "true"

    root_block_device {
       volume_size = "100"
    }

    tags {
        Owner = "robhatnagar@deloitte.ca"
        ManagedBy = "CMS-TF"
        SolutionName = "Insurcloud Peel"
        Environment = "Dev"
        ApplicationName = "guidewire, mssql, docker"
        DataClassification = "PII, PHI"
        DLM-Backup = "Daily-1"
        DLM-Retention = "90"
        PatchGroup = "AmazonLinux2"
        VendorManaged = "False"
        ResourceType = "Client"
        ResourceSeverity = "Normal"
        Name = "A-MC-CAC-D-A003-PEEL-DEV01-GW-SUITE-LNX-EC2"
        AccountID = "${data.aws_caller_identity.current.account_id}"
        Role = "Container"
        CLIENT = "InsurCloud PEEL"
		Account = "PEEL-dev"
		Client = "Peel"
		KeyName = "PeelGuidewire"
    }    

    provisioner "local-exec" {
        command = "echo ${aws_instance.ICPEEL_DEV01_GUIDEWIRE_SUITE.private_ip} >> dev_hosts.txt"
    }
}

output "ICPEEL_DEV01_GUIDEWIRE_SUITE_PRIVATEIP" {
    value = "${aws_instance.ICPEEL_DEV01_GUIDEWIRE_SUITE.private_ip}"
}

######### MICROSERVICES ###########
resource "aws_instance" "ICPEEL_DEV01_MICROSERVICES" {
    ami = "${var.ami_id_amazonlinux2base}"
    vpc_security_group_ids = [
        "${aws_security_group.SG_MICROSERVICES_DEV.id}",
        "${aws_security_group.SG_MONITORING.id}",
        "${aws_security_group.SG_AVIATRIX_SSH.id}",
        "${aws_security_group.SG_JENKINS_CLIENT_SSH.id}",
        "${aws_security_group.SG_MYSQL_DB.id}"
    ]
    subnet_id = "${var.peel-dev-private-1a-subnet}"
    instance_type = "t3a.2xlarge"
    key_name    = "PeelMicroservices"
    associate_public_ip_address = false
    iam_instance_profile = "CMS-SSMInstanceRole"
	ebs_optimized = "true"

    root_block_device {
       volume_size = "200"
    }

    tags {
        Owner = "robhatnagar@deloitte.ca"
        ManagedBy = "CMS-TF"
        SolutionName = "Insurcloud Peel"
        Environment = "Dev"
        ApplicationName = "docker, microservices, mssql"
        DataClassification = "PII, PHI"
        DLM-Backup = "Daily-1"
        DLM-Retention = "90"
        PatchGroup = "AmazonLinux2"
        VendorManaged = "False"
        ResourceType = "Client"
        ResourceSeverity = "Normal"
        Name = "A-MC-CAC-D-A003-PEEL-DEV01-MICROSERVICES-LNX-EC2"
        AccountID = "${data.aws_caller_identity.current.account_id}"
        Role = "Container"
        CLIENT = "InsurCloud PEEL"
		Account = "PEEL-dev"
		Client = "Peel"
		KeyName = "PeelMicroservices"
    }

    provisioner "local-exec" {
        command = "echo ${aws_instance.ICPEEL_DEV01_MICROSERVICES.private_ip} >> dev_hosts.txt"
    }
}

output "ICPEEL_DEV01_MICROSERVICES_PRIVATEIP" {
    value = "${aws_instance.ICPEEL_DEV01_MICROSERVICES.private_ip}"
}

######### HAPROXY ##############
resource "aws_instance" "ICPEEL_DEV01_INTERNAL_HAPROXY" {
    ami = "${var.ami_id_amazonlinux2base}"
    vpc_security_group_ids = [
        "${aws_security_group.SG_INTERNAL_HAPROXY_DEV.id}",
        "${aws_security_group.SG_AVIATRIX_SSH.id}",
        "${aws_security_group.SG_JENKINS_CLIENT_SSH.id}",
        "${aws_security_group.SG_CLIENT_PING_TEST.id}"
    ]
    subnet_id = "${var.peel-dev-private-1a-subnet}"
    instance_type = "t3a.small"
    key_name    = "PeelHAProxy"
    associate_public_ip_address = false
    iam_instance_profile = "CMS-SSMInstanceRole"
	ebs_optimized = "true"

    root_block_device {
       volume_size = "40"
    }

    tags {
        Owner = "robhatnagar@deloitte.ca"
        ManagedBy = "CMS-TF"
        SolutionName = "Insurcloud Peel"
        Environment = "Dev"
        ApplicationName = "docker, haproxy"
        DataClassification = "PII, PHI"
        DLM-Backup = "Daily-1"
        DLM-Retention = "90"
        PatchGroup = "AmazonLinux2"
        VendorManaged = "False"
        ResourceType = "Client"
        ResourceSeverity = "Normal"
        Name = "A-MC-CAC-D-A003-PEEL-DEV01-INTERNAL-HAPROXY-EC2"
        AccountID = "${data.aws_caller_identity.current.account_id}"
        Role = "Container"
        CLIENT = "InsurCloud PEEL"
		Account = "PEEL-dev"
		Client = "Peel"
		KeyName = "PeelHAProxy"
    }
   
    provisioner "local-exec" {
        command = "echo ${aws_instance.ICPEEL_DEV01_INTERNAL_HAPROXY.private_ip} >> dev_hosts.txt"
    }
}

output "ICPEEL_DEV01_INTERNAL_HAPROXY_PRIVATEIP" {
    value = "${aws_instance.ICPEEL_DEV01_INTERNAL_HAPROXY.private_ip}"
}

# Not being used in Dev01 yet
# resource "aws_instance" "ICPEEL_DEV01_EXTERNAL_HAPROXY" {
#     ami = "${var.ami_id_amazonlinux2base}"
#     vpc_security_group_ids = [
#         "${aws_security_group.SG_EXTERNAL_HAPROXY_DEV.id}",
#         "${aws_security_group.SG_AVIATRIX_SSH.id}",
#         "${aws_security_group.SG_JENKINS_CLIENT_SSH.id}"
#     ]
#     subnet_id = "${var.peel-dev-private-1a-subnet}"
#     instance_type = "t3a.small"
#     key_name    = "PeelHAProxy"
#     associate_public_ip_address = false
#     iam_instance_profile = "CMS-SSMInstanceRole"
# 	ebs_optimized = "true"

#     root_block_device {
#        volume_size = "40"
#     }

#     tags {
#         Owner = "robhatnagar@deloitte.ca"
#         ManagedBy = "CMS-TF"
#         SolutionName = "Insurcloud Peel"
#         Environment = "Dev"
#         ApplicationName = "docker, haproxy"
#         DataClassification = "PII, PHI"
#         DLM-Backup = "Daily-1"
#         DLM-Retention = "90"
#         PatchGroup = "AmazonLinux2"
#         ResourceType = "Client"
#         ResourceSeverity = "Normal"
#         Name = "A-MC-CAC-D-A003-PEEL-DEV01-EXTERNAL-HAPROXY-EC2"
#         AccountID = "${data.aws_caller_identity.current.account_id}"
#         Role = "Container"
#        CLIENT = "InsurCloud PEEL"
#     }
   
#     provisioner "local-exec" {
#         command = "echo ${aws_instance.ICPEEL_DEV01_EXTERNAL_HAPROXY.private_ip} >> dev_hosts.txt"
#     }
# }

# output "ICPEEL_DEV01_EXTERNAL_HAPROXY_PRIVATEIP" {
#     value = "${aws_instance.ICPEEL_DEV01_EXTERNAL_HAPROXY.private_ip}"
# }

# #=======================================================================
# # PPS
# #=======================================================================
######### GUIDEWIRE ##############
resource "aws_instance" "ICPEEL_PPS_GUIDEWIRE_APPLICATION_1" {
    ami = "${var.ami_id_amazonlinux2base}"
    vpc_security_group_ids = [
        "${aws_security_group.SG_GUIDEWIRE.id}",
        "${aws_security_group.SG_GUIDEWIRE_CLUSTER_NBIO_EVENT_QUEUE.id}",
        "${aws_security_group.SG_JENKINS_GUIDEWIRE_HEALTH_CHECK.id}",
        "${aws_security_group.SG_AVIATRIX_SSH.id}",
        "${aws_security_group.SG_JENKINS_CLIENT_SSH.id}"
    ]
    subnet_id = "${var.peel-dev-private-1a-subnet}"
    instance_type = "r5a.xlarge"
    key_name    = "PeelGuidewire"
    associate_public_ip_address = false
    iam_instance_profile = "CMS-SSMInstanceRole"
	ebs_optimized = "true"

    root_block_device {
       volume_size = "100"
    }

    tags {
        Owner = "robhatnagar@deloitte.ca"
        ManagedBy = "CMS-TF"
        SolutionName = "Insurcloud Peel"
        Environment = "Dev"
        ApplicationName = "guidewire, docker"
        DataClassification = "PII, PHI"
        DLM-Backup = "Daily-1"
        DLM-Retention = "90"
        PatchGroup = "AmazonLinux2"
        VendorManaged = "False"
        ResourceType = "Client"
        ResourceSeverity = "Normal"
        Name = "A-MC-CAC-D-A003-PEEL-PPS-GW-APP-1-LNX-EC2"
        AccountID = "${data.aws_caller_identity.current.account_id}"
        Role = "Container"
        CLIENT = "InsurCloud PEEL"
		Account = "PEEL-dev"
		Client = "Peel"
		KeyName = "PeelGuidewire"
    }    

    provisioner "local-exec" {
        command = "echo ${aws_instance.ICPEEL_PPS_GUIDEWIRE_APPLICATION_1.private_ip} >> dev_hosts.txt"
    }
}

output "ICPEEL_PPS_GUIDEWIRE_APPLICATION_1_PRIVATEIP" {
    value = "${aws_instance.ICPEEL_PPS_GUIDEWIRE_APPLICATION_1.private_ip}"
}

resource "aws_instance" "ICPEEL_PPS_GUIDEWIRE_APPLICATION_2" {
    ami = "${var.ami_id_amazonlinux2base}"
    vpc_security_group_ids = [
        "${aws_security_group.SG_GUIDEWIRE.id}",
        "${aws_security_group.SG_GUIDEWIRE_CLUSTER_NBIO_EVENT_QUEUE.id}",
        "${aws_security_group.SG_JENKINS_GUIDEWIRE_HEALTH_CHECK.id}",
        "${aws_security_group.SG_AVIATRIX_SSH.id}",
        "${aws_security_group.SG_JENKINS_CLIENT_SSH.id}"
    ]
    subnet_id = "${var.peel-dev-private-1b-subnet}"
    instance_type = "r5a.xlarge"
    key_name    = "PeelGuidewire"
    associate_public_ip_address = false
    iam_instance_profile = "CMS-SSMInstanceRole"
	ebs_optimized = "true"

    root_block_device {
       volume_size = "100"
    }

    tags {
        Owner = "robhatnagar@deloitte.ca"
        ManagedBy = "CMS-TF"
        SolutionName = "Insurcloud Peel"
        Environment = "Dev"
        ApplicationName = "guidewire, docker"
        DataClassification = "PII, PHI"
        DLM-Backup = "Daily-1"
        DLM-Retention = "90"
        PatchGroup = "AmazonLinux2"
        VendorManaged = "False"
        ResourceType = "Client"
        ResourceSeverity = "Normal"
        Name = "A-MC-CAC-D-A003-PEEL-PPS-GW-APP-2-LNX-EC2"
        AccountID = "${data.aws_caller_identity.current.account_id}"
        Role = "Container"
        CLIENT = "InsurCloud PEEL"
		Account = "PEEL-dev"
		Client = "Peel"
		KeyName = "PeelGuidewire"
    }    

    provisioner "local-exec" {
        command = "echo ${aws_instance.ICPEEL_PPS_GUIDEWIRE_APPLICATION_2.private_ip} >> dev_hosts.txt"
    }
} 

output "ICPEEL_PPS_GUIDEWIRE_APPLICATION_2_PRIVATEIP" {
    value = "${aws_instance.ICPEEL_PPS_GUIDEWIRE_APPLICATION_2.private_ip}"
}

data "template_file" "PEEL_DOMAIN_JOIN_1" {
    template = "${file("IC-Peel-DEV/windowsdata.tpl")}"

    vars {
        computer_name = "GW-MSSQL-DB-W"
        region              = "${var.region}"
        domain              = "${var.ad_domain}"
        dns1                = "${var.ad_dns1}"
        dns2                = "${var.ad_dns2}"
        dns3                = "${var.ad_dns3}"
        ouselection         = "${var.ou_name}"
        pwordparameter      = "${var.pwordparameter}"
        unameparameter      = "${var.unameparameter}"
    }
}

resource "aws_instance" "ICPEEL_PPS_GUIDEWIRE_MSSQL_DATABASE" {
    ami = "${var.ami_id_windows2012R2base}"
    vpc_security_group_ids      = [
        "${aws_security_group.SG_MSSQL_DB.id}",
        "${aws_security_group.SG_CLIENT_CONVERSION_GW_SQL_DB.id}",
        "${aws_security_group.SG_AVIATRIX_RDP.id}"
    ]
    subnet_id = "${var.peel-dev-database-1a-subnet}"
    instance_type = "r5a.large"
    key_name    = "PeelGuidewire"
    associate_public_ip_address = false
    iam_instance_profile = "CMS-SSMInstanceRole"
	ebs_optimized = "true"

    root_block_device {
       volume_size = "200"
       delete_on_termination = false
    }

    user_data = "${data.template_file.PEEL_DOMAIN_JOIN_1.rendered}"

    tags {
        Owner = "robhatnagar@deloitte.ca"
        ManagedBy = "CMS-TF"
        SolutionName = "Insurcloud Peel"
        Environment = "Dev"
        ApplicationName = "database, mssql, guidewire"
        DataClassification = "PII, PHI"
        DLM-Backup = "Daily-7"
        DLM-Retention = "90"
        PatchGroup = "Windows"
        VendorManaged = "False"
        OSName = "Windows"
        ResourceType = "Client"
        ResourceSeverity = "Normal"
        Name = "A-MC-CAC-D-A003-PEEL-PPS-GW-MSSQL-DB-WIN-EC2"
        AccountID = "${data.aws_caller_identity.current.account_id}"
        Role = ""
        CLIENT = "InsurCloud PEEL"
		Account = "PEEL-dev"
		Client = "Peel"
		KeyName = "PeelGuidewire"
    }    

    provisioner "local-exec" {
        command = "echo ${aws_instance.ICPEEL_PPS_GUIDEWIRE_MSSQL_DATABASE.private_ip} >> dev_hosts.txt"
    } 
}

output "ICPEEL_PPS_GUIDEWIRE_MSSQL_DATABASE_PRIVATEIP" {
    value = "${aws_instance.ICPEEL_PPS_GUIDEWIRE_MSSQL_DATABASE.private_ip}"
}

######### MICROSERVICES ###########
resource "aws_instance" "ICPEEL_PPS_MICROSERVICES_MANAGER_1" {
    ami = "${var.ami_id_amazonlinux2base}"
    vpc_security_group_ids      = [
        "${aws_security_group.SG_MICROSERVICES.id}",
        "${aws_security_group.SG_AVIATRIX_SSH.id}",
        "${aws_security_group.SG_JENKINS_CLIENT_SSH.id}"
    ]
    subnet_id = "${var.peel-dev-private-1a-subnet}"
    instance_type = "t3a.small"
    key_name    = "PeelMicroservices"
    associate_public_ip_address = false
    iam_instance_profile = "CMS-SSMInstanceRole"
	ebs_optimized = "true"

    root_block_device {
       volume_size = "16"
    }

    tags {
        Owner = "robhatnagar@deloitte.ca"
        ManagedBy = "CMS-TF"
        SolutionName = "Insurcloud Peel"
        Environment = "Dev"
        ApplicationName = "docker, microservices"
        DataClassification = "PII, PHI"
        DLM-Backup = "Daily-1"
        DLM-Retention = "90"
        PatchGroup = "AmazonLinux2-Microsvc1"
        VendorManaged = "False"
        ResourceType = "Client"
        ResourceSeverity = "Normal"
        Name = "A-MC-CAC-D-A003-PEEL-PPS-MICROSERVICES-MANAGER-1-LNX-EC2"
        AccountID = "${data.aws_caller_identity.current.account_id}"
        Role = "Container"
        CLIENT = "InsurCloud PEEL"
		Account = "PEEL-dev"
		Client = "Peel"
		KeyName = "PeelMicroservices"
    }

    provisioner "local-exec" {
        command = "echo ${aws_instance.ICPEEL_PPS_MICROSERVICES_MANAGER_1.private_ip} >> dev_hosts.txt"
    }
}

output "ICPEEL_PPS_MICROSERVICES_MANAGER_1_PRIVATEIP" {
    value = "${aws_instance.ICPEEL_PPS_MICROSERVICES_MANAGER_1.private_ip}"
}

resource "aws_instance" "ICPEEL_PPS_MICROSERVICES_MANAGER_2" {
    ami = "${var.ami_id_amazonlinux2base}"
    vpc_security_group_ids      = [
        "${aws_security_group.SG_MICROSERVICES.id}",
        "${aws_security_group.SG_AVIATRIX_SSH.id}",
        "${aws_security_group.SG_JENKINS_CLIENT_SSH.id}"
    ]
    subnet_id = "${var.peel-dev-private-1b-subnet}"
    instance_type = "t3a.small"
    key_name    = "PeelMicroservices"
    associate_public_ip_address = false
    iam_instance_profile = "CMS-SSMInstanceRole"
	ebs_optimized = "true"

    root_block_device {
       volume_size = "16"
    }

    tags {
        Owner = "robhatnagar@deloitte.ca"
        ManagedBy = "CMS-TF"
        SolutionName = "Insurcloud Peel"
        Environment = "Dev"
        ApplicationName = "docker, microservices"
        DataClassification = "PII, PHI"
        DLM-Backup = "Daily-1"
        DLM-Retention = "90"
        PatchGroup = "AmazonLinux2-Microsvc2"
        VendorManaged = "False"
        ResourceType = "Client"
        ResourceSeverity = "Normal"
        Name = "A-MC-CAC-D-A003-PEEL-PPS-MICROSERVICES-MANAGER-2-LNX-EC2"
        AccountID = "${data.aws_caller_identity.current.account_id}"
        Role = "Container"
        CLIENT = "InsurCloud PEEL"
		Account = "PEEL-dev"
		Client = "Peel"
		KeyName = "PeelMicroservices"
    }

    provisioner "local-exec" {
        command = "echo ${aws_instance.ICPEEL_PPS_MICROSERVICES_MANAGER_2.private_ip} >> dev_hosts.txt"
    }
}

output "ICPEEL_PPS_MICROSERVICES_MANAGER_2_PRIVATEIP" {
    value = "${aws_instance.ICPEEL_PPS_MICROSERVICES_MANAGER_2.private_ip}"
}

resource "aws_instance" "ICPEEL_PPS_MICROSERVICES_MANAGER_3" {
    ami = "${var.ami_id_amazonlinux2base}"
    vpc_security_group_ids      = [
        "${aws_security_group.SG_MICROSERVICES.id}",
        "${aws_security_group.SG_AVIATRIX_SSH.id}",
        "${aws_security_group.SG_JENKINS_CLIENT_SSH.id}"
    ]
    subnet_id = "${var.peel-dev-private-1a-subnet}"
    instance_type = "t3a.small"
    key_name    = "PeelMicroservices"
    associate_public_ip_address = false
    iam_instance_profile = "CMS-SSMInstanceRole"
	ebs_optimized = "true"

    root_block_device {
       volume_size = "16"
    }

    tags {
        Owner = "robhatnagar@deloitte.ca"
        ManagedBy = "CMS-TF"
        SolutionName = "Insurcloud Peel"
        Environment = "Dev"
        ApplicationName = "docker, microservices"
        DataClassification = "PII, PHI"
        DLM-Backup = "Daily-1"
        DLM-Retention = "90"
        PatchGroup = "AmazonLinux2-Microsvc3"
        VendorManaged = "False"
        ResourceType = "Client"
        ResourceSeverity = "Normal"
        Name = "A-MC-CAC-D-A003-PEEL-PPS-MICROSERVICES-MANAGER-3-LNX-EC2"
        AccountID = "${data.aws_caller_identity.current.account_id}"
        Role = "Container"
        CLIENT = "InsurCloud PEEL"
		Account = "PEEL-dev"
		Client = "Peel"
		KeyName = "PeelMicroservices"
    }

    provisioner "local-exec" {
        command = "echo ${aws_instance.ICPEEL_PPS_MICROSERVICES_MANAGER_3.private_ip} >> dev_hosts.txt"
    }
}

output "ICPEEL_PPS_MICROSERVICES_MANAGER_3_PRIVATEIP" {
    value = "${aws_instance.ICPEEL_PPS_MICROSERVICES_MANAGER_3.private_ip}"
}

resource "aws_instance" "ICPEEL_PPS_MICROSERVICES_APPLICATION_1" {
    ami = "${var.ami_id_amazonlinux2base}"
    vpc_security_group_ids = [
        "${aws_security_group.SG_MICROSERVICES.id}",
        "${aws_security_group.SG_MONITORING.id}",
        "${aws_security_group.SG_AVIATRIX_SSH.id}",
        "${aws_security_group.SG_JENKINS_CLIENT_SSH.id}"
    ]
    subnet_id = "${var.peel-dev-private-1a-subnet}"
    instance_type = "t3a.2xlarge"
    key_name    = "PeelMicroservices"
    associate_public_ip_address = false
    iam_instance_profile = "CMS-SSMInstanceRole"
	ebs_optimized = "true"

    root_block_device {
       volume_size = "100"
    }

    tags {
        Owner = "robhatnagar@deloitte.ca"
        ManagedBy = "CMS-TF"
        SolutionName = "Insurcloud Peel"
        Environment = "Dev"
        ApplicationName = "docker, microservices"
        DataClassification = "PII, PHI"
        DLM-Backup = "Daily-7"
        DLM-Retention = "90"
        PatchGroup = "AmazonLinux2-Microsvc4"
        VendorManaged = "False"
        ResourceType = "Client"
        ResourceSeverity = "Normal"
        Name = "A-MC-CAC-D-A003-PEEL-PPS-MICROSERVICES-APP-1-LNX-EC2"
        AccountID = "${data.aws_caller_identity.current.account_id}"
        Role = "Container"
        CLIENT = "InsurCloud PEEL"
		Account = "PEEL-dev"
		Client = "Peel"
		KeyName = "PeelMicroservices"
    }

    provisioner "local-exec" {
        command = "echo ${aws_instance.ICPEEL_PPS_MICROSERVICES_APPLICATION_1.private_ip} >> dev_hosts.txt"
    }
}

output "ICPEEL_PPS_MICROSERVICES_APPLICATION_1_PRIVATEIP" {
    value = "${aws_instance.ICPEEL_PPS_MICROSERVICES_APPLICATION_1.private_ip}"
}

resource "aws_instance" "ICPEEL_PPS_MICROSERVICES_APPLICATION_2" {
    ami = "${var.ami_id_amazonlinux2base}"
    vpc_security_group_ids = [
        "${aws_security_group.SG_MICROSERVICES.id}",
        "${aws_security_group.SG_MONITORING.id}",
        "${aws_security_group.SG_AVIATRIX_SSH.id}",
        "${aws_security_group.SG_JENKINS_CLIENT_SSH.id}"
    ]
    subnet_id = "${var.peel-dev-private-1b-subnet}"
    instance_type = "t3a.2xlarge"
    key_name    = "PeelMicroservices"
    associate_public_ip_address = false
    iam_instance_profile = "CMS-SSMInstanceRole"
	ebs_optimized = "true"

    root_block_device {
       volume_size = "100"
    }
    
    tags {
        Owner = "robhatnagar@deloitte.ca"
        ManagedBy = "CMS-TF"
        SolutionName = "Insurcloud Peel"
        Environment = "Dev"
        ApplicationName = "docker, microservices"
        DataClassification = "PII, PHI"
        DLM-Backup = "Daily-7"
        DLM-Retention = "90"
        PatchGroup = "AmazonLinux2-Microsvc5"
        VendorManaged = "False"
        ResourceType = "Client"
        ResourceSeverity = "Normal"
        Name = "A-MC-CAC-D-A003-PEEL-PPS-MICROSERVICES-APP-2-LNX-EC2"
        AccountID = "${data.aws_caller_identity.current.account_id}"
        Role = "Container"
        CLIENT = "InsurCloud PEEL"
		Account = "PEEL-dev"
		Client = "Peel"
		KeyName = "PeelMicroservices"
    }

    provisioner "local-exec" {
        command = "echo ${aws_instance.ICPEEL_PPS_MICROSERVICES_APPLICATION_2.private_ip} >> dev_hosts.txt"
    }
}

output "ICPEEL_PPS_MICROSERVICES_APPLICATION_2_PRIVATEIP" {
    value = "${aws_instance.ICPEEL_PPS_MICROSERVICES_APPLICATION_2.private_ip}"
}

######### HAPROXY ##############
resource "aws_instance" "ICPEEL_PPS_EXTERNAL_HAPROXY_1" {
    ami = "${var.ami_id_amazonlinux2base}"
    vpc_security_group_ids = [
        "${aws_security_group.SG_EXTERNAL_HAPROXY.id}",
        "${aws_security_group.SG_HAPROXY_EXTERNAL_PEERING.id}",
        "${aws_security_group.SG_AVIATRIX_SSH.id}",
        "${aws_security_group.SG_JENKINS_CLIENT_SSH.id}"
    ]
    subnet_id = "${var.peel-dev-private-1a-subnet}"
    instance_type = "t3a.small"
    key_name    = "PeelHAProxy"
    associate_public_ip_address = false
    iam_instance_profile = "CMS-SSMInstanceRole"
	ebs_optimized = "true"

    root_block_device {
       volume_size = "20"
    }

    tags {
        Owner = "robhatnagar@deloitte.ca"
        ManagedBy = "CMS-TF"
        SolutionName = "Insurcloud Peel"
        Environment = "Dev"
        ApplicationName = "docker, haproxy"
        DataClassification = "PII, PHI"
        DLM-Backup = "Daily-1"
        DLM-Retention = "90"
        PatchGroup = "AmazonLinux2"
        VendorManaged = "False"
        ResourceType = "Client"
        ResourceSeverity = "Normal"
        Name = "A-MC-CAC-D-A003-PEEL-PPS-EXTERNAL-HAPROXY-1-EC2"
        AccountID = "${data.aws_caller_identity.current.account_id}"
        Role = "Container"
        CLIENT = "InsurCloud PEEL"
		Account = "PEEL-dev"
		Client = "Peel"
		KeyName = "PeelHAProxy"
    }
   
    provisioner "local-exec" {
        command = "echo ${aws_instance.ICPEEL_PPS_EXTERNAL_HAPROXY_1.private_ip} >> dev_hosts.txt"
    }
}

output "ICPEEL_PPS_EXTERNAL_HAPROXY_1_PRIVATEIP" {
    value = "${aws_instance.ICPEEL_PPS_EXTERNAL_HAPROXY_1.private_ip}"
}

resource "aws_instance" "ICPEEL_PPS_EXTERNAL_HAPROXY_2" {
    ami = "${var.ami_id_amazonlinux2base}"
    vpc_security_group_ids = [
        "${aws_security_group.SG_EXTERNAL_HAPROXY.id}",
        "${aws_security_group.SG_HAPROXY_EXTERNAL_PEERING.id}",
        "${aws_security_group.SG_AVIATRIX_SSH.id}",
        "${aws_security_group.SG_JENKINS_CLIENT_SSH.id}"
    ]
    subnet_id = "${var.peel-dev-private-1b-subnet}"
    instance_type = "t3a.small"
    key_name    = "PeelHAProxy"
    associate_public_ip_address = false
    iam_instance_profile = "CMS-SSMInstanceRole"
	ebs_optimized = "true"

    root_block_device {
       volume_size = "20"
    }

    tags {
        Owner = "robhatnagar@deloitte.ca"
        ManagedBy = "CMS-TF"
        SolutionName = "Insurcloud Peel"
        Environment = "Dev"
        ApplicationName = "docker, haproxy"
        DataClassification = "NA"
        DLM-Backup = "Daily-1"
        DLM-Retention = "90"
        PatchGroup = "AmazonLinux2"
        VendorManaged = "False"
        ResourceType = "Client"
        ResourceSeverity = "Normal"
        Name = "A-MC-CAC-D-A003-PEEL-PPS-EXTERNAL-HAPROXY-2-EC2"
        AccountID = "${data.aws_caller_identity.current.account_id}"
        Role = "Container"
        CLIENT = "InsurCloud PEEL"
		Account = "PEEL-dev"
		Client = "Peel"
		KeyName = "PeelHAProxy"
    }

    provisioner "local-exec" {
        command = "echo ${aws_instance.ICPEEL_PPS_EXTERNAL_HAPROXY_2.private_ip} >> dev_hosts.txt"
    }
}

output "ICPEEL_PPS_EXTERNAL_HAPROXY_2_PRIVATEIP" {
    value = "${aws_instance.ICPEEL_PPS_EXTERNAL_HAPROXY_2.private_ip}"
}

resource "aws_instance" "ICPEEL_PPS_INTERNAL_HAPROXY_1" {
    ami = "${var.ami_id_amazonlinux2base}"
    vpc_security_group_ids = [
        "${aws_security_group.SG_INTERNAL_HAPROXY.id}",
        "${aws_security_group.SG_HAPROXY_INTERNAL_PEERING.id}",
        "${aws_security_group.SG_AVIATRIX_SSH.id}",
        "${aws_security_group.SG_JENKINS_CLIENT_SSH.id}",
        "${aws_security_group.SG_CLIENT_PING_TEST.id}"
    ]
    subnet_id = "${var.peel-dev-private-1a-subnet}"
    instance_type = "t3a.small"
    key_name    = "PeelHAProxy"
    associate_public_ip_address = false
    iam_instance_profile = "CMS-SSMInstanceRole"
	ebs_optimized = "true"

    root_block_device {
       volume_size = "20"
    }

    tags {
        Owner = "robhatnagar@deloitte.ca"
        ManagedBy = "CMS-TF"
        SolutionName = "Insurcloud Peel"
        Environment = "Dev"
        ApplicationName = "docker, haproxy"
        DataClassification = "PII, PHI"
        DLM-Backup = "Daily-1"
        DLM-Retention = "90"
        PatchGroup = "AmazonLinux2"
        VendorManaged = "False"
        ResourceType = "Client"
        ResourceSeverity = "Normal"
        Name = "A-MC-CAC-D-A003-PEEL-PPS-INTERNAL-HAPROXY-1-EC2"
        AccountID = "${data.aws_caller_identity.current.account_id}"
        Role = "Container"
        CLIENT = "InsurCloud PEEL"
		Account = "PEEL-dev"
		Client = "Peel"
		KeyName = "PeelHAProxy"
    }

    provisioner "local-exec" {
        command = "echo ${aws_instance.ICPEEL_PPS_INTERNAL_HAPROXY_1.private_ip} >> dev_hosts.txt"
    }
}

output "ICPEEL_PPS_INTERNAL_HAPROXY_1_PRIVATEIP" {
    value = "${aws_instance.ICPEEL_PPS_INTERNAL_HAPROXY_1.private_ip}"
}

resource "aws_instance" "ICPEEL_PPS_INTERNAL_HAPROXY_2" {
    ami = "${var.ami_id_amazonlinux2base}"
    vpc_security_group_ids = [
        "${aws_security_group.SG_INTERNAL_HAPROXY.id}",
        "${aws_security_group.SG_HAPROXY_INTERNAL_PEERING.id}",
        "${aws_security_group.SG_AVIATRIX_SSH.id}",
        "${aws_security_group.SG_JENKINS_CLIENT_SSH.id}",
        "${aws_security_group.SG_CLIENT_PING_TEST.id}"
    ]
    subnet_id = "${var.peel-dev-private-1b-subnet}"
    instance_type = "t3a.small"
    key_name    = "PeelHAProxy"
    associate_public_ip_address = false
    iam_instance_profile = "CMS-SSMInstanceRole"
	ebs_optimized = "true"

    root_block_device {
       volume_size = "20"
    }

    tags {
        Owner = "robhatnagar@deloitte.ca"
        ManagedBy = "CMS-TF"
        SolutionName = "Insurcloud Peel"
        Environment = "Dev"
        ApplicationName = "docker, haproxy"
        DataClassification = "NA"
        DLM-Backup = "Daily-1"
        DLM-Retention = "90"
        PatchGroup = "AmazonLinux2"
        VendorManaged = "False"
        ResourceType = "Client"
        ResourceSeverity = "Normal"
        Name = "A-MC-CAC-D-A003-PEEL-PPS-INTERNAL-HAPROXY-2-EC2"
        AccountID = "${data.aws_caller_identity.current.account_id}"
        Role = "Container"
        CLIENT = "InsurCloud PEEL"
		Account = "PEEL-dev"
		Client = "Peel"
		KeyName = "PeelHAProxy"
    }

    provisioner "local-exec" {
        command = "echo ${aws_instance.ICPEEL_PPS_INTERNAL_HAPROXY_2.private_ip} >> dev_hosts.txt"
    }
}

output "ICPEEL_PPS_INTERNAL_HAPROXY_2_PRIVATEIP" {
    value = "${aws_instance.ICPEEL_PPS_INTERNAL_HAPROXY_2.private_ip}"
}

######### DATA ##############

# #=======================================================================
# # PPS/DEV/QA
# #=======================================================================
######### OPENTEXT ##############
data "template_file" "PEEL_DOMAIN_JOIN_5" {
    template = "${file("IC-Peel-DEV/windowsdata.tpl")}"

    vars {
        computer_name = "EXST-COMM-WIN"
        region              = "${var.region}"
        domain              = "${var.ad_domain}"
        dns1                = "${var.ad_dns1}"
        dns2                = "${var.ad_dns2}"
        dns3                = "${var.ad_dns3}"
        ouselection         = "${var.ou_name}"
        pwordparameter      = "${var.pwordparameter}"
        unameparameter      = "${var.unameparameter}"
    }
}

resource "aws_instance" "ICPEEL_DEV_EXSTREAM_COM_SERVER" {
    ami                         = "${var.ami_id_windows2016base}"
    vpc_security_group_ids      = [
        "${aws_security_group.SG_EXSTREAM.id}",
        "${aws_security_group.SG_AVIATRIX_RDP.id}"
    ]
    subnet_id                   = "${var.peel-dev-private-1a-subnet}"
    instance_type               = "r5a.large"
    key_name                    = "PeelOpenText"
    associate_public_ip_address = false
    iam_instance_profile        = "CMS-SSMInstanceRole"
	ebs_optimized = "true"

    lifecycle {
        prevent_destroy = false
    }

    root_block_device {
        volume_size = "600"
    }

    user_data = "${data.template_file.PEEL_DOMAIN_JOIN_5.rendered}"

    tags {
        Owner = "robhatnagar@deloitte.ca"
        ManagedBy = "CMS-TF"
        SolutionName = "Insurcloud Peel"
        Environment = "Dev"
        ApplicationName = "opentext, exstream"
        DataClassification = "PII, PHI"
        DLM-Backup = "Daily-1"
        DLM-Retention = "90"
        PatchGroup = "Windows"
        VendorManaged = "False"
        OSName = "Windows"
        ResourceType = "Client"
        ResourceSeverity = "Normal"
        Name = "A-MC-CAC-D-A003-PEEL-DEV-EXSTREAM-COMMUNICATIONS-WIN-EC2"
        AccountID = "${data.aws_caller_identity.current.account_id}"
        Role = ""
        CLIENT = "InsurCloud PEEL"
		Account = "PEEL-dev"
		Client = "Peel"
		KeyName = "PeelOpenText"
    }
}

output "ICPEEL_DEV_EXSTREAM_COM_SERVER_PRIVATEIP" {
    value = "${aws_instance.ICPEEL_DEV_EXSTREAM_COM_SERVER.private_ip}"
}

data "template_file" "PEEL_DOMAIN_JOIN_8" {
    template = "${file("IC-Peel-DEV/windowsdata.tpl")}"

    vars {
        computer_name = "OT-MSSQL-DB-W"
        region              = "${var.region}"
        domain              = "${var.ad_domain}"
        dns1                = "${var.ad_dns1}"
        dns2                = "${var.ad_dns2}"
        dns3                = "${var.ad_dns3}"
        ouselection         = "${var.ou_name}"
        pwordparameter      = "${var.pwordparameter}"
        unameparameter      = "${var.unameparameter}"
    }
}

resource "aws_instance" "ICPEEL_DEV_OT_MSSQL_DATABASE" {
    ami = "${var.ami_id_windows2012R2base}"
    vpc_security_group_ids      = [
        "${aws_security_group.SG_OT_MSSQL_DB.id}",
        "${aws_security_group.SG_AVIATRIX_RDP.id}"
    ]
    subnet_id = "${var.peel-dev-database-1a-subnet}"
    instance_type = "r5a.xlarge"
    key_name    = "PeelOpenText"
    associate_public_ip_address = false
    iam_instance_profile = "CMS-SSMInstanceRole"
    user_data = "${data.template_file.PEEL_DOMAIN_JOIN_8.rendered}"
	ebs_optimized = "true"

    lifecycle {
        prevent_destroy = false
    }

    root_block_device {
       volume_size = "200"
       delete_on_termination = false
    }

    tags {
        Owner = "robhatnagar@deloitte.ca"
        ManagedBy = "CMS-TF"
        SolutionName = "Insurcloud Peel"
        Environment = "Dev"
        ApplicationName = "database, mssql, opentext, exstream, content suite"
        DataClassification = "PII, PHI"
        DLM-Backup = "Daily-7"
        DLM-Retention = "90"
        PatchGroup = "Windows"
        VendorManaged = "False"
        OSName = "Windows"
        ResourceType = "Client"
        ResourceSeverity = "Normal"
        Name = "A-MC-CAC-D-A003-PEEL-DEV-OT-MSSQL-DB-WIN-EC2"
        AccountID = "${data.aws_caller_identity.current.account_id}"
        Role = ""
        CLIENT = "InsurCloud PEEL"
		Account = "PEEL-dev"
		Client = "Peel"
		KeyName = "PeelOpenText"
    }    

    provisioner "local-exec" {
        command = "echo ${aws_instance.ICPEEL_DEV_OT_MSSQL_DATABASE.private_ip} >> dev_hosts.txt"
    } 
}

output "ICPEEL_DEV_OT_MSSQL_DATABASE_PRIVATEIP" {
    value = "${aws_instance.ICPEEL_DEV_OT_MSSQL_DATABASE.private_ip}"
}

############# JENKINS CLIENT #################
resource "aws_instance" "ICPEEL_DEV_JENKINS_CLIENT" {
    ami = "${var.ami_id_amazonlinux2base}"
    vpc_security_group_ids = ["${aws_security_group.SG_JENKINS_CLIENT.id}"]
	subnet_id = "${var.peel-dev-private-1a-subnet}"
    instance_type = "r5a.xlarge"
    key_name    = "PeelJenkins"
    associate_public_ip_address = false
    iam_instance_profile = "CMS-SSMInstanceRole"
	ebs_optimized = "true"

	lifecycle {
       prevent_destroy = false
	}

	root_block_device {
       volume_size = "250"
	}


	tags {
		Owner = "robhatnagar@deloitte.ca"
		ManagedBy = "CMS-TF"
		SolutionName = "Insurcloud Peel"
		Environment = "Dev"
		ApplicationName = "jenkins, docker"
		DataClassification = "NA"
		DLM-Backup = "Daily-1"
		DLM-Retention = "90"
		PatchGroup = "AmazonLinux2"
        VendorManaged = "False"
		ResourceType = "Client"
		ResourceSeverity = "Normal"
		Name = "A-MC-CAC-D-A003-PEEL-DEV-JENKINS-CLIENT-EC2"
		AccountID = "${data.aws_caller_identity.current.account_id}"
		Role = "Container"
        CLIENT = "InsurCloud PEEL"
		Account = "PEEL-dev"
		Client = "Peel"
		KeyName = "PeelJenkins"
	}
}

output "ICPEEL_DEV_JENKINS_CLIENT_PRIVATEIP" {
  value = "${aws_instance.ICPEEL_DEV_JENKINS_CLIENT.private_ip}"
}

######################################
