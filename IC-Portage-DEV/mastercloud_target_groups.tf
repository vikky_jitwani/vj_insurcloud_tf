# Dev 01

# dev01 guidewire PC target group

resource "aws_alb_target_group" "dev01_guidewire_pc_target_group" {
  name        = "dev01-guidewire-pc-target-group"
  port        = "8080"
  protocol    = "HTTP"
  target_type = "instance"
  vpc_id      = "${var.vpc_id}"

  tags {
    name = "dev01_guidewire_pc_target_group"
  }

  stickiness {
    type            = "lb_cookie"
    cookie_duration = 1800
    enabled         = "${var.target_group_sticky}"
  }

  health_check {
    healthy_threshold   = 3
    unhealthy_threshold = 10
    timeout             = 5
    interval            = 10
    path                = "/pc/ping"
    port                = "8080"
  }
}

resource "aws_alb_target_group_attachment" "dev01_guidewire_pc_instance_attachment" {
  target_group_arn = "${aws_alb_target_group.dev01_guidewire_pc_target_group.arn}"
  target_id        = "${aws_instance.ICPORTAGE_DEV01_GUIDEWIRE_SUITE.id}"
  port             = 8080
}

# dev01 guidewire BC target group

resource "aws_alb_target_group" "dev01_guidewire_bc_target_group" {
  name        = "dev01-guidewire-bc-target-group"
  port        = "8081"
  protocol    = "HTTP"
  target_type = "instance"
  vpc_id      = "${var.vpc_id}"

  tags {
    name = "dev01_guidewire_bc_target_group"
  }

  stickiness {
    type            = "lb_cookie"
    cookie_duration = 1800
    enabled         = "${var.target_group_sticky}"
  }

  health_check {
    healthy_threshold   = 3
    unhealthy_threshold = 10
    timeout             = 5
    interval            = 10
    path                = "/bc/ping"
    port                = "8081"
  }
}

resource "aws_alb_target_group_attachment" "dev01_guidewire_bc_instance_attachment" {
  target_group_arn = "${aws_alb_target_group.dev01_guidewire_bc_target_group.arn}"
  target_id        = "${aws_instance.ICPORTAGE_DEV01_GUIDEWIRE_SUITE.id}"
  port             = 8081
}

# dev01 guidewire CM target group

resource "aws_alb_target_group" "dev01_guidewire_cm_target_group" {
  name        = "dev01-guidewire-cm-target-group"
  port        = "8083"
  protocol    = "HTTP"
  target_type = "instance"
  vpc_id      = "${var.vpc_id}"

  tags {
    name = "dev01_guidewire_cm_target_group"
  }

  stickiness {
    type            = "lb_cookie"
    cookie_duration = 1800
    enabled         = "${var.target_group_sticky}"
  }

  health_check {
    healthy_threshold   = 3
    unhealthy_threshold = 10
    timeout             = 5
    interval            = 10
    path                = "/ab/ping"
    port                = "8083"
  }
}

resource "aws_alb_target_group_attachment" "dev01_guidewire_cm_instance_attachment" {
  target_group_arn = "${aws_alb_target_group.dev01_guidewire_cm_target_group.arn}"
  target_id        = "${aws_instance.ICPORTAGE_DEV01_GUIDEWIRE_SUITE.id}"
  port             = 8083
}

# dev01 microservices target group

resource "aws_alb_target_group" "dev01_microservices_admin_target_group" {
  name        = "dev01-ms-admin-target-group"
  port        = "9050"
  protocol    = "HTTP"
  target_type = "instance"
  vpc_id      = "${var.vpc_id}"

  tags {
    name = "dev01_microservices_target_group"
  }

  stickiness {
    type            = "lb_cookie"
    cookie_duration = 1800
    enabled         = "${var.target_group_sticky}"
  }

  health_check {
    healthy_threshold   = 3
    unhealthy_threshold = 10
    timeout             = 5
    interval            = 10
    path                = "/admin-service/applications"
    port                = "9050"
  }
}

# Instance Attachment 10.171.8.171 (Dev01 Microservices) - admin-service

resource "aws_alb_target_group_attachment" "dev01_ms_instance_attachment" {
  target_group_arn = "${aws_alb_target_group.dev01_microservices_admin_target_group.arn}"
  target_id        = "${aws_instance.ICPORTAGE_DEV01_MICROSERVICES.id}"
  port             = 9050
}

# Dev 02

# dev02 guidewire PC target group

resource "aws_alb_target_group" "dev02_guidewire_pc_target_group" {
  name        = "dev02-guidewire-pc-target-group"
  port        = "8080"
  protocol    = "HTTP"
  target_type = "instance"
  vpc_id      = "${var.vpc_id}"

  tags {
    name = "dev02_guidewire_pc_target_group"
  }

  stickiness {
    type            = "lb_cookie"
    cookie_duration = 1800
    enabled         = "${var.target_group_sticky}"
  }

  health_check {
    healthy_threshold   = 3
    unhealthy_threshold = 10
    timeout             = 5
    interval            = 10
    path                = "/pc/ping"
    port                = "8080"
  }
}

resource "aws_alb_target_group_attachment" "dev02_guidewire_pc_instance_attachment" {
  target_group_arn = "${aws_alb_target_group.dev02_guidewire_pc_target_group.arn}"
  target_id        = "${var.dev02-gw-instance-id}"
  port             = 8080
}

# dev02 guidewire BC target group

resource "aws_alb_target_group" "dev02_guidewire_bc_target_group" {
  name        = "dev02-guidewire-bc-target-group"
  port        = "8081"
  protocol    = "HTTP"
  target_type = "instance"
  vpc_id      = "${var.vpc_id}"

  tags {
    name = "dev02_guidewire_bc_target_group"
  }

  stickiness {
    type            = "lb_cookie"
    cookie_duration = 1800
    enabled         = "${var.target_group_sticky}"
  }

  health_check {
    healthy_threshold   = 3
    unhealthy_threshold = 10
    timeout             = 5
    interval            = 10
    path                = "/bc/ping"
    port                = "8081"
  }
}

resource "aws_alb_target_group_attachment" "dev02_guidewire_bc_instance_attachment" {
  target_group_arn = "${aws_alb_target_group.dev02_guidewire_bc_target_group.arn}"
  target_id        = "${var.dev02-gw-instance-id}"
  port             = 8081
}

# dev02 guidewire CM target group

resource "aws_alb_target_group" "dev02_guidewire_cm_target_group" {
  name        = "dev02-guidewire-cm-target-group"
  port        = "8083"
  protocol    = "HTTP"
  target_type = "instance"
  vpc_id      = "${var.vpc_id}"

  tags {
    name = "dev02_guidewire_cm_target_group"
  }

  stickiness {
    type            = "lb_cookie"
    cookie_duration = 1800
    enabled         = "${var.target_group_sticky}"
  }

  health_check {
    healthy_threshold   = 3
    unhealthy_threshold = 10
    timeout             = 5
    interval            = 10
    path                = "/ab/ping"
    port                = "8083"
  }
}

resource "aws_alb_target_group_attachment" "dev02_guidewire_cm_instance_attachment" {
  target_group_arn = "${aws_alb_target_group.dev02_guidewire_cm_target_group.arn}"
  target_id        = "${var.dev02-gw-instance-id}"
  port             = 8083
}

# dev02 microservices target group

resource "aws_alb_target_group" "dev02_microservices_admin_target_group" {
  name        = "dev02-ms-admin-target-group"
  port        = "9050"
  protocol    = "HTTP"
  target_type = "instance"
  vpc_id      = "${var.vpc_id}"

  tags {
    name = "dev02_microservices_target_group"
  }

  stickiness {
    type            = "lb_cookie"
    cookie_duration = 1800
    enabled         = "${var.target_group_sticky}"
  }

  health_check {
    healthy_threshold   = 3
    unhealthy_threshold = 10
    timeout             = 5
    interval            = 10
    path                = "/admin-service/applications"
    port                = "9050"
  }
}

# Instance Attachment (dev02 Microservices) - admin-service

resource "aws_alb_target_group_attachment" "dev02_ms_instance_attachment" {
  target_group_arn = "${aws_alb_target_group.dev02_microservices_admin_target_group.arn}"
  target_id        = "${aws_instance.ICPORTAGE_DEV02_MICROSERVICES.id}"
  port             = 9050
}

# Cnv 01

# cnv01 guidewire PC target group

resource "aws_alb_target_group" "cnv01_guidewire_pc_target_group" {
  name        = "cnv01-guidewire-pc-target-group"
  port        = "8080"
  protocol    = "HTTP"
  target_type = "instance"
  vpc_id      = "${var.vpc_id}"

  tags {
    name = "cnv01_guidewire_pc_target_group"
  }

  stickiness {
    type            = "lb_cookie"
    cookie_duration = 1800
    enabled         = "${var.target_group_sticky}"
  }

  health_check {
    healthy_threshold   = 3
    unhealthy_threshold = 10
    timeout             = 5
    interval            = 10
    path                = "/pc/ping"
    port                = "8080"
  }
}

resource "aws_alb_target_group_attachment" "cnv01_guidewire_pc_instance_attachment" {
  target_group_arn = "${aws_alb_target_group.cnv01_guidewire_pc_target_group.arn}"
  target_id        = "${aws_instance.ICPORTAGE_CNV01_GUIDEWIRE_SUITE.id}"
  port             = 8080
}

# cnv01 guidewire BC target group

resource "aws_alb_target_group" "cnv01_guidewire_bc_target_group" {
  name        = "cnv01-guidewire-bc-target-group"
  port        = "8081"
  protocol    = "HTTP"
  target_type = "instance"
  vpc_id      = "${var.vpc_id}"

  tags {
    name = "cnv01_guidewire_bc_target_group"
  }

  stickiness {
    type            = "lb_cookie"
    cookie_duration = 1800
    enabled         = "${var.target_group_sticky}"
  }

  health_check {
    healthy_threshold   = 3
    unhealthy_threshold = 10
    timeout             = 5
    interval            = 10
    path                = "/bc/ping"
    port                = "8081"
  }
}

resource "aws_alb_target_group_attachment" "cnv01_guidewire_bc_target_group" {
  target_group_arn = "${aws_alb_target_group.cnv01_guidewire_bc_target_group.arn}"
  target_id        = "${aws_instance.ICPORTAGE_CNV01_GUIDEWIRE_SUITE.id}"
  port             = 8081
}

# cnv01 guidewire CM target group

resource "aws_alb_target_group" "cnv01_guidewire_cm_target_group" {
  name        = "cnv01-guidewire-cm-target-group"
  port        = "8083"
  protocol    = "HTTP"
  target_type = "instance"
  vpc_id      = "${var.vpc_id}"

  tags {
    name = "cnv01_guidewire_cm_target_group"
  }

  stickiness {
    type            = "lb_cookie"
    cookie_duration = 1800
    enabled         = "${var.target_group_sticky}"
  }

  health_check {
    healthy_threshold   = 3
    unhealthy_threshold = 10
    timeout             = 5
    interval            = 10
    path                = "/ab/ping"
    port                = "8083"
  }
}

resource "aws_alb_target_group_attachment" "cnv01_guidewire_cm_instance_attachment" {
  target_group_arn = "${aws_alb_target_group.cnv01_guidewire_cm_target_group.arn}"
  target_id        = "${aws_instance.ICPORTAGE_CNV01_GUIDEWIRE_SUITE.id}"
  port             = 8083
}

# cnv01 microservices target group

resource "aws_alb_target_group" "cnv01_microservices_admin_target_group" {
  name        = "cnv01-ms-admin-target-group"
  port        = "9050"
  protocol    = "HTTP"
  target_type = "instance"
  vpc_id      = "${var.vpc_id}"

  tags {
    name = "cnv01_microservices_target_group"
  }

  stickiness {
    type            = "lb_cookie"
    cookie_duration = 1800
    enabled         = "${var.target_group_sticky}"
  }

  health_check {
    healthy_threshold   = 3
    unhealthy_threshold = 10
    timeout             = 5
    interval            = 10
    path                = "/admin-service/applications"
    port                = "9050"
  }
}

# Instance Attachment (cnv01 Microservices) - admin-service

resource "aws_alb_target_group_attachment" "cnv01_ms_instance_attachment" {
  target_group_arn = "${aws_alb_target_group.cnv01_microservices_admin_target_group.arn}"
  target_id        = "${aws_instance.ICPORTAGE_CNV01_MICROSERVICES.id}"
  port             = 9050
}

# QA 01

# qa01 guidewire PC target group

resource "aws_alb_target_group" "qa01_guidewire_pc_target_group" {
  name        = "qa01-guidewire-pc-target-group"
  port        = "8080"
  protocol    = "HTTP"
  target_type = "instance"
  vpc_id      = "${var.vpc_id}"

  tags {
    name = "qa01_guidewire_pc_target_group"
  }

  stickiness {
    type            = "lb_cookie"
    cookie_duration = 1800
    enabled         = "${var.target_group_sticky}"
  }

  health_check {
    healthy_threshold   = 3
    unhealthy_threshold = 10
    timeout             = 5
    interval            = 10
    path                = "/pc/ping"
    port                = "8080"
  }
}

resource "aws_alb_target_group_attachment" "qa01_guidewire_pc_instance_attachment" {
  target_group_arn = "${aws_alb_target_group.qa01_guidewire_pc_target_group.arn}"
  target_id        = "${aws_instance.ICPORTAGE_QA01_GUIDEWIRE_SUITE.id}"
  port             = 8080
}

# qa01 guidewire BC target group

resource "aws_alb_target_group" "qa01_guidewire_bc_target_group" {
  name        = "qa01-guidewire-bc-target-group"
  port        = "8081"
  protocol    = "HTTP"
  target_type = "instance"
  vpc_id      = "${var.vpc_id}"

  tags {
    name = "qa01_guidewire_bc_target_group"
  }

  stickiness {
    type            = "lb_cookie"
    cookie_duration = 1800
    enabled         = "${var.target_group_sticky}"
  }

  health_check {
    healthy_threshold   = 3
    unhealthy_threshold = 10
    timeout             = 5
    interval            = 10
    path                = "/bc/ping"
    port                = "8081"
  }
}

resource "aws_alb_target_group_attachment" "qa01_guidewire_bc_instance_attachment" {
  target_group_arn = "${aws_alb_target_group.qa01_guidewire_bc_target_group.arn}"
  target_id        = "${aws_instance.ICPORTAGE_QA01_GUIDEWIRE_SUITE.id}"
  port             = 8081
}

# qa01 guidewire CM target group

resource "aws_alb_target_group" "qa01_guidewire_cm_target_group" {
  name        = "qa01-guidewire-cm-target-group"
  port        = "8083"
  protocol    = "HTTP"
  target_type = "instance"
  vpc_id      = "${var.vpc_id}"

  tags {
    name = "qa01_guidewire_cm_target_group"
  }

  stickiness {
    type            = "lb_cookie"
    cookie_duration = 1800
    enabled         = "${var.target_group_sticky}"
  }

  health_check {
    healthy_threshold   = 3
    unhealthy_threshold = 10
    timeout             = 5
    interval            = 10
    path                = "/ab/ping"
    port                = "8083"
  }
}

resource "aws_alb_target_group_attachment" "qa01_guidewire_cm_instance_attachment" {
  target_group_arn = "${aws_alb_target_group.qa01_guidewire_cm_target_group.arn}"
  target_id        = "${aws_instance.ICPORTAGE_QA01_GUIDEWIRE_SUITE.id}"
  port             = 8083
}

# qa01 microservices target group

resource "aws_alb_target_group" "qa01_microservices_admin_target_group" {
  name        = "qa01-ms-admin-target-group"
  port        = "9050"
  protocol    = "HTTP"
  target_type = "instance"
  vpc_id      = "${var.vpc_id}"

  tags {
    name = "qa01_microservices_target_group"
  }

  stickiness {
    type            = "lb_cookie"
    cookie_duration = 1800
    enabled         = "${var.target_group_sticky}"
  }

  health_check {
    healthy_threshold   = 3
    unhealthy_threshold = 10
    timeout             = 5
    interval            = 10
    path                = "/admin-service/applications"
    port                = "9050"
  }
}

# Instance Attachment (qa01 Microservices) - admin-service

resource "aws_alb_target_group_attachment" "qa01_ms_instance_attachment" {
  target_group_arn = "${aws_alb_target_group.qa01_microservices_admin_target_group.arn}"
  target_id        = "${aws_instance.ICPORTAGE_QA01_MICROSERVICES.id}"
  port             = 9050
}

# QA 02

# qa02 guidewire PC target group

resource "aws_alb_target_group" "qa02_guidewire_pc_target_group" {
  name        = "qa02-guidewire-pc-target-group"
  port        = "8080"
  protocol    = "HTTP"
  target_type = "instance"
  vpc_id      = "${var.vpc_id}"

  tags {
    name = "qa02_guidewire_pc_target_group"
  }

  stickiness {
    type            = "lb_cookie"
    cookie_duration = 1800
    enabled         = "${var.target_group_sticky}"
  }

  health_check {
    healthy_threshold   = 3
    unhealthy_threshold = 10
    timeout             = 5
    interval            = 10
    path                = "/pc/ping"
    port                = "8080"
  }
}

resource "aws_alb_target_group_attachment" "qa02_guidewire_pc_instance_attachment" {
  target_group_arn = "${aws_alb_target_group.qa02_guidewire_pc_target_group.arn}"
  target_id        = "${aws_instance.ICPORTAGE_QA02_GUIDEWIRE_SUITE.id}"
  port             = 8080
}

# qa02 guidewire BC target group

resource "aws_alb_target_group" "qa02_guidewire_bc_target_group" {
  name        = "qa02-guidewire-bc-target-group"
  port        = "8081"
  protocol    = "HTTP"
  target_type = "instance"
  vpc_id      = "${var.vpc_id}"

  tags {
    name = "qa02_guidewire_bc_target_group"
  }

  stickiness {
    type            = "lb_cookie"
    cookie_duration = 1800
    enabled         = "${var.target_group_sticky}"
  }

  health_check {
    healthy_threshold   = 3
    unhealthy_threshold = 10
    timeout             = 5
    interval            = 10
    path                = "/bc/ping"
    port                = "8081"
  }
}

resource "aws_alb_target_group_attachment" "qa02_guidewire_bc_instance_attachment" {
  target_group_arn = "${aws_alb_target_group.qa02_guidewire_bc_target_group.arn}"
  target_id        = "${aws_instance.ICPORTAGE_QA02_GUIDEWIRE_SUITE.id}"
  port             = 8081
}

# qa02 guidewire CM target group

resource "aws_alb_target_group" "qa02_guidewire_cm_target_group" {
  name        = "qa02-guidewire-cm-target-group"
  port        = "8083"
  protocol    = "HTTP"
  target_type = "instance"
  vpc_id      = "${var.vpc_id}"

  tags {
    name = "qa02_guidewire_cm_target_group"
  }

  stickiness {
    type            = "lb_cookie"
    cookie_duration = 1800
    enabled         = "${var.target_group_sticky}"
  }

  health_check {
    healthy_threshold   = 3
    unhealthy_threshold = 10
    timeout             = 5
    interval            = 10
    path                = "/ab/ping"
    port                = "8083"
  }
}

resource "aws_alb_target_group_attachment" "qa02_guidewire_cm_instance_attachment" {
  target_group_arn = "${aws_alb_target_group.qa02_guidewire_cm_target_group.arn}"
  target_id        = "${aws_instance.ICPORTAGE_QA02_GUIDEWIRE_SUITE.id}"
  port             = 8083
}

# qa02 microservices target group

resource "aws_alb_target_group" "qa02_microservices_admin_target_group" {
  name        = "qa02-ms-admin-target-group"
  port        = "9050"
  protocol    = "HTTP"
  target_type = "instance"
  vpc_id      = "${var.vpc_id}"

  tags {
    name = "qa02_microservices_target_group"
  }

  stickiness {
    type            = "lb_cookie"
    cookie_duration = 1800
    enabled         = "${var.target_group_sticky}"
  }

  health_check {
    healthy_threshold   = 3
    unhealthy_threshold = 10
    timeout             = 5
    interval            = 10
    path                = "/admin-service/applications"
    port                = "9050"
  }
}

# Instance Attachment (qa02 Microservices) - admin-service

resource "aws_alb_target_group_attachment" "qa02_ms_instance_attachment" {
  target_group_arn = "${aws_alb_target_group.qa02_microservices_admin_target_group.arn}"
  target_id        = "${aws_instance.ICPORTAGE_QA02_MICROSERVICES.id}"
  port             = 9050
}

# dev01 microservices target group

resource "aws_alb_target_group" "dev01_microservices_edge_target_group" {
  name        = "dev01-ms-edge-target-group"
  port        = "9002"
  protocol    = "HTTP"
  target_type = "instance"
  vpc_id      = "${var.vpc_id}"

  tags {
    name = "dev01_microservices_edge_target_group"
  }

  stickiness {
    type            = "lb_cookie"
    cookie_duration = 1800
    enabled         = "${var.target_group_sticky}"
  }

  health_check {
    healthy_threshold   = 3
    unhealthy_threshold = 10
    timeout             = 5
    interval            = 10
    path                = "/edge-service/applications"
    port                = "9002"
  }
}

# Instance Attachment 10.171.8.171 (Dev01 Microservices) - edge-service

resource "aws_alb_target_group_attachment" "dev01_ms_edge_instance_attachment" {
  target_group_arn = "${aws_alb_target_group.dev01_microservices_edge_target_group.arn}"
  target_id        = "${aws_instance.ICPORTAGE_DEV01_MICROSERVICES.id}"
  port             = 9002
}

# dev01 microservices edge rule

resource "aws_alb_listener_rule" "dev01_microservices_edge_listener_rule" {
  depends_on   = ["aws_alb_target_group.dev01_microservices_edge_target_group"]
  listener_arn = "${aws_alb_listener.nonprod_portage_load_balancer_listener.arn}"

  action {
    type             = "forward"
    target_group_arn = "${aws_alb_target_group.dev01_microservices_edge_target_group.arn}"
  }

  condition {
    host_header {
      values = ["dev01-ms-edge.nonprod.portage.insurcloud.ca"]
    }
  }
}

# dev02 microservices target group

resource "aws_alb_target_group" "dev02_microservices_edge_target_group" {
  name        = "dev02-ms-edge-target-group"
  port        = "9002"
  protocol    = "HTTP"
  target_type = "instance"
  vpc_id      = "${var.vpc_id}"

  tags {
    name = "dev02_microservices_edge_target_group"
  }

  stickiness {
    type            = "lb_cookie"
    cookie_duration = 1800
    enabled         = "${var.target_group_sticky}"
  }

  health_check {
    healthy_threshold   = 3
    unhealthy_threshold = 10
    timeout             = 5
    interval            = 10
    path                = "/edge-service/applications"
    port                = "9002"
  }
}

# Instance Attachment (dev02 Microservices) - edge-service

resource "aws_alb_target_group_attachment" "dev02_ms_edge_instance_attachment" {
  target_group_arn = "${aws_alb_target_group.dev02_microservices_edge_target_group.arn}"
  target_id        = "${aws_instance.ICPORTAGE_DEV02_MICROSERVICES.id}"
  port             = 9002
}

# dev02 microservices edge rule

resource "aws_alb_listener_rule" "dev02_microservices_edge_listener_rule" {
  depends_on   = ["aws_alb_target_group.dev02_microservices_edge_target_group"]
  listener_arn = "${aws_alb_listener.nonprod_portage_load_balancer_listener.arn}"

  action {
    type             = "forward"
    target_group_arn = "${aws_alb_target_group.dev02_microservices_edge_target_group.arn}"
  }

  condition {
    host_header {
      values = ["dev02-ms-edge.nonprod.portage.insurcloud.ca"]
    }
  }
}

# cnv01 microservices target group

resource "aws_alb_target_group" "cnv01_microservices_edge_target_group" {
  name        = "cnv01-ms-edge-target-group"
  port        = "9002"
  protocol    = "HTTP"
  target_type = "instance"
  vpc_id      = "${var.vpc_id}"

  tags {
    name = "cnv01_microservices_edge_target_group"
  }

  stickiness {
    type            = "lb_cookie"
    cookie_duration = 1800
    enabled         = "${var.target_group_sticky}"
  }

  health_check {
    healthy_threshold   = 3
    unhealthy_threshold = 10
    timeout             = 5
    interval            = 10
    path                = "/edge-service/applications"
    port                = "9002"
  }
}

# Instance Attachment (cnv01 Microservices) - edge-service

resource "aws_alb_target_group_attachment" "cnv01_ms_edge_instance_attachment" {
  target_group_arn = "${aws_alb_target_group.cnv01_microservices_edge_target_group.arn}"
  target_id        = "${aws_instance.ICPORTAGE_CNV01_MICROSERVICES.id}"
  port             = 9002
}

# cnv01 microservices edge rule

resource "aws_alb_listener_rule" "cnv01_microservices_edge_listener_rule" {
  depends_on   = ["aws_alb_target_group.cnv01_microservices_edge_target_group"]
  listener_arn = "${aws_alb_listener.nonprod_portage_load_balancer_listener.arn}"

  action {
    type             = "forward"
    target_group_arn = "${aws_alb_target_group.cnv01_microservices_edge_target_group.arn}"
  }

  condition {
    host_header {
      values = ["cnv01-ms-edge.nonprod.portage.insurcloud.ca"]
    }
  }
}

# qa01 microservices target group

resource "aws_alb_target_group" "qa01_microservices_edge_target_group" {
  name        = "qa01-ms-edge-target-group"
  port        = "9002"
  protocol    = "HTTP"
  target_type = "instance"
  vpc_id      = "${var.vpc_id}"

  tags {
    name = "qa01_microservices_edge_target_group"
  }

  stickiness {
    type            = "lb_cookie"
    cookie_duration = 1800
    enabled         = "${var.target_group_sticky}"
  }

  health_check {
    healthy_threshold   = 3
    unhealthy_threshold = 10
    timeout             = 5
    interval            = 10
    path                = "/edge-service/applications"
    port                = "9002"
  }
}

# Instance Attachment (qa01 Microservices) - edge-service

resource "aws_alb_target_group_attachment" "qa01_ms_edge_instance_attachment" {
  target_group_arn = "${aws_alb_target_group.qa01_microservices_edge_target_group.arn}"
  target_id        = "${aws_instance.ICPORTAGE_QA01_MICROSERVICES.id}"
  port             = 9002
}

# qa01 microservices edge rule

resource "aws_alb_listener_rule" "qa01_microservices_edge_listener_rule" {
  depends_on   = ["aws_alb_target_group.qa01_microservices_edge_target_group"]
  listener_arn = "${aws_alb_listener.nonprod_portage_load_balancer_listener.arn}"

  action {
    type             = "forward"
    target_group_arn = "${aws_alb_target_group.qa01_microservices_edge_target_group.arn}"
  }

  condition {
    host_header {
      values = ["qa01-ms-edge.nonprod.portage.insurcloud.ca"]
    }
  }
}

# qa02 microservices target group

resource "aws_alb_target_group" "qa02_microservices_edge_target_group" {
  name        = "qa02-ms-edge-target-group"
  port        = "9002"
  protocol    = "HTTP"
  target_type = "instance"
  vpc_id      = "${var.vpc_id}"

  tags {
    name = "qa02_microservices_edge_target_group"
  }

  stickiness {
    type            = "lb_cookie"
    cookie_duration = 1800
    enabled         = "${var.target_group_sticky}"
  }

  health_check {
    healthy_threshold   = 3
    unhealthy_threshold = 10
    timeout             = 5
    interval            = 10
    path                = "/edge-service/applications"
    port                = "9002"
  }
}

# Instance Attachment (qa02 Microservices) - edge-service

resource "aws_alb_target_group_attachment" "qa02_ms_edge_instance_attachment" {
  target_group_arn = "${aws_alb_target_group.qa02_microservices_edge_target_group.arn}"
  target_id        = "${aws_instance.ICPORTAGE_QA02_MICROSERVICES.id}"
  port             = 9002
}

# qa02 microservices edge rule

resource "aws_alb_listener_rule" "qa02_microservices_edge_listener_rule" {
  depends_on   = ["aws_alb_target_group.qa02_microservices_edge_target_group"]
  listener_arn = "${aws_alb_listener.nonprod_portage_load_balancer_listener.arn}"

  action {
    type             = "forward"
    target_group_arn = "${aws_alb_target_group.qa02_microservices_edge_target_group.arn}"
  }

  condition {
    host_header {
      values = ["qa02-ms-edge.nonprod.portage.insurcloud.ca"]
    }
  }
}