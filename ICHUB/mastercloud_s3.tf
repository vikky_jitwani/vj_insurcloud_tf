// resource "aws_s3_bucket" "ICHUB_HUB_CONSUL" {
//   bucket = "cmig-consul-deployment"
//   acl = "private"

//   lifecycle_rule {
//     enabled = true

//     transition {
//       days = 30
//       storage_class = "STANDARD_IA"
//     }

//     transition {
//       days = 60
//       storage_class = "GLACIER"
//     }
//   }
//   tags {
//     Name = "cmig-consul-deployment"
//   }
// }

// resource "aws_s3_bucket_policy" "ICHUB_HUB_CONSUL_BUCKET_POLICY" {
//   bucket = "${aws_s3_bucket.ICHUB_HUB_CONSUL.id}"

//   policy = <<POLICY
// {
//     "Version": "2012-10-17",
//     "Id": "ICHUB_HUB_CONSUL_BUCKET_POLICY",
//     "Statement": [
//         {
//             "Sid": "DenyPublicReadACL",
//             "Effect": "Deny",
//             "Principal": {
//                 "AWS": "*"
//             },
//             "Action": [
//                 "s3:PutObject",
//                 "s3:PutObjectAcl"
//             ],
//             "Resource": "arn:aws:s3:::cmig-consul-deployment/*",
//             "Condition": {
//                 "StringEquals": {
//                     "s3:x-amz-acl": [
//                         "public-read",
//                         "public-read-write",
//                         "authenticated-read"
//                     ]
//                 }
//             }
//         },
//         {
//             "Sid": "DenyPublicReadGrant",
//             "Effect": "Deny",
//             "Principal": {
//                 "AWS": "*"
//             },
//             "Action": [
//                 "s3:PutObject",
//                 "s3:PutObjectAcl"
//             ],
//             "Resource": "arn:aws:s3:::cmig-consul-deployment/*",
//             "Condition": {
//                 "StringLike": {
//                     "s3:x-amz-grant-read": [
//                         "*http://acs.amazonaws.com/groups/global/AllUsers*",
//                         "*http://acs.amazonaws.com/groups/global/AuthenticatedUsers*"
//                     ]
//                 }
//             }
//         },
//         {
//             "Sid": "DenyPublicListACL",
//             "Effect": "Deny",
//             "Principal": {
//                 "AWS": "*"
//             },
//             "Action": "s3:PutBucketAcl",
//             "Resource": "arn:aws:s3:::cmig-consul-deployment",
//             "Condition": {
//                 "StringEquals": {
//                     "s3:x-amz-acl": [
//                         "public-read",
//                         "public-read-write",
//                         "authenticated-read"
//                     ]
//                 }
//             }
//         },
//         {
//             "Sid": "DenyPublicListGrant",
//             "Effect": "Deny",
//             "Principal": {
//                 "AWS": "*"
//             },
//             "Action": "s3:PutBucketAcl",
//             "Resource": "arn:aws:s3:::cmig-consul-deployment",
//             "Condition": {
//                 "StringLike": {
//                     "s3:x-amz-grant-read": [
//                         "*http://acs.amazonaws.com/groups/global/AllUsers*",
//                         "*http://acs.amazonaws.com/groups/global/AuthenticatedUsers*"
//                     ]
//                 }
//             }
//         },
//         {
//             "Sid": "DenyPublicReadConsole",
//             "Effect": "Deny",
//             "Principal": {
//                 "AWS": "*"
//             },
//             "Action": [
//                 "s3:PutObjectAcl",
//                 "s3:PutBucketAcl"
//             ],
//             "Resource": [
//                 "arn:aws:s3:::cmig-consul-deployment/*",
//                 "arn:aws:s3:::cmig-consul-deployment"
//             ],
//             "Condition": {
//                 "Null": {
//                     "s3:x-amz-grant-write": "true",
//                     "s3:x-amz-grant-read": "true",
//                     "s3:x-amz-acl": "true",
//                     "s3:x-amz-grant-read-acp": "true",
//                     "s3:x-amz-grant-full-control": "true",
//                     "s3:x-amz-grant-write-acp": "true"
//                 }
//             }
//         },
//         {
//             "Sid": "DevInstanceListBucketAllow",
//             "Effect": "Allow",
//             "Principal": {
//                 "AWS": "arn:aws:iam::623885847088:role/CmigServiceRole"
//             },
//             "Action": "s3:ListBucket",
//             "Resource": "arn:aws:s3:::cmig-consul-deployment"
//         },
//         {
//             "Sid": "DevInstanceObjectAllow",
//             "Effect": "Allow",
//             "Principal": {
//                 "AWS": "arn:aws:iam::623885847088:role/CmigServiceRole"
//             },
//             "Action": [
//                 "s3:GetObject",
//                 "s3:PutObject",
//                 "s3:DeleteObject"
//             ],
//             "Resource": "arn:aws:s3:::cmig-consul-deployment/*"
//         },
//         {
//             "Sid": "HubInstanceListBucketAllow",
//             "Effect": "Allow",
//             "Principal": {
//                 "AWS": "arn:aws:iam::532382648425:role/CmigServiceRole"
//             },
//             "Action": "s3:ListBucket",
//             "Resource": "arn:aws:s3:::cmig-consul-deployment"
//         },
//         {
//             "Sid": "HubInstanceObjectAllow",
//             "Effect": "Allow",
//             "Principal": {
//                 "AWS": "arn:aws:iam::532382648425:role/CmigServiceRole"
//             },
//             "Action": [
//                 "s3:GetObject",
//                 "s3:PutObject",
//                 "s3:DeleteObject"
//             ],
//             "Resource": "arn:aws:s3:::cmig-consul-deployment/*"
//         }
//     ]
// }
// POLICY
// }

// resource "aws_s3_bucket" "ICHUB_HUB_GW_PROPERTIES" {
//   bucket = "cmig-gw-properties"
//   acl = "private"

//   lifecycle_rule {
//     enabled = true

//     transition {
//       days = 30
//       storage_class = "STANDARD_IA"
//     }

//     transition {
//       days = 60
//       storage_class = "GLACIER"
//     }
//   }
//   tags {
//     Name = "cmig-gw-properties"
//   }

// }

// resource "aws_s3_bucket_policy" "ICHUB_HUB_GW_PROPERTIES_BUCKET_POLICY" {
//   bucket = "${aws_s3_bucket.ICHUB_HUB_GW_PROPERTIES.id}"

//   policy = <<POLICY
// {
//     "Version": "2012-10-17",
//     "Id": "ICHUB_HUB_GW_PROPERTIES_BUCKET_POLICY",
//     "Statement": [
//         {
//             "Sid": "DenyPublicReadACL",
//             "Effect": "Deny",
//             "Principal": {
//                 "AWS": "*"
//             },
//             "Action": [
//                 "s3:PutObject",
//                 "s3:PutObjectAcl"
//             ],
//             "Resource": "arn:aws:s3:::cmig-gw-properties/*",
//             "Condition": {
//                 "StringEquals": {
//                     "s3:x-amz-acl": [
//                         "public-read",
//                         "public-read-write",
//                         "authenticated-read"
//                     ]
//                 }
//             }
//         },
//         {
//             "Sid": "DenyPublicReadGrant",
//             "Effect": "Deny",
//             "Principal": {
//                 "AWS": "*"
//             },
//             "Action": [
//                 "s3:PutObject",
//                 "s3:PutObjectAcl"
//             ],
//             "Resource": "arn:aws:s3:::cmig-gw-properties/*",
//             "Condition": {
//                 "StringLike": {
//                     "s3:x-amz-grant-read": [
//                         "*http://acs.amazonaws.com/groups/global/AllUsers*",
//                         "*http://acs.amazonaws.com/groups/global/AuthenticatedUsers*"
//                     ]
//                 }
//             }
//         },
//         {
//             "Sid": "DenyPublicListACL",
//             "Effect": "Deny",
//             "Principal": {
//                 "AWS": "*"
//             },
//             "Action": "s3:PutBucketAcl",
//             "Resource": "arn:aws:s3:::cmig-gw-properties",
//             "Condition": {
//                 "StringEquals": {
//                     "s3:x-amz-acl": [
//                         "public-read",
//                         "public-read-write",
//                         "authenticated-read"
//                     ]
//                 }
//             }
//         },
//         {
//             "Sid": "DenyPublicListGrant",
//             "Effect": "Deny",
//             "Principal": {
//                 "AWS": "*"
//             },
//             "Action": "s3:PutBucketAcl",
//             "Resource": "arn:aws:s3:::cmig-gw-properties",
//             "Condition": {
//                 "StringLike": {
//                     "s3:x-amz-grant-read": [
//                         "*http://acs.amazonaws.com/groups/global/AllUsers*",
//                         "*http://acs.amazonaws.com/groups/global/AuthenticatedUsers*"
//                     ]
//                 }
//             }
//         },
//         {
//             "Sid": "DenyPublicReadConsole",
//             "Effect": "Deny",
//             "Principal": {
//                 "AWS": "*"
//             },
//             "Action": [
//                 "s3:PutObjectAcl",
//                 "s3:PutBucketAcl"
//             ],
//             "Resource": [
//                 "arn:aws:s3:::cmig-gw-properties/*",
//                 "arn:aws:s3:::cmig-gw-properties"
//             ],
//             "Condition": {
//                 "Null": {
//                     "s3:x-amz-grant-write": "true",
//                     "s3:x-amz-grant-read": "true",
//                     "s3:x-amz-acl": "true",
//                     "s3:x-amz-grant-read-acp": "true",
//                     "s3:x-amz-grant-full-control": "true",
//                     "s3:x-amz-grant-write-acp": "true"
//                 }
//             }
//         },
//         {
//             "Sid": "DevInstanceListBucketAllow",
//             "Effect": "Allow",
//             "Principal": {
//                 "AWS": "arn:aws:iam::623885847088:role/CmigServiceRole"
//             },
//             "Action": "s3:ListBucket",
//             "Resource": "arn:aws:s3:::cmig-gw-properties"
//         },
//         {
//             "Sid": "DevInstanceObjectAllow",
//             "Effect": "Allow",
//             "Principal": {
//                 "AWS": "arn:aws:iam::623885847088:role/CmigServiceRole"
//             },
//             "Action": [
//                 "s3:GetObject",
//                 "s3:PutObject",
//                 "s3:DeleteObject"
//             ],
//             "Resource": "arn:aws:s3:::cmig-gw-properties/dev*/*"
//         },
//         {
//             "Sid": "HubInstanceListBucketAllow",
//             "Effect": "Allow",
//             "Principal": {
//                 "AWS": "arn:aws:iam::532382648425:role/CmigServiceRole"
//             },
//             "Action": "s3:ListBucket",
//             "Resource": "arn:aws:s3:::cmig-gw-properties"
//         },
//         {
//             "Sid": "HubInstanceGetObjectAllow",
//             "Effect": "Allow",
//             "Principal": {
//                 "AWS": "arn:aws:iam::532382648425:role/CmigServiceRole"
//             },
//             "Action": [
//                 "s3:GetObject",
//                 "s3:PutObject",
//                 "s3:DeleteObject"
//             ],
//             "Resource": [
//                 "arn:aws:s3:::cmig-gw-properties/dev*/*",
//                 "arn:aws:s3:::cmig-gw-properties/prod/*"
//             ]
//         },
//         {
//             "Sid": "ProdInstanceListBucketAllow",
//             "Effect": "Allow",
//             "Principal": {
//                 "AWS": "arn:aws:iam::942574907772:role/CmigServiceRole"
//             },
//             "Action": "s3:ListBucket",
//             "Resource": "arn:aws:s3:::cmig-gw-properties"
//         },
//         {
//             "Sid": "ProdInstanceObjectAllow",
//             "Effect": "Allow",
//             "Principal": {
//                 "AWS": "arn:aws:iam::942574907772:role/CmigServiceRole"
//             },
//             "Action": [
//                 "s3:GetObject",
//                 "s3:PutObject",
//                 "s3:DeleteObject"
//             ],
//             "Resource": "arn:aws:s3:::cmig-gw-properties/prod/*"
//         }
//     ]
// }
// POLICY
// }

// resource "aws_s3_bucket" "ICHUB_HUB_MS_PROPERTIES" {
//   bucket = "cmig-ms-properties"
//   acl = "private"

//   lifecycle_rule {
//     enabled = true

//     transition {
//       days = 30
//       storage_class = "STANDARD_IA"
//     }

//     transition {
//       days = 60
//       storage_class = "GLACIER"
//     }
//   }
//   tags {
//     Name = "cmig-ms-properties"
//   }

// }

// resource "aws_s3_bucket_policy" "ICHUB_HUB_MS_PROPERTIES_BUCKET_POLICY" {
//   bucket = "${aws_s3_bucket.ICHUB_HUB_MS_PROPERTIES.id}"

//   policy = <<POLICY
// {
//     "Version": "2012-10-17",
//     "Id": "ICHUB_HUB_MS_PROPERTIES_BUCKET_POLICY",
//     "Statement": [
//         {
//             "Sid": "DenyPublicReadACL",
//             "Effect": "Deny",
//             "Principal": {
//                 "AWS": "*"
//             },
//             "Action": [
//                 "s3:PutObject",
//                 "s3:PutObjectAcl"
//             ],
//             "Resource": "arn:aws:s3:::cmig-ms-properties/*",
//             "Condition": {
//                 "StringEquals": {
//                     "s3:x-amz-acl": [
//                         "public-read",
//                         "public-read-write",
//                         "authenticated-read"
//                     ]
//                 }
//             }
//         },
//         {
//             "Sid": "DenyPublicReadGrant",
//             "Effect": "Deny",
//             "Principal": {
//                 "AWS": "*"
//             },
//             "Action": [
//                 "s3:PutObject",
//                 "s3:PutObjectAcl"
//             ],
//             "Resource": "arn:aws:s3:::cmig-ms-properties/*",
//             "Condition": {
//                 "StringLike": {
//                     "s3:x-amz-grant-read": [
//                         "*http://acs.amazonaws.com/groups/global/AllUsers*",
//                         "*http://acs.amazonaws.com/groups/global/AuthenticatedUsers*"
//                     ]
//                 }
//             }
//         },
//         {
//             "Sid": "DenyPublicListACL",
//             "Effect": "Deny",
//             "Principal": {
//                 "AWS": "*"
//             },
//             "Action": "s3:PutBucketAcl",
//             "Resource": "arn:aws:s3:::cmig-ms-properties",
//             "Condition": {
//                 "StringEquals": {
//                     "s3:x-amz-acl": [
//                         "public-read",
//                         "public-read-write",
//                         "authenticated-read"
//                     ]
//                 }
//             }
//         },
//         {
//             "Sid": "DenyPublicListGrant",
//             "Effect": "Deny",
//             "Principal": {
//                 "AWS": "*"
//             },
//             "Action": "s3:PutBucketAcl",
//             "Resource": "arn:aws:s3:::cmig-ms-properties",
//             "Condition": {
//                 "StringLike": {
//                     "s3:x-amz-grant-read": [
//                         "*http://acs.amazonaws.com/groups/global/AllUsers*",
//                         "*http://acs.amazonaws.com/groups/global/AuthenticatedUsers*"
//                     ]
//                 }
//             }
//         },
//         {
//             "Sid": "DenyPublicReadConsole",
//             "Effect": "Deny",
//             "Principal": {
//                 "AWS": "*"
//             },
//             "Action": [
//                 "s3:PutObjectAcl",
//                 "s3:PutBucketAcl"
//             ],
//             "Resource": [
//                 "arn:aws:s3:::cmig-ms-properties/*",
//                 "arn:aws:s3:::cmig-ms-properties"
//             ],
//             "Condition": {
//                 "Null": {
//                     "s3:x-amz-grant-write": "true",
//                     "s3:x-amz-grant-read": "true",
//                     "s3:x-amz-acl": "true",
//                     "s3:x-amz-grant-read-acp": "true",
//                     "s3:x-amz-grant-full-control": "true",
//                     "s3:x-amz-grant-write-acp": "true"
//                 }
//             }
//         },
//         {
//             "Sid": "DevInstanceListBucketAllow",
//             "Effect": "Allow",
//             "Principal": {
//                 "AWS": "arn:aws:iam::623885847088:role/CmigServiceRole"
//             },
//             "Action": "s3:ListBucket",
//             "Resource": "arn:aws:s3:::cmig-ms-properties"
//         },
//         {
//             "Sid": "DevInstanceObjectAllow",
//             "Effect": "Allow",
//             "Principal": {
//                 "AWS": "arn:aws:iam::623885847088:role/CmigServiceRole"
//             },
//             "Action": [
//                 "s3:GetObject",
//                 "s3:PutObject",
//                 "s3:DeleteObject"
//             ],
//             "Resource": "arn:aws:s3:::cmig-ms-properties/DEV/*"
//         },
//         {
//             "Sid": "HubInstanceListBucketAllow",
//             "Effect": "Allow",
//             "Principal": {
//                 "AWS": "arn:aws:iam::532382648425:role/CmigServiceRole"
//             },
//             "Action": "s3:ListBucket",
//             "Resource": "arn:aws:s3:::cmig-ms-properties"
//         },
//         {
//             "Sid": "HubInstanceObjectAllow",
//             "Effect": "Allow",
//             "Principal": {
//                 "AWS": "arn:aws:iam::532382648425:role/CmigServiceRole"
//             },
//             "Action": [
//                 "s3:GetObject",
//                 "s3:PutObject",
//                 "s3:DeleteObject"
//             ],
//             "Resource": [
//                 "arn:aws:s3:::cmig-ms-properties/DEV/*",
//                 "arn:aws:s3:::cmig-ms-properties/PROD/*"
//             ]
//         },
//         {
//             "Sid": "ProdInstanceListBucketAllow",
//             "Effect": "Allow",
//             "Principal": {
//                 "AWS": "arn:aws:iam::942574907772:role/CmigServiceRole"
//             },
//             "Action": "s3:ListBucket",
//             "Resource": "arn:aws:s3:::cmig-ms-properties"
//         },
//         {
//             "Sid": "ProdInstanceObjectAllow",
//             "Effect": "Allow",
//             "Principal": {
//                 "AWS": "arn:aws:iam::942574907772:role/CmigServiceRole"
//             },
//             "Action": [
//                 "s3:GetObject",
//                 "s3:PutObject",
//                 "s3:DeleteObject"
//             ],
//             "Resource": "arn:aws:s3:::cmig-ms-properties/PROD/*"
//         }
//     ]
// }
// POLICY
// }

// resource "aws_s3_bucket" "ICHUB_HUB_GATEWAY_PROPERTIES" {
//   bucket = "cmig-ms-lib-properties"
//   acl = "private"

//   lifecycle_rule {
//     enabled = true

//     transition {
//       days = 30
//       storage_class = "STANDARD_IA"
//     }

//     transition {
//       days = 60
//       storage_class = "GLACIER"
//     }
//   }
//   tags {
//     Name = "cmig-ms-lib-properties"
//   }

// }

// resource "aws_s3_bucket_policy" "ICHUB_HUB_GATEWAY_PROPERTIES_BUCKET_POLICY" {
//   bucket = "${aws_s3_bucket.ICHUB_HUB_GATEWAY_PROPERTIES.id}"

//   policy = <<POLICY
// {
//     "Version": "2012-10-17",
//     "Id": "ICHUB_HUB_GATEWAY_PROPERTIES_BUCKET_POLICY",
//     "Statement": [
//         {
//             "Sid": "DenyPublicReadACL",
//             "Effect": "Deny",
//             "Principal": {
//                 "AWS": "*"
//             },
//             "Action": [
//                 "s3:PutObject",
//                 "s3:PutObjectAcl"
//             ],
//             "Resource": "arn:aws:s3:::cmig-ms-lib-properties/*",
//             "Condition": {
//                 "StringEquals": {
//                     "s3:x-amz-acl": [
//                         "public-read",
//                         "public-read-write",
//                         "authenticated-read"
//                     ]
//                 }
//             }
//         },
//         {
//             "Sid": "DenyPublicReadGrant",
//             "Effect": "Deny",
//             "Principal": {
//                 "AWS": "*"
//             },
//             "Action": [
//                 "s3:PutObject",
//                 "s3:PutObjectAcl"
//             ],
//             "Resource": "arn:aws:s3:::cmig-ms-lib-properties/*",
//             "Condition": {
//                 "StringLike": {
//                     "s3:x-amz-grant-read": [
//                         "*http://acs.amazonaws.com/groups/global/AllUsers*",
//                         "*http://acs.amazonaws.com/groups/global/AuthenticatedUsers*"
//                     ]
//                 }
//             }
//         },
//         {
//             "Sid": "DenyPublicListACL",
//             "Effect": "Deny",
//             "Principal": {
//                 "AWS": "*"
//             },
//             "Action": "s3:PutBucketAcl",
//             "Resource": "arn:aws:s3:::cmig-ms-lib-properties",
//             "Condition": {
//                 "StringEquals": {
//                     "s3:x-amz-acl": [
//                         "public-read",
//                         "public-read-write",
//                         "authenticated-read"
//                     ]
//                 }
//             }
//         },
//         {
//             "Sid": "DenyPublicListGrant",
//             "Effect": "Deny",
//             "Principal": {
//                 "AWS": "*"
//             },
//             "Action": "s3:PutBucketAcl",
//             "Resource": "arn:aws:s3:::cmig-ms-lib-properties",
//             "Condition": {
//                 "StringLike": {
//                     "s3:x-amz-grant-read": [
//                         "*http://acs.amazonaws.com/groups/global/AllUsers*",
//                         "*http://acs.amazonaws.com/groups/global/AuthenticatedUsers*"
//                     ]
//                 }
//             }
//         },
//         {
//             "Sid": "DenyPublicReadConsole",
//             "Effect": "Deny",
//             "Principal": {
//                 "AWS": "*"
//             },
//             "Action": [
//                 "s3:PutObjectAcl",
//                 "s3:PutBucketAcl"
//             ],
//             "Resource": [
//                 "arn:aws:s3:::cmig-ms-lib-properties/*",
//                 "arn:aws:s3:::cmig-ms-lib-properties"
//             ],
//             "Condition": {
//                 "Null": {
//                     "s3:x-amz-grant-write": "true",
//                     "s3:x-amz-grant-read": "true",
//                     "s3:x-amz-acl": "true",
//                     "s3:x-amz-grant-read-acp": "true",
//                     "s3:x-amz-grant-full-control": "true",
//                     "s3:x-amz-grant-write-acp": "true"
//                 }
//             }
//         },
//         {
//             "Sid": "DevInstanceListBucketAllow",
//             "Effect": "Allow",
//             "Principal": {
//                 "AWS": "arn:aws:iam::623885847088:role/CmigServiceRole"
//             },
//             "Action": "s3:ListBucket",
//             "Resource": "arn:aws:s3:::cmig-ms-lib-properties"
//         },
//         {
//             "Sid": "DevInstanceObjectAllow",
//             "Effect": "Allow",
//             "Principal": {
//                 "AWS": "arn:aws:iam::623885847088:role/CmigServiceRole"
//             },
//             "Action": [
//                 "s3:GetObject",
//                 "s3:PutObject",
//                 "s3:DeleteObject"
//             ],
//             "Resource": "arn:aws:s3:::cmig-ms-lib-properties/DEV/*"
//         },
//         {
//             "Sid": "HubInstanceListBucketAllow",
//             "Effect": "Allow",
//             "Principal": {
//                 "AWS": "arn:aws:iam::532382648425:role/CmigServiceRole"
//             },
//             "Action": "s3:ListBucket",
//             "Resource": "arn:aws:s3:::cmig-ms-lib-properties"
//         },
//         {
//             "Sid": "HubInstanceObjectAllow",
//             "Effect": "Allow",
//             "Principal": {
//                 "AWS": "arn:aws:iam::532382648425:role/CmigServiceRole"
//             },
//             "Action": [
//                 "s3:GetObject",
//                 "s3:PutObject",
//                 "s3:DeleteObject"
//             ],
//             "Resource": [
//                 "arn:aws:s3:::cmig-ms-lib-properties/DEV/*",
//                 "arn:aws:s3:::cmig-ms-lib-properties/PROD/*"
//             ]
//         },
//         {
//             "Sid": "ProdInstanceListBucketAllow",
//             "Effect": "Allow",
//             "Principal": {
//                 "AWS": "arn:aws:iam::942574907772:role/CmigServiceRole"
//             },
//             "Action": "s3:ListBucket",
//             "Resource": "arn:aws:s3:::cmig-ms-lib-properties"
//         },
//         {
//             "Sid": "ProdInstanceObjectAllow",
//             "Effect": "Allow",
//             "Principal": {
//                 "AWS": "arn:aws:iam::942574907772:role/CmigServiceRole"
//             },
//             "Action": [
//                 "s3:GetObject",
//                 "s3:PutObject",
//                 "s3:DeleteObject"
//             ],
//             "Resource": "arn:aws:s3:::cmig-ms-lib-properties/PROD/*"
//         }
//     ]
// }
// POLICY
// }

// resource "aws_s3_bucket" "ICHUB_HUB_HAPROXY" {
//   bucket = "cmig-haproxy-deployment"
//   acl = "private"

//   lifecycle_rule {
//     enabled = true

//     transition {
//       days = 30
//       storage_class = "STANDARD_IA"
//     }

//     transition {
//       days = 60
//       storage_class = "GLACIER"
//     }
//   }
//   tags {
//     Name = "cmig-haproxy-deployment"
//   }

// }

// resource "aws_s3_bucket_policy" "ICHUB_HUB_HAPROXY_BUCKET_POLICY" {
//   bucket = "${aws_s3_bucket.ICHUB_HUB_HAPROXY.id}"

//   policy = <<POLICY
// {
//     "Version": "2012-10-17",
//     "Id": "ICHUB_HUB_HAPROXY_BUCKET_POLICY",
//     "Statement": [
//         {
//             "Sid": "DenyPublicReadACL",
//             "Effect": "Deny",
//             "Principal": {
//                 "AWS": "*"
//             },
//             "Action": [
//                 "s3:PutObject",
//                 "s3:PutObjectAcl"
//             ],
//             "Resource": "arn:aws:s3:::cmig-haproxy-deployment/*",
//             "Condition": {
//                 "StringEquals": {
//                     "s3:x-amz-acl": [
//                         "public-read",
//                         "public-read-write",
//                         "authenticated-read"
//                     ]
//                 }
//             }
//         },
//         {
//             "Sid": "DenyPublicReadGrant",
//             "Effect": "Deny",
//             "Principal": {
//                 "AWS": "*"
//             },
//             "Action": [
//                 "s3:PutObject",
//                 "s3:PutObjectAcl"
//             ],
//             "Resource": "arn:aws:s3:::cmig-haproxy-deployment/*",
//             "Condition": {
//                 "StringLike": {
//                     "s3:x-amz-grant-read": [
//                         "*http://acs.amazonaws.com/groups/global/AllUsers*",
//                         "*http://acs.amazonaws.com/groups/global/AuthenticatedUsers*"
//                     ]
//                 }
//             }
//         },
//         {
//             "Sid": "DenyPublicListACL",
//             "Effect": "Deny",
//             "Principal": {
//                 "AWS": "*"
//             },
//             "Action": "s3:PutBucketAcl",
//             "Resource": "arn:aws:s3:::cmig-haproxy-deployment",
//             "Condition": {
//                 "StringEquals": {
//                     "s3:x-amz-acl": [
//                         "public-read",
//                         "public-read-write",
//                         "authenticated-read"
//                     ]
//                 }
//             }
//         },
//         {
//             "Sid": "DenyPublicListGrant",
//             "Effect": "Deny",
//             "Principal": {
//                 "AWS": "*"
//             },
//             "Action": "s3:PutBucketAcl",
//             "Resource": "arn:aws:s3:::cmig-haproxy-deployment",
//             "Condition": {
//                 "StringLike": {
//                     "s3:x-amz-grant-read": [
//                         "*http://acs.amazonaws.com/groups/global/AllUsers*",
//                         "*http://acs.amazonaws.com/groups/global/AuthenticatedUsers*"
//                     ]
//                 }
//             }
//         },
//         {
//             "Sid": "DenyPublicReadConsole",
//             "Effect": "Deny",
//             "Principal": {
//                 "AWS": "*"
//             },
//             "Action": [
//                 "s3:PutObjectAcl",
//                 "s3:PutBucketAcl"
//             ],
//             "Resource": [
//                 "arn:aws:s3:::cmig-haproxy-deployment/*",
//                 "arn:aws:s3:::cmig-haproxy-deployment"
//             ],
//             "Condition": {
//                 "Null": {
//                     "s3:x-amz-grant-write": "true",
//                     "s3:x-amz-grant-read": "true",
//                     "s3:x-amz-acl": "true",
//                     "s3:x-amz-grant-read-acp": "true",
//                     "s3:x-amz-grant-full-control": "true",
//                     "s3:x-amz-grant-write-acp": "true"
//                 }
//             }
//         },
//         {
//             "Sid": "DevInstanceListBucketAllow",
//             "Effect": "Allow",
//             "Principal": {
//                 "AWS": "arn:aws:iam::623885847088:role/CmigServiceRole"
//             },
//             "Action": "s3:ListBucket",
//             "Resource": "arn:aws:s3:::cmig-haproxy-deployment"
//         },
//         {
//             "Sid": "DevInstanceObjectAllow",
//             "Effect": "Allow",
//             "Principal": {
//                 "AWS": "arn:aws:iam::623885847088:role/CmigServiceRole"
//             },
//             "Action": [
//                 "s3:GetObject",
//                 "s3:PutObject",
//                 "s3:DeleteObject"
//             ],
//             "Resource": "arn:aws:s3:::cmig-haproxy-deployment/dev/*"
//         },
//         {
//             "Sid": "HubInstanceListBucketAllow",
//             "Effect": "Allow",
//             "Principal": {
//                 "AWS": "arn:aws:iam::532382648425:role/CmigServiceRole"
//             },
//             "Action": "s3:ListBucket",
//             "Resource": "arn:aws:s3:::cmig-haproxy-deployment"
//         },
//         {
//             "Sid": "HubInstanceObjectAllow",
//             "Effect": "Allow",
//             "Principal": {
//                 "AWS": "arn:aws:iam::532382648425:role/CmigServiceRole"
//             },
//             "Action": [
//                 "s3:GetObject",
//                 "s3:PutObject",
//                 "s3:DeleteObject"
//             ],
//             "Resource": [
//                 "arn:aws:s3:::cmig-haproxy-deployment/dev/*",
//                 "arn:aws:s3:::cmig-haproxy-deployment/prod/*"
//             ]
//         },
//         {
//             "Sid": "ProdInstanceListBucketAllow",
//             "Effect": "Allow",
//             "Principal": {
//                 "AWS": "arn:aws:iam::942574907772:role/CmigServiceRole"
//             },
//             "Action": "s3:ListBucket",
//             "Resource": "arn:aws:s3:::cmig-haproxy-deployment"
//         },
//         {
//             "Sid": "ProdInstanceObjectAllow",
//             "Effect": "Allow",
//             "Principal": {
//                 "AWS": "arn:aws:iam::942574907772:role/CmigServiceRole"
//             },
//             "Action": [
//                 "s3:GetObject",
//                 "s3:PutObject",
//                 "s3:DeleteObject"
//             ],
//             "Resource": "arn:aws:s3:::cmig-haproxy-deployment/prod/*"
//         }
//     ]
// }
// POLICY
// }

// resource "aws_s3_bucket" "ICHUB_PROD_MS_BMS_INITIAL_LOAD" {
//   bucket = "cmig-microservices-bms-initial-load"
//   acl = "private"

//   lifecycle_rule {
//     enabled = true

//     transition {
//       days = 30
//       storage_class = "STANDARD_IA"
//     }

//     transition {
//       days = 60
//       storage_class = "GLACIER"
//     }
//   }
//   tags {
//     Name = "cmig-microservices-bms-initial-load"
//   }

// }

// resource "aws_s3_bucket_policy" "ICHUB_PROD_MS_BMS_INITIAL_LOAD_BUCKET_POLICY" {
//   bucket = "${aws_s3_bucket.ICHUB_PROD_MS_BMS_INITIAL_LOAD.id}"

//   policy = <<POLICY
// {
//     "Version": "2012-10-17",
//     "Id": "ICHUB_PROD_MS_BMS_INITIAL_LOAD",
//     "Statement": [
//         {
//             "Sid": "DenyPublicReadACL",
//             "Effect": "Deny",
//             "Principal": {
//                 "AWS": "*"
//             },
//             "Action": [
//                 "s3:PutObject",
//                 "s3:PutObjectAcl"
//             ],
//             "Resource": "arn:aws:s3:::${aws_s3_bucket.ICHUB_PROD_MS_BMS_INITIAL_LOAD.id}/*",
//             "Condition": {
//                 "StringEquals": {
//                     "s3:x-amz-acl": [
//                         "public-read",
//                         "public-read-write",
//                         "authenticated-read"
//                     ]
//                 }
//             }
//         },
//         {
//             "Sid": "DenyPublicReadGrant",
//             "Effect": "Deny",
//             "Principal": {
//                 "AWS": "*"
//             },
//             "Action": [
//                 "s3:PutObject",
//                 "s3:PutObjectAcl"
//             ],
//             "Resource": "arn:aws:s3:::${aws_s3_bucket.ICHUB_PROD_MS_BMS_INITIAL_LOAD.id}/*",
//             "Condition": {
//                 "StringLike": {
//                     "s3:x-amz-grant-read": [
//                         "*http://acs.amazonaws.com/groups/global/AllUsers*",
//                         "*http://acs.amazonaws.com/groups/global/AuthenticatedUsers*"
//                     ]
//                 }
//             }
//         },
//         {
//             "Sid": "DenyPublicListACL",
//             "Effect": "Deny",
//             "Principal": {
//                 "AWS": "*"
//             },
//             "Action": "s3:PutBucketAcl",
//             "Resource": "arn:aws:s3:::${aws_s3_bucket.ICHUB_PROD_MS_BMS_INITIAL_LOAD.id}",
//             "Condition": {
//                 "StringEquals": {
//                     "s3:x-amz-acl": [
//                         "public-read",
//                         "public-read-write",
//                         "authenticated-read"
//                     ]
//                 }
//             }
//         },
//         {
//             "Sid": "DenyPublicListGrant",
//             "Effect": "Deny",
//             "Principal": {
//                 "AWS": "*"
//             },
//             "Action": "s3:PutBucketAcl",
//             "Resource": "arn:aws:s3:::${aws_s3_bucket.ICHUB_PROD_MS_BMS_INITIAL_LOAD.id}",
//             "Condition": {
//                 "StringLike": {
//                     "s3:x-amz-grant-read": [
//                         "*http://acs.amazonaws.com/groups/global/AllUsers*",
//                         "*http://acs.amazonaws.com/groups/global/AuthenticatedUsers*"
//                     ]
//                 }
//             }
//         },
//         {
//             "Sid": "DenyPublicReadConsole",
//             "Effect": "Deny",
//             "Principal": {
//                 "AWS": "*"
//             },
//             "Action": [
//                 "s3:PutObjectAcl",
//                 "s3:PutBucketAcl"
//             ],
//             "Resource": [
//                 "arn:aws:s3:::${aws_s3_bucket.ICHUB_PROD_MS_BMS_INITIAL_LOAD.id}/*",
//                 "arn:aws:s3:::${aws_s3_bucket.ICHUB_PROD_MS_BMS_INITIAL_LOAD.id}"
//             ],
//             "Condition": {
//                 "Null": {
//                     "s3:x-amz-grant-write": "true",
//                     "s3:x-amz-grant-read": "true",
//                     "s3:x-amz-acl": "true",
//                     "s3:x-amz-grant-read-acp": "true",
//                     "s3:x-amz-grant-full-control": "true",
//                     "s3:x-amz-grant-write-acp": "true"
//                 }
//             }
//         },
//         {
//             "Sid": "InstanceListBucketAllow",
//             "Effect": "Allow",
//             "Principal": {
//                 "AWS": "arn:aws:iam::942574907772:user/cmig-applicationuser"
//             },
//             "Action": "s3:ListBucket",
//             "Resource": "arn:aws:s3:::${aws_s3_bucket.ICHUB_PROD_MS_BMS_INITIAL_LOAD.id}"
//         },
//         {
//             "Sid": "InstanceObjectAllow",
//             "Effect": "Allow",
//             "Principal": {
//                 "AWS": "arn:aws:iam::942574907772:user/cmig-applicationuser"
//             },
//             "Action": [
//                 "s3:GetObject",
//                 "s3:PutObject",
//                 "s3:DeleteObject"
//             ],
//             "Resource": "arn:aws:s3:::${aws_s3_bucket.ICHUB_PROD_MS_BMS_INITIAL_LOAD.id}/*"
//         }
//     ]
// }
// POLICY
// }

// resource "aws_s3_bucket" "ICHUB_PROD_MS_DOCUMENTS" {
//   bucket = "cmig-microservices-documents"
//   acl = "private"

//   lifecycle_rule {
//     enabled = true

//     transition {
//       days = 30
//       storage_class = "STANDARD_IA"
//     }

//     transition {
//       days = 60
//       storage_class = "GLACIER"
//     }
//   }
//   tags {
//     Name = "cmig-microservices-documents"
//   }

// }

// resource "aws_s3_bucket_policy" "ICHUB_PROD_MS_DOCUMENTS_BUCKET_POLICY" {
//   bucket = "${aws_s3_bucket.ICHUB_PROD_MS_DOCUMENTS.id}"

//   policy = <<POLICY
// {
//     "Version": "2012-10-17",
//     "Id": "ICHUB_PROD_MS_DOCUMENTS",
//     "Statement": [
//         {
//             "Sid": "DenyPublicReadACL",
//             "Effect": "Deny",
//             "Principal": {
//                 "AWS": "*"
//             },
//             "Action": [
//                 "s3:PutObject",
//                 "s3:PutObjectAcl"
//             ],
//             "Resource": "arn:aws:s3:::${aws_s3_bucket.ICHUB_PROD_MS_DOCUMENTS.id}/*",
//             "Condition": {
//                 "StringEquals": {
//                     "s3:x-amz-acl": [
//                         "public-read",
//                         "public-read-write",
//                         "authenticated-read"
//                     ]
//                 }
//             }
//         },
//         {
//             "Sid": "DenyPublicReadGrant",
//             "Effect": "Deny",
//             "Principal": {
//                 "AWS": "*"
//             },
//             "Action": [
//                 "s3:PutObject",
//                 "s3:PutObjectAcl"
//             ],
//             "Resource": "arn:aws:s3:::${aws_s3_bucket.ICHUB_PROD_MS_DOCUMENTS.id}/*",
//             "Condition": {
//                 "StringLike": {
//                     "s3:x-amz-grant-read": [
//                         "*http://acs.amazonaws.com/groups/global/AllUsers*",
//                         "*http://acs.amazonaws.com/groups/global/AuthenticatedUsers*"
//                     ]
//                 }
//             }
//         },
//         {
//             "Sid": "DenyPublicListACL",
//             "Effect": "Deny",
//             "Principal": {
//                 "AWS": "*"
//             },
//             "Action": "s3:PutBucketAcl",
//             "Resource": "arn:aws:s3:::${aws_s3_bucket.ICHUB_PROD_MS_DOCUMENTS.id}",
//             "Condition": {
//                 "StringEquals": {
//                     "s3:x-amz-acl": [
//                         "public-read",
//                         "public-read-write",
//                         "authenticated-read"
//                     ]
//                 }
//             }
//         },
//         {
//             "Sid": "DenyPublicListGrant",
//             "Effect": "Deny",
//             "Principal": {
//                 "AWS": "*"
//             },
//             "Action": "s3:PutBucketAcl",
//             "Resource": "arn:aws:s3:::${aws_s3_bucket.ICHUB_PROD_MS_DOCUMENTS.id}",
//             "Condition": {
//                 "StringLike": {
//                     "s3:x-amz-grant-read": [
//                         "*http://acs.amazonaws.com/groups/global/AllUsers*",
//                         "*http://acs.amazonaws.com/groups/global/AuthenticatedUsers*"
//                     ]
//                 }
//             }
//         },
//         {
//             "Sid": "DenyPublicReadConsole",
//             "Effect": "Deny",
//             "Principal": {
//                 "AWS": "*"
//             },
//             "Action": [
//                 "s3:PutObjectAcl",
//                 "s3:PutBucketAcl"
//             ],
//             "Resource": [
//                 "arn:aws:s3:::${aws_s3_bucket.ICHUB_PROD_MS_DOCUMENTS.id}/*",
//                 "arn:aws:s3:::${aws_s3_bucket.ICHUB_PROD_MS_DOCUMENTS.id}"
//             ],
//             "Condition": {
//                 "Null": {
//                     "s3:x-amz-grant-write": "true",
//                     "s3:x-amz-grant-read": "true",
//                     "s3:x-amz-acl": "true",
//                     "s3:x-amz-grant-read-acp": "true",
//                     "s3:x-amz-grant-full-control": "true",
//                     "s3:x-amz-grant-write-acp": "true"
//                 }
//             }
//         },
//         {
//             "Sid": "InstanceListBucketAllow",
//             "Effect": "Allow",
//             "Principal": {
//                 "AWS": "arn:aws:iam::942574907772:user/cmig-applicationuser"
//             },
//             "Action": "s3:ListBucket",
//             "Resource": "arn:aws:s3:::${aws_s3_bucket.ICHUB_PROD_MS_DOCUMENTS.id}"
//         },
//         {
//             "Sid": "InstanceObjectAllow",
//             "Effect": "Allow",
//             "Principal": {
//                 "AWS": "arn:aws:iam::942574907772:user/cmig-applicationuser"
//             },
//             "Action": [
//                 "s3:GetObject",
//                 "s3:PutObject",
//                 "s3:DeleteObject"
//             ],
//             "Resource": "arn:aws:s3:::${aws_s3_bucket.ICHUB_PROD_MS_DOCUMENTS.id}/*"
//         }
//     ]
// }
// POLICY
// }

// resource "aws_s3_bucket" "ICHUB_PROD_MS_XACTIMATE" {
//   bucket = "cmig-microservices-xactimate"
//   acl = "private"

//   lifecycle_rule {
//     enabled = true

//     transition {
//       days = 30
//       storage_class = "STANDARD_IA"
//     }

//     transition {
//       days = 60
//       storage_class = "GLACIER"
//     }
//   }
//   tags {
//     Name = "cmig-microservices-xactimate"
//   }

// }

// resource "aws_s3_bucket_policy" "ICHUB_PROD_MS_XACTIMATE_BUCKET_POLICY" {
//   bucket = "${aws_s3_bucket.ICHUB_PROD_MS_XACTIMATE.id}"

//   policy = <<POLICY
// {
//     "Version": "2012-10-17",
//     "Id": "ICHUB_PROD_MS_XACTIMATE",
//     "Statement": [
//         {
//             "Sid": "DenyPublicReadACL",
//             "Effect": "Deny",
//             "Principal": {
//                 "AWS": "*"
//             },
//             "Action": [
//                 "s3:PutObject",
//                 "s3:PutObjectAcl"
//             ],
//             "Resource": "arn:aws:s3:::${aws_s3_bucket.ICHUB_PROD_MS_XACTIMATE.id}/*",
//             "Condition": {
//                 "StringEquals": {
//                     "s3:x-amz-acl": [
//                         "public-read",
//                         "public-read-write",
//                         "authenticated-read"
//                     ]
//                 }
//             }
//         },
//         {
//             "Sid": "DenyPublicReadGrant",
//             "Effect": "Deny",
//             "Principal": {
//                 "AWS": "*"
//             },
//             "Action": [
//                 "s3:PutObject",
//                 "s3:PutObjectAcl"
//             ],
//             "Resource": "arn:aws:s3:::${aws_s3_bucket.ICHUB_PROD_MS_XACTIMATE.id}/*",
//             "Condition": {
//                 "StringLike": {
//                     "s3:x-amz-grant-read": [
//                         "*http://acs.amazonaws.com/groups/global/AllUsers*",
//                         "*http://acs.amazonaws.com/groups/global/AuthenticatedUsers*"
//                     ]
//                 }
//             }
//         },
//         {
//             "Sid": "DenyPublicListACL",
//             "Effect": "Deny",
//             "Principal": {
//                 "AWS": "*"
//             },
//             "Action": "s3:PutBucketAcl",
//             "Resource": "arn:aws:s3:::${aws_s3_bucket.ICHUB_PROD_MS_XACTIMATE.id}",
//             "Condition": {
//                 "StringEquals": {
//                     "s3:x-amz-acl": [
//                         "public-read",
//                         "public-read-write",
//                         "authenticated-read"
//                     ]
//                 }
//             }
//         },
//         {
//             "Sid": "DenyPublicListGrant",
//             "Effect": "Deny",
//             "Principal": {
//                 "AWS": "*"
//             },
//             "Action": "s3:PutBucketAcl",
//             "Resource": "arn:aws:s3:::${aws_s3_bucket.ICHUB_PROD_MS_XACTIMATE.id}",
//             "Condition": {
//                 "StringLike": {
//                     "s3:x-amz-grant-read": [
//                         "*http://acs.amazonaws.com/groups/global/AllUsers*",
//                         "*http://acs.amazonaws.com/groups/global/AuthenticatedUsers*"
//                     ]
//                 }
//             }
//         },
//         {
//             "Sid": "DenyPublicReadConsole",
//             "Effect": "Deny",
//             "Principal": {
//                 "AWS": "*"
//             },
//             "Action": [
//                 "s3:PutObjectAcl",
//                 "s3:PutBucketAcl"
//             ],
//             "Resource": [
//                 "arn:aws:s3:::${aws_s3_bucket.ICHUB_PROD_MS_XACTIMATE.id}/*",
//                 "arn:aws:s3:::${aws_s3_bucket.ICHUB_PROD_MS_XACTIMATE.id}"
//             ],
//             "Condition": {
//                 "Null": {
//                     "s3:x-amz-grant-write": "true",
//                     "s3:x-amz-grant-read": "true",
//                     "s3:x-amz-acl": "true",
//                     "s3:x-amz-grant-read-acp": "true",
//                     "s3:x-amz-grant-full-control": "true",
//                     "s3:x-amz-grant-write-acp": "true"
//                 }
//             }
//         },
//         {
//             "Sid": "InstanceListBucketAllow",
//             "Effect": "Allow",
//             "Principal": {
//                 "AWS": "arn:aws:iam::942574907772:user/cmig-applicationuser"
//             },
//             "Action": "s3:ListBucket",
//             "Resource": "arn:aws:s3:::${aws_s3_bucket.ICHUB_PROD_MS_XACTIMATE.id}"
//         },
//         {
//             "Sid": "InstanceObjectAllow",
//             "Effect": "Allow",
//             "Principal": {
//                 "AWS": "arn:aws:iam::942574907772:user/cmig-applicationuser"
//             },
//             "Action": [
//                 "s3:GetObject",
//                 "s3:PutObject",
//                 "s3:DeleteObject"
//             ],
//             "Resource": "arn:aws:s3:::${aws_s3_bucket.ICHUB_PROD_MS_XACTIMATE.id}/*"
//         }
//     ]
// }
// POLICY
// }

// resource "aws_s3_bucket" "ICHUB_DEV_MS_BMS_INITIAL_LOAD" {
//   bucket = "cmig-dev-microservices-bms-initial-load"
//   acl = "private"

//   lifecycle_rule {
//     enabled = true

//     transition {
//       days = 30
//       storage_class = "STANDARD_IA"
//     }

//     transition {
//       days = 60
//       storage_class = "GLACIER"
//     }
//   }
//   tags {
//     Name = "cmig-dev-microservices-bms-initial-load"
//   }

// }

// resource "aws_s3_bucket_policy" "ICHUB_DEV_MS_BMS_INITIAL_LOAD_BUCKET_POLICY" {
//   bucket = "${aws_s3_bucket.ICHUB_DEV_MS_BMS_INITIAL_LOAD.id}"

//   policy = <<POLICY
// {
//     "Version": "2012-10-17",
//     "Id": "ICHUB_DEV_MS_BMS_INITIAL_LOAD",
//     "Statement": [
//         {
//             "Sid": "DenyPublicReadACL",
//             "Effect": "Deny",
//             "Principal": {
//                 "AWS": "*"
//             },
//             "Action": [
//                 "s3:PutObject",
//                 "s3:PutObjectAcl"
//             ],
//             "Resource": "arn:aws:s3:::${aws_s3_bucket.ICHUB_DEV_MS_BMS_INITIAL_LOAD.id}/*",
//             "Condition": {
//                 "StringEquals": {
//                     "s3:x-amz-acl": [
//                         "public-read",
//                         "public-read-write",
//                         "authenticated-read"
//                     ]
//                 }
//             }
//         },
//         {
//             "Sid": "DenyPublicReadGrant",
//             "Effect": "Deny",
//             "Principal": {
//                 "AWS": "*"
//             },
//             "Action": [
//                 "s3:PutObject",
//                 "s3:PutObjectAcl"
//             ],
//             "Resource": "arn:aws:s3:::${aws_s3_bucket.ICHUB_DEV_MS_BMS_INITIAL_LOAD.id}/*",
//             "Condition": {
//                 "StringLike": {
//                     "s3:x-amz-grant-read": [
//                         "*http://acs.amazonaws.com/groups/global/AllUsers*",
//                         "*http://acs.amazonaws.com/groups/global/AuthenticatedUsers*"
//                     ]
//                 }
//             }
//         },
//         {
//             "Sid": "DenyPublicListACL",
//             "Effect": "Deny",
//             "Principal": {
//                 "AWS": "*"
//             },
//             "Action": "s3:PutBucketAcl",
//             "Resource": "arn:aws:s3:::${aws_s3_bucket.ICHUB_DEV_MS_BMS_INITIAL_LOAD.id}",
//             "Condition": {
//                 "StringEquals": {
//                     "s3:x-amz-acl": [
//                         "public-read",
//                         "public-read-write",
//                         "authenticated-read"
//                     ]
//                 }
//             }
//         },
//         {
//             "Sid": "DenyPublicListGrant",
//             "Effect": "Deny",
//             "Principal": {
//                 "AWS": "*"
//             },
//             "Action": "s3:PutBucketAcl",
//             "Resource": "arn:aws:s3:::${aws_s3_bucket.ICHUB_DEV_MS_BMS_INITIAL_LOAD.id}",
//             "Condition": {
//                 "StringLike": {
//                     "s3:x-amz-grant-read": [
//                         "*http://acs.amazonaws.com/groups/global/AllUsers*",
//                         "*http://acs.amazonaws.com/groups/global/AuthenticatedUsers*"
//                     ]
//                 }
//             }
//         },
//         {
//             "Sid": "DenyPublicReadConsole",
//             "Effect": "Deny",
//             "Principal": {
//                 "AWS": "*"
//             },
//             "Action": [
//                 "s3:PutObjectAcl",
//                 "s3:PutBucketAcl"
//             ],
//             "Resource": [
//                 "arn:aws:s3:::${aws_s3_bucket.ICHUB_DEV_MS_BMS_INITIAL_LOAD.id}/*",
//                 "arn:aws:s3:::${aws_s3_bucket.ICHUB_DEV_MS_BMS_INITIAL_LOAD.id}"
//             ],
//             "Condition": {
//                 "Null": {
//                     "s3:x-amz-grant-write": "true",
//                     "s3:x-amz-grant-read": "true",
//                     "s3:x-amz-acl": "true",
//                     "s3:x-amz-grant-read-acp": "true",
//                     "s3:x-amz-grant-full-control": "true",
//                     "s3:x-amz-grant-write-acp": "true"
//                 }
//             }
//         },
//         {
//             "Sid": "InstanceListBucketAllow",
//             "Effect": "Allow",
//             "Principal": {
//                 "AWS": "arn:aws:iam::623885847088:user/cmig-applicationuser"
//             },
//             "Action": "s3:ListBucket",
//             "Resource": "arn:aws:s3:::${aws_s3_bucket.ICHUB_DEV_MS_BMS_INITIAL_LOAD.id}"
//         },
//         {
//             "Sid": "InstanceObjectAllow",
//             "Effect": "Allow",
//             "Principal": {
//                 "AWS": "arn:aws:iam::623885847088:user/cmig-applicationuser"
//             },
//             "Action": [
//                 "s3:GetObject",
//                 "s3:PutObject",
//                 "s3:DeleteObject"
//             ],
//             "Resource": "arn:aws:s3:::${aws_s3_bucket.ICHUB_DEV_MS_BMS_INITIAL_LOAD.id}/*"
//         }
//     ]
// }
// POLICY
// }

// resource "aws_s3_bucket" "ICHUB_DEV_MS_DOCUMENTS" {
//   bucket = "cmig-dev-microservices-documents"
//   acl = "private"

//   lifecycle_rule {
//     enabled = true

//     transition {
//       days = 30
//       storage_class = "STANDARD_IA"
//     }

//     transition {
//       days = 60
//       storage_class = "GLACIER"
//     }
//   }
//   tags {
//     Name = "cmig-dev-microservices-documents"
//   }

// }

// resource "aws_s3_bucket_policy" "ICHUB_DEV_MS_DOCUMENTS_BUCKET_POLICY" {
//   bucket = "${aws_s3_bucket.ICHUB_DEV_MS_DOCUMENTS.id}"

//   policy = <<POLICY
// {
//     "Version": "2012-10-17",
//     "Id": "ICHUB_DEV_MS_DOCUMENTS",
//     "Statement": [
//         {
//             "Sid": "DenyPublicReadACL",
//             "Effect": "Deny",
//             "Principal": {
//                 "AWS": "*"
//             },
//             "Action": [
//                 "s3:PutObject",
//                 "s3:PutObjectAcl"
//             ],
//             "Resource": "arn:aws:s3:::${aws_s3_bucket.ICHUB_DEV_MS_DOCUMENTS.id}/*",
//             "Condition": {
//                 "StringEquals": {
//                     "s3:x-amz-acl": [
//                         "public-read",
//                         "public-read-write",
//                         "authenticated-read"
//                     ]
//                 }
//             }
//         },
//         {
//             "Sid": "DenyPublicReadGrant",
//             "Effect": "Deny",
//             "Principal": {
//                 "AWS": "*"
//             },
//             "Action": [
//                 "s3:PutObject",
//                 "s3:PutObjectAcl"
//             ],
//             "Resource": "arn:aws:s3:::${aws_s3_bucket.ICHUB_DEV_MS_DOCUMENTS.id}/*",
//             "Condition": {
//                 "StringLike": {
//                     "s3:x-amz-grant-read": [
//                         "*http://acs.amazonaws.com/groups/global/AllUsers*",
//                         "*http://acs.amazonaws.com/groups/global/AuthenticatedUsers*"
//                     ]
//                 }
//             }
//         },
//         {
//             "Sid": "DenyPublicListACL",
//             "Effect": "Deny",
//             "Principal": {
//                 "AWS": "*"
//             },
//             "Action": "s3:PutBucketAcl",
//             "Resource": "arn:aws:s3:::${aws_s3_bucket.ICHUB_DEV_MS_DOCUMENTS.id}",
//             "Condition": {
//                 "StringEquals": {
//                     "s3:x-amz-acl": [
//                         "public-read",
//                         "public-read-write",
//                         "authenticated-read"
//                     ]
//                 }
//             }
//         },
//         {
//             "Sid": "DenyPublicListGrant",
//             "Effect": "Deny",
//             "Principal": {
//                 "AWS": "*"
//             },
//             "Action": "s3:PutBucketAcl",
//             "Resource": "arn:aws:s3:::${aws_s3_bucket.ICHUB_DEV_MS_DOCUMENTS.id}",
//             "Condition": {
//                 "StringLike": {
//                     "s3:x-amz-grant-read": [
//                         "*http://acs.amazonaws.com/groups/global/AllUsers*",
//                         "*http://acs.amazonaws.com/groups/global/AuthenticatedUsers*"
//                     ]
//                 }
//             }
//         },
//         {
//             "Sid": "DenyPublicReadConsole",
//             "Effect": "Deny",
//             "Principal": {
//                 "AWS": "*"
//             },
//             "Action": [
//                 "s3:PutObjectAcl",
//                 "s3:PutBucketAcl"
//             ],
//             "Resource": [
//                 "arn:aws:s3:::${aws_s3_bucket.ICHUB_DEV_MS_DOCUMENTS.id}/*",
//                 "arn:aws:s3:::${aws_s3_bucket.ICHUB_DEV_MS_DOCUMENTS.id}"
//             ],
//             "Condition": {
//                 "Null": {
//                     "s3:x-amz-grant-write": "true",
//                     "s3:x-amz-grant-read": "true",
//                     "s3:x-amz-acl": "true",
//                     "s3:x-amz-grant-read-acp": "true",
//                     "s3:x-amz-grant-full-control": "true",
//                     "s3:x-amz-grant-write-acp": "true"
//                 }
//             }
//         },
//         {
//             "Sid": "InstanceListBucketAllow",
//             "Effect": "Allow",
//             "Principal": {
//                 "AWS": "arn:aws:iam::623885847088:user/cmig-applicationuser"
//             },
//             "Action": "s3:ListBucket",
//             "Resource": "arn:aws:s3:::${aws_s3_bucket.ICHUB_DEV_MS_DOCUMENTS.id}"
//         },
//         {
//             "Sid": "InstanceObjectAllow",
//             "Effect": "Allow",
//             "Principal": {
//                 "AWS": "arn:aws:iam::623885847088:user/cmig-applicationuser"
//             },
//             "Action": [
//                 "s3:GetObject",
//                 "s3:PutObject",
//                 "s3:DeleteObject"
//             ],
//             "Resource": "arn:aws:s3:::${aws_s3_bucket.ICHUB_DEV_MS_DOCUMENTS.id}/*"
//         }
//     ]
// }
// POLICY
// }

// resource "aws_s3_bucket" "ICHUB_DEV_MS_XACTIMATE" {
//   bucket = "cmig-dev-microservices-xactimate"
//   acl = "private"

//   lifecycle_rule {
//     enabled = true

//     transition {
//       days = 30
//       storage_class = "STANDARD_IA"
//     }

//     transition {
//       days = 60
//       storage_class = "GLACIER"
//     }
//   }
//   tags {
//     Name = "cmig-dev-microservices-xactimate"
//   }

// }

// resource "aws_s3_bucket_policy" "ICHUB_DEV_MS_XACTIMATE_BUCKET_POLICY" {
//   bucket = "${aws_s3_bucket.ICHUB_DEV_MS_XACTIMATE.id}"

//   policy = <<POLICY
// {
//     "Version": "2012-10-17",
//     "Id": "ICHUB_PROD_MS_XACTIMATE",
//     "Statement": [
//         {
//             "Sid": "DenyPublicReadACL",
//             "Effect": "Deny",
//             "Principal": {
//                 "AWS": "*"
//             },
//             "Action": [
//                 "s3:PutObject",
//                 "s3:PutObjectAcl"
//             ],
//             "Resource": "arn:aws:s3:::${aws_s3_bucket.ICHUB_DEV_MS_XACTIMATE.id}/*",
//             "Condition": {
//                 "StringEquals": {
//                     "s3:x-amz-acl": [
//                         "public-read",
//                         "public-read-write",
//                         "authenticated-read"
//                     ]
//                 }
//             }
//         },
//         {
//             "Sid": "DenyPublicReadGrant",
//             "Effect": "Deny",
//             "Principal": {
//                 "AWS": "*"
//             },
//             "Action": [
//                 "s3:PutObject",
//                 "s3:PutObjectAcl"
//             ],
//             "Resource": "arn:aws:s3:::${aws_s3_bucket.ICHUB_DEV_MS_XACTIMATE.id}/*",
//             "Condition": {
//                 "StringLike": {
//                     "s3:x-amz-grant-read": [
//                         "*http://acs.amazonaws.com/groups/global/AllUsers*",
//                         "*http://acs.amazonaws.com/groups/global/AuthenticatedUsers*"
//                     ]
//                 }
//             }
//         },
//         {
//             "Sid": "DenyPublicListACL",
//             "Effect": "Deny",
//             "Principal": {
//                 "AWS": "*"
//             },
//             "Action": "s3:PutBucketAcl",
//             "Resource": "arn:aws:s3:::${aws_s3_bucket.ICHUB_DEV_MS_XACTIMATE.id}",
//             "Condition": {
//                 "StringEquals": {
//                     "s3:x-amz-acl": [
//                         "public-read",
//                         "public-read-write",
//                         "authenticated-read"
//                     ]
//                 }
//             }
//         },
//         {
//             "Sid": "DenyPublicListGrant",
//             "Effect": "Deny",
//             "Principal": {
//                 "AWS": "*"
//             },
//             "Action": "s3:PutBucketAcl",
//             "Resource": "arn:aws:s3:::${aws_s3_bucket.ICHUB_DEV_MS_XACTIMATE.id}",
//             "Condition": {
//                 "StringLike": {
//                     "s3:x-amz-grant-read": [
//                         "*http://acs.amazonaws.com/groups/global/AllUsers*",
//                         "*http://acs.amazonaws.com/groups/global/AuthenticatedUsers*"
//                     ]
//                 }
//             }
//         },
//         {
//             "Sid": "DenyPublicReadConsole",
//             "Effect": "Deny",
//             "Principal": {
//                 "AWS": "*"
//             },
//             "Action": [
//                 "s3:PutObjectAcl",
//                 "s3:PutBucketAcl"
//             ],
//             "Resource": [
//                 "arn:aws:s3:::${aws_s3_bucket.ICHUB_DEV_MS_XACTIMATE.id}/*",
//                 "arn:aws:s3:::${aws_s3_bucket.ICHUB_DEV_MS_XACTIMATE.id}"
//             ],
//             "Condition": {
//                 "Null": {
//                     "s3:x-amz-grant-write": "true",
//                     "s3:x-amz-grant-read": "true",
//                     "s3:x-amz-acl": "true",
//                     "s3:x-amz-grant-read-acp": "true",
//                     "s3:x-amz-grant-full-control": "true",
//                     "s3:x-amz-grant-write-acp": "true"
//                 }
//             }
//         },
//         {
//             "Sid": "InstanceListBucketAllow",
//             "Effect": "Allow",
//             "Principal": {
//                 "AWS": "arn:aws:iam::623885847088:user/cmig-applicationuser"
//             },
//             "Action": "s3:ListBucket",
//             "Resource": "arn:aws:s3:::${aws_s3_bucket.ICHUB_DEV_MS_XACTIMATE.id}"
//         },
//         {
//             "Sid": "InstanceObjectAllow",
//             "Effect": "Allow",
//             "Principal": {
//                 "AWS": "arn:aws:iam::623885847088:user/cmig-applicationuser"
//             },
//             "Action": [
//                 "s3:GetObject",
//                 "s3:PutObject",
//                 "s3:DeleteObject"
//             ],
//             "Resource": "arn:aws:s3:::${aws_s3_bucket.ICHUB_DEV_MS_XACTIMATE.id}/*"
//         }
//     ]
// }
// POLICY
// } 

// resource "aws_s3_bucket" "ICHUB_HUB_SAP_SFTP" {
//   bucket = "cmig-hub-sap-sftp"
//   acl = "private"

//   lifecycle_rule {
//     enabled = true

//     transition {
//       days = 30
//       storage_class = "STANDARD_IA"
//     }

//     transition {
//       days = 60
//       storage_class = "GLACIER"
//     }
//   }
//   tags {
//     Name = "cmig-hub-sap-sftp"
//   }

// }

// resource "aws_s3_bucket_policy" "ICHUB_HUB_SAP_SFTP_POLICY" {
//   bucket = "${aws_s3_bucket.ICHUB_HUB_SAP_SFTP.id}"

//   policy = <<POLICY
// {
//     "Version": "2012-10-17",
//     "Id": "ICHUB_HUB_SAP_SFTP",
//     "Statement": [
//         {
//             "Sid": "DenyPublicReadACL",
//             "Effect": "Deny",
//             "Principal": {
//                 "AWS": "*"
//             },
//             "Action": [
//                 "s3:PutObject",
//                 "s3:PutObjectAcl"
//             ],
//             "Resource": "arn:aws:s3:::${aws_s3_bucket.ICHUB_HUB_SAP_SFTP.id}/*",
//             "Condition": {
//                 "StringEquals": {
//                     "s3:x-amz-acl": [
//                         "public-read",
//                         "public-read-write",
//                         "authenticated-read"
//                     ]
//                 }
//             }
//         },
//         {
//             "Sid": "DenyPublicReadGrant",
//             "Effect": "Deny",
//             "Principal": {
//                 "AWS": "*"
//             },
//             "Action": [
//                 "s3:PutObject",
//                 "s3:PutObjectAcl"
//             ],
//             "Resource": "arn:aws:s3:::${aws_s3_bucket.ICHUB_HUB_SAP_SFTP.id}/*",
//             "Condition": {
//                 "StringLike": {
//                     "s3:x-amz-grant-read": [
//                         "*http://acs.amazonaws.com/groups/global/AllUsers*",
//                         "*http://acs.amazonaws.com/groups/global/AuthenticatedUsers*"
//                     ]
//                 }
//             }
//         },
//         {
//             "Sid": "DenyPublicListACL",
//             "Effect": "Deny",
//             "Principal": {
//                 "AWS": "*"
//             },
//             "Action": "s3:PutBucketAcl",
//             "Resource": "arn:aws:s3:::${aws_s3_bucket.ICHUB_HUB_SAP_SFTP.id}",
//             "Condition": {
//                 "StringEquals": {
//                     "s3:x-amz-acl": [
//                         "public-read",
//                         "public-read-write",
//                         "authenticated-read"
//                     ]
//                 }
//             }
//         },
//         {
//             "Sid": "DenyPublicListGrant",
//             "Effect": "Deny",
//             "Principal": {
//                 "AWS": "*"
//             },
//             "Action": "s3:PutBucketAcl",
//             "Resource": "arn:aws:s3:::${aws_s3_bucket.ICHUB_HUB_SAP_SFTP.id}",
//             "Condition": {
//                 "StringLike": {
//                     "s3:x-amz-grant-read": [
//                         "*http://acs.amazonaws.com/groups/global/AllUsers*",
//                         "*http://acs.amazonaws.com/groups/global/AuthenticatedUsers*"
//                     ]
//                 }
//             }
//         },
//         {
//             "Sid": "DenyPublicReadConsole",
//             "Effect": "Deny",
//             "Principal": {
//                 "AWS": "*"
//             },
//             "Action": [
//                 "s3:PutObjectAcl",
//                 "s3:PutBucketAcl"
//             ],
//             "Resource": [
//                 "arn:aws:s3:::${aws_s3_bucket.ICHUB_HUB_SAP_SFTP.id}/*",
//                 "arn:aws:s3:::${aws_s3_bucket.ICHUB_HUB_SAP_SFTP.id}"
//             ],
//             "Condition": {
//                 "Null": {
//                     "s3:x-amz-grant-write": "true",
//                     "s3:x-amz-grant-read": "true",
//                     "s3:x-amz-acl": "true",
//                     "s3:x-amz-grant-read-acp": "true",
//                     "s3:x-amz-grant-full-control": "true",
//                     "s3:x-amz-grant-write-acp": "true"
//                 }
//             }
//         },
//         {
//             "Sid": "InstanceListBucketAllow",
//             "Effect": "Allow",
//             "Principal": {
//                 "AWS": "arn:aws:iam::623885847088:role/CmigServiceRole",
//                 "AWS": "arn:aws:iam::942574907772:role/CmigServiceRole", 
//                 "AWS": "arn:aws:iam::532382648425:role/CmigServiceRole"
//             },
//             "Action": "s3:ListBucket",
//             "Resource": "arn:aws:s3:::${aws_s3_bucket.ICHUB_HUB_SAP_SFTP.id}"
//         },
//         {
//             "Sid": "InstanceObjectAllow",
//             "Effect": "Allow",
//             "Principal": {
//                 "AWS": "arn:aws:iam::623885847088:role/CmigServiceRole",
//                 "AWS": "arn:aws:iam::942574907772:role/CmigServiceRole",
//                 "AWS": "arn:aws:iam::532382648425:role/CmigServiceRole"
//             },
//             "Action": [
//                 "s3:GetObject",
//                 "s3:PutObject",
//                 "s3:DeleteObject"
//             ],
//             "Resource":   ["arn:aws:s3:::${aws_s3_bucket.ICHUB_HUB_SAP_SFTP.id}/*",
//                 "arn:aws:s3:::${aws_s3_bucket.ICHUB_HUB_SAP_SFTP.id}"]
//         }
//     ]
// }
// POLICY
// }

// resource "aws_s3_bucket" "ICHUB_PROD_EXTERNAL_NLB" {
//   bucket = "cmig-prod-external-traffic-access-logs"
//   acl = "private"

//   lifecycle_rule {
//     enabled = true

//     transition {
//       days = 30
//       storage_class = "STANDARD_IA"
//     }

//     transition {
//       days = 60
//       storage_class = "GLACIER"
//     }
//   }
//   tags {
//     Name = "cmig-prod-external-traffic-access-logs"
//   }
// }

// resource "aws_s3_bucket_policy" "ICHUB_PROD_EXTERNAL_NLB_POLICY" {
//   bucket = "${aws_s3_bucket.ICHUB_PROD_EXTERNAL_NLB.id}"

//   policy = <<POLICY
// {
//     "Version": "2012-10-17",
//     "Id": "ICHUB_PROD_EXTERNAL_NLB",
//     "Statement": [
//         {
//             "Sid": "DenyPublicReadACL",
//             "Effect": "Deny",
//             "Principal": {
//                 "AWS": "*"
//             },
//             "Action": [
//                 "s3:PutObject",
//                 "s3:PutObjectAcl"
//             ],
//             "Resource": "arn:aws:s3:::${aws_s3_bucket.ICHUB_PROD_EXTERNAL_NLB.id}/*",
//             "Condition": {
//                 "StringEquals": {
//                     "s3:x-amz-acl": [
//                         "public-read",
//                         "public-read-write",
//                         "authenticated-read"
//                     ]
//                 }
//             }
//         },
//         {
//             "Sid": "DenyPublicReadGrant",
//             "Effect": "Deny",
//             "Principal": {
//                 "AWS": "*"
//             },
//             "Action": [
//                 "s3:PutObject",
//                 "s3:PutObjectAcl"
//             ],
//             "Resource": "arn:aws:s3:::${aws_s3_bucket.ICHUB_PROD_EXTERNAL_NLB.id}/*",
//             "Condition": {
//                 "StringLike": {
//                     "s3:x-amz-grant-read": [
//                         "*http://acs.amazonaws.com/groups/global/AllUsers*",
//                         "*http://acs.amazonaws.com/groups/global/AuthenticatedUsers*"
//                     ]
//                 }
//             }
//         },
//         {
//             "Sid": "DenyPublicListACL",
//             "Effect": "Deny",
//             "Principal": {
//                 "AWS": "*"
//             },
//             "Action": "s3:PutBucketAcl",
//             "Resource": "arn:aws:s3:::${aws_s3_bucket.ICHUB_PROD_EXTERNAL_NLB.id}",
//             "Condition": {
//                 "StringEquals": {
//                     "s3:x-amz-acl": [
//                         "public-read",
//                         "public-read-write",
//                         "authenticated-read"
//                     ]
//                 }
//             }
//         },
//         {
//             "Sid": "DenyPublicListGrant",
//             "Effect": "Deny",
//             "Principal": {
//                 "AWS": "*"
//             },
//             "Action": "s3:PutBucketAcl",
//             "Resource": "arn:aws:s3:::${aws_s3_bucket.ICHUB_PROD_EXTERNAL_NLB.id}",
//             "Condition": {
//                 "StringLike": {
//                     "s3:x-amz-grant-read": [
//                         "*http://acs.amazonaws.com/groups/global/AllUsers*",
//                         "*http://acs.amazonaws.com/groups/global/AuthenticatedUsers*"
//                     ]
//                 }
//             }
//         },
//         {
//             "Sid": "DenyPublicReadConsole",
//             "Effect": "Deny",
//             "Principal": {
//                 "AWS": "*"
//             },
//             "Action": [
//                 "s3:PutObjectAcl",
//                 "s3:PutBucketAcl"
//             ],
//             "Resource": [
//                 "arn:aws:s3:::${aws_s3_bucket.ICHUB_PROD_EXTERNAL_NLB.id}/*",
//                 "arn:aws:s3:::${aws_s3_bucket.ICHUB_PROD_EXTERNAL_NLB.id}"
//             ],
//             "Condition": {
//                 "Null": {
//                     "s3:x-amz-grant-write": "true",
//                     "s3:x-amz-grant-read": "true",
//                     "s3:x-amz-acl": "true",
//                     "s3:x-amz-grant-read-acp": "true",
//                     "s3:x-amz-grant-full-control": "true",
//                     "s3:x-amz-grant-write-acp": "true"
//                 }
//             }
//         }
//     ]
// }
// POLICY
// }

// resource "aws_s3_bucket" "ICHUB_PPS_EXTERNAL_NLB" {
//   bucket = "cmig-pps-external-traffic-access-logs"
//   acl = "private"

//   lifecycle_rule {
//     enabled = true

//     transition {
//       days = 30
//       storage_class = "STANDARD_IA"
//     }

//     transition {
//       days = 60
//       storage_class = "GLACIER"
//     }
//   }
//   tags {
//     Name = "cmig-pps-external-traffic-access-logs"
//   }
// }

// resource "aws_s3_bucket_policy" "ICHUB_PPS_EXTERNAL_NLB_POLICY" {
//   bucket = "${aws_s3_bucket.ICHUB_PPS_EXTERNAL_NLB.id}"

//   policy = <<POLICY
// {
//     "Version": "2012-10-17",
//     "Id": "ICHUB_PPS_EXTERNAL_NLB",
//     "Statement": [
//         {
//             "Sid": "DenyPublicReadACL",
//             "Effect": "Deny",
//             "Principal": {
//                 "AWS": "*"
//             },
//             "Action": [
//                 "s3:PutObject",
//                 "s3:PutObjectAcl"
//             ],
//             "Resource": "arn:aws:s3:::${aws_s3_bucket.ICHUB_PPS_EXTERNAL_NLB.id}/*",
//             "Condition": {
//                 "StringEquals": {
//                     "s3:x-amz-acl": [
//                         "public-read",
//                         "public-read-write",
//                         "authenticated-read"
//                     ]
//                 }
//             }
//         },
//         {
//             "Sid": "DenyPublicReadGrant",
//             "Effect": "Deny",
//             "Principal": {
//                 "AWS": "*"
//             },
//             "Action": [
//                 "s3:PutObject",
//                 "s3:PutObjectAcl"
//             ],
//             "Resource": "arn:aws:s3:::${aws_s3_bucket.ICHUB_PPS_EXTERNAL_NLB.id}/*",
//             "Condition": {
//                 "StringLike": {
//                     "s3:x-amz-grant-read": [
//                         "*http://acs.amazonaws.com/groups/global/AllUsers*",
//                         "*http://acs.amazonaws.com/groups/global/AuthenticatedUsers*"
//                     ]
//                 }
//             }
//         },
//         {
//             "Sid": "DenyPublicListACL",
//             "Effect": "Deny",
//             "Principal": {
//                 "AWS": "*"
//             },
//             "Action": "s3:PutBucketAcl",
//             "Resource": "arn:aws:s3:::${aws_s3_bucket.ICHUB_PPS_EXTERNAL_NLB.id}",
//             "Condition": {
//                 "StringEquals": {
//                     "s3:x-amz-acl": [
//                         "public-read",
//                         "public-read-write",
//                         "authenticated-read"
//                     ]
//                 }
//             }
//         },
//         {
//             "Sid": "DenyPublicListGrant",
//             "Effect": "Deny",
//             "Principal": {
//                 "AWS": "*"
//             },
//             "Action": "s3:PutBucketAcl",
//             "Resource": "arn:aws:s3:::${aws_s3_bucket.ICHUB_PPS_EXTERNAL_NLB.id}",
//             "Condition": {
//                 "StringLike": {
//                     "s3:x-amz-grant-read": [
//                         "*http://acs.amazonaws.com/groups/global/AllUsers*",
//                         "*http://acs.amazonaws.com/groups/global/AuthenticatedUsers*"
//                     ]
//                 }
//             }
//         },
//         {
//             "Sid": "DenyPublicReadConsole",
//             "Effect": "Deny",
//             "Principal": {
//                 "AWS": "*"
//             },
//             "Action": [
//                 "s3:PutObjectAcl",
//                 "s3:PutBucketAcl"
//             ],
//             "Resource": [
//                 "arn:aws:s3:::${aws_s3_bucket.ICHUB_PPS_EXTERNAL_NLB.id}/*",
//                 "arn:aws:s3:::${aws_s3_bucket.ICHUB_PPS_EXTERNAL_NLB.id}"
//             ],
//             "Condition": {
//                 "Null": {
//                     "s3:x-amz-grant-write": "true",
//                     "s3:x-amz-grant-read": "true",
//                     "s3:x-amz-acl": "true",
//                     "s3:x-amz-grant-read-acp": "true",
//                     "s3:x-amz-grant-full-control": "true",
//                     "s3:x-amz-grant-write-acp": "true"
//                 }
//             }
//         }
//     ]
// }
// POLICY
// }
