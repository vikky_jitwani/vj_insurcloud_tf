resource "aws_db_subnet_group" "RPA_MSSQL_DB_SUBNET" {
    name        = "default-eco-mssql-db-subnet"
    description = "RDS Subnet Group"
    subnet_ids  = ["${var.economical-prod-database-1a-subnet}", "${var.economical-prod-database-1b-subnet}"]
}

# #=======================================================================
# # PROD01
# #=======================================================================

resource "aws_db_instance" "PROD01_RPA_MSSQL_DB" {
    license_model       = "license-included"
    engine              = "sqlserver-se"
    engine_version      = "14.00.3281.6.v1"
    instance_class      = "db.r5.large"
    allocated_storage   = 125
    storage_encrypted   = true # sql express does not support storage encryption
    identifier          = "economical-prod01-rpa-mssqldb"
    username            = "sa"
    password            = "${var.mssql-rds-password}" # password
    port                = 1433
    timezone            = "Eastern Standard Time"
    final_snapshot_identifier = "DELETEME"
    skip_final_snapshot = true

    domain              = "d-9d6721ab36"
    domain_iam_role_name = "CMS-SSMRole"

    maintenance_window  = "Mon:00:00-Mon:03:00"
    backup_window       = "03:00-06:00"
    deletion_protection = true

    db_subnet_group_name    = "${aws_db_subnet_group.RPA_MSSQL_DB_SUBNET.name}"
    parameter_group_name    = "default.sqlserver-se-14.0"
    multi_az                = true
    vpc_security_group_ids  = ["${aws_security_group.SG_MSSQL_DB.id}"]
    storage_type            = "gp2"
    backup_retention_period = 30 # how long you're going to keep your backups

    enabled_cloudwatch_logs_exports = [
        "agent",
        "error"
    ]

    copy_tags_to_snapshot = true
    tags = {
        Contract = "MANAGED_SERVICES"
        Name        = "economical-prod01-rpa-mssqldb"
        Environment = "PROD01"
    }
}

output "ICECONOMICAL_PROD01_RPA_DB_PRIVATE_DNS" {
    value = "${aws_db_instance.PROD01_RPA_MSSQL_DB.address}"
}
