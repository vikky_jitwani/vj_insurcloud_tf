
#### CONDUIT DB #####

resource "aws_db_subnet_group" "POSTGRES_DB_SUBNET" {
    name        = "postgres-db-subnet"
    description = "RDS subnet group"
    subnet_ids  = ["${var.commonwell-hub-database-1a-subnet}", "${var.commonwell-hub-database-1b-subnet}"]
}

resource "aws_db_instance" "conduitdb" {
  engine              = "postgres"
  engine_version      = "10.13"
  instance_class      = "db.t3.medium"
  allocated_storage   = 20
  storage_encrypted   = true
  identifier          = "conduitdb"
  username            = "postgres"
  password            = "${var.conduit-rds-password}" # password
  port                = 5432
  maintenance_window  = "Mon:00:00-Mon:03:00"
  backup_window       = "03:00-06:00"
  deletion_protection = true
}
