variable "client_name" {
  description = "AWS client name for resources (Ex: MASTERCLOUD)"
  default = ""
}

variable "enviornment_name" {
  description = "AWS enviornment name for resources (Ex: NON-PROD)"
  default = ""
}

variable "enviornment_type" {
  description = "AWS enviornment type for resources (Ex: P=PROD/HUB, D=NON-PROD)"
  default = ""
}

variable "client_number" {
  description = "AWS client number for tagging (Ex: A001)"
  default = "A003"
}

variable "region" {
  description = "AWS region for hosting our your network"
  default = "ca-central-1"
}

variable "volume_key" {
  description = "KMS volume key hub account"
  default = ""
}

variable "ami_id_amazonlinux201709base" {
  description = "Amazon Linux Encypted Root Volume AMI"
  default = ""
}

variable "ami_id_redhatlinux75" {
  description = "Redhat Linux Encypted Root Volume AMI"
  default = ""
}

variable "ami_id_amazonlinux201803base" {
  description = "Amazon Linux 2018 03 Encypted Root Volume AMI"
  default = ""
}

variable "ami_id_amazonlinux2base" {
  description = "Amazon Linux 2 Base Encypted Root Volume AMI"
  default = ""
}

variable "ami_id_windows2008base" {
  description = "Windows 2008 Base Encypted Root Volume AMI"
  default = ""
}

variable "ami_id_amazonlinux201712base" {
  description = "Amazon Linux 2017 12 Base Encypted Root Volume AMI"
  default = ""
}

variable "ami_id_windows2016base" {
  description = "Windows 2016 Base Encypted Root Volume AMI"
  default = ""
}

variable "ami_id_windows2012base" {
  description = "Windows 2012 Base Encypted Root Volume AMI"
  default = ""
}

variable "ami_id_windows2012R2base" {
  description = "Windows 2012 R2 Base Encypted Root Volume AMI"
  default = ""
}

variable "ami_id_ubuntu1604baseimage" {
  description = "Ubuntu 16 04 Base Encypted Root Volume AMI"
  default = ""
}

variable "ami_id_ubuntu1804baseimage" {
  description = "Ubuntu 18 04 Base Encypted Root Volume AMI"
  default = ""
}

variable "ami_id_rhel75baseimage" {
  description = "RHEL 7.5 Base image from the cloud"
  default = ""
}


variable "vpc_id" {
  description = "VPC id for HUB account"
  default = "vpc-01c39132b104e1b86"
}

variable "vpc_cidr" {
  description = "VPC cidr dev account"
  default = ""
}

variable "conduit-rds-password" {
  description = "Conduit DB Password"
  default = ""
}

variable "commonwell-hub-database-1a-subnet" {
  description = "Database subnet one"
  default = ""
}

variable "commonwell-hub-database-1b-subnet" {
  description = "Database subnet two"
  default = ""
}

variable "commonwell-hub-private-1a-subnet" {
  description = "Private Subnet One"
  default = ""
}

variable "commonwell-hub-private-1b-subnet" {
  description = "Private Subnet Two"
  default = ""
}

variable "commonwell-hub-public-1a-subnet" {
  description = "Public Subnet One"
  default = ""
}

variable "commonwell-hub-public-1b-subnet" {
  description = "Public Subnet two"
  default = ""
}

variable "default_cidr" {
  description = "default cidr used"
  default = ["10.0.0.0/8"]
}

variable "deloitte_cidr" {
    default = "10.10.4.0/22"
}

variable "commonwell_aws_cidr" {
    default = "172.31.0.0/16"
}

variable "commonwell_lindsay_cidr" {
    default = "10.19.0.0/16"
}

variable "commonwell_whitby_cidr" {
    default = "10.20.0.0/16"
}

variable "zone_id" {
  description = "default zone id"
  default = ""
}

variable "commonwell-dev_cidr" {
  description = "default cidr used"
  type = "list"
  default = ["10.10.64.0/20"]
}

variable "commonwell-prod_cidr" {
  description = "default cidr used"
  type = "list"
  default = ["10.10.48.0/20"]
}




variable "insurcloud-hub_cidr" {
  description = "default cidr used"
  type = "list"
  default = ["10.10.4.0/22"]
}

variable "insurcloud-hub_private_cidr" {
  description = "default cidr used"
  type = "list"
  default = ["10.10.4.0/24", "10.10.6.0/24"]
}

data "aws_caller_identity" "current" {}
