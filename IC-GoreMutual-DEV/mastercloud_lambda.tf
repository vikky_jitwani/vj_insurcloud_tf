## Document-Service ##
resource "aws_lambda_permission" "s3_lambda_smartcomm" {
  statement_id  = "AllowExecutionFromS3"
  action        = "lambda:InvokeFunction"
  function_name = "${aws_lambda_function.gore_smartcomm_upload.function_name}"
  principal     = "s3.amazonaws.com"
  source_arn = "arn:aws:s3:::gore-smartcomm-dev-output" 
}

resource "aws_lambda_function" "gore_smartcomm_upload" {

  function_name = "gore_smartcomm_upload"
  role          = "arn:aws:iam::${var.gore-dev-account}:role/Gore-LambdaBasicExecutionRoleForCreatingFunctions"
  handler       = "lambda_function.lambda_handler"
  runtime       = "python3.7"
  timeout       = 60
  s3_bucket     = "gore-smartcomm-source-lambda"
  s3_key        = "smartcomm_lambda.zip"

  vpc_config {
    subnet_ids = ["${var.gore-dev-private-1a-subnet}", "${var.gore-dev-private-1b-subnet}"]
    security_group_ids = ["${aws_security_group.SG_LAMBDA.id}"]
  }
}
 
##################
# Adding S3 bucket as trigger to my lambda and giving the permissions
##################
resource "aws_s3_bucket_notification" "aws_lambda_trigger" {
  bucket = "gore-smartcomm-dev-output"
  lambda_function {
    lambda_function_arn = "${aws_lambda_function.gore_smartcomm_upload.arn}"
  events              = ["s3:ObjectCreated:*"]
  filter_prefix       = "output/archive/"
  filter_suffix       = ".xml"
}
}

resource "aws_lambda_permission" "test" {
statement_id  = "AllowS3Invoke"
action        = "lambda:InvokeFunction"
function_name = "${aws_lambda_function.gore_smartcomm_upload.function_name}"
principal = "s3.amazonaws.com"
source_arn = "arn:aws:s3:::gore-smartcomm-dev-output"
}
###########
# output of lambda arn
###########
output "arn" {
value = "${aws_lambda_function.gore_smartcomm_upload.arn}"
}
