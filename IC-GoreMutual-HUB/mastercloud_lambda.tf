######################NONPROD##############################

#API Gateway permission 
resource "aws_lambda_permission" "apigw_lambda_authenrizer" {
  statement_id  = "AllowExecutionFromAPIGateway"
  action        = "lambda:InvokeFunction"
  function_name = "${aws_lambda_function.gore_AGWAuthenrizer_Address_Hub.function_name}"
  principal     = "apigateway.amazonaws.com"
  # More: http://docs.aws.amazon.com/apigateway/latest/developerguide/api-gateway-control-access-using-iam-policies-to-invoke-api.html
  source_arn = "arn:aws:execute-api:${var.region}:${var.gore-hub-account}:*"
  #source_arn = "arn:aws:execute-api:${var.region}:${var.gore-hub-account}:${aws_api_gateway_rest_api.GORE.id}/*"
  #source_arn = "arn:aws:execute-api:${var.region}:${var.gore-hub-account}:${aws_api_gateway_rest_api.GORE_AUTH0.id}/*"
}

#Lambda function for Nonprod AddressValidation
resource "aws_lambda_function" "gore_AGWAuthenrizer_Address_Hub" {
  function_name = "gore_AGWAuthenrizer_Address_Hub"
  role          = "arn:aws:iam::${var.gore-hub-account}:role/LambdaAssume_Hub"
  handler       = "index.handler"
  runtime       = "nodejs10.x"
  s3_bucket     = "gore-custom-auth"
  s3_key        = "custom-authorizer.zip"

  environment {
    variables = {
      AUDIENCE = "https://gore.insurcloud-dev.ca/api/address-validation/v1"
      JWKS_URI = "https://kai-test.us.auth0.com/.well-known/jwks.json"
      TOKEN_ISSUER = "https://kai-test.us.auth0.com/"
    }
  }
}

#Lambda function for Nonprod AddressValidation
resource "aws_lambda_function" "gore_AGWAuthenrizer_AddressValidation_Hub" {
  function_name = "gore_AGWAuthenrizer_AddressValidation_Hub"
  role          = "arn:aws:iam::${var.gore-hub-account}:role/LambdaAssume_Hub"
  handler       = "index.handler"
  runtime       = "nodejs10.x"
  s3_bucket     = "gore-custom-auth"
  s3_key        = "custom-authorizer.zip"

  environment {
    variables = {
      AUDIENCE = "https://insurcloud/address-validation"
      JWKS_URI = "https://dev.login.goremutual.ca/.well-known/jwks.json"
      TOKEN_ISSUER = "https://dev.login.goremutual.ca/"
    }
  }
}

#Lambda function for Nonprod Cheque recon
resource "aws_lambda_function" "gore_AGWAuthenrizer_Cheque_Hub" {
  function_name = "gore_AGWAuthenrizer_Cheque_Hub"
  role          = "arn:aws:iam::${var.gore-hub-account}:role/LambdaAssume_Hub"
  handler       = "index.handler"
  runtime       = "nodejs10.x"
  s3_bucket     = "gore-custom-auth"
  s3_key        = "custom-authorizer.zip"

  environment {
    variables = {
      AUDIENCE = "https://insurcloud/cheque-printing"
      JWKS_URI = "https://dev.login.goremutual.ca/.well-known/jwks.json"
      TOKEN_ISSUER = "https://dev.login.goremutual.ca/"
    }
  }
}
#Lambda function for Nonprod MVR service
resource "aws_lambda_function" "gore_AGWAuthenrizer_MVR_Hub" {
  function_name = "gore_AGWAuthenrizer_MVR_Hub"
  role          = "arn:aws:iam::${var.gore-hub-account}:role/LambdaAssume_Hub"
  handler       = "index.handler"
  runtime       = "nodejs10.x"
  s3_bucket     = "gore-custom-auth"
  s3_key        = "custom-authorizer.zip"

  environment {
    variables = {
      AUDIENCE = "https://insurcloud/mvr"
      JWKS_URI = "https://dev.login.goremutual.ca/.well-known/jwks.json"
      TOKEN_ISSUER = "https://dev.login.goremutual.ca/"
    }
  }
}

#Lambda function for Nonprod Uniban service
resource "aws_lambda_function" "gore_AGWAuthenrizer_UnibanService_Hub" {
  function_name = "gore_AGWAuthenrizer_UnibanService_Hub"
  role          = "arn:aws:iam::${var.gore-hub-account}:role/LambdaAssume_Hub"
  handler       = "index.handler"
  runtime       = "nodejs10.x"
  s3_bucket     = "gore-custom-auth"
  s3_key        = "custom-authorizer.zip"

  environment {
    variables = {
      AUDIENCE = "https://uniban/uniban"
      JWKS_URI = "https://dev.login.goremutual.ca/.well-known/jwks.json"
      TOKEN_ISSUER = "https://dev.login.goremutual.ca/"
    }
  }
}
#Lambda function for Nonprod Quoting
resource "aws_lambda_function" "gore_AGWAuthenrizer_Quoting_Hub" {
  function_name = "gore_AGWAuthenrizer_Quoting_Hub"
  role          = "arn:aws:iam::${var.gore-hub-account}:role/LambdaAssume_Hub"
  handler       = "index.handler"
  runtime       = "nodejs10.x"
  s3_bucket     = "gore-custom-auth"
  s3_key        = "custom-authorizer.zip"

  environment {
    variables = {
      AUDIENCE = "https://insurcloud/quoting"
      JWKS_URI = "https://dev.login.goremutual.ca/.well-known/jwks.json"
      TOKEN_ISSUER = "https://dev.login.goremutual.ca/"
    }
  }
}
#Lambda function for Nonprod Upload
resource "aws_lambda_function" "gore_AGWAuthenrizer_Upload_Hub" {
  function_name = "gore_AGWAuthenrizer_Upload_Hub"
  role          = "arn:aws:iam::${var.gore-hub-account}:role/LambdaAssume_Hub"
  handler       = "index.handler"
  runtime       = "nodejs10.x"
  s3_bucket     = "gore-custom-auth"
  s3_key        = "custom-authorizer.zip"

  environment {
    variables = {
      AUDIENCE = "https://insurcloud/upload"
      JWKS_URI = "https://dev.login.goremutual.ca/.well-known/jwks.json"
      TOKEN_ISSUER = "https://dev.login.goremutual.ca/"
    }
  }
}

#Lambda function for Nonprod Inquiry
resource "aws_lambda_function" "gore_AGWAuthenrizer_Inquiry_Hub" {
  function_name = "gore_AGWAuthenrizer_Inquiry_Hub"
  role          = "arn:aws:iam::${var.gore-hub-account}:role/LambdaAssume_Hub"
  handler       = "index.handler"
  runtime       = "nodejs10.x"
  s3_bucket     = "gore-custom-auth"
  s3_key        = "custom-authorizer.zip"

  environment {
    variables = {
      AUDIENCE = "https://insurcloud/inquiry"
      JWKS_URI = "https://dev.login.goremutual.ca/.well-known/jwks.json"
      TOKEN_ISSUER = "https://dev.login.goremutual.ca/"
    }
  }
}

#Lambda function for Nonprod Vehicle Information
resource "aws_lambda_function" "gore_AGWAuthenrizer_Vehicle_Hub" {
  function_name = "gore_AGWAuthenrizer_Vehicle_Hub"
  role          = "arn:aws:iam::${var.gore-hub-account}:role/LambdaAssume_Hub"
  handler       = "index.handler"
  runtime       = "nodejs10.x"
  s3_bucket     = "gore-custom-auth"
  s3_key        = "custom-authorizer.zip"

  environment {
    variables = {
      AUDIENCE = "https://insurcloud/ibc-vinlink"
      JWKS_URI = "https://dev.login.goremutual.ca/.well-known/jwks.json"
      TOKEN_ISSUER = "https://dev.login.goremutual.ca/"
    }
  }
}

#Lambda function for Nonprod Enterprise
resource "aws_lambda_function" "gore_AGWAuthenrizer_EHI_Hub" {
  function_name = "gore_AGWAuthenrizer_EHI_Hub"
  role          = "arn:aws:iam::${var.gore-hub-account}:role/LambdaAssume_Hub"
  handler       = "index.handler"
  runtime       = "nodejs10.x"
  s3_bucket     = "gore-custom-auth"
  s3_key        = "custom-authorizer.zip"

  environment {
    variables = {
      AUDIENCE = "https://gore.insurcloud-dev.ca/api/enterprise/v1"
      JWKS_URI = "https://kai-test.us.auth0.com/.well-known/jwks.json"
      TOKEN_ISSUER = "https://kai-test.us.auth0.com/"
    }
  }
}

#Lambda function for Nonprod Uniban
resource "aws_lambda_function" "gore_AGWAuthenrizer_Uniban_Hub" {
  function_name = "gore_AGWAuthenrizer_Uniban_Hub"
  role          = "arn:aws:iam::${var.gore-hub-account}:role/LambdaAssume_Hub"
  handler       = "index.handler"
  runtime       = "nodejs10.x"
  s3_bucket     = "gore-custom-auth"
  s3_key        = "custom-authorizer.zip"

  environment {
    variables = {
      AUDIENCE = "https://gore.insurcloud-dev.ca/api/uniban/v1"
      JWKS_URI = "https://kai-test.us.auth0.com/.well-known/jwks.json"
      TOKEN_ISSUER = "https://kai-test.us.auth0.com/"
    }
  }
}

#Lambda function for Nonprod CRC
resource "aws_lambda_function" "gore_AGWAuthenrizer_CRC_Hub" {
  function_name = "gore_AGWAuthenrizer_CRC_Hub"
  role          = "arn:aws:iam::${var.gore-hub-account}:role/LambdaAssume_Hub"
  handler       = "index.handler"
  runtime       = "nodejs10.x"
  s3_bucket     = "gore-custom-auth"
  s3_key        = "custom-authorizer.zip"

  environment {
    variables = {
      AUDIENCE = "https://crc/crc"
      JWKS_URI = "https://dev.login.goremutual.ca/.well-known/jwks.json"
      TOKEN_ISSUER = "https://dev.login.goremutual.ca/"
    }
  }
}

#Lambda function for Nonprod Audatex
resource "aws_lambda_function" "gore_AGWAuthenrizer_Audatex_Hub" {
  function_name = "gore_AGWAuthenrizer_Audatex_Hub"
  role          = "arn:aws:iam::${var.gore-hub-account}:role/LambdaAssume_Hub"
  handler       = "index.handler"
  runtime       = "nodejs10.x"
  s3_bucket     = "gore-custom-auth"
  s3_key        = "custom-authorizer.zip"

  environment {
    variables = {
      AUDIENCE = "https://guidewire/audatex-assignment"
      JWKS_URI = "https://dev.login.goremutual.ca/.well-known/jwks.json"
      TOKEN_ISSUER = "https://dev.login.goremutual.ca/"
    }
  }
}
#Lambda function for HCAI
resource "aws_lambda_function" "gore_AGWAuthenrizer_HCAI_Hub" {
  function_name = "gore_AGWAuthenrizer_HCAI_Hub"
  role          = "arn:aws:iam::${var.gore-hub-account}:role/LambdaAssume_Hub"
  handler       = "index.handler"
  runtime       = "nodejs10.x"
  s3_bucket     = "gore-custom-auth"
  s3_key        = "custom-authorizer.zip"

  environment {
    variables = {
      AUDIENCE = "https://guidewire/hcai"
      JWKS_URI = "https://dev.login.goremutual.ca/.well-known/jwks.json"
      TOKEN_ISSUER = "https://dev.login.goremutual.ca/"
    }
  }
}




# resource "aws_lambda_permission" "apigw_lambda_authenrizer_prod" {
#   statement_id  = "AllowExecutionFromAPIGateway"
#   action        = "lambda:InvokeFunction"
#   function_name = "${aws_lambda_function.peelprod-APIGatewayAuthenrizer_Hub.function_name}"
#   principal     = "apigateway.amazonaws.com"
#   # More: http://docs.aws.amazon.com/apigateway/latest/developerguide/api-gateway-control-access-using-iam-policies-to-invoke-api.html
#   source_arn = "arn:aws:execute-api:${var.region}:${var.peel-hub-account}:${aws_api_gateway_rest_api.GLCheque.id}/*"
# }

# resource "aws_lambda_function" "peelprod-APIGatewayAuthenrizer_Hub" {
#   function_name = "peelprod-APIGatewayAuthenrizer_Hub"
#   role          = "arn:aws:iam::${var.peel-hub-account}:role/LambdaAssume_Hub"
#   handler       = "index.handler"
#   runtime       = "nodejs10.x"
#   s3_bucket     = "peel-custom-auth"
#   s3_key        = "custom-authorizer.zip"

#   environment {
#     variables = {
#       AUDIENCE = "https://peel.insurcloud.ca/ms/api/v2"
#       JWKS_URI = "https://insurcloud.auth0.com/.well-known/jwks.json"
#       TOKEN_ISSUER = "https://insurcloud.auth0.com/"
#     }
#   }
# }

