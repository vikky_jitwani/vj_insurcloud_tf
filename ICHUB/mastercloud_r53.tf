#=======================================================================
# HUB 
#=======================================================================
resource "aws_route53_record" "JENKINS_MASTER_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "jenkins.insurcloud.ca"
    type    = "A"

    alias {
        name                   = "${aws_lb.INTERNAL_ALB_CICD.dns_name}"
        zone_id                = "${aws_lb.INTERNAL_ALB_CICD.zone_id}"
        evaluate_target_health = false
    }
}

resource "aws_route53_record" "ICCW_JENKINS_MASTER_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "jenkins.cmig.insurcloud.ca"
    type    = "A"

    alias {
        name                   = "${aws_lb.INTERNAL_ALB_CICD.dns_name}"
        zone_id                = "${aws_lb.INTERNAL_ALB_CICD.zone_id}"
        evaluate_target_health = false
    }
}

resource "aws_route53_record" "ICPEEL_JENKINS_MASTER_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "jenkins.peel.insurcloud.ca"
    type    = "A"

    alias {
        name                   = "${aws_lb.INTERNAL_ALB_CICD.dns_name}"
        zone_id                = "${aws_lb.INTERNAL_ALB_CICD.zone_id}"
        evaluate_target_health = false
    }
}

resource "aws_route53_record" "PEEL_GW_PPS_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "gw-pps.nonprod.peel.insurcloud.ca"
    type    = "A"

    alias {
        name                   = "ICPEEL-PPS-INTERNAL-NLB-12d6227c88fc6226.elb.ca-central-1.amazonaws.com"
        zone_id                = "Z2EPGBW3API2WT"
        evaluate_target_health = false
    }
}

resource "aws_route53_record" "PEEL_GW_DEV01_NONPROD_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "gw-dev01.nonprod.peel.insurcloud.ca"
    type    = "A"

    alias {
        name                   = "ICPEEL-DEV01-INTERNAL-NLB-55857cdca8f80da0.elb.ca-central-1.amazonaws.com"
        zone_id                = "Z2EPGBW3API2WT"
        evaluate_target_health = false
    }
}

resource "aws_route53_record" "PEEL_GW_DEV01_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "gw.dev01.peel.insurcloud.ca"
    type    = "A"

    alias {
        name                   = "ICPEEL-DEV01-INTERNAL-NLB-55857cdca8f80da0.elb.ca-central-1.amazonaws.com"
        zone_id                = "Z2EPGBW3API2WT"
        evaluate_target_health = false
    }
}

resource "aws_route53_record" "CMIG_GW_PPS_NONPROD_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "gw-pps.nonprod.cmig.insurcloud.ca"
    type    = "A"

    alias {
        name                   = "CW-INTERNAL-NLB-f8a01be1316736c2.elb.ca-central-1.amazonaws.com"
        zone_id                = "Z2EPGBW3API2WT"
        evaluate_target_health = false
    }
}

resource "aws_route53_record" "CMIG_GW_PPS_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "gw.pps.cmig.insurcloud.ca"
    type    = "A"

    alias {
        name                   = "CW-INTERNAL-NLB-f8a01be1316736c2.elb.ca-central-1.amazonaws.com"
        zone_id                = "Z2EPGBW3API2WT"
        evaluate_target_health = false
    }
}

resource "aws_route53_record" "GW_DEV01_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "gw.dev01.cmig.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${var.cmig_dev01_haproxy_ip}"]
}


resource "aws_route53_record" "ICECO_JENKINS_MASTER_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "jenkins.economical.insurcloud.ca"
    type    = "A"

    alias {
        name                   = "${aws_lb.INTERNAL_ALB_CICD.dns_name}"
        zone_id                = "${aws_lb.INTERNAL_ALB_CICD.zone_id}"
        evaluate_target_health = false
    }
}

resource "aws_route53_record" "ICGORE_JENKINS_MASTER_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "jenkins.gore.insurcloud.ca"
    type    = "A"

    alias {
        name                   = "${aws_lb.INTERNAL_ALB_CICD.dns_name}"
        zone_id                = "${aws_lb.INTERNAL_ALB_CICD.zone_id}"
        evaluate_target_health = false
    }
}

resource "aws_route53_record" "ICPORTAGE_JENKINS_MASTER_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "jenkins.portage.insurcloud.ca"
    type    = "A"

    alias {
        name                   = "${aws_lb.INTERNAL_ALB_CICD.dns_name}"
        zone_id                = "${aws_lb.INTERNAL_ALB_CICD.zone_id}"
        evaluate_target_health = false
    }
}

resource "aws_route53_record" "ICBCAA_JENKINS_MASTER_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "jenkins.bcaa.insurcloud.ca"
    type    = "A"

    alias {
        name                   = "${aws_lb.INTERNAL_ALB_CICD.dns_name}"
        zone_id                = "${aws_lb.INTERNAL_ALB_CICD.zone_id}"
        evaluate_target_health = false
    }
}

resource "aws_route53_record" "NEXUS_CMIG_REG_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "nexus.cmig.insurcloud.ca"
    type    = "A"

    alias {
        name                   = "${aws_lb.INTERNAL_ALB_CICD.dns_name}"
        zone_id                = "${aws_lb.INTERNAL_ALB_CICD.zone_id}"
        evaluate_target_health = false
    }
}

resource "aws_route53_record" "NEXUS_CMIG_PROD_REG_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "nexus-prod.cmig.insurcloud.ca"
    type    = "A"

    alias {
        name                   = "${aws_lb.INTERNAL_ALB_CICD.dns_name}"
        zone_id                = "${aws_lb.INTERNAL_ALB_CICD.zone_id}"
        evaluate_target_health = false
    }
}

resource "aws_route53_record" "NEXUS_PEEL_REG_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "nexus.peel.insurcloud.ca"
    type    = "A"

    alias {
        name                   = "${aws_lb.INTERNAL_ALB_CICD.dns_name}"
        zone_id                = "${aws_lb.INTERNAL_ALB_CICD.zone_id}"
        evaluate_target_health = false
    }
}

resource "aws_route53_record" "NEXUS_PEEL_PROD_REG_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "nexus-prod.peel.insurcloud.ca"
    type    = "A"

    alias {
        name                   = "${aws_lb.INTERNAL_ALB_CICD.dns_name}"
        zone_id                = "${aws_lb.INTERNAL_ALB_CICD.zone_id}"
        evaluate_target_health = false
    }
}

resource "aws_route53_record" "NEXUS_ECO_REG_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "nexus.economical.insurcloud.ca"
    type    = "A"

    alias {
        name                   = "${aws_lb.INTERNAL_ALB_CICD.dns_name}"
        zone_id                = "${aws_lb.INTERNAL_ALB_CICD.zone_id}"
        evaluate_target_health = false
    }
}

resource "aws_route53_record" "NEXUS_ECO_PROD_REG_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "nexus-prod.economical.insurcloud.ca"
    type    = "A"

    alias {
        name                   = "${aws_lb.INTERNAL_ALB_CICD.dns_name}"
        zone_id                = "${aws_lb.INTERNAL_ALB_CICD.zone_id}"
        evaluate_target_health = false
    }
}

resource "aws_route53_record" "NEXUS_GORE_REG_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "nexus.gore.insurcloud.ca"
    type    = "A"

    alias {
        name                   = "${aws_lb.INTERNAL_ALB_CICD.dns_name}"
        zone_id                = "${aws_lb.INTERNAL_ALB_CICD.zone_id}"
        evaluate_target_health = false
    }
}

resource "aws_route53_record" "NEXUS_PORTAGE_REG_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "nexus.portage.insurcloud.ca"
    type    = "A"

    alias {
        name                   = "${aws_lb.INTERNAL_ALB_CICD.dns_name}"
        zone_id                = "${aws_lb.INTERNAL_ALB_CICD.zone_id}"
        evaluate_target_health = false
    }
}

resource "aws_route53_record" "NEXUS_BCAA_REG_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "nexus.bcaa.insurcloud.ca"
    type    = "A"

    alias {
        name                   = "${aws_lb.INTERNAL_ALB_CICD.dns_name}"
        zone_id                = "${aws_lb.INTERNAL_ALB_CICD.zone_id}"
        evaluate_target_health = false
    }
}

resource "aws_route53_record" "NEXUS_BASE_REG_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "nexus.insurcloud.ca"
    type    = "A"

    alias {
        name                   = "${aws_lb.INTERNAL_ALB_CICD.dns_name}"
        zone_id                = "${aws_lb.INTERNAL_ALB_CICD.zone_id}"
        evaluate_target_health = false
    }
}

resource "aws_route53_record" "NEXUS_BASE_PROD_REG_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "nexus-prod.insurcloud.ca"
    type    = "A"

    alias {
        name                   = "${aws_lb.INTERNAL_ALB_CICD.dns_name}"
        zone_id                = "${aws_lb.INTERNAL_ALB_CICD.zone_id}"
        evaluate_target_health = false
    }
}

resource "aws_route53_record" "NEXUS_GORE_PROD_REG_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "nexus-prod.gore.insurcloud.ca"
    type    = "A"

    alias {
        name                   = "${aws_lb.INTERNAL_ALB_CICD.dns_name}"
        zone_id                = "${aws_lb.INTERNAL_ALB_CICD.zone_id}"
        evaluate_target_health = false
    }
}

resource "aws_route53_record" "NEXUS_PORTAGE_PROD_REG_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "nexus-prod.portage.insurcloud.ca"
    type    = "A"

    alias {
        name                   = "${aws_lb.INTERNAL_ALB_CICD.dns_name}"
        zone_id                = "${aws_lb.INTERNAL_ALB_CICD.zone_id}"
        evaluate_target_health = false
    }
}

resource "aws_route53_record" "NEXUS_BCAA_PROD_REG_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "nexus-prod.bcaa.insurcloud.ca"
    type    = "A"

    alias {
        name                   = "${aws_lb.INTERNAL_ALB_CICD.dns_name}"
        zone_id                = "${aws_lb.INTERNAL_ALB_CICD.zone_id}"
        evaluate_target_health = false
    }
}

resource "aws_route53_record" "NEXUS_UI_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "nexus-ui.insurcloud.ca"
    type    = "A"

    alias {
        name                   = "${aws_lb.INTERNAL_ALB_CICD.dns_name}"
        zone_id                = "${aws_lb.INTERNAL_ALB_CICD.zone_id}"
        evaluate_target_health = false
    }
}

resource "aws_route53_record" "ICCW_NEXUS_UI_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "nexus-ui.cmig.insurcloud.ca"
    type    = "A"

    alias {
        name                   = "${aws_lb.INTERNAL_ALB_CICD.dns_name}"
        zone_id                = "${aws_lb.INTERNAL_ALB_CICD.zone_id}"
        evaluate_target_health = false
    }
}

resource "aws_route53_record" "ICPEEL_NEXUS_UI_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "nexus-ui.peel.insurcloud.ca"
    type    = "A"

    alias {
        name                   = "${aws_lb.INTERNAL_ALB_CICD.dns_name}"
        zone_id                = "${aws_lb.INTERNAL_ALB_CICD.zone_id}"
        evaluate_target_health = false
    }
}

resource "aws_route53_record" "ICECO_NEXUS_UI_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "nexus-ui.economical.insurcloud.ca"
    type    = "A"

    alias {
        name                   = "${aws_lb.INTERNAL_ALB_CICD.dns_name}"
        zone_id                = "${aws_lb.INTERNAL_ALB_CICD.zone_id}"
        evaluate_target_health = false
    }
}

resource "aws_route53_record" "ICGORE_NEXUS_UI_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "nexus-ui.gore.insurcloud.ca"
    type    = "A"

    alias {
        name                   = "${aws_lb.INTERNAL_ALB_CICD.dns_name}"
        zone_id                = "${aws_lb.INTERNAL_ALB_CICD.zone_id}"
        evaluate_target_health = false
    }
}

resource "aws_route53_record" "ICPORTAGE_NEXUS_UI_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "nexus-ui.portage.insurcloud.ca"
    type    = "A"

    alias {
        name                   = "${aws_lb.INTERNAL_ALB_CICD.dns_name}"
        zone_id                = "${aws_lb.INTERNAL_ALB_CICD.zone_id}"
        evaluate_target_health = false
    }
}

resource "aws_route53_record" "ICBCAA_NEXUS_UI_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "nexus-ui.bcaa.insurcloud.ca"
    type    = "A"

    alias {
        name                   = "${aws_lb.INTERNAL_ALB_CICD.dns_name}"
        zone_id                = "${aws_lb.INTERNAL_ALB_CICD.zone_id}"
        evaluate_target_health = false
    }
}

resource "aws_route53_record" "SONARQUBE_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "sonarqube.insurcloud.ca"
    type    = "A"

    alias {
        name                   = "${aws_lb.INTERNAL_ALB_CICD.dns_name}"
        zone_id                = "${aws_lb.INTERNAL_ALB_CICD.zone_id}"
        evaluate_target_health = false
    }
}

resource "aws_route53_record" "ICCW_SONARQUBE_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "sonarqube.cmig.insurcloud.ca"
    type    = "A"

    alias {
        name                   = "${aws_lb.INTERNAL_ALB_CICD.dns_name}"
        zone_id                = "${aws_lb.INTERNAL_ALB_CICD.zone_id}"
        evaluate_target_health = false
    }
}

resource "aws_route53_record" "ICPEEL_SONARQUBE_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "sonarqube.peel.insurcloud.ca"
    type    = "A"

    alias {
        name                   = "${aws_lb.INTERNAL_ALB_CICD.dns_name}"
        zone_id                = "${aws_lb.INTERNAL_ALB_CICD.zone_id}"
        evaluate_target_health = false
    }
}

resource "aws_route53_record" "ICECO_SONARQUBE_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "sonarqube.economical.insurcloud.ca"
    type    = "A"

    alias {
        name                   = "${aws_lb.INTERNAL_ALB_CICD.dns_name}"
        zone_id                = "${aws_lb.INTERNAL_ALB_CICD.zone_id}"
        evaluate_target_health = false
    }
}

resource "aws_route53_record" "ICGORE_SONARQUBE_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "sonarqube.gore.insurcloud.ca"
    type    = "A"

    alias {
        name                   = "${aws_lb.INTERNAL_ALB_CICD.dns_name}"
        zone_id                = "${aws_lb.INTERNAL_ALB_CICD.zone_id}"
        evaluate_target_health = false
    }
}

resource "aws_route53_record" "ICPORTAGE_SONARQUBE_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "sonarqube.portage.insurcloud.ca"
    type    = "A"

    alias {
        name                   = "${aws_lb.INTERNAL_ALB_CICD.dns_name}"
        zone_id                = "${aws_lb.INTERNAL_ALB_CICD.zone_id}"
        evaluate_target_health = false
    }
}

resource "aws_route53_record" "ICBCAA_SONARQUBE_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "sonarqube.bcaa.insurcloud.ca"
    type    = "A"

    alias {
        name                   = "${aws_lb.INTERNAL_ALB_CICD.dns_name}"
        zone_id                = "${aws_lb.INTERNAL_ALB_CICD.zone_id}"
        evaluate_target_health = false
    }
}

resource "aws_route53_record" "CONSUL_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "consul.insurcloud.ca"
    type    = "A"

    alias {
        name                   = "${aws_lb.INTERNAL_ALB_CICD.dns_name}"
        zone_id                = "${aws_lb.INTERNAL_ALB_CICD.zone_id}"
        evaluate_target_health = false
    }
}

resource "aws_route53_record" "ICCW_CONSUL_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "consul.cmig.insurcloud.ca"
    type    = "A"

    alias {
        name                   = "${aws_lb.INTERNAL_ALB_CICD.dns_name}"
        zone_id                = "${aws_lb.INTERNAL_ALB_CICD.zone_id}"
        evaluate_target_health = false
    }
}

resource "aws_route53_record" "ICPEEL_CONSUL_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "consul.peel.insurcloud.ca"
    type    = "A"

    alias {
        name                   = "${aws_lb.INTERNAL_ALB_CICD.dns_name}"
        zone_id                = "${aws_lb.INTERNAL_ALB_CICD.zone_id}"
        evaluate_target_health = false
    }
}

resource "aws_route53_record" "ICECO_CONSUL_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "consul.economical.insurcloud.ca"
    type    = "A"

    alias {
        name                   = "${aws_lb.INTERNAL_ALB_CICD.dns_name}"
        zone_id                = "${aws_lb.INTERNAL_ALB_CICD.zone_id}"
        evaluate_target_health = false
    }
}
    
resource "aws_route53_record" "ICGORE_CONSUL_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "consul.gore.insurcloud.ca"
    type    = "A"

    alias {
        name                   = "${aws_lb.INTERNAL_ALB_CICD.dns_name}"
        zone_id                = "${aws_lb.INTERNAL_ALB_CICD.zone_id}"
        evaluate_target_health = false
    }
}

resource "aws_route53_record" "ICPORTAGE_CONSUL_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "consul.portage.insurcloud.ca"
    type    = "A"

    alias {
        name                   = "${aws_lb.INTERNAL_ALB_CICD.dns_name}"
        zone_id                = "${aws_lb.INTERNAL_ALB_CICD.zone_id}"
        evaluate_target_health = false
    }
}

resource "aws_route53_record" "ICBCAA_CONSUL_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "consul.bcaa.insurcloud.ca"
    type    = "A"

    alias {
        name                   = "${aws_lb.INTERNAL_ALB_CICD.dns_name}"
        zone_id                = "${aws_lb.INTERNAL_ALB_CICD.zone_id}"
        evaluate_target_health = false
    }
}