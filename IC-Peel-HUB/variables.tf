variable "client_name" {
  description = "AWS client name for resources (Ex: MASTERCLOUD)"
  default = ""
}

variable "environment_name" {
  description = "AWS environment name for resources (Ex: NON-PROD)"
  default = ""
}

variable "environment_type" {
  description = "AWS environment type for resources (Ex: P=PROD/HUB, D=NON-PROD)"
  default = ""
}

variable "client_number" {
  description = "AWS client number for tagging (Ex: A001)"
  default = "A003"
}

variable "region" {
  description = "AWS region for hosting our your network"
  default = "ca-central-1"
}

variable "volume_key" {
  description = "KMS volume key hub account"
  default = ""
}

variable "ami_id_amazonlinux201709base" {
  description = "Amazon Linux Encypted Root Volume AMI"
  default = ""
}

variable "ami_id_redhatlinux75" {
  description = "Redhat Linux Encypted Root Volume AMI"
  default = ""
}

variable "ami_id_amazonlinux201803base" {
  description = "Amazon Linux 2018 03 Encypted Root Volume AMI"
  default = ""
}

variable "ami_id_amazonlinux2base" {
  description = "Amazon Linux 2 Base Encypted Root Volume AMI"
  default = ""
}

variable "ami_id_windows2008base" {
  description = "Windows 2008 Base Encypted Root Volume AMI"
  default = ""
}

variable "ami_id_amazonlinux201712base" {
  description = "Amazon Linux 2017 12 Base Encypted Root Volume AMI"
  default = ""
}

variable "ami_id_windows2016base" {
  description = "Windows 2016 Base Encypted Root Volume AMI"
  default = ""
}

variable "ami_id_windows2012base" {
  description = "Windows 2012 Base Encypted Root Volume AMI"
  default = ""
}

variable "ami_id_windows2012R2base" {
  description = "Windows 2012 R2 Base Encypted Root Volume AMI"
  default = ""
}

variable "ami_id_ubuntu1604baseimage" {
  description = "Ubuntu 16 04 Base Encypted Root Volume AMI"
  default = ""
}

variable "ami_id_ubuntu1804baseimage" {
  description = "Ubuntu 18 04 Base Encypted Root Volume AMI"
  default = ""
}

variable "ami_id_rhel75baseimage" {
  description = "RHEL 7.5 Base image from the cloud"
  default = ""
}


variable "vpc_id" {
  description = "VPC id for HUB account"
  default = "vpc-01c39132b104e1b86"
}

variable "vpc_cidr" {
  description = "VPC cidr dev account"
  default = ""
}

variable "conduit-rds-password" {
  description = "Conduit DB Password"
  default = ""
}

variable "peel-hub-database-1a-subnet" {
  description = "Database subnet one"
  default = ""
}

variable "peel-hub-database-1b-subnet" {
  description = "Database subnet two"
  default = ""
}

variable "peel-hub-private-1a-subnet" {
  description = "Private Subnet One"
  default = ""
}

variable "peel-hub-private-1b-subnet" {
  description = "Private Subnet Two"
  default = ""
}

variable "peel-hub-public-1a-subnet" {
  description = "Public Subnet One"
  default = ""
}

variable "peel-hub-public-1b-subnet" {
  description = "Public Subnet two"
  default = ""
}

variable "default_cidr" {
  description = "default cidr used"
  default = ["10.0.0.0/8"]
}

variable "deloitte_cidr" {
    default = "10.10.4.0/22"
}

variable "peel_cidr" {
    default = "10.10.10.0/23"
}

variable "peel_watchguard_cidr" {
    default = "192.168.113.0/24"
}

variable "peel_dmz_cidr" {
    default = "192.168.100.0/24"
}

variable "peel_wireless_cidr" {
    default = "192.168.101.0/24"
}

variable "zone_id" {
  description = "default zone id"
  default = ""
}

variable "peel_dev_cidr" {
  description = "default cidr used"
  type = "list"
  default = ["10.11.0.0/22"]
}

variable "peel_prod_cidr" {
  description = "default cidr used"
  type = "list"
  default = ["10.12.0.0/22"]
}

variable "peel_hub_cidr" {
  description = "default cidr used"
  type = "list"
  default = ["10.13.0.0/22"]
}

variable "ichub_cidr" {
  description = " cidr for ichub"
  type = "list"
  default = ["10.15.16.0/22"]
}

variable "peel-hub_private_cidr" {
  description = "default cidr used"
  type = "list"
  default = ["10.13.0.0/24", "10.13.1.0/24"]
}
variable "peel_aviatrix_vpn" {
    default = "10.13.2.96/32"
}

variable "ad_dns1" {
    default = "10.13.0.78"
}
variable "ad_dns2" {
    default = "10.13.1.136"
}
variable "ad_dns3" {
    default = "10.13.0.2"
}
variable "ou_name" {
    default = "hub"
}
variable "pwordparameter" {
    default = "domain_password"
}
variable "unameparameter" {
    default = "domain_username"
}
variable "ad_domain" {
    default = "internal.peel.insurcloud.ca"
}
data "aws_caller_identity" "current" {}

variable "peel-dev-account" {
  default = "005810662120"
}

variable "peel-prod-account" {
  default = "413329031558"
}

variable "peel-hub-account" {
  default = "186239701968"
}
