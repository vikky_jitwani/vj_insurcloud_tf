#!/bin/sh
sudo yum update -y
sudo yum upgrade -y
sudo yum install git -y
sudo amazon-linux-extras install docker
sudo yum install docker -y
sudo service docker start
sudo usermod -a -G docker ec2-user