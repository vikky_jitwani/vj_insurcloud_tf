##### ALBs #####
########## Internal ###########

##### NLBs ####################
########## External ###########
resource "aws_lb" "CW_PROD_EXTERNAL_NLB" {
    name               = "cmig-insurcloud"
    internal           = false
    load_balancer_type = "network"
    enable_cross_zone_load_balancing = false
    subnets            = [
        "${var.commonwell-hub-public-1a-subnet}",
        "${var.commonwell-hub-public-1b-subnet}"
    ] # Changing this value will force a recreation of the resource

    enable_deletion_protection = true

    # access_logs {
    #     bucket  = "${aws_s3_bucket.ICCW_PROD_EXTERNAL_NLB.bucket}"
    #     enabled = true
    # }

    tags = {
        Name = "CW-PROD-EXTERNAL-NLB"
    }
}

resource "aws_lb_target_group" "CW_PROD_EXTERNAL_NLB_TARGET_GROUP" {
    name        = "CW-PROD-EXTERNAL-NLB"
    port        = 443
    protocol    = "TCP"
    target_type = "ip"
    vpc_id      = "${var.vpc_id}"

    health_check {
        protocol = "TCP"
        port = 8444
    }

    tags = {
        Name = "CW-PROD-EXTERNAL-NLB-TARGET-GROUP"
    }
}

resource "aws_lb_listener" "CW_PROD_EXTERNAL_NLB_LISTENER" {
    load_balancer_arn = "${aws_lb.CW_PROD_EXTERNAL_NLB.arn}"
    port              = "443"
    protocol          = "TCP"

    default_action {
        type             = "forward"
        target_group_arn = "${aws_lb_target_group.CW_PROD_EXTERNAL_NLB_TARGET_GROUP.arn}"
    }
}

resource "aws_lb" "CW_PPS_EXTERNAL_NLB" {
    name               = "cmig-insurcloud-pps"
    internal           = false
    load_balancer_type = "network"
    enable_cross_zone_load_balancing = false
    subnets            = [
        "${var.commonwell-hub-public-1a-subnet}",
        "${var.commonwell-hub-public-1b-subnet}"
    ] # Changing this value will force a recreation of the resource
    
    enable_deletion_protection = true

    # access_logs {
    #     bucket  = "${aws_s3_bucket.ICCW_PPS_EXTERNAL_NLB.bucket}"
    #     enabled = true
    # }

    tags = {
        Name = "CW-PPS-EXTERNAL-NLB"
    }
}

resource "aws_lb_target_group" "CW_PPS_EXTERNAL_NLB_TARGET_GROUP" {
    name        = "CW-PPS-EXTERNAL-NLB"
    port        = 443
    protocol    = "TCP"
    target_type = "ip"
    vpc_id      = "${var.vpc_id}"

    health_check {
        protocol = "TCP"
        port = 8444
    }

    tags = {
        Name = "CW-PPS-EXTERNAL-NLB-TARGET-GROUP"
    }
}

resource "aws_lb_listener" "CW_PPS_EXTERNAL_NLB_LISTENER" {
    load_balancer_arn = "${aws_lb.CW_PPS_EXTERNAL_NLB.arn}"
    port              = "443"
    protocol          = "TCP"

    default_action {
        type             = "forward"
        target_group_arn = "${aws_lb_target_group.CW_PPS_EXTERNAL_NLB_TARGET_GROUP.arn}"
    }
}
