#Envrionment specific variables
client_name = "IC-GORE"
enviornment_name = "PROD"
enviornment_type = "P"
client_number = "A004"
#ad-dc1 = "10.10.4.10"
#ad-dc2 = "10.10.6.10"
volume_key = "arn:aws:kms:ca-central-1:948423377884:key/7e7c51b7-cce0-480e-8491-b667d796f780"
ami_id_windows2012R2sql14enterprise = "ami-0b7e5050eee245b99"
ami_id_windows2012R2sql14standard = "ami-0913a2afb4fa5fc5b"
ami_id_windows2019container = "ami-013453540e1aa0d48"
ami_id_windows2019base = "ami-06b8f69a907cf6bb2"
ami_id_windows2016base = "ami-0f279f997ff6cb1f5"
ami_id_windows2012R2base = "ami-08f7dedaab95e4621"
ami_id_windows2008base = "ami-0446e08d2d99fc8c9"
ami_id_redhatlinux75 = "ami-05c33d286020a47f9"
ami_id_ubuntu1804baseimage = "ami-032172cc5f34b32fc"
ami_id_ubuntu1604baseimage = "ami-04f40cca01b3186ea"
ami_id_amazonlinux201712base = "ami-0a777bbf8ef3f43bf"
ami_id_amazonlinux201709base = "ami-058dba934995c5468"
ami_id_amazonlinux2base = "ami-04978aa25eaba28b7"
ami_id_amazonlinux201803base = "ami-06829694f407e15c1"
ami_id_amazonlinuxbase20200506 = "ami-071594d49d11fcb38"

#VPC Specific variables
vpc_id = "vpc-0e395cfc38bb7090c"
vpc_cidr = "192.168.24.0/22"


GORE-prod-database-1a-subnet = "subnet-077c7c0e60be4583d"
GORE-prod-database-1b-subnet = "subnet-0c4d568dfd754a826"
GORE-prod-private-1a-subnet = "subnet-077c7c0e60be4583d"
GORE-prod-private-1b-subnet = "subnet-0211002fbe970842a"


#RDS
mssql-rds-password = "Toronto12345"
mysql-rds-password = "Toronto12345"
postgres-rds-password = "Toronto12345"

#Accounts
GORE-dev-account = "780193124257"
GORE-prod-account = "948423377884"
GORE-hub-account = "963473599983"
