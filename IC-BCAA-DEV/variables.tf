variable "client_name" {
  description = "AWS client name for resources (Ex: MASTERCLOUD)"
  default = ""
}

variable "environment_name" {
  description = "AWS environment name for resources (Ex: NON-PROD)"
  default = ""
}

variable "environment_type" {
  description = "AWS environment type for resources (Ex: P=PROD/HUB, D=NON-PROD)"
  default = ""
}

variable "client_number" {
  description = "AWS client number for tagging (Ex: A001)"
  default = "A010"
}

variable "region" {
  description = "AWS region for hosting our your network"
  default = "ca-central-1"
}

variable "volume_key" {
  description = "KMS volume key hub account"
  default = ""
}

variable "ami_id_amazonlinux201709base" {
  description = "Amazon Linux Encypted Root Volume AMI"
  default = ""
}

variable "ami_id_redhatlinux75" {
  description = "Redhat Linux Encypted Root Volume AMI"
  default = ""
}

variable "ami_id_amazonlinux2base_2020_12_11" {
  description = "Amazon Linux Machine"
  default = ""
}

variable "ami_id_windows2016base_2020_12_11" {
  description = "Windows machine used for BCAA DHIC server"
  default = ""
}

variable "ami_id_amazonlinux201803base" {
  description = "Amazon Linux 2018 03 Encypted Root Volume AMI"
  default = ""
}

variable "ami_id_amazonlinux2base" {
  description = "Amazon Linux 2 Base Encypted Root Volume AMI"
  default = ""
}

variable "ami_id_windows2008base" {
  description = "Windows 2008 Base Encypted Root Volume AMI"
  default = ""
}

variable "ami_id_amazonlinux201712base" {
  description = "Amazon Linux 2017 12 Base Encypted Root Volume AMI"
  default = ""
}

variable "ami_id_windows2012base" {
  description = "Windows 2012 Base Encypted Root Volume AMI with SQL 2014 Express"
  default = ""
}


variable "ami_id_windows2012R2base" {
  description = "Windows 2012 R2 Base Encypted Root Volume AMI"
  default = ""
}


variable "ami_id_windows2012SQLEnterprisebase" {
  description = "Windows 2012 RTM Base Encypted Root Volume AMI with SQL 2014 Enterprise"
  default = ""
}

variable "ami_id_windows2012R2SQLExpressbase" {
  description = "Windows 2012 R2 RTM Base Encypted Root Volume AMI with SQL 2014 Express"
  default = ""
}

variable "ami_id_windows2016base" {
  description = "Windows 2016 Base Encypted Root Volume AMI"
  default = ""
}

variable "ami_id_windows2019base" {
  description = "Windows 2019 Base Encypted Root Volume AMI"
  default = ""
}

variable "ami_id_windows2019container" {
  description = "Windows 2019 Base with containers Encypted Root Volume AMI"
  default = ""
}

variable "ami_id_ubuntu1604baseimage" {
  description = "Ubuntu 16 04 Base Encypted Root Volume AMI"
  default = ""
}

variable "ami_id_ubuntu1804baseimage" {
  description = "Ubuntu 18 04 Base Encypted Root Volume AMI"
  default = ""
}

variable "ami_id_windows_dev_infocenter_datahub_server" {
  description = "InfoCenter & Datahub Base Image for Dev. Based on ami_id_windows2016base"
  default = ""
}

variable "ami_id_" {
  description = "Ubuntu 18 04 Base Encypted Root Volume AMI"
  default = ""
}
variable "vpc_id" {
  description = "VPC id for DEV account"
  default = ""
}

variable "vpc_cidr" {
  description = "VPC cidr dev account"
  default = ""
}

variable "bcaa-dev-database-1a-subnet" {
  description = "Database subnet one"
  default = ""
}

variable "bcaa-dev-database-1b-subnet" {
  description = "Database subnet two"
  default = ""
}

variable "bcaa-dev-private-1a-subnet" {
  description = "Private Subnet One"
  default = ""
}

variable "bcaa-dev-private-1b-subnet" {
  description = "Private Subnet Two"
  default = ""
}

variable "bcaa_workspaces" {
  description = "BCAA AWS Workspaces"
  type        = "list"

  default = [
    "10.41.0.0/16"
  ]
}

variable "default_cidr" {
  description = "default cidr used"
  default = ["10.0.0.0/8"]
}

variable "bcaa_cidr" {
    default = "10.41.0.0/16"
}

variable "bcaa_watchguard_cidr" {
    default = "192.168.113.0/24"
}

variable "bcaa_dmz_cidr" {
    default = "192.168.100.0/24"
}

variable "bcaa_wireless_cidr" {
    default = "192.168.101.0/24"
}

variable "mssql-rds-password" {
    default = "Canada@123"
}
variable "mysql-rds-password" {
    default = "Canada@123"
}

variable "postgres-rds-password" {
    default = "Canada@123"
}

variable "zone_id" {
  description = "default zone id"
  default = ""
}
variable "bcaa_dev_cidr" {
 description = "default cidr used"
 type = "list"
 default = ["10.41.8.0/22"]
}
 
variable "bcaa_prod_cidr" {
 description = "default cidr used"
 type = "list"
 default = ["10.41.4.0/22"]
}
 
variable "bcaa_hub_cidr" {
 description = "default cidr used"
 type = "list"
 default = ["10.41.0.0/22"]
}

variable "bcaa-hub_private_cidr" {
  description = "default cidr used"
  type = "list"
  default = ["10.41.0.0/24", "10.41.1.0/24"]
}

variable "bcaa_aviatrix_vpn" {
    default = "10.41.2.108/32"
}

variable "bcaa_dhic_sap" {
    default = "10.41.8.62/32"
}

variable "bcaa_dhic_db" {
    default = "10.41.9.214/32"
}

variable "bcaa_dev01_microservices_cidr" {
    default = "10.41.8.192/32"
}

variable "ad_dns1" {
    default = "10.41.0.91"
}
variable "ad_dns2" {
    default = "10.41.1.111"
}
variable "ad_dns3" {
    default = "10.41.0.2"
}
variable "ou_name" {
    default = "dev"
}
variable "pwordparameter" {
    default = "domain_password"
}
variable "unameparameter" {
    default = "domain_username"
}
variable "ad_domain" {
    default = "internal.bcaa.insurcloud.ca"
}
data "aws_caller_identity" "current" {}

variable "bcaa-dev-account" {
  default = "775629271970"
}

variable "bcaa-prod-account" {
  default = "338020371242"
}

variable "bcaa-hub-account" {
  default = "057006516829"
}

variable "bcaa_dev01_dhic_sap" {
  default = "10.41.8.62/32"
}

variable "bcaa_jenkins_client" {
  default = "10.41.8.32/32"
}

variable "bcaa_gateways_cidr" {
  description = "BCAA VPN gateways"
  type = "list"
  default = [
    "192.197.50.196/32",
    "44.228.230.226/32",
    "52.38.177.188/32"
  ]
}
