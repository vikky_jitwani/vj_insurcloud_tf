#### GUIDEWIRE SG #####
resource "aws_security_group" "SG_GUIDEWIRE" {
  name = "GUIDEWIRE-SG"

  ingress {
    from_port = 8080
    to_port = 8083
    protocol = "tcp"
    security_groups = [
      "${aws_security_group.SG_INTERNAL_HAPROXY.id}",
      "${aws_security_group.SG_INTERNAL_HAPROXY_DEV.id}"
    ]

    description = "HTTP - Internal"
  }

  ingress {
    from_port = 8080
    to_port = 8083
    protocol = "tcp"
    security_groups = [
      "${aws_security_group.SG_EXTERNAL_HAPROXY.id}"
    ]

    description = "HTTP - External"
  }

  ingress {
    from_port = 8080
    to_port = 8083
    protocol = "tcp"
    cidr_blocks = [
      "${var.economical_aviatrix_vpn}"
    ]

    description = "HTTP - Internal No Proxy"
  }

  ingress {
    from_port = 8080
    to_port = 8083
    protocol = "tcp"
    cidr_blocks = "${var.economical_corp_cidrs}"
    description = "HTTP - Economical Corp Internal No Proxy"
  }

  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = [
      "0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "MANAGED_SERVICES"
    Name = "GUIDEWIRE-SG"
  }
}

resource "aws_security_group" "SG_GUIDEWIRE_EFS" {
  name = "SG-GUIDEWIRE-EFS"

  ingress {
    from_port = 2049
    to_port = 2049
    protocol = "tcp"
    security_groups = [
      "${aws_security_group.SG_GUIDEWIRE.id}",
      "${aws_security_group.SG_GUIDEWIRE_DEV.id}"
    ]

    description = "GW EFS"
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "MANAGED_SERVICES"
    Name = "SG-GUIDEWIRE-EFS"
  }
}

resource "aws_security_group" "SG_GUIDEWIRE_DEV" {
  name = "GUIDEWIRE-DEV-SG"

  ingress {
    from_port = 8080
    to_port = 8083
    protocol = "tcp"
    security_groups = [
      "${aws_security_group.SG_INTERNAL_HAPROXY_DEV.id}"
    ]

    description = "HTTP - Internal"
  }

  ingress {
    from_port = 8080
    to_port = 8083
    protocol = "tcp"
    security_groups = [
      "${aws_security_group.SG_EXTERNAL_HAPROXY_DEV.id}"
    ]

    description = "HTTP - External"
  }

  ingress {
    from_port = 8080
    to_port = 8083
    protocol = "tcp"
    cidr_blocks = [
      "${var.economical_aviatrix_vpn}"
    ]

    description = "HTTP - Internal No Proxy"
  }

  ingress {
    from_port = 8080
    to_port = 8083
    protocol = "tcp"
    cidr_blocks = "${var.economical_corp_cidrs}"
    description = "HTTP - Economical Corp Internal No Proxy"
  }

  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = [
      "0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "MANAGED_SERVICES"
    Name = "GUIDEWIRE-DEV-SG"
  }
}


#### MICROSERVICES SG #####
resource "aws_security_group" "SG_MICROSERVICES" {
  name = "MICROSERVICES-SG"

  ingress {
    from_port = 8443
    to_port = 8443
    protocol = "tcp"
    security_groups = [
      "${aws_security_group.SG_EXTERNAL_HAPROXY.id}"
    ]
    description = "Orchestrator"
  }

  ingress {
    from_port = 15672
    to_port = 15672
    protocol = "tcp"
    cidr_blocks = [
      "${var.economical_aviatrix_vpn}"
    ]
    security_groups = [
      "${aws_security_group.SG_INTERNAL_HAPROXY.id}"
    ]
    description = "RabbitMQ Management UI"
  }

  ingress {
    from_port = 9002
    to_port = 9002
    protocol = "tcp"
    security_groups = [
      "${aws_security_group.SG_INTERNAL_HAPROXY.id}"
    ]
    description = "Edge"
  }

  ingress {
    from_port = 2377
    to_port = 2377
    protocol = "tcp"
    cidr_blocks = [
      "${var.economical_dev_cidr}"
    ]
    description = "Cluster Management Communications"
  }

  ingress {
    from_port = 7946
    to_port = 7946
    protocol = "udp"
    cidr_blocks = [
      "${var.economical_dev_cidr}"
    ]
    description = "Communication Among Nodes"
  }

  ingress {
    from_port = 7946
    to_port = 7946
    protocol = "tcp"
    cidr_blocks = [
      "${var.economical_dev_cidr}"
    ]
    description = "Communication Among Nodes"
  }

  ingress {
    from_port = 4789
    to_port = 4789
    protocol = "udp"
    cidr_blocks = [
      "${var.economical_dev_cidr}"
    ]
    description = "Overlay Network Traffic"
  }

  ingress {
    from_port = 8500
    to_port = 8500
    protocol = "tcp"
    cidr_blocks = [
      "${var.economical_aviatrix_vpn}"
    ]
    description = "Consul UI"
  }

  ingress {
    from_port = 80
    to_port = 80
    protocol = "tcp"
    cidr_blocks = [
      "${var.economical_dev_cidr}",
      "${var.economical_aviatrix_vpn}"
    ]
    description = "Grafana UI"
  }

  ingress {
    from_port = 19191
    to_port = 19191
    protocol = "tcp"
    cidr_blocks = [
      "${var.economical_dev_cidr}",
      "${var.economical_aviatrix_vpn}"
    ]
    description = "Thanos Query"
  }

  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = [
      "0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "MANAGED_SERVICES"
    Name = "MICROSERVICES-SG"
  }
}

resource "aws_security_group" "SG_MICROSERVICES_DEV" {
  name = "MICROSERVICES-DEV-SG"

  ingress {
    from_port = 8443
    to_port = 8443
    protocol = "tcp"
    security_groups = [
      "${aws_security_group.SG_EXTERNAL_HAPROXY_DEV.id}"
    ]
    description = "Orchestrator"
  }

  ingress {
    from_port = 15672
    to_port = 15672
    protocol = "tcp"
    cidr_blocks = [
      "${var.economical_aviatrix_vpn}"
    ]
    security_groups = [
      "${aws_security_group.SG_INTERNAL_HAPROXY_DEV.id}"
    ]
    description = "RabbitMQ Management UI"
  }

  ingress {
    from_port = 9002
    to_port = 9002
    protocol = "tcp"
    security_groups = [
      "${aws_security_group.SG_INTERNAL_HAPROXY_DEV.id}"
    ]
    description = "Edge"
  }

  ingress {
    from_port = 2377
    to_port = 2377
    protocol = "tcp"
    cidr_blocks = [
      "${var.economical_dev_cidr}"
    ]
    description = "Cluster Management Communications"
  }

  ingress {
    from_port = 7946
    to_port = 7946
    protocol = "udp"
    cidr_blocks = [
      "${var.economical_dev_cidr}"
    ]
    description = "Communication Among Nodes"
  }

  ingress {
    from_port = 7946
    to_port = 7946
    protocol = "tcp"
    cidr_blocks = [
      "${var.economical_dev_cidr}"
    ]
    description = "Communication Among Nodes"
  }

  ingress {
    from_port = 4789
    to_port = 4789
    protocol = "udp"
    cidr_blocks = [
      "${var.economical_dev_cidr}"
    ]
    description = "Overlay Network Traffic"
  }

  ingress {
    from_port = 8500
    to_port = 8500
    protocol = "tcp"
    cidr_blocks = [
      "${var.economical_aviatrix_vpn}"
    ]
    description = "Consul UI"
  }

  ingress {
    from_port = 80
    to_port = 80
    protocol = "tcp"
    cidr_blocks = [
      "${var.economical_dev_cidr}",
      "${var.economical_aviatrix_vpn}"
    ]
    description = "Grafana UI"
  }

  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = [
      "0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "MANAGED_SERVICES"
    Name = "MICROSERVICES-DEV-SG"
  }
}

resource "aws_security_group" "SG_EXTERNAL_HAPROXY_DEV" {
  name = "EXTERNAL-HAPROXY-DEV-SG"

  ingress {
    from_port = 80
    to_port = 80
    protocol = "tcp"
    cidr_blocks = [
      "${var.economical_hub_cidr}"
    ]
    description = "HTTP"
  }

  ingress {
    from_port = 443
    to_port = 443
    protocol = "tcp"
    cidr_blocks = [
      "${var.economical_hub_cidr}"
    ]
    description = "HTTPS"
  }

  ingress {
    from_port = 8444
    to_port = 8444
    protocol = "tcp"
    cidr_blocks = [
      "${var.economical_dev_cidr}"
    ]
    description = "Health"
  }

  ingress {
    from_port = 8404
    to_port = 8404
    protocol = "tcp"
    cidr_blocks = [
      "${var.economical_dev_cidr}",
      "${var.economical_aviatrix_vpn}"
    ]
    description = "Stats"
  }

  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = [
      "0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "MANAGED_SERVICES"
    Name = "EXTERNAL-HAPROXY-DEV-SG"
  }
}

resource "aws_security_group" "SG_INTERNAL_HAPROXY_DEV" {
  name = "INTERNAL-HAPROXY-DEV-SG"

  ingress {
    from_port = 80
    to_port = 80
    protocol = "tcp"
    cidr_blocks = [
      "${var.economical_dev_cidr}",
      "${var.economical_aviatrix_vpn}",
      "${var.economical_cidr}"
    ]
    description = "HTTP"
  }

  ingress {
    from_port = 443
    to_port = 443
    protocol = "tcp"
    cidr_blocks = [
      "${var.economical_dev_cidr}",
      "${var.economical_aviatrix_vpn}",
      "${var.economical_cidr}"
    ]
    description = "HTTPS"
  }

  ingress {
    from_port = 80
    to_port = 80
    protocol = "tcp"
    cidr_blocks = "${var.economical_corp_cidrs}"
    description = "HTTP"
  }

  ingress {
    from_port = 443
    to_port = 443
    protocol = "tcp"
    cidr_blocks = "${var.economical_corp_cidrs}"
    description = "HTTPS"
  }

  ingress {
    from_port = 8444
    to_port = 8444
    protocol = "tcp"
    cidr_blocks = [
      "${var.economical_dev_cidr}"
    ]
    description = "Health"
  }

  ingress {
    from_port = 8404
    to_port = 8404
    protocol = "tcp"
    cidr_blocks = [
      "${var.economical_dev_cidr}",
      "${var.economical_aviatrix_vpn}"
    ]
    description = "Stats"
  }

  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = [
      "0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "MANAGED_SERVICES"
    Name = "INTERNAL-HAPROXY-DEV-SG"
  }
}

resource "aws_security_group" "SG_EXTERNAL_HAPROXY" {
  name = "EXTERNAL-HAPROXY-SG"

  ingress {
    from_port = 443
    to_port = 443
    protocol = "tcp"
    cidr_blocks = [
      "${var.economical_hub_cidr}"
    ]
    description = "HTTPS"
  }

  ingress {
    from_port = 8444
    to_port = 8444
    protocol = "tcp"
    cidr_blocks = [
      "${var.economical_dev_cidr}"
    ]
    description = "Health"
  }

  ingress {
    from_port = 8404
    to_port = 8404
    protocol = "tcp"
    cidr_blocks = [
      "${var.economical_dev_cidr}",
      "${var.economical_aviatrix_vpn}"
    ]
    description = "Stats"
  }

  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = [
      "0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "MANAGED_SERVICES"
    Name = "EXTERNAL-HAPROXY-SG"
  }
}

resource "aws_security_group" "SG_INTERNAL_HAPROXY" {
  name = "INTERNAL-HAPROXY-SG"

  ingress {
    from_port = 443
    to_port = 443
    protocol = "tcp"
    cidr_blocks = [
      "${var.economical_dev_cidr}",
      "${var.economical_aviatrix_vpn}",
      "${var.economical_cidr}"
    ]
    description = "HTTPS"
  }

  ingress {
    from_port = 443
    to_port = 443
    protocol = "tcp"
    cidr_blocks = "${var.economical_corp_cidrs}"
    description = "HTTPS"
  }

  ingress {
    from_port = 8444
    to_port = 8444
    protocol = "tcp"
    cidr_blocks = [
      "${var.economical_dev_cidr}"
    ]
    description = "Health"
  }

  ingress {
    from_port = 8404
    to_port = 8404
    protocol = "tcp"
    cidr_blocks = [
      "${var.economical_dev_cidr}",
      "${var.economical_aviatrix_vpn}"
    ]
    description = "Stats"
  }

  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = [
      "0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "MANAGED_SERVICES"
    Name = "INTERNAL-HAPROXY-SG"
  }
}

resource "aws_security_group" "SG_HAPROXY_INTERNAL_PEERING" {
  name = "HAPROXY-INTERNAL-PEERING-SG"

  ingress {
    from_port = 1024
    to_port = 1024
    protocol = "tcp"
    security_groups = [
      "${aws_security_group.SG_INTERNAL_HAPROXY.id}"]
    description = "HAProxy Peering"
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "MANAGED_SERVICES"
    Name = "HAPROXY-INTERNAL-PEERING-SG"
  }
}

resource "aws_security_group" "SG_HAPROXY_EXTERNAL_PEERING" {
  name = "HAPROXY-EXTERNAL-PEERING-SG"

  ingress {
    from_port = 1024
    to_port = 1024
    protocol = "tcp"
    security_groups = [
      "${aws_security_group.SG_EXTERNAL_HAPROXY.id}"]
    description = "HAProxy Peering"
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "MANAGED_SERVICES"
    Name = "HAPROXY-EXTERNAL-PEERING-SG"
  }
}

resource "aws_security_group" "SG_GUIDEWIRE_CLUSTER_NBIO_EVENT_QUEUE" {
  name = "GUIDEWIRE-CLUSTER-NBIO-EVENT-QUEUE-SG"

  ingress {
    from_port = 40000
    to_port = 40003
    protocol = "tcp"
    security_groups = [
      "${aws_security_group.SG_GUIDEWIRE.id}"]
    description = "Cluster NBIO event queue"
  }

  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = [
      "0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "MANAGED_SERVICES"
    Name = "GUIDEWIRE-CLUSTER-NBIO-EVENT-QUEUE-SG"
  }
}

resource "aws_security_group" "SG_JENKINS_GUIDEWIRE_HEALTH_CHECK" {
  name = "JENKINS-GUIDEWIRE-HEALTH-CHECK-SG"

  ingress {
    from_port = 8080
    to_port = 8083
    protocol = "tcp"
    security_groups = [
      "${aws_security_group.SG_JENKINS_CLIENT.id}"
    ]
    description = "Dev Jenkins Client Health Check"
  }

  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = [
      "0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "MANAGED_SERVICES"
    Name = "JENKINS-GUIDEWIRE-HEALTH-CHECK-SG"
  }
}


#### RPA SG #####
resource "aws_security_group" "SG_RPA_APP" {
  name = "RPA-APP-SG"

  ingress {
    from_port = 8181
    to_port = 8181
    protocol = "tcp"

    cidr_blocks = [
      "${var.economical_dev_cidr}",
      "${var.economical_aviatrix_vpn}"
    ]

    description = "HTTP - RPA Webserver, and RPA Cross Talk 8181"
  }

  ingress {
    from_port = 8199
    to_port = 8199
    protocol = "tcp"

    cidr_blocks = [
      "${var.economical_dev_cidr}"
    ]

    description = "RPA Cross Talk 8199"
  }

  ingress {
    from_port = 49152
    to_port = 65535
    protocol = "tcp"

    cidr_blocks = [
      "${var.economical_dev_cidr}"
    ]

    description = "RPA Cross Talk High Port"
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "MANAGED_SERVICES"
    Name = "RPA-APP-SG"
  }
}

resource "aws_security_group" "SG_RPA_CLIENT" {
  name = "RPA-CLIENT-SG"

  ingress {
    from_port = 8181
    to_port = 8181
    protocol = "tcp"

    cidr_blocks = [
      "${var.economical_dev_cidr}",
      "${var.economical_aviatrix_vpn}"
    ]

    description = "HTTP - RPA Webserver and RPA Cross talk 8181"
  }

  ingress {
    from_port = 8199
    to_port = 8199
    protocol = "tcp"

    cidr_blocks = [
      "${var.economical_dev_cidr}"
    ]

    description = "RPA Cross Talk 8199"
  }

  ingress {
    from_port = 49152
    to_port = 65535
    protocol = "tcp"

    cidr_blocks = [
      "${var.economical_dev_cidr}"
    ]

    description = "RPA Cross Talk High Port"
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "MANAGED_SERVICES"
    Name = "RPA-CLIENT-SG"
  }
}

resource "aws_security_group" "SG_RPA_APP_DEV" {
  name = "RPA-APP-DEV-SG"

  ingress {
    from_port = 8181
    to_port = 8181
    protocol = "tcp"

    cidr_blocks = [
      "${var.economical_dev_cidr}",
      "${var.economical_aviatrix_vpn}"
    ]

    description = "HTTP - RPA Webserver, and RPA Cross Talk 8181"
  }

  ingress {
    from_port = 8199
    to_port = 8199
    protocol = "tcp"

    cidr_blocks = [
      "${var.economical_dev_cidr}"
    ]

    description = "RPA Cross Talk 8199"
  }

  ingress {
    from_port = 49152
    to_port = 65535
    protocol = "tcp"

    cidr_blocks = [
      "${var.economical_dev_cidr}"
    ]

    description = "RPA Cross Talk High Port"
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "MANAGED_SERVICES"
    Name = "RPA-APP-DEV-SG"
  }
}

resource "aws_security_group" "SG_RPA_CLIENT_DEV" {
  name = "RPA-CLIENT-DEV-SG"

  ingress {
    from_port = 8181
    to_port = 8181
    protocol = "tcp"

    cidr_blocks = [
      "${var.economical_dev_cidr}",
      "${var.economical_aviatrix_vpn}"
    ]

    description = "HTTP - RPA Webserver and RPA Cross talk 8181"
  }

  ingress {
    from_port = 8199
    to_port = 8199
    protocol = "tcp"

    cidr_blocks = [
      "${var.economical_dev_cidr}"
    ]

    description = "RPA Cross Talk 8199"
  }

  ingress {
    from_port = 49152
    to_port = 65535
    protocol = "tcp"

    cidr_blocks = [
      "${var.economical_dev_cidr}"
    ]

    description = "RPA Cross Talk High Port"
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "MANAGED_SERVICES"
    Name = "RPA-CLIENT-DEV-SG"
  }
}


#### DATABASES SG #####
resource "aws_security_group" "SG_MSSQL_DB" {
  name = "MSSQL-DB-SG"

  ingress {
    from_port = 1433
    to_port = 1433
    protocol = "tcp"
    cidr_blocks = [
      "${var.economical_dev_cidr}",
      "${var.economical_aviatrix_vpn}"
    ]
  }

  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = [
      "0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "MANAGED_SERVICES"
    Name = "MSSQL-DB-SG"
  }
}

resource "aws_security_group" "SG_CLIENT_PING_TEST" {
  name = "CLIENT-PING-TEST"

  ingress {
    from_port = -1
    to_port = -1
    protocol = "icmp"
    cidr_blocks = [
      "${var.economical_cidr}"]
  }

  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = [
      "0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "MANAGED_SERVICES"
    Name = "CLIENT-PING-TEST"
  }
}

resource "aws_security_group" "SG_MONITORING" {
  name = "MONITORING-SG"

  ingress {
    from_port = 10000
    to_port = 10001
    protocol = "tcp"
    cidr_blocks = [
      "${var.economical_dev_cidr}",
      "${var.economical_aviatrix_vpn}"
    ]
    description = "Prometheus/Alertmanager"
  }

  ingress {
    from_port = 3000
    to_port = 3000
    protocol = "tcp"
    cidr_blocks = [
      "${var.economical_dev_cidr}",
      "${var.economical_aviatrix_vpn}"
    ]
    description = "Grafana"
  }

  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = [
      "0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "MANAGED_SERVICES"
    Name = "MONITORING-SG"
  }
}

resource "aws_security_group" "SG_AVIATRIX_RDP" {
  name = "AVIATRIX-RDP-SG"

  ingress {
    from_port = 3389
    to_port = 3389
    protocol = "tcp"
    cidr_blocks = [
      "${var.economical_aviatrix_vpn}"
    ]
    description = "RDP"
  }

  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = [
      "0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "MANAGED_SERVICES"
    Name = "AVIATRIX-RDP-SG"
  }
}

resource "aws_security_group" "SG_AVIATRIX_SSH" {
  name = "AVIATRIX-SSH-SG"

  ingress {
    from_port = 22
    to_port = 22
    protocol = "tcp"
    cidr_blocks = [
      "${var.economical_aviatrix_vpn}"
    ]
    description = "SSH"
  }

  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = [
      "0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "MANAGED_SERVICES"
    Name = "AVIATRIX-SSH-SG"
  }
}


#### JENKINS SG #####
resource "aws_security_group" "SG_JENKINS_CLIENT" {
  name = "JENKINS-CLIENT-SG"

  ingress {
    from_port = 22
    to_port = 22
    protocol = "tcp"
    cidr_blocks = [
      "10.0.0.0/8"
    ]
  }

  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = [
      "0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "MANAGED_SERVICES"
    Name = "JENKINS-CLIENT-SG"
  }
}

resource "aws_security_group" "SG_JENKINS_CLIENT_SSH" {
  name = "JENKINS-CLIENT-SSH-SG"

  ingress {
    from_port = 22
    to_port = 22
    protocol = "tcp"
    security_groups = [
      "${aws_security_group.SG_JENKINS_CLIENT.id}"
    ]
    description = "Jenkins Client SSH"
  }

  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = [
      "0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "MANAGED_SERVICES"
    Name = "JENKINS-CLIENT-SSH-SG"
  }
}

### Route53 Resolver - IC HUB DNS ###
resource "aws_security_group" "SG_IC_HUB_DNS_RESOLVER_ENDPOINT" {
  name = "IC-HUB-DNS-RESOLVER-ENDPOINT-SG"

  ingress {
    from_port   = 53
    to_port     = 53
    protocol    = "tcp"
    cidr_blocks = [
      "${var.vpc_cidr}"
    ]
    description = "DNS"
  }

  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "MANAGED_SERVICES"
    Name = "IC-HUB-DNS-RESOLVER-ENDPOINT-SG"
  }
}
