# #=======================================================================
# # Shared Resource
# #=======================================================================

############ Jenkins Client ##################
resource "aws_volume_attachment" "ICBCAA_DEV_JENKINS_CLIENT_DOCKER_EBS_ATTACHMENT" {
  device_name = "/dev/sdh"
  volume_id   = "${aws_ebs_volume.ICBCAA_DEV_JENKINS_CLIENT_DOCKER_EBS.id}"
  instance_id = "${aws_instance.ICBCAA_DEV_JENKINS_CLIENT.id}"
}

resource "aws_ebs_volume" "ICBCAA_DEV_JENKINS_CLIENT_DOCKER_EBS" {
  availability_zone = "${aws_instance.ICBCAA_DEV_JENKINS_CLIENT.availability_zone}"
  size              = 25
  type              = "gp2"
  encrypted         = true
  kms_key_id        = "arn:aws:kms:ca-central-1:775629271970:key/48f133c3-6815-4a24-b1eb-b18ddbfb7319"

  tags = {
    Name   = "ICBCAA_DEV_JENKINS_CLIENT_DOCKER_EBS"
    Docker = "True"
    BILLINGCODE = "FPE01903-CO-TE-TO-1000"
    BILLINGCONTACT = "marrojas@deloitte.ca"
    COUNTRY = "CA"
    CSCLASS = "Confidential"
    CSQUAL = "CI/PI Data"
    CSTYPE = "External"
    ENVIRONMENT = "NPD"
    FUNCTION = "CON"
    MEMBERFIRM = "CA"
    PRIMARYCONTACT = "marrojas@deloitte.ca"
    SECONDARYCONTACT = "Samir Modh"
  }
}

# #=======================================================================
# # DEV01
# #=======================================================================

############ Guidewire Volumes ##################
resource "aws_volume_attachment" "ICBCAA_DEV01_GUIDEWIRE_SUITE_DOCKER_EBS_ATTACHMENT" {
  device_name = "/dev/sdh"
  volume_id   = "${aws_ebs_volume.ICBCAA_DEV01_GUIDEWIRE_SUITE_DOCKER_EBS.id}"
  instance_id = "${aws_instance.ICBCAA_DEV01_GUIDEWIRE_SUITE.id}"
}

resource "aws_ebs_volume" "ICBCAA_DEV01_GUIDEWIRE_SUITE_DOCKER_EBS" {
  availability_zone = "ca-central-1a"
  size              = 35
  type              = "gp2"
  encrypted         = true
  kms_key_id        = "arn:aws:kms:ca-central-1:775629271970:key/48f133c3-6815-4a24-b1eb-b18ddbfb7319"

  tags = {
    Name   = "ICBCAA_DEV01_GUIDEWIRE_SUITE_DOCKER_EBS"
    Docker = "True"
    BILLINGCODE = "FPE01903-CO-TE-TO-1000"
    BILLINGCONTACT = "marrojas@deloitte.ca"
    COUNTRY = "CA"
    CSCLASS = "Confidential"
    CSQUAL = "CI/PI Data"
    CSTYPE = "External"
    ENVIRONMENT = "NPD"
    FUNCTION = "CON"
    MEMBERFIRM = "CA"
    PRIMARYCONTACT = "marrojas@deloitte.ca"
    SECONDARYCONTACT = "Samir Modh"
  }
}

resource "aws_volume_attachment" "ICBCAA_DEV01_GUIDEWIRE_SUITE_LOGS_EBS_ATTACHMENT" {
  device_name = "/dev/sdi"
  volume_id   = "${aws_ebs_volume.ICBCAA_DEV01_GUIDEWIRE_SUITE_LOGS_EBS.id}"
  instance_id = "${aws_instance.ICBCAA_DEV01_GUIDEWIRE_SUITE.id}"
}

resource "aws_ebs_volume" "ICBCAA_DEV01_GUIDEWIRE_SUITE_LOGS_EBS" {
  availability_zone = "ca-central-1a"
  size              = 35
  type              = "gp2"
  encrypted         = true
  kms_key_id        = "arn:aws:kms:ca-central-1:775629271970:key/48f133c3-6815-4a24-b1eb-b18ddbfb7319"

  tags = {
    Name   = "ICBCAA_DEV01_GUIDEWIRE_SUITE_LOGS_EBS"
    Docker = "True"
    BILLINGCODE = "FPE01903-CO-TE-TO-1000"
    BILLINGCONTACT = "marrojas@deloitte.ca"
    COUNTRY = "CA"
    CSCLASS = "Confidential"
    CSQUAL = "CI/PI Data"
    CSTYPE = "External"
    ENVIRONMENT = "NPD"
    FUNCTION = "CON"
    MEMBERFIRM = "CA"
    PRIMARYCONTACT = "marrojas@deloitte.ca"
    SECONDARYCONTACT = "Samir Modh"
  }
}

############ Microservices Volumes ##################
resource "aws_volume_attachment" "ICBCAA_DEV01_MICROSERVICES_DOCKER_EBS_ATTACHMENT" {
  device_name = "/dev/sdh"
  volume_id   = "${aws_ebs_volume.ICBCAA_DEV01_MICROSERVICES_DOCKER_EBS.id}"
  instance_id = "${aws_instance.ICBCAA_DEV01_MICROSERVICES.id}"
}

resource "aws_ebs_volume" "ICBCAA_DEV01_MICROSERVICES_DOCKER_EBS" {
  availability_zone = "ca-central-1a"
  size              = 25
  encrypted         = true
  kms_key_id        = "arn:aws:kms:ca-central-1:775629271970:key/48f133c3-6815-4a24-b1eb-b18ddbfb7319"

  tags = {
    Name   = "ICBCAA_DEV01_MICROSERVICES_DOCKER_EBS"
    Docker = "True"
  }
}


############ Internal HAProxy Volumes ##################
resource "aws_volume_attachment" "ICBCAA_DEV01_INTERNAL_HAPROXY_DOCKER_EBS_ATTACHMENT" {
  device_name = "/dev/sdh"
  volume_id   = "${aws_ebs_volume.ICBCAA_DEV01_INTERNAL_HAPROXY_DOCKER_EBS.id}"
  instance_id = "${aws_instance.ICBCAA_DEV01_INTERNAL_HAPROXY.id}"
}

resource "aws_ebs_volume" "ICBCAA_DEV01_INTERNAL_HAPROXY_DOCKER_EBS" {
  availability_zone = "ca-central-1a"
  size              = 20
  encrypted         = true
  kms_key_id        = "arn:aws:kms:ca-central-1:775629271970:key/48f133c3-6815-4a24-b1eb-b18ddbfb7319"

  tags = {
    Name   = "ICBCAA_DEV01_INTERNAL_HAPROXY_DOCKER_EBS"
    Docker = "True"
    BILLINGCODE = "FPE01903-CO-TE-TO-1000"
    BILLINGCONTACT = "marrojas@deloitte.ca"
    COUNTRY = "CA"
    CSCLASS = "Confidential"
    CSQUAL = "CI/PI Data"
    CSTYPE = "External"
    ENVIRONMENT = "NPD"
    FUNCTION = "CON"
    MEMBERFIRM = "CA"
    PRIMARYCONTACT = "marrojas@deloitte.ca"
    SECONDARYCONTACT = "Samir Modh"
  }
}
# END OF DEV01

# #=======================================================================
# # DEV02
# #=======================================================================

############ Guidewire Volumes ##################
resource "aws_volume_attachment" "ICBCAA_DEV02_GUIDEWIRE_SUITE_DOCKER_EBS_ATTACHMENT" {
  device_name = "/dev/sdh"
  volume_id   = "${aws_ebs_volume.ICBCAA_DEV02_GUIDEWIRE_SUITE_DOCKER_EBS.id}"
  instance_id = "${aws_instance.ICBCAA_DEV02_GUIDEWIRE_SUITE.id}"
}

resource "aws_ebs_volume" "ICBCAA_DEV02_GUIDEWIRE_SUITE_DOCKER_EBS" {
  availability_zone = "ca-central-1a"
  size              = 35
  type              = "gp2"
  encrypted         = true
  kms_key_id        = "arn:aws:kms:ca-central-1:775629271970:key/48f133c3-6815-4a24-b1eb-b18ddbfb7319"

  tags = {
    Name   = "ICBCAA_DEV02_GUIDEWIRE_SUITE_DOCKER_EBS"
    Docker = "True"
    BILLINGCODE = "FPE01903-CO-TE-TO-1000"
    BILLINGCONTACT = "marrojas@deloitte.ca"
    COUNTRY = "CA"
    CSCLASS = "Confidential"
    CSQUAL = "CI/PI Data"
    CSTYPE = "External"
    ENVIRONMENT = "NPD"
    FUNCTION = "CON"
    MEMBERFIRM = "CA"
    PRIMARYCONTACT = "marrojas@deloitte.ca"
    SECONDARYCONTACT = "Samir Modh"
  }
}

resource "aws_volume_attachment" "ICBCAA_DEV02_GUIDEWIRE_SUITE_LOGS_EBS_ATTACHMENT" {
  device_name = "/dev/sdi"
  volume_id   = "${aws_ebs_volume.ICBCAA_DEV02_GUIDEWIRE_SUITE_LOGS_EBS.id}"
  instance_id = "${aws_instance.ICBCAA_DEV02_GUIDEWIRE_SUITE.id}"
}

resource "aws_ebs_volume" "ICBCAA_DEV02_GUIDEWIRE_SUITE_LOGS_EBS" {
  availability_zone = "ca-central-1a"
  size              = 35
  type              = "gp2"
  encrypted         = true
  kms_key_id        = "arn:aws:kms:ca-central-1:775629271970:key/48f133c3-6815-4a24-b1eb-b18ddbfb7319"

  tags = {
    Name   = "ICBCAA_DEV02_GUIDEWIRE_SUITE_LOGS_EBS"
    Docker = "True"
    BILLINGCODE = "FPE01903-CO-TE-TO-1000"
    BILLINGCONTACT = "marrojas@deloitte.ca"
    COUNTRY = "CA"
    CSCLASS = "Confidential"
    CSQUAL = "CI/PI Data"
    CSTYPE = "External"
    ENVIRONMENT = "NPD"
    FUNCTION = "CON"
    MEMBERFIRM = "CA"
    PRIMARYCONTACT = "marrojas@deloitte.ca"
    SECONDARYCONTACT = "Samir Modh"
  }
}

############ Microservices Volumes ##################
resource "aws_volume_attachment" "ICBCAA_DEV02_MICROSERVICES_DOCKER_EBS_ATTACHMENT" {
  device_name = "/dev/sdh"
  volume_id   = "${aws_ebs_volume.ICBCAA_DEV02_MICROSERVICES_DOCKER_EBS.id}"
  instance_id = "${aws_instance.ICBCAA_DEV02_MICROSERVICES.id}"
}

resource "aws_ebs_volume" "ICBCAA_DEV02_MICROSERVICES_DOCKER_EBS" {
  availability_zone = "ca-central-1a"
  size              = 25
  encrypted         = true
  kms_key_id        = "arn:aws:kms:ca-central-1:775629271970:key/48f133c3-6815-4a24-b1eb-b18ddbfb7319"

  tags = {
    Name   = "ICBCAA_DEV02_MICROSERVICES_DOCKER_EBS"
    Docker = "True"
    BILLINGCODE = "FPE01903-CO-TE-TO-1000"
    BILLINGCONTACT = "marrojas@deloitte.ca"
    COUNTRY = "CA"
    CSCLASS = "Confidential"
    CSQUAL = "CI/PI Data"
    CSTYPE = "External"
    ENVIRONMENT = "NPD"
    FUNCTION = "CON"
    MEMBERFIRM = "CA"
    PRIMARYCONTACT = "marrojas@deloitte.ca"
    SECONDARYCONTACT = "Samir Modh"
  }
}
# No separate HAProxy volume creation for DEV02 as we will use one proxy server for all instances
# End of DEV02