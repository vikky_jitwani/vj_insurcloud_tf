############ External HAProxy Volumes ##################
resource "aws_volume_attachment" "ICGORE_PROD_EXTERNAL_HAPROXY_1_DOCKER_EBS_ATTACHMENT" {
  device_name = "/dev/sdh"
  volume_id   = "${aws_ebs_volume.ICGORE_PROD_EXTERNAL_HAPROXY_1_DOCKER_EBS.id}"
  instance_id = "${aws_instance.ICGORE_PROD_EXTERNAL_HAPROXY_1.id}"
}

resource "aws_ebs_volume" "ICGORE_PROD_EXTERNAL_HAPROXY_1_DOCKER_EBS" {
  availability_zone = "ca-central-1a"
  size              = 20
  encrypted         = true
  kms_key_id        = "arn:aws:kms:ca-central-1:413329031558:key/5cdd3e56-2101-47df-bfe7-c4d1732f684f"
  tags = {
    Name = "ICGORE_PROD_EXTERNAL_HAPROXY_1_DOCKER_EBS"
    Docker = "True"
  }
}

resource "aws_volume_attachment" "ICGORE_PROD_EXTERNAL_HAPROXY_2_DOCKER_EBS_ATTACHMENT" {
  device_name = "/dev/sdh"
  volume_id   = "${aws_ebs_volume.ICGORE_PROD_EXTERNAL_HAPROXY_2_DOCKER_EBS.id}"
  instance_id = "${aws_instance.ICGORE_PROD_EXTERNAL_HAPROXY_2.id}"
}

resource "aws_ebs_volume" "ICGORE_PROD_EXTERNAL_HAPROXY_2_DOCKER_EBS" {
  availability_zone = "ca-central-1b"
  size              = 20
  encrypted         = true
  kms_key_id        = "arn:aws:kms:ca-central-1:413329031558:key/5cdd3e56-2101-47df-bfe7-c4d1732f684f"
  tags = {
    Name = "ICGORE_PROD_EXTERNAL_HAPROXY_2_DOCKER_EBS"
    Docker = "True"
  }
}

############ Internal HAProxy Volumes ##################
resource "aws_volume_attachment" "ICGORE_PROD_INTERNAL_HAPROXY_1_DOCKER_EBS_ATTACHMENT" {
  device_name = "/dev/sdh"
  volume_id   = "${aws_ebs_volume.ICGORE_PROD_INTERNAL_HAPROXY_1_DOCKER_EBS.id}"
  instance_id = "${aws_instance.ICGORE_PROD_INTERNAL_HAPROXY_1.id}"
}

resource "aws_ebs_volume" "ICGORE_PROD_INTERNAL_HAPROXY_1_DOCKER_EBS" {
  availability_zone = "ca-central-1a"
  size              = 20
  encrypted         = true
  kms_key_id        = "arn:aws:kms:ca-central-1:413329031558:key/5cdd3e56-2101-47df-bfe7-c4d1732f684f"
  tags = {
    Name = "ICGORE_PROD_INTERNAL_HAPROXY_1_DOCKER_EBS"
    Docker = "True"
  }
}

resource "aws_volume_attachment" "ICGORE_PROD_INTERNAL_HAPROXY_2_DOCKER_EBS_ATTACHMENT" {
  device_name = "/dev/sdh"
  volume_id   = "${aws_ebs_volume.ICGORE_PROD_INTERNAL_HAPROXY_2_DOCKER_EBS.id}"
  instance_id = "${aws_instance.ICGORE_PROD_INTERNAL_HAPROXY_2.id}"
}

resource "aws_ebs_volume" "ICGORE_PROD_INTERNAL_HAPROXY_2_DOCKER_EBS" {
  availability_zone = "ca-central-1b"
  size              = 20
  encrypted         = true
  kms_key_id        = "arn:aws:kms:ca-central-1:413329031558:key/5cdd3e56-2101-47df-bfe7-c4d1732f684f"
  tags = {
    Name = "ICGORE_PROD_INTERNAL_HAPROXY_2_DOCKER_EBS"
    Docker = "True"
  }
}

############ Microservices Volumes ##################

resource "aws_volume_attachment" "ICGORE_PROD_MICROSERVICES_MANAGER_1_DOCKER_EBS_ATTACHMENT" {
  device_name = "/dev/sdh"
  volume_id   = "${aws_ebs_volume.ICGORE_PROD_MICROSERVICES_MANAGER_1_DOCKER_EBS.id}"
  instance_id = "${aws_instance.ICGORE_PROD_MICROSERVICES_MANAGER_1.id}"
}

resource "aws_ebs_volume" "ICGORE_PROD_MICROSERVICES_MANAGER_1_DOCKER_EBS" {
  availability_zone = "ca-central-1a"
  size              = 50
  encrypted         = true
  kms_key_id        = "arn:aws:kms:ca-central-1:413329031558:key/5cdd3e56-2101-47df-bfe7-c4d1732f684f"
  tags = {
    Name = "ICGORE_PROD_MICROSERVICES_MANAGER_1_DOCKER_EBS"
    Docker = "True"
  }
}

resource "aws_volume_attachment" "ICGORE_PROD_MICROSERVICES_MANAGER_2_DOCKER_EBS_ATTACHMENT" {
  device_name = "/dev/sdh"
  volume_id   = "${aws_ebs_volume.ICGORE_PROD_MICROSERVICES_MANAGER_2_DOCKER_EBS.id}"
  instance_id = "${aws_instance.ICGORE_PROD_MICROSERVICES_MANAGER_2.id}"
}

resource "aws_ebs_volume" "ICGORE_PROD_MICROSERVICES_MANAGER_2_DOCKER_EBS" {
  availability_zone = "ca-central-1b"
  size              = 50
  encrypted         = true
  kms_key_id        = "arn:aws:kms:ca-central-1:413329031558:key/5cdd3e56-2101-47df-bfe7-c4d1732f684f"
  tags = {
    Name = "ICGORE_PROD_MICROSERVICES_MANAGER_2_DOCKER_EBS"
    Docker = "True"
  }
}

resource "aws_volume_attachment" "ICGORE_PROD_MICROSERVICES_MANAGER_3_DOCKER_EBS_ATTACHMENT" {
  device_name = "/dev/sdh"
  volume_id   = "${aws_ebs_volume.ICGORE_PROD_MICROSERVICES_MANAGER_3_DOCKER_EBS.id}"
  instance_id = "${aws_instance.ICGORE_PROD_MICROSERVICES_MANAGER_3.id}"
}

resource "aws_ebs_volume" "ICGORE_PROD_MICROSERVICES_MANAGER_3_DOCKER_EBS" {
  availability_zone = "ca-central-1a"
  size              = 50
  encrypted         = true
  kms_key_id        = "arn:aws:kms:ca-central-1:413329031558:key/5cdd3e56-2101-47df-bfe7-c4d1732f684f"
  tags = {
    Name = "ICGORE_PROD_MICROSERVICES_MANAGER_3_DOCKER_EBS"
    Docker = "True"
  }
}

resource "aws_volume_attachment" "ICGORE_PROD_MICROSERVICES_WORKER_1_DOCKER_EBS_ATTACHMENT" {
  device_name = "/dev/sdh"
  volume_id   = "${aws_ebs_volume.ICGORE_PROD_MICROSERVICES_APPLICATION_1_DOCKER_EBS.id}"
  instance_id = "${aws_instance.ICGORE_PROD_MICROSERVICES_APPLICATION_1.id}"
}

resource "aws_ebs_volume" "ICGORE_PROD_MICROSERVICES_APPLICATION_1_DOCKER_EBS" {
  availability_zone = "ca-central-1a"
  size              = 150
  encrypted         = true
  kms_key_id        = "arn:aws:kms:ca-central-1:413329031558:key/5cdd3e56-2101-47df-bfe7-c4d1732f684f"
  tags = {
    Name = "ICGORE_PROD_MICROSERVICES_APPLICATION_1_DOCKER_EBS"
    Docker = "True"
  }
}

resource "aws_volume_attachment" "ICGORE_PROD_MICROSERVICES_WORKER_2_DOCKER_EBS_ATTACHMENT" {
  device_name = "/dev/sdh"
  volume_id   = "${aws_ebs_volume.ICGORE_PROD_MICROSERVICES_APPLICATION_2_DOCKER_EBS.id}"
  instance_id = "${aws_instance.ICGORE_PROD_MICROSERVICES_APPLICATION_2.id}"
}

resource "aws_ebs_volume" "ICGORE_PROD_MICROSERVICES_APPLICATION_2_DOCKER_EBS" {
  availability_zone = "ca-central-1b"
  size              = 150
  encrypted         = true
  kms_key_id        = "arn:aws:kms:ca-central-1:413329031558:key/5cdd3e56-2101-47df-bfe7-c4d1732f684f"
  tags = {
    Name = "ICGORE_PROD_MICROSERVICES_APPLICATION_2_DOCKER_EBS"
    Docker = "True"
  }
}

############ Jenkins Client ##################
resource "aws_volume_attachment" "ICGORE_PROD_JENKINS_CLIENT_DOCKER_EBS_ATTACHMENT" {
  device_name = "/dev/sdh"
  volume_id   = "${aws_ebs_volume.ICGORE_PROD_JENKINS_CLIENT_DOCKER_EBS.id}"
  instance_id = "${aws_instance.ICGORE_PROD_JENKINS_CLIENT.id}"
}

resource "aws_ebs_volume" "ICGORE_PROD_JENKINS_CLIENT_DOCKER_EBS" {
  availability_zone = "${aws_instance.ICGORE_PROD_JENKINS_CLIENT.availability_zone}"
  size              = 50
  type              = "gp2"
  encrypted         = true
  kms_key_id        = "arn:aws:kms:ca-central-1:413329031558:key/5cdd3e56-2101-47df-bfe7-c4d1732f684f"

  tags = {
    Name          = "ICGORE_PROD_JENKINS_CLIENT_DOCKER_EBS"
    Docker        = "True"
  }
}
