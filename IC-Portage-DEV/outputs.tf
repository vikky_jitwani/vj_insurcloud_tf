
output "aws_lb_nonprod_portage_load_balancer_dns" {
  value = "${aws_lb.nonprod_portage_load_balancer.dns_name}"
}

output "aws_lb_nonprod_portage_load_balancer_zone_id" {
  value = "${aws_lb.nonprod_portage_load_balancer.zone_id}"
}