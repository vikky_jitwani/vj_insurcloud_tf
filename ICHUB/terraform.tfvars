#Envrionment specific variables
client_name = "ICHUB"
enviornment_name = "HUB"
enviornment_type = "NP"
client_number = "A003"
#ad-dc1 = "10.10.4.10"
#ad-dc2 = "10.10.6.10"
volume_key = "arn:aws:kms:ca-central-1:020266858005:key/6344f41d-b112-48df-882d-24dbcc75a7a5"
ami_id_windows2012sql14enterprise = "ami-07a67e65265ba59dc"
ami_id_windows2019container = "ami-013453540e1aa0d48"
ami_id_windows2019base = "ami-06b8f69a907cf6bb2"
ami_id_windows2016base = "ami-0bce38ba7ac166aa9"
ami_id_windows2012R2base = "ami-09b707ce10f2e9cee"
ami_id_windows2008base = "ami-0446e08d2d99fc8c9"
ami_id_redhatlinux75 = "ami-05c33d286020a47f9"
ami_id_ubuntu1804baseimage = "ami-032172cc5f34b32fc"
ami_id_ubuntu1604baseimage = "ami-04f40cca01b3186ea"
ami_id_amazonlinux201712base = "ami-0a777bbf8ef3f43bf"
ami_id_amazonlinux201709base = "ami-058dba934995c5468"
ami_id_amazonlinux2base = "ami-0ad9abdd25c431def"
ami_id_amazonlinux201803base = "ami-06829694f407e15c1"

#VPC Specific variables
vpc_id = "vpc-041c7f53b40a9ca78"
vpc_cidr = "10.15.16.0/22"
#sg_cidr = "XXX"

ichub-private-1a-subnet = "subnet-0b687dd606c6310ba"
ichub-private-1b-subnet = "subnet-0d6e4b15c39d7cfba"
ichub-public-1a-subnet = "subnet-052c51b070e48cd45"
ichub-public-1b-subnet = "subnet-0ec1eb1079da76ca9"

#R53 
zone_id = "Z0926334S0O93ZMFR7EU"

#Conduit RDS Password
conduit-rds-password = "postgres"