resource "aws_security_group" "SG_WORKSPACES" {
  name = "WORKSPACES-SG"

  ingress {
    from_port   = 1433
    to_port     = 1433
    protocol    = "tcp"
    description = "From AWS Workspaces to SAP Server"
    cidr_blocks = "${var.data_and_reporting_workspaces}"
  }

  ingress {
    from_port   = 8080
    to_port     = 8080
    protocol    = "tcp"
    description = "From AWS Workspaces to SAP Server"
    cidr_blocks = "${var.data_and_reporting_workspaces}"
  }

  ingress {
    from_port   = 49152
    to_port     = 65535
    protocol    = "tcp"
    description = "From AWS Workspaces to SAP Server"
    cidr_blocks = "${var.data_and_reporting_workspaces}"
  }

  ingress {
    from_port   = 4010
    to_port     = 4010
    protocol    = "tcp"
    description = "From AWS Workspaces to SAP Server"
    cidr_blocks = "${var.data_and_reporting_workspaces}"
  }

  ingress {
    from_port   = 4013
    to_port     = 4013
    protocol    = "tcp"
    description = "From AWS Workspaces to SAP Server"
    cidr_blocks = "${var.data_and_reporting_workspaces}"
  }

  ingress {
    from_port   = 4012
    to_port     = 4012
    protocol    = "tcp"
    description = "From AWS Workspaces to SAP Server"
    cidr_blocks = "${var.data_and_reporting_workspaces}"
  }

  ingress {
    from_port   = 6400
    to_port     = 6400
    protocol    = "tcp"
    description = "From AWS Workspaces to SAP Server"
    cidr_blocks = "${var.data_and_reporting_workspaces}"
  }

  ingress {
    from_port   = 445
    to_port     = 445
    protocol    = "tcp"
    description = "From AWS Workspaces to SAP Server"
    cidr_blocks = "${var.data_and_reporting_workspaces}"
  }

  ingress {
    from_port   = 4001
    to_port     = 4001
    protocol    = "tcp"
    description = "From AWS Workspaces to SAP Server"
    cidr_blocks = "${var.data_and_reporting_workspaces}"
  }

  ingress {
    from_port   = 4000
    to_port     = 4000
    protocol    = "tcp"
    description = "From AWS Workspaces to SAP Server"
    cidr_blocks = "${var.data_and_reporting_workspaces}"
  }

  ingress {
    from_port   = 3500
    to_port     = 3500
    protocol    = "tcp"
    description = "From AWS Workspaces to SAP Server"
    cidr_blocks = "${var.data_and_reporting_workspaces}"
  }
  ingress {
    from_port   = 4011
    to_port     = 4011
    protocol    = "tcp"
    description = "From AWS Workspaces to SAP Server"
    cidr_blocks = "${var.data_and_reporting_workspaces}"
  }

  ingress {
    from_port   = 5001
    to_port     = 5001
    protocol    = "tcp"
    description = "From AWS Workspaces to SAP Server"
    cidr_blocks = "${var.data_and_reporting_workspaces}"
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    "Name" = "WORKSPACES-SG"
  }
}