resource "aws_security_group" "SG_JOB_SCHEDULER" {
  name = "JOB-SCHEDULER-SG"

  ingress {
    from_port       = 4444
    to_port         = 4444
    protocol        = "tcp"
    cidr_blocks = [
      "${var.bcaa_dev_cidr}",
      "${var.bcaa_aviatrix_vpn}"
    ]
  }

  ingress {
    from_port       = 4444
    to_port         = 4444
    protocol        = "udp"
    cidr_blocks = [
      "${var.bcaa_dev_cidr}",
      "${var.bcaa_aviatrix_vpn}"
    ]
  }

  ingress {
    from_port       = 4446
    to_port         = 4446
    protocol        = "tcp"
    cidr_blocks = [
      "${var.bcaa_dev_cidr}",
      "${var.bcaa_aviatrix_vpn}"
    ]
  }

  ingress {
    from_port       = 4446
    to_port         = 4446
    protocol        = "udp"
    cidr_blocks = [
      "${var.bcaa_dev_cidr}",
      "${var.bcaa_aviatrix_vpn}"
    ]
  }

  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    Name = "JOB-SCHEDULER-SG"
    BILLINGCODE = "FPE01903-CO-TE-TO-1000"
    BILLINGCONTACT = "marrojas@deloitte.ca"
    COUNTRY = "CA"
    CSCLASS = "Confidential"
    CSQUAL = "CI/PI Data"
    CSTYPE = "External"
    ENVIRONMENT = "NPD"
    FUNCTION = "CON"
    MEMBERFIRM = "CA"
    PRIMARYCONTACT = "marrojas@deloitte.ca"
    SECONDARYCONTACT = "Samir Modh"
  }
}

resource "aws_security_group" "SG_MICROSERVICES" {
  name = "MICROSERVICES-SG"

    ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = [
      "${var.bcaa_aviatrix_vpn}"
    ]
    description = "Aviatrix VPN and Grafana UI"
  }

    ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = [
      "${var.bcaa_dev_cidr}"
    ]
    description = "BCAA-DEV-CIDR and Grafana UI"
  }

  ingress {
    from_port   = 8443
    to_port     = 8443
    protocol    = "tcp"
    security_groups = [
      "${aws_security_group.SG_EXTERNAL_HAPROXY.id}"
    ]
    description = "Orchestrator"
  }

  ingress {
    from_port   = 15672
    to_port     = 15672
    protocol    = "tcp"
    cidr_blocks = [
      "${var.bcaa_aviatrix_vpn}"
    ]
    security_groups = [
      "${aws_security_group.SG_INTERNAL_HAPROXY.id}"
    ]
    description = "RabbitMQ Management UI"
  }

  ingress {
    from_port   = 9002
    to_port     = 9002
    protocol    = "tcp"
    security_groups = [
      "${aws_security_group.SG_INTERNAL_HAPROXY.id}"
    ]
    description = "Edge"
  }

  ingress {
    from_port   = 2377
    to_port     = 2377
    protocol    = "tcp"
    cidr_blocks = [
      "${var.bcaa_dev_cidr}"
    ]
    description = "Cluster Management Communications"
  }

  ingress {
    from_port   = 7946
    to_port     = 7946
    protocol    = "udp"
    cidr_blocks = [
      "${var.bcaa_dev_cidr}"
    ]
    description = "Communication Among Nodes"
  }

  ingress {
    from_port   = 7946
    to_port     = 7946
    protocol    = "tcp"
    cidr_blocks = [
      "${var.bcaa_dev_cidr}"
    ]
    description = "Communication Among Nodes"
  }

  ingress {
    from_port   = 4789
    to_port     = 4789
    protocol    = "udp"
    cidr_blocks = [
      "${var.bcaa_dev_cidr}"
    ]
    description = "Overlay Network Traffic"
  }

  ingress {
    from_port   = 8500
    to_port     = 8500
    protocol    = "tcp"
     cidr_blocks = [
      "${var.bcaa_aviatrix_vpn}"
    ]
    description = "Consul UI"
  }

  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    Name = "MICROSERVICES-SG"
    BILLINGCODE = "FPE01903-CO-TE-TO-1000"
    BILLINGCONTACT = "marrojas@deloitte.ca"
    COUNTRY = "CA"
    CSCLASS = "Confidential"
    CSQUAL = "CI/PI Data"
    CSTYPE = "External"
    ENVIRONMENT = "NPD"
    FUNCTION = "CON"
    MEMBERFIRM = "CA"
    PRIMARYCONTACT = "marrojas@deloitte.ca"
    SECONDARYCONTACT = "Samir Modh"
  }
}

resource "aws_security_group" "SG_MICROSERVICES_DEV" {
  name = "MICROSERVICES-DEV-SG"

  ingress {
    from_port = 8443
    to_port   = 8443
    protocol  = "tcp"

    security_groups = ["${aws_security_group.SG_EXTERNAL_HAPROXY_DEV.id}", "${aws_security_group.SG_INTERNAL_HAPROXY_DEV.id}"]

    description = "Orchestrator"
  }

  ingress {
    from_port   = 8443
    to_port     = 8443
    protocol    = "tcp"
    cidr_blocks = ["${var.bcaa_aviatrix_vpn}", "${var.bcaa_dev_cidr}", "${var.bcaa_hub_cidr}"]
    description = "Orchestrator Dev/Aviatrix"
  }

  ingress {
    from_port = 9002
    to_port   = 9002
    protocol  = "tcp"

    security_groups = ["${aws_security_group.SG_GUIDEWIRE_DEV.id}"]

    description = "Guidewire"
  }

  ingress {
    from_port = 15672
    to_port   = 15672
    protocol  = "tcp"

    cidr_blocks = ["${var.bcaa_aviatrix_vpn}"]

    security_groups = ["${aws_security_group.SG_INTERNAL_HAPROXY_DEV.id}"]

    description = "RabbitMQ Management UI"
  }

  ingress {
    from_port = 9050
    to_port   = 9050
    protocol  = "tcp"

    cidr_blocks = ["${var.bcaa_aviatrix_vpn}"]

    security_groups = ["${aws_security_group.SG_INTERNAL_HAPROXY_DEV.id}"]

    description = "Admin Service"
  }

  ingress {
    from_port = 9002
    to_port   = 9002
    protocol  = "tcp"

    cidr_blocks = ["${var.bcaa_aviatrix_vpn}"]

    security_groups = ["${aws_security_group.SG_INTERNAL_HAPROXY_DEV.id}"]

    description = "Edge Service"
  }

  ingress {
    from_port = 2377
    to_port   = 2377
    protocol  = "tcp"

    cidr_blocks = ["${var.bcaa_dev_cidr}"]

    description = "Cluster Management Communications"
  }

  ingress {
    from_port = 7946
    to_port   = 7946
    protocol  = "udp"

    cidr_blocks = ["${var.bcaa_dev_cidr}"]

    description = "Communication Among Nodes"
  }

  ingress {
    from_port = 7946
    to_port   = 7946
    protocol  = "tcp"

    cidr_blocks = ["${var.bcaa_dev_cidr}"]

    description = "Communication Among Nodes"
  }

  ingress {
    from_port = 4789
    to_port   = 4789
    protocol  = "udp"

    cidr_blocks = ["${var.bcaa_dev_cidr}"]

    description = "Overlay Network Traffic"
  }

  ingress {
    from_port = 8500
    to_port   = 8500
    protocol  = "tcp"

    cidr_blocks = ["${var.bcaa_aviatrix_vpn}"]

    description = "Consul UI"
  }

  ingress {
    from_port = 80
    to_port   = 80
    protocol  = "tcp"

    cidr_blocks = ["10.41.8.192/32"]

    description = "Orchestrator"
  }

  ingress {
    from_port = 8080
    to_port   = 8080
    protocol  = "tcp"

    cidr_blocks = ["${var.bcaa_aviatrix_vpn}"]

    description = "CAdvisory"
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    Contract = "MANAGED_SERVICES"
    Name     = "MICROSERVICES-DEV-SG"
    BILLINGCODE = "FPE01903-CO-TE-TO-1000"
    BILLINGCONTACT = "marrojas@deloitte.ca"
    COUNTRY = "CA"
    CSCLASS = "Confidential"
    CSQUAL = "CI/PI Data"
    CSTYPE = "External"
    ENVIRONMENT = "NPD"
    FUNCTION = "CON"
    MEMBERFIRM = "CA"
    PRIMARYCONTACT = "marrojas@deloitte.ca"
    SECONDARYCONTACT = "Samir Modh"
  }

  ingress {
    from_port = 8079
    to_port   = 8079
    protocol  = "tcp"

    cidr_blocks = ["${var.bcaa_aviatrix_vpn}"]

    description = "Wiremock"
  }
}

resource "aws_security_group" "SG_EXSTREAM" {
  name = "EXSTREAM-SG"

  ingress {
    from_port   = 28700
    to_port     = 28702
    protocol    = "tcp"
    cidr_blocks = [
      "${var.bcaa_dev_cidr}",
      "${var.bcaa_hub_cidr}"
    ]
  }

  ingress {
    from_port   = 2701
    to_port     = 2702
    protocol    = "tcp"
    cidr_blocks = [
      "${var.bcaa_dev_cidr}",
      "${var.bcaa_hub_cidr}"
    ]
  }

  ingress {
    from_port   = 8080
    to_port     = 8080
    protocol    = "tcp"
    cidr_blocks = [
      "${var.bcaa_dev_cidr}",
      "${var.bcaa_hub_cidr}"
    ]
  }

  ingress {
    from_port   = 8443
    to_port     = 8443
    protocol    = "tcp"
    cidr_blocks = [
      "${var.bcaa_dev_cidr}",
      "${var.bcaa_hub_cidr}"
    ]
  }

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = [
      "${var.bcaa_dev_cidr}",
      "${var.bcaa_hub_cidr}"
    ]
  }

  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = [
      "${var.bcaa_dev_cidr}",
      "${var.bcaa_hub_cidr}"
    ]
  }

  ingress {
    from_port   = 5858
    to_port     = 5858
    protocol    = "tcp"
    cidr_blocks = [
      "${var.bcaa_dev_cidr}",
      "${var.bcaa_hub_cidr}"
    ]
  }

  ingress {
    from_port   = 3099
    to_port     = 3099
    protocol    = "tcp"
    cidr_blocks = [
      "${var.bcaa_dev_cidr}",
      "${var.bcaa_hub_cidr}"
    ]
  }

  ingress {
    from_port   = 2099
    to_port     = 2099
    protocol    = "tcp"
    cidr_blocks = [
      "${var.bcaa_dev_cidr}",
      "${var.bcaa_hub_cidr}"
    ]
  }

  ingress {
    from_port   = 389
    to_port     = 389
    protocol    = "tcp"
    cidr_blocks = [
      "${var.bcaa_dev_cidr}",
      "${var.bcaa_hub_cidr}"
    ]
  }

  ingress {
    from_port   = 4440
    to_port     = 4440
    protocol    = "tcp"
    cidr_blocks = [
      "${var.bcaa_dev_cidr}",
      "${var.bcaa_hub_cidr}"
    ]
  }

  ingress {
    from_port   = 8009
    to_port     = 8009
    protocol    = "tcp"
    cidr_blocks = [
      "${var.bcaa_dev_cidr}",
      "${var.bcaa_hub_cidr}"
    ]
  }

  ingress {
    from_port   = 4040
    to_port     = 4040
    protocol    = "tcp"
    cidr_blocks = [
      "${var.bcaa_dev_cidr}",
      "${var.bcaa_hub_cidr}"
    ]
  }

  ingress {
    from_port   = 8989
    to_port     = 8989
    protocol    = "tcp"
    cidr_blocks = [
      "${var.bcaa_dev_cidr}",
      "${var.bcaa_hub_cidr}"
    ]
  }

  ingress {
    from_port   = 41616
    to_port     = 41616
    protocol    = "tcp"
    cidr_blocks = [
      "${var.bcaa_dev_cidr}",
      "${var.bcaa_hub_cidr}"
    ]
  }

  ingress {
    from_port   = 5868
    to_port     = 5869
    protocol    = "tcp"
    cidr_blocks = [
      "${var.bcaa_dev_cidr}",
      "${var.bcaa_hub_cidr}"
    ]
  }

  ingress {
    from_port   = 8100
    to_port     = 8100
    protocol    = "tcp"
    cidr_blocks = [
      "${var.bcaa_dev_cidr}",
      "${var.bcaa_hub_cidr}"
    ]
  }

  ingress {
    from_port   = 8300
    to_port     = 8300
    protocol    = "tcp"
    cidr_blocks = [
      "${var.bcaa_dev_cidr}",
      "${var.bcaa_hub_cidr}"
    ]
  }

  ingress {
    from_port   = 8500
    to_port     = 8500
    protocol    = "tcp"
    cidr_blocks = [
      "${var.bcaa_dev_cidr}",
      "${var.bcaa_hub_cidr}"
    ]
  }

  ingress {
    from_port   = 8600
    to_port     = 8603
    protocol    = "tcp"
    cidr_blocks = [
      "${var.bcaa_dev_cidr}",
      "${var.bcaa_hub_cidr}"
    ]
  }

  ingress {
    from_port   = 8512
    to_port     = 8515
    protocol    = "tcp"
    cidr_blocks = [
      "${var.bcaa_dev_cidr}",
      "${var.bcaa_hub_cidr}"
    ]
  }

  ingress {
    from_port   = 2719
    to_port     = 2719
    protocol    = "tcp"
    cidr_blocks = [
      "${var.bcaa_dev_cidr}",
      "${var.bcaa_hub_cidr}"
    ]
  }

  ingress {
    from_port   = 135
    to_port     = 139
    protocol    = "tcp"
    cidr_blocks = [
      "${var.bcaa_dev_cidr}",
      "${var.bcaa_hub_cidr}"
    ]
  }

  ingress {
    from_port   = 135
    to_port     = 139
    protocol    = "udp"
    cidr_blocks = [
      "${var.bcaa_dev_cidr}",
      "${var.bcaa_hub_cidr}"
    ]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    Name = "EXSTREAM-SG"
    BILLINGCODE = "FPE01903-CO-TE-TO-1000"
    BILLINGCONTACT = "marrojas@deloitte.ca"
    COUNTRY = "CA"
    CSCLASS = "Confidential"
    CSQUAL = "CI/PI Data"
    CSTYPE = "External"
    ENVIRONMENT = "NPD"
    FUNCTION = "CON"
    MEMBERFIRM = "CA"
    PRIMARYCONTACT = "marrojas@deloitte.ca"
    SECONDARYCONTACT = "Samir Modh"
  }
}

resource "aws_security_group" "SG_EXTERNAL_HAPROXY_DEV" {
  name = "EXTERNAL-HAPROXY-DEV-SG"

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = [
      "${var.bcaa_hub_cidr}"
    ]
    description = "HTTP"
  }

    ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = [
      "${var.bcaa_hub_cidr}"
    ]
    description = "HTTPS"
  }

  ingress {
    from_port   = 8444
    to_port     = 8444
    protocol    = "tcp"
    cidr_blocks = [
      "${var.bcaa_dev_cidr}"
    ]
    description = "Health"
  }

  ingress {
    from_port   = 8404
    to_port     = 8404
    protocol    = "tcp"
    cidr_blocks = [
      "${var.bcaa_dev_cidr}",
      "${var.bcaa_aviatrix_vpn}"
    ]
    description = "Stats"
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    Name = "EXTERNAL-HAPROXY-DEV-SG"
    BILLINGCODE = "FPE01903-CO-TE-TO-1000"
    BILLINGCONTACT = "marrojas@deloitte.ca"
    COUNTRY = "CA"
    CSCLASS = "Confidential"
    CSQUAL = "CI/PI Data"
    CSTYPE = "External"
    ENVIRONMENT = "NPD"
    FUNCTION = "CON"
    MEMBERFIRM = "CA"
    PRIMARYCONTACT = "marrojas@deloitte.ca"
    SECONDARYCONTACT = "Samir Modh"
  }
}

resource "aws_security_group" "SG_INTERNAL_HAPROXY_DEV" {
  name = "INTERNAL-HAPROXY-DEV-SG"

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = [
      "${var.bcaa_dev_cidr}",
      "${var.bcaa_aviatrix_vpn}",
      "${var.bcaa_cidr}",
      "${var.bcaa_watchguard_cidr}",
      "${var.bcaa_dmz_cidr}",
      "${var.bcaa_wireless_cidr}",
      "${var.bcaa_hub_cidr}", # For Jobscheduler
      "${var.bcaa_gateways_cidr}"
    ]
    description = "HTTP"
  }

  ingress {
    from_port   = 8444
    to_port     = 8444
    protocol    = "tcp"
    cidr_blocks = [
      "${var.bcaa_dev_cidr}",
      "${var.bcaa_hub_cidr}"
    ]
    description = "Health"
  }

  ingress {
    from_port   = 8404
    to_port     = 8404
    protocol    = "tcp"
    cidr_blocks = [
      "${var.bcaa_dev_cidr}",
      "${var.bcaa_aviatrix_vpn}"
    ]
    description = "Stats"
  }

    ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = [
      "${var.bcaa_gateways_cidr}"
    ]
    description = "Guidewire endpoints VPN"
  }


  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    Name = "INTERNAL-HAPROXY-DEV-SG"
    BILLINGCODE = "FPE01903-CO-TE-TO-1000"
    BILLINGCONTACT = "marrojas@deloitte.ca"
    COUNTRY = "CA"
    CSCLASS = "Confidential"
    CSQUAL = "CI/PI Data"
    CSTYPE = "External"
    ENVIRONMENT = "NPD"
    FUNCTION = "CON"
    MEMBERFIRM = "CA"
    PRIMARYCONTACT = "marrojas@deloitte.ca"
    SECONDARYCONTACT = "Samir Modh"
  }
}

resource "aws_security_group" "SG_EXTERNAL_HAPROXY" {
  name = "EXTERNAL-HAPROXY-SG"

  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = [
      "${var.bcaa_hub_cidr}"
    ]
    description = "HTTPS"
  }

  ingress {
    from_port   = 8444
    to_port     = 8444
    protocol    = "tcp"
    cidr_blocks = [
      "${var.bcaa_dev_cidr}"
    ]
    description = "Health"
  }

  ingress {
    from_port   = 8404
    to_port     = 8404
    protocol    = "tcp"
    cidr_blocks = [
      "${var.bcaa_dev_cidr}",
      "${var.bcaa_aviatrix_vpn}"
    ]
    description = "Stats"
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    Name = "EXTERNAL-HAPROXY-SG"
  }
}

resource "aws_security_group" "SG_INTERNAL_HAPROXY" {
  name = "INTERNAL-HAPROXY-SG"

  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = [
      "${var.bcaa_dev_cidr}",
      "${var.bcaa_aviatrix_vpn}",
      "${var.bcaa_cidr}",
      "${var.bcaa_watchguard_cidr}",
      "${var.bcaa_dmz_cidr}",
      "${var.bcaa_wireless_cidr}",
      "${var.bcaa_hub_cidr}" # For Jobscheduler
    ]
    description = "HTTPS"
  }

  ingress {
    from_port   = 8444
    to_port     = 8444
    protocol    = "tcp"
    cidr_blocks = [
      "${var.bcaa_dev_cidr}",
      "${var.bcaa_hub_cidr}"
    ]
    description = "Health"
  }

  ingress {
    from_port   = 8404
    to_port     = 8404
    protocol    = "tcp"
    cidr_blocks = [
      "${var.bcaa_dev_cidr}",
      "${var.bcaa_aviatrix_vpn}"
    ]
    description = "Stats"
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    Name = "INTERNAL-HAPROXY-SG"
    BILLINGCODE = "FPE01903-CO-TE-TO-1000"
    BILLINGCONTACT = "marrojas@deloitte.ca"
    COUNTRY = "CA"
    CSCLASS = "Confidential"
    CSQUAL = "CI/PI Data"
    CSTYPE = "External"
    ENVIRONMENT = "NPD"
    FUNCTION = "CON"
    MEMBERFIRM = "CA"
    PRIMARYCONTACT = "marrojas@deloitte.ca"
    SECONDARYCONTACT = "Samir Modh"
  }
}

resource "aws_security_group" "SG_HAPROXY_INTERNAL_PEERING" {
  name = "HAPROXY-INTERNAL-PEERING-SG"

  ingress {
    from_port       = 1024
    to_port         = 1024
    protocol        = "tcp"
    security_groups = ["${aws_security_group.SG_INTERNAL_HAPROXY.id}"]
    description     = "HAProxy Peering"
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    Name = "HAPROXY-INTERNAL-PEERING-SG"
    BILLINGCODE = "FPE01903-CO-TE-TO-1000"
    BILLINGCONTACT = "marrojas@deloitte.ca"
    COUNTRY = "CA"
    CSCLASS = "Confidential"
    CSQUAL = "CI/PI Data"
    CSTYPE = "External"
    ENVIRONMENT = "NPD"
    FUNCTION = "CON"
    MEMBERFIRM = "CA"
    PRIMARYCONTACT = "marrojas@deloitte.ca"
    SECONDARYCONTACT = "Samir Modh"
  }
}

resource "aws_security_group" "SG_HAPROXY_EXTERNAL_PEERING" {
  name = "HAPROXY-EXTERNAL-PEERING-SG"

  ingress {
    from_port       = 1024
    to_port         = 1024
    protocol        = "tcp"
    security_groups = ["${aws_security_group.SG_EXTERNAL_HAPROXY.id}"]
    description     = "HAProxy Peering"
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    Name = "HAPROXY-EXTERNAL-PEERING-SG"
    BILLINGCODE = "FPE01903-CO-TE-TO-1000"
    BILLINGCONTACT = "marrojas@deloitte.ca"
    COUNTRY = "CA"
    CSCLASS = "Confidential"
    CSQUAL = "CI/PI Data"
    CSTYPE = "External"
    ENVIRONMENT = "NPD"
    FUNCTION = "CON"
    MEMBERFIRM = "CA"
    PRIMARYCONTACT = "marrojas@deloitte.ca"
    SECONDARYCONTACT = "Samir Modh"
  }
}

resource "aws_security_group" "SG_GUIDEWIRE" {
  name = "GUIDEWIRE-SG"

  ingress {
    from_port   = 8080
    to_port     = 8083
    protocol    = "tcp"
    security_groups = [
      "${aws_security_group.SG_INTERNAL_HAPROXY.id}"
    ]

    description = "HTTP - Internal"
  }

  ingress {
    from_port   = 8080
    to_port     = 8080
    protocol    = "tcp"
    security_groups = [
      "${aws_security_group.SG_EXTERNAL_HAPROXY.id}"
    ]

    description = "HTTP - External"
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    Name = "GUIDEWIRE-SG"
    BILLINGCODE = "FPE01903-CO-TE-TO-1000"
    BILLINGCONTACT = "marrojas@deloitte.ca"
    COUNTRY = "CA"
    CSCLASS = "Confidential"
    CSQUAL = "CI/PI Data"
    CSTYPE = "External"
    ENVIRONMENT = "NPD"
    FUNCTION = "CON"
    MEMBERFIRM = "CA"
    PRIMARYCONTACT = "marrojas@deloitte.ca"
    SECONDARYCONTACT = "Samir Modh"
  }
}

resource "aws_security_group" "SG_GUIDEWIRE_DEV" {
  name = "GUIDEWIRE-DEV-SG"

  ingress {
    from_port   = 8080
    to_port     = 8083
    protocol    = "tcp"
    security_groups = [
      "${aws_security_group.SG_INTERNAL_HAPROXY_DEV.id}"
    ]

    description = "HTTP - Internal"
  }

  ingress {
    from_port   = 8080
    to_port     = 8080
    protocol    = "tcp"
    security_groups = [
      "${aws_security_group.SG_EXTERNAL_HAPROXY_DEV.id}"
    ]

    description = "HTTP - External"
  }

  ingress {
    from_port   = 8080
    to_port     = 8083
    protocol    = "tcp"
    cidr_blocks = [
      "${var.bcaa_hub_cidr}"
    ]
    description = "bcaa hub cidr"
  }

  ingress {
    from_port = 1433
    to_port = 1433
    protocol = "tcp"
    description = "From AWS Workspaces"
    cidr_blocks = [
      "${var.bcaa_workspaces}"
    ]
  }
    ingress {
      from_port = 8080
      to_port = 8083
      protocol = "tcp"
      description = "From AWS Workspaces"
      cidr_blocks = [
        "${var.bcaa_workspaces}"
      ]
    }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    Name = "GUIDEWIRE-DEV-SG"
    BILLINGCODE = "FPE01903-CO-TE-TO-1000"
    BILLINGCONTACT = "marrojas@deloitte.ca"
    COUNTRY = "CA"
    CSCLASS = "Confidential"
    CSQUAL = "CI/PI Data"
    CSTYPE = "External"
    ENVIRONMENT = "NPD"
    FUNCTION = "CON"
    MEMBERFIRM = "CA"
    PRIMARYCONTACT = "marrojas@deloitte.ca"
    SECONDARYCONTACT = "Samir Modh"
  }
}

resource "aws_security_group" "SG_GUIDEWIRE_CLUSTER_NBIO_EVENT_QUEUE" {
  name = "GUIDEWIRE-CLUSTER-NBIO-EVENT-QUEUE-SG"

  ingress {
    from_port   = 40000
    to_port     = 40003
    protocol    = "tcp"
    security_groups = ["${aws_security_group.SG_GUIDEWIRE.id}"]
    description = "Cluster NBIO event queue"
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    Name = "GUIDEWIRE-CLUSTER-NBIO-EVENT-QUEUE-SG"
    BILLINGCODE = "FPE01903-CO-TE-TO-1000"
    BILLINGCONTACT = "marrojas@deloitte.ca"
    COUNTRY = "CA"
    CSCLASS = "Confidential"
    CSQUAL = "CI/PI Data"
    CSTYPE = "External"
    ENVIRONMENT = "NPD"
    FUNCTION = "CON"
    MEMBERFIRM = "CA"
    PRIMARYCONTACT = "marrojas@deloitte.ca"
    SECONDARYCONTACT = "Samir Modh"
  }
}

resource "aws_security_group" "SG_JENKINS_GUIDEWIRE_HEALTH_CHECK" {
  name = "JENKINS-GUIDEWIRE-HEALTH-CHECK-SG"

  ingress {
    from_port   = 8080
    to_port     = 8083
    protocol    = "tcp"
    security_groups = [
      "${aws_security_group.SG_JENKINS_CLIENT.id}"
    ]
    description = "Dev Jenkins Client Health Check"
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    Name = "JENKINS-GUIDEWIRE-HEALTH-CHECK-SG"
    BILLINGCODE = "FPE01903-CO-TE-TO-1000"
    BILLINGCONTACT = "marrojas@deloitte.ca"
    COUNTRY = "CA"
    CSCLASS = "Confidential"
    CSQUAL = "CI/PI Data"
    CSTYPE = "External"
    ENVIRONMENT = "NPD"
    FUNCTION = "CON"
    MEMBERFIRM = "CA"
    PRIMARYCONTACT = "marrojas@deloitte.ca"
    SECONDARYCONTACT = "Samir Modh"
  }
}

# resource "aws_security_group" "SG_CLIENT_CONVERSION_GW_SQL_DB" {
#   name = "CLIENT-CONVERSION-GW-SQL-DB-SG"

#   ingress {
#     from_port   = 1433
#     to_port     = 1433
#     protocol    = "tcp"
#     cidr_blocks = [
#       "10.10.10.19/32", # bcaa-HUBIO
#       "10.10.10.23/32", # bcaa-HUBIO02
#       "10.10.10.24/32" # bcaa-HUBIO03
#     ]
#   }

#   egress {
#     from_port   = 0
#     to_port     = 0
#     protocol    = "-1"
#     cidr_blocks = ["0.0.0.0/0"]
#   }

#   vpc_id = "${var.vpc_id}"

#   tags = {
#     Name = "CLIENT-CONVERSION-GW-SQL-DB-SG"
#   }
# }

resource "aws_security_group" "SG_OT_MSSQL_DB" {
  name = "OT-MSSQL-DB-SG"

  ingress {
    from_port   = 1433
    to_port     = 1433
    protocol    = "tcp"
    cidr_blocks = [
      "${var.bcaa_dev_cidr}",
      "${var.bcaa_aviatrix_vpn}"
    ]
  }

  ingress {
    from_port   = 1434
    to_port     = 1434
    protocol    = "udp"
    cidr_blocks = [
      "${var.bcaa_dev_cidr}",
      "${var.bcaa_aviatrix_vpn}"
    ]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    Name = "OT-MSSQL-DB-SG"
    BILLINGCODE = "FPE01903-CO-TE-TO-1000"
    BILLINGCONTACT = "marrojas@deloitte.ca"
    COUNTRY = "CA"
    CSCLASS = "Confidential"
    CSQUAL = "CI/PI Data"
    CSTYPE = "External"
    ENVIRONMENT = "NPD"
    FUNCTION = "CON"
    MEMBERFIRM = "CA"
    PRIMARYCONTACT = "marrojas@deloitte.ca"
    SECONDARYCONTACT = "Samir Modh"
  }
}

resource "aws_security_group" "SG_POSTGRES_DB" {
  name = "POSTGRES-DB-SG"

  ingress {
    from_port   = 5432
    to_port     = 5432
    protocol    = "tcp"
    cidr_blocks = [
      "${var.bcaa_dev_cidr}",
      "${var.bcaa_aviatrix_vpn}"
    ]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    Name = "POSTGRES-DB-SG"
    BILLINGCODE = "FPE01903-CO-TE-TO-1000"
    BILLINGCONTACT = "marrojas@deloitte.ca"
    COUNTRY = "CA"
    CSCLASS = "Confidential"
    CSQUAL = "CI/PI Data"
    CSTYPE = "External"
    ENVIRONMENT = "NPD"
    FUNCTION = "CON"
    MEMBERFIRM = "CA"
    PRIMARYCONTACT = "marrojas@deloitte.ca"
    SECONDARYCONTACT = "Samir Modh"
  }
}

resource "aws_security_group" "SG_MYSQL_DB" {
  name = "MYSQL-DB-SG"

  ingress {
    from_port   = 3306
    to_port     = 3306
    protocol    = "tcp"
    cidr_blocks = [
      "${var.bcaa_dev_cidr}",
      "${var.bcaa_hub_cidr}",
      "${var.bcaa_aviatrix_vpn}",
      "${var.bcaa_jenkins_client}",
      "${var.bcaa_dev01_microservices_cidr}"
    ]
  }

  ingress {
    from_port   = 3301
    to_port     = 3301
    protocol    = "tcp"
    cidr_blocks = [
      "${var.bcaa_dev_cidr}",
      "${var.bcaa_aviatrix_vpn}"
    ]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    Name = "MYSQL-DB-SG"
    BILLINGCODE = "FPE01903-CO-TE-TO-1000"
    BILLINGCONTACT = "marrojas@deloitte.ca"
    COUNTRY = "CA"
    CSCLASS = "Confidential"
    CSQUAL = "CI/PI Data"
    CSTYPE = "External"
    ENVIRONMENT = "NPD"
    FUNCTION = "CON"
    MEMBERFIRM = "CA"
    PRIMARYCONTACT = "marrojas@deloitte.ca"
    SECONDARYCONTACT = "Samir Modh"
  }
}

resource "aws_security_group" "SG_CLIENT_PING_TEST" {
  name = "CLIENT-PING-TEST"

  ingress {
    from_port       = -1
    to_port         = -1
    protocol        = "icmp"
    cidr_blocks     = [
      "${var.bcaa_cidr}",
      "${var.bcaa_watchguard_cidr}",
      "${var.bcaa_dmz_cidr}",
      "${var.bcaa_wireless_cidr}"
    ]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    Name = "CLIENT-PING-TEST"
    BILLINGCODE = "FPE01903-CO-TE-TO-1000"
    BILLINGCONTACT = "marrojas@deloitte.ca"
    COUNTRY = "CA"
    CSCLASS = "Confidential"
    CSQUAL = "CI/PI Data"
    CSTYPE = "External"
    ENVIRONMENT = "NPD"
    FUNCTION = "CON"
    MEMBERFIRM = "CA"
    PRIMARYCONTACT = "marrojas@deloitte.ca"
    SECONDARYCONTACT = "Samir Modh"
  }
}

resource "aws_security_group" "SG_SAP" {
  name = "SAP-SG"

  ingress {
    from_port       = 4444
    to_port         = 4444
    protocol        = "tcp"
    cidr_blocks = [
      "${var.bcaa_dev_cidr}",
      "${var.bcaa_aviatrix_vpn}"
    ]
  }

  ingress {
    from_port       = 4444
    to_port         = 4444
    protocol        = "udp"
    cidr_blocks = [
      "${var.bcaa_dev_cidr}",
      "${var.bcaa_aviatrix_vpn}"
    ]
  }

  ingress {
    from_port       = 4446
    to_port         = 4446
    protocol        = "tcp"
    cidr_blocks = [
      "${var.bcaa_dev_cidr}",
      "${var.bcaa_aviatrix_vpn}"
    ]
  }

  ingress {
    from_port       = 4446
    to_port         = 4446
    protocol        = "udp"
    cidr_blocks = [
      "${var.bcaa_dev_cidr}",
      "${var.bcaa_aviatrix_vpn}"
    ]
  }

  ingress {
    from_port       = 8080
    to_port         = 8080
    protocol        = "tcp"
    cidr_blocks = [
      "${var.bcaa_dev_cidr}",
      "${var.bcaa_aviatrix_vpn}"
    ]
  }

  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    "Name" = "SAP-SG"
    BILLINGCODE = "FPE01903-CO-TE-TO-1000"
    BILLINGCONTACT = "marrojas@deloitte.ca"
    COUNTRY = "CA"
    CSCLASS = "Confidential"
    CSQUAL = "CI/PI Data"
    CSTYPE = "External"
    ENVIRONMENT = "NPD"
    FUNCTION = "CON"
    MEMBERFIRM = "CA"
    PRIMARYCONTACT = "marrojas@deloitte.ca"
    SECONDARYCONTACT = "Samir Modh"
  }
}

resource "aws_security_group" "SG_HUBIO" {
  name = "HUBIO-SG"

  ingress {
    from_port   = 8080
    to_port     = 8080
    protocol    = "tcp"
    cidr_blocks = [
      "${var.bcaa_dev_cidr}",
      "${var.bcaa_aviatrix_vpn}"
    ]
  }

  ingress {
    from_port   = 9000
    to_port     = 9006
    protocol    = "tcp"
    cidr_blocks = [
      "${var.bcaa_dev_cidr}",
      "${var.bcaa_aviatrix_vpn}"
    ]
  }

  ingress {
    from_port   = 4200
    to_port     = 4200
    protocol    = "tcp"
    cidr_blocks = [
      "${var.bcaa_dev_cidr}",
      "${var.bcaa_aviatrix_vpn}"
    ]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    Name = "HUBIO-SG"
    BILLINGCODE = "FPE01903-CO-TE-TO-1000"
    BILLINGCONTACT = "marrojas@deloitte.ca"
    COUNTRY = "CA"
    CSCLASS = "Confidential"
    CSQUAL = "CI/PI Data"
    CSTYPE = "External"
    ENVIRONMENT = "NPD"
    FUNCTION = "CON"
    MEMBERFIRM = "CA"
    PRIMARYCONTACT = "marrojas@deloitte.ca"
    SECONDARYCONTACT = "Samir Modh"
  }
}

resource "aws_security_group" "SG_MONITORING" {
  name = "MONITORING-SG"

  ingress {
    from_port   = 10000
    to_port     = 10001
    protocol    = "tcp"
    cidr_blocks = [
      "${var.bcaa_dev_cidr}",
      "${var.bcaa_aviatrix_vpn}"
    ]
    description = "Prometheus/Alertmanager"
  }

  ingress {
    from_port   = 3000
    to_port     = 3000
    protocol    = "tcp"
    cidr_blocks = [
      "${var.bcaa_dev_cidr}",
      "${var.bcaa_aviatrix_vpn}"
    ]
    description = "Grafana"
  }

  ingress {
    from_port   = 19191
    to_port     = 19191
    protocol    = "tcp"
    cidr_blocks = [
      "${var.bcaa_dev01_microservices_cidr}"
    ]
    security_groups = [
      "${aws_security_group.SG_INTERNAL_HAPROXY.id}",
      "${aws_security_group.SG_INTERNAL_HAPROXY_DEV.id}"
    ]
    description = "Thanos UI"
  }

  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    Name = "MONITORING-SG"
    BILLINGCODE = "FPE01903-CO-TE-TO-1000"
    BILLINGCONTACT = "marrojas@deloitte.ca"
    COUNTRY = "CA"
    CSCLASS = "Confidential"
    CSQUAL = "CI/PI Data"
    CSTYPE = "External"
    ENVIRONMENT = "NPD"
    FUNCTION = "CON"
    MEMBERFIRM = "CA"
    PRIMARYCONTACT = "marrojas@deloitte.ca"
    SECONDARYCONTACT = "Samir Modh"
  }
}

resource "aws_security_group" "SG_AVIATRIX_RDP" {
  name = "AVIATRIX-RDP-SG"

  ingress {
    from_port   = 3389
    to_port     = 3389
    protocol    = "tcp"
    cidr_blocks = [
      "${var.bcaa_aviatrix_vpn}"
    ]
    description = "RDP (BCAA Aviatrix VPN)"
  }

  ingress {
    from_port   = 3389
    to_port     = 3389
    protocol    = "tcp"
    cidr_blocks = [ 
      "${var.bcaa_cidr}"
    ]
    description = "RDP (BCAA CIDR Range)"
  }

  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    Name = "AVIATRIX-RDP-SG"
    BILLINGCODE = "FPE01903-CO-TE-TO-1000"
    BILLINGCONTACT = "marrojas@deloitte.ca"
    COUNTRY = "CA"
    CSCLASS = "Confidential"
    CSQUAL = "CI/PI Data"
    CSTYPE = "External"
    ENVIRONMENT = "NPD"
    FUNCTION = "CON"
    MEMBERFIRM = "CA"
    PRIMARYCONTACT = "marrojas@deloitte.ca"
    SECONDARYCONTACT = "Samir Modh"
  }
}

resource "aws_security_group" "SG_AVIATRIX_SSH" {
  name = "AVIATRIX-SSH-SG"

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = [
      "${var.default_cidr}"
    ]
    description = "SSH (Default CIDR)"
  }

  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    Name = "AVIATRIX-SSH-SG"
    BILLINGCODE = "FPE01903-CO-TE-TO-1000"
    BILLINGCONTACT = "marrojas@deloitte.ca"
    COUNTRY = "CA"
    CSCLASS = "Confidential"
    CSQUAL = "CI/PI Data"
    CSTYPE = "External"
    ENVIRONMENT = "NPD"
    FUNCTION = "CON"
    MEMBERFIRM = "CA"
    PRIMARYCONTACT = "marrojas@deloitte.ca"
    SECONDARYCONTACT = "Samir Modh"
  }
}

resource "aws_security_group" "SG_JOBSCHEDULER_SSH" {
  name = "JOBSCHEDULER-SSH-SG"
 
 ingress {
    from_port       = 22
    to_port         = 22
    protocol        = "tcp"
    security_groups = [
      "${aws_security_group.SG_JOB_SCHEDULER.id}"
    ]
  }

  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    Name = "JOBSCHEDULER-SSH-SG"
    BILLINGCODE = "FPE01903-CO-TE-TO-1000"
    BILLINGCONTACT = "marrojas@deloitte.ca"
    COUNTRY = "CA"
    CSCLASS = "Confidential"
    CSQUAL = "CI/PI Data"
    CSTYPE = "External"
    ENVIRONMENT = "NPD"
    FUNCTION = "CON"
    MEMBERFIRM = "CA"
    PRIMARYCONTACT = "marrojas@deloitte.ca"
    SECONDARYCONTACT = "Samir Modh"
  }
}

resource "aws_security_group" "SG_JENKINS_CLIENT" {
  name = "JENKINS-CLIENT-SG"

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = [
      "10.0.0.0/8"
    ]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    Name = "JENKINS-CLIENT-SG"
    BILLINGCODE = "FPE01903-CO-TE-TO-1000"
    BILLINGCONTACT = "marrojas@deloitte.ca"
    COUNTRY = "CA"
    CSCLASS = "Confidential"
    CSQUAL = "CI/PI Data"
    CSTYPE = "External"
    ENVIRONMENT = "NPD"
    FUNCTION = "CON"
    MEMBERFIRM = "CA"
    PRIMARYCONTACT = "marrojas@deloitte.ca"
    SECONDARYCONTACT = "Samir Modh"
  }
}

resource "aws_security_group" "SG_JENKINS_CLIENT_SSH" {
  name = "JENKINS-CLIENT-SSH-SG"
 
  ingress {
    from_port       = 22
    to_port         = 22
    protocol        = "tcp"
    security_groups = [
      "${aws_security_group.SG_JENKINS_CLIENT.id}"
    ]
    description = "Jenkins Client SSH"
  }

  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    Name = "JENKINS-CLIENT-SSH-SG"
    BILLINGCODE = "FPE01903-CO-TE-TO-1000"
    BILLINGCONTACT = "marrojas@deloitte.ca"
    COUNTRY = "CA"
    CSCLASS = "Confidential"
    CSQUAL = "CI/PI Data"
    CSTYPE = "External"
    ENVIRONMENT = "NPD"
    FUNCTION = "CON"
    MEMBERFIRM = "CA"
    PRIMARYCONTACT = "marrojas@deloitte.ca"
    SECONDARYCONTACT = "Samir Modh"
  }
}

### DATA ###
resource "aws_security_group" "SG_BCAA_MSSQL_DB" {
  name = "BCAA-MSSQL-DB-SG"

  ingress {
    from_port = 1433
    to_port   = 1433
    protocol  = "tcp"

    cidr_blocks = ["${var.bcaa_cidr}"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    Name = "BCAA-MSSQL-DB-SG"
    BILLINGCODE = "FPE01903-CO-TE-TO-1000"
    BILLINGCONTACT = "marrojas@deloitte.ca"
    COUNTRY = "CA"
    CSCLASS = "Confidential"
    CSQUAL = "CI/PI Data"
    CSTYPE = "External"
    ENVIRONMENT = "NPD"
    FUNCTION = "CON"
    MEMBERFIRM = "CA"
    PRIMARYCONTACT = "marrojas@deloitte.ca"
    SECONDARYCONTACT = "Samir Modh"
  }
}

### Route53 Resolver - IC HUB DNS ###
resource "aws_security_group" "SG_IC_HUB_DNS_RESOLVER_ENDPOINT" {
  name = "IC-HUB-DNS-RESOLVER-ENDPOINT-SG"

  ingress {
    from_port   = 53
    to_port     = 53
    protocol    = "tcp"
    cidr_blocks = [
      "${var.vpc_cidr}"
    ]
    description = "DNS"
  }

  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
  }
  vpc_id = "${var.vpc_id}"

  tags = {
    Name = "IC-HUB-DNS-RESOLVER-ENDPOINT-SG"
  }
}
