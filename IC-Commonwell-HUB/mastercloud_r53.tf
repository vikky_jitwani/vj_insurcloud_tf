#=======================================================================
# HUB 
#=======================================================================
resource "aws_route53_record" "JENKINS_MASTER_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "jenkins.cmig.insurcloud.ca"
    type    = "A"

    alias {
        name                   = "internal-cicd-internal-alb-2023433918.ca-central-1.elb.amazonaws.com"
        zone_id                = "ZQSVJUPU6J1EY"
        evaluate_target_health = false
    }
}

resource "aws_route53_record" "JENKINS_DEV_CLIENT_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "jenkins-dev-client.cmig.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${aws_instance.ICCW_DEV_JENKINS_CLIENT.private_ip}"]
}

resource "aws_route53_record" "JENKINS_PROD_CLIENT_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "jenkins-prod-client.cmig.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${aws_instance.ICCW_PROD_JENKINS_CLIENT.private_ip}"]
}

resource "aws_route53_record" "NEXUS_REG_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "nexus.cmig.insurcloud.ca"
    type    = "A"

    alias {
        name                   = "internal-cicd-internal-alb-2023433918.ca-central-1.elb.amazonaws.com"
        zone_id                = "ZQSVJUPU6J1EY"
        evaluate_target_health = false
    }
}

resource "aws_route53_record" "NEXUS_PROD_REG_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "nexus-prod.cmig.insurcloud.ca"
    type    = "A"

    alias {
        name                   = "internal-cicd-internal-alb-2023433918.ca-central-1.elb.amazonaws.com"
        zone_id                = "ZQSVJUPU6J1EY"
        evaluate_target_health = false
    }
}

resource "aws_route53_record" "NEXUS_UI_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "nexus-ui.cmig.insurcloud.ca"
    type    = "A"

    alias {
        name                   = "internal-cicd-internal-alb-2023433918.ca-central-1.elb.amazonaws.com"
        zone_id                = "ZQSVJUPU6J1EY"
        evaluate_target_health = false
    }
}

resource "aws_route53_record" "SONARQUBE_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "sonarqube.cmig.insurcloud.ca"
    type    = "A"

    alias {
        name                   = "internal-cicd-internal-alb-2023433918.ca-central-1.elb.amazonaws.com"
        zone_id                = "ZQSVJUPU6J1EY"
        evaluate_target_health = false
    }
}

resource "aws_route53_record" "CONSUL_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "consul.cmig.insurcloud.ca"
    type    = "A"

    alias {
        name                   = "internal-cicd-internal-alb-2023433918.ca-central-1.elb.amazonaws.com"
        zone_id                = "ZQSVJUPU6J1EY"
        evaluate_target_health = false
    }
}
#=======================================================================
# DEV
#=======================================================================

resource "aws_route53_record" "DEV_INTERNAL_HA_PROXY_N1_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "internalhaproxydev1.cmig.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICCW_INTERNAL_HA_PROXY_N1_PRIVATEIP}"]
}

resource "aws_route53_record" "DEV_INTERNAL_HA_PROXY_N2_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "internalhaproxydev2.cmig.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICCW_INTERNAL_HA_PROXY_N2_PRIVATEIP}"]
}

resource "aws_route53_record" "DEV_EXTERNAL_HA_PROXY_N1_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "externalhaproxydev1.cmig.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICCW_EXTERNAL_HA_PROXY_N1_PRIVATEIP}"]
}

resource "aws_route53_record" "DEV_EXTERNAL_HA_PROXY_N2_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "externalhaproxydev2.cmig.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICCW_EXTERNAL_HA_PROXY_N2_PRIVATEIP}"]
}

resource "aws_route53_record" "GWPC_1_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "gwpc1dev01.cmig.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICCW_GUIDEWIRE_APPLICATION_1_PRIVATEIP}"]
}

resource "aws_route53_record" "GWPC_2_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "gwpc2dev01.cmig.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICCW_GUIDEWIRE_APPLICATION_2_PRIVATEIP}"]
}

resource "aws_route53_record" "GWCC_1_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "gwcc1dev01.cmig.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICCW_GUIDEWIRE_APPLICATION_1_PRIVATEIP}"]
}

resource "aws_route53_record" "GWCC_2_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "gwcc2dev01.cmig.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICCW_GUIDEWIRE_APPLICATION_2_PRIVATEIP}"]
}

resource "aws_route53_record" "GWCM_1_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "gwcm1dev01.cmig.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICCW_GUIDEWIRE_APPLICATION_1_PRIVATEIP}"]
}

resource "aws_route53_record" "GWCM_2_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "gwcm2dev01.cmig.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICCW_GUIDEWIRE_APPLICATION_2_PRIVATEIP}"]
}

resource "aws_route53_record" "GWBC_1_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "gwbc1dev01.cmig.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICCW_GUIDEWIRE_APPLICATION_1_PRIVATEIP}"]
}

resource "aws_route53_record" "GWBC_2_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "gwbc2dev01.cmig.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICCW_GUIDEWIRE_APPLICATION_2_PRIVATEIP}"]
}

resource "aws_route53_record" "GWDB_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "gwdbdev01.cmig.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICCW_GUIDEWIRE_DATABASE_PRIVATEIP}"]
}

resource "aws_route53_record" "GWDH_DB_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "datahubdb-dev.cmig.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["10.10.69.165"]
}

resource "aws_route53_record" "MICROSERVICES_1_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "ms1dev01.cmig.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICCW_MICROSERVICES_APPLICATION_1_PRIVATEIP}"]
}

resource "aws_route53_record" "MICROSERVICES_2_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "ms2dev01.cmig.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICCW_MICROSERVICES_APPLICATION_2_PRIVATEIP}"]
}

resource "aws_route53_record" "INTERNAL_NLB_DEV_GW_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "gw.pps.cmig.insurcloud.ca"
    type    = "A"
    alias {
        name                   = "${data.terraform_remote_state.dev_remote_state.INTERNAL_NLB_DNS_NAME}"
        zone_id                = "${data.terraform_remote_state.dev_remote_state.INTERNAL_NLB_ZONE_ID}"
        evaluate_target_health = false
    }
}

resource "aws_route53_record" "INTERNAL_NLB_DEV_MS_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "ms.pps.cmig.insurcloud.ca"
    type    = "A"
    alias {
        name                   = "${data.terraform_remote_state.dev_remote_state.INTERNAL_NLB_DNS_NAME}"
        zone_id                = "${data.terraform_remote_state.dev_remote_state.INTERNAL_NLB_ZONE_ID}"
        evaluate_target_health = false
    }
}

resource "aws_route53_record" "EXTERNAL_NLB_DEV_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "api-dev.cmig.insurcloud.ca"
    type    = "A"

    alias {
        name                   = "${data.terraform_remote_state.dev_remote_state.EXTERNAL_NLB_DNS_NAME}"
        zone_id                = "${data.terraform_remote_state.dev_remote_state.EXTERNAL_NLB_ZONE_ID}"
        evaluate_target_health = false
    }
}

resource "aws_route53_record" "DEV_EXSTREAM_COM_SERVER_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "exstreamserver-dev.cmig.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICCW_EXSTREAM_COM_SERVER_PRIVATEIP}"]
}

resource "aws_route53_record" "DEV_OT_CONTENT_SUITE_INDEX_SERVER_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "otcsindex-dev.cmig.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICCW_OT_CONTENT_SUITE_INDEX_SERVER_PRIVATEIP}"]
}

resource "aws_route53_record" "DEV_OT_CONTENT_SUITE_BRAVA_SERVER_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "otcsbrava-dev.cmig.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICCW_OT_CONTENT_SUITE_BRAVA_SERVER_PRIVATEIP}"]
}

resource "aws_route53_record" "DEV_EXSTREAM_DB_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "exstreamdb-dev.cmig.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["10.10.69.118"]
}

resource "aws_route53_record" "DEV_OT_DB_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "otdb-dev.cmig.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICCW_OT_DATABASE_PRIVATEIP}"]
}

resource "aws_route53_record" "DEV_MICROSERVICES_MANAGER_1_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "swarmmanagerdev1.cmig.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICCW_MICROSERVICES_MANAGER_1_PRIVATEIP}"]
}

resource "aws_route53_record" "DEV_MICROSERVICES_MANAGER_2_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "swarmmanagerdev2.cmig.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICCW_MICROSERVICES_MANAGER_2_PRIVATEIP}"]
}

resource "aws_route53_record" "DEV_MICROSERVICES_MANAGER_3_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "swarmmanagerdev3.cmig.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICCW_MICROSERVICES_MANAGER_3_PRIVATEIP}"]
}

##############################
resource "aws_route53_record" "DEV_HUBIO_SERVER_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "hubioregul8dev.cmig.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICCW_HUBIO_SERVER_PRIVATEIP}"]
}

resource "aws_route53_record" "DEV_INFOCENTER_DATAHUB_SAP_SERVER_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "dhicsapdev.cmig.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.prod_remote_state.ICCW_INFOCENTER_DATAHUB_SAP_SERVER_TEST_EC2_PRIVATEIP}"]
}

resource "aws_route53_record" "DEV_JOB_SCHEDULER_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "jobschedulerdev.cmig.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICCW_JOB_SCHEDULER_PRIVATEIP}"]
}

resource "aws_route53_record" "DEV_PROMETHEUS_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "prometheusdev.cmig.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["10.10.67.212"]
}

#=======================================================================
# Prod
#=======================================================================
resource "aws_route53_record" "PROD_INTERNAL_HA_PROXY_N1_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "internalhaproxy1.cmig.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.prod_remote_state.ICCW_PROD_INTERNAL_HA_PROXY_N1_PRIVATEIP}"]
}

resource "aws_route53_record" "PROD_INTERNAL_HA_PROXY_N2_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "internalhaproxy2.cmig.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.prod_remote_state.ICCW_PROD_INTERNAL_HA_PROXY_N2_PRIVATEIP}"]
}

resource "aws_route53_record" "PROD_EXTERNAL_HA_PROXY_N1_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "externalhaproxy1.cmig.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.prod_remote_state.ICCW_PROD_EXTERNAL_HA_PROXY_N1_PRIVATEIP}"]
}

resource "aws_route53_record" "PROD_EXTERNAL_HA_PROXY_N2_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "externalhaproxy2.cmig.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.prod_remote_state.ICCW_PROD_EXTERNAL_HA_PROXY_N2_PRIVATEIP}"]
}

resource "aws_route53_record" "INTERNAL_NLB_PROD_GW_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "gw.cmig.insurcloud.ca"
    type    = "A"
    alias {
        name                   = "${data.terraform_remote_state.prod_remote_state.INTERNAL_NLB_DNS_NAME}"
        zone_id                = "${data.terraform_remote_state.prod_remote_state.INTERNAL_NLB_ZONE_ID}"
        evaluate_target_health = false
    }
}

resource "aws_route53_record" "INTERNAL_NLB_PROD_GW_BATCH_1_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "gw-batch-1.cmig.insurcloud.ca"
    type    = "A"
    alias {
        name                   = "${data.terraform_remote_state.prod_remote_state.INTERNAL_NLB_DNS_NAME}"
        zone_id                = "${data.terraform_remote_state.prod_remote_state.INTERNAL_NLB_ZONE_ID}"
        evaluate_target_health = false
    }
}

resource "aws_route53_record" "INTERNAL_NLB_PROD_GW_BATCH_2_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "gw-batch-2.cmig.insurcloud.ca"
    type    = "A"
    alias {
        name                   = "${data.terraform_remote_state.prod_remote_state.INTERNAL_NLB_DNS_NAME}"
        zone_id                = "${data.terraform_remote_state.prod_remote_state.INTERNAL_NLB_ZONE_ID}"
        evaluate_target_health = false
    }
}

resource "aws_route53_record" "INTERNAL_NLB_PROD_GW_ONLINE_1_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "gw-online-1.cmig.insurcloud.ca"
    type    = "A"
    alias {
        name                   = "${data.terraform_remote_state.prod_remote_state.INTERNAL_NLB_DNS_NAME}"
        zone_id                = "${data.terraform_remote_state.prod_remote_state.INTERNAL_NLB_ZONE_ID}"
        evaluate_target_health = false
    }
}

resource "aws_route53_record" "INTERNAL_NLB_PROD_GW_ONLINE_2_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "gw-online-2.cmig.insurcloud.ca"
    type    = "A"
    alias {
        name                   = "${data.terraform_remote_state.prod_remote_state.INTERNAL_NLB_DNS_NAME}"
        zone_id                = "${data.terraform_remote_state.prod_remote_state.INTERNAL_NLB_ZONE_ID}"
        evaluate_target_health = false
    }
}

resource "aws_route53_record" "INTERNAL_NLB_PROD_MS_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "ms.cmig.insurcloud.ca"
    type    = "A"
    alias {
        name                   = "${data.terraform_remote_state.prod_remote_state.INTERNAL_NLB_DNS_NAME}"
        zone_id                = "${data.terraform_remote_state.prod_remote_state.INTERNAL_NLB_ZONE_ID}"
        evaluate_target_health = false
    }
}

resource "aws_route53_record" "EXTERNAL_NLB_PROD_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "external.cmig.insurcloud.ca"
    type    = "A"

    alias {
        name                   = "${data.terraform_remote_state.prod_remote_state.EXTERNAL_NLB_DNS_NAME}"
        zone_id                = "${data.terraform_remote_state.prod_remote_state.EXTERNAL_NLB_ZONE_ID}"
        evaluate_target_health = false
    }
}

resource "aws_route53_record" "PROD_GWDB_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "gwdb.cmig.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["10.10.52.230"]
}

resource "aws_route53_record" "PROD_MICROSERVICES_APP_1_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "ms1.cmig.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.prod_remote_state.ICCW_PROD_MICROSERVICES_APPLICATION_1_PRIVATEIP}"]
}

resource "aws_route53_record" "PROD_MICROSERVICES_APP_2_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "ms2.cmig.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.prod_remote_state.ICCW_PROD_MICROSERVICES_APPLICATION_2_PRIVATEIP}"]
}

##### PROD SHARED NODES ########
resource "aws_route53_record" "ICCW_PROD_GUIDEWIRE_ONLINE_1_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "gwo1.cmig.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.prod_remote_state.ICCW_PROD_GUIDEWIRE_ONLINE_1_PRIVATEIP}"]
}

resource "aws_route53_record" "ICCW_PROD_GUIDEWIRE_ONLINE_2_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "gwo2.cmig.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.prod_remote_state.ICCW_PROD_GUIDEWIRE_ONLINE_2_PRIVATEIP}"]
}

resource "aws_route53_record" "ICCW_PROD_GUIDEWIRE_BATCH_1_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "gwb1.cmig.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.prod_remote_state.ICCW_PROD_GUIDEWIRE_BATCH_1_PRIVATEIP}"]
}

resource "aws_route53_record" "ICCW_PROD_GUIDEWIRE_BATCH_2_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "gwb2.cmig.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.prod_remote_state.ICCW_PROD_GUIDEWIRE_BATCH_2_PRIVATEIP}"]
}

######### MS Manager ###########

resource "aws_route53_record" "PROD_MICROSERVICES_MANAGER_1_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "swarmmanager1.cmig.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.prod_remote_state.ICCW_PROD_MICROSERVICES_MANAGER_1_PRIVATEIP}"]
}

resource "aws_route53_record" "PROD_MICROSERVICES_MANAGER_2_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "swarmmanager2.cmig.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.prod_remote_state.ICCW_PROD_MICROSERVICES_MANAGER_2_PRIVATEIP}"]
}

resource "aws_route53_record" "PROD_MICROSERVICES_MANAGER_3_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "swarmmanager3.cmig.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.prod_remote_state.ICCW_PROD_MICROSERVICES_MANAGER_3_PRIVATEIP}"]
}

##############################
resource "aws_route53_record" "PROD_HUBIO_SERVER_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "hubioregul8.cmig.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.prod_remote_state.ICCW_PROD_HUBIO_SERVER_PRIVATEIP}"]
}

resource "aws_route53_record" "PROD_QLIK_SENSE_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "qliksense.cmig.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.prod_remote_state.ICCW_QLIK_SENSE_PRIVATEIP}"]
}

resource "aws_route53_record" "PROD_NPRINTING_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "nprinting.cmig.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.prod_remote_state.ICCW_NPRINTING_PRIVATEIP}"]
}

resource "aws_route53_record" "PROD_INFOCENTER_DATAHUB_SAP_SERVER_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "dhicsap.cmig.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.prod_remote_state.ICCW_INFOCENTER_DATAHUB_SAP_SERVER_PRIVATEIP}"]
}

resource "aws_route53_record" "PROD_JOB_SCHEDULER_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "jobscheduler.cmig.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.prod_remote_state.ICCW_PROD_JOB_SCHEDULER_PRIVATEIP}"]
}

resource "aws_route53_record" "PROD_PROMETHEUS_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "prometheus.cmig.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["10.10.51.74"]
}

resource "aws_route53_record" "PROD_GRAFANA_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "grafana.cmig.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["10.10.50.137"]
}

resource "aws_route53_record" "PROD_EXSTREAM_COM_SERVER_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "exstreamserver.cmig.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.prod_remote_state.ICCW_EXSTREAM_COM_SERVER_PRIVATEIP}"]
}

resource "aws_route53_record" "PROD_OT_CONTENT_SUITE_INDEX_SERVER_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "otcsindex.cmig.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.prod_remote_state.ICCW_OT_CONTENT_SUITE_INDEX_SERVER_PRIVATEIP}"]
}

resource "aws_route53_record" "PROD_OT_CONTENT_SUITE_BRAVA_SERVER_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "otcsbrava.cmig.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.prod_remote_state.ICCW_OT_CONTENT_SUITE_BRAVA_SERVER_PRIVATEIP}"]
}

resource "aws_route53_record" "PROD_EXSTREAM_DB_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "exstreamdb.cmig.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["10.10.53.56"]
}

resource "aws_route53_record" "PROD_GWDH_DB_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "datahubdb.cmig.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["10.10.52.222"]
}

#####################DEV/QA###########################

resource "aws_route53_record" "DEV_INTERNAL_HA_PROXY_DEV01_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "internalhaproxy.dev01.cmig.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICCW_INTERNAL_HA_PROXY_DEV01_PRIVATEIP}"]
}

resource "aws_route53_record" "DEV_EXTERNAL_HA_PROXY_DEV01_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "externalhaproxy.dev01.cmig.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICCW_EXTERNAL_HA_PROXY_DEV01_PRIVATEIP}"]
}

resource "aws_route53_record" "GW_DEV01_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "gw.dev01.cmig.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICCW_INTERNAL_HA_PROXY_DEV01_PRIVATEIP}"]
}

resource "aws_route53_record" "GWPC_DEV01_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "gwpc.dev01.cmig.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICCW_GUIDEWIRE_DEV01_PRIVATEIP}"]
}

resource "aws_route53_record" "GWBC_DEV01_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "gwbc.dev01.cmig.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICCW_GUIDEWIRE_DEV01_PRIVATEIP}"]
}

resource "aws_route53_record" "GWCC_DEV01_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "gwcc.dev01.cmig.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICCW_GUIDEWIRE_DEV01_PRIVATEIP}"]
}

resource "aws_route53_record" "GWCM_DEV01_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "gwcm.dev01.cmig.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICCW_GUIDEWIRE_DEV01_PRIVATEIP}"]
}

resource "aws_route53_record" "GWDB_DEV01_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "gwdb.dev01.cmig.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICCW_GUIDEWIRE_DEV01_PRIVATEIP}"]
}

resource "aws_route53_record" "MICROSERVICES_WORKER_DEV01_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "ms1.dev01.cmig.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICCW_MICROSERVICES_DEV01_PRIVATEIP}"]
}

resource "aws_route53_record" "MICROSERVICES_DEV01_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "ms.dev01.cmig.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICCW_INTERNAL_HA_PROXY_DEV01_PRIVATEIP}"]
}

resource "aws_route53_record" "MICROSERVICESDB_DEV01_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "msdb.dev01.cmig.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICCW_MICROSERVICES_DEV01_PRIVATEIP}"]
}

# TODO
resource "aws_route53_record" "VAULT_PPS_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "vault.pps.cmig.insurcloud.ca"
    type    = "A"

    alias {
        name                   = "a-mc-cac-d-a003-ic-pps-vault-84458cafb01e039e.elb.ca-central-1.amazonaws.com"
        zone_id                = "Z2EPGBW3API2WT"
        evaluate_target_health = false
    }
}

resource "aws_route53_record" "VAULT_PROD_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "vault.prod.cmig.insurcloud.ca"
    type    = "A"

    alias {
        name                   = "a-mc-cac-d-a003-ic-prod-vault-7b0fa62bf2511ffc.elb.ca-central-1.amazonaws.com"
        zone_id                = "Z2EPGBW3API2WT"
        evaluate_target_health = false
    }
}

