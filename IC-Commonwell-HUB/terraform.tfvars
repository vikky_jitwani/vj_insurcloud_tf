#Envrionment specific variables
client_name = "IC-Commonwell"
enviornment_name = "HUB"
enviornment_type = "NP"
client_number = "A003"
#ad-dc1 = "10.10.4.10"
#ad-dc2 = "10.10.6.10"
volume_key = "arn:aws:kms:ca-central-1:532382648425:key/36d3cec7-2957-4ebf-9b2c-4d15d7128ce9"
ami_id_amazonlinux201709base = "ami-0024f7a5443f60bce"
ami_id_amazonlinux201712base = "ami-03bcd20cb4070e488"
ami_id_amazonlinux201803base = "ami-033eb978e7ec03b77"
ami_id_amazonlinux2base = "ami-03501a4a3ff25cc1c"
ami_id_redhatlinux75 = "ami-0331f65e26647e9f7"
ami_id_windows2008base = "ami-03b6e09268e30fcca"
ami_id_windows2012base = "ami-0fa3e7b7730a2c849"
ami_id_windows2012R2base = "ami-0f2ef3ae4e127b482"
ami_id_windows2016base = "ami-01b912743b28aabf9"
ami_id_ubuntu1604baseimage = "ami-0c5d6a28a2ade058b"
ami_id_ubuntu1804baseimage = "ami-0fa8da332170c5e8e"
ami_id_rhel75baseimage = "ami-49f0762d"

#VPC Specific variables
vpc_id = "vpc-01c39132b104e1b86"
vpc_cidr = "10.10.4.0/22"
#sg_cidr ="10.10.4.0/24"

commonwell-hub-database-1a-subnet = "subnet-0328397102b3fab5d"
commonwell-hub-database-1b-subnet = "subnet-07f63c52670531a68"
commonwell-hub-private-1a-subnet = "subnet-05f793fd91e767ba9"
commonwell-hub-private-1b-subnet = "subnet-018b484cdc29954f0"
commonwell-hub-public-1a-subnet = "subnet-0441dc508d8e7149a"
commonwell-hub-public-1b-subnet = "subnet-072f10cb7e2f634b5"

#R53 
zone_id = "Z05730741M2OP9QKWLS28"

#RDS
mssql-rds-password = "c0mm0nwe11"
mysql-rds-password = "c0mm0nwe11"
postgres-rds-password = "c0mm0nwe11"
conduit-rds-password = "postgres"