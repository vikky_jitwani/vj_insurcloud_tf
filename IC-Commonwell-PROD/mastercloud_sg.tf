
#GUIDEWIRE SHARED NODES SECURITY GROUPS 
resource "aws_security_group" "SG_GUIDEWIRE" {
  name = "GUIDEWIRE-SG"

  ingress {
    from_port   = 8080
    to_port     = 8083
    protocol    = "tcp"
    security_groups = [
      "${aws_security_group.SG_INTERNAL_HAPROXY.id}"
    ]

    description = "HTTP - Internal"
  }

  ingress {
    from_port   = 8080
    to_port     = 8080
    protocol    = "tcp"
    security_groups = [
      "${aws_security_group.SG_EXTERNAL_HAPROXY.id}"
    ]

    description = "HTTP - External for PC"
  }

  ingress {
    from_port   = 8082
    to_port     = 8082
    protocol    = "tcp"
    security_groups = [
      "${aws_security_group.SG_EXTERNAL_HAPROXY.id}"
    ]

    description = "HTTP - External for CC" 
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    Name = "GUIDEWIRE-SG"
  }
}

resource "aws_security_group" "SG_GUIDEWIRE_CLUSTER_NBIO_EVENT_QUEUE" {
  name = "GUIDEWIRE-CLUSTER-NBIO-EVENT-QUEUE-SG"

  ingress {
    from_port   = 40000
    to_port     = 40003
    protocol    = "tcp"
    security_groups = ["${aws_security_group.SG_GUIDEWIRE.id}"]
    description = "Cluster NBIO event queue"
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    Name = "GUIDEWIRE-CLUSTER-NBIO-EVENT-QUEUE-SG"
  }
}

resource "aws_security_group" "SG_JENKINS_CLIENT_SSH" {

  name = "JENKINS-CLIENT-SSH-SG"
 
  ingress {
    from_port       = 22
    to_port         = 22
    protocol        = "tcp"
    cidr_blocks = ["10.10.4.15/32"]
    description = "Jenkins Client SSH"
  }

  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    Name = "JENKINS-CLIENT-SSH-SG"
  }
}

resource "aws_security_group" "SG_AVIATRIX_SSH" {
  name = "COMMONWELL-AVIATRIX-SSH-SG"

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = [
      "${var.commonwell_aviatrix_vpn_1a}",
      "${var.commonwell_aviatrix_vpn_1b}"
    ]
    description = "SSH"
  }

  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    Name = "COMMONWELL-AVIATRIX-SSH-SG"
  }
}

resource "aws_security_group" "SG_JOB_SCHEDULER" {
  name = "COMMONWELL-JOB-SCHEDULER-SG"

  ingress {
    from_port       = 4444
    to_port         = 4444
    protocol        = "tcp"
    cidr_blocks = [
      "${var.deloitte_cidr}",
      "${var.hub_cidr}",
      "${var.commonwell_aws_cidr}",
      "${var.commonwell_lindsay_cidr}",
      "${var.commonwell_whitby_cidr}"
    ]
  }

  ingress {
    from_port       = 4444
    to_port         = 4444
    protocol        = "udp"
    cidr_blocks = [
      "${var.deloitte_cidr}",
      "${var.hub_cidr}",
      "${var.commonwell_aws_cidr}",
      "${var.commonwell_lindsay_cidr}",
      "${var.commonwell_whitby_cidr}"
    ]
  }

  ingress {
    from_port       = 4446
    to_port         = 4446
    protocol        = "tcp"
    cidr_blocks = [
      "${var.deloitte_cidr}",
      "${var.hub_cidr}",
      "${var.commonwell_aws_cidr}",
      "${var.commonwell_lindsay_cidr}",
      "${var.commonwell_whitby_cidr}"
    ]
  }

  ingress {
    from_port       = 4446
    to_port         = 4446
    protocol        = "udp"
    cidr_blocks = [
      "${var.deloitte_cidr}",
      "${var.hub_cidr}",
      "${var.commonwell_aws_cidr}",
      "${var.commonwell_lindsay_cidr}",
      "${var.commonwell_whitby_cidr}"
    ]
  }

  ingress {
    from_port       = 22
    to_port         = 22
    protocol        = "tcp"
    cidr_blocks = [
      "${var.deloitte_cidr}",
      "${var.hub_cidr}",
      "${var.commonwell_aws_cidr}",
      "${var.commonwell_lindsay_cidr}",
      "${var.commonwell_whitby_cidr}"
    ]
  }

  ingress {
    from_port       = 443
    to_port         = 443
    protocol        = "tcp"
    cidr_blocks = [
      "${var.deloitte_cidr}",
      "${var.hub_cidr}"
    ]
  }

  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    Name = "COMMONWELL-JOB-SCHEDULER-SG"
  }
}

resource "aws_security_group" "SG_COMMONWELL_MICROSERVICES" {
  name = "COMMONWELL-MICROSERVICES-SG"

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = [
      "${var.hub_cidr}"
    ]
  }

  ingress {
    from_port   = 8443
    to_port     = 8443
    protocol    = "tcp"
    security_groups = [
      "${aws_security_group.SG_EXTERNAL_HAPROXY.id}"
    ]
    description = "Orchestrator"
  }

  ingress {
    from_port   = 15672
    to_port     = 15672
    protocol    = "tcp"
    cidr_blocks = [
      "${var.hub_cidr}"
    ]
    security_groups = [
      "${aws_security_group.SG_INTERNAL_HAPROXY.id}"
    ]
    description = "RabbitMQ Management UI"
  }

  ingress {
    from_port   = 9002
    to_port     = 9002
    protocol    = "tcp"
    security_groups = [
      "${aws_security_group.SG_INTERNAL_HAPROXY.id}"
    ]
    description = "Edge"
  }

  ingress {
    from_port   = 9222
    to_port     = 9222
    protocol    = "tcp"
    security_groups = [
      "${aws_security_group.SG_EXTERNAL_HAPROXY.id}",
      "${aws_security_group.SG_INTERNAL_HAPROXY.id}"
    ]
    description = "Audatex"
  }

  ingress {
    from_port   = 2377
    to_port     = 2377
    protocol    = "tcp"
    cidr_blocks = [
      "${var.deloitte_cidr}"
    ]
    description = "Cluster Management Communications"
  }

  ingress {
    from_port   = 7946
    to_port     = 7946
    protocol    = "udp"
    cidr_blocks = [
      "${var.deloitte_cidr}"
    ]
    description = "Communication Among Nodes"
  }

  ingress {
    from_port   = 7946
    to_port     = 7946
    protocol    = "tcp"
    cidr_blocks = [
      "${var.deloitte_cidr}"
    ]
    description = "Communication Among Nodes"
  }

  ingress {
    from_port   = 4789
    to_port     = 4789
    protocol    = "udp"
    cidr_blocks = [
      "${var.deloitte_cidr}"
    ]
    description = "Overlay Network Traffic"
  }

  ingress {
    from_port   = 8500
    to_port     = 8500
    protocol    = "tcp"
    cidr_blocks = [
      "${var.hub_cidr}"
    ]
    description = "Consul UI"
  }

  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    Name = "COMMONWELL-MICROSERVICES-SG"
  }
}

resource "aws_security_group" "SG_COMMONWELL_EXSTREAM" {
  name = "COMMONWELL-EXSTREAM-SG"

  ingress {
    from_port   = 3389
    to_port     = 3389
    protocol    = "tcp"
    cidr_blocks = ["${var.default_cidr}"]
  }

  ingress {
    from_port   = 28700
    to_port     = 28702
    protocol    = "tcp"
    cidr_blocks = ["${var.default_cidr}"]
  }

  ingress {
    from_port   = 2701
    to_port     = 2702
    protocol    = "tcp"
    cidr_blocks = ["${var.default_cidr}"]
  }

  ingress {
    from_port   = 8080
    to_port     = 8080
    protocol    = "tcp"
    cidr_blocks = ["${var.default_cidr}"]
  }

  ingress {
    from_port   = 8443
    to_port     = 8443
    protocol    = "tcp"
    cidr_blocks = ["${var.default_cidr}"]
  }

  ingress {
    from_port   = 1433
    to_port     = 1433
    protocol    = "tcp"
    cidr_blocks = ["${var.default_cidr}"]
  }

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["${var.default_cidr}"]
    security_groups = [
      "${aws_security_group.SG_GUIDEWIRE.id}"
    ]
  }

  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["${var.default_cidr}"]
  }

  ingress {
    from_port   = 5858
    to_port     = 5858
    protocol    = "tcp"
    cidr_blocks = ["${var.default_cidr}"]
  }

  ingress {
    from_port   = 3099
    to_port     = 3099
    protocol    = "tcp"
    cidr_blocks = ["${var.default_cidr}"]
  }

  ingress {
    from_port   = 2099
    to_port     = 2099
    protocol    = "tcp"
    cidr_blocks = ["${var.default_cidr}"]
  }

  ingress {
    from_port   = 389
    to_port     = 389
    protocol    = "tcp"
    cidr_blocks = ["${var.default_cidr}"]
  }

  ingress {
    from_port   = 4440
    to_port     = 4440
    protocol    = "tcp"
    cidr_blocks = ["${var.default_cidr}"]
  }

  ingress {
    from_port   = 8009
    to_port     = 8009
    protocol    = "tcp"
    cidr_blocks = ["${var.default_cidr}"]
  }

  ingress {
    from_port   = 4040
    to_port     = 4040
    protocol    = "tcp"
    cidr_blocks = ["${var.default_cidr}"]
  }

  ingress {
    from_port   = 8989
    to_port     = 8989
    protocol    = "tcp"
    cidr_blocks = ["${var.default_cidr}"]
  }

  ingress {
    from_port   = 41616
    to_port     = 41616
    protocol    = "tcp"
    cidr_blocks = ["${var.default_cidr}"]
  }

  ingress {
    from_port   = 5868
    to_port     = 5869
    protocol    = "tcp"
    cidr_blocks = ["${var.default_cidr}"]
  }

  ingress {
    from_port   = 8100
    to_port     = 8100
    protocol    = "tcp"
    cidr_blocks = ["${var.default_cidr}"]
  }

  ingress {
    from_port   = 8300
    to_port     = 8300
    protocol    = "tcp"
    cidr_blocks = ["${var.default_cidr}"]
  }

  ingress {
    from_port   = 8500
    to_port     = 8500
    protocol    = "tcp"
    cidr_blocks = ["${var.default_cidr}"]
  }

  ingress {
    from_port   = 8600
    to_port     = 8603
    protocol    = "tcp"
    cidr_blocks = ["${var.default_cidr}"]
  }

  ingress {
    from_port   = 8512
    to_port     = 8515
    protocol    = "tcp"
    cidr_blocks = ["${var.default_cidr}"]
  }

  ingress {
    from_port   = 2719
    to_port     = 2719
    protocol    = "tcp"
    cidr_blocks = ["${var.default_cidr}"]
  }

  ingress {
    from_port   = 8888
    to_port     = 8888
    protocol    = "tcp"
    cidr_blocks = ["${var.default_cidr}"]
    description = "System Center"
  }

  ingress {
    from_port   = 135
    to_port     = 139
    protocol    = "tcp"
    cidr_blocks = ["${var.deloitte_cidr}"]
  }

  ingress {
    from_port   = 135
    to_port     = 139
    protocol    = "udp"
    cidr_blocks = ["${var.deloitte_cidr}"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    Name = "COMMONWELL-EXSTREAM-SG"
  }
}

resource "aws_security_group" "SG_EXTERNAL_HAPROXY" {
  name = "COMMONWELL-EXTERNAL-HAPROXY-SG"

  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = [
      "${var.deloitte_cidr}",
      "${var.hub_cidr}",
      "${var.commonwell_aws_cidr}",
      "${var.commonwell_lindsay_cidr}",
      "${var.commonwell_whitby_cidr}"
    ]
    description = "HTTPS"
  }

  ingress {
    from_port   = 8444
    to_port     = 8444
    protocol    = "tcp"
    cidr_blocks = [
      "${var.deloitte_cidr}"
    ]
    description = "Health"
  }

  ingress {
    from_port   = 8404
    to_port     = 8404
    protocol    = "tcp"
    cidr_blocks = [
      "${var.deloitte_cidr}",
      "${var.hub_cidr}"
    ]
    description = "Stats"
  }

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = [
      "${var.deloitte_cidr}",
      "${var.hub_cidr}"
    ]
    description = "SSH"
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    Name = "COMMONWELL-EXTERNAL-HAPROXY-SG"
  }
}

resource "aws_security_group" "SG_INTERNAL_HAPROXY" {
  name = "COMMONWELL-INTERNAL-HAPROXY-SG"

  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = [
      "${var.deloitte_cidr}",
      "${var.hub_cidr}",
      "${var.commonwell_aws_cidr}",
      "${var.commonwell_lindsay_cidr}",
      "${var.commonwell_whitby_cidr}"
    ]
    description = "HTTPS"
  }

  ingress {
    from_port   = 8444
    to_port     = 8444
    protocol    = "tcp"
    cidr_blocks = [
      "${var.deloitte_cidr}"
    ]
    description = "Health"
  }

  ingress {
    from_port   = 8404
    to_port     = 8404
    protocol    = "tcp"
    cidr_blocks = [
      "${var.deloitte_cidr}",
      "${var.hub_cidr}"
    ]
    description = "Stats"
  }

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = [
      "${var.hub_cidr}"
    ]
    description = "SSH"
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    Name = "COMMONWELL-INTERNAL-HAPROXY-SG"
  }
}

resource "aws_security_group" "SG_HAPROXY_INTERNAL_PEERING" {
  name = "COMMONWELL-HAPROXY-INTERNAL-PEERING-SG"

  ingress {
    from_port       = 1024
    to_port         = 1024
    protocol        = "tcp"
    security_groups = ["${aws_security_group.SG_INTERNAL_HAPROXY.id}"]
    description     = "HAProxy Peering"
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    Name = "COMMONWELL-HAPROXY-INTERNAL-PEERING-SG"
  }
}

resource "aws_security_group" "SG_HAPROXY_EXTERNAL_PEERING" {
  name = "COMMONWELL-HAPROXY-EXTERNAL-PEERING-SG"

  ingress {
    from_port       = 1024
    to_port         = 1024
    protocol        = "tcp"
    security_groups = ["${aws_security_group.SG_EXTERNAL_HAPROXY.id}"]
    description     = "HAProxy Peering"
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    Name = "COMMONWELL-HAPROXY-EXTERNAL-PEERING-SG"
  }
}

resource "aws_security_group" "SG_JENKINS_GUIDEWIRE_HEALTH_CHECK" {
  name = "COMMONWELL-JENKINS-GUIDEWIRE-HEALTH-CHECK-SG"

  ingress {
    from_port   = 8080
    to_port     = 8083
    protocol    = "tcp"
    cidr_blocks = ["10.10.4.15/32"]
    description = "Prod Jenkins Client Health Check"
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    Name = "COMMONWELL-JENKINS-GUIDEWIRE-HEALTH-CHECK-SG"
  }
}

resource "aws_security_group" "SG_MSSQL_DB" {
  name = "COMMONWELL-MSSQL-DB-SG"

  ingress {
    from_port   = 1433
    to_port     = 1433
    protocol    = "tcp"
    cidr_blocks = [
      "${var.deloitte_cidr}",
      "${var.hub_cidr}",
      "${var.commonwell_aws_cidr}",
      "${var.commonwell_lindsay_cidr}",
      "${var.commonwell_whitby_cidr}"
    ]
    # Should narrow down to just instances with Guidewire SG
  }

  ingress {
    from_port   = 3389
    to_port     = 3389
    protocol    = "tcp"
    cidr_blocks = [
      "${var.deloitte_cidr}",
      "${var.hub_cidr}",
      "${var.commonwell_aws_cidr}",
      "${var.commonwell_lindsay_cidr}",
      "${var.commonwell_whitby_cidr}"
    ]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    Name = "COMMONWELL-MSSQL-DB-SG"
  }
}

resource "aws_security_group" "SG_OT_MSSQL_DB" {
  name = "COMMONWELL-OT-MSSQL-DB-SG"

  ingress {
    from_port   = 1433
    to_port     = 1433
    protocol    = "tcp"
    cidr_blocks = [
      "${var.deloitte_cidr}",
      "${var.hub_cidr}",
      "${var.commonwell_aws_cidr}",
      "${var.commonwell_lindsay_cidr}",
      "${var.commonwell_whitby_cidr}"
    ]
    # Should narrow down to just instances with Guidewire SG
  }

  ingress {
    from_port   = 1434
    to_port     = 1434
    protocol    = "udp"
    cidr_blocks = [
      "${var.deloitte_cidr}",
      "${var.hub_cidr}",
      "${var.commonwell_aws_cidr}",
      "${var.commonwell_lindsay_cidr}",
      "${var.commonwell_whitby_cidr}"
    ]
  }

  ingress {
    from_port   = 3389
    to_port     = 3389
    protocol    = "tcp"
    cidr_blocks = [
      "${var.deloitte_cidr}",
      "${var.hub_cidr}",
      "${var.commonwell_aws_cidr}",
      "${var.commonwell_lindsay_cidr}",
      "${var.commonwell_whitby_cidr}"
    ]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    Name = "COMMONWELL-OT-MSSQL-DB-SG"
  }
}

resource "aws_security_group" "SG_POSTGRES_DB" {
  name = "COMMONWELL-POSTGRES-DB-SG"

  ingress {
    from_port   = 5432
    to_port     = 5432
    protocol    = "tcp"
    cidr_blocks = [
      "${var.deloitte_cidr}",
      "${var.hub_cidr}",
      "${var.commonwell_aws_cidr}",
      "${var.commonwell_lindsay_cidr}",
      "${var.commonwell_whitby_cidr}"
    ]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    Name = "COMMONWELL-POSTGRES-DB-SG"
  }
}

resource "aws_security_group" "SG_MYSQL_DB" {
  name = "COMMONWELL-MYSQL-DB-SG"

  ingress {
    from_port   = 3306
    to_port     = 3306
    protocol    = "tcp"
    cidr_blocks = [
      "${var.deloitte_cidr}",
      "${var.hub_cidr}",
      "${var.commonwell_aws_cidr}",
      "${var.commonwell_lindsay_cidr}",
      "${var.commonwell_whitby_cidr}"
    ]
  }

  ingress {
    from_port   = 3301
    to_port     = 3301
    protocol    = "tcp"
    cidr_blocks = [
      "${var.deloitte_cidr}",
      "${var.hub_cidr}",
      "${var.commonwell_aws_cidr}",
      "${var.commonwell_lindsay_cidr}",
      "${var.commonwell_whitby_cidr}"
    ]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    Name = "COMMONWELL-MYSQL-DB-SG"
  }
}

resource "aws_security_group" "SG_PING_TEST" {
  name = "COMMONWELL-PING-TEST-SG"

  ingress {
    from_port       = -1
    to_port         = -1
    protocol        = "icmp"
    cidr_blocks     = "${var.default_cidr}"
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    Name = "COMMONWELL-PING-TEST-SG"
  }
}

resource "aws_security_group" "SG_HUBIO" {
  name = "A-MC-CAC-D-A003-COMMONWELL-HUBIO-SG"

  ingress {
    from_port   = 8080
    to_port     = 8080
    protocol    = "tcp"
    cidr_blocks = [
      "${var.deloitte_cidr}",
      "${var.hub_cidr}",
      "${var.commonwell_aws_cidr}",
      "${var.commonwell_lindsay_cidr}",
      "${var.commonwell_whitby_cidr}"
    ]
  }

  ingress {
    from_port   = 3389
    to_port     = 3389
    protocol    = "tcp"
    cidr_blocks = [
      "${var.deloitte_cidr}",
      "${var.hub_cidr}",
      "${var.commonwell_aws_cidr}",
      "${var.commonwell_lindsay_cidr}",
      "${var.commonwell_whitby_cidr}"
    ]
  }

  ingress {
    from_port   = 4200
    to_port     = 4200
    protocol    = "tcp"
    cidr_blocks = "${var.default_cidr}"
  }

  ingress {
    from_port   = 9000
    to_port     = 9006
    protocol    = "tcp"
    cidr_blocks = "${var.default_cidr}"
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    Name = "COMMONWELL-HUBIO-SG"
  }
}

resource "aws_security_group" "SG_DEV_SAP_DB_ACCESS" {
  name = "A-MC-CAC-D-A003-COMMONWELL-DEV-SAP-DB-ACCESS-SG"

  ingress {
    from_port   = 1433
    to_port     = 1433
    protocol    = "tcp"
    cidr_blocks = ["10.10.70.5/32"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    Name = "COMMONWELL-DEV-SAP-DB-ACCESS-SG"
  }
}

resource "aws_security_group" "SG_QLIKSENSE" {
  name = "COMMONWELL-QLIKSENSE-SG"

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = [
      "${var.deloitte_cidr}",
      "${var.hub_cidr}",
      "${var.commonwell_aws_cidr}",
      "${var.commonwell_lindsay_cidr}",
      "${var.commonwell_whitby_cidr}"
    ]
  }

  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = [
      "${var.deloitte_cidr}",
      "${var.hub_cidr}",
      "${var.commonwell_aws_cidr}",
      "${var.commonwell_lindsay_cidr}",
      "${var.commonwell_whitby_cidr}"
    ]
  }

  ingress {
    from_port   = 3389
    to_port     = 3389
    protocol    = "tcp"
    cidr_blocks = [
      "${var.hub_cidr}",
      "${var.commonwell_lindsay_cidr}"
    ]
  }

  ingress {
    from_port   = 4244
    to_port     = 4244
    protocol    = "tcp"
    cidr_blocks = [
      "${var.deloitte_cidr}",
      "${var.hub_cidr}",
      "${var.commonwell_aws_cidr}",
      "${var.commonwell_lindsay_cidr}",
      "${var.commonwell_whitby_cidr}"
    ]
  }

  ingress {
    from_port   = 4248
    to_port     = 4248
    protocol    = "tcp"
    cidr_blocks = [
      "${var.deloitte_cidr}",
      "${var.hub_cidr}",
      "${var.commonwell_aws_cidr}",
      "${var.commonwell_lindsay_cidr}",
      "${var.commonwell_whitby_cidr}"
    ]
  }

  ingress {
    from_port   = 4747
    to_port     = 4747
    protocol    = "tcp"
    cidr_blocks = [
      "${var.deloitte_cidr}"
    ]
    description = "Qlik/NPrinting Application"
  }

  ingress {
    from_port   = 4997
    to_port     = 4997
    protocol    = "tcp"
    cidr_blocks = [
      "${var.deloitte_cidr}"
    ]
    description = "Qlik/NPrinting Application"
  }

  ingress {
    from_port   = 4242
    to_port     = 4243
    protocol    = "tcp"
    cidr_blocks = [
      "${var.deloitte_cidr}"
    ]
    description = "Qlik/NPrinting Application"
  }

  ingress {
    from_port   = 4343
    to_port     = 4343
    protocol    = "tcp"
    cidr_blocks = [
      "${var.deloitte_cidr}"
    ]
    description = "Qlik/NPrinting Application"
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    Name = "COMMONWELL-QLIKSENSE-SG"
  }
}

resource "aws_security_group" "SG_NPRINTING" {
  name = "COMMONWELL-NPRINTING-SG"

  ingress {
    from_port   = 4242
    to_port     = 4243
    protocol    = "tcp"
    cidr_blocks = [
      "${var.default_cidr}"
    ]
    description = "QRS/QPS REST API"
  }

  ingress {
    from_port   = 4242
    to_port     = 4243
    protocol    = "udp"
    cidr_blocks = [
      "${var.default_cidr}"
    ]
    description = "QRS/QPS REST API"
  }

  ingress {
    from_port   = 4747
    to_port     = 4747
    protocol    = "tcp"
    cidr_blocks = [
      "${var.default_cidr}"
    ]
    description = "QES/QVS"
  }

  ingress {
    from_port   = 4747
    to_port     = 4747
    protocol    = "udp"
    cidr_blocks = [
      "${var.default_cidr}"
    ]
    description = "QES/QVS"
  }

  ingress {
    from_port   = 4992
    to_port     = 4996
    protocol    = "tcp"
    cidr_blocks = [
      "${var.default_cidr}"
    ]
    description = "Repository Service/Qlik NPriting Web console proxy/NewsStand proxy/Windows Authentication Service/Web engine service"
  }

  ingress {
    from_port   = 4992
    to_port     = 4996
    protocol    = "udp"
    cidr_blocks = [
      "${var.default_cidr}"
    ]
    description = "Repository Service/Qlik NPriting Web console proxy/NewsStand proxy/Windows Authentication Service/Web engine service"
  }

  ingress {
    from_port   = 5672
    to_port     = 5672
    protocol    = "tcp"
    cidr_blocks = [
      "${var.default_cidr}"
    ]
    description = "Qlik NPrinting messaging service"
  }

  ingress {
    from_port   = 5672
    to_port     = 5672
    protocol    = "udp"
    cidr_blocks = [
      "${var.default_cidr}"
    ]
    description = "Qlik NPrinting messaging service"
  }

  ingress {
    from_port   = 15672
    to_port     = 15672
    protocol    = "tcp"
    cidr_blocks = [
      "${var.default_cidr}"
    ]
    description = "Qlik NPrinting messaging service management plugin"
  }

  ingress {
    from_port   = 15672
    to_port     = 15672
    protocol    = "udp"
    cidr_blocks = [
      "${var.default_cidr}"
    ]
    description = "Qlik NPrinting messaging service management plugin"
  }

  ingress {
    from_port   = 4730
    to_port     = 4730
    protocol    = "tcp"
    cidr_blocks = [
      "${var.default_cidr}"
    ]
    description = "QlikView DSC service"
  }

  ingress {
    from_port   = 4730
    to_port     = 4730
    protocol    = "udp"
    cidr_blocks = [
      "${var.default_cidr}"
    ]
    description = "QlikView DSC service"
  }

  ingress {
    from_port   = 4799
    to_port     = 4799
    protocol    = "tcp"
    cidr_blocks = [
      "${var.default_cidr}"
    ]
    description = "QlikView Management Service"
  }

  ingress {
    from_port   = 4799
    to_port     = 4799
    protocol    = "udp"
    cidr_blocks = [
      "${var.default_cidr}"
    ]
    description = "QlikView Management Service"
  }

  ingress {
    from_port   = 4997
    to_port     = 4997
    protocol    = "tcp"
    cidr_blocks = [
      "${var.default_cidr}"
    ]
    description = "Engine TLS certificates exchange server"
  }

  ingress {
    from_port   = 4997
    to_port     = 4997
    protocol    = "udp"
    cidr_blocks = [
      "${var.default_cidr}"
    ]
    description = "Engine TLS certificates exchange server"
  }

  ingress {
    from_port   = 3389
    to_port     = 3389
    protocol    = "tcp"
    cidr_blocks = [
      "${var.hub_cidr}"
    ]
  }

  ingress {
    from_port   = 21
    to_port     = 21
    protocol    = "tcp"
    cidr_blocks = [
      "${var.hub_cidr}",
      "${var.commonwell_aws_cidr}",
      "${var.commonwell_lindsay_cidr}",
      "${var.commonwell_whitby_cidr}"
    ]
    description = "FTPS"
  }

  ingress {
    from_port   = 990
    to_port     = 990
    protocol    = "tcp"
    cidr_blocks = [
      "${var.hub_cidr}",
      "${var.commonwell_aws_cidr}",
      "${var.commonwell_lindsay_cidr}",
      "${var.commonwell_whitby_cidr}"
    ]
    description = "FTPS"
  }

  ingress {
    from_port   = 5000
    to_port     = 5100
    protocol    = "tcp"
    cidr_blocks = [
      "${var.hub_cidr}",
      "${var.commonwell_aws_cidr}",
      "${var.commonwell_lindsay_cidr}",
      "${var.commonwell_whitby_cidr}"
    ]
    description = "FTPS"
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    Name = "COMMONWELL-NPRINTING-SG"
    BILLINGCODE = "DEL02890-01-01-01-NB10"
    BILLINGCONTACT = "smodh@deloitte.ca"
    CLIENT = "CMS"
    COUNTRY = "CA"
    CSCLASS = "Confidential"
    CSQUAL = "No CI/PI data"
    CSTYPE = "External"
    ENVIRONMENT = "PRD"
    FUNCTION = "CON"
    GROUPCONTACT = "smodh@deloitte.ca"
    MEMBERFIRM = "CA" 
    PRIMARYCONTACT = "session1"
    SECONDARYCONTACT = "smodh@deloitte.ca"
  }
}

resource "aws_security_group" "SG_DATAHUB_INFOCENTER" {
  name = "A-MC-CAC-D-A003-COMMONWELL-DATAHUB-INFOCENTER-SG"

  ingress {
      from_port   = 3389
      to_port     = 3389
      protocol    = "tcp"
      cidr_blocks = [
        "10.10.5.104/32", # aviatrix
        "${var.deloitte_cidr}",
      "${var.hub_cidr}",
        "${var.commonwell_aws_cidr}",
        "${var.commonwell_lindsay_cidr}",
        "${var.commonwell_whitby_cidr}"
      ]
  }

  ingress {
    from_port       = 4444
    to_port         = 4444
    protocol        = "tcp"
    cidr_blocks = [
      "${var.deloitte_cidr}",
      "${var.hub_cidr}",
      "${var.commonwell_aws_cidr}",
      "${var.commonwell_lindsay_cidr}",
      "${var.commonwell_whitby_cidr}"
    ]
  }

  ingress {
    from_port       = 4444
    to_port         = 4444
    protocol        = "udp"
    cidr_blocks = [
      "${var.deloitte_cidr}",
      "${var.hub_cidr}",
      "${var.commonwell_aws_cidr}",
      "${var.commonwell_lindsay_cidr}",
      "${var.commonwell_whitby_cidr}"
    ]
  }

  ingress {
    from_port       = 4446
    to_port         = 4446
    protocol        = "tcp"
    cidr_blocks = [
      "${var.deloitte_cidr}",
      "${var.hub_cidr}",
      "${var.commonwell_aws_cidr}",
      "${var.commonwell_lindsay_cidr}",
      "${var.commonwell_whitby_cidr}"
    ]
  }

  ingress {
    from_port       = 4446
    to_port         = 4446
    protocol        = "udp"
    cidr_blocks = [
      "${var.deloitte_cidr}",
      "${var.hub_cidr}",
      "${var.commonwell_aws_cidr}",
      "${var.commonwell_lindsay_cidr}",
      "${var.commonwell_whitby_cidr}"
    ]
  }

  ingress {
    from_port       = 8080
    to_port         = 8080
    protocol        = "tcp"
    cidr_blocks = [
      "${var.deloitte_cidr}",
      "${var.hub_cidr}",
      "${var.commonwell_aws_cidr}",
      "${var.commonwell_lindsay_cidr}",
      "${var.commonwell_whitby_cidr}"
    ]
  }

  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    Name = "A-MC-CAC-D-A003-COMMONWELL-DATAHUB-INFOCENTER-SG"
  }
}

resource "aws_security_group" "SG_COMMONWELL-PROMETHEUS-GRAFANA" {
  name = "COMMONWELL-PROMETHEUS-GRAFANA-SG"

  ingress {
    from_port   = 10000
    to_port     = 10001
    protocol    = "tcp"
    cidr_blocks = [
      "${var.commonwell_aviatrix_vpn_1a}",
      "${var.commonwell_aviatrix_vpn_1b}",
      "${var.deloitte_cidr}"
    ]
    description = "Prometheus"
  }

  ingress {
    from_port   = 19191
    to_port     = 19191
    protocol    = "tcp"
    cidr_blocks = [
      "${var.commonwell_aviatrix_vpn_1a}",
      "${var.commonwell_aviatrix_vpn_1b}"
    ]
    security_groups = [
      "${aws_security_group.SG_INTERNAL_HAPROXY.id}"
    ]
    description = "Thanos Query"
  }

  ingress {
    from_port   = 3000
    to_port     = 3000
    protocol    = "tcp"
    cidr_blocks = [
      "${var.commonwell_aviatrix_vpn_1a}",
      "${var.commonwell_aviatrix_vpn_1b}",
      "${var.deloitte_cidr}"
    ]
    description = "Grafana"
  }
  
    egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    Name = "COMMONWELL-PROMETHEUS-GRAFANA-SG"
  }
}

resource "aws_security_group" "SG_COMMONWELL_JOBSCHEDULER_SSH" {
  name = "COMMONWELL-JOBSCHEDULER-SSH-SG"
 
 ingress {
    from_port       = 22
    to_port         = 22
    protocol        = "tcp"
    security_groups = ["${aws_security_group.SG_JOB_SCHEDULER.id}"]
  }

  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    Name = "COMMONWELL-JOBSCHEDULER-SSH-SG"
  }
}

resource "aws_security_group" "SG_AVIATRIX_RDP" {
  name = "COMMONWELL-AVIATRIX-RDP-SG"

  ingress {
    from_port   = 3389
    to_port     = 3389
    protocol    = "tcp"
    cidr_blocks = [
      "${var.commonwell_aviatrix_vpn_1a}",
      "${var.commonwell_aviatrix_vpn_1b}"
    ]
    description = "RDP"
  }

  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    Name = "COMMONWELL-AVIATRIX-RDP-SG"
  }
}

###CHG0030157
resource "aws_security_group" "SG_FORTICLIENT_RDP" {
  name = "COMMONWELL-FORTICLIENT-RDP-SG"

  ingress {
    from_port   = 3389
    to_port     = 3389
    protocol    = "tcp"
    cidr_blocks = [
      "${var.commonwell_aws_cidr}"
    ]
    description = "RDP"
  }

  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    Name = "COMMONWELL-FORTICLIENT-RDP-SG"
  }
}

resource "aws_security_group" "SG_DATAHUB_INFOCENTER_QA" {
  name = "COMMONWELL-DATAHUB-INFOCENTER-QA-SG"

  ingress {
    from_port   = 1433
    to_port     = 1433
    protocol    = "tcp"
    cidr_blocks = [
      "10.10.52.230/32",
      "${var.deloitte_cidr}",
      "${var.hub_cidr}",
      "${var.commonwell_aws_cidr}",
      "${var.commonwell_lindsay_cidr}",
      "${var.commonwell_whitby_cidr}"
    ]
  }

  ingress {
    from_port   = 1434
    to_port     = 1434
    protocol    = "udp"
    cidr_blocks = [
      "10.10.52.230/32",
      "${var.deloitte_cidr}",
      "${var.hub_cidr}",
      "${var.commonwell_aws_cidr}",
      "${var.commonwell_lindsay_cidr}",
      "${var.commonwell_whitby_cidr}"
    ]
  }

  ingress {
    from_port       = 4444
    to_port         = 4444
    protocol        = "tcp"
    cidr_blocks = [
      "${var.deloitte_cidr}",
      "${var.hub_cidr}",
      "${var.commonwell_aws_cidr}",
      "${var.commonwell_lindsay_cidr}",
      "${var.commonwell_whitby_cidr}"
    ]
  }

  ingress {
      from_port   = 3389
      to_port     = 3389
      protocol    = "tcp"
      cidr_blocks = [
        "10.10.5.104/32", # aviatrix
        "${var.deloitte_cidr}",
        "${var.hub_cidr}",
        "${var.commonwell_aws_cidr}",
        "${var.commonwell_lindsay_cidr}",
        "${var.commonwell_whitby_cidr}"
      ]
  }

  ingress {
    from_port       = 4444
    to_port         = 4444
    protocol        = "udp"
    cidr_blocks = [
      "${var.deloitte_cidr}",
      "${var.hub_cidr}",
      "${var.commonwell_aws_cidr}",
      "${var.commonwell_lindsay_cidr}",
      "${var.commonwell_whitby_cidr}"
    ]
  }

  ingress {
    from_port       = 4446
    to_port         = 4446
    protocol        = "tcp"
    cidr_blocks = [
      "${var.deloitte_cidr}",
      "${var.hub_cidr}",
      "${var.commonwell_aws_cidr}",
      "${var.commonwell_lindsay_cidr}",
      "${var.commonwell_whitby_cidr}"
    ]
  }

  ingress {
    from_port       = 4446
    to_port         = 4446
    protocol        = "udp"
    cidr_blocks = [
      "${var.deloitte_cidr}",
      "${var.hub_cidr}",
      "${var.commonwell_aws_cidr}",
      "${var.commonwell_lindsay_cidr}",
      "${var.commonwell_whitby_cidr}"
    ]
  }

  ingress {
    from_port       = 8080
    to_port         = 8080
    protocol        = "tcp"
    cidr_blocks = [
      "${var.deloitte_cidr}",
      "${var.hub_cidr}",
      "${var.commonwell_aws_cidr}",
      "${var.commonwell_lindsay_cidr}",
      "${var.commonwell_whitby_cidr}"
    ]
  }

  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    "Name" = "COMMONWELL-DATAHUB-INFOCENTER-QA-SG"
  }
}

resource "aws_security_group" "SG_MSSQL_OMNIA_DB" {
  name = "COMMONWELL-MSSQL-OMNIA-DB-SG"

  ingress {
    from_port   = 1433
    to_port     = 1433
    protocol    = "tcp"
    cidr_blocks = [
      "${var.deloitte_cidr}",
      "${var.hub_cidr}",
      "${var.commonwell_aws_cidr}",
      "${var.commonwell_lindsay_cidr}",
      "${var.commonwell_whitby_cidr}",
      "${var.commonwell_omnia_cidr}"
    ]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    Name = "COMMONWELL-MSSQL-OMNIA-DB-SG"
  }
}