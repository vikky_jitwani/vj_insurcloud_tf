resource "aws_cloudwatch_log_metric_filter" "gwpc_batch_prod_error_metric" {
  name           = "GWPCBatchProdErrorCount"
  pattern        = "ERROR"
  log_group_name = "cmig-PROD-gwpc-batch"

  metric_transformation {
    name      = "gwpc-batch-prod-error-counter"
    namespace = "LogMetrics"
    value     = "1"
  }
}

resource "aws_cloudwatch_metric_alarm" "gwpc_batch_prod_error_alarm" {
  alarm_name            = "gwpc-batch-prod-error-alarm"
  comparison_operator   = "GreaterThanOrEqualToThreshold"
  evaluation_periods    = "2"
  threshold             = "10"
  statistic             = "Sum"
  period                = "120"
  metric_name           = "GWPCBatchProdErrorCount"
  alarm_description     = "alarm for errors"
  namespace = "LogMetrics"
  insufficient_data_actions = []

  alarm_actions     = ["arn:aws:sns:ca-central-1:942574907772:CMS-PROD-GW-NOTIFICATIONS"]
}

