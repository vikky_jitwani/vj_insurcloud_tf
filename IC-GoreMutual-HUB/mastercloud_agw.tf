#API Gateway

#DEV
resource "aws_api_gateway_rest_api" "GORE" {
  name        = "GORE"
  description = "This is the API gateway for GORE API Gateway Collection."
  endpoint_configuration {
    types = ["REGIONAL"]
  }
}
######### AUTHORIZER ##############
#Token Auth DEV

resource "aws_api_gateway_authorizer" "NonProd_Auth0_EHI" {
  name                   = "NonProd_Auth0_EHI"
  rest_api_id            = "${aws_api_gateway_rest_api.GORE.id}"
  authorizer_uri         = "${aws_lambda_function.gore_AGWAuthenrizer_Address_Hub.invoke_arn}"
  authorizer_credentials = "arn:aws:iam::${var.gore-hub-account}:role/Auth0_invocation_role"
}

resource "aws_api_gateway_authorizer" "NonProd_Auth0_Address" {
  name                   = "NonProd_Auth0_Address"
  rest_api_id            = "${aws_api_gateway_rest_api.GORE.id}"
  authorizer_uri         = "${aws_lambda_function.gore_AGWAuthenrizer_Address_Hub.invoke_arn}"
  authorizer_credentials = "arn:aws:iam::${var.gore-hub-account}:role/Auth0_invocation_role"
}
resource "aws_api_gateway_authorizer" "NonProd_Auth0_Uniban" {
  name                   = "NonProd_Auth0_Uniban"
  rest_api_id            = "${aws_api_gateway_rest_api.GORE.id}"
  authorizer_uri         = "${aws_lambda_function.gore_AGWAuthenrizer_Uniban_Hub.invoke_arn}"
  authorizer_credentials = "arn:aws:iam::${var.gore-hub-account}:role/Auth0_invocation_role"
}
resource "aws_api_gateway_authorizer" "NonProd_Auth0_Cheque" {
  name                   = "NonProd_Auth0_Cheque"
  rest_api_id            = "${aws_api_gateway_rest_api.GORE.id}"
  authorizer_uri         = "${aws_lambda_function.gore_AGWAuthenrizer_Cheque_Hub.invoke_arn}"
  authorizer_credentials = "arn:aws:iam::${var.gore-hub-account}:role/Auth0_invocation_role"
}
resource "aws_api_gateway_authorizer" "NonProd_Auth0_Quoting" {
  name                   = "NonProd_Auth0_Quoting"
  rest_api_id            = "${aws_api_gateway_rest_api.GORE.id}"
  authorizer_uri         = "${aws_lambda_function.gore_AGWAuthenrizer_Quoting_Hub.invoke_arn}"
  authorizer_credentials = "arn:aws:iam::${var.gore-hub-account}:role/Auth0_invocation_role"
}
resource "aws_api_gateway_authorizer" "NonProd_Auth0_Upload" {
  name                   = "NonProd_Auth0_Upload"
  rest_api_id            = "${aws_api_gateway_rest_api.GORE.id}"
  authorizer_uri         = "${aws_lambda_function.gore_AGWAuthenrizer_Upload_Hub.invoke_arn}"
  authorizer_credentials = "arn:aws:iam::${var.gore-hub-account}:role/Auth0_invocation_role"
}
resource "aws_api_gateway_authorizer" "NonProd_Auth0_Inquiry" {
  name                   = "NonProd_Auth0_Inquiry"
  rest_api_id            = "${aws_api_gateway_rest_api.GORE.id}"
  authorizer_uri         = "${aws_lambda_function.gore_AGWAuthenrizer_Inquiry_Hub.invoke_arn}"
  authorizer_credentials = "arn:aws:iam::${var.gore-hub-account}:role/Auth0_invocation_role"
}


#Token Auth Prod 
# resource "aws_api_gateway_authorizer" "Auth0_prod" {
#   name                   = "Auth0_prod"
#   rest_api_id            = "${aws_api_gateway_rest_api.GLCheque.id}"
#   authorizer_uri         = "${aws_lambda_function.goreProd-APIGatewayAuthenrizer_Hub.invoke_arn}"
#   authorizer_credentials = "arn:aws:iam::${var.gore-hub-account}:role/Auth0_invocation_role"
# }

######### VPC LINKS ##############
# resource "aws_api_gateway_vpc_link" "gore-insurcloud-prod" {
#   name        = "gore-insurcloud-prod"
#   #target_arns = ["arn:aws:elasticloadbalancing:${var.region}:${var.peel-hub-account}:loadbalancer/net/peel-insurcloud/a5af8ea64200ff9e"]
#   target_arns = ["${aws_lb.ICGORE_PROD_APIGATEWAY_NLB.arn}"]
# }

# #VPC Link gore-insurcloud-dev01
resource "aws_api_gateway_vpc_link" "gore-insurcloud-dev01" {
  name        = "gore-insurcloud-dev01"
  #target_arns = ["arn:aws:elasticloadbalancing:${var.region}:${var.gore-hub-account}:loadbalancer/net/gore-insurcloud-dev01/ac3d5916817d1ad2"]
  target_arns = ["${aws_lb.ICGORE_DEV01_APW_NLB.arn}"]
}

resource "aws_api_gateway_vpc_link" "gore-insurcloud-qa01" {
  name        = "gore-insurcloud-qa01"
  #target_arns = ["arn:aws:elasticloadbalancing:${var.region}:${var.gore-hub-account}:loadbalancer/net/gore-insurcloud-dev01/ac3d5916817d1ad2"]
  target_arns = ["${aws_lb.ICGORE_QA01_APW_NLB.arn}"]
}
# #VPC Link gore-insurcloud-dev02
resource "aws_api_gateway_vpc_link" "gore-insurcloud-dev02" {
  name        = "gore-insurcloud-dev02"
  target_arns = ["${aws_lb.ICGORE_DEV02_APW_NLB.arn}"]
}
# # #VPC Link peel-insurcloud-pps
# resource "aws_api_gateway_vpc_link" "peel-insurcloud-pps" {
#   name        = "peel-insurcloud-pps"
#   #target_arns = ["arn:aws:elasticloadbalancing:${var.region}:${var.peel-hub-account}:loadbalancer/net/peel-insurcloud-pps/d5263379262f668f"]
#   target_arns = ["${aws_lb.ICPEEL_PPS_APIGATEWAY_NLB.arn}"]
# }


######### RESOURCE GROUPS ##############
#########DEV01#########
resource "aws_api_gateway_resource" "DEV01" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE.id}"
  parent_id   = "${aws_api_gateway_rest_api.GORE.root_resource_id}"
  path_part   = "DEV01"
}

###Enterprise###
resource "aws_api_gateway_resource" "Enterprise" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE.id}"
  parent_id   = "${aws_api_gateway_resource.DEV01.id}"
  path_part   = "Enterprise"
}

# Enterprise Outbound GW -API Gateway - ARMS
resource "aws_api_gateway_resource" "EHIOutbound" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE.id}"
  parent_id   = "${aws_api_gateway_resource.Enterprise.id}"
  path_part   = "EHIOutbound"
}

resource "aws_api_gateway_method" "RentalReservationRequestRequest" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE.id}"
  resource_id   = "${aws_api_gateway_resource.EHIOutbound.id}"
  http_method   = "POST"
  authorization = "NONE"
}

resource "aws_api_gateway_integration" "RentalReservationRequestRequest" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE.id}"
  resource_id             = "${aws_api_gateway_resource.EHIOutbound.id}"
  http_method             = "${aws_api_gateway_method.RentalReservationRequestRequest.http_method}"
  integration_http_method = "POST"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-dev01.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.dev01.gore.insurcloud.ca/soap-security-service/v1/proxy"
}

resource "aws_api_gateway_method_response" "RentalReservationRequestRequest" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE.id}"
  resource_id = "${aws_api_gateway_resource.EHIOutbound.id}"
  http_method = "${aws_api_gateway_method.RentalReservationRequestRequest.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}

#Enterprise --> API gateway --> GW
resource "aws_api_gateway_resource" "EHIInboundGW" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE.id}"
  parent_id   = "${aws_api_gateway_resource.Enterprise.id}"
  path_part   = "EHIInboundGW"
}

resource "aws_api_gateway_resource" "UpdateNotificationRequestAPI" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE.id}"
  parent_id   = "${aws_api_gateway_resource.EHIInboundGW.id}"
  path_part   = "UpdateNotificationRequestAPI"
}

#Update Notification request
resource "aws_api_gateway_method" "UpdateNotificationRequestAPI" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE.id}"
  resource_id   = "${aws_api_gateway_resource.UpdateNotificationRequestAPI.id}"
  http_method   = "POST"
  authorization = "NONE"
}

resource "aws_api_gateway_integration" "UpdateNotificationRequestAPI" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE.id}"
  resource_id             = "${aws_api_gateway_resource.UpdateNotificationRequestAPI.id}"
  http_method             = "${aws_api_gateway_method.UpdateNotificationRequestAPI.http_method}"
  integration_http_method = "POST"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-dev01.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.dev01.gore.insurcloud.ca/soap-security-service/v1/inbound-proxy"
}

resource "aws_api_gateway_method_response" "UpdateNotificationRequestAPI" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE.id}"
  resource_id = "${aws_api_gateway_resource.UpdateNotificationRequestAPI.id}"
  http_method = "${aws_api_gateway_method.UpdateNotificationRequestAPI.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}

#Enterprise --> API gateway --> Enterprise MS --> GW & Document MS
resource "aws_api_gateway_resource" "EHIInboundMS" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE.id}"
  parent_id   = "${aws_api_gateway_resource.Enterprise.id}"
  path_part   = "EHIInboundMS"
}

#Update Notification request
resource "aws_api_gateway_method" "SendInvoiceNotificationRequestAPI" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE.id}"
  resource_id   = "${aws_api_gateway_resource.EHIInboundMS.id}"
  http_method   = "POST"
  authorization = "NONE"
}

resource "aws_api_gateway_integration" "SendInvoiceNotificationRequestAPI" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE.id}"
  resource_id             = "${aws_api_gateway_resource.EHIInboundMS.id}"
  http_method             = "${aws_api_gateway_method.SendInvoiceNotificationRequestAPI.http_method}"
  integration_http_method = "POST"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-dev01.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.dev01.gore.insurcloud.ca/car-rental-service/v1/rental/sendInvoiceNotificationRequest"
}

resource "aws_api_gateway_method_response" "SendInvoiceNotificationRequestAPI" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE.id}"
  resource_id = "${aws_api_gateway_resource.EHIInboundMS.id}"
  http_method = "${aws_api_gateway_method.SendInvoiceNotificationRequestAPI.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}

###Impact###
resource "aws_api_gateway_resource" "Impact" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE.id}"
  parent_id   = "${aws_api_gateway_resource.DEV01.id}"
  path_part   = "Impact"
}

# Impact Outbound GW -API Gateway - ARMS
resource "aws_api_gateway_method" "Impact_Auth" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE.id}"
  resource_id   = "${aws_api_gateway_resource.Impact.id}"
  http_method   = "POST"
  authorization = "CUSTOM"
  authorizer_id = "${aws_api_gateway_authorizer.NonProd_Auth0_Address.id}"
}

resource "aws_api_gateway_integration" "Impact_Auth" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE.id}"
  resource_id             = "${aws_api_gateway_resource.Impact.id}"
  http_method             = "${aws_api_gateway_method.Impact_Auth.http_method}"
  integration_http_method = "POST"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-dev01.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.dev01.gore.insurcloud.ca/genericassignmentservice.svc"
}

resource "aws_api_gateway_method_response" "Impact_Auth" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE.id}"
  resource_id = "${aws_api_gateway_resource.Impact.id}"
  http_method = "${aws_api_gateway_method.Impact_Auth.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}

####Audatex###
resource "aws_api_gateway_resource" "Audatex" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE.id}"
  parent_id   = "${aws_api_gateway_resource.DEV01.id}"
  path_part   = "Audatex"
}

# Audatex Inbount Acknowledgment - ARMS
resource "aws_api_gateway_resource" "AudatexInboundAck" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE.id}"
  parent_id   = "${aws_api_gateway_resource.Audatex.id}"
  path_part   = "AudatexInboundAck"
}

resource "aws_api_gateway_method" "AudatexInboundAck" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE.id}"
  resource_id   = "${aws_api_gateway_resource.AudatexInboundAck.id}"
  http_method   = "POST"
  authorization = "CUSTOM"
  authorizer_id = "${aws_api_gateway_authorizer.NonProd_Auth0_Address.id}"
}

resource "aws_api_gateway_integration" "AudatexInboundAck" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE.id}"
  resource_id             = "${aws_api_gateway_resource.AudatexInboundAck.id}"
  http_method             = "${aws_api_gateway_method.AudatexInboundAck.http_method}"
  integration_http_method = "POST"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-dev01.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.dev01.gore.insurcloud.ca/audatex-service/v1/assignment/ack"
}

resource "aws_api_gateway_method_response" "AudatexInboundAck" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE.id}"
  resource_id = "${aws_api_gateway_resource.AudatexInboundAck.id}"
  http_method = "${aws_api_gateway_method.AudatexInboundAck.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}

##Audatex Inbound estimate
resource "aws_api_gateway_resource" "AudatexInboundEstimate" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE.id}"
  parent_id   = "${aws_api_gateway_resource.Audatex.id}"
  path_part   = "AudatexInboundEstimate"
}

resource "aws_api_gateway_method" "AudatexInboundEstimate" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE.id}"
  resource_id   = "${aws_api_gateway_resource.AudatexInboundEstimate.id}"
  http_method   = "POST"
  authorization = "CUSTOM"
  authorizer_id = "${aws_api_gateway_authorizer.NonProd_Auth0_Address.id}"
}


resource "aws_api_gateway_integration" "AudatexInboundEstimate" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE.id}"
  resource_id             = "${aws_api_gateway_resource.AudatexInboundEstimate.id}"
  http_method             = "${aws_api_gateway_method.AudatexInboundEstimate.http_method}"
  integration_http_method = "POST"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-dev01.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.dev01.gore.insurcloud.ca/audatex-service/v1/estimate"
}

resource "aws_api_gateway_method_response" "AudatexInboundEstimate" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE.id}"
  resource_id = "${aws_api_gateway_resource.AudatexInboundEstimate.id}"
  http_method = "${aws_api_gateway_method.AudatexInboundEstimate.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}
####end of audatex#####

# ###Cheque ###
resource "aws_api_gateway_resource" "Cheque" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE.id}"
  parent_id   = "${aws_api_gateway_resource.DEV01.id}"
  path_part   = "Cheque"
}  
# #Cheque --> F & 0 --> MS#####
resource "aws_api_gateway_resource" "ChequeInbound" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE.id}"
  parent_id   = "${aws_api_gateway_resource.Cheque.id}"
  path_part   = "ChequeInbound"
}

resource "aws_api_gateway_method" "ChequeInbound" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE.id}"
  resource_id   = "${aws_api_gateway_resource.ChequeInbound.id}"
  http_method   = "POST"
  authorization = "CUSTOM"
  authorizer_id = "${aws_api_gateway_authorizer.NonProd_Auth0_Cheque.id}"
}

resource "aws_api_gateway_integration" "ChequeInbound" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE.id}"
  resource_id             = "${aws_api_gateway_resource.ChequeInbound.id}"
  http_method             = "${aws_api_gateway_method.ChequeInbound.http_method}"
  integration_http_method = "POST"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-dev01.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.dev01.gore.insurcloud.ca/cheque-service/v1/cheque/updateChequeNumbers"
}

resource "aws_api_gateway_method_response" "ChequeInbound" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE.id}"
  resource_id = "${aws_api_gateway_resource.ChequeInbound.id}"
  http_method = "${aws_api_gateway_method.ChequeInbound.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}

# #Cheque --> GW --> MS#####
resource "aws_api_gateway_resource" "ChequeOutbound" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE.id}"
  parent_id   = "${aws_api_gateway_resource.Cheque.id}"
  path_part   = "ChequeOutbound"
}

resource "aws_api_gateway_method" "ChequeOutbound" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE.id}"
  resource_id   = "${aws_api_gateway_resource.ChequeOutbound.id}"
  http_method   = "POST"
  authorization = "CUSTOM"
  authorizer_id = "${aws_api_gateway_authorizer.NonProd_Auth0_Cheque.id}"
}

resource "aws_api_gateway_integration" "ChequeOutbound" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE.id}"
  resource_id             = "${aws_api_gateway_resource.ChequeOutbound.id}"
  http_method             = "${aws_api_gateway_method.ChequeOutbound.http_method}"
  integration_http_method = "POST"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-dev01.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.dev01.gore.insurcloud.ca/cheque-service/v1/cheque/print"
}

resource "aws_api_gateway_method_response" "ChequeOutbound" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE.id}"
  resource_id = "${aws_api_gateway_resource.ChequeOutbound.id}"
  http_method = "${aws_api_gateway_method.ChequeOutbound.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}
####Quoting##
resource "aws_api_gateway_resource" "Quoting" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE.id}"
  parent_id   = "${aws_api_gateway_resource.DEV01.id}"
  path_part   = "Quoting"
}
resource "aws_api_gateway_method" "Quoting" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE.id}"
  resource_id   = "${aws_api_gateway_resource.Quoting.id}"
  http_method   = "POST"
  authorization = "CUSTOM"
  authorizer_id = "${aws_api_gateway_authorizer.NonProd_Auth0_Quoting.id}"
}

resource "aws_api_gateway_integration" "Quoting" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE.id}"
  resource_id             = "${aws_api_gateway_resource.Quoting.id}"
  http_method             = "${aws_api_gateway_method.Quoting.http_method}"
  integration_http_method = "POST"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-dev01.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.dev01.gore.insurcloud.ca/quoting-service/microservices/quoting/ws/"
}

resource "aws_api_gateway_method_response" "Quoting" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE.id}"
  resource_id = "${aws_api_gateway_resource.Quoting.id}"
  http_method = "${aws_api_gateway_method.Quoting.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}
###NewSubmission/Upload###
resource "aws_api_gateway_resource" "Newsubmission" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE.id}"
  parent_id   = "${aws_api_gateway_resource.DEV01.id}"
  path_part   = "Newsubmission"
}
resource "aws_api_gateway_method" "Newsubmission" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE.id}"
  resource_id   = "${aws_api_gateway_resource.Newsubmission.id}"
  http_method   = "POST"
  authorization = "CUSTOM"
  authorizer_id = "${aws_api_gateway_authorizer.NonProd_Auth0_Upload.id}"
}

resource "aws_api_gateway_integration" "Newsubmission" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE.id}"
  resource_id             = "${aws_api_gateway_resource.Newsubmission.id}"
  http_method             = "${aws_api_gateway_method.Newsubmission.http_method}"
  integration_http_method = "POST"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-dev01.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.dev01.gore.insurcloud.ca/newsubmission-service/microservices/newsubmission/ws/"
}

resource "aws_api_gateway_method_response" "Newsubmission" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE.id}"
  resource_id = "${aws_api_gateway_resource.Newsubmission.id}"
  http_method = "${aws_api_gateway_method.Newsubmission.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}

###PolicyInquiry###
resource "aws_api_gateway_resource" "PolicyInquiry" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE.id}"
  parent_id   = "${aws_api_gateway_resource.DEV01.id}"
  path_part   = "PolicyInquiry"
}
resource "aws_api_gateway_method" "PolicyInquiry" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE.id}"
  resource_id   = "${aws_api_gateway_resource.PolicyInquiry.id}"
  http_method   = "POST"
  authorization = "CUSTOM"
  authorizer_id = "${aws_api_gateway_authorizer.NonProd_Auth0_Inquiry.id}"
}

resource "aws_api_gateway_integration" "PolicyInquiry" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE.id}"
  resource_id             = "${aws_api_gateway_resource.PolicyInquiry.id}"
  http_method             = "${aws_api_gateway_method.PolicyInquiry.http_method}"
  integration_http_method = "POST"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-dev01.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.dev01.gore.insurcloud.ca/policy-inquiry-service/microservices/policy/inquiry/ws/"
}

resource "aws_api_gateway_method_response" "PolicyInquiry" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE.id}"
  resource_id = "${aws_api_gateway_resource.PolicyInquiry.id}"
  http_method = "${aws_api_gateway_method.PolicyInquiry.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}

###HCAI###
resource "aws_api_gateway_resource" "HCAI" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE.id}"
  parent_id   = "${aws_api_gateway_resource.DEV01.id}"
  path_part   = "HCAI"
}

# HCAI Outbound GW -API Gateway - ARMS
resource "aws_api_gateway_method" "HCAI" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE.id}"
  resource_id   = "${aws_api_gateway_resource.HCAI.id}"
  http_method   = "POST"
  authorization = "NONE"
}

resource "aws_api_gateway_integration" "HCAI" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE.id}"
  resource_id             = "${aws_api_gateway_resource.HCAI.id}"
  http_method             = "${aws_api_gateway_method.HCAI.http_method}"
  integration_http_method = "POST"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-dev01.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.dev01.gore.insurcloud.ca/soap-security-service/v1/proxy"
}

resource "aws_api_gateway_method_response" "HCAI" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE.id}"
  resource_id = "${aws_api_gateway_resource.HCAI.id}"
  http_method = "${aws_api_gateway_method.HCAI.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}

###CRC###
resource "aws_api_gateway_resource" "CRCInbound" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE.id}"
  parent_id   = "${aws_api_gateway_resource.DEV01.id}"
  path_part   = "CRCInbound"
}

#CRC --> Guidewire
resource "aws_api_gateway_method" "CRCInbound" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE.id}"
  resource_id   = "${aws_api_gateway_resource.CRCInbound.id}"
  http_method   = "POST"
  authorization = "NONE"
}

resource "aws_api_gateway_integration" "CRCInbound" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE.id}"
  resource_id             = "${aws_api_gateway_resource.CRCInbound.id}"
  http_method             = "${aws_api_gateway_method.CRCInbound.http_method}"
  integration_http_method = "POST"
  connection_type         = "INTERNET"
  type                    = "HTTP_PROXY"
  uri                     = "https://dev.goredev.guidewire.net/cc/ws/ic/integration/crc/CollisionReportingCenterAPI"
}

resource "aws_api_gateway_method_response" "CRCInbound" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE.id}"
  resource_id = "${aws_api_gateway_resource.CRCInbound.id}"
  http_method = "${aws_api_gateway_method.CRCInbound.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}

#CRC --> MS 

resource "aws_api_gateway_resource" "CRCInboundMS" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE.id}"
  parent_id   = "${aws_api_gateway_resource.CRCInbound.id}"
  path_part   = "CRCInboundMS"
}

resource "aws_api_gateway_method" "CRCInboundMS" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE.id}"
  resource_id   = "${aws_api_gateway_resource.CRCInboundMS.id}"
  http_method   = "POST"
  authorization = "NONE"
}

resource "aws_api_gateway_integration" "CRCInboundMS" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE.id}"
  resource_id             = "${aws_api_gateway_resource.CRCInboundMS.id}"
  http_method             = "${aws_api_gateway_method.CRCInboundMS.http_method}"
  integration_http_method = "POST"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-dev01.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.dev01.gore.insurcloud.ca/crc-service/microservices/crc/ws/"
}

resource "aws_api_gateway_method_response" "CRCInboundMS" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE.id}"
  resource_id = "${aws_api_gateway_resource.CRCInboundMS.id}"
  http_method = "${aws_api_gateway_method.CRCInboundMS.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}

# ###Uniban###
resource "aws_api_gateway_resource" "Uniban" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE.id}"
  parent_id   = "${aws_api_gateway_resource.DEV01.id}"
  path_part   = "Uniban"
}

##Uniban --> API
resource "aws_api_gateway_method" "Uniban" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE.id}"
  resource_id   = "${aws_api_gateway_resource.Uniban.id}"
  http_method   = "GET"
  authorization = "NONE"
  request_parameters = {
    "method.request.querystring.policyNumber"= true
    "method.request.querystring.dataOfLoss"= true
    "method.request.querystring.vin"= true
  }
}

resource "aws_api_gateway_integration" "Uniban" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE.id}"
  resource_id             = "${aws_api_gateway_resource.Uniban.id}"
  http_method             = "${aws_api_gateway_method.Uniban.http_method}"
  integration_http_method = "GET"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-dev01.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.dev01.gore.insurcloud.ca/uniban-service/v1/retrievePolicy"
}

resource "aws_api_gateway_method_response" "Uniban" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE.id}"
  resource_id = "${aws_api_gateway_resource.Uniban.id}"
  http_method = "${aws_api_gateway_method.Uniban.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}

# resource "aws_api_gateway_deployment" "DEV01" {
#   depends_on = ["aws_api_gateway_integration.RentalReservationAPI"]
#   rest_api_id = "${aws_api_gateway_rest_api.GORE.id}"
#   stage_name  = "DEV01"

#   variables = {
#     "GWenvID" = "dev"
#   }
# }


# Deployment stages in DEV01, PPS and PROD
# resource "aws_api_gateway_deployment" "PEEL" {
#   depends_on = ["aws_api_gateway_integration.retrieveChequeEntries"]
#   rest_api_id = "${aws_api_gateway_rest_api.GLCheque.id}"
#   stage_name  = "PEEL"
# }

#waf and logs needs to be set at stage level afterwards 



#########nonprod#########
resource "aws_api_gateway_resource" "QA01" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE.id}"
  parent_id   = "${aws_api_gateway_rest_api.GORE.root_resource_id}"
  path_part   = "QA01"
}

###Enterprise###
resource "aws_api_gateway_resource" "QA01_Enterprise" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE.id}"
  parent_id   = "${aws_api_gateway_resource.QA01.id}"
  path_part   = "QA01_Enterprise"
}

# Enterprise Outbound GW -API Gateway - ARMS
resource "aws_api_gateway_resource" "QA01_EHIOutbound" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE.id}"
  parent_id   = "${aws_api_gateway_resource.QA01_Enterprise.id}"
  path_part   = "QA01_EHIOutbound"
}

resource "aws_api_gateway_method" "QA01_EHIOutbound" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE.id}"
  resource_id   = "${aws_api_gateway_resource.QA01_EHIOutbound.id}"
  http_method   = "POST"
  authorization = "NONE"
}

resource "aws_api_gateway_integration" "QA01_EHIOutbound" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE.id}"
  resource_id             = "${aws_api_gateway_resource.QA01_EHIOutbound.id}"
  http_method             = "${aws_api_gateway_method.QA01_EHIOutbound.http_method}"
  integration_http_method = "POST"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-qa01.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.qa01.gore.insurcloud.ca/soap-security-service/v1/proxy"
}

resource "aws_api_gateway_method_response" "QA01_EHIOutbound" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE.id}"
  resource_id = "${aws_api_gateway_resource.QA01_EHIOutbound.id}"
  http_method = "${aws_api_gateway_method.QA01_EHIOutbound.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}

# #Enterprise --> API gateway --> GW
resource "aws_api_gateway_resource" "QA01_EHIInboundGW" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE.id}"
  parent_id   = "${aws_api_gateway_resource.QA01_Enterprise.id}"
  path_part   = "QA01_EHIInboundGW"
}

# #Update Notification request
resource "aws_api_gateway_method" "QA01_EHIInboundGW" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE.id}"
  resource_id   = "${aws_api_gateway_resource.QA01_EHIInboundGW.id}"
  http_method   = "POST"
  authorization = "NONE"
}

resource "aws_api_gateway_integration" "QA01_EHIInboundGW" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE.id}"
  resource_id             = "${aws_api_gateway_resource.QA01_EHIInboundGW.id}"
  http_method             = "${aws_api_gateway_method.QA01_EHIInboundGW.http_method}"
  integration_http_method = "POST"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-qa01.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.QA01.gore.insurcloud.ca/soap-security-service/v1/inbound-proxy"}

resource "aws_api_gateway_method_response" "QA01_EHIInboundGW" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE.id}"
  resource_id = "${aws_api_gateway_resource.QA01_EHIInboundGW.id}"
  http_method = "${aws_api_gateway_method.QA01_EHIInboundGW.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}

# #Enterprise --> API gateway --> Enterprise MS --> GW & Document MS
resource "aws_api_gateway_resource" "QA01_EHIInboundMS" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE.id}"
  parent_id   = "${aws_api_gateway_resource.QA01_Enterprise.id}"
  path_part   = "QA01_EHIInboundMS"
}

# #Update Notification request
resource "aws_api_gateway_method" "QA01_EHIInboundMS" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE.id}"
  resource_id   = "${aws_api_gateway_resource.QA01_EHIInboundMS.id}"
  http_method   = "POST"
  authorization = "NONE"
}

resource "aws_api_gateway_integration" "QA01_EHIInboundMS" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE.id}"
  resource_id             = "${aws_api_gateway_resource.QA01_EHIInboundMS.id}"
  http_method             = "${aws_api_gateway_method.QA01_EHIInboundMS.http_method}"
  integration_http_method = "POST"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-qa01.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.qa01.gore.insurcloud.ca/car-rental-service/v1/rental/sendInvoiceNotificationRequest"
}

resource "aws_api_gateway_method_response" "QA01_EHIInboundMS" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE.id}"
  resource_id = "${aws_api_gateway_resource.QA01_EHIInboundMS.id}"
  http_method = "${aws_api_gateway_method.QA01_EHIInboundMS.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}
####Audatex###
resource "aws_api_gateway_resource" "QA01_Audatex" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE.id}"
  parent_id   = "${aws_api_gateway_resource.QA01.id}"
  path_part   = "QA01_Audatex"
}

# Audatex Inbount Acknowledgment - ARMS
resource "aws_api_gateway_resource" "QA01_AudatexInboundAck" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE.id}"
  parent_id   = "${aws_api_gateway_resource.QA01_Audatex.id}"
  path_part   = "QA01_AudatexInboundAck"
}

resource "aws_api_gateway_method" "QA01_AudatexInboundAck" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE.id}"
  resource_id   = "${aws_api_gateway_resource.QA01_AudatexInboundAck.id}"
  http_method   = "POST"
  authorization = "CUSTOM"
  authorizer_id = "${aws_api_gateway_authorizer.NonProd_Auth0_Address.id}"
}

resource "aws_api_gateway_integration" "QA01_AudatexInboundAck" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE.id}"
  resource_id             = "${aws_api_gateway_resource.QA01_AudatexInboundAck.id}"
  http_method             = "${aws_api_gateway_method.QA01_AudatexInboundAck.http_method}"
  integration_http_method = "POST"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-qa01.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.qa01.gore.insurcloud.ca/audatex-service/v1/assignment/ack"
}

resource "aws_api_gateway_method_response" "QA01_AudatexInboundAck" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE.id}"
  resource_id = "${aws_api_gateway_resource.QA01_AudatexInboundAck.id}"
  http_method = "${aws_api_gateway_method.QA01_AudatexInboundAck.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}

##Audatex Inbound estimate
resource "aws_api_gateway_resource" "QA01_AudatexInboundEstimate" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE.id}"
  parent_id   = "${aws_api_gateway_resource.QA01_Audatex.id}"
  path_part   = "QA01_AudatexInboundEstimate"
}

resource "aws_api_gateway_method" "QA01_AudatexInboundEstimate" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE.id}"
  resource_id   = "${aws_api_gateway_resource.QA01_AudatexInboundEstimate.id}"
  http_method   = "POST"
  authorization = "CUSTOM"
  authorizer_id = "${aws_api_gateway_authorizer.NonProd_Auth0_Address.id}"
}


resource "aws_api_gateway_integration" "QA01_AudatexInboundEstimate" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE.id}"
  resource_id             = "${aws_api_gateway_resource.QA01_AudatexInboundEstimate.id}"
  http_method             = "${aws_api_gateway_method.QA01_AudatexInboundEstimate.http_method}"
  integration_http_method = "POST"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-qa01.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.qa01.gore.insurcloud.ca/audatex-service/v1/estimate"
}

resource "aws_api_gateway_method_response" "QA01_AudatexInboundEstimate" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE.id}"
  resource_id = "${aws_api_gateway_resource.QA01_AudatexInboundEstimate.id}"
  http_method = "${aws_api_gateway_method.QA01_AudatexInboundEstimate.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}
####end of audatex#####

# ###Cheque ###
resource "aws_api_gateway_resource" "QA01_Cheque" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE.id}"
  parent_id   = "${aws_api_gateway_resource.QA01.id}"
  path_part   = "QA01_Cheque"
}
  
# #Cheque --> F & 0 --> MS#####
resource "aws_api_gateway_resource" "QA01_ChequeInbound" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE.id}"
  parent_id   = "${aws_api_gateway_resource.QA01_Cheque.id}"
  path_part   = "QA01_ChequeInbound"
}

resource "aws_api_gateway_method" "QA01_ChequeInbound" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE.id}"
  resource_id   = "${aws_api_gateway_resource.QA01_ChequeInbound.id}"
  http_method   = "POST"
  authorization = "CUSTOM"
  authorizer_id = "${aws_api_gateway_authorizer.NonProd_Auth0_Cheque.id}"
}

resource "aws_api_gateway_integration" "QA01_ChequeInbound" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE.id}"
  resource_id             = "${aws_api_gateway_resource.QA01_ChequeInbound.id}"
  http_method             = "${aws_api_gateway_method.QA01_ChequeInbound.http_method}"
  integration_http_method = "POST"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-qa01.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.qa01.gore.insurcloud.ca/cheque-service/v1/cheque/updateChequeNumbers"
}

resource "aws_api_gateway_method_response" "QA01_ChequeInbound" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE.id}"
  resource_id = "${aws_api_gateway_resource.QA01_ChequeInbound.id}"
  http_method = "${aws_api_gateway_method.QA01_ChequeInbound.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}

# #Cheque --> GW --> MS#####
resource "aws_api_gateway_resource" "QA01_ChequeOutbound" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE.id}"
  parent_id   = "${aws_api_gateway_resource.QA01_Cheque.id}"
  path_part   = "QA01_ChequeOutbound"
}

resource "aws_api_gateway_method" "QA01_ChequeOutbound" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE.id}"
  resource_id   = "${aws_api_gateway_resource.QA01_ChequeOutbound.id}"
  http_method   = "POST"
  authorization = "CUSTOM"
  authorizer_id = "${aws_api_gateway_authorizer.NonProd_Auth0_Cheque.id}"
}

resource "aws_api_gateway_integration" "QA01_ChequeOutbound" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE.id}"
  resource_id             = "${aws_api_gateway_resource.QA01_ChequeOutbound.id}"
  http_method             = "${aws_api_gateway_method.QA01_ChequeOutbound.http_method}"
  integration_http_method = "POST"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-qa01.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.qa01.gore.insurcloud.ca/cheque-service/v1/cheque/print"
}

resource "aws_api_gateway_method_response" "QA01_ChequeOutbound" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE.id}"
  resource_id = "${aws_api_gateway_resource.QA01_ChequeOutbound.id}"
  http_method = "${aws_api_gateway_method.QA01_ChequeOutbound.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}

# ###Impact###
resource "aws_api_gateway_resource" "QA01_Impact" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE.id}"
  parent_id   = "${aws_api_gateway_resource.QA01.id}"
  path_part   = "QA01_Impact"
}

# # Impact Outbound GW -API Gateway - ARMS
resource "aws_api_gateway_method" "QA01_Impact" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE.id}"
  resource_id   = "${aws_api_gateway_resource.QA01_Impact.id}"
  http_method   = "POST"
  authorization = "CUSTOM"
  authorizer_id = "${aws_api_gateway_authorizer.NonProd_Auth0_Address.id}"
}

resource "aws_api_gateway_integration" "QA01_Impact" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE.id}"
  resource_id             = "${aws_api_gateway_resource.QA01_Impact.id}"
  http_method             = "${aws_api_gateway_method.QA01_Impact.http_method}"
  integration_http_method = "POST"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-qa01.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.qa01.gore.insurcloud.ca/genericassignmentservice.svc"
}

resource "aws_api_gateway_method_response" "QA01_Impact" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE.id}"
  resource_id = "${aws_api_gateway_resource.QA01_Impact.id}"
  http_method = "${aws_api_gateway_method.QA01_Impact.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}


# ###HCAI###
resource "aws_api_gateway_resource" "QA01_HCAI" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE.id}"
  parent_id   = "${aws_api_gateway_resource.QA01.id}"
  path_part   = "QA01_HCAI"
}

# # HCAI Outbound GW -API Gateway - ARMS
resource "aws_api_gateway_method" "QA01_HCAI" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE.id}"
  resource_id   = "${aws_api_gateway_resource.QA01_HCAI.id}"
  http_method   = "POST"
  authorization = "NONE"
}

resource "aws_api_gateway_integration" "QA01_HCAI" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE.id}"
  resource_id             = "${aws_api_gateway_resource.QA01_HCAI.id}"
  http_method             = "${aws_api_gateway_method.QA01_HCAI.http_method}"
  integration_http_method = "POST"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-qa01.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.qa01.gore.insurcloud.ca/soap-security-service/v1/proxy"
}

resource "aws_api_gateway_method_response" "QA01_HCAI" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE.id}"
  resource_id = "${aws_api_gateway_resource.QA01_HCAI.id}"
  http_method = "${aws_api_gateway_method.QA01_HCAI.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}

# ###CRC###
resource "aws_api_gateway_resource" "QA01_CRCInbound" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE.id}"
  parent_id   = "${aws_api_gateway_resource.QA01.id}"
  path_part   = "QA01_CRCInbound"
}

# #CRC --> Guidewire
resource "aws_api_gateway_method" "QA01_CRCInbound" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE.id}"
  resource_id   = "${aws_api_gateway_resource.QA01_CRCInbound.id}"
  http_method   = "POST"
  authorization = "NONE"
}

resource "aws_api_gateway_integration" "QA01_CRCInbound" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE.id}"
  resource_id             = "${aws_api_gateway_resource.QA01_CRCInbound.id}"
  http_method             = "${aws_api_gateway_method.QA01_CRCInbound.http_method}"
  integration_http_method = "POST"
  connection_type         = "INTERNET"
  type                    = "HTTP_PROXY"
  uri                     = "https://qa.goredev.guidewire.net/cc/ws/ic/integration/crc/CollisionReportingCenterAPI"
}

resource "aws_api_gateway_method_response" "QA01_CRCInbound" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE.id}"
  resource_id = "${aws_api_gateway_resource.QA01_CRCInbound.id}"
  http_method = "${aws_api_gateway_method.QA01_CRCInbound.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}

#CRC --> MS 

resource "aws_api_gateway_resource" "QA01_CRCInboundMS" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE.id}"
  parent_id   = "${aws_api_gateway_resource.QA01_CRCInbound.id}"
  path_part   = "QA01_CRCInboundMS"
}


resource "aws_api_gateway_method" "QA01_CRCInboundMS" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE.id}"
  resource_id   = "${aws_api_gateway_resource.QA01_CRCInboundMS.id}"
  http_method   = "POST"
  authorization = "NONE"
}

resource "aws_api_gateway_integration" "QA01_CRCInboundMS" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE.id}"
  resource_id             = "${aws_api_gateway_resource.QA01_CRCInboundMS.id}"
  http_method             = "${aws_api_gateway_method.QA01_CRCInboundMS.http_method}"
  integration_http_method = "POST"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-qa01.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.qa01.gore.insurcloud.ca/crc-service/microservices/crc/ws/"
}

resource "aws_api_gateway_method_response" "QA01_CRCInboundMS" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE.id}"
  resource_id = "${aws_api_gateway_resource.QA01_CRCInboundMS.id}"
  http_method = "${aws_api_gateway_method.QA01_CRCInboundMS.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}

# ###Uniban###
resource "aws_api_gateway_resource" "QA01_Uniban" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE.id}"
  parent_id   = "${aws_api_gateway_resource.QA01.id}"
  path_part   = "QA01_Uniban"
}

##Uniban --> API
resource "aws_api_gateway_method" "QA01_Uniban" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE.id}"
  resource_id   = "${aws_api_gateway_resource.QA01_Uniban.id}"
  http_method   = "GET"
  authorization = "NONE"
  request_parameters = {
    "method.request.querystring.policyNumber"= true
    "method.request.querystring.dataOfLoss"= true
    "method.request.querystring.vin"= true
    }
}

resource "aws_api_gateway_integration" "QA01_Uniban" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE.id}"
  resource_id             = "${aws_api_gateway_resource.QA01_Uniban.id}"
  http_method             = "${aws_api_gateway_method.QA01_Uniban.http_method}"
  integration_http_method = "GET"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-qa01.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.qa01.gore.insurcloud.ca/uniban-service/v1/retrievePolicy"
}

resource "aws_api_gateway_method_response" "QA01_Uniban" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE.id}"
  resource_id = "${aws_api_gateway_resource.QA01_Uniban.id}"
  http_method = "${aws_api_gateway_method.QA01_Uniban.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}

###RESOURCE GROUPS###	
###DEV02####
resource "aws_api_gateway_resource" "DEV02" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE.id}"
  parent_id   = "${aws_api_gateway_rest_api.GORE.root_resource_id}"
  path_part   = "DEV02"
}

###Enterprise###
resource "aws_api_gateway_resource" "DEV02_Enterprise" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE.id}"
  parent_id   = "${aws_api_gateway_resource.DEV02.id}"
  path_part   = "DEV02_Enterprise"
}

# Enterprise Outbound GW -API Gateway - ARMS
resource "aws_api_gateway_resource" "DEV02_EHIOutbound" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE.id}"
  parent_id   = "${aws_api_gateway_resource.DEV02_Enterprise.id}"
  path_part   = "DEV02_EHIOutbound"
}

resource "aws_api_gateway_method" "DEV02_EHIOutbound" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE.id}"
  resource_id   = "${aws_api_gateway_resource.DEV02_EHIOutbound.id}"
  http_method   = "POST"
  authorization = "NONE"
}

resource "aws_api_gateway_integration" "DEV02_EHIOutbound" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE.id}"
  resource_id             = "${aws_api_gateway_resource.DEV02_EHIOutbound.id}"
  http_method             = "${aws_api_gateway_method.DEV02_EHIOutbound.http_method}"
  integration_http_method = "POST"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-dev02.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.dev02.gore.insurcloud.ca/soap-security-service/v1/proxy"
}

resource "aws_api_gateway_method_response" "DEV02_EHIOutbound" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE.id}"
  resource_id = "${aws_api_gateway_resource.DEV02_EHIOutbound.id}"
  http_method = "${aws_api_gateway_method.DEV02_EHIOutbound.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}

# #Enterprise --> API gateway --> GW
resource "aws_api_gateway_resource" "DEV02_EHIInboundGW" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE.id}"
  parent_id   = "${aws_api_gateway_resource.DEV02_Enterprise.id}"
  path_part   = "DEV02_EHIInboundGW"
}

# #Update Notification request
resource "aws_api_gateway_method" "DEV02_EHIInboundGW" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE.id}"
  resource_id   = "${aws_api_gateway_resource.DEV02_EHIInboundGW.id}"
  http_method   = "POST"
  authorization = "NONE"
}

resource "aws_api_gateway_integration" "DEV02_EHIInboundGW" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE.id}"
  resource_id             = "${aws_api_gateway_resource.DEV02_EHIInboundGW.id}"
  http_method             = "${aws_api_gateway_method.DEV02_EHIInboundGW.http_method}"
  integration_http_method = "POST"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-dev02.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.dev02.gore.insurcloud.ca/soap-security-service/v1/inbound-proxy"}

resource "aws_api_gateway_method_response" "DEV02_EHIInboundGW" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE.id}"
  resource_id = "${aws_api_gateway_resource.DEV02_EHIInboundGW.id}"
  http_method = "${aws_api_gateway_method.DEV02_EHIInboundGW.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}

# #Enterprise --> API gateway --> Enterprise MS --> GW & Document MS
resource "aws_api_gateway_resource" "DEV02_EHIInboundMS" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE.id}"
  parent_id   = "${aws_api_gateway_resource.DEV02_Enterprise.id}"
  path_part   = "DEV02_EHIInboundMS"
}

# #Update Notification request
resource "aws_api_gateway_method" "DEV02_EHIInboundMS" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE.id}"
  resource_id   = "${aws_api_gateway_resource.DEV02_EHIInboundMS.id}"
  http_method   = "POST"
  authorization = "NONE"
}

resource "aws_api_gateway_integration" "DEV02_EHIInboundMS" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE.id}"
  resource_id             = "${aws_api_gateway_resource.DEV02_EHIInboundMS.id}"
  http_method             = "${aws_api_gateway_method.DEV02_EHIInboundMS.http_method}"
  integration_http_method = "POST"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-dev02.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.dev02.gore.insurcloud.ca/car-rental-service/v1/rental/sendInvoiceNotificationRequest"
}

resource "aws_api_gateway_method_response" "DEV02_EHIInboundMS" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE.id}"
  resource_id = "${aws_api_gateway_resource.DEV02_EHIInboundMS.id}"
  http_method = "${aws_api_gateway_method.DEV02_EHIInboundMS.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}

# ###Impact###
resource "aws_api_gateway_resource" "DEV02_Impact" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE.id}"
  parent_id   = "${aws_api_gateway_resource.DEV02.id}"
  path_part   = "DEV02_Impact"
}

# # Impact Outbound GW -API Gateway - ARMS
resource "aws_api_gateway_method" "DEV02_Impact" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE.id}"
  resource_id   = "${aws_api_gateway_resource.DEV02_Impact.id}"
  http_method   = "POST"
  authorization = "CUSTOM"
  authorizer_id = "${aws_api_gateway_authorizer.NonProd_Auth0_Address.id}"
}

resource "aws_api_gateway_integration" "DEV02_Impact" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE.id}"
  resource_id             = "${aws_api_gateway_resource.DEV02_Impact.id}"
  http_method             = "${aws_api_gateway_method.DEV02_Impact.http_method}"
  integration_http_method = "POST"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-dev02.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.dev02.gore.insurcloud.ca/genericassignmentservice.svc"
}

resource "aws_api_gateway_method_response" "DEV02_Impact" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE.id}"
  resource_id = "${aws_api_gateway_resource.DEV02_Impact.id}"
  http_method = "${aws_api_gateway_method.DEV02_Impact.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}


# ###HCAI###
resource "aws_api_gateway_resource" "DEV02_HCAI" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE.id}"
  parent_id   = "${aws_api_gateway_resource.DEV02.id}"
  path_part   = "DEV02_HCAI"
}

# # HCAI Outbound GW -API Gateway - ARMS
resource "aws_api_gateway_method" "DEV02_HCAI" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE.id}"
  resource_id   = "${aws_api_gateway_resource.DEV02_HCAI.id}"
  http_method   = "POST"
  authorization = "NONE"
}

resource "aws_api_gateway_integration" "DEV02_HCAI" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE.id}"
  resource_id             = "${aws_api_gateway_resource.DEV02_HCAI.id}"
  http_method             = "${aws_api_gateway_method.DEV02_HCAI.http_method}"
  integration_http_method = "POST"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-dev02.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.dev02.gore.insurcloud.ca/soap-security-service/v1/proxy"
}

resource "aws_api_gateway_method_response" "DEV02_HCAI" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE.id}"
  resource_id = "${aws_api_gateway_resource.DEV02_HCAI.id}"
  http_method = "${aws_api_gateway_method.DEV02_HCAI.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}

# ###CRC###
resource "aws_api_gateway_resource" "DEV02_CRCInbound" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE.id}"
  parent_id   = "${aws_api_gateway_resource.DEV02.id}"
  path_part   = "DEV02_CRCInbound"
}

# #CRC --> Guidewire
resource "aws_api_gateway_method" "DEV02_CRCInbound" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE.id}"
  resource_id   = "${aws_api_gateway_resource.DEV02_CRCInbound.id}"
  http_method   = "POST"
  authorization = "NONE"
}

resource "aws_api_gateway_integration" "DEV02_CRCInbound" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE.id}"
  resource_id             = "${aws_api_gateway_resource.DEV02_CRCInbound.id}"
  http_method             = "${aws_api_gateway_method.DEV02_CRCInbound.http_method}"
  integration_http_method = "POST"
  connection_type         = "INTERNET"
  type                    = "HTTP_PROXY"
  uri                     = "https://dev2.goredev.guidewire.net/cc/ws/ic/integration/crc/CollisionReportingCenterAPI"
}

resource "aws_api_gateway_method_response" "DEV02_CRCInbound" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE.id}"
  resource_id = "${aws_api_gateway_resource.DEV02_CRCInbound.id}"
  http_method = "${aws_api_gateway_method.DEV02_CRCInbound.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}

#CRC --> MS 
resource "aws_api_gateway_resource" "DEV02_CRCInboundMS" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE.id}"
  parent_id   = "${aws_api_gateway_resource.DEV02_CRCInbound.id}"
  path_part   = "DEV02_CRCInboundMS"
}

resource "aws_api_gateway_method" "DEV02_CRCInboundMS" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE.id}"
  resource_id   = "${aws_api_gateway_resource.DEV02_CRCInboundMS.id}"
  http_method   = "POST"
  authorization = "NONE"
}

resource "aws_api_gateway_integration" "DEV02_CRCInboundMS" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE.id}"
  resource_id             = "${aws_api_gateway_resource.DEV02_CRCInboundMS.id}"
  http_method             = "${aws_api_gateway_method.DEV02_CRCInboundMS.http_method}"
  integration_http_method = "POST"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-dev02.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.dev02.gore.insurcloud.ca/crc-service/microservices/crc/ws/"
}

resource "aws_api_gateway_method_response" "DEV02_CRCInboundMS" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE.id}"
  resource_id = "${aws_api_gateway_resource.DEV02_CRCInboundMS.id}"
  http_method = "${aws_api_gateway_method.DEV02_CRCInboundMS.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}

# ###Cheque ###
resource "aws_api_gateway_resource" "DEV02_Cheque" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE.id}"
  parent_id   = "${aws_api_gateway_resource.DEV02.id}"
  path_part   = "DEV02_Cheque"
}
# #Cheque --> F & 0 --> MS#####
resource "aws_api_gateway_resource" "DEV02_ChequeInbound" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE.id}"
  parent_id   = "${aws_api_gateway_resource.DEV02_Cheque.id}"
  path_part   = "DEV02_ChequeInbound"
}

resource "aws_api_gateway_method" "DEV02_ChequeInbound" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE.id}"
  resource_id   = "${aws_api_gateway_resource.DEV02_ChequeInbound.id}"
  http_method   = "POST"
  authorization = "CUSTOM"
  authorizer_id = "${aws_api_gateway_authorizer.NonProd_Auth0_Cheque.id}"
}

resource "aws_api_gateway_integration" "DEV02_ChequeInbound" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE.id}"
  resource_id             = "${aws_api_gateway_resource.DEV02_ChequeInbound.id}"
  http_method             = "${aws_api_gateway_method.DEV02_ChequeInbound.http_method}"
  integration_http_method = "POST"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-dev02.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.dev02.gore.insurcloud.ca/cheque-service/v1/cheque/updateChequeNumbers"
}

resource "aws_api_gateway_method_response" "DEV02_ChequeInbound" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE.id}"
  resource_id = "${aws_api_gateway_resource.DEV02_ChequeInbound.id}"
  http_method = "${aws_api_gateway_method.DEV02_ChequeInbound.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}

# #Cheque --> GW --> MS#####
resource "aws_api_gateway_resource" "DEV02_ChequeOutbound" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE.id}"
  parent_id   = "${aws_api_gateway_resource.DEV02_Cheque.id}"
  path_part   = "DEV02_ChequeOutbound"
}

resource "aws_api_gateway_method" "DEV02_ChequeOutbound" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE.id}"
  resource_id   = "${aws_api_gateway_resource.DEV02_ChequeOutbound.id}"
  http_method   = "POST"
  authorization = "CUSTOM"
  authorizer_id = "${aws_api_gateway_authorizer.NonProd_Auth0_Cheque.id}"
}

resource "aws_api_gateway_integration" "DEV02_ChequeOutbound" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE.id}"
  resource_id             = "${aws_api_gateway_resource.DEV02_ChequeOutbound.id}"
  http_method             = "${aws_api_gateway_method.DEV02_ChequeOutbound.http_method}"
  integration_http_method = "POST"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-dev02.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.dev02.gore.insurcloud.ca/cheque-service/v1/cheque/print"
}

resource "aws_api_gateway_method_response" "DEV02_ChequeOutbound" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE.id}"
  resource_id = "${aws_api_gateway_resource.DEV02_ChequeOutbound.id}"
  http_method = "${aws_api_gateway_method.DEV02_ChequeOutbound.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}

####Audatex###
resource "aws_api_gateway_resource" "DEV02_Audatex" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE.id}"
  parent_id   = "${aws_api_gateway_resource.DEV02.id}"
  path_part   = "DEV02_Audatex"
}

# Audatex Inbount Acknowledgment - ARMS
resource "aws_api_gateway_resource" "DEV02_AudatexInboundAck" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE.id}"
  parent_id   = "${aws_api_gateway_resource.DEV02_Audatex.id}"
  path_part   = "DEV02_AudatexInboundAck"
}

resource "aws_api_gateway_method" "DEV02_AudatexInboundAck" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE.id}"
  resource_id   = "${aws_api_gateway_resource.DEV02_AudatexInboundAck.id}"
  http_method   = "POST"
  authorization = "CUSTOM"
  authorizer_id = "${aws_api_gateway_authorizer.NonProd_Auth0_Address.id}"
}

resource "aws_api_gateway_integration" "DEV02_AudatexInboundAck" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE.id}"
  resource_id             = "${aws_api_gateway_resource.DEV02_AudatexInboundAck.id}"
  http_method             = "${aws_api_gateway_method.DEV02_AudatexInboundAck.http_method}"
  integration_http_method = "POST"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-dev02.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.dev02.gore.insurcloud.ca/audatex-service/v1/assignment/ack"
}

resource "aws_api_gateway_method_response" "DEV02_AudatexInboundAck" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE.id}"
  resource_id = "${aws_api_gateway_resource.DEV02_AudatexInboundAck.id}"
  http_method = "${aws_api_gateway_method.DEV02_AudatexInboundAck.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}

##Audatex Inbound estimate
resource "aws_api_gateway_resource" "DEV02_AudatexInboundEstimate" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE.id}"
  parent_id   = "${aws_api_gateway_resource.DEV02_Audatex.id}"
  path_part   = "DEV02_AudatexInboundEstimate"
}

resource "aws_api_gateway_method" "DEV02_AudatexInboundEstimate" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE.id}"
  resource_id   = "${aws_api_gateway_resource.DEV02_AudatexInboundEstimate.id}"
  http_method   = "POST"
  authorization = "CUSTOM"
  authorizer_id = "${aws_api_gateway_authorizer.NonProd_Auth0_Address.id}"
}


resource "aws_api_gateway_integration" "DEV02_AudatexInboundEstimate" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE.id}"
  resource_id             = "${aws_api_gateway_resource.DEV02_AudatexInboundEstimate.id}"
  http_method             = "${aws_api_gateway_method.DEV02_AudatexInboundEstimate.http_method}"
  integration_http_method = "POST"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-dev02.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.dev02.gore.insurcloud.ca/audatex-service/v1/estimate"
}

resource "aws_api_gateway_method_response" "DEV02_AudatexInboundEstimate" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE.id}"
  resource_id = "${aws_api_gateway_resource.DEV02_AudatexInboundAck.id}"
  http_method = "${aws_api_gateway_method.DEV02_AudatexInboundEstimate.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}
####end of audatex#####

###RESOURCE GROUPS###	
###Preprod####
resource "aws_api_gateway_resource" "Preprod" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE.id}"
  parent_id   = "${aws_api_gateway_rest_api.GORE.root_resource_id}"
  path_part   = "Preprod"
}

###Earnix###
resource "aws_api_gateway_resource" "Preprod_Earnix" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE.id}"
  parent_id   = "${aws_api_gateway_resource.Preprod.id}"
  path_part   = "Preprod_Earnix"
}

# GET Earnix -API Gateway - ARMS
resource "aws_api_gateway_resource" "Preprod_Earnix_Get" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE.id}"
  parent_id   = "${aws_api_gateway_resource.Preprod_Earnix.id}"
  path_part   = "Preprod_Earnix_Get"
}

resource "aws_api_gateway_method" "Preprod_Earnix_Get" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE.id}"
  resource_id   = "${aws_api_gateway_resource.Preprod_Earnix_Get.id}"
  http_method   = "GET"
  authorization = "CUSTOM"
  authorizer_id = "${aws_api_gateway_authorizer.NonProd_Auth0_Address.id}"

}

#to-do - update the proxy server to the one in perf
resource "aws_api_gateway_integration" "Preprod_Earnix_Get" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE.id}"
  resource_id             = "${aws_api_gateway_resource.Preprod_Earnix_Get.id}"
  http_method             = "${aws_api_gateway_method.Preprod_Earnix_Get.http_method}"
  integration_http_method = "GET"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-dev01.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.dev01.gore.insurcloud.ca/earnix-get"
}

resource "aws_api_gateway_method_response" "Preprod_Earnix_Get" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE.id}"
  resource_id = "${aws_api_gateway_resource.Preprod_Earnix_Get.id}"
  http_method = "${aws_api_gateway_method.Preprod_Earnix_Get.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}

# #Post Earnix --> API gateway --> GW
resource "aws_api_gateway_resource" "Preprod_Earnix_Post" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE.id}"
  parent_id   = "${aws_api_gateway_resource.Preprod_Earnix.id}"
  path_part   = "Preprod_Earnix_Post"
}

resource "aws_api_gateway_method" "Preprod_Earnix_Post" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE.id}"
  resource_id   = "${aws_api_gateway_resource.Preprod_Earnix_Post.id}"
  http_method   = "POST"
  authorization = "NONE"
}

#to-do - update the proxy server to the one in perf
resource "aws_api_gateway_integration" "Preprod_Earnix_Post" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE.id}"
  resource_id             = "${aws_api_gateway_resource.Preprod_Earnix_Post.id}"
  http_method             = "${aws_api_gateway_method.Preprod_Earnix_Post.http_method}"
  integration_http_method = "POST"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-dev01.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.dev01.gore.insurcloud.ca/earnix-post"
}

resource "aws_api_gateway_method_response" "Preprod_Earnix_Post" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE.id}"
  resource_id = "${aws_api_gateway_resource.Preprod_Earnix_Post.id}"
  http_method = "${aws_api_gateway_method.Preprod_Earnix_Post.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}