#Envrionment specific variables
client_name = "IC-GORE"
environment_name = "HUB"
environment_type = "NP"
client_number = "A004"
#ad-dc1 = "10.10.4.10"
#ad-dc2 = "10.10.6.10"
volume_key = "arn:aws:kms:ca-central-1:963473599983:key/927c1536-38fb-40d3-b9f0-74780ffccf59"
ami_id_windows2012R2sql14enterprise = "ami-0b7e5050eee245b99"
ami_id_windows2012R2sql14standard = "ami-0913a2afb4fa5fc5b"
ami_id_windows2019container = "ami-013453540e1aa0d48"
ami_id_windows2019base = "ami-06b8f69a907cf6bb2"
ami_id_windows2016base = "ami-0f279f997ff6cb1f5"
ami_id_windows2012R2base = "ami-08f7dedaab95e4621"
ami_id_windows2008base = "ami-0446e08d2d99fc8c9"
ami_id_redhatlinux75 = "ami-05c33d286020a47f9"
ami_id_ubuntu1804baseimage = "ami-032172cc5f34b32fc"
ami_id_ubuntu1604baseimage = "ami-04f40cca01b3186ea"
ami_id_amazonlinux201712base = "ami-0a777bbf8ef3f43bf"
ami_id_amazonlinux201709base = "ami-058dba934995c5468"
ami_id_amazonlinux2base = "ami-04978aa25eaba28b7"
ami_id_amazonlinux201803base = "ami-06829694f407e15c1"
ami_id_amazonlinuxbase20200506 = "ami-071594d49d11fcb38"


#VPC Specific variables
vpc_id = "vpc-0a4550ad55f78b1a1"
vpc_cidr = "192.168.20.0/22"
#sg_cidr = "XXX"

gore-hub-database-1a-subnet = "subnet-00f12b4fb4d404e3d"
gore-hub-database-1b-subnet = "subnet-0d9b81d383a31736b"
gore-hub-private-1a-subnet = "subnet-0df29e5f36dfd109c"
gore-hub-private-1b-subnet = "subnet-01c41cb8583bab308"
gore-hub-public-1a-subnet = "subnet-0ecc5504950642ee8"
gore-hub-public-1b-subnet = "subnet-0d1c3605ef2bb196d"

#R53 
zone_id = "Z10463761BA0B5FS1ZRWW"
public_zone_id = "Z082279634FQSHATPRT1G"

#Accounts
gore-dev-account = "780193124257"
gore-prod-account = "948423377884"
gore-hub-account = "963473599983"
