pipeline {
    agent any

    environment {
      tf_version = '0.11.11'
      config_path = 'IC-Economical-HUB'
    }

    parameters {
        booleanParam(defaultValue: false, description: 'Set Value to True to Initiate Destroy Stage', name: 'destroy')
    }

    options {
        buildDiscarder(logRotator(numToKeepStr: '10'))
        disableConcurrentBuilds()
    }

    stages {
        stage('TerraRising') {
            steps {
                 load "${config_path}/parameters.groovy"
                 sh '''#!/bin/bash -l
                    unset AWS_ACCESS_KEY_ID
                    unset AWS_SECRET_ACCESS_KEY
                    unset AWS_SESSION_TOKEN
                        
                    temp_role=$(aws sts assume-role \
                        --role-arn "arn:aws:iam::393766723611:role/admin_ops_auto" \
                        --role-session-name "session1")

                    export AWS_ACCESS_KEY_ID=$(echo $temp_role | jq .Credentials.AccessKeyId | xargs)
                    export AWS_SECRET_ACCESS_KEY=$(echo $temp_role | jq .Credentials.SecretAccessKey | xargs)
                    export AWS_SESSION_TOKEN=$(echo $temp_role | jq .Credentials.SessionToken | xargs)

                    # Setting up Terraform in build environment
                    echo "Downloading Terraform ${tf_version}"
                    wget -q https://releases.hashicorp.com/terraform/${tf_version}/terraform_${tf_version}_linux_amd64.zip
                    unzip -qo terraform_${tf_version}_linux_amd64.zip
                    chmod 755 terraform
                    ./terraform --version
                    
                    rm -rf .terraform

                    # Setups up Terraform state file and source modules
                    set -x #Turns on screen outputs
                    ./terraform init -backend-config="access_key=$AWS_ACCESS_KEY_ID" -backend-config="secret_key=$AWS_SECRET_ACCESS_KEY" -backend-config="token=$AWS_SESSION_TOKEN" ./$tf_path

                 '''
                 
            }
        }
        stage('TerraPlanning') {
            when {
                expression { !params.destroy }
            }
            steps {
                load "${config_path}/parameters.groovy"
                echo "Planning $account_moniker in ... $aws_account / $aws_region"
                sh '''#!/bin/bash -l

                    echo "Terraform Plan of $account_moniker Build in ... $aws_account / $aws_region"
                    echo "============================ Planning for InsurCloud Economical Hub Servers ============================"
                    unset AWS_ACCESS_KEY_ID
                    unset AWS_SECRET_ACCESS_KEY
                    unset AWS_SESSION_TOKEN

                    ./terraform plan -var-file="IC-Economical-HUB/terraform.tfvars" -out=current.tfplan ./$tf_path
                    '''
                  
            }
        }

        stage('TerraDestroy') {
            when {
                expression { params.destroy }
            }
            steps {
                load "${config_path}/parameters.groovy"
                echo "Terraform Destroy of $account_moniker in ... $aws_account / $aws_region"
                sh '''#!/bin/bash -l
                    echo "Terraform Destroy of $account_moniker in ... $aws_account / $aws_region"
                    echo "============================ InsurCloud Economical Servers Destorying ============================"
                    unset AWS_ACCESS_KEY_ID
                    unset AWS_SECRET_ACCESS_KEY
                    unset AWS_SESSION_TOKEN
                    
                    temp_role=$(aws sts assume-role \
                    --role-arn "arn:aws:iam::${aws_account}:role/admin_ops_auto" \
                    --role-session-name "session1")

                    export AWS_ACCESS_KEY_ID=$(echo $temp_role | jq .Credentials.AccessKeyId | xargs)
                    export AWS_SECRET_ACCESS_KEY=$(echo $temp_role | jq .Credentials.SecretAccessKey | xargs)
                    export AWS_SESSION_TOKEN=$(echo $temp_role | jq .Credentials.SessionToken | xargs)

                    ./terraform destroy -auto-approve ./$tf_path
                  '''
            }
        }

        stage("ValidateBeforeDeploy") {
            when {
                expression { !params.destroy }
            }
            steps {
                input 'Are you sure? Review the output of the previous step before proceeding!'
            }
        }

        stage('TerraApplying') {
            when {
                expression { !params.destroy }
            }
            steps {
                load "${config_path}/parameters.groovy"
                sh '''#!/bin/bash -l
                    echo "Terraform Apply of $account_moniker Build in ... $aws_account / $aws_region"
                    echo "============================ InsurCloud Economical Hub Servers Apply ============================"
                    unset AWS_ACCESS_KEY_ID
                    unset AWS_SECRET_ACCESS_KEY
                    unset AWS_SESSION_TOKEN
                    
                    temp_role=$(aws sts assume-role \
                    --role-arn "arn:aws:iam::${aws_account}:role/admin_ops_auto" \
                    --role-session-name "session1")

                    export AWS_ACCESS_KEY_ID=$(echo $temp_role | jq .Credentials.AccessKeyId | xargs)
                    export AWS_SECRET_ACCESS_KEY=$(echo $temp_role | jq .Credentials.SecretAccessKey | xargs)
                    export AWS_SESSION_TOKEN=$(echo $temp_role | jq .Credentials.SessionToken | xargs)

                    ./terraform apply current.tfplan
                  '''
        }
    }}
    post {
        failure {
            echo "Error occurred. --> ${currentBuild.fullDisplayName} --> ${env.BUILD_URL}"
        }
    }
}

