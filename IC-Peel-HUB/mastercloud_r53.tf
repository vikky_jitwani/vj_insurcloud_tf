// #=======================================================================
// # HUB 
// #=======================================================================
resource "aws_route53_record" "ICPEEL_EXSTREAM_LIC_1_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "exstreamlic1.peel.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${aws_instance.ICPEEL_EXSTREAM_LIC_1.private_ip}"]
}

resource "aws_route53_record" "ICPEEL_EXSTREAM_LIC_2_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "exstreamlic2.peel.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${aws_instance.ICPEEL_EXSTREAM_LIC_2.private_ip}"]
}

resource "aws_route53_record" "ICPEEL_EXSTREAM_LIC_3_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "exstreamlic3.peel.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${aws_instance.ICPEEL_EXSTREAM_LIC_3.private_ip}"]
}

resource "aws_route53_record" "ICPEEL_NEXUS_REG_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "nexus.peel.insurcloud.ca"
    type    = "A"

    alias {
        name                   = "internal-cicd-internal-alb-2023433918.ca-central-1.elb.amazonaws.com"
        zone_id                = "ZQSVJUPU6J1EY"
        evaluate_target_health = false
    }
}

resource "aws_route53_record" "ICPEEL_NEXUS_PROD_REG_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "nexus-prod.peel.insurcloud.ca"
    type    = "A"

    alias {
        name                   = "internal-cicd-internal-alb-2023433918.ca-central-1.elb.amazonaws.com"
        zone_id                = "ZQSVJUPU6J1EY"
        evaluate_target_health = false
    }
}

resource "aws_route53_record" "ICPEEL_NEXUS_UI_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "nexus-ui.peel.insurcloud.ca"
    type    = "A"

    alias {
        name                   = "internal-cicd-internal-alb-2023433918.ca-central-1.elb.amazonaws.com"
        zone_id                = "ZQSVJUPU6J1EY"
        evaluate_target_health = false
    }
}

resource "aws_route53_record" "ICPEEL_SONARQUBE_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "sonarqube.peel.insurcloud.ca"
    type    = "A"

    alias {
        name                   = "internal-cicd-internal-alb-2023433918.ca-central-1.elb.amazonaws.com"
        zone_id                = "ZQSVJUPU6J1EY"
        evaluate_target_health = false
    }
}

resource "aws_route53_record" "ICPEEL_CONSUL_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "consul.peel.insurcloud.ca"
    type    = "A"

    alias {
        name                   = "internal-cicd-internal-alb-2023433918.ca-central-1.elb.amazonaws.com"
        zone_id                = "ZQSVJUPU6J1EY"
        evaluate_target_health = false
    }
}

resource "aws_route53_record" "ICPEEL_JENKINS_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "jenkins.peel.insurcloud.ca"
    type    = "A"

    alias {
        name                   = "internal-cicd-internal-alb-2023433918.ca-central-1.elb.amazonaws.com"
        zone_id                = "ZQSVJUPU6J1EY"
        evaluate_target_health = false
    }
}

resource "aws_route53_record" "ICPEEL_VAULT_PPS_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "vault.pps.peel.insurcloud.ca"
    type    = "A"

    alias {
        name                   = "a-mc-cac-d-a003-ic-pps-vault-84458cafb01e039e.elb.ca-central-1.amazonaws.com"
        zone_id                = "Z2EPGBW3API2WT"
        evaluate_target_health = false
    }
}

resource "aws_route53_record" "ICPEEL_VAULT_PROD_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "vault.prod.peel.insurcloud.ca"
    type    = "A"

    alias {
        name                   = "a-mc-cac-d-a003-ic-prod-vault-7b0fa62bf2511ffc.elb.ca-central-1.amazonaws.com"
        zone_id                = "Z2EPGBW3API2WT"
        evaluate_target_health = false
    }
}

# #=======================================================================
# # DEV01
# #=======================================================================

######### GUIDEWIRE ##############
resource "aws_route53_record" "ICPEEL_DEV01_GUIDEWIRE_SUITE_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "gw1.dev01.peel.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICPEEL_DEV01_GUIDEWIRE_SUITE_PRIVATEIP}"]
}

resource "aws_route53_record" "ICPEEL_DEV01_INTERNAL_NLB_GUIDEWIRE_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "gw.dev01.peel.insurcloud.ca"
    type    = "A"
    alias {
        name                   = "${data.terraform_remote_state.dev_remote_state.ICPEEL_DEV01_INTERNAL_NLB_DNS_NAME}"
        zone_id                = "${data.terraform_remote_state.dev_remote_state.ICPEEL_DEV01_INTERNAL_NLB_ZONE_ID}"
        evaluate_target_health = false
    }
}

resource "aws_route53_record" "ICPEEL_DEV01_GUIDEWIRE_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "gw-dev01.nonprod.peel.insurcloud.ca"
    type    = "A"
    alias {
        name                   = "${data.terraform_remote_state.dev_remote_state.ICPEEL_DEV01_INTERNAL_NLB_DNS_NAME}"
        zone_id                = "${data.terraform_remote_state.dev_remote_state.ICPEEL_DEV01_INTERNAL_NLB_ZONE_ID}"
        evaluate_target_health = false
    }
}

resource "aws_route53_record" "ICPEEL_DEV01_BROKER_NLB_GUIDEWIRE_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "is.dev01.peel.insurcloud.ca"
    type    = "A"
    alias {
        name                   = "${aws_lb.ICPEEL_DEV01_BROKER_NLB.dns_name}"
        zone_id                = "${aws_lb.ICPEEL_DEV01_BROKER_NLB.zone_id}"
        evaluate_target_health = false
    }
}

resource "aws_route53_record" "ICPEEL_DEV01_PUBLIC_GUIDEWIRE_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "is-dev01.nonprod.peel.insurcloud.ca"
    type    = "A"
    alias {
        name                   = "${aws_lb.ICPEEL_DEV01_BROKER_NLB.dns_name}"
        zone_id                = "${aws_lb.ICPEEL_DEV01_BROKER_NLB.zone_id}"
        evaluate_target_health = false
    }
}

######### MICROSERVICES ###########
resource "aws_route53_record" "ICPEEL_DEV01_MICROSERVICES_APPLICATION_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "ms1.dev01.peel.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICPEEL_DEV01_MICROSERVICES_PRIVATEIP}"]
}

resource "aws_route53_record" "ICPEEL_DEV01_INTERNAL_NLB_MICROSERVICES_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "ms.dev01.peel.insurcloud.ca"
    type    = "A"
    alias {
        name                   = "${data.terraform_remote_state.dev_remote_state.ICPEEL_DEV01_INTERNAL_NLB_DNS_NAME}"
        zone_id                = "${data.terraform_remote_state.dev_remote_state.ICPEEL_DEV01_INTERNAL_NLB_ZONE_ID}"
        evaluate_target_health = false
    }
}

resource "aws_route53_record" "ICPEEL_DEV01_MICROSERVICES_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "ms-dev01.nonprod.peel.insurcloud.ca"
    type    = "A"
    alias {
        name                   = "${data.terraform_remote_state.dev_remote_state.ICPEEL_DEV01_INTERNAL_NLB_DNS_NAME}"
        zone_id                = "${data.terraform_remote_state.dev_remote_state.ICPEEL_DEV01_INTERNAL_NLB_ZONE_ID}"
        evaluate_target_health = false
    }
}

resource "aws_route53_record" "ICPEEL_DEV01_BROKER_NLB_ORCHESTRATOR_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "api.dev01.peel.insurcloud.ca"
    type    = "A"
    alias {
        name                   = "${aws_lb.ICPEEL_DEV01_BROKER_NLB.dns_name}"
        zone_id                = "${aws_lb.ICPEEL_DEV01_BROKER_NLB.zone_id}"
        evaluate_target_health = false
    }
}

resource "aws_route53_record" "ICPEEL_DEV01_PUBLIC_ORCHESTRATOR_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "api-dev01.nonprod.peel.insurcloud.ca"
    type    = "A"
    alias {
        name                   = "${aws_lb.ICPEEL_DEV01_BROKER_NLB.dns_name}"
        zone_id                = "${aws_lb.ICPEEL_DEV01_BROKER_NLB.zone_id}"
        evaluate_target_health = false
    }
}

######### HAPROXY ##############
resource "aws_route53_record" "ICPEEL_DEV01_INTERNAL_HAPROXY_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "inthap.dev01.peel.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICPEEL_DEV01_INTERNAL_HAPROXY_PRIVATEIP}"]
}

# Not being used in Dev01 yet
# resource "aws_route53_record" "ICPEEL_DEV01_EXTERNAL_HAPROXY_RECORD" {
#     zone_id = "${var.zone_id}"
#     name    = "exthap.dev01.peel.insurcloud.ca"
#     ttl = "300"
#     type    = "A"
#     records = ["${data.terraform_remote_state.dev_remote_state.ICPEEL_DEV01_EXTERNAL_HAPROXY_PRIVATEIP}"]
# }

# #=======================================================================
# # PPS
# #=======================================================================
######### GUIDEWIRE ##############
resource "aws_route53_record" "ICPEEL_PPS_GUIDEWIRE_APPLICATION_1_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "gw1.pps.peel.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICPEEL_PPS_GUIDEWIRE_APPLICATION_1_PRIVATEIP}"]
}

resource "aws_route53_record" "ICPEEL_PPS_GUIDEWIRE_APPLICATION_2_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "gw2.pps.peel.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICPEEL_PPS_GUIDEWIRE_APPLICATION_2_PRIVATEIP}"]
}

resource "aws_route53_record" "ICPEEL_PPS_GUIDEWIRE_DB_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "gwdb.pps.peel.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICPEEL_PPS_GUIDEWIRE_MSSQL_DATABASE_PRIVATEIP}"]
}

resource "aws_route53_record" "ICPEEL_PPS_INTERNAL_NLB_GUIDEWIRE_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "gw.pps.peel.insurcloud.ca"
    type    = "A"
    alias {
        name                   = "${data.terraform_remote_state.dev_remote_state.ICPEEL_PPS_INTERNAL_NLB_DNS_NAME}"
        zone_id                = "${data.terraform_remote_state.dev_remote_state.ICPEEL_PPS_INTERNAL_NLB_ZONE_ID}"
        evaluate_target_health = false
    }
}

resource "aws_route53_record" "ICPEEL_PPS_INTERNAL_NLB_GUIDEWIRE_NODE_1_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "gwnode1.pps.peel.insurcloud.ca"
    type    = "A"
    alias {
        name                   = "${data.terraform_remote_state.dev_remote_state.ICPEEL_PPS_INTERNAL_NLB_DNS_NAME}"
        zone_id                = "${data.terraform_remote_state.dev_remote_state.ICPEEL_PPS_INTERNAL_NLB_ZONE_ID}"
        evaluate_target_health = false
    }
}

resource "aws_route53_record" "ICPEEL_PPS_INTERNAL_NLB_GUIDEWIRE_NODE_2_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "gwnode2.pps.peel.insurcloud.ca"
    type    = "A"
    alias {
        name                   = "${data.terraform_remote_state.dev_remote_state.ICPEEL_PPS_INTERNAL_NLB_DNS_NAME}"
        zone_id                = "${data.terraform_remote_state.dev_remote_state.ICPEEL_PPS_INTERNAL_NLB_ZONE_ID}"
        evaluate_target_health = false
    }
}

resource "aws_route53_record" "ICPEEL_PPS_GUIDEWIRE_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "gw-pps.nonprod.peel.insurcloud.ca"
    type    = "A"
    alias {
        name                   = "${data.terraform_remote_state.dev_remote_state.ICPEEL_PPS_INTERNAL_NLB_DNS_NAME}"
        zone_id                = "${data.terraform_remote_state.dev_remote_state.ICPEEL_PPS_INTERNAL_NLB_ZONE_ID}"
        evaluate_target_health = false
    }
}

resource "aws_route53_record" "ICPEEL_PPS_GUIDEWIRE_NODE_1_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "gwnode1-pps.nonprod.peel.insurcloud.ca"
    type    = "A"
    alias {
        name                   = "${data.terraform_remote_state.dev_remote_state.ICPEEL_PPS_INTERNAL_NLB_DNS_NAME}"
        zone_id                = "${data.terraform_remote_state.dev_remote_state.ICPEEL_PPS_INTERNAL_NLB_ZONE_ID}"
        evaluate_target_health = false
    }
}

resource "aws_route53_record" "ICPEEL_PPS_GUIDEWIRE_NODE_2_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "gwnode2-pps.nonprod.peel.insurcloud.ca"
    type    = "A"
    alias {
        name                   = "${data.terraform_remote_state.dev_remote_state.ICPEEL_PPS_INTERNAL_NLB_DNS_NAME}"
        zone_id                = "${data.terraform_remote_state.dev_remote_state.ICPEEL_PPS_INTERNAL_NLB_ZONE_ID}"
        evaluate_target_health = false
    }
}

resource "aws_route53_record" "ICPEEL_PPS_BROKER_NLB_GWPC_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "is.pps.peel.insurcloud.ca"
    type    = "A"
    alias {
        name                   = "${aws_lb.ICPEEL_PPS_BROKER_NLB.dns_name}"
        zone_id                = "${aws_lb.ICPEEL_PPS_BROKER_NLB.zone_id}"
        evaluate_target_health = false
    }
}

resource "aws_route53_record" "ICPEEL_PPS_PUBLIC_GWPC_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "is-pps.nonprod.peel.insurcloud.ca"
    type    = "A"
    alias {
        name                   = "${aws_lb.ICPEEL_PPS_BROKER_NLB.dns_name}"
        zone_id                = "${aws_lb.ICPEEL_PPS_BROKER_NLB.zone_id}"
        evaluate_target_health = false
    }
}

######### MICROSERVICES ###########
resource "aws_route53_record" "ICPEEL_PPS_MICROSERVICES_MANAGER_1_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "msman1.pps.peel.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICPEEL_PPS_MICROSERVICES_MANAGER_1_PRIVATEIP}"]
}

resource "aws_route53_record" "ICPEEL_PPS_MICROSERVICES_MANAGER_2_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "msman2.pps.peel.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICPEEL_PPS_MICROSERVICES_MANAGER_2_PRIVATEIP}"]
}

resource "aws_route53_record" "ICPEEL_PPS_MICROSERVICES_MANAGER_3_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "msman3.pps.peel.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICPEEL_PPS_MICROSERVICES_MANAGER_3_PRIVATEIP}"]
}

resource "aws_route53_record" "ICPEEL_PPS_MICROSERVICES_APPLICATION_1_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "ms1.pps.peel.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICPEEL_PPS_MICROSERVICES_APPLICATION_1_PRIVATEIP}"]
}

resource "aws_route53_record" "ICPEEL_PPS_MICROSERVICES_APPLICATION_2_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "ms2.pps.peel.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICPEEL_PPS_MICROSERVICES_APPLICATION_2_PRIVATEIP}"]
}

resource "aws_route53_record" "ICPEEL_PPS_INTERNAL_NLB_MICROSERVICES_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "ms.pps.peel.insurcloud.ca"
    type    = "A"
    alias {
        name                   = "${data.terraform_remote_state.dev_remote_state.ICPEEL_PPS_INTERNAL_NLB_DNS_NAME}"
        zone_id                = "${data.terraform_remote_state.dev_remote_state.ICPEEL_PPS_INTERNAL_NLB_ZONE_ID}"
        evaluate_target_health = false
    }
}

resource "aws_route53_record" "ICPEEL_PPS_MICROSERVICES_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "ms-pps.nonprod.peel.insurcloud.ca"
    type    = "A"
    alias {
        name                   = "${data.terraform_remote_state.dev_remote_state.ICPEEL_PPS_INTERNAL_NLB_DNS_NAME}"
        zone_id                = "${data.terraform_remote_state.dev_remote_state.ICPEEL_PPS_INTERNAL_NLB_ZONE_ID}"
        evaluate_target_health = false
    }
}

resource "aws_route53_record" "ICPEEL_PPS_BROKER_NLB_ORCHESTRATOR_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "api.pps.peel.insurcloud.ca"
    type    = "A"
    alias {
        name                   = "${aws_lb.ICPEEL_PPS_BROKER_NLB.dns_name}"
        zone_id                = "${aws_lb.ICPEEL_PPS_BROKER_NLB.zone_id}"
        evaluate_target_health = false
    }
}

resource "aws_route53_record" "ICPEEL_PPS_PUBLIC_ORCHESTRATOR_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "api-pps.nonprod.peel.insurcloud.ca"
    type    = "A"
    alias {
        name                   = "${aws_lb.ICPEEL_PPS_BROKER_NLB.dns_name}"
        zone_id                = "${aws_lb.ICPEEL_PPS_BROKER_NLB.zone_id}"
        evaluate_target_health = false
    }
}

######### HAPROXY ##############
resource "aws_route53_record" "ICPEEL_PPS_INTERNAL_HAPROXY_1_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "inthap1.pps.peel.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICPEEL_PPS_INTERNAL_HAPROXY_1_PRIVATEIP}"]
}

resource "aws_route53_record" "ICPEEL_PPS_INTERNAL_HAPROXY_2_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "inthap2.pps.peel.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICPEEL_PPS_INTERNAL_HAPROXY_2_PRIVATEIP}"]
}

resource "aws_route53_record" "ICPEEL_PPS_EXTERNAL_HAPROXY_1_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "exthap1.pps.peel.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICPEEL_PPS_EXTERNAL_HAPROXY_1_PRIVATEIP}"]
}

resource "aws_route53_record" "ICPEEL_PPS_EXTERNAL_HAPROXY_2_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "exthap2.pps.peel.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICPEEL_PPS_EXTERNAL_HAPROXY_2_PRIVATEIP}"]
}

######### DATA ##############
resource "aws_route53_record" "ICPEEL_PPS_SAP_SSIS_MSSQL_DATABASE_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "sapssisdb.pps.peel.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.prod_remote_state.ICPEEL_PPS_SAP_SSIS_MSSQL_DATABASE_PRIVATEIP}"]
}

resource "aws_route53_record" "ICPEEL_PPS_SAP_SERVER_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "sap.pps.peel.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.prod_remote_state.ICPEEL_PPS_SAP_SERVER_PRIVATEIP}"]
}

resource "aws_route53_record" "ICPEEL_PPS_HUBIO_SERVER_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "hubio.pps.peel.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.prod_remote_state.ICPEEL_PPS_HUBIO_SERVER_PRIVATEIP}"]
}

# #=======================================================================
# # PPS/DEV/QA
# #=======================================================================
######### OPENTEXT ##############
resource "aws_route53_record" "ICPEEL_DEV_EXSTREAM_COM_SERVER_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "exstream.pps.peel.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICPEEL_DEV_EXSTREAM_COM_SERVER_PRIVATEIP}"]
}

resource "aws_route53_record" "ICPEEL_DEV_OT_MSSQL_DATABASE_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "otdb.dev.peel.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICPEEL_DEV_OT_MSSQL_DATABASE_PRIVATEIP}"]
}

######### JOBSCHEDULER ##############
resource "aws_route53_record" "ICPEEL_DEV_JOB_SCHEDULER_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "jobscheduler.dev.peel.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${aws_instance.ICPEEL_DEV_JOB_SCHEDULER.private_ip}"]
}

############# JENKINS CLIENT #################
resource "aws_route53_record" "ICPEEL_DEV_JENKINS_CLIENT_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "jenkins-client.dev.peel.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICPEEL_DEV_JENKINS_CLIENT_PRIVATEIP}"]
}

#=======================================================================
# PROD
#=======================================================================
######### GUIDEWIRE ##############
resource "aws_route53_record" "ICPEEL_PROD_GUIDEWIRE_ONLINE_1_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "gwo1.peel.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.prod_remote_state.ICPEEL_PROD_GUIDEWIRE_ONLINE_1_PRIVATEIP}"]
}

resource "aws_route53_record" "ICPEEL_PROD_GUIDEWIRE_ONLINE_2_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "gwo2.peel.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.prod_remote_state.ICPEEL_PROD_GUIDEWIRE_ONLINE_2_PRIVATEIP}"]
}

resource "aws_route53_record" "ICPEEL_PROD_GUIDEWIRE_BATCH_1_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "gwb1.peel.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.prod_remote_state.ICPEEL_PROD_GUIDEWIRE_BATCH_1_PRIVATEIP}"]
}

resource "aws_route53_record" "ICPEEL_PROD_GUIDEWIRE_BATCH_2_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "gwb2.peel.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.prod_remote_state.ICPEEL_PROD_GUIDEWIRE_BATCH_2_PRIVATEIP}"]
}

resource "aws_route53_record" "ICPEEL_PROD_GUIDEWIRE_DB_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "gwdb.peel.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.prod_remote_state.ICPEEL_PROD_GUIDEWIRE_MSSQL_DATABASE_1_PRIVATEIP}"]
}

resource "aws_route53_record" "ICPEEL_PROD_INTERNAL_NLB_GUIDEWIRE_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "gw.peel.insurcloud.ca"
    type    = "A"
    alias {
        name                   = "${data.terraform_remote_state.prod_remote_state.ICPEEL_PROD_INTERNAL_NLB_DNS_NAME}"
        zone_id                = "${data.terraform_remote_state.prod_remote_state.ICPEEL_PROD_INTERNAL_NLB_ZONE_ID}"
        evaluate_target_health = false
    }
}

resource "aws_route53_record" "ICPEEL_PROD_INTERNAL_NLB_GUIDEWIRE_BATCH_1_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "gwbatch1.peel.insurcloud.ca"
    type    = "A"
    alias {
        name                   = "${data.terraform_remote_state.prod_remote_state.ICPEEL_PROD_INTERNAL_NLB_DNS_NAME}"
        zone_id                = "${data.terraform_remote_state.prod_remote_state.ICPEEL_PROD_INTERNAL_NLB_ZONE_ID}"
        evaluate_target_health = false
    }
}

resource "aws_route53_record" "ICPEEL_PROD_INTERNAL_NLB_GUIDEWIRE_BATCH_2_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "gwbatch2.peel.insurcloud.ca"
    type    = "A"
    alias {
        name                   = "${data.terraform_remote_state.prod_remote_state.ICPEEL_PROD_INTERNAL_NLB_DNS_NAME}"
        zone_id                = "${data.terraform_remote_state.prod_remote_state.ICPEEL_PROD_INTERNAL_NLB_ZONE_ID}"
        evaluate_target_health = false
    }
}

resource "aws_route53_record" "ICPEEL_PROD_INTERNAL_NLB_GUIDEWIRE_ONLINE_1_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "gwonline1.peel.insurcloud.ca"
    type    = "A"
    alias {
        name                   = "${data.terraform_remote_state.prod_remote_state.ICPEEL_PROD_INTERNAL_NLB_DNS_NAME}"
        zone_id                = "${data.terraform_remote_state.prod_remote_state.ICPEEL_PROD_INTERNAL_NLB_ZONE_ID}"
        evaluate_target_health = false
    }
}

resource "aws_route53_record" "ICPEEL_PROD_INTERNAL_NLB_GUIDEWIRE_ONLINE_2_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "gwonline2.peel.insurcloud.ca"
    type    = "A"
    alias {
        name                   = "${data.terraform_remote_state.prod_remote_state.ICPEEL_PROD_INTERNAL_NLB_DNS_NAME}"
        zone_id                = "${data.terraform_remote_state.prod_remote_state.ICPEEL_PROD_INTERNAL_NLB_ZONE_ID}"
        evaluate_target_health = false
    }
}

resource "aws_route53_record" "ICPEEL_PROD_BROKER_NLB_GWPC_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "is.peel.insurcloud.ca"
    type    = "A"
    alias {
        name                   = "${aws_lb.ICPEEL_PROD_BROKER_NLB.dns_name}"
        zone_id                = "${aws_lb.ICPEEL_PROD_BROKER_NLB.zone_id}"
        evaluate_target_health = false
    }
}

######### MICROSERVICES ###########
resource "aws_route53_record" "ICPEEL_PROD_MICROSERVICES_MANAGER_1_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "msman1.peel.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.prod_remote_state.ICPEEL_PROD_MICROSERVICES_MANAGER_1_PRIVATEIP}"]
}

resource "aws_route53_record" "ICPEEL_PROD_MICROSERVICES_MANAGER_2_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "msman2.peel.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.prod_remote_state.ICPEEL_PROD_MICROSERVICES_MANAGER_2_PRIVATEIP}"]
}

resource "aws_route53_record" "ICPEEL_PROD_MICROSERVICES_MANAGER_3_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "msman3.peel.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.prod_remote_state.ICPEEL_PROD_MICROSERVICES_MANAGER_2_PRIVATEIP}"]
}

resource "aws_route53_record" "ICPEEL_PROD_MICROSERVICES_APPLICATION_1_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "ms1.peel.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.prod_remote_state.ICPEEL_PROD_MICROSERVICES_APPLICATION_1_PRIVATEIP}"]
}

resource "aws_route53_record" "ICPEEL_PROD_MICROSERVICES_APPLICATION_2_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "ms2.peel.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.prod_remote_state.ICPEEL_PROD_MICROSERVICES_APPLICATION_2_PRIVATEIP}"]
}

resource "aws_route53_record" "ICPEEL_PROD_INTERNAL_NLB_MICROSERVICES_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "ms.peel.insurcloud.ca"
    type    = "A"
    alias {
        name                   = "${data.terraform_remote_state.prod_remote_state.ICPEEL_PROD_INTERNAL_NLB_DNS_NAME}"
        zone_id                = "${data.terraform_remote_state.prod_remote_state.ICPEEL_PROD_INTERNAL_NLB_ZONE_ID}"
        evaluate_target_health = false
    }
}

resource "aws_route53_record" "ICPEEL_PROD_BROKER_NLB_ORCHESTRATOR_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "api.peel.insurcloud.ca"
    type    = "A"
    alias {
        name                   = "${aws_lb.ICPEEL_PROD_BROKER_NLB.dns_name}"
        zone_id                = "${aws_lb.ICPEEL_PROD_BROKER_NLB.zone_id}"
        evaluate_target_health = false
    }
}

######### HAPROXY ##############
resource "aws_route53_record" "ICPEEL_PROD_INTERNAL_HAPROXY_1_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "inthap1.peel.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.prod_remote_state.ICPEEL_PROD_INTERNAL_HAPROXY_1_PRIVATEIP}"]
}

resource "aws_route53_record" "ICPEEL_PROD_INTERNAL_HAPROXY_2_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "inthap2.peel.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.prod_remote_state.ICPEEL_PROD_INTERNAL_HAPROXY_2_PRIVATEIP}"]
}

resource "aws_route53_record" "ICPEEL_PROD_EXTERNAL_HAPROXY_1_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "exthap1.peel.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.prod_remote_state.ICPEEL_PROD_EXTERNAL_HAPROXY_1_PRIVATEIP}"]
}

resource "aws_route53_record" "ICPEEL_PROD_EXTERNAL_HAPROXY_2_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "exthap2.peel.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.prod_remote_state.ICPEEL_PROD_EXTERNAL_HAPROXY_2_PRIVATEIP}"]
}

######### DATA ##############
resource "aws_route53_record" "ICPEEL_PROD_SAP_SERVER_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "sap.peel.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.prod_remote_state.ICPEEL_PROD_SAP_SERVER_PRIVATEIP}"]
}

resource "aws_route53_record" "ICPEEL_PROD_SAP_SSIS_MSSQL_DATABASE_1_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "sapssisdb.peel.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.prod_remote_state.ICPEEL_PROD_SAP_SSIS_MSSQL_DATABASE_1_PRIVATEIP}"]
}

resource "aws_route53_record" "ICPEEL_PROD_HUBIO_SERVER_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "hubio.peel.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.prod_remote_state.ICPEEL_PROD_HUBIO_SERVER_PRIVATEIP}"]
}

resource "aws_route53_record" "ICPEEL_PROD_QLIK_SENSE_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "qliksense.peel.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.prod_remote_state.ICPEEL_PROD_QLIK_SENSE_PRIVATEIP}"]
}

######### OPENTEXT ##############
resource "aws_route53_record" "ICPEEL_PROD_EXSTREAM_COM_SERVER_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "exstream.peel.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.prod_remote_state.ICPEEL_PROD_EXSTREAM_COM_SERVER_PRIVATEIP}"]
}

resource "aws_route53_record" "ICPEEL_PROD_OT_MSSQL_DATABASE_1_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "otdb.peel.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.prod_remote_state.ICPEEL_PROD_OT_MSSQL_DATABASE_1_PRIVATEIP}"]
}

######### JOBSCHEDULER ##############
resource "aws_route53_record" "ICPEEL_PROD_JOB_SCHEDULER_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "jobscheduler.peel.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.prod_remote_state.ICPEEL_PROD_JOB_SCHEDULER_PRIVATEIP}"]
}

############# JENKINS CLIENT #################
resource "aws_route53_record" "ICPEEL_PROD_JENKINS_CLIENT_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "jenkins-client.peel.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.prod_remote_state.ICPEEL_PROD_JENKINS_CLIENT_PRIVATEIP}"]
}
