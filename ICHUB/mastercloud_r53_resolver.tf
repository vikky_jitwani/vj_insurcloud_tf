resource "aws_route53_resolver_endpoint" "IC_HUB_DNS" {
  name      = "ic-hub-dns"
  direction = "INBOUND"

  security_group_ids = [
    "${aws_security_group.SG_IC_HUB_DNS_RESOLVER_ENDPOINT.id}"
  ]

  ip_address {
    subnet_id = "${var.ichub-private-1a-subnet}"
  }

  ip_address {
    subnet_id = "${var.ichub-private-1b-subnet}"
  }
}
