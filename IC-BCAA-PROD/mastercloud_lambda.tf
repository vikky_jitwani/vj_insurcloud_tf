## Resources##
resource "aws_lambda_layer_version" "ODBCPy_layer" {
  s3_bucket = "bcaa-lambda-dependencies"
  s3_key = "ODBCPythonDependencies_1.zip"
  layer_name = "ODBCPythonDependencies_1"
  compatible_runtimes = ["python3.7"]
}

## PPS Lambdas ##
resource "aws_lambda_permission" "apigw_lambda_ppsglfeed" {
  statement_id  = "AllowExecutionFromAPIGateway"
  action        = "lambda:InvokeFunction"
  function_name = "${aws_lambda_function.bcaa-PPSGLFeed.function_name}"
  principal     = "apigateway.amazonaws.com"

  # More: http://docs.aws.amazon.com/apigateway/latest/developerguide/api-gateway-control-access-using-iam-policies-to-invoke-api.html
  # They are in the hub 
  source_arn = "arn:aws:execute-api:${var.region}:${var.bcaa-hub-account}:nbfett4mhd/*/*/*"
}

resource "aws_lambda_function" "bcaa-PPSGLFeed" {

  function_name = "bcaa-PPSGLFeed"
  role          = "arn:aws:iam::413329031558:role/LambdaAssume"
  handler       = "bcaaGLIntegration_get.readFromTable"
  runtime       = "python3.7"
  timeout       = 60
  layers        = ["${aws_lambda_layer_version.ODBCPy_layer.arn}"]
  s3_bucket     = "bcaa-lambda-dependencies"
  s3_key        = "bcaaGLIntegration_get.zip"

  vpc_config    ={
    subnet_ids = ["${var.bcaa-prod-private-1a-subnet}", "${var.bcaa-prod-private-1b-subnet}"]
    security_group_ids = ["${aws_security_group.SG_LAMBDA.id}"]
  }

  environment {
    variables = {
      REGION_NAME = "ca-central-1"
      SECRET_NAME = "bcaa-PPS-ICUser"
    }
  }
}

#Lambda function for PPS bcaa GL Update
resource "aws_lambda_permission" "apigw_lambda_ppsglupdate" {
  statement_id  = "AllowExecutionFromAPIGateway"
  action        = "lambda:InvokeFunction"
  function_name = "${aws_lambda_function.bcaa-PPSGLUpdate.function_name}"
  principal     = "apigateway.amazonaws.com"

  # More: http://docs.aws.amazon.com/apigateway/latest/developerguide/api-gateway-control-access-using-iam-policies-to-invoke-api.html
  # They are in the hub 
  #source_arn = "arn:aws:execute-api:${var.region}:${var.accountId}:${aws_api_gateway_rest_api.GLCheque.id}/*/${aws_api_gateway_method.bcaaGLUpdate.http_method}${aws_api_gateway_resource.bcaaGLUpdate.path}"
  source_arn = "arn:aws:execute-api:${var.region}:${var.bcaa-hub-account}:nbfett4mhd/*/*/*"
}

resource "aws_lambda_function" "bcaa-PPSGLUpdate" {
  function_name = "bcaa-PPSGLUpdate"
  role          = "arn:aws:iam::413329031558:role/LambdaAssume"
  handler       = "bcaaGLIntegration_post.putInTable"
  runtime       = "python3.7"
  timeout       = 60
  layers        = ["${aws_lambda_layer_version.ODBCPy_layer.arn}"]
  s3_bucket     = "bcaa-lambda-dependencies"
  s3_key        = "bcaaGLIntegration_post.zip"

  vpc_config    ={
    subnet_ids = ["${var.bcaa-prod-private-1a-subnet}", "${var.bcaa-prod-private-1b-subnet}"]
    security_group_ids = ["${aws_security_group.SG_LAMBDA.id}"]
  }

  environment {
    variables = {
      REGION_NAME = "ca-central-1"
      SECRET_NAME = "bcaa-PPS-ICUser"
    }
  }
}

##PROD LAMBDAs##
resource "aws_lambda_permission" "apigw_lambda_prodglfeed" {
  statement_id  = "AllowExecutionFromAPIGateway"
  action        = "lambda:InvokeFunction"
  function_name = "${aws_lambda_function.bcaa-ProdGLFeed.function_name}"
  principal     = "apigateway.amazonaws.com"

  # More: http://docs.aws.amazon.com/apigateway/latest/developerguide/api-gateway-control-access-using-iam-policies-to-invoke-api.html
  # They are in the hub 
  # source_arn = "arn:aws:execute-api:${var.region}:${var.accountId}:${aws_api_gateway_rest_api.GLCheque.id}/*/${aws_api_gateway_method.bcaaGLFeed.http_method}${aws_api_gateway_resource.bcaaGLFeed.path}"
  source_arn = "arn:aws:execute-api:${var.region}:${var.bcaa-hub-account}:nbfett4mhd/*/*/*"
}

resource "aws_lambda_function" "bcaa-ProdGLFeed" {
  function_name = "bcaa-ProdGLFeed"
  role          = "arn:aws:iam::413329031558:role/LambdaAssume"
  handler       = "bcaaGLIntegration_get.readFromTable"
  runtime       = "python3.7"
  timeout       = 60
  layers        = ["${aws_lambda_layer_version.ODBCPy_layer.arn}"]
  s3_bucket     = "bcaa-lambda-dependencies"
  s3_key        = "bcaaGLIntegration_get.zip"

  vpc_config    ={
    subnet_ids = ["${var.bcaa-prod-private-1a-subnet}", "${var.bcaa-prod-private-1b-subnet}"]
    security_group_ids = ["${aws_security_group.SG_LAMBDA.id}"]
  }

  environment {
    variables = {
      REGION_NAME = "ca-central-1"
      SECRET_NAME = "bcaa_PROD_InfoCenter_DBOwner"
    }
  }
}

#Lambda function for PPS bcaa GL Update
resource "aws_lambda_permission" "apigw_lambda_prodglupdate" {
  statement_id  = "AllowExecutionFromAPIGateway"
  action        = "lambda:InvokeFunction"
  function_name = "${aws_lambda_function.bcaa-ProdGLUpdate.function_name}"
  principal     = "apigateway.amazonaws.com"

  # More: http://docs.aws.amazon.com/apigateway/latest/developerguide/api-gateway-control-access-using-iam-policies-to-invoke-api.html
  # They are in the hub 
  #source_arn = "arn:aws:execute-api:${var.region}:${var.accountId}:${aws_api_gateway_rest_api.GLCheque.id}/*/${aws_api_gateway_method.bcaaGLUpdate.http_method}${aws_api_gateway_resource.bcaaGLUpdate.path}"
   source_arn = "arn:aws:execute-api:${var.region}:${var.bcaa-hub-account}:nbfett4mhd/*/*/*"
}

resource "aws_lambda_function" "bcaa-ProdGLUpdate" {
  function_name = "bcaa-ProdGLUpdate"
  role          = "arn:aws:iam::413329031558:role/LambdaAssume"
  handler       = "bcaaGLIntegration_post.putInTable"
  runtime       = "python3.7"
  timeout       = 60
  layers        = ["${aws_lambda_layer_version.ODBCPy_layer.arn}"]
  s3_bucket     = "bcaa-lambda-dependencies"
  s3_key        = "bcaaGLIntegration_post.zip"

  vpc_config    ={
    subnet_ids = ["${var.bcaa-prod-private-1a-subnet}", "${var.bcaa-prod-private-1b-subnet}"]
    security_group_ids = ["${aws_security_group.SG_LAMBDA.id}"]
  }

  environment {
    variables = {
      REGION_NAME = "ca-central-1"
      SECRET_NAME = "bcaa_PROD_InfoCenter_DBOwner"
    }
  }
}