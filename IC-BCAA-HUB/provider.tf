
provider "aws" {
  
  #allowed_account_ids = ["057006516829"]
  region     = "ca-central-1"
  assume_role = {
    role_arn = "arn:aws:iam::057006516829:role/admin_ops_auto"
  }
}

terraform {
 backend "s3" {
    bucket     = "a-c-mc-p-a010-bcaa-tfstate"
    key        = "IC-BCAA-HUB/STATE/current.tf"
    region     = "ca-central-1"
    # role_arn       = "arn:aws:iam::393766723611:role/admin_ops_auto"
      #encrypt    = true
      #kms_key_id = "arn:aws:kms:ca-central-1::key/"
  }
}

data "terraform_remote_state" "dev_remote_state" {
  backend = "s3"
  config = {
    bucket     = "a-c-mc-p-a010-bcaa-tfstate"
    key        = "IC-BCAA-DEV/STATE/current.tf"
    region     = "ca-central-1"
    # role_arn   = "arn:aws:iam::393766723611:role/admin_ops_auto"
  }
}

data "terraform_remote_state" "prod_remote_state" {
  backend = "s3"
  config = {
    bucket     = "a-c-mc-p-a010-bcaa-tfstate"
    key        = "IC-BCAA-PROD/STATE/current.tf"
    region     = "ca-central-1"
    # role_arn   = "arn:aws:iam::393766723611:role/admin_ops_auto"
  }
}