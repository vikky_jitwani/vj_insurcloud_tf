resource "aws_s3_bucket" "ICECONOMICAL_DEV_MS_RSTUDIO_DROPZONE" {
  bucket = "eco-ms-rstudio-dropzone"
  acl = "private"
  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm = "AES256"
      }
    }
  }

  lifecycle_rule {
    enabled = true

    transition {
      days = 30
      storage_class = "STANDARD_IA"
    }
  }

  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "MANAGED_SERVICES"
    Name = "eco-ms-rstudio-dropzone"
  }
}

resource "aws_s3_bucket_policy" "ICECONOMICAL_DEV_MS_RSTUDIO_DROPZONE_BUCKET_POLICY" {
  bucket = "eco-ms-rstudio-dropzone"
  depends_on = [
    "aws_s3_bucket.ICECONOMICAL_DEV_MS_RSTUDIO_DROPZONE"
  ]

  policy = <<POLICY
{
    "Version": "2012-10-17",
    "Id": "ICECONOMICAL_DEV_MS_RSTUDIO_DROPZONE_BUCKET_POLICY",
    "Statement": [
        {
            "Sid": "DenyPublicReadACL",
            "Effect": "Deny",
            "Principal": {
                "AWS": "*"
            },
            "Action": [
                "s3:PutObject",
                "s3:PutObjectAcl"
            ],
            "Resource": "arn:aws:s3:::${aws_s3_bucket.ICECONOMICAL_DEV_MS_RSTUDIO_DROPZONE.id}/*",
            "Condition": {
                "StringEquals": {
                    "s3:x-amz-acl": [
                        "public-read",
                        "public-read-write",
                        "authenticated-read"
                    ]
                }
            }
        },
        {
            "Sid": "DenyPublicReadGrant",
            "Effect": "Deny",
            "Principal": {
                "AWS": "*"
            },
            "Action": [
                "s3:PutObject",
                "s3:PutObjectAcl"
            ],
            "Resource": "arn:aws:s3:::${aws_s3_bucket.ICECONOMICAL_DEV_MS_RSTUDIO_DROPZONE.id}/*",
            "Condition": {
                "StringLike": {
                    "s3:x-amz-grant-read": [
                        "*http://acs.amazonaws.com/groups/global/AllUsers*",
                        "*http://acs.amazonaws.com/groups/global/AuthenticatedUsers*"
                    ]
                }
            }
        },
        {
            "Sid": "DenyPublicListACL",
            "Effect": "Deny",
            "Principal": {
                "AWS": "*"
            },
            "Action": "s3:PutBucketAcl",
            "Resource": "arn:aws:s3:::${aws_s3_bucket.ICECONOMICAL_DEV_MS_RSTUDIO_DROPZONE.id}",
            "Condition": {
                "StringEquals": {
                    "s3:x-amz-acl": [
                        "public-read",
                        "public-read-write",
                        "authenticated-read"
                    ]
                }
            }
        },
        {
            "Sid": "DenyPublicListGrant",
            "Effect": "Deny",
            "Principal": {
                "AWS": "*"
            },
            "Action": "s3:PutBucketAcl",
            "Resource": "arn:aws:s3:::${aws_s3_bucket.ICECONOMICAL_DEV_MS_RSTUDIO_DROPZONE.id}",
            "Condition": {
                "StringLike": {
                    "s3:x-amz-grant-read": [
                        "*http://acs.amazonaws.com/groups/global/AllUsers*",
                        "*http://acs.amazonaws.com/groups/global/AuthenticatedUsers*"
                    ]
                }
            }
        },
        {
            "Sid": "DenyPublicReadConsole",
            "Effect": "Deny",
            "Principal": {
                "AWS": "*"
            },
            "Action": [
                "s3:PutObjectAcl",
                "s3:PutBucketAcl"
            ],
            "Resource": [
                "arn:aws:s3:::${aws_s3_bucket.ICECONOMICAL_DEV_MS_RSTUDIO_DROPZONE.id}/*",
                "arn:aws:s3:::${aws_s3_bucket.ICECONOMICAL_DEV_MS_RSTUDIO_DROPZONE.id}"
            ],
            "Condition": {
                "Null": {
                    "s3:x-amz-grant-write": "true",
                    "s3:x-amz-grant-read": "true",
                    "s3:x-amz-acl": "true",
                    "s3:x-amz-grant-read-acp": "true",
                    "s3:x-amz-grant-full-control": "true",
                    "s3:x-amz-grant-write-acp": "true"
                }
            }
        },
        {
            "Sid": "DevInstanceListBucketAllow",
            "Effect": "Allow",
            "Principal": {
                "AWS": [
                    "arn:aws:iam::${var.economical-dev-account}:role/CMS-SSMRole",
                    "arn:aws:iam::${var.economical-dev-account}:user/eco-application-user"
                ]
            },
            "Action": "s3:ListBucket",
            "Resource": "arn:aws:s3:::${aws_s3_bucket.ICECONOMICAL_DEV_MS_RSTUDIO_DROPZONE.id}"
        },
        {
            "Sid": "DevInstanceObjectAllow",
            "Effect": "Allow",
            "Principal": {
                "AWS": [
                    "arn:aws:iam::${var.economical-dev-account}:role/CMS-SSMRole",
                    "arn:aws:iam::${var.economical-dev-account}:user/eco-application-user"
                ]
            },
            "Action": [
                "s3:GetObject",
                "s3:PutObject",
                "s3:DeleteObject"
            ],
            "Resource": [
                "arn:aws:s3:::${aws_s3_bucket.ICECONOMICAL_DEV_MS_RSTUDIO_DROPZONE.id}",
                "arn:aws:s3:::${aws_s3_bucket.ICECONOMICAL_DEV_MS_RSTUDIO_DROPZONE.id}/*"
            ]
        }
    ]
}
POLICY
}

resource "aws_s3_bucket" "ICECONOMICAL_DEV_MS_DOCUMENTS" {
  bucket = "eco-ms-documents"
  acl = "private"
  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm = "AES256"
      }
    }
  }

  lifecycle_rule {
    enabled = true

    transition {
      days = 30
      storage_class = "STANDARD_IA"
    }
  }
  
  lifecycle_rule {
    enabled = true
    abort_incomplete_multipart_upload_days = 7
    expiration {
      days = 60
    }
    noncurrent_version_expiration {
      days = 7
    }
  }    

  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "MANAGED_SERVICES"
    Name = "eco-ms-documents"
  }
}

resource "aws_s3_bucket_policy" "ICECONOMICAL_DEV_MS_DOCUMENTS_BUCKET_POLICY" {
  bucket = "eco-ms-documents"
  depends_on = [
    "aws_s3_bucket.ICECONOMICAL_DEV_MS_DOCUMENTS"
  ]

  policy = <<POLICY
{
    "Version": "2012-10-17",
    "Id": "ICECONOMICAL_DEV_MS_DOCUMENTS_BUCKET_POLICY",
    "Statement": [
        {
            "Sid": "DenyPublicReadACL",
            "Effect": "Deny",
            "Principal": {
                "AWS": "*"
            },
            "Action": [
                "s3:PutObject",
                "s3:PutObjectAcl"
            ],
            "Resource": "arn:aws:s3:::eco-ms-documents/*",
            "Condition": {
                "StringEquals": {
                    "s3:x-amz-acl": [
                        "public-read",
                        "public-read-write",
                        "authenticated-read"
                    ]
                }
            }
        },
        {
            "Sid": "DenyPublicReadGrant",
            "Effect": "Deny",
            "Principal": {
                "AWS": "*"
            },
            "Action": [
                "s3:PutObject",
                "s3:PutObjectAcl"
            ],
            "Resource": "arn:aws:s3:::eco-ms-documents/*",
            "Condition": {
                "StringLike": {
                    "s3:x-amz-grant-read": [
                        "*http://acs.amazonaws.com/groups/global/AllUsers*",
                        "*http://acs.amazonaws.com/groups/global/AuthenticatedUsers*"
                    ]
                }
            }
        },
        {
            "Sid": "DenyPublicListACL",
            "Effect": "Deny",
            "Principal": {
                "AWS": "*"
            },
            "Action": "s3:PutBucketAcl",
            "Resource": "arn:aws:s3:::eco-ms-documents",
            "Condition": {
                "StringEquals": {
                    "s3:x-amz-acl": [
                        "public-read",
                        "public-read-write",
                        "authenticated-read"
                    ]
                }
            }
        },
        {
            "Sid": "DenyPublicListGrant",
            "Effect": "Deny",
            "Principal": {
                "AWS": "*"
            },
            "Action": "s3:PutBucketAcl",
            "Resource": "arn:aws:s3:::eco-ms-documents",
            "Condition": {
                "StringLike": {
                    "s3:x-amz-grant-read": [
                        "*http://acs.amazonaws.com/groups/global/AllUsers*",
                        "*http://acs.amazonaws.com/groups/global/AuthenticatedUsers*"
                    ]
                }
            }
        },
        {
            "Sid": "DenyPublicReadConsole",
            "Effect": "Deny",
            "Principal": {
                "AWS": "*"
            },
            "Action": [
                "s3:PutObjectAcl",
                "s3:PutBucketAcl"
            ],
            "Resource": [
                "arn:aws:s3:::eco-ms-documents/*",
                "arn:aws:s3:::eco-ms-documents"
            ],
            "Condition": {
                "Null": {
                    "s3:x-amz-grant-write-acp": "true",
                    "s3:x-amz-acl": "true",
                    "s3:x-amz-grant-full-control": "true",
                    "s3:x-amz-grant-write": "true",
                    "s3:x-amz-grant-read-acp": "true",
                    "s3:x-amz-grant-read": "true"
                }
            }
        },
        {
            "Sid": "DevInstanceListBucketAllow",
            "Effect": "Allow",
            "Principal": {
                "AWS": [
                    "arn:aws:iam::${var.economical-dev-account}:role/CMS-SSMRole",
                    "arn:aws:iam::${var.economical-dev-account}:user/eco-application-user"
                ]
            },
            "Action": "s3:ListBucket",
            "Resource": "arn:aws:s3:::eco-ms-documents"
        },
        {
            "Sid": "DevInstanceObjectAllow",
            "Effect": "Allow",
            "Principal": {
                "AWS": [
                    "arn:aws:iam::${var.economical-dev-account}:role/CMS-SSMRole",
                    "arn:aws:iam::${var.economical-dev-account}:user/eco-application-user"
                ]
            },
            "Action": [
                "s3:GetObject",
                "s3:DeleteObject"
            ],
            "Resource": [
                "arn:aws:s3:::eco-ms-documents",
                "arn:aws:s3:::eco-ms-documents/*"
            ]
        },
        {
            "Sid": "ForceSSLDeny",
            "Effect": "Deny",
            "Principal": {
                "AWS": "*"
            },
            "Action": "s3:PutObject",
            "Resource": [
                "arn:aws:s3:::eco-ms-documents",
                "arn:aws:s3:::eco-ms-documents/*"
            ],
            "Condition": {
                "Bool": {
                    "aws:SecureTransport": "false"
                }
            }
        },
        {
            "Sid": "UberSourceIp",
            "Effect": "Deny",
            "Principal": {
                "AWS": "*"
            },
            "Action": "s3:PutObject",
            "Resource": [
                "arn:aws:s3:::eco-ms-documents",
                "arn:aws:s3:::eco-ms-documents/*"
            ],
            "Condition": {
                "NotIpAddress": {
                    "aws:SourceIp": [
                        "10.0.0.0/8",
                        "172.0.0.0/8",
                        "3.97.24.50/32",
                        "3.97.46.244/32",
                        "15.223.12.234/32",
                        "15.222.109.117/32",
                        "104.36.192.0/21",
                        "34.236.102.148/32",
                        "76.102.44.6/32",
                        "184.148.114.129/32",
                        "184.148.114.104/32"
                    ]
                }
            }
        }
    ]
}
POLICY
}
