variable "client_name" {
  description = "AWS client name for resources (Ex: MASTERCLOUD)"
  default = ""
}

variable "environment_name" {
  description = "AWS environment name for resources (Ex: NON-PROD)"
  default = ""
}

variable "environment_type" {
  description = "AWS environment type for resources (Ex: P=PROD/HUB, D=NON-PROD)"
  default = ""
}

variable "client_number" {
  description = "AWS client number for tagging (Ex: A001)"
  default = "A004"
}

variable "region" {
  description = "AWS region for hosting our your network"
  default = "ca-central-1"
}

variable "volume_key" {
  description = "KMS volume key dev account"
  default = ""
}
variable "ami_id_windows2012sql14enterprise" {
  description = "Windows 2012 SQL 2014 Enterprise Encypted Root Volume AMI"
  default = ""
}

variable "ami_id_windows2019container" {
  description = "Windows 2019 Container Encypted Root Volume AMI"
  default = ""
}

variable "ami_id_windows2019base" {
  description = "Windows 2019 Base Encypted Root Volume AMI"
  default = ""
}

variable "ami_id_windows2016base" {
  description = "Windows 2016 Base Encypted Root Volume AMI"
  default = ""
}

variable "ami_id_windows2012R2base" {
  description = "Windows 2012 R2 Base Encypted Root Volume AMI"
  default = ""
}

variable "ami_id_windows2008base" {
  description = "Windows 2008 Base Encypted Root Volume AMI"
  default = ""
}

variable "ami_id_windows2016base20200826" {
  description = "Windows 2016 Base Encypted Root Volume AMI - With CMS 2020 Aug Patch"
  default = ""
}

variable "ami_id_redhatlinux75" {
  description = "Redhat Linux 7.5 Encypted Root Volume AMI"
  default = ""
}


variable "ami_id_ubuntu1804baseimage" {
  description = "Ubuntu 18.04 Base Encypted Root Volume AMI"
  default = ""
}


variable "ami_id_ubuntu1604baseimage" {
  description = "Ubuntu 16.04 Base Encypted Root Volume AMI"
  default = ""
}

variable "ami_id_amazonlinux201712base" {
  description = "Amazon Linux 2017 12 Base Encypted Root Volume AMI"
  default = ""
}

variable "ami_id_amazonlinux201709base" {
  description = "Amazon Linux 2017 09 Base Encypted Root Volume AMI"
  default = ""
}

variable "ami_id_amazonlinux2base" {
  description = "Amazon Linux 2 Base Encypted Root Volume AMI"
  default = ""
}

variable "ami_id_amazonlinux201803base" {
  description = "Amazon Linux 2018 03 Base Encypted Root Volume AMI"
  default = ""
}

variable "ami_id_amazonlinuxbase20200506" {
  description = "CMS CIS Hardened Amazon Linux with Docker 19 and Encrypted Root Volume AMI"
  default = ""
}

variable "vpc_id" {
  description = "VPC id for DEV account"
  default = ""
}

variable "vpc_cidr" {
  description = "VPC cidr dev account"
  default = ""
}

variable "economical-dev-database-1a-subnet" {
  description = "Database subnet one"
  default = ""
}

variable "economical-dev-database-1b-subnet" {
  description = "Database subnet two"
  default = ""
}

variable "economical-dev-private-1a-subnet" {
  description = "Private Subnet One"
  default = ""
}

variable "economical-dev-private-1b-subnet" {
  description = "Private Subnet Two"
  default = ""
}

variable "default_cidr" {
  description = "default cidr used"
  default = [
    "10.0.0.0/8"]
}

variable "economical_cidr" {
  default = "10.227.0.0/23"
}

variable "mssql-rds-password" {
  default = "Toronto12345"
}
variable "mysql-rds-password" {
  default = "Toronto12345"
}

variable "postgres-rds-password" {
  default = "Toronto12345"
}

variable "zone_id" {
  description = "default zone id"
  default = ""
}
variable "economical_dev_cidr" {
  description = "default cidr used"
  type = "list"
  default = [
    "10.227.8.0/22"]
}

variable "economical_prod_cidr" {
  description = "default cidr used"
  type = "list"
  default = [
    "10.227.4.0/22"]
}

variable "economical_hub_cidr" {
  description = "default cidr used"
  type = "list"
  default = [
    "10.227.0.0/22"]
}
variable "economical-hub_private_cidr" {
  description = "default cidr used"
  type = "list"
  default = [
    "10.227.0.0/24",
    "10.227.1.0/24"]
}
variable "economical_aviatrix_vpn" {
  default = "10.227.2.10/32"
}

variable "economical_corp_cidrs" {
  description = "default economical corporate cidrs"
  type = "list"
  default = [
    "10.0.0.0/8",
    "172.0.0.0/8"
  ]
}

variable "ad_dns1" {
  default = "10.227.0.139"
}
variable "ad_dns2" {
  default = "10.227.1.6"
}
variable "ad_dns3" {
  default = "10.227.0.2"
}
variable "ou_name" {
  default = "dev"
}
variable "pwordparameter" {
  default = "domain_password"
}
variable "unameparameter" {
  default = "domain_username"
}
variable "ad_domain" {
  default = "internal.economical.insurcloud.ca"
}
data "aws_caller_identity" "current" {}

variable "economical-dev-account" {
  default = "632109065501"
}

variable "economical-prod-account" {
  default = "852868701745"
}

variable "economical-hub-account" {
  default = "348868695504"
}

variable "cms-default-ignore-tags" {
  default = [
    "tags.%",
    "tags.BILLINGCODE",
    "tags.BILLINGCONTACT",
    "tags.CLIENT",
    "tags.COUNTRY",
    "tags.CSCLASS",
    "tags.CSQUAL",
    "tags.CSTYPE",
    "tags.ENVIRONMENT",
    "tags.FUNCTION",
    "tags.GROUPCONTACT",
    "tags.MEMBERFIRM",
    "tags.PRIMARYCONTACT",
    "tags.SECONDARYCONTACT",
    "volume_tags.%",
    "volume_tags.AccountID",
    "volume_tags.ApplicationName",
    "volume_tags.BILLINGCODE",
    "volume_tags.BILLINGCONTACT",
    "volume_tags.CLIENT",
    "volume_tags.COUNTRY",
    "volume_tags.CSCLASS",
    "volume_tags.CSQUAL",
    "volume_tags.CSTYPE",
    "volume_tags.DataClassification",
    "volume_tags.Docker",
    "volume_tags.ENVIRONMENT",
    "volume_tags.Environment",
    "volume_tags.FUNCTION",
    "volume_tags.GROUPCONTACT",
    "volume_tags.MEMBERFIRM",
    "volume_tags.ManagedBy",
    "volume_tags.Name",
    "volume_tags.Owner",
    "volume_tags.PRIMARYCONTACT",
    "volume_tags.PatchGroup",
    "volume_tags.ResourceSeverity",
    "volume_tags.ResourceType",
    "volume_tags.Role",
    "volume_tags.SECONDARYCONTACT",
    "volume_tags.SolutionName",
    "volume_tags.VendorManaged"
  ]
}
