variable "client_name" {
  description = "AWS client name for resources (Ex: MASTERCLOUD)"
  default = ""
}

variable "enviornment_name" {
  description = "AWS enviornment name for resources (Ex: NON-PROD)"
  default = ""
}

variable "enviornment_type" {
  description = "AWS enviornment type for resources (Ex: P=PROD/HUB, D=NON-PROD)"
  default = ""
}

variable "client_number" {
  description = "AWS client number for tagging (Ex: A001)"
  default = "A005"
}

variable "region" {
  description = "AWS region for hosting our your network"
  default = "ca-central-1"
}

variable "volume_key" {
  description = "KMS volume key prod account"
  default = ""
}

variable "timezone" {
  description = "(Optional) Time zone of the DB instance. timezone is currently only supported by Microsoft SQL Server. The timezone can only be set on creation. See MSSQL User Guide for more information."
  default     = "Eastern"
}
variable "ami_id_windows2012sql14enterprise" {
  description = "Windows 2012 SQL 2014 Enterprise Encypted Root Volume AMI"
  default = ""
}

variable "ami_id_windows2019container" {
  description = "Windows 2019 Container Encypted Root Volume AMI"
  default = ""
}

variable "ami_id_windows2019base" {
  description = "Windows 2019 Base Encypted Root Volume AMI"
  default = ""
}

variable "ami_id_windows2016base" {
  description = "Windows 2016 Base Encypted Root Volume AMI"
  default = ""
}

variable "ami_id_windows2012R2base" {
  description = "Windows 2012 R2 Base Encypted Root Volume AMI"
  default = ""
}

variable "ami_id_windows2008base" {
  description = "Windows 2008 Base Encypted Root Volume AMI"
  default = ""
}

variable "ami_id_redhatlinux75" {
  description = "Redhat Linux 7.5 Encypted Root Volume AMI"
  default = ""
}

variable "ami_id_windows2016base20200806" {
  description = "Updated Windows 2016 Base Encypted Root Volume AMI 6 August 2020"
  default = ""
}

variable "ami_id_amazonlinuxbase20200806" {
  description = "Updated Amazon Linux 2 Base Encypted Root Volume AMI 6 August 2020"
  default = ""
}

variable "ami_id_ubuntu1804baseimage" {
  description = "Ubuntu 18.04 Base Encypted Root Volume AMI"
  default = ""
}


variable "ami_id_ubuntu1604baseimage" {
  description = "Ubuntu 16.04 Base Encypted Root Volume AMI"
  default = ""
}

variable "ami_id_amazonlinux201712base" {
  description = "Amazon Linux 2017 12 Base Encypted Root Volume AMI"
  default = ""
}

variable "ami_id_amazonlinux201709base" {
  description = "Amazon Linux 2017 09 Base Encypted Root Volume AMI"
  default = ""
}

variable "ami_id_amazonlinux2base" {
  description = "Amazon Linux 2 Base Encypted Root Volume AMI"
  default = ""
}

variable "ami_id_amazonlinux201803base" {
  description = "Amazon Linux 2018 03 Base Encypted Root Volume AMI"
  default = ""
}

variable "ami_id_amazonlinuxbase20200506" {
  description = "CMS CIS Hardened Amazon Linux with Docker 19 and Encrypted Root Volume AMI"
  default = ""
}

variable "vpc_id" {
  description = "VPC id for PROD account"
  default = "vpc-045517b075443581e"
}

variable "vpc_cidr" {
  description = "VPC cidr prod account"
  default = ""
}

variable "portage-prod-database-1a-subnet" {
  description = "Database subnet one"
  default = ""
}

variable "portage-prod-database-1b-subnet" {
  description = "Database subnet two"
  default = ""
}

variable "portage-prod-private-1a-subnet" {
  description = "Private Subnet one"
  default = ""
}

variable "portage-prod-private-1b-subnet" {
  description = "Private Subnet Two"
  default = ""

}
variable "default_cidr" {
  description = "default cidr used"
  default = ["10.0.0.0/8"]
}

variable "portage_cidr" {
    default = "10.171.0.0/23"
}

variable "mssql-rds-password" {
    default = "TSYfnasTg4V27cHZ"
}

variable "zone_id" {
  description = "default zone id"
  default = ""
}
variable "portage_dev_cidr" {
 description = "default cidr used"
 type = "list"
 default = ["10.171.8.0/22"]
}
 
variable "portage_prod_cidr" {
 description = "default cidr used"
 type = "list"
 default = ["10.171.4.0/22"]
}
 
variable "portage_hub_cidr" {
 description = "default cidr used"
 type = "list"
 default = ["10.171.0.0/22"]
}
variable "portage-hub_private_cidr" {
  description = "default cidr used"
  type = "list"
  default = ["10.171.0.0/24", "10.171.1.0/24"]
}
variable "portage_aviatrix_vpn" {
    default = "10.171.2.10/32"
}
variable "portage_corp_cidrs" {
  description = "default portage corporate cidrs"
  type = "list"
  default = [
    "10.0.0.0/8",
    "172.0.0.0/8"
  ]
}

variable "ad_dns1" {
    default = "10.171.0.80"
}
variable "ad_dns2" {
    default = "10.171.1.41"
}
variable "ad_dns3" {
    default = "10.171.0.2"
}
variable "ou_name" {
    default = "dev"
}
variable "pwordparameter" {
    default = "domain_password"
}
variable "unameparameter" {
    default = "domain_username"
}
variable "ad_domain" {
    default = "internal.portage.insurcloud.ca"
}
data "aws_caller_identity" "current" {}

variable "portage-dev-account" {
  default = "091072499599"
}

variable "portage-prod-account" {
  default = "625725589452"
}

variable "portage-hub-account" {
  default = "342339668068"
}
