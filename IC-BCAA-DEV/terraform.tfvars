#Envrionment specific variables
client_name = "IC-Bcaa"
environment_name = "DEV"
environment_type = "NP"
client_number = "A010"
#ad-dc1 = "10.10.4.10"
#ad-dc2 = "10.10.6.10"
volume_key = "arn:aws:kms:ca-central-1:775629271970:key/48f133c3-6815-4a24-b1eb-b18ddbfb7319"
ami_id_windows2012sql14enterprise_2020_12_11 	= "ami-0a98a578d8c99a362"
ami_id_windows2016base_2020_12_11 				= "ami-02d51b2c3cf987496" 
ami_id_windows2019container_2020_12_11  		= "ami-02f7108e0d1e573d9"
ami_id_windows2019base_2020_12_11       		= "ami-07df0407a8c4fe342"
#ami_id_redhatlinux75_2020_12_11             	= ""
ami_id_amazonlinux2base_2020_12_11           	= "ami-02496f7e580ca22e3"
ami_id_amazonlinux201803base_2020_12_11      	= "ami-04f3673aac82a6c59"
ami_id_windows2016base_server_dhicdev01			= "ami-02c6af2c8292710be"
ami_id_windows2016base_database_dhicdev01		= "ami-008e55415d7aab0cd"



#VPC Specific variables
vpc_id = "vpc-0537fe142bcc53dbb"
vpc_cidr = "10.41.8.0/22"
account_cidr = "10.41.8.0/22"
#sg_cidr = "XXX"

bcaa-dev-database-1a-subnet = "subnet-098377c0b95b0f1f9"
bcaa-dev-database-1b-subnet = "subnet-0d85882be4c97c092"
bcaa-dev-private-1a-subnet = "subnet-0bd3bf9a1631d9099"
bcaa-dev-private-1b-subnet = "subnet-07d3ecea5b28bd03e"

#RDS
mssql-rds-password = "Toronto12345"
mysql-rds-password = "Toronto12345"
postgres-rds-password = "Toronto12345"

#Accounts
bcaa-dev-account = "775629271970"
bcaa-prod-account = "338020371242"
bcaa-hub-account = "057006516829"
