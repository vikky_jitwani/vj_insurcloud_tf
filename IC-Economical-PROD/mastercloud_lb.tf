# #=======================================================================
# # PROD01
# #=======================================================================
##### ALBs #####

##### NLBs ####################
########## Database ###########
resource "aws_lb" "ICECONOMICAL_PROD01_GW_MSSQL_DB_NLB" {
  name = "ICECO-PROD01-GWDB-NLB"
  internal = true
  load_balancer_type = "network"
  enable_cross_zone_load_balancing = false
  subnets = [
    "${var.economical-prod-database-1a-subnet}",
    "${var.economical-prod-database-1b-subnet}"
  ]

  tags = {
    Contract = "MANAGED_SERVICES"
    Name = "ICECO-PROD01-GWDB-NLB"
  }
}

resource "aws_lb_target_group" "ICECONOMICAL_PROD01_GW_MSSQL_DB_NLB_TG" {
  name = "ICECO-PROD01-GWDB-NLB-TG"
  port = 1433
  protocol = "TCP"
  vpc_id = "${var.vpc_id}"

  health_check {
    protocol = "TCP"
    port = 1433
  }

  tags = {
    Contract = "MANAGED_SERVICES"
    Name = "ICECO-PROD01-GWDB-NLB-TG"
  }
}

resource "aws_lb_listener" "ICECONOMICAL_PROD01_GW_MSSQL_DB_NLB_TG_LISTENER" {
  load_balancer_arn = "${aws_lb.ICECONOMICAL_PROD01_GW_MSSQL_DB_NLB.arn}"
  port = "1433"
  protocol = "TCP"

  default_action {
    type = "forward"
    target_group_arn = "${aws_lb_target_group.ICECONOMICAL_PROD01_GW_MSSQL_DB_NLB_TG.arn}"
  }
}

resource "aws_lb_target_group_attachment" "ICECONOMICAL_PROD01_GW_MSSQL_DB_NLB_TG_ATT01" {
  target_group_arn = "${aws_lb_target_group.ICECONOMICAL_PROD01_GW_MSSQL_DB_NLB_TG.arn}"
  target_id = "${aws_instance.ICECONOMICAL_PROD01_GUIDEWIRE_MSSQL_DATABASE01.id}"
  port = 1433
}

resource "aws_lb_target_group_attachment" "ICECONOMICAL_PROD01_GW_MSSQL_DB_NLB_TG_ATT02" {
  target_group_arn = "${aws_lb_target_group.ICECONOMICAL_PROD01_GW_MSSQL_DB_NLB_TG.arn}"
  target_id = "${aws_instance.ICECONOMICAL_PROD01_GUIDEWIRE_MSSQL_DATABASE02.id}"
  port = 1433
}

output "ICECONOMICAL_PROD01_GW_MSSQL_DB_NLB_DNS_NAME" {
  value = "${aws_lb.ICECONOMICAL_PROD01_GW_MSSQL_DB_NLB.dns_name}"
}

output "ICECONOMICAL_PROD01_GW_MSSQL_DB_NLB_ZONE_ID" {
  value = "${aws_lb.ICECONOMICAL_PROD01_GW_MSSQL_DB_NLB.zone_id}"
}


########## Internal ###########
resource "aws_lb" "ICECONOMICAL_PROD01_INTERNAL_HAPROXY_NLB" {
  name = "ICECO-PROD01-INTHAP-NLB"
  internal = true
  load_balancer_type = "network"
  enable_cross_zone_load_balancing = false
  subnets = [
    "${var.economical-prod-private-1a-subnet}",
    "${var.economical-prod-private-1b-subnet}"
  ]

  tags = {
    Contract = "MANAGED_SERVICES"
    Contract = "MANAGED_SERVICES"
    Name = "ICECO-PROD01-INTHAP-NLB"
  }
}

resource "aws_lb_target_group" "ICECONOMICAL_PROD01_INTERNAL_HAPROXY_NLB_TG" {
  name = "ICECO-PROD01-INTHAP-NLB-TG"
  port = 443
  protocol = "TCP"
  vpc_id = "${var.vpc_id}"

  health_check {
    protocol = "TCP"
    port = 8444
  }

  tags = {
    Contract = "MANAGED_SERVICES"
    Name = "ICECO-PROD01-INTHAP-NLB-TG"
  }
}

resource "aws_lb_listener" "ICECONOMICAL_PROD01_INTERNAL_NLB_HAPROXY_LISTENER1" {
  load_balancer_arn = "${aws_lb.ICECONOMICAL_PROD01_INTERNAL_HAPROXY_NLB.arn}"
  port = "80"
  protocol = "TCP"

  default_action {
    type = "forward"
    target_group_arn = "${aws_lb_target_group.ICECONOMICAL_PROD01_INTERNAL_HAPROXY_NLB_TG.arn}"
  }
}

resource "aws_lb_listener" "ICECONOMICAL_PROD01_INTERNAL_NLB_HAPROXY_LISTENER2" {
  load_balancer_arn = "${aws_lb.ICECONOMICAL_PROD01_INTERNAL_HAPROXY_NLB.arn}"
  port = "443"
  protocol = "TCP"

  default_action {
    type = "forward"
    target_group_arn = "${aws_lb_target_group.ICECONOMICAL_PROD01_INTERNAL_HAPROXY_NLB_TG.arn}"
  }
}

resource "aws_lb_target_group_attachment" "ICECONOMICAL_PROD01_INTERNAL_NLB_TARGET_GROUP_ATTACHMENT_HAPROXY01" {
  target_group_arn = "${aws_lb_target_group.ICECONOMICAL_PROD01_INTERNAL_HAPROXY_NLB_TG.arn}"
  target_id = "${aws_instance.ICECONOMICAL_PROD01_INTERNAL_HAPROXY01.id}"
  port = 443
}

resource "aws_lb_target_group_attachment" "ICECONOMICAL_PROD01_INTERNAL_NLB_TARGET_GROUP_ATTACHMENT_HAPROXY02" {
  target_group_arn = "${aws_lb_target_group.ICECONOMICAL_PROD01_INTERNAL_HAPROXY_NLB_TG.arn}"
  target_id = "${aws_instance.ICECONOMICAL_PROD01_INTERNAL_HAPROXY02.id}"
  port = 443
}

output "ICECONOMICAL_PROD01_INTERNAL_NLB_DNS_NAME" {
  value = "${aws_lb.ICECONOMICAL_PROD01_INTERNAL_HAPROXY_NLB.dns_name}"
}

output "ICECONOMICAL_PROD01_INTERNAL_NLB_ZONE_ID" {
  value = "${aws_lb.ICECONOMICAL_PROD01_INTERNAL_HAPROXY_NLB.zone_id}"
}

resource "aws_lb" "ICECONOMICAL_PROD01_RPA_NLB" {
  name = "ICECONOMICAL-PROD01-RPA-NLB"
  internal = true
  load_balancer_type = "network"
  enable_cross_zone_load_balancing = false
  subnets = [
    "${var.economical-prod-private-1a-subnet}",
    "${var.economical-prod-private-1b-subnet}"
  ]

  tags = {
    Contract = "MANAGED_SERVICES"
    Name = "ICECONOMICAL-PROD01-RPA-NLB"
  }
}

resource "aws_lb_target_group" "ICECONOMICAL_PROD01_RPA_NLB_TG" {
  name = "ICECONOMICAL-PROD01-RPA-NLB-TG"
  port = 8181
  protocol = "TCP"
  vpc_id = "${var.vpc_id}"

  health_check {
    protocol = "TCP"
    port = 8181
  }

  tags = {
    Contract = "MANAGED_SERVICES"
    Name = "ICECONOMICAL-PROD01-RPA-NLB-TG"
  }
}

resource "aws_lb_listener" "ICECONOMICAL_PROD01_RPA_NLB_LISTENER" {
  load_balancer_arn = "${aws_lb.ICECONOMICAL_PROD01_RPA_NLB.arn}"
  port = "80"
  protocol = "TCP"

  default_action {
    type = "forward"
    target_group_arn = "${aws_lb_target_group.ICECONOMICAL_PROD01_RPA_NLB_TG.arn}"
  }
}

resource "aws_lb_target_group_attachment" "ICECONOMICAL_PROD01_RPA_NLB_TG_ATTACHMENT_C01" {
  target_group_arn = "${aws_lb_target_group.ICECONOMICAL_PROD01_RPA_NLB_TG.arn}"
  target_id = "${aws_instance.ICECONOMICAL_PROD01_RPA_CLIENT01.id}"
  port = 8181
}

resource "aws_lb_target_group_attachment" "ICECONOMICAL_PROD01_RPA_NLB_TG_ATTACHMENT_C02" {
  target_group_arn = "${aws_lb_target_group.ICECONOMICAL_PROD01_RPA_NLB_TG.arn}"
  target_id = "${aws_instance.ICECONOMICAL_PROD01_RPA_CLIENT02.id}"
  port = 8181
}

output "ICECONOMICAL_PROD01_RPA_NLB_DNS_NAME" {
  value = "${aws_lb.ICECONOMICAL_PROD01_RPA_NLB.dns_name}"
}

output "ICECONOMICAL_PROD01_RPA_NLB_ZONE_ID" {
  value = "${aws_lb.ICECONOMICAL_PROD01_RPA_NLB.zone_id}"
}
