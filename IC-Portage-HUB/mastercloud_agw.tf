/*
#PORTAGE API Gateways
resource "aws_api_gateway_rest_api" "PORTAGE" {
  name        = "PORTAGE"
  description = "This is the API gateway for Portage API Gateway Collection."
  endpoint_configuration {
    types = ["REGIONAL"]
  }
}

# #Token Auth DEV
# resource "aws_api_gateway_authorizer" "Auth0" {
#   name                   = "Auth0"
#   rest_api_id            = "${aws_api_gateway_rest_api.PORTAGE.id}"
#   authorizer_uri         = "${aws_lambda_function.portage-APIGatewayAuthenrizer_Hub.invoke_arn}"
#   authorizer_credentials = "arn:aws:iam::${var.portage-hub-account}:role/Auth0_invocation_role"
# }

# #VPC Link peel-insurcloud-dev01
resource "aws_api_gateway_vpc_link" "portage-insurcloud-dev01" {
  name        = "portage-insurcloud-dev01"
  target_arns = ["${aws_lb.ICPORTAGE_DEV01_APIGATEWAY_NLB.arn}"]
}

#DEV01 Resource Group 
resource "aws_api_gateway_resource" "DEV01" {
  rest_api_id = "${aws_api_gateway_rest_api.PORTAGE.id}"
  parent_id   = "${aws_api_gateway_rest_api.PORTAGE.root_resource_id}"
  path_part   = "DEV01"
}

# Calls Portage New Submission Entry MS in DEV
resource "aws_api_gateway_resource" "newSubmission" {
  rest_api_id = "${aws_api_gateway_rest_api.PORTAGE.id}"
  parent_id   = "${aws_api_gateway_resource.DEV01.id}"
  path_part   = "newSubmission"
}

resource "aws_api_gateway_method" "newSubmission" {
  rest_api_id   = "${aws_api_gateway_rest_api.PORTAGE.id}"
  resource_id   = "${aws_api_gateway_resource.newSubmission.id}"
  http_method   = "POST"
  authorization = "NONE"
 #authorizer_id = "${aws_api_gateway_authorizer.Auth0.id}"
}

resource "aws_api_gateway_integration" "newSubmission" {
  rest_api_id             = "${aws_api_gateway_rest_api.PORTAGE.id}"
  resource_id             = "${aws_api_gateway_resource.newSubmission.id}"
  http_method             = "${aws_api_gateway_method.newSubmission.http_method}"
  integration_http_method = "POST"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.portage-insurcloud-dev01.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms1.dev01.portage.insurcloud.ca:9002/newsubmission-service/microservices/newsubmission/ws/"                           
}

resource "aws_api_gateway_method_response" "newSubmission" {
  rest_api_id = "${aws_api_gateway_rest_api.PORTAGE.id}"
  resource_id = "${aws_api_gateway_resource.newSubmission.id}"
  http_method = "${aws_api_gateway_method.newSubmission.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}


# Deployment stages in DEV01, PPS and PROD
# resource "aws_api_gateway_deployment" "PORTAGE" {
#   depends_on = ["aws_api_gateway_integration.newSubmission"]
#   rest_api_id = "${aws_api_gateway_rest_api.PORTAGE.id}"
#   stage_name  = "PORTAGE"
# }

#waf and logs needs to be set at stage level afterwards 

*/