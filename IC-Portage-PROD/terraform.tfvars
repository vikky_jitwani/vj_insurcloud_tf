#Envrionment specific variables
client_name = "IC-Portage"
enviornment_name = "PROD"
enviornment_type = "P"
client_number = "A005"
#ad-dc1 = "10.10.4.10"
#ad-dc2 = "10.10.6.10"
volume_key = "arn:aws:kms:ca-central-1:625725589452:key/f98a8019-6c9e-4ba8-8173-a14465501bed"
ami_id_windows2012R2sql14enterprise = "ami-0b7e5050eee245b99"
ami_id_windows2012R2sql14standard = "ami-0913a2afb4fa5fc5b"
ami_id_windows2019container = "ami-013453540e1aa0d48"
ami_id_windows2019base = "ami-06b8f69a907cf6bb2"
ami_id_windows2016base20200806 = "ami-0388b8d3cb35753f8"
ami_id_windows2016base = "ami-0bce38ba7ac166aa9"
ami_id_windows2012R2base = "ami-08f7dedaab95e4621"
ami_id_windows2008base = "ami-0446e08d2d99fc8c9"
ami_id_redhatlinux75 = "ami-05c33d286020a47f9"
ami_id_ubuntu1804baseimage = "ami-032172cc5f34b32fc"
ami_id_ubuntu1604baseimage = "ami-04f40cca01b3186ea"
ami_id_amazonlinux201712base = "ami-0a777bbf8ef3f43bf"
ami_id_amazonlinux201709base = "ami-058dba934995c5468"
ami_id_amazonlinux2base = "ami-071594d49d11fcb38"
ami_id_amazonlinux201803base = "ami-06829694f407e15c1"
ami_id_amazonlinuxbase20200806 = "ami-04978aa25eaba28b7"

#VPC Specific variables
vpc_id = "vpc-045517b075443581e"
vpc_cidr = "10.171.4.0/22"


portage-prod-database-1a-subnet = "subnet-05fd013c51d1b8fdb"
portage-prod-database-1b-subnet = "subnet-0c14ea446ee75ed54"
portage-prod-private-1a-subnet = "subnet-0530979d2b514d0f7"
portage-prod-private-1b-subnet = "subnet-0b8e2a4b95fe3426c"


#RDS
mssql-rds-password = "Toronto12345"
mysql-rds-password = "Toronto12345"
postgres-rds-password = "Toronto12345"

#Accounts
portage-dev-account = "091072499599"
portage-prod-account = "625725589452"
portage-hub-account = "342339668068"
