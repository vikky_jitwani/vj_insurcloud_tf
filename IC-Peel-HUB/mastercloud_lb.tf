##### NLBs ####################
########## External Load Balancers: North of firewall ###########
#=======================================================================
# PROD
#=======================================================================
resource "aws_lb" "ICPEEL_PROD_BROKER_NLB" {
    name               = "peel-insurcloud"
    internal           = false
    load_balancer_type = "network"
    enable_cross_zone_load_balancing = false
    subnets            = [
        "${var.peel-hub-public-1a-subnet}",
        "${var.peel-hub-public-1b-subnet}"
    ] # Changing this value will force a recreation of the resource

    enable_deletion_protection = false

    # access_logs {
    #     bucket  = "${aws_s3_bucket.ICPEEL_PROD_BROKER_NLB.bucket}"
    #     enabled = true
    # }

    tags = {
        Name = "PEEL-PROD-BROKER-NLB"
    }
}

resource "aws_lb_target_group" "ICPEEL_PROD_BROKER_NLB_TARGET_GROUP" {
    name        = "CW-PROD-BROKER-NLB"
    port        = 443
    protocol    = "TCP"
    target_type = "ip"
    vpc_id      = "${var.vpc_id}"

    health_check {
        protocol = "TCP"
        port = 8444
    }

    tags = {
        Name = "PEEL-PROD-BROKER-NLB-TARGET-GROUP"
    }
}

resource "aws_lb_listener" "ICPEEL_PROD_BROKER_NLB_LISTENER" {
    load_balancer_arn = "${aws_lb.ICPEEL_PROD_BROKER_NLB.arn}"
    port              = "443"
    protocol          = "TCP"

    default_action {
        type             = "forward"
        target_group_arn = "${aws_lb_target_group.ICPEEL_PROD_BROKER_NLB_TARGET_GROUP.arn}"
    }
}

resource "aws_lb" "ICPEEL_PROD_APIGATEWAY_NLB" {
    name               = "peel-apigateway-insurcloud"
    internal           = true
    load_balancer_type = "network"
    enable_cross_zone_load_balancing = false
    subnets            = [
        "${var.peel-hub-private-1a-subnet}",
        "${var.peel-hub-private-1b-subnet}"
    ] # Changing this value will force a recreation of the resource

    enable_deletion_protection = false

    # access_logs {
    #     bucket  = "${aws_s3_bucket.ICPEEL_PROD_APIGATEWAY_NLB.bucket}"
    #     enabled = true
    # }

    tags = {
        Name = "PEEL-PROD-APIGATEWAY-NLB"
    }
}

resource "aws_lb_target_group" "ICPEEL_PROD_APIGATEWAY_NLB_TARGET_GROUP" {
    name        = "PEEL-PROD-APIGATEWAY-NLB"
    port        = 443
    protocol    = "TCP"
    target_type = "ip"
    vpc_id      = "${var.vpc_id}"

    health_check {
        protocol = "TCP"
        port = 8444
    }

    tags = {
        Name = "PEEL-PROD-APIGATEWAY-NLB-TARGET-GROUP"
    }
}

resource "aws_lb_listener" "ICPEEL_PROD_APIGATEWAY_NLB_LISTENER" {
    load_balancer_arn = "${aws_lb.ICPEEL_PROD_APIGATEWAY_NLB.arn}"
    port              = "443"
    protocol          = "TCP"

    default_action {
        type             = "forward"
        target_group_arn = "${aws_lb_target_group.ICPEEL_PROD_APIGATEWAY_NLB_TARGET_GROUP.arn}"
    }
}

#=======================================================================
# PPS
#=======================================================================
resource "aws_lb" "ICPEEL_PPS_BROKER_NLB" {
    name               = "peel-insurcloud-pps"
    internal           = false
    load_balancer_type = "network"
    enable_cross_zone_load_balancing = false
    subnets            = [
        "${var.peel-hub-public-1a-subnet}",
        "${var.peel-hub-public-1b-subnet}"
    ] # Changing this value will force a recreation of the resource
    
    enable_deletion_protection = false

    # access_logs {
    #     bucket  = "${aws_s3_bucket.ICPEEL_PPS_BROKER_NLB.bucket}"
    #     enabled = true
    # }

    tags = {
        Name = "PEEL-PPS-BROKER-NLB"
    }
}

resource "aws_lb_target_group" "ICPEEL_PPS_BROKER_NLB_TARGET_GROUP" {
    name        = "PEEL-PPS-BROKER-NLB"
    port        = 443
    protocol    = "TCP"
    target_type = "ip"
    vpc_id      = "${var.vpc_id}"

    health_check {
        protocol = "TCP"
        port = 8444
    }

    tags = {
        Name = "PEEL-PPS-BROKER-NLB-TARGET-GROUP"
    }
}

resource "aws_lb_listener" "ICPEEL_PPS_BROKER_NLB_LISTENER" {
    load_balancer_arn = "${aws_lb.ICPEEL_PPS_BROKER_NLB.arn}"
    port              = "443"
    protocol          = "TCP"

    default_action {
        type             = "forward"
        target_group_arn = "${aws_lb_target_group.ICPEEL_PPS_BROKER_NLB_TARGET_GROUP.arn}"
    }
}


resource "aws_lb" "ICPEEL_PPS_APIGATEWAY_NLB" {
    name               = "peel-apigateway-pps"
    internal           = true
    load_balancer_type = "network"
    enable_cross_zone_load_balancing = false
    subnets            = [
        "${var.peel-hub-private-1a-subnet}",
        "${var.peel-hub-private-1b-subnet}"
    ] # Changing this value will force a recreation of the resource
    
    enable_deletion_protection = false

    # access_logs {
    #     bucket  = "${aws_s3_bucket.ICPEEL_PPS_APIGATEWAY_NLB.bucket}"
    #     enabled = true
    # }

    tags = {
        Name = "PEEL-PPS-APIGATEWAY-NLB"
    }
}

resource "aws_lb_target_group" "ICPEEL_PPS_APIGATEWAY_NLB_TARGET_GROUP" {
    name        = "PEEL-PPS-APIGATEWAY-NLB"
    port        = 443
    protocol    = "TCP"
    target_type = "ip"
    vpc_id      = "${var.vpc_id}"

    health_check {
        protocol = "TCP"
        port = 8444
    }

    tags = {
        Name = "PEEL-PPS-APIGATEWAY-NLB-TARGET-GROUP"
    }
}

resource "aws_lb_listener" "ICPEEL_PPS_APIGATEWAY_NLB_LISTENER" {
    load_balancer_arn = "${aws_lb.ICPEEL_PPS_APIGATEWAY_NLB.arn}"
    port              = "443"
    protocol          = "TCP"

    default_action {
        type             = "forward"
        target_group_arn = "${aws_lb_target_group.ICPEEL_PPS_APIGATEWAY_NLB_TARGET_GROUP.arn}"
    }
}

#=======================================================================
# DEV01
#=======================================================================
resource "aws_lb" "ICPEEL_DEV01_BROKER_NLB" {
    name               = "peel-insurcloud-dev01"
    internal           = false
    load_balancer_type = "network"
    enable_cross_zone_load_balancing = false
    subnets            = [
        "${var.peel-hub-public-1a-subnet}",
        "${var.peel-hub-public-1b-subnet}"
    ] # Changing this value will force a recreation of the resource
    
    enable_deletion_protection = false

    # access_logs {
    #     bucket  = "${aws_s3_bucket.ICPEEL_DEV01_BROKER_NLB.bucket}"
    #     enabled = true
    # }

    tags = {
        Name = "PEEL-DEV01-BROKER-NLB"
    }
}

resource "aws_lb_target_group" "ICPEEL_DEV01_BROKER_NLB_TARGET_GROUP" {
    name        = "PEEL-DEV01-BROKER-NLB"
    port        = 443
    protocol    = "TCP"
    target_type = "ip"
    vpc_id      = "${var.vpc_id}"

    health_check {
        protocol = "TCP"
        port = 8444
    }

    tags = {
        Name = "PEEL-DEV01-BROKER-NLB-TARGET-GROUP"
    }
}

resource "aws_lb_listener" "ICPEEL_DEV01_BROKER_NLB_LISTENER" {
    load_balancer_arn = "${aws_lb.ICPEEL_DEV01_BROKER_NLB.arn}"
    port              = "443"
    protocol          = "TCP"

    default_action {
        type             = "forward"
        target_group_arn = "${aws_lb_target_group.ICPEEL_DEV01_BROKER_NLB_TARGET_GROUP.arn}"
    }
}


resource "aws_lb" "ICPEEL_DEV01_APIGATEWAY_NLB" {
    name               = "peel-insurcloud-apigateway-dev01"
    internal           = true
    load_balancer_type = "network"
    enable_cross_zone_load_balancing = false
    subnets            = [
        "${var.peel-hub-private-1a-subnet}",
        "${var.peel-hub-private-1b-subnet}"
    ] # Changing this value will force a recreation of the resource
    
    enable_deletion_protection = false

    # access_logs {
    #     bucket  = "${aws_s3_bucket.ICPEEL_DEV01_APIGATEWAY_NLB.bucket}"
    #     enabled = true
    # }

    tags = {
        Name = "PEEL-DEV01-APIGATEWAY-NLB"
    }
}

resource "aws_lb_target_group" "ICPEEL_DEV01_APIGATEWAY_NLB_TARGET_GROUP" {
    name        = "PEEL-DEV01-APIGATEWAY-NLB"
    port        = 80
    protocol    = "TCP"
    target_type = "ip"
    vpc_id      = "${var.vpc_id}"

    health_check {
        protocol = "TCP"
        port = 8444
    }

    tags = {
        Name = "PEEL-DEV01-APIGATEWAY-NLB-TARGET-GROUP"
    }
}

resource "aws_lb_listener" "ICPEEL_DEV01_APIGATEWAY_NLB_LISTENER" {
    load_balancer_arn = "${aws_lb.ICPEEL_DEV01_APIGATEWAY_NLB.arn}"
    port              = "80"
    protocol          = "TCP"

    default_action {
        type             = "forward"
        target_group_arn = "${aws_lb_target_group.ICPEEL_DEV01_APIGATEWAY_NLB_TARGET_GROUP.arn}"
    }
}
