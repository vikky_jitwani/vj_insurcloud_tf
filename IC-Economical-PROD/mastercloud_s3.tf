resource "aws_s3_bucket" "ICECONOMICAL_PROD_MS_RSTUDIO_DROPZONE" {
  bucket = "eco-ms-prod-rstudio-dropzone"
  acl = "private"
  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm = "AES256"
      }
    }
  }

  lifecycle_rule {
    enabled = true

    transition {
      days = 30
      storage_class = "STANDARD_IA"
    }
  }

  tags = {
    AccountID = "${var.economical-prod-account}"
    Client = "Economical"
    Contract = "MANAGED_SERVICES"
    Name = "eco-ms-prod-rstudio-dropzone"
  }
}

resource "aws_s3_bucket_policy" "ICECONOMICAL_PROD_MS_RSTUDIO_DROPZONE_BUCKET_POLICY" {
  bucket = "eco-ms-prod-rstudio-dropzone"

  policy = <<POLICY
{
    "Version": "2012-10-17",
    "Id": "ICECONOMICAL_PROD_MS_RSTUDIO_DROPZONE_BUCKET_POLICY",
    "Statement": [
        {
            "Sid": "DenyPublicReadACL",
            "Effect": "Deny",
            "Principal": {
                "AWS": "*"
            },
            "Action": [
                "s3:PutObject",
                "s3:PutObjectAcl"
            ],
            "Resource": "arn:aws:s3:::${aws_s3_bucket.ICECONOMICAL_PROD_MS_RSTUDIO_DROPZONE.id}/*",
            "Condition": {
                "StringEquals": {
                    "s3:x-amz-acl": [
                        "public-read",
                        "public-read-write",
                        "authenticated-read"
                    ]
                }
            }
        },
        {
            "Sid": "DenyPublicReadGrant",
            "Effect": "Deny",
            "Principal": {
                "AWS": "*"
            },
            "Action": [
                "s3:PutObject",
                "s3:PutObjectAcl"
            ],
            "Resource": "arn:aws:s3:::${aws_s3_bucket.ICECONOMICAL_PROD_MS_RSTUDIO_DROPZONE.id}/*",
            "Condition": {
                "StringLike": {
                    "s3:x-amz-grant-read": [
                        "*http://acs.amazonaws.com/groups/global/AllUsers*",
                        "*http://acs.amazonaws.com/groups/global/AuthenticatedUsers*"
                    ]
                }
            }
        },
        {
            "Sid": "DenyPublicListACL",
            "Effect": "Deny",
            "Principal": {
                "AWS": "*"
            },
            "Action": "s3:PutBucketAcl",
            "Resource": "arn:aws:s3:::${aws_s3_bucket.ICECONOMICAL_PROD_MS_RSTUDIO_DROPZONE.id}",
            "Condition": {
                "StringEquals": {
                    "s3:x-amz-acl": [
                        "public-read",
                        "public-read-write",
                        "authenticated-read"
                    ]
                }
            }
        },
        {
            "Sid": "DenyPublicListGrant",
            "Effect": "Deny",
            "Principal": {
                "AWS": "*"
            },
            "Action": "s3:PutBucketAcl",
            "Resource": "arn:aws:s3:::${aws_s3_bucket.ICECONOMICAL_PROD_MS_RSTUDIO_DROPZONE.id}",
            "Condition": {
                "StringLike": {
                    "s3:x-amz-grant-read": [
                        "*http://acs.amazonaws.com/groups/global/AllUsers*",
                        "*http://acs.amazonaws.com/groups/global/AuthenticatedUsers*"
                    ]
                }
            }
        },
        {
            "Sid": "DenyPublicReadConsole",
            "Effect": "Deny",
            "Principal": {
                "AWS": "*"
            },
            "Action": [
                "s3:PutObjectAcl",
                "s3:PutBucketAcl"
            ],
            "Resource": [
                "arn:aws:s3:::${aws_s3_bucket.ICECONOMICAL_PROD_MS_RSTUDIO_DROPZONE.id}/*",
                "arn:aws:s3:::${aws_s3_bucket.ICECONOMICAL_PROD_MS_RSTUDIO_DROPZONE.id}"
            ],
            "Condition": {
                "Null": {
                    "s3:x-amz-grant-read": "true",
                    "s3:x-amz-grant-read-acp": "true",
                    "s3:x-amz-grant-full-control": "true",
                    "s3:x-amz-grant-write-acp": "true",
                    "s3:x-amz-acl": "true",
                    "s3:x-amz-grant-write": "true"
                }
            }
        },
        {
            "Sid": "ProdInstanceListBucketAllow",
            "Effect": "Allow",
            "Principal": {
                "AWS": [
                    "arn:aws:iam::${var.economical-prod-account}:role/CMS-SSMRole",
                    "arn:aws:iam::${var.economical-prod-account}:user/eco-application-user"
                ]
            },
            "Action": "s3:ListBucket",
            "Resource": "arn:aws:s3:::${aws_s3_bucket.ICECONOMICAL_PROD_MS_RSTUDIO_DROPZONE.id}"
        },
        {
            "Sid": "ProdInstanceObjectAllow",
            "Effect": "Allow",
            "Principal": {
                "AWS": [
                    "arn:aws:iam::${var.economical-prod-account}:role/CMS-SSMRole",
                    "arn:aws:iam::${var.economical-prod-account}:user/eco-application-user"
                ]
            },
            "Action": [
                "s3:GetObject",
                "s3:PutObject",
                "s3:DeleteObject"
            ],
            "Resource": [
                "arn:aws:s3:::${aws_s3_bucket.ICECONOMICAL_PROD_MS_RSTUDIO_DROPZONE.id}",
                "arn:aws:s3:::${aws_s3_bucket.ICECONOMICAL_PROD_MS_RSTUDIO_DROPZONE.id}/*"
            ]
        }
    ]
}
POLICY
}

resource "aws_s3_bucket" "ICECONOMICAL_PROD_MS_DOCUMENTS" {
  bucket = "eco-ms-prod-documents"
  acl = "private"
  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm = "AES256"
      }
    }
  }

  lifecycle_rule {
    enabled = true

    transition {
      days = 30
      storage_class = "STANDARD_IA"
    }
  }

  tags = {
    AccountID = "${var.economical-prod-account}"
    Client = "Economical"
    Contract = "MANAGED_SERVICES"
    Name = "eco-ms-prod-documents"
  }
}

resource "aws_s3_bucket_policy" "ICECONOMICAL_PROD_MS_DOCUMENTS_BUCKET_POLICY" {
  bucket = "eco-ms-prod-documents"
  depends_on = [
    "aws_s3_bucket.ICECONOMICAL_PROD_MS_DOCUMENTS"
  ]

  policy = <<POLICY
{
    "Version": "2012-10-17",
    "Id": "ICECONOMICAL_PROD_MS_DOCUMENTS_BUCKET_POLICY",
    "Statement": [
        {
            "Sid": "DenyPublicReadACL",
            "Effect": "Deny",
            "Principal": {
                "AWS": "*"
            },
            "Action": [
                "s3:PutObject",
                "s3:PutObjectAcl"
            ],
            "Resource": "arn:aws:s3:::eco-ms-prod-documents/*",
            "Condition": {
                "StringEquals": {
                    "s3:x-amz-acl": [
                        "public-read",
                        "public-read-write",
                        "authenticated-read"
                    ]
                }
            }
        },
        {
            "Sid": "DenyPublicReadGrant",
            "Effect": "Deny",
            "Principal": {
                "AWS": "*"
            },
            "Action": [
                "s3:PutObject",
                "s3:PutObjectAcl"
            ],
            "Resource": "arn:aws:s3:::eco-ms-prod-documents/*",
            "Condition": {
                "StringLike": {
                    "s3:x-amz-grant-read": [
                        "*http://acs.amazonaws.com/groups/global/AllUsers*",
                        "*http://acs.amazonaws.com/groups/global/AuthenticatedUsers*"
                    ]
                }
            }
        },
        {
            "Sid": "DenyPublicListACL",
            "Effect": "Deny",
            "Principal": {
                "AWS": "*"
            },
            "Action": "s3:PutBucketAcl",
            "Resource": "arn:aws:s3:::eco-ms-prod-documents",
            "Condition": {
                "StringEquals": {
                    "s3:x-amz-acl": [
                        "public-read",
                        "public-read-write",
                        "authenticated-read"
                    ]
                }
            }
        },
        {
            "Sid": "DenyPublicListGrant",
            "Effect": "Deny",
            "Principal": {
                "AWS": "*"
            },
            "Action": "s3:PutBucketAcl",
            "Resource": "arn:aws:s3:::eco-ms-prod-documents",
            "Condition": {
                "StringLike": {
                    "s3:x-amz-grant-read": [
                        "*http://acs.amazonaws.com/groups/global/AllUsers*",
                        "*http://acs.amazonaws.com/groups/global/AuthenticatedUsers*"
                    ]
                }
            }
        },
        {
            "Sid": "DenyPublicReadConsole",
            "Effect": "Deny",
            "Principal": {
                "AWS": "*"
            },
            "Action": [
                "s3:PutObjectAcl",
                "s3:PutBucketAcl"
            ],
            "Resource": [
                "arn:aws:s3:::eco-ms-prod-documents/*",
                "arn:aws:s3:::eco-ms-prod-documents"
            ],
            "Condition": {
                "Null": {
                    "s3:x-amz-grant-write-acp": "true",
                    "s3:x-amz-grant-read": "true",
                    "s3:x-amz-acl": "true",
                    "s3:x-amz-grant-read-acp": "true",
                    "s3:x-amz-grant-full-control": "true",
                    "s3:x-amz-grant-write": "true"
                }
            }
        },
        {
            "Sid": "ProdInstanceListBucketAllow",
            "Effect": "Allow",
            "Principal": {
                "AWS": [
                    "arn:aws:iam::${var.economical-prod-account}:user/eco-application-user",
                    "arn:aws:iam::${var.economical-prod-account}:role/CMS-SSMRole"
                ]
            },
            "Action": "s3:ListBucket",
            "Resource": "arn:aws:s3:::eco-ms-prod-documents"
        },
        {
            "Sid": "ProdInstanceObjectAllow",
            "Effect": "Allow",
            "Principal": {
                "AWS": [
                    "arn:aws:iam::${var.economical-prod-account}:user/eco-application-user",
                    "arn:aws:iam::${var.economical-prod-account}:role/CMS-SSMRole"
                ]
            },
            "Action": [
                "s3:GetObject",
                "s3:DeleteObject"
            ],
            "Resource": [
                "arn:aws:s3:::eco-ms-prod-documents",
                "arn:aws:s3:::eco-ms-prod-documents/*"
            ]
        },
        {
            "Sid": "ForceSSLDeny",
            "Effect": "Deny",
            "Principal": {
                "AWS": "*"
            },
            "Action": "s3:PutObject",
            "Resource": [
                "arn:aws:s3:::eco-ms-prod-documents",
                "arn:aws:s3:::eco-ms-prod-documents/*"
            ],
            "Condition": {
                "Bool": {
                    "aws:SecureTransport": "false"
                }
            }
        },
        {
            "Sid": "UberSourceIp",
            "Effect": "Deny",
            "Principal": {
                "AWS": "*"
            },
            "Action": "s3:PutObject",
            "Resource": [
                "arn:aws:s3:::eco-ms-prod-documents",
                "arn:aws:s3:::eco-ms-prod-documents/*"
            ],
            "Condition": {
                "NotIpAddress": {
                    "aws:SourceIp": [
                        "10.0.0.0/8",
                        "172.0.0.0/8",
                        "15.223.12.234/32",
                        "15.222.109.117/32",
                        "104.36.192.0/21"
                    ]
                }
            }
        }
    ]
}
POLICY
}

resource "aws_s3_bucket" "ICECONOMICAL_PROD_MON_PROM" {
  bucket = "eco-ms-prod-monitoring-prometheus"
  acl = "private"
  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm = "AES256"
      }
    }
  }

  lifecycle_rule {
    enabled = true

    transition {
      days = 30
      storage_class = "STANDARD_IA"
    }
  }

  tags = {
    Contract = "MANAGED_SERVICES"
    Name = "eco-ms-prod-monitoring-prometheus"
  }

  lifecycle {
    ignore_changes = ["logging"]
  }
}

resource "aws_s3_bucket_policy" "ICECONOMICAL_PROD_MON_PROM_BUCKET_POLICY" {
  bucket = "eco-ms-prod-monitoring-prometheus"
  depends_on = [
    "aws_s3_bucket.ICECONOMICAL_PROD_MON_PROM"
  ]

  policy = <<POLICY
{
    "Version": "2012-10-17",
    "Id": "ICECONOMICAL_PROD_MON_PROM_BUCKET_POLICY",
    "Statement": [
        {
            "Sid": "DenyPublicReadACL",
            "Effect": "Deny",
            "Principal": {
                "AWS": "*"
            },
            "Action": [
                "s3:PutObject",
                "s3:PutObjectAcl"
            ],
            "Resource": "arn:aws:s3:::eco-ms-prod-monitoring-prometheus/*",
            "Condition": {
                "StringEquals": {
                    "s3:x-amz-acl": [
                        "public-read",
                        "public-read-write",
                        "authenticated-read"
                    ]
                }
            }
        },
        {
            "Sid": "DenyPublicReadGrant",
            "Effect": "Deny",
            "Principal": {
                "AWS": "*"
            },
            "Action": [
                "s3:PutObject",
                "s3:PutObjectAcl"
            ],
            "Resource": "arn:aws:s3:::eco-ms-prod-monitoring-prometheus/*",
            "Condition": {
                "StringLike": {
                    "s3:x-amz-grant-read": [
                        "*http://acs.amazonaws.com/groups/global/AllUsers*",
                        "*http://acs.amazonaws.com/groups/global/AuthenticatedUsers*"
                    ]
                }
            }
        },
        {
            "Sid": "DenyPublicListACL",
            "Effect": "Deny",
            "Principal": {
                "AWS": "*"
            },
            "Action": "s3:PutBucketAcl",
            "Resource": "arn:aws:s3:::eco-ms-prod-monitoring-prometheus",
            "Condition": {
                "StringEquals": {
                    "s3:x-amz-acl": [
                        "public-read",
                        "public-read-write",
                        "authenticated-read"
                    ]
                }
            }
        },
        {
            "Sid": "DenyPublicListGrant",
            "Effect": "Deny",
            "Principal": {
                "AWS": "*"
            },
            "Action": "s3:PutBucketAcl",
            "Resource": "arn:aws:s3:::eco-ms-prod-monitoring-prometheus",
            "Condition": {
                "StringLike": {
                    "s3:x-amz-grant-read": [
                        "*http://acs.amazonaws.com/groups/global/AllUsers*",
                        "*http://acs.amazonaws.com/groups/global/AuthenticatedUsers*"
                    ]
                }
            }
        },
        {
            "Sid": "DenyPublicReadConsole",
            "Effect": "Deny",
            "Principal": {
                "AWS": "*"
            },
            "Action": [
                "s3:PutObjectAcl",
                "s3:PutBucketAcl"
            ],
            "Resource": [
                "arn:aws:s3:::eco-ms-prod-monitoring-prometheus/*",
                "arn:aws:s3:::eco-ms-prod-monitoring-prometheus"
            ],
            "Condition": {
                "Null": {
                    "s3:x-amz-grant-write": "true",
                    "s3:x-amz-grant-read": "true",
                    "s3:x-amz-acl": "true",
                    "s3:x-amz-grant-read-acp": "true",
                    "s3:x-amz-grant-full-control": "true",
                    "s3:x-amz-grant-write-acp": "true"
                }
            }
        },
        {
            "Sid": "ProdInstanceListBucketAllow",
            "Effect": "Allow",
            "Principal": {
                "AWS": [
                    "arn:aws:iam::${var.economical-prod-account}:role/CMS-SSMRole",
                    "arn:aws:iam::${var.economical-prod-account}:user/eco-application-user"
                ]
            },
            "Action": "s3:ListBucket",
            "Resource": "arn:aws:s3:::eco-ms-prod-monitoring-prometheus"
        },
        {
            "Sid": "ProdInstanceObjectAllow",
            "Effect": "Allow",
            "Principal": {
                "AWS": [
                    "arn:aws:iam::${var.economical-prod-account}:role/CMS-SSMRole",
                    "arn:aws:iam::${var.economical-prod-account}:user/eco-application-user"
                ]
            },
            "Action": [
                "s3:GetObject",
                "s3:PutObject",
                "s3:DeleteObject"
            ],
            "Resource": [
                "arn:aws:s3:::eco-ms-prod-monitoring-prometheus/*"
            ]
        }
    ]
}
POLICY
}
