resource "aws_route53_resolver_endpoint" "IC_HUB_DNS" {
  name      = "ic-hub-dns"
  direction = "OUTBOUND"

  security_group_ids = [
    "${aws_security_group.SG_IC_HUB_DNS_RESOLVER_ENDPOINT.id}"
  ]

  ip_address {
    subnet_id = "${var.bcaa-dev-private-1a-subnet}"
  }

  ip_address {
    subnet_id = "${var.bcaa-dev-private-1b-subnet}"
  }
}

resource "aws_route53_resolver_rule" "IC_HUB_DNS" {
  domain_name          = "insurcloud.ca"
  name                 = "ic-hub-dns"
  rule_type            = "FORWARD"
  resolver_endpoint_id = "${aws_route53_resolver_endpoint.IC_HUB_DNS.id}"

  target_ip {
    ip = "10.15.17.177"
  }
  target_ip {
    ip = "10.15.16.149"
  }
}

resource "aws_route53_resolver_rule_association" "IC_HUB_DNS" {
  resolver_rule_id = "${aws_route53_resolver_rule.IC_HUB_DNS.id}"
  vpc_id           = "${var.vpc_id}"
}
