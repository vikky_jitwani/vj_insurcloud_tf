resource "aws_s3_bucket" "ICPORTAGE_DEV_MS_DOCUMENTS" {
  bucket = "portage-microservices-documents-dev"
  acl    = "private"

  lifecycle_rule {
    enabled = true

    transition {
      days          = 30
      storage_class = "STANDARD_IA"
    }
  }

  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm     = "aws:kms"
        kms_master_key_id = "arn:aws:kms:ca-central-1:091072499599:key/c4ea7960-0340-4db3-96c3-dac13da4e25e"
      }
    }
  }

  tags {
    Name = "portage-microservices-documents-dev"
  }
}

resource "aws_s3_bucket_policy" "ICPORTAGE_DEV_MS_DOCUMENTS_BUCKET_POLICY" {
  bucket = "${aws_s3_bucket.ICPORTAGE_DEV_MS_DOCUMENTS.id}"

  policy = <<POLICY
{
    "Version": "2012-10-17",
    "Id": "ICPORTAGE_DEV_MS_DOCUMENTS",
    "Statement": [
        {
            "Sid": "DenyPublicReadACL",
            "Effect": "Deny",
            "Principal": {
                "AWS": "*"
            },
            "Action": [
                "s3:PutObject",
                "s3:PutObjectAcl"
            ],
            "Resource": "arn:aws:s3:::${aws_s3_bucket.ICPORTAGE_DEV_MS_DOCUMENTS.id}/*",
            "Condition": {
                "StringEquals": {
                    "s3:x-amz-acl": [
                        "public-read",
                        "public-read-write",
                        "authenticated-read"
                    ]
                }
            }
        },
        {
            "Sid": "DenyPublicReadGrant",
            "Effect": "Deny",
            "Principal": {
                "AWS": "*"
            },
            "Action": [
                "s3:PutObject",
                "s3:PutObjectAcl"
            ],
            "Resource": "arn:aws:s3:::${aws_s3_bucket.ICPORTAGE_DEV_MS_DOCUMENTS.id}/*",
            "Condition": {
                "StringLike": {
                    "s3:x-amz-grant-read": [
                        "*http://acs.amazonaws.com/groups/global/AllUsers*",
                        "*http://acs.amazonaws.com/groups/global/AuthenticatedUsers*"
                    ]
                }
            }
        },
        {
            "Sid": "DenyPublicListACL",
            "Effect": "Deny",
            "Principal": {
                "AWS": "*"
            },
            "Action": "s3:PutBucketAcl",
            "Resource": "arn:aws:s3:::${aws_s3_bucket.ICPORTAGE_DEV_MS_DOCUMENTS.id}",
            "Condition": {
                "StringEquals": {
                    "s3:x-amz-acl": [
                        "public-read",
                        "public-read-write",
                        "authenticated-read"
                    ]
                }
            }
        },
        {
            "Sid": "DenyPublicListGrant",
            "Effect": "Deny",
            "Principal": {
                "AWS": "*"
            },
            "Action": "s3:PutBucketAcl",
            "Resource": "arn:aws:s3:::${aws_s3_bucket.ICPORTAGE_DEV_MS_DOCUMENTS.id}",
            "Condition": {
                "StringLike": {
                    "s3:x-amz-grant-read": [
                        "*http://acs.amazonaws.com/groups/global/AllUsers*",
                        "*http://acs.amazonaws.com/groups/global/AuthenticatedUsers*"
                    ]
                }
            }
        },
        {
            "Sid": "DenyPublicReadConsole",
            "Effect": "Deny",
            "Principal": {
                "AWS": "*"
            },
            "Action": [
                "s3:PutObjectAcl",
                "s3:PutBucketAcl"
            ],
            "Resource": [
                "arn:aws:s3:::${aws_s3_bucket.ICPORTAGE_DEV_MS_DOCUMENTS.id}/*",
                "arn:aws:s3:::${aws_s3_bucket.ICPORTAGE_DEV_MS_DOCUMENTS.id}"
            ],
            "Condition": {
                "Null": {
                    "s3:x-amz-grant-write": "true",
                    "s3:x-amz-grant-read": "true",
                    "s3:x-amz-acl": "true",
                    "s3:x-amz-grant-read-acp": "true",
                    "s3:x-amz-grant-full-control": "true",
                    "s3:x-amz-grant-write-acp": "true"
                }
            }
        },
        {
            "Sid": "InstanceListBucketAllow",
            "Effect": "Allow",
            "Principal": {
                "AWS": "arn:aws:iam::${var.portage-dev-account}:user/portage-application-user"
            },
            "Action": "s3:ListBucket",
            "Resource": "arn:aws:s3:::${aws_s3_bucket.ICPORTAGE_DEV_MS_DOCUMENTS.id}"
        },
        {
            "Sid": "InstanceObjectAllow",
            "Effect": "Allow",
            "Principal": {
                "AWS": "arn:aws:iam::${var.portage-dev-account}:user/portage-application-user"
            },
            "Action": [
                "s3:GetObject",
                "s3:PutObject",
                "s3:DeleteObject"
            ],
            "Resource": "arn:aws:s3:::${aws_s3_bucket.ICPORTAGE_DEV_MS_DOCUMENTS.id}/*"
        }
    ]
}
POLICY
}

resource "aws_s3_bucket" "ICPORTAGE_EC2_DB_BACKUPS" {
  bucket = "portage-ec2-db-backups"
  acl    = "private"

  lifecycle_rule {
    enabled = true

    transition {
      days          = 30
      storage_class = "STANDARD_IA"
    }
  }

  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm = "AES256"
      }
    }
  }

  tags {
    Name = "portage-ec2-db-backups"
  }
}

resource "aws_s3_bucket_policy" "ICPORTAGE_EC2_DB_BACKUPS" {
  bucket = "${aws_s3_bucket.ICPORTAGE_EC2_DB_BACKUPS.id}"

  policy = <<POLICY
{
    "Version": "2012-10-17",
    "Id": "ICPORTAGE_EC2_DB_BACKUPS",
    "Statement": [
        {
            "Sid": "DenyPublicReadACL",
            "Effect": "Deny",
            "Principal": {
                "AWS": "*"
            },
            "Action": [
                "s3:PutObject",
                "s3:PutObjectAcl"
            ],
            "Resource": "arn:aws:s3:::${aws_s3_bucket.ICPORTAGE_EC2_DB_BACKUPS.id}/*",
            "Condition": {
                "StringEquals": {
                    "s3:x-amz-acl": [
                        "public-read",
                        "public-read-write",
                        "authenticated-read"
                    ]
                }
            }
        },
        {
            "Sid": "DenyPublicReadGrant",
            "Effect": "Deny",
            "Principal": {
                "AWS": "*"
            },
            "Action": [
                "s3:PutObject",
                "s3:PutObjectAcl"
            ],
            "Resource": "arn:aws:s3:::${aws_s3_bucket.ICPORTAGE_EC2_DB_BACKUPS.id}/*",
            "Condition": {
                "StringLike": {
                    "s3:x-amz-grant-read": [
                        "*http://acs.amazonaws.com/groups/global/AllUsers*",
                        "*http://acs.amazonaws.com/groups/global/AuthenticatedUsers*"
                    ]
                }
            }
        },
        {
            "Sid": "DenyPublicListACL",
            "Effect": "Deny",
            "Principal": {
                "AWS": "*"
            },
            "Action": "s3:PutBucketAcl",
            "Resource": "arn:aws:s3:::${aws_s3_bucket.ICPORTAGE_EC2_DB_BACKUPS.id}",
            "Condition": {
                "StringEquals": {
                    "s3:x-amz-acl": [
                        "public-read",
                        "public-read-write",
                        "authenticated-read"
                    ]
                }
            }
        },
        {
            "Sid": "DenyPublicListGrant",
            "Effect": "Deny",
            "Principal": {
                "AWS": "*"
            },
            "Action": "s3:PutBucketAcl",
            "Resource": "arn:aws:s3:::${aws_s3_bucket.ICPORTAGE_EC2_DB_BACKUPS.id}",
            "Condition": {
                "StringLike": {
                    "s3:x-amz-grant-read": [
                        "*http://acs.amazonaws.com/groups/global/AllUsers*",
                        "*http://acs.amazonaws.com/groups/global/AuthenticatedUsers*"
                    ]
                }
            }
        },
        {
            "Sid": "DenyPublicReadConsole",
            "Effect": "Deny",
            "Principal": {
                "AWS": "*"
            },
            "Action": [
                "s3:PutObjectAcl",
                "s3:PutBucketAcl"
            ],
            "Resource": [
                "arn:aws:s3:::${aws_s3_bucket.ICPORTAGE_EC2_DB_BACKUPS.id}/*",
                "arn:aws:s3:::${aws_s3_bucket.ICPORTAGE_EC2_DB_BACKUPS.id}"
            ],
            "Condition": {
                "Null": {
                    "s3:x-amz-grant-write": "true",
                    "s3:x-amz-grant-read": "true",
                    "s3:x-amz-acl": "true",
                    "s3:x-amz-grant-read-acp": "true",
                    "s3:x-amz-grant-full-control": "true",
                    "s3:x-amz-grant-write-acp": "true"
                }
            }
        },
        {
            "Sid": "InstanceListBucketAllow",
            "Effect": "Allow",
            "Principal": {
                "AWS": "arn:aws:iam::${var.portage-dev-account}:role/CMS-SSMRole"
            },
            "Action": "s3:ListBucket",
            "Resource": "arn:aws:s3:::${aws_s3_bucket.ICPORTAGE_EC2_DB_BACKUPS.id}"
        },
        {
            "Sid": "InstanceObjectAllow",
            "Effect": "Allow",
            "Principal": {
                "AWS": "arn:aws:iam::${var.portage-dev-account}:role/CMS-SSMRole"
            },
            "Action": [
                "s3:GetObject",
                "s3:PutObject",
                "s3:DeleteObject"
            ],
            "Resource": "arn:aws:s3:::${aws_s3_bucket.ICPORTAGE_EC2_DB_BACKUPS.id}/*"
        }
    ]
}
POLICY
}

resource "aws_s3_bucket" "ICPORTAGE_DEV01_PROM_THANOS" {
  bucket = "portage-monitoring-prometheus-dev01"
  acl    = "private"

  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm     = "aws:kms"
        kms_master_key_id = "arn:aws:kms:ca-central-1:091072499599:key/c4ea7960-0340-4db3-96c3-dac13da4e25e"
      }
    }
  }

  tags {
    Name = "portage-microservices-documents-dev01"
  }
}

resource "aws_s3_bucket_policy" "ICPORTAGE_DEV01_PROM_THANOS_BUCKET_POLICY" {
  bucket = "${aws_s3_bucket.ICPORTAGE_DEV01_PROM_THANOS.id}"

  policy = <<POLICY
{
    "Version": "2012-10-17",
    "Id": "ICPORTAGE_DEV01_PROM_THANOS",
    "Statement": [
        {
            "Sid": "DenyPublicReadACL",
            "Effect": "Deny",
            "Principal": {
                "AWS": "*"
            },
            "Action": [
                "s3:PutObject",
                "s3:PutObjectAcl"
            ],
            "Resource": "arn:aws:s3:::${aws_s3_bucket.ICPORTAGE_DEV01_PROM_THANOS.id}/*",
            "Condition": {
                "StringEquals": {
                    "s3:x-amz-acl": [
                        "public-read",
                        "public-read-write",
                        "authenticated-read"
                    ]
                }
            }
        },
        {
            "Sid": "DenyPublicReadGrant",
            "Effect": "Deny",
            "Principal": {
                "AWS": "*"
            },
            "Action": [
                "s3:PutObject",
                "s3:PutObjectAcl"
            ],
            "Resource": "arn:aws:s3:::${aws_s3_bucket.ICPORTAGE_DEV01_PROM_THANOS.id}/*",
            "Condition": {
                "StringLike": {
                    "s3:x-amz-grant-read": [
                        "*http://acs.amazonaws.com/groups/global/AllUsers*",
                        "*http://acs.amazonaws.com/groups/global/AuthenticatedUsers*"
                    ]
                }
            }
        },
        {
            "Sid": "DenyPublicListACL",
            "Effect": "Deny",
            "Principal": {
                "AWS": "*"
            },
            "Action": "s3:PutBucketAcl",
            "Resource": "arn:aws:s3:::${aws_s3_bucket.ICPORTAGE_DEV01_PROM_THANOS.id}",
            "Condition": {
                "StringEquals": {
                    "s3:x-amz-acl": [
                        "public-read",
                        "public-read-write",
                        "authenticated-read"
                    ]
                }
            }
        },
        {
            "Sid": "DenyPublicListGrant",
            "Effect": "Deny",
            "Principal": {
                "AWS": "*"
            },
            "Action": "s3:PutBucketAcl",
            "Resource": "arn:aws:s3:::${aws_s3_bucket.ICPORTAGE_DEV01_PROM_THANOS.id}",
            "Condition": {
                "StringLike": {
                    "s3:x-amz-grant-read": [
                        "*http://acs.amazonaws.com/groups/global/AllUsers*",
                        "*http://acs.amazonaws.com/groups/global/AuthenticatedUsers*"
                    ]
                }
            }
        },
        {
            "Sid": "DenyPublicReadConsole",
            "Effect": "Deny",
            "Principal": {
                "AWS": "*"
            },
            "Action": [
                "s3:PutObjectAcl",
                "s3:PutBucketAcl"
            ],
            "Resource": [
                "arn:aws:s3:::${aws_s3_bucket.ICPORTAGE_DEV01_PROM_THANOS.id}/*",
                "arn:aws:s3:::${aws_s3_bucket.ICPORTAGE_DEV01_PROM_THANOS.id}"
            ],
            "Condition": {
                "Null": {
                    "s3:x-amz-grant-write": "true",
                    "s3:x-amz-grant-read": "true",
                    "s3:x-amz-acl": "true",
                    "s3:x-amz-grant-read-acp": "true",
                    "s3:x-amz-grant-full-control": "true",
                    "s3:x-amz-grant-write-acp": "true"
                }
            }
        },
        {
            "Sid": "InstanceListBucketAllow",
            "Effect": "Allow",
            "Principal": {
                "AWS": "arn:aws:iam::${var.portage-dev-account}:user/portage-application-user"
            },
            "Action": "s3:ListBucket",
            "Resource": "arn:aws:s3:::${aws_s3_bucket.ICPORTAGE_DEV01_PROM_THANOS.id}"
        },
        {
            "Sid": "InstanceObjectAllow",
            "Effect": "Allow",
            "Principal": {
                "AWS": "arn:aws:iam::${var.portage-dev-account}:user/portage-application-user"
            },
            "Action": [
                "s3:GetObject",
                "s3:PutObject",
                "s3:DeleteObject"
            ],
            "Resource": "arn:aws:s3:::${aws_s3_bucket.ICPORTAGE_DEV01_PROM_THANOS.id}/*"
        }
    ]
}
POLICY
}

resource "aws_s3_bucket" "ICPORTAGE_DEV02_PROM_THANOS" {
  bucket = "portage-monitoring-prometheus-dev02"
  acl    = "private"

  lifecycle_rule {
    enabled = true

    transition {
      days          = 30
      storage_class = "STANDARD_IA"
    }
  }

  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm     = "aws:kms"
        kms_master_key_id = "arn:aws:kms:ca-central-1:091072499599:key/c4ea7960-0340-4db3-96c3-dac13da4e25e"
      }
    }
  }

  tags {
    Name = "portage-microservices-documents-dev02"
  }
}

resource "aws_s3_bucket_policy" "ICPORTAGE_DEV02_PROM_THANOS" {
  bucket = "${aws_s3_bucket.ICPORTAGE_DEV02_PROM_THANOS.id}"

  policy = <<POLICY
{
    "Version": "2012-10-17",
    "Id": "ICPORTAGE_DEV02_PROM_THANOS",
    "Statement": [
        {
            "Sid": "DenyPublicReadACL",
            "Effect": "Deny",
            "Principal": {
                "AWS": "*"
            },
            "Action": [
                "s3:PutObject",
                "s3:PutObjectAcl"
            ],
            "Resource": "arn:aws:s3:::${aws_s3_bucket.ICPORTAGE_DEV02_PROM_THANOS.id}/*",
            "Condition": {
                "StringEquals": {
                    "s3:x-amz-acl": [
                        "public-read",
                        "public-read-write",
                        "authenticated-read"
                    ]
                }
            }
        },
        {
            "Sid": "DenyPublicReadGrant",
            "Effect": "Deny",
            "Principal": {
                "AWS": "*"
            },
            "Action": [
                "s3:PutObject",
                "s3:PutObjectAcl"
            ],
            "Resource": "arn:aws:s3:::${aws_s3_bucket.ICPORTAGE_DEV02_PROM_THANOS.id}/*",
            "Condition": {
                "StringLike": {
                    "s3:x-amz-grant-read": [
                        "*http://acs.amazonaws.com/groups/global/AllUsers*",
                        "*http://acs.amazonaws.com/groups/global/AuthenticatedUsers*"
                    ]
                }
            }
        },
        {
            "Sid": "DenyPublicListACL",
            "Effect": "Deny",
            "Principal": {
                "AWS": "*"
            },
            "Action": "s3:PutBucketAcl",
            "Resource": "arn:aws:s3:::${aws_s3_bucket.ICPORTAGE_DEV02_PROM_THANOS.id}",
            "Condition": {
                "StringEquals": {
                    "s3:x-amz-acl": [
                        "public-read",
                        "public-read-write",
                        "authenticated-read"
                    ]
                }
            }
        },
        {
            "Sid": "DenyPublicListGrant",
            "Effect": "Deny",
            "Principal": {
                "AWS": "*"
            },
            "Action": "s3:PutBucketAcl",
            "Resource": "arn:aws:s3:::${aws_s3_bucket.ICPORTAGE_DEV02_PROM_THANOS.id}",
            "Condition": {
                "StringLike": {
                    "s3:x-amz-grant-read": [
                        "*http://acs.amazonaws.com/groups/global/AllUsers*",
                        "*http://acs.amazonaws.com/groups/global/AuthenticatedUsers*"
                    ]
                }
            }
        },
        {
            "Sid": "DenyPublicReadConsole",
            "Effect": "Deny",
            "Principal": {
                "AWS": "*"
            },
            "Action": [
                "s3:PutObjectAcl",
                "s3:PutBucketAcl"
            ],
            "Resource": [
                "arn:aws:s3:::${aws_s3_bucket.ICPORTAGE_DEV02_PROM_THANOS.id}/*",
                "arn:aws:s3:::${aws_s3_bucket.ICPORTAGE_DEV02_PROM_THANOS.id}"
            ],
            "Condition": {
                "Null": {
                    "s3:x-amz-grant-write": "true",
                    "s3:x-amz-grant-read": "true",
                    "s3:x-amz-acl": "true",
                    "s3:x-amz-grant-read-acp": "true",
                    "s3:x-amz-grant-full-control": "true",
                    "s3:x-amz-grant-write-acp": "true"
                }
            }
        },
        {
            "Sid": "InstanceListBucketAllow",
            "Effect": "Allow",
            "Principal": {
                "AWS": "arn:aws:iam::${var.portage-dev-account}:user/portage-application-user"
            },
            "Action": "s3:ListBucket",
            "Resource": "arn:aws:s3:::${aws_s3_bucket.ICPORTAGE_DEV02_PROM_THANOS.id}"
        },
        {
            "Sid": "InstanceObjectAllow",
            "Effect": "Allow",
            "Principal": {
                "AWS": "arn:aws:iam::${var.portage-dev-account}:user/portage-application-user"
            },
            "Action": [
                "s3:GetObject",
                "s3:PutObject",
                "s3:DeleteObject"
            ],
            "Resource": "arn:aws:s3:::${aws_s3_bucket.ICPORTAGE_DEV02_PROM_THANOS.id}/*"
        }
    ]
}
POLICY
}

resource "aws_s3_bucket" "ICPORTAGE_SMARTCOMM-DEV-INPUT" {
  bucket = "portage-smartcomm-dev-input-dev"
  acl    = "private"

  lifecycle_rule {
    enabled = true

    transition {
      days          = 30
      storage_class = "STANDARD_IA"
    }

  }

  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm     = "aws:kms"
        kms_master_key_id = "arn:aws:kms:ca-central-1:091072499599:key/c4ea7960-0340-4db3-96c3-dac13da4e25e"
      }
    }
  }

  tags {
    Name = "portage-smartcomm-dev-input-dev"
  }
}

resource "aws_s3_bucket_policy" "ICPORTAGE_SMARTCOMM-DEV-INPUT" {
  bucket = "${aws_s3_bucket.ICPORTAGE_SMARTCOMM-DEV-INPUT.id}"

  policy = <<POLICY
{
    "Version": "2012-10-17",
    "Id": "ICPORTAGE_SMARTCOMM-DEV-INPUT",
    "Statement": [
        {
            "Sid": "DenyPublicReadACL",
            "Effect": "Deny",
            "Principal": {
                "AWS": "*"
            },
            "Action": [
                "s3:PutObject",
                "s3:PutObjectAcl"
            ],
            "Resource": "arn:aws:s3:::${aws_s3_bucket.ICPORTAGE_SMARTCOMM-DEV-INPUT.id}/*",
            "Condition": {
                "StringEquals": {
                    "s3:x-amz-acl": [
                        "public-read",
                        "public-read-write",
                        "authenticated-read"
                    ]
                }
            }
        },
        {
            "Sid": "DenyPublicReadGrant",
            "Effect": "Deny",
            "Principal": {
                "AWS": "*"
            },
            "Action": [
                "s3:PutObject",
                "s3:PutObjectAcl"
            ],
            "Resource": "arn:aws:s3:::${aws_s3_bucket.ICPORTAGE_SMARTCOMM-DEV-INPUT.id}/*",
            "Condition": {
                "StringLike": {
                    "s3:x-amz-grant-read": [
                        "*http://acs.amazonaws.com/groups/global/AllUsers*",
                        "*http://acs.amazonaws.com/groups/global/AuthenticatedUsers*"
                    ]
                }
            }
        },
        {
            "Sid": "DenyPublicListACL",
            "Effect": "Deny",
            "Principal": {
                "AWS": "*"
            },
            "Action": "s3:PutBucketAcl",
            "Resource": "arn:aws:s3:::${aws_s3_bucket.ICPORTAGE_SMARTCOMM-DEV-INPUT.id}",
            "Condition": {
                "StringEquals": {
                    "s3:x-amz-acl": [
                        "public-read",
                        "public-read-write",
                        "authenticated-read"
                    ]
                }
            }
        },
        {
            "Sid": "DenyPublicListGrant",
            "Effect": "Deny",
            "Principal": {
                "AWS": "*"
            },
            "Action": "s3:PutBucketAcl",
            "Resource": "arn:aws:s3:::${aws_s3_bucket.ICPORTAGE_SMARTCOMM-DEV-INPUT.id}",
            "Condition": {
                "StringLike": {
                    "s3:x-amz-grant-read": [
                        "*http://acs.amazonaws.com/groups/global/AllUsers*",
                        "*http://acs.amazonaws.com/groups/global/AuthenticatedUsers*"
                    ]
                }
            }
        },
        {
            "Sid": "DenyPublicReadConsole",
            "Effect": "Deny",
            "Principal": {
                "AWS": "*"
            },
            "Action": [
                "s3:PutObjectAcl",
                "s3:PutBucketAcl"
            ],
            "Resource": [
                "arn:aws:s3:::${aws_s3_bucket.ICPORTAGE_SMARTCOMM-DEV-INPUT.id}/*",
                "arn:aws:s3:::${aws_s3_bucket.ICPORTAGE_SMARTCOMM-DEV-INPUT.id}"
            ],
            "Condition": {
                "Null": {
                    "s3:x-amz-grant-write": "true",
                    "s3:x-amz-grant-read": "true",
                    "s3:x-amz-acl": "true",
                    "s3:x-amz-grant-read-acp": "true",
                    "s3:x-amz-grant-full-control": "true",
                    "s3:x-amz-grant-write-acp": "true"
                }
            }
        },
        {
            "Sid": "InstanceListBucketAllow",
            "Effect": "Allow",
            "Principal": {
                "AWS": "arn:aws:iam::${var.portage-dev-account}:user/portage-application-user"
            },
            "Action": "s3:ListBucket",
            "Resource": "arn:aws:s3:::${aws_s3_bucket.ICPORTAGE_SMARTCOMM-DEV-INPUT.id}"
        },
        {
            "Sid": "InstanceObjectAllow",
            "Effect": "Allow",
            "Principal": {
                "AWS": "arn:aws:iam::${var.portage-dev-account}:user/portage-application-user"
            },
            "Action": [
                "s3:GetObject",
                "s3:PutObject",
                "s3:DeleteObject"
            ],
            "Resource": "arn:aws:s3:::${aws_s3_bucket.ICPORTAGE_SMARTCOMM-DEV-INPUT.id}/*"
        }
    ]
}
POLICY
}

resource "aws_s3_bucket" "ICPORTAGE_SMARTCOMM-DEV-OUTPUT" {
  bucket = "portage-smartcomm-dev-output-dev"
  acl    = "private"

  lifecycle_rule {
    enabled = true

    transition {
      days          = 30
      storage_class = "STANDARD_IA"
    }

  }

  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm     = "aws:kms"
        kms_master_key_id = "arn:aws:kms:ca-central-1:091072499599:key/c4ea7960-0340-4db3-96c3-dac13da4e25e"
      }
    }
  }

  tags {
    Name = "portage-smartcomm-dev-output-dev"
  }
}

resource "aws_s3_bucket_policy" "ICPORTAGE_SMARTCOMM-DEV-OUTPUT" {
  bucket = "${aws_s3_bucket.ICPORTAGE_SMARTCOMM-DEV-OUTPUT.id}"

  policy = <<POLICY
{
    "Version": "2012-10-17",
    "Id": "ICPORTAGE_SMARTCOMM-DEV-OUTPUT",
    "Statement": [
        {
            "Sid": "DenyPublicReadACL",
            "Effect": "Deny",
            "Principal": {
                "AWS": "*"
            },
            "Action": [
                "s3:PutObject",
                "s3:PutObjectAcl"
            ],
            "Resource": "arn:aws:s3:::${aws_s3_bucket.ICPORTAGE_SMARTCOMM-DEV-OUTPUT.id}/*",
            "Condition": {
                "StringEquals": {
                    "s3:x-amz-acl": [
                        "public-read",
                        "public-read-write",
                        "authenticated-read"
                    ]
                }
            }
        },
        {
            "Sid": "DenyPublicReadGrant",
            "Effect": "Deny",
            "Principal": {
                "AWS": "*"
            },
            "Action": [
                "s3:PutObject",
                "s3:PutObjectAcl"
            ],
            "Resource": "arn:aws:s3:::${aws_s3_bucket.ICPORTAGE_SMARTCOMM-DEV-OUTPUT.id}/*",
            "Condition": {
                "StringLike": {
                    "s3:x-amz-grant-read": [
                        "*http://acs.amazonaws.com/groups/global/AllUsers*",
                        "*http://acs.amazonaws.com/groups/global/AuthenticatedUsers*"
                    ]
                }
            }
        },
        {
            "Sid": "DenyPublicListACL",
            "Effect": "Deny",
            "Principal": {
                "AWS": "*"
            },
            "Action": "s3:PutBucketAcl",
            "Resource": "arn:aws:s3:::${aws_s3_bucket.ICPORTAGE_SMARTCOMM-DEV-OUTPUT.id}",
            "Condition": {
                "StringEquals": {
                    "s3:x-amz-acl": [
                        "public-read",
                        "public-read-write",
                        "authenticated-read"
                    ]
                }
            }
        },
        {
            "Sid": "DenyPublicListGrant",
            "Effect": "Deny",
            "Principal": {
                "AWS": "*"
            },
            "Action": "s3:PutBucketAcl",
            "Resource": "arn:aws:s3:::${aws_s3_bucket.ICPORTAGE_SMARTCOMM-DEV-OUTPUT.id}",
            "Condition": {
                "StringLike": {
                    "s3:x-amz-grant-read": [
                        "*http://acs.amazonaws.com/groups/global/AllUsers*",
                        "*http://acs.amazonaws.com/groups/global/AuthenticatedUsers*"
                    ]
                }
            }
        },
        {
            "Sid": "DenyPublicReadConsole",
            "Effect": "Deny",
            "Principal": {
                "AWS": "*"
            },
            "Action": [
                "s3:PutObjectAcl",
                "s3:PutBucketAcl"
            ],
            "Resource": [
                "arn:aws:s3:::${aws_s3_bucket.ICPORTAGE_SMARTCOMM-DEV-OUTPUT.id}/*",
                "arn:aws:s3:::${aws_s3_bucket.ICPORTAGE_SMARTCOMM-DEV-OUTPUT.id}"
            ],
            "Condition": {
                "Null": {
                    "s3:x-amz-grant-write": "true",
                    "s3:x-amz-grant-read": "true",
                    "s3:x-amz-acl": "true",
                    "s3:x-amz-grant-read-acp": "true",
                    "s3:x-amz-grant-full-control": "true",
                    "s3:x-amz-grant-write-acp": "true"
                }
            }
        },
        {
            "Sid": "InstanceListBucketAllow",
            "Effect": "Allow",
            "Principal": {
                "AWS": "arn:aws:iam::${var.portage-dev-account}:user/portage-application-user"
            },
            "Action": "s3:ListBucket",
            "Resource": "arn:aws:s3:::${aws_s3_bucket.ICPORTAGE_SMARTCOMM-DEV-OUTPUT.id}"
        },
        {
            "Sid": "InstanceObjectAllow",
            "Effect": "Allow",
            "Principal": {
                "AWS": "arn:aws:iam::${var.portage-dev-account}:user/portage-application-user"
            },
            "Action": [
                "s3:GetObject",
                "s3:PutObject",
                "s3:DeleteObject"
            ],
            "Resource": "arn:aws:s3:::${aws_s3_bucket.ICPORTAGE_SMARTCOMM-DEV-OUTPUT.id}/*"
        }
    ]
}
POLICY
}

resource "aws_s3_bucket" "ic-portage-dev-lambda-ms" {
  bucket = "ic-portage-dev-lambda-ms"
  acl    = "private"

  lifecycle_rule {
    enabled = true

    transition {
      days          = 30
      storage_class = "STANDARD_IA"
    }

  }

  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm     = "aws:kms"
        kms_master_key_id = "arn:aws:kms:ca-central-1:091072499599:key/c4ea7960-0340-4db3-96c3-dac13da4e25e"
      }
    }
  }

  tags {
    Name = "ic-portage-dev-lambda-ms"
  }
}

resource "aws_s3_bucket_policy" "ic-portage-dev-lambda-ms" {
  bucket = "${aws_s3_bucket.ic-portage-dev-lambda-ms.id}"

  policy = <<POLICY
{
    "Version": "2012-10-17",
    "Id": "ic-portage-dev-lambda-ms",
    "Statement": [
        {
            "Sid": "DenyPublicReadACL",
            "Effect": "Deny",
            "Principal": {
                "AWS": "*"
            },
            "Action": [
                "s3:PutObject",
                "s3:PutObjectAcl"
            ],
            "Resource": "arn:aws:s3:::${aws_s3_bucket.ic-portage-dev-lambda-ms.id}/*",
            "Condition": {
                "StringEquals": {
                    "s3:x-amz-acl": [
                        "public-read",
                        "public-read-write",
                        "authenticated-read"
                    ]
                }
            }
        },
        {
            "Sid": "DenyPublicReadGrant",
            "Effect": "Deny",
            "Principal": {
                "AWS": "*"
            },
            "Action": [
                "s3:PutObject",
                "s3:PutObjectAcl"
            ],
            "Resource": "arn:aws:s3:::${aws_s3_bucket.ic-portage-dev-lambda-ms.id}/*",
            "Condition": {
                "StringLike": {
                    "s3:x-amz-grant-read": [
                        "*http://acs.amazonaws.com/groups/global/AllUsers*",
                        "*http://acs.amazonaws.com/groups/global/AuthenticatedUsers*"
                    ]
                }
            }
        },
        {
            "Sid": "DenyPublicListACL",
            "Effect": "Deny",
            "Principal": {
                "AWS": "*"
            },
            "Action": "s3:PutBucketAcl",
            "Resource": "arn:aws:s3:::${aws_s3_bucket.ic-portage-dev-lambda-ms.id}",
            "Condition": {
                "StringEquals": {
                    "s3:x-amz-acl": [
                        "public-read",
                        "public-read-write",
                        "authenticated-read"
                    ]
                }
            }
        },
        {
            "Sid": "DenyPublicListGrant",
            "Effect": "Deny",
            "Principal": {
                "AWS": "*"
            },
            "Action": "s3:PutBucketAcl",
            "Resource": "arn:aws:s3:::${aws_s3_bucket.ic-portage-dev-lambda-ms.id}",
            "Condition": {
                "StringLike": {
                    "s3:x-amz-grant-read": [
                        "*http://acs.amazonaws.com/groups/global/AllUsers*",
                        "*http://acs.amazonaws.com/groups/global/AuthenticatedUsers*"
                    ]
                }
            }
        },
        {
            "Sid": "DenyPublicReadConsole",
            "Effect": "Deny",
            "Principal": {
                "AWS": "*"
            },
            "Action": [
                "s3:PutObjectAcl",
                "s3:PutBucketAcl"
            ],
            "Resource": [
                "arn:aws:s3:::${aws_s3_bucket.ic-portage-dev-lambda-ms.id}/*",
                "arn:aws:s3:::${aws_s3_bucket.ic-portage-dev-lambda-ms.id}"
            ],
            "Condition": {
                "Null": {
                    "s3:x-amz-grant-write": "true",
                    "s3:x-amz-grant-read": "true",
                    "s3:x-amz-acl": "true",
                    "s3:x-amz-grant-read-acp": "true",
                    "s3:x-amz-grant-full-control": "true",
                    "s3:x-amz-grant-write-acp": "true"
                }
            }
        },
        {
            "Sid": "InstanceListBucketAllow",
            "Effect": "Allow",
            "Principal": {
                "AWS": "arn:aws:iam::${var.portage-dev-account}:user/portage-application-user"
            },
            "Action": "s3:ListBucket",
            "Resource": "arn:aws:s3:::${aws_s3_bucket.ic-portage-dev-lambda-ms.id}"
        },
        {
            "Sid": "InstanceObjectAllow",
            "Effect": "Allow",
            "Principal": {
                "AWS": "arn:aws:iam::${var.portage-dev-account}:user/portage-application-user"
            },
            "Action": [
                "s3:GetObject",
                "s3:PutObject",
                "s3:DeleteObject"
            ],
            "Resource": "arn:aws:s3:::${aws_s3_bucket.ic-portage-dev-lambda-ms.id}/*"
        }
    ]
}
POLICY
}