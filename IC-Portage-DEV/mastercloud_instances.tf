# #=======================================================================
# # Shared Resource PPS/DEV/QA
# #=======================================================================

############# JENKINS CLIENT #################
resource "aws_instance" "ICPORTAGE_DEV_JENKINS_CLIENT" {
  ami                         = "ami-0cfd82bf21b5f28b3" #jenkins backup AMI 26 August 2020
  vpc_security_group_ids      = ["${aws_security_group.SG_JENKINS_CLIENT.id}"]
  subnet_id                   = "${var.portage-dev-private-1a-subnet}"
  instance_type               = "r5a.xlarge"
  key_name                    = "PortageJenkinsDev"
  associate_public_ip_address = false
  iam_instance_profile        = "CMS-SSMInstanceRole"
  ebs_optimized               = "true"

  lifecycle {
    prevent_destroy = false
  }

  root_block_device {
    volume_size = "100"
  }

  tags {
    Owner              = "robhatnagar@deloitte.ca"
    ManagedBy          = "CMS-TF"
    SolutionName       = "Insurcloud Portage"
    Environment        = "Dev"
    ApplicationName    = "jenkins, docker"
    DataClassification = "NA"
    DLM-Backup         = "Yes"
    DLM-Retention      = "90"
    PatchGroup         = "AmazonLinux2"
    VendorManaged      = "False"
    ResourceType       = "Client"
    ResourceSeverity   = "High"
    Name               = "A-MC-CAC-D-A005-PORTAGE-DEV-JENKINS-CLIENT-EC2"
    AccountID          = "${data.aws_caller_identity.current.account_id}"
    Role               = "Container"
    CLIENT             = "Portage"
  }
}

output "ICPORTAGE_DEV_JENKINS_CLIENT_PRIVATEIP" {
  value = "${aws_instance.ICPORTAGE_DEV_JENKINS_CLIENT.private_ip}"
}

######### DATA ##############

######### DHIC DATABASE SERVER ###########

data "template_file" "PORTAGE_DOMAIN_JOIN_1" {
  template = "${file("IC-Portage-DEV/windowsdata.tpl")}"

  vars {
    computer_name  = "DEV01DHIC-DB"
    region         = "${var.region}"
    domain         = "${var.ad_domain}"
    dns1           = "${var.ad_dns1}"
    dns2           = "${var.ad_dns2}"
    dns3           = "${var.ad_dns3}"
    ouselection    = "${var.ou_name}"
    pwordparameter = "${var.pwordparameter}"
    unameparameter = "${var.unameparameter}"
  }
}

resource "aws_instance" "ICPORTAGE_DEV01_DHIC_MSSQL_DATABASE" {
  ami = "${var.ami_id_windows2016base20200806}"

  vpc_security_group_ids = [
    "${aws_security_group.SG_MSSQL_DB.id}",
    "${aws_security_group.SG_PORTAGE_MSSQL_DB.id}",
    "${aws_security_group.SG_AVIATRIX_RDP.id}",
  ]

  subnet_id                   = "${var.portage-dev-database-1a-subnet}"
  instance_type               = "r5a.large"
  key_name                    = "PortageDatabaseDev"
  associate_public_ip_address = false
  iam_instance_profile        = "CMS-SSMInstanceRole"
  ebs_optimized               = "true"

  lifecycle {
    prevent_destroy = false
  }

  root_block_device {
    volume_size = "256"
  }

  user_data = "${data.template_file.PORTAGE_DOMAIN_JOIN_1.rendered}"

  tags {
    Owner              = "robhatnagar@deloitte.ca"
    ManagedBy          = "CMS-TF"
    SolutionName       = "Insurcloud Portage"
    Environment        = "Dev"
    ApplicationName    = "database, mssql, sap, ssis"
    DataClassification = "PII, PHI"
    DLM-Backup         = "Yes"
    DLM-Retention      = "90"
    PatchGroup         = "Windows"
    VendorManaged      = "False"
    OSName             = "Windows"
    ResourceType       = "Client"
    ResourceSeverity   = "Normal"
    Name               = "A-MC-CAC-D-A005-PORTAGE-DEV01-DHIC-DATABASE-MSSQL-WIN-EC2"
    AccountID          = "${data.aws_caller_identity.current.account_id}"
    Role               = "Container"
    CLIENT             = "Portage"
  }

  provisioner "local-exec" {
    command = "echo ${aws_instance.ICPORTAGE_DEV01_DHIC_MSSQL_DATABASE.private_ip} >> dev_hosts.txt"
  }
}

output "ICPORTAGE_DEV01_DHIC_MSSQL_DATABASE_PRIVATEIP" {
  value = "${aws_instance.ICPORTAGE_DEV01_DHIC_MSSQL_DATABASE.private_ip}"
}

######### DHIC SERVER ###########

data "template_file" "PORTAGE_DOMAIN_JOIN_2" {
  template = "${file("IC-Portage-DEV/windowsdata.tpl")}"

  vars {
    computer_name  = "DEV01DHIC"
    region         = "${var.region}"
    domain         = "${var.ad_domain}"
    dns1           = "${var.ad_dns1}"
    dns2           = "${var.ad_dns2}"
    dns3           = "${var.ad_dns3}"
    ouselection    = "${var.ou_name}"
    pwordparameter = "${var.pwordparameter}"
    unameparameter = "${var.unameparameter}"
  }
}

resource "aws_instance" "ICPORTAGE_DEV01_DHIC_SERVER" {
  ami = "${var.ami_id_windows2016base20200806}"

  vpc_security_group_ids = [
    "${aws_security_group.SG_DHIC.id}",
    "${aws_security_group.SG_AVIATRIX_RDP.id}",
    "${aws_security_group.SG_WORKSPACES.id}"
  ]

  subnet_id                   = "${var.portage-dev-private-1a-subnet}"
  instance_type               = "t3a.xlarge"
  key_name                    = "PortageDataDev"
  associate_public_ip_address = false
  iam_instance_profile        = "CMS-SSMInstanceRole"
  ebs_optimized               = "true"
  lifecycle {
    prevent_destroy = false
  }
  root_block_device {
    volume_size = "500"
  }
  user_data = "${data.template_file.PORTAGE_DOMAIN_JOIN_2.rendered}"
  tags {
    Owner              = "robhatnagar@deloitte.ca"
    ManagedBy          = "CMS-TF"
    SolutionName       = "Insurcloud Portage"
    Environment        = "Dev"
    ApplicationName    = "sap"
    DataClassification = "PII, PHI"
    DLM-Backup         = "Yes"
    DLM-Retention      = "90"
    PatchGroup         = "Windows"
    VendorManaged      = "False"
    OSName             = "Windows"
    ResourceType       = "Client"
    ResourceSeverity   = "Normal"
    Name               = "A-MC-CAC-D-A005-PORTAGE-DEV01-DHIC-SERVER-WIN-EC2"
    AccountID          = "${data.aws_caller_identity.current.account_id}"
    Role               = "Container"
    CLIENT             = "Portage"
  }
  provisioner "local-exec" {
    command = "echo ${aws_instance.ICPORTAGE_DEV01_DHIC_SERVER.private_ip} >> dev_hosts.txt"
  }
}

output "ICPORTAGE_DEV01_DHIC_SERVER_PRIVATEIP" {
  value = "${aws_instance.ICPORTAGE_DEV01_DHIC_SERVER.private_ip}"
}

######### DHIC WINDOWS CLIENTSERVER ###########

data "template_file" "PORTAGE_DOMAIN_JOIN_3" {
  template = "${file("IC-Portage-DEV/windowsdata.tpl")}"

  vars {
    computer_name  = "DEV01DHIC"
    region         = "${var.region}"
    domain         = "${var.ad_domain}"
    dns1           = "${var.ad_dns1}"
    dns2           = "${var.ad_dns2}"
    dns3           = "${var.ad_dns3}"
    ouselection    = "${var.ou_name}"
    pwordparameter = "${var.pwordparameter}"
    unameparameter = "${var.unameparameter}"
  }
}

resource "aws_instance" "ICPORTAGE_DEV01_DHIC_SERVER_MAC" {
  ami = "${var.ami_id_windows2016base20200806}"

  vpc_security_group_ids = [
    "${aws_security_group.SG_DHIC.id}",
    "${aws_security_group.SG_AVIATRIX_RDP.id}",
    "${aws_security_group.SG_WORKSPACES.id}"
  ]

  subnet_id                   = "${var.portage-dev-private-1a-subnet}"
  instance_type               = "t3a.xlarge"
  key_name                    = "PortageDataDev"
  associate_public_ip_address = false
  iam_instance_profile        = "CMS-SSMInstanceRole"
  ebs_optimized               = "true"
  lifecycle {
    prevent_destroy = false
  }
  root_block_device {
    volume_size = "100"
  }
  user_data = "${data.template_file.PORTAGE_DOMAIN_JOIN_3.rendered}"
  tags {
    Owner              = "robhatnagar@deloitte.ca"
    ManagedBy          = "CMS-TF"
    SolutionName       = "Insurcloud Portage"
    Environment        = "Dev"
    ApplicationName    = "sap"
    DataClassification = "PII, PHI"
    DLM-Backup         = "Yes"
    DLM-Retention      = "90"
    PatchGroup         = "Windows"
    VendorManaged      = "False"
    OSName             = "Windows"
    ResourceType       = "Client"
    ResourceSeverity   = "Normal"
    Name               = "A-MC-CAC-D-A005-PORTAGE-DEV01-DHIC-SERVER-MAC-WIN-EC2"
    AccountID          = "${data.aws_caller_identity.current.account_id}"
    Role               = "Container"
    CLIENT             = "Portage"
  }
  provisioner "local-exec" {
    command = "echo ${aws_instance.ICPORTAGE_DEV01_DHIC_SERVER_MAC.private_ip} >> dev_hosts.txt"
  }
}

output "ICPORTAGE_DEV01_DHIC_SERVER_PRIVATEIP_MAC" {
  value = "${aws_instance.ICPORTAGE_DEV01_DHIC_SERVER_MAC.private_ip}"
}

######################################

# #=======================================================================
# # DEV01
# #=======================================================================

######### GUIDEWIRE ##############
resource "aws_instance" "ICPORTAGE_DEV01_GUIDEWIRE_SUITE" {
  ami = "${var.ami_id_amazonlinuxbase20200806}"

  vpc_security_group_ids = [
    "${aws_security_group.SG_GUIDEWIRE_DEV.id}",
    "${aws_security_group.SG_JENKINS_GUIDEWIRE_HEALTH_CHECK.id}",
    "${aws_security_group.SG_AVIATRIX_SSH.id}",
    "${aws_security_group.SG_JENKINS_CLIENT_SSH.id}",
    "${aws_security_group.SG_MSSQL_DB.id}",
  ]

  subnet_id                   = "${var.portage-dev-private-1a-subnet}"
  instance_type               = "r5a.xlarge"
  key_name                    = "PortageGuidewireDev"
  associate_public_ip_address = false
  iam_instance_profile        = "CMS-SSMInstanceRole"
  ebs_optimized               = "true"

  root_block_device {
    volume_size = "35"
  }

  tags {
    Owner              = "robhatnagar@deloitte.ca"
    ManagedBy          = "CMS-TF"
    SolutionName       = "Insurcloud Portage"
    Environment        = "Dev"
    ApplicationName    = "guidewire, mssql, docker"
    DataClassification = "PII, PHI"
    DLM-Backup         = "Yes"
    DLM-Retention      = "90"
    PatchGroup         = "AmazonLinux2"
    VendorManaged      = "False"
    ResourceType       = "Client"
    ResourceSeverity   = "Normal"
    Name               = "A-MC-CAC-D-A005-PORTAGE-DEV01-GW-SUITE-LNX-EC2"
    AccountID          = "${data.aws_caller_identity.current.account_id}"
    Role               = "Container"
    CLIENT             = "Portage"
  }

  provisioner "local-exec" {
    command = "echo ${aws_instance.ICPORTAGE_DEV01_GUIDEWIRE_SUITE.private_ip} >> dev_hosts.txt"
  }
}

output "ICPORTAGE_DEV01_GUIDEWIRE_SUITE_PRIVATEIP" {
  value = "${aws_instance.ICPORTAGE_DEV01_GUIDEWIRE_SUITE.private_ip}"
}

######### MICROSERVICES ###########
resource "aws_instance" "ICPORTAGE_DEV01_MICROSERVICES" {
  ami = "${var.ami_id_amazonlinuxbase20200806}"

  vpc_security_group_ids = [
    "${aws_security_group.SG_MICROSERVICES_DEV.id}",
    "${aws_security_group.SG_MONITORING.id}",
    "${aws_security_group.SG_AVIATRIX_SSH.id}",
    "${aws_security_group.SG_JENKINS_CLIENT_SSH.id}",
    "${aws_security_group.SG_MYSQL_DB.id}",
  ]

  subnet_id                   = "${var.portage-dev-private-1a-subnet}"
  instance_type               = "r5a.xlarge"
  key_name                    = "PortageMicroservicesDev"
  associate_public_ip_address = false
  iam_instance_profile        = "CMS-SSMInstanceRole"
  ebs_optimized               = "true"

  root_block_device {
    volume_size = "50"
  }

  tags {
    Owner              = "robhatnagar@deloitte.ca"
    ManagedBy          = "CMS-TF"
    SolutionName       = "Insurcloud Portage"
    Environment        = "Dev"
    ApplicationName    = "docker, microservices, mysql"
    DataClassification = "PII, PHI"
    DLM-Backup         = "Yes"
    DLM-Retention      = "90"
    PatchGroup         = "AmazonLinux2"
    VendorManaged      = "False"
    ResourceType       = "Client"
    ResourceSeverity   = "Normal"
    Name               = "A-MC-CAC-D-A005-PORTAGE-DEV01-MICROSERVICES-LNX-EC2"
    AccountID          = "${data.aws_caller_identity.current.account_id}"
    Role               = "Container"
    CLIENT             = "Portage"
  }

  provisioner "local-exec" {
    command = "echo ${aws_instance.ICPORTAGE_DEV01_MICROSERVICES.private_ip} >> dev_hosts.txt"
  }
}

output "ICPORTAGE_DEV01_MICROSERVICES_PRIVATEIP" {
  value = "${aws_instance.ICPORTAGE_DEV01_MICROSERVICES.private_ip}"
}

######### HAPROXY ##############
resource "aws_instance" "ICPORTAGE_DEV01_INTERNAL_HAPROXY" {
  ami = "${var.ami_id_amazonlinux2base}"

  vpc_security_group_ids = [
    "${aws_security_group.SG_INTERNAL_HAPROXY_DEV.id}",
    "${aws_security_group.SG_AVIATRIX_SSH.id}",
    "${aws_security_group.SG_JENKINS_CLIENT_SSH.id}",
    "${aws_security_group.SG_CLIENT_PING_TEST.id}",
  ]

  subnet_id                   = "${var.portage-dev-private-1a-subnet}"
  instance_type               = "t3a.small"
  key_name                    = "PortageHAProxyDev"
  associate_public_ip_address = false
  iam_instance_profile        = "CMS-SSMInstanceRole"
  ebs_optimized               = "true"

  root_block_device {
    volume_size = "20"
  }

  tags {
    Owner              = "robhatnagar@deloitte.ca"
    ManagedBy          = "CMS-TF"
    SolutionName       = "Insurcloud Portage"
    Environment        = "Dev"
    ApplicationName    = "docker, haproxy"
    DataClassification = "PII, PHI"
    DLM-Backup         = "Yes"
    DLM-Retention      = "90"
    PatchGroup         = "AmazonLinux2"
    VendorManaged      = "False"
    ResourceType       = "Client"
    ResourceSeverity   = "Normal"
    Name               = "A-MC-CAC-D-A005-PORTAGE-DEV01-INTERNAL-HAPROXY-EC2"
    AccountID          = "${data.aws_caller_identity.current.account_id}"
    Role               = "Container"
    CLIENT             = "Portage"
  }

  provisioner "local-exec" {
    command = "echo ${aws_instance.ICPORTAGE_DEV01_INTERNAL_HAPROXY.private_ip} >> dev_hosts.txt"
  }
}

output "ICPORTAGE_DEV01_INTERNAL_HAPROXY_PRIVATEIP" {
  value = "${aws_instance.ICPORTAGE_DEV01_INTERNAL_HAPROXY.private_ip}"
}

######################################

# #=======================================================================
# # DEV02
# #=======================================================================

######### GUIDEWIRE ##############
resource "aws_instance" "ICPORTAGE_DEV02_GUIDEWIRE_SUITE" {
  ami = "${var.ami_id_amazonlinuxbase20200806}"

  vpc_security_group_ids = [
    "${aws_security_group.SG_GUIDEWIRE_DEV.id}",
    "${aws_security_group.SG_JENKINS_GUIDEWIRE_HEALTH_CHECK.id}",
    "${aws_security_group.SG_AVIATRIX_SSH.id}",
    "${aws_security_group.SG_JENKINS_CLIENT_SSH.id}",
    "${aws_security_group.SG_MSSQL_DB.id}",
  ]

  subnet_id                   = "${var.portage-dev-private-1a-subnet}"
  instance_type               = "r5a.xlarge"
  key_name                    = "PortageGuidewireDev"
  associate_public_ip_address = false
  iam_instance_profile        = "CMS-SSMInstanceRole"
  ebs_optimized               = "true"

  root_block_device {
    volume_size = "35"
  }

  tags {
    Owner              = "robhatnagar@deloitte.ca"
    ManagedBy          = "CMS-TF"
    SolutionName       = "Insurcloud Portage"
    Environment        = "Dev"
    ApplicationName    = "guidewire, mssql, docker"
    DataClassification = "PII, PHI"
    DLM-Backup         = "Yes"
    DLM-Retention      = "90"
    PatchGroup         = "AmazonLinux2"
    VendorManaged      = "False"
    ResourceType       = "Client"
    ResourceSeverity   = "Normal"
    Name               = "A-MC-CAC-D-A005-PORTAGE-DEV02-GW-SUITE-LNX-EC2"
    AccountID          = "${data.aws_caller_identity.current.account_id}"
    Role               = "Container"
    CLIENT             = "Portage"
  }

  provisioner "local-exec" {
    command = "echo ${aws_instance.ICPORTAGE_DEV02_GUIDEWIRE_SUITE.private_ip} >> dev_hosts.txt"
  }
}

output "ICPORTAGE_DEV02_GUIDEWIRE_SUITE_PRIVATEIP" {
  value = "${aws_instance.ICPORTAGE_DEV02_GUIDEWIRE_SUITE.private_ip}"
}

######### MICROSERVICES ###########
resource "aws_instance" "ICPORTAGE_DEV02_MICROSERVICES" {
  ami = "${var.ami_id_amazonlinuxbase20200806}"

  vpc_security_group_ids = [
    "${aws_security_group.SG_MICROSERVICES_DEV.id}",
    "${aws_security_group.SG_MONITORING.id}",
    "${aws_security_group.SG_AVIATRIX_SSH.id}",
    "${aws_security_group.SG_JENKINS_CLIENT_SSH.id}",
    "${aws_security_group.SG_MYSQL_DB.id}",
  ]

  subnet_id                   = "${var.portage-dev-private-1a-subnet}"
  instance_type               = "r5a.xlarge"
  key_name                    = "PortageMicroservicesDev"
  associate_public_ip_address = false
  iam_instance_profile        = "CMS-SSMInstanceRole"
  ebs_optimized               = "true"

  root_block_device {
    volume_size = "50"
  }

  tags {
    Owner              = "robhatnagar@deloitte.ca"
    ManagedBy          = "CMS-TF"
    SolutionName       = "Insurcloud Portage"
    Environment        = "Dev"
    ApplicationName    = "docker, microservices, mysql"
    DataClassification = "PII, PHI"
    DLM-Backup         = "Yes"
    DLM-Retention      = "90"
    PatchGroup         = "AmazonLinux2"
    VendorManaged      = "False"
    ResourceType       = "Client"
    ResourceSeverity   = "Normal"
    Name               = "A-MC-CAC-D-A005-PORTAGE-DEV02-MICROSERVICES-LNX-EC2"
    AccountID          = "${data.aws_caller_identity.current.account_id}"
    Role               = "Container"
    CLIENT             = "Portage"
  }

  provisioner "local-exec" {
    command = "echo ${aws_instance.ICPORTAGE_DEV02_MICROSERVICES.private_ip} >> dev_hosts.txt"
  }
}

output "ICPORTAGE_DEV02_MICROSERVICES_PRIVATEIP" {
  value = "${aws_instance.ICPORTAGE_DEV02_MICROSERVICES.private_ip}"
}

# #=======================================================================
# # DEV03
# #=======================================================================

######### GUIDEWIRE ##############
resource "aws_instance" "ICPORTAGE_DEV03_GUIDEWIRE_SUITE" {
  ami = "${var.ami_id_amazonlinuxbase20200806}"

  vpc_security_group_ids = [
    "${aws_security_group.SG_GUIDEWIRE_DEV.id}",
    "${aws_security_group.SG_JENKINS_GUIDEWIRE_HEALTH_CHECK.id}",
    "${aws_security_group.SG_AVIATRIX_SSH.id}",
    "${aws_security_group.SG_JENKINS_CLIENT_SSH.id}",
    "${aws_security_group.SG_MSSQL_DB.id}",
  ]

  subnet_id                   = "${var.portage-dev-private-1a-subnet}"
  instance_type               = "r5a.xlarge"
  key_name                    = "PortageGuidewireDev"
  associate_public_ip_address = false
  iam_instance_profile        = "CMS-SSMInstanceRole"
  ebs_optimized               = "true"

  root_block_device {
    volume_size = "35"
  }

  tags {
    Owner              = "robhatnagar@deloitte.ca"
    ManagedBy          = "CMS-TF"
    SolutionName       = "Insurcloud Portage"
    Environment        = "Dev"
    ApplicationName    = "guidewire, mssql, docker"
    DataClassification = "PII, PHI"
    DLM-Backup         = "Yes"
    DLM-Retention      = "90"
    PatchGroup         = "AmazonLinux2"
    VendorManaged      = "False"
    ResourceType       = "Client"
    ResourceSeverity   = "Normal"
    Name               = "A-MC-CAC-D-A005-PORTAGE-DEV03-GW-SUITE-LNX-EC2"
    AccountID          = "${data.aws_caller_identity.current.account_id}"
    Role               = "Container"
    CLIENT             = "Portage"
    Schedule           = "portage-weekend-hours"
  }

  provisioner "local-exec" {
    command = "echo ${aws_instance.ICPORTAGE_DEV03_GUIDEWIRE_SUITE.private_ip} >> dev_hosts.txt"
  }
}

output "ICPORTAGE_DEV03_GUIDEWIRE_SUITE_PRIVATEIP" {
  value = "${aws_instance.ICPORTAGE_DEV03_GUIDEWIRE_SUITE.private_ip}"
}


######################################

# #=======================================================================
# # QAQ01
# #=======================================================================

######### GUIDEWIRE ##############
resource "aws_instance" "ICPORTAGE_QA01_GUIDEWIRE_SUITE" {
  ami = "${var.ami_id_amazonlinuxbase20200806}"

  vpc_security_group_ids = [
    "${aws_security_group.SG_GUIDEWIRE_DEV.id}",
    "${aws_security_group.SG_JENKINS_GUIDEWIRE_HEALTH_CHECK.id}",
    "${aws_security_group.SG_AVIATRIX_SSH.id}",
    "${aws_security_group.SG_JENKINS_CLIENT_SSH.id}",
    "${aws_security_group.SG_MSSQL_DB.id}",
  ]

  subnet_id                   = "${var.portage-dev-private-1a-subnet}"
  instance_type               = "r5a.xlarge"
  key_name                    = "PortageGuidewireDev"
  associate_public_ip_address = false
  iam_instance_profile        = "CMS-SSMInstanceRole"
  ebs_optimized               = "true"

  root_block_device {
    volume_size = "35"
  }

  tags {
    Owner              = "robhatnagar@deloitte.ca"
    ManagedBy          = "CMS-TF"
    SolutionName       = "Insurcloud Portage"
    Environment        = "Dev"
    ApplicationName    = "guidewire, mssql, docker"
    DataClassification = "PII, PHI"
    DLM-Backup         = "Yes"
    DLM-Retention      = "90"
    PatchGroup         = "AmazonLinux2"
    VendorManaged      = "False"
    ResourceType       = "Client"
    ResourceSeverity   = "Normal"
    Name               = "A-MC-CAC-D-A005-PORTAGE-QA01-GW-SUITE-LNX-EC2"
    AccountID          = "${data.aws_caller_identity.current.account_id}"
    Role               = "Container"
    CLIENT             = "Portage"
  }

  provisioner "local-exec" {
    command = "echo ${aws_instance.ICPORTAGE_QA01_GUIDEWIRE_SUITE.private_ip} >> dev_hosts.txt"
  }
}

output "ICPORTAGE_QA01_GUIDEWIRE_SUITE_PRIVATEIP" {
  value = "${aws_instance.ICPORTAGE_QA01_GUIDEWIRE_SUITE.private_ip}"
}

######### MICROSERVICES ###########
resource "aws_instance" "ICPORTAGE_QA01_MICROSERVICES" {
  ami = "${var.ami_id_amazonlinuxbase20200806}"

  vpc_security_group_ids = [
    "${aws_security_group.SG_MICROSERVICES_DEV.id}",
    "${aws_security_group.SG_MONITORING.id}",
    "${aws_security_group.SG_AVIATRIX_SSH.id}",
    "${aws_security_group.SG_JENKINS_CLIENT_SSH.id}",
    "${aws_security_group.SG_MYSQL_DB.id}",
  ]

  subnet_id                   = "${var.portage-dev-private-1a-subnet}"
  instance_type               = "r5a.xlarge"
  key_name                    = "PortageMicroservicesDev"
  associate_public_ip_address = false
  iam_instance_profile        = "CMS-SSMInstanceRole"
  ebs_optimized               = "true"

  root_block_device {
    volume_size = "50"
  }

  tags {
    Owner              = "robhatnagar@deloitte.ca"
    ManagedBy          = "CMS-TF"
    SolutionName       = "Insurcloud Portage"
    Environment        = "Dev"
    ApplicationName    = "docker, microservices, mysql"
    DataClassification = "PII, PHI"
    DLM-Backup         = "Yes"
    DLM-Retention      = "90"
    PatchGroup         = "AmazonLinux2"
    VendorManaged      = "False"
    ResourceType       = "Client"
    ResourceSeverity   = "Normal"
    Name               = "A-MC-CAC-D-A005-PORTAGE-QA01-MICROSERVICES-LNX-EC2"
    AccountID          = "${data.aws_caller_identity.current.account_id}"
    Role               = "Container"
    CLIENT             = "Portage"
  }

  provisioner "local-exec" {
    command = "echo ${aws_instance.ICPORTAGE_QA01_MICROSERVICES.private_ip} >> dev_hosts.txt"
  }
}

output "ICPORTAGE_QA01_MICROSERVICES_PRIVATEIP" {
  value = "${aws_instance.ICPORTAGE_QA01_MICROSERVICES.private_ip}"
}



######################################

# #=======================================================================
# # CNV01
# #=======================================================================

######### GUIDEWIRE ##############
resource "aws_instance" "ICPORTAGE_CNV01_GUIDEWIRE_SUITE" {
  ami = "${var.ami_id_amazonlinuxbase20200806}"

  vpc_security_group_ids = [
    "${aws_security_group.SG_GUIDEWIRE_DEV.id}",
    "${aws_security_group.SG_JENKINS_GUIDEWIRE_HEALTH_CHECK.id}",
    "${aws_security_group.SG_AVIATRIX_SSH.id}",
    "${aws_security_group.SG_JENKINS_CLIENT_SSH.id}",
    "${aws_security_group.SG_MSSQL_DB.id}",
  ]

  subnet_id                   = "${var.portage-dev-private-1a-subnet}"
  instance_type               = "r5a.xlarge"
  key_name                    = "PortageGuidewireDev"
  associate_public_ip_address = false
  iam_instance_profile        = "CMS-SSMInstanceRole"
  ebs_optimized               = "true"

  root_block_device {
    volume_size = "35"
  }

  tags {
    Owner              = "robhatnagar@deloitte.ca"
    ManagedBy          = "CMS-TF"
    SolutionName       = "Insurcloud Portage"
    Environment        = "Dev"
    ApplicationName    = "guidewire, mssql, docker"
    DataClassification = "PII, PHI"
    DLM-Backup         = "Yes"
    DLM-Retention      = "90"
    PatchGroup         = "AmazonLinux2"
    VendorManaged      = "False"
    ResourceType       = "Client"
    ResourceSeverity   = "Normal"
    Name               = "A-MC-CAC-D-A005-PORTAGE-CNV01-GW-SUITE-LNX-EC2"
    AccountID          = "${data.aws_caller_identity.current.account_id}"
    Role               = "Container"
    CLIENT             = "Portage"
  }

  provisioner "local-exec" {
    command = "echo ${aws_instance.ICPORTAGE_CNV01_GUIDEWIRE_SUITE.private_ip} >> dev_hosts.txt"
  }
}

output "ICPORTAGE_CNV01_GUIDEWIRE_SUITE_PRIVATEIP" {
  value = "${aws_instance.ICPORTAGE_CNV01_GUIDEWIRE_SUITE.private_ip}"
}

######### MICROSERVICES ###########
resource "aws_instance" "ICPORTAGE_CNV01_MICROSERVICES" {
  ami = "${var.ami_id_amazonlinuxbase20200806}"

  vpc_security_group_ids = [
    "${aws_security_group.SG_MICROSERVICES_DEV.id}",
    "${aws_security_group.SG_MONITORING.id}",
    "${aws_security_group.SG_AVIATRIX_SSH.id}",
    "${aws_security_group.SG_JENKINS_CLIENT_SSH.id}",
    "${aws_security_group.SG_MYSQL_DB.id}",
  ]

  subnet_id                   = "${var.portage-dev-private-1a-subnet}"
  instance_type               = "r5a.xlarge"
  key_name                    = "PortageMicroservicesDev"
  associate_public_ip_address = false
  iam_instance_profile        = "CMS-SSMInstanceRole"
  ebs_optimized               = "true"

  root_block_device {
    volume_size = "75"
  }

  tags {
    Owner              = "robhatnagar@deloitte.ca"
    ManagedBy          = "CMS-TF"
    SolutionName       = "Insurcloud Portage"
    Environment        = "Dev"
    ApplicationName    = "docker, microservices, mysql"
    DataClassification = "PII, PHI"
    DLM-Backup         = "Yes"
    DLM-Retention      = "90"
    PatchGroup         = "AmazonLinux2"
    VendorManaged      = "False"
    ResourceType       = "Client"
    ResourceSeverity   = "Normal"
    Name               = "A-MC-CAC-D-A005-PORTAGE-CNV01-MICROSERVICES-LNX-EC2"
    AccountID          = "${data.aws_caller_identity.current.account_id}"
    Role               = "Container"
    CLIENT             = "Portage"
  }

  provisioner "local-exec" {
    command = "echo ${aws_instance.ICPORTAGE_CNV01_MICROSERVICES.private_ip} >> dev_hosts.txt"
  }
}

output "ICPORTAGE_CNV01_MICROSERVICES_PRIVATEIP" {
  value = "${aws_instance.ICPORTAGE_CNV01_MICROSERVICES.private_ip}"
}

######### DHIC SERVER ###########

data "template_file" "PORTAGE_DOMAIN_JOIN_4" {
  template = "${file("IC-Portage-DEV/windowsdata.tpl")}"

  vars {
    computer_name  = "CNV01DHIC"
    region         = "${var.region}"
    domain         = "${var.ad_domain}"
    dns1           = "${var.ad_dns1}"
    dns2           = "${var.ad_dns2}"
    dns3           = "${var.ad_dns3}"
    ouselection    = "${var.ou_name}"
    pwordparameter = "${var.pwordparameter}"
    unameparameter = "${var.unameparameter}"
  }
}

resource "aws_instance" "ICPORTAGE_CNV01_DHIC_SERVER" {
  ami = "${var.ami_id_windowsdhicsap}"

  vpc_security_group_ids = [
    "${aws_security_group.SG_DHIC.id}",
    "${aws_security_group.SG_AVIATRIX_RDP.id}",
    "${aws_security_group.SG_WORKSPACES.id}"
  ]

  subnet_id                   = "${var.portage-dev-private-1a-subnet}"
  instance_type               = "t3a.xlarge"
  key_name                    = "PortageDataDev"
  associate_public_ip_address = false
  iam_instance_profile        = "CMS-SSMInstanceRole"
  ebs_optimized               = "true"
  lifecycle {
    prevent_destroy = false
  }
  root_block_device {
    volume_size = "100"
  }
  user_data = "${data.template_file.PORTAGE_DOMAIN_JOIN_4.rendered}"
  tags {
    Owner              = "robhatnagar@deloitte.ca"
    ManagedBy          = "CMS-TF"
    SolutionName       = "Insurcloud Portage"
    Environment        = "Dev"
    ApplicationName    = "sap"
    DataClassification = "PII, PHI"
    DLM-Backup         = "Yes"
    DLM-Retention      = "90"
    PatchGroup         = "Windows"
    VendorManaged      = "False"
    OSName             = "Windows"
    ResourceType       = "Client"
    ResourceSeverity   = "Normal"
    Name               = "A-MC-CAC-D-A005-PORTAGE-CNV01-DHIC-SERVER-WIN-EC2"
    AccountID          = "${data.aws_caller_identity.current.account_id}"
    Role               = "Container"
    CLIENT             = "Portage"
  }
  provisioner "local-exec" {
    command = "echo ${aws_instance.ICPORTAGE_CNV01_DHIC_SERVER.private_ip} >> dev_hosts.txt"
  }
}

output "ICPORTAGE_CNV01_DHIC_SERVER_PRIVATEIP" {
  value = "${aws_instance.ICPORTAGE_CNV01_DHIC_SERVER.private_ip}"
}

######### DHIC DATABASE SERVER ###########

data "template_file" "PORTAGE_DOMAIN_JOIN_5" {
  template = "${file("IC-Portage-DEV/windowsdata.tpl")}"

  vars {
    computer_name  = "CNV01DHIC-DB"
    region         = "${var.region}"
    domain         = "${var.ad_domain}"
    dns1           = "${var.ad_dns1}"
    dns2           = "${var.ad_dns2}"
    dns3           = "${var.ad_dns3}"
    ouselection    = "${var.ou_name}"
    pwordparameter = "${var.pwordparameter}"
    unameparameter = "${var.unameparameter}"
  }
}

resource "aws_instance" "ICPORTAGE_CNV01_DHIC_MSSQL_DATABASE" {
  ami = "${var.ami_id_windowsdhicdb}"

  vpc_security_group_ids = [
    "${aws_security_group.SG_MSSQL_DB.id}",
    "${aws_security_group.SG_PORTAGE_MSSQL_DB.id}",
    "${aws_security_group.SG_AVIATRIX_RDP.id}",
  ]

  subnet_id                   = "${var.portage-dev-database-1a-subnet}"
  instance_type               = "r5a.large"
  key_name                    = "PortageDatabaseDev"
  associate_public_ip_address = false
  iam_instance_profile        = "CMS-SSMInstanceRole"
  ebs_optimized               = "true"

  lifecycle {
    prevent_destroy = false
  }

  root_block_device {
    volume_size = "256"
  }

  user_data = "${data.template_file.PORTAGE_DOMAIN_JOIN_5.rendered}"

  tags {
    Owner              = "robhatnagar@deloitte.ca"
    ManagedBy          = "CMS-TF"
    SolutionName       = "Insurcloud Portage"
    Environment        = "Dev"
    ApplicationName    = "database, mssql, sap, ssis"
    DataClassification = "PII, PHI"
    DLM-Backup         = "Yes"
    DLM-Retention      = "90"
    PatchGroup         = "Windows"
    VendorManaged      = "False"
    OSName             = "Windows"
    ResourceType       = "Client"
    ResourceSeverity   = "Normal"
    Name               = "A-MC-CAC-D-A005-PORTAGE-CNV01-DHIC-DATABASE-MSSQL-WIN-EC2"
    AccountID          = "${data.aws_caller_identity.current.account_id}"
    Role               = "Container"
    CLIENT             = "Portage"
  }

  provisioner "local-exec" {
    command = "echo ${aws_instance.ICPORTAGE_CNV01_DHIC_MSSQL_DATABASE.private_ip} >> dev_hosts.txt"
  }
}

output "ICPORTAGE_CNV01_DHIC_MSSQL_DATABASE_PRIVATEIP" {
  value = "${aws_instance.ICPORTAGE_CNV01_DHIC_MSSQL_DATABASE.private_ip}"
}

# ------------------ Automation Framework Server ------------------ #
resource "aws_instance" "ICPORTAGE_AUTOMATION_FRAMEWORK_SERVER" {
  ami = "${var.ami_id_amazonlinuxbase20200806}"

  vpc_security_group_ids = [
    "${aws_security_group.SG_AUTOMATION_FRAMEWORK_1.id}",
    "${aws_security_group.SG_AUTOMATION_FRAMEWORK_2.id}",
  ]

  subnet_id                   = "${var.portage-dev-private-1a-subnet}"
  instance_type               = "r5a.xlarge"
  key_name                    = "PortageAutomationFrameworkServerKey"
  associate_public_ip_address = false
  iam_instance_profile        = "CMS-SSMInstanceRole"
  ebs_optimized               = "true"

  lifecycle {
    prevent_destroy = false
  }

  root_block_device {
    volume_size = "100"
  }

  tags {
    Owner              = "robhatnagar@deloitte.ca"
    ManagedBy          = "CMS-TF"
    SolutionName       = "Insurcloud Portage"
    Environment        = "Dev"
    ApplicationName    = "automation_framework, docker"
    DataClassification = "NA"
    DLM-Backup         = "Yes"
    DLM-Retention      = "90"
    PatchGroup         = "AmazonLinux2"
    VendorManaged      = "False"
    ResourceType       = "Client"
    ResourceSeverity   = "High"
    Name               = "A-MC-CAC-D-A005-PORTAGE-AUTOMATION-FRAMEWORK-EC2"
    AccountID          = "${data.aws_caller_identity.current.account_id}"
    Role               = "Container"
    CLIENT             = "Portage"
  }
}

output "ICPORTAGE_AUTOMATION_FRAMEWORK_SERVER_PRIVATEIP" {
  value = "${aws_instance.ICPORTAGE_AUTOMATION_FRAMEWORK_SERVER.private_ip}"
}

######### DATA QA01 ##############

######### DHIC DATABASE SERVER ###########

data "template_file" "PORTAGE_DOMAIN_JOIN_7" {
  template = "${file("IC-Portage-DEV/windowsdata.tpl")}"

  vars {
    computer_name  = "QA01DHIC-DB"
    region         = "${var.region}"
    domain         = "${var.ad_domain}"
    dns1           = "${var.ad_dns1}"
    dns2           = "${var.ad_dns2}"
    dns3           = "${var.ad_dns3}"
    ouselection    = "${var.ou_name}"
    pwordparameter = "${var.pwordparameter}"
    unameparameter = "${var.unameparameter}"
  }
}

resource "aws_instance" "ICPORTAGE_QA01_DHIC_MSSQL_DATABASE" {
  ami = "${var.ami_id_windows2016base20201216_mssql_win_ec2}"

  vpc_security_group_ids = [
    "${aws_security_group.SG_MSSQL_DB.id}",
    "${aws_security_group.SG_PORTAGE_MSSQL_DB.id}",
    "${aws_security_group.SG_AVIATRIX_RDP.id}",
  ]

  subnet_id                   = "${var.portage-dev-database-1a-subnet}"
  instance_type               = "r5a.large"
  key_name                    = "PortageDatabaseDev"
  associate_public_ip_address = false
  iam_instance_profile        = "CMS-SSMInstanceRole"
  ebs_optimized               = "true"

  lifecycle {
    prevent_destroy = false
  }

  root_block_device {
    volume_size = "256"
  }

  user_data = "${data.template_file.PORTAGE_DOMAIN_JOIN_7.rendered}"

  tags {
    Owner              = "robhatnagar@deloitte.ca"
    ManagedBy          = "CMS-TF"
    SolutionName       = "Insurcloud Portage"
    Environment        = "Dev"
    ApplicationName    = "database, mssql, sap, ssis"
    DataClassification = "PII, PHI"
    DLM-Backup         = "Yes"
    DLM-Retention      = "90"
    PatchGroup         = "Windows"
    VendorManaged      = "False"
    OSName             = "Windows"
    ResourceType       = "Client"
    ResourceSeverity   = "Normal"
    Name               = "A-MC-CAC-D-A005-PORTAGE-QA01-DHIC-DATABASE-MSSQL-WIN-EC2"
    AccountID          = "${data.aws_caller_identity.current.account_id}"
    Role               = "Container"
    CLIENT             = "Portage"
  }

  provisioner "local-exec" {
    command = "echo ${aws_instance.ICPORTAGE_QA01_DHIC_MSSQL_DATABASE.private_ip} >> dev_hosts.txt"
  }
}

output "ICPORTAGE_QA01_DHIC_MSSQL_DATABASE_PRIVATEIP" {
  value = "${aws_instance.ICPORTAGE_QA01_DHIC_MSSQL_DATABASE.private_ip}"
}

######### DHIC SERVER ###########

data "template_file" "PORTAGE_DOMAIN_JOIN_8" {
  template = "${file("IC-Portage-DEV/windowsdata.tpl")}"

  vars {
    computer_name  = "QA01DHIC"
    region         = "${var.region}"
    domain         = "${var.ad_domain}"
    dns1           = "${var.ad_dns1}"
    dns2           = "${var.ad_dns2}"
    dns3           = "${var.ad_dns3}"
    ouselection    = "${var.ou_name}"
    pwordparameter = "${var.pwordparameter}"
    unameparameter = "${var.unameparameter}"
  }
}

resource "aws_instance" "ICPORTAGE_QA01_DHIC_SERVER" {
  ami = "${var.ami_id_windows2016base20201216_win_ec2}"

  vpc_security_group_ids = [
    "${aws_security_group.SG_DHIC.id}",
    "${aws_security_group.SG_AVIATRIX_RDP.id}",
    "${aws_security_group.SG_WORKSPACES.id}"
  ]

  subnet_id                   = "${var.portage-dev-private-1a-subnet}"
  instance_type               = "t3a.xlarge"
  key_name                    = "PortageDataDev"
  associate_public_ip_address = false
  iam_instance_profile        = "CMS-SSMInstanceRole"
  ebs_optimized               = "true"
  lifecycle {
    prevent_destroy = false
  }

  root_block_device {
    volume_size = "100"
  }

  user_data = "${data.template_file.PORTAGE_DOMAIN_JOIN_8.rendered}"

  tags {
    Owner              = "robhatnagar@deloitte.ca"
    ManagedBy          = "CMS-TF"
    SolutionName       = "Insurcloud Portage"
    Environment        = "Dev"
    ApplicationName    = "sap"
    DataClassification = "PII, PHI"
    DLM-Backup         = "Yes"
    DLM-Retention      = "90"
    PatchGroup         = "Windows"
    VendorManaged      = "False"
    OSName             = "Windows"
    ResourceType       = "Client"
    ResourceSeverity   = "Normal"
    Name               = "A-MC-CAC-D-A005-PORTAGE-QA01-DHIC-SERVER-WIN-EC2"
    AccountID          = "${data.aws_caller_identity.current.account_id}"
    Role               = "Container"
    CLIENT             = "Portage"
  }
  provisioner "local-exec" {
    command = "echo ${aws_instance.ICPORTAGE_QA01_DHIC_SERVER.private_ip} >> dev_hosts.txt"
  }
}

output "ICPORTAGE_QA01_DHIC_SERVER_PRIVATEIP" {
  value = "${aws_instance.ICPORTAGE_QA01_DHIC_SERVER.private_ip}"
}

######### DATA QA01 ##############
