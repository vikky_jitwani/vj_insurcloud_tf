resource "aws_s3_bucket" "ICGORE_DEV_HAPROXY" {
  bucket = "gore-dev-haproxy-deployment"
  acl = "private"

  lifecycle_rule {
    enabled = true

    transition {
      days = 30
      storage_class = "STANDARD_IA"
    }
  }

server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm     = "aws:kms"
        kms_master_key_id = "arn:aws:kms:ca-central-1:${var.gore-dev-account}:key/e02c870d-c484-4ff1-aaa0-01bd5945f0f8"
      }
    }
  }
  
  tags {
    Name = "gore-dev-haproxy-deployment"
  }
}

resource "aws_s3_bucket_policy" "ICGORE_DEV_HAPROXY_BUCKET_POLICY" {
  bucket = "${aws_s3_bucket.ICGORE_DEV_HAPROXY.id}"

  policy = <<POLICY
{
    "Version": "2012-10-17",
    "Id": "ICGORE_DEV_HAPROXY_BUCKET_POLICY",
    "Statement": [
        {
            "Sid": "DenyPublicReadACL",
            "Effect": "Deny",
            "Principal": {
                "AWS": "*"
            },
            "Action": [
                "s3:PutObject",
                "s3:PutObjectAcl"
            ],
            "Resource": "arn:aws:s3:::${aws_s3_bucket.ICGORE_DEV_HAPROXY.id}/*",
            "Condition": {
                "StringEquals": {
                    "s3:x-amz-acl": [
                        "public-read",
                        "public-read-write",
                        "authenticated-read"
                    ]
                }
            }
        },
        {
            "Sid": "DenyPublicReadGrant",
            "Effect": "Deny",
            "Principal": {
                "AWS": "*"
            },
            "Action": [
                "s3:PutObject",
                "s3:PutObjectAcl"
            ],
            "Resource": "arn:aws:s3:::${aws_s3_bucket.ICGORE_DEV_HAPROXY.id}/*",
            "Condition": {
                "StringLike": {
                    "s3:x-amz-grant-read": [
                        "*http://acs.amazonaws.com/groups/global/AllUsers*",
                        "*http://acs.amazonaws.com/groups/global/AuthenticatedUsers*"
                    ]
                }
            }
        },
        {
            "Sid": "DenyPublicListACL",
            "Effect": "Deny",
            "Principal": {
                "AWS": "*"
            },
            "Action": "s3:PutBucketAcl",
            "Resource": "arn:aws:s3:::${aws_s3_bucket.ICGORE_DEV_HAPROXY.id}",
            "Condition": {
                "StringEquals": {
                    "s3:x-amz-acl": [
                        "public-read",
                        "public-read-write",
                        "authenticated-read"
                    ]
                }
            }
        },
        {
            "Sid": "DenyPublicListGrant",
            "Effect": "Deny",
            "Principal": {
                "AWS": "*"
            },
            "Action": "s3:PutBucketAcl",
            "Resource": "arn:aws:s3:::${aws_s3_bucket.ICGORE_DEV_HAPROXY.id}",
            "Condition": {
                "StringLike": {
                    "s3:x-amz-grant-read": [
                        "*http://acs.amazonaws.com/groups/global/AllUsers*",
                        "*http://acs.amazonaws.com/groups/global/AuthenticatedUsers*"
                    ]
                }
            }
        },
        {
            "Sid": "DenyPublicReadConsole",
            "Effect": "Deny",
            "Principal": {
                "AWS": "*"
            },
            "Action": [
                "s3:PutObjectAcl",
                "s3:PutBucketAcl"
            ],
            "Resource": [
                "arn:aws:s3:::${aws_s3_bucket.ICGORE_DEV_HAPROXY.id}/*",
                "arn:aws:s3:::${aws_s3_bucket.ICGORE_DEV_HAPROXY.id}"
            ],
            "Condition": {
                "Null": {
                    "s3:x-amz-grant-write": "true",
                    "s3:x-amz-grant-read": "true",
                    "s3:x-amz-acl": "true",
                    "s3:x-amz-grant-read-acp": "true",
                    "s3:x-amz-grant-full-control": "true",
                    "s3:x-amz-grant-write-acp": "true"
                }
            }
        },
        {
            "Sid": "DevInstanceListBucketAllow",
            "Effect": "Allow",
            "Principal": {
                "AWS": "arn:aws:iam::${var.gore-dev-account}:role/CMS-SSMRole"
            },
            "Action": "s3:ListBucket",
            "Resource": "arn:aws:s3:::${aws_s3_bucket.ICGORE_DEV_HAPROXY.id}"
        },
        {
            "Sid": "DevInstanceObjectAllow",
            "Effect": "Allow",
            "Principal": {
                "AWS": "arn:aws:iam::${var.gore-dev-account}:role/CMS-SSMRole"
            },
            "Action": [
                "s3:GetObject",
                "s3:PutObject",
                "s3:DeleteObject"
            ],
            "Resource": [
                "arn:aws:s3:::${aws_s3_bucket.ICGORE_DEV_HAPROXY.id}/DEV*/*",
                "arn:aws:s3:::${aws_s3_bucket.ICGORE_DEV_HAPROXY.id}/QA*/*",
                "arn:aws:s3:::${aws_s3_bucket.ICGORE_DEV_HAPROXY.id}/TBT*/*",
                "arn:aws:s3:::${aws_s3_bucket.ICGORE_DEV_HAPROXY.id}/BAT*/*",
                "arn:aws:s3:::${aws_s3_bucket.ICGORE_DEV_HAPROXY.id}/PERF*/*"
            ]
        },
        {
            "Sid": "HubInstanceListBucketAllow",
            "Effect": "Allow",
            "Principal": {
                "AWS": "arn:aws:iam::${var.gore-hub-account}:role/CMS-SSMRole"
            },
            "Action": "s3:ListBucket",
            "Resource": "arn:aws:s3:::${aws_s3_bucket.ICGORE_DEV_HAPROXY.id}"
        },
        {
            "Sid": "HubInstanceObjectAllow",
            "Effect": "Allow",
            "Principal": {
                "AWS": "arn:aws:iam::${var.gore-hub-account}:role/CMS-SSMRole"
            },
            "Action": [
                "s3:GetObject",
                "s3:PutObject",
                "s3:DeleteObject"
            ],
            "Resource": [
                "arn:aws:s3:::${aws_s3_bucket.ICGORE_DEV_HAPROXY.id}/DEV*/*",
                "arn:aws:s3:::${aws_s3_bucket.ICGORE_DEV_HAPROXY.id}/QA*/*",
                "arn:aws:s3:::${aws_s3_bucket.ICGORE_DEV_HAPROXY.id}/TBT*/*",
                "arn:aws:s3:::${aws_s3_bucket.ICGORE_DEV_HAPROXY.id}/BAT/*",
                "arn:aws:s3:::${aws_s3_bucket.ICGORE_DEV_HAPROXY.id}/PERF/*"
            ]
        }
    ]
}
POLICY
}
