#### GUIDEWIRE SG #####
resource "aws_security_group" "SG_GUIDEWIRE" {
  name = "GUIDEWIRE-SG"

  ingress {
    from_port = 8080
    to_port   = 8083
    protocol  = "tcp"

    security_groups = ["${aws_security_group.SG_INTERNAL_HAPROXY.id}"]
    description     = "HTTP - Internal"
  }

  ingress {
    from_port = 8080
    to_port   = 8083
    protocol  = "tcp"

    security_groups = ["${aws_security_group.SG_EXTERNAL_HAPROXY.id}"]
    description     = "HTTP - External"
  }

  ingress {
    from_port = 8080
    to_port   = 8083
    protocol  = "tcp"

    cidr_blocks = ["${var.portage_aviatrix_vpn}"]
    description = "HTTP - Internal No Proxy"
  }

  egress {
    from_port = 0
    to_port   = 0
    protocol  = "-1"

    cidr_blocks = ["0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    Contract = "MANAGED_SERVICES"
    Name     = "GUIDEWIRE-SG"
  }
}

resource "aws_security_group" "SG_GUIDEWIRE_DEV" {
  name = "GUIDEWIRE-DEV-SG"

  ingress {
    from_port = 8080
    to_port   = 8083
    protocol  = "tcp"

    security_groups = ["${aws_security_group.SG_INTERNAL_HAPROXY_DEV.id}"]

    description = "HTTP - Internal"
  }

  ingress {
    from_port   = 8080
    to_port     = 8083
    protocol    = "tcp"
    cidr_blocks = ["${var.portage_corp_cidrs}", "10.171.9.81/32"]
    description = "HTTP - Portage Corp Internal No Proxy"
  }

  ingress {
    from_port = 8080
    to_port   = 8083
    protocol  = "tcp"

    security_groups = ["${aws_security_group.SG_EXTERNAL_HAPROXY_DEV.id}"]

    description = "HTTP - External"
  }

  ingress {
    from_port = 8080
    to_port   = 8083
    protocol  = "tcp"

    security_groups = ["${aws_security_group.SG_INTERNAL_HAPROXY_DEV.id}"]

    description = "HTTP - Internal"
  }

  ingress {
    from_port = 8080
    to_port   = 8083
    protocol  = "tcp"

    cidr_blocks = ["${var.portage_aviatrix_vpn}"]

    description = "HTTP - Internal No Proxy"
  }

  # Custom TCP	TCP	8080 - 8083	10.170.140.80/32	-

  ingress {
    from_port = 8080
    to_port   = 8083
    protocol  = "tcp"

    cidr_blocks = ["10.170.140.80/32"]

    description = "-"
  }
  ingress {
    from_port = 8080
    to_port   = 8083
    protocol  = "tcp"

    cidr_blocks = ["${var.portage_dev01_microservices}"]

    description = "MICROSERVICES"
  }

  ingress {
    from_port   = 1433
    to_port     = 1433
    protocol    = "tcp"
    cidr_blocks = ["10.171.9.81/32"]
    description = "HTTP - Portage Corp Internal No Proxy"
  }

  egress {
    from_port = 0
    to_port   = 0
    protocol  = "-1"

    cidr_blocks = ["0.0.0.0/0"]
  }
  vpc_id = "${var.vpc_id}"
  tags = {
    Contract = "MANAGED_SERVICES"
    Name     = "GUIDEWIRE-DEV-SG"
  }

  ingress {
    from_port   = 8080
    to_port     = 8083
    protocol    = "tcp"
    cidr_blocks = ["10.171.8.64/32"]
    description = "New rule - Carlos - MS dev02 to access gw dev03"
  }

  ingress {
    from_port   = 1433
    to_port     = 1433
    protocol    = "tcp"
    description = "From AWS Workspaces"
    cidr_blocks = "${var.portage_workspaces}"
  }

  ingress {
    from_port   = 8080
    to_port     = 8083
    protocol    = "tcp"
    description = "From AWS Workspaces"
    cidr_blocks = "${var.portage_workspaces}"
  }

  # Access from application load balancer to the GW instances
  ingress {
    from_port = 8080
    to_port   = 8083
    protocol  = "tcp"

    security_groups = ["${var.nonpro-alb-segurity-group-id}"]

    description = "Access from application load balancer to the GW instances"
  }
}

resource "aws_security_group" "SG_MICROSERVICES" {
  name = "MICROSERVICES-SG"

  ingress {
    from_port = 8443
    to_port   = 8443
    protocol  = "tcp"

    security_groups = ["${aws_security_group.SG_EXTERNAL_HAPROXY.id}"]

    description = "Orchestrator"
  }

  ingress {
    from_port = 15672
    to_port   = 15672
    protocol  = "tcp"

    cidr_blocks = ["${var.portage_aviatrix_vpn}"]

    security_groups = ["${aws_security_group.SG_INTERNAL_HAPROXY.id}"]

    description = "RabbitMQ Management UI"
  }

  ingress {
    from_port = 9002
    to_port   = 9002
    protocol  = "tcp"

    security_groups = ["${aws_security_group.SG_INTERNAL_HAPROXY.id}"]

    description = "Edge"
  }

  ingress {
    from_port = 2377
    to_port   = 2377
    protocol  = "tcp"

    cidr_blocks = ["${var.portage_dev_cidr}"]

    description = "Cluster Management Communications"
  }

  ingress {
    from_port = 7946
    to_port   = 7946
    protocol  = "udp"

    cidr_blocks = ["${var.portage_dev_cidr}"]

    description = "Communication Among Nodes"
  }

  ingress {
    from_port = 7946
    to_port   = 7946
    protocol  = "tcp"

    cidr_blocks = ["${var.portage_dev_cidr}"]

    description = "Communication Among Nodes"
  }

  ingress {
    from_port = 4789
    to_port   = 4789
    protocol  = "udp"

    cidr_blocks = ["${var.portage_dev_cidr}"]

    description = "Overlay Network Traffic"
  }

  ingress {
    from_port = 8500
    to_port   = 8500
    protocol  = "tcp"

    cidr_blocks = ["${var.portage_aviatrix_vpn}"]

    description = "Consul UI"
  }

  ingress {
    from_port = 80
    to_port   = 80
    protocol  = "tcp"

    cidr_blocks = [
      "${var.portage_dev_cidr}",
      "${var.portage_aviatrix_vpn}",
    ]

    description = "Grafana UI"
  }

  egress {
    from_port = 0
    to_port   = 0
    protocol  = "-1"

    cidr_blocks = ["0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    Contract = "MANAGED_SERVICES"
    Name     = "MICROSERVICES-SG"
  }
}

resource "aws_security_group" "SG_MICROSERVICES_DEV" {
  name = "MICROSERVICES-DEV-SG"

  ingress {
    from_port = 8443
    to_port   = 8443
    protocol  = "tcp"

    security_groups = ["${aws_security_group.SG_EXTERNAL_HAPROXY_DEV.id}", "${aws_security_group.SG_INTERNAL_HAPROXY_DEV.id}"]

    description = "Orchestrator"
  }

  ingress {
    from_port   = 8443
    to_port     = 8443
    protocol    = "tcp"
    cidr_blocks = ["${var.portage_aviatrix_vpn}", "${var.portage_dev_cidr}", "${var.portage_hub_cidr}"]
    description = "Orchestrator Dev/Aviatrix"
  }

  ingress {
    from_port = 9002
    to_port   = 9002
    protocol  = "tcp"

    security_groups = ["${aws_security_group.SG_GUIDEWIRE_DEV.id}"]

    description = "Guidewire"
  }

  ingress {
    from_port = 15672
    to_port   = 15672
    protocol  = "tcp"

    cidr_blocks = ["${var.portage_aviatrix_vpn}"]

    security_groups = ["${aws_security_group.SG_INTERNAL_HAPROXY_DEV.id}"]

    description = "RabbitMQ Management UI"
  }

  ingress {
    from_port = 9050
    to_port   = 9050
    protocol  = "tcp"

    cidr_blocks = ["${var.portage_aviatrix_vpn}"]

    security_groups = ["${aws_security_group.SG_INTERNAL_HAPROXY_DEV.id}"]

    description = "Admin Service"
  }

  ingress {
    from_port = 9002
    to_port   = 9002
    protocol  = "tcp"

    cidr_blocks = ["${var.portage_aviatrix_vpn}"]

    security_groups = ["${aws_security_group.SG_INTERNAL_HAPROXY_DEV.id}"]

    description = "Edge Service"
  }

  ingress {
    from_port = 2377
    to_port   = 2377
    protocol  = "tcp"

    cidr_blocks = ["${var.portage_dev_cidr}"]

    description = "Cluster Management Communications"
  }

  ingress {
    from_port = 7946
    to_port   = 7946
    protocol  = "udp"

    cidr_blocks = ["${var.portage_dev_cidr}"]

    description = "Communication Among Nodes"
  }

  ingress {
    from_port = 7946
    to_port   = 7946
    protocol  = "tcp"

    cidr_blocks = ["${var.portage_dev_cidr}"]

    description = "Communication Among Nodes"
  }

  ingress {
    from_port = 4789
    to_port   = 4789
    protocol  = "udp"

    cidr_blocks = ["${var.portage_dev_cidr}"]

    description = "Overlay Network Traffic"
  }

  ingress {
    from_port = 8500
    to_port   = 8500
    protocol  = "tcp"

    cidr_blocks = ["${var.portage_aviatrix_vpn}"]

    description = "Consul UI"
  }

  ingress {
    from_port = 80
    to_port   = 80
    protocol  = "tcp"

    cidr_blocks = ["10.171.8.171/32"]

    description = "Orchestrator"
  }

  ingress {
    from_port = 8080
    to_port   = 8080
    protocol  = "tcp"

    cidr_blocks = ["10.171.2.61/32"]

    description = "CAdvisory"
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    Contract = "MANAGED_SERVICES"
    Name     = "MICROSERVICES-DEV-SG"
  }

  ingress {
    from_port = 8079
    to_port   = 8079
    protocol  = "tcp"

    cidr_blocks = ["${var.portage_aviatrix_vpn}"]

    description = "Wiremock"
  }

  ingress {
    from_port = 8082
    to_port   = 8082
    protocol  = "tcp"

    cidr_blocks = ["${var.portage_aviatrix_vpn}"]

    description = "Port for ACS"
  }

  # Access from application load balancer to the MS Endpoints
  ingress {
    from_port = 15672
    to_port   = 15672
    protocol  = "tcp"

    security_groups = ["${var.nonpro-alb-segurity-group-id}"]

    description = "Access from application load balancer to the MS endpoints - rabbitmq"
  }

  # Access from application load balancer to the MS Endpoints
  ingress {
    from_port = 5672
    to_port   = 5672
    protocol  = "tcp"

    security_groups = ["${var.nonpro-alb-segurity-group-id}"]

    description = "Access from application load balancer to the MS endpoints - rabbitmq 2ql"
  }

  # Access from application load balancer to the MS Endpoints
  ingress {
    from_port = 3306
    to_port   = 3306
    protocol  = "tcp"

    security_groups = ["${var.nonpro-alb-segurity-group-id}"]

    description = "Access from application load balancer to the MS endpoints - mysql"
  }

  # Access from application load balancer to the MS Endpoints
  ingress {
    from_port = 9050
    to_port   = 9050
    protocol  = "tcp"

    security_groups = ["${var.nonpro-alb-segurity-group-id}"]

    description = "Access from application load balancer to the MS endpoints - admin-service"
  }

  # Access from application load balancer to the MS Endpoints
  ingress {
    from_port = 9002
    to_port   = 9002
    protocol  = "tcp"

    security_groups = ["${var.nonpro-alb-segurity-group-id}"]

    description = "Access from application load balancer to the MS endpoints - edge-service"
  }

  ingress {
    from_port = 9002
    to_port   = 9002
    protocol  = "tcp"

    cidr_blocks = ["10.170.0.0/16"]

    description = "Access from Portage to Microservices"
  }

  ingress {
    from_port = 9002
    to_port   = 9002
    protocol  = "tcp"

    cidr_blocks = ["10.170.150.25/32"]

    description = "teest"
  }

  ingress {
    from_port = 9002
    to_port   = 9002
    protocol  = "tcp"

    security_groups = ["sg-0ab17a1f7a6f954f2"]

    description = "Lambda"
  }

  ingress {
    from_port = 9002
    to_port   = 9002
    protocol  = "tcp"

    security_groups = ["sg-0e55019658395fdcf"]

    description = "Guidewire"
  }
}

resource "aws_security_group" "SG_EXTERNAL_HAPROXY_DEV" {
  name = "EXTERNAL-HAPROXY-DEV-SG"

  ingress {
    from_port = 80
    to_port   = 80
    protocol  = "tcp"

    cidr_blocks = ["${var.portage_hub_cidr}"]

    description = "HTTP"
  }

  ingress {
    from_port = 443
    to_port   = 443
    protocol  = "tcp"

    cidr_blocks = ["${var.portage_hub_cidr}"]

    description = "HTTPS"
  }

  ingress {
    from_port = 8444
    to_port   = 8444
    protocol  = "tcp"

    cidr_blocks = ["${var.portage_dev_cidr}"]

    description = "Health"
  }

  ingress {
    from_port = 8404
    to_port   = 8404
    protocol  = "tcp"

    cidr_blocks = [
      "${var.portage_dev_cidr}",
      "${var.portage_aviatrix_vpn}",
    ]

    description = "Stats"
  }

  egress {
    from_port = 0
    to_port   = 0
    protocol  = "-1"

    cidr_blocks = ["0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    Contract = "MANAGED_SERVICES"
    Name     = "EXTERNAL-HAPROXY-DEV-SG"
  }
}

resource "aws_security_group" "SG_INTERNAL_HAPROXY_DEV" {
  name = "INTERNAL-HAPROXY-DEV-SG"

  ingress {
    from_port = 80
    to_port   = 80
    protocol  = "tcp"

    cidr_blocks = [
      "${var.portage_dev_cidr}",
      "${var.portage_aviatrix_vpn}",
      "${var.portage_cidr}",
    ]

    description = "HTTP"
  }

  ingress {
    from_port = 443
    to_port   = 443
    protocol  = "tcp"

    cidr_blocks = [
      "${var.portage_dev_cidr}",
      "${var.portage_aviatrix_vpn}",
      "${var.portage_cidr}",
    ]

    description = "HTTPS"
  }

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = "${var.portage_corp_cidrs}"
    description = "HTTP"
  }

  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = "${var.portage_corp_cidrs}"
    description = "HTTPS"
  }

  ingress {
    from_port = 8444
    to_port   = 8444
    protocol  = "tcp"

    cidr_blocks = ["${var.portage_dev_cidr}"]

    description = "Health"
  }

  ingress {
    from_port = 8404
    to_port   = 8404
    protocol  = "tcp"

    cidr_blocks = [
      "${var.portage_dev_cidr}",
      "${var.portage_aviatrix_vpn}",
    ]

    description = "Stats"
  }

  egress {
    from_port = 0
    to_port   = 0
    protocol  = "-1"

    cidr_blocks = ["0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    Contract = "MANAGED_SERVICES"
    Name     = "INTERNAL-HAPROXY-DEV-SG"
  }
}

resource "aws_security_group" "SG_EXTERNAL_HAPROXY" {
  name = "EXTERNAL-HAPROXY-SG"

  ingress {
    from_port = 443
    to_port   = 443
    protocol  = "tcp"

    cidr_blocks = ["${var.portage_hub_cidr}"]

    description = "HTTPS"
  }

  ingress {
    from_port = 8444
    to_port   = 8444
    protocol  = "tcp"

    cidr_blocks = ["${var.portage_dev_cidr}"]

    description = "Health"
  }

  ingress {
    from_port = 8404
    to_port   = 8404
    protocol  = "tcp"

    cidr_blocks = [
      "${var.portage_dev_cidr}",
      "${var.portage_aviatrix_vpn}",
    ]

    description = "Stats"
  }

  egress {
    from_port = 0
    to_port   = 0
    protocol  = "-1"

    cidr_blocks = ["0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    Contract = "MANAGED_SERVICES"
    Name     = "EXTERNAL-HAPROXY-SG"
  }
}

resource "aws_security_group" "SG_MYSQL_DB" {
  name = "MYSQL-DB-SG"

  ingress {
    from_port = 3306
    to_port   = 3306
    protocol  = "tcp"

    cidr_blocks = [
      "${var.portage_dev_cidr}",
      "${var.portage_hub_cidr}",
      "${var.portage_aviatrix_vpn}",
      "${var.portage_dev02_microservices_ip}",
      "10.171.8.17/32"
    ]
  }

  ingress {
    from_port = 3301
    to_port   = 3301
    protocol  = "tcp"

    cidr_blocks = [
      "${var.portage_dev_cidr}",
      "${var.portage_aviatrix_vpn}",
    ]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    Name = "MYSQL-DB-SG"
  }
}

resource "aws_security_group" "SG_INTERNAL_HAPROXY" {
  name = "INTERNAL-HAPROXY-SG"

  ingress {
    from_port = 443
    to_port   = 443
    protocol  = "tcp"

    cidr_blocks = [
      "${var.portage_dev_cidr}",
      "${var.portage_aviatrix_vpn}",
      "${var.portage_cidr}",
    ]

    description = "HTTPS"
  }

  ingress {
    from_port = 8443
    to_port   = 8443
    protocol  = "tcp"

    cidr_blocks = [
      "${var.portage_dev01_microservices}",
    ]

    description = "Microservices"
  }

  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = "${var.portage_corp_cidrs}"
    description = "HTTPS"
  }

  ingress {
    from_port = 8444
    to_port   = 8444
    protocol  = "tcp"

    cidr_blocks = ["${var.portage_dev_cidr}"]

    description = "Health"
  }

  ingress {
    from_port = 8404
    to_port   = 8404
    protocol  = "tcp"

    cidr_blocks = [
      "${var.portage_dev_cidr}",
      "${var.portage_aviatrix_vpn}",
    ]

    description = "Stats"
  }

  egress {
    from_port = 0
    to_port   = 0
    protocol  = "-1"

    cidr_blocks = [
      "0.0.0.0/0",
    ]
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    Contract = "MANAGED_SERVICES"
    Name     = "INTERNAL-HAPROXY-SG"
  }
}

resource "aws_security_group" "SG_HAPROXY_INTERNAL_PEERING" {
  name = "HAPROXY-INTERNAL-PEERING-SG"

  ingress {
    from_port = 1024
    to_port   = 1024
    protocol  = "tcp"

    security_groups = ["${aws_security_group.SG_INTERNAL_HAPROXY.id}"]

    description = "HAProxy Peering"
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    Contract = "MANAGED_SERVICES"
    Name     = "HAPROXY-INTERNAL-PEERING-SG"
  }
}

resource "aws_security_group" "SG_HAPROXY_EXTERNAL_PEERING" {
  name = "HAPROXY-EXTERNAL-PEERING-SG"

  ingress {
    from_port = 1024
    to_port   = 1024
    protocol  = "tcp"

    security_groups = ["${aws_security_group.SG_EXTERNAL_HAPROXY.id}"]

    description = "HAProxy Peering"
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    Contract = "MANAGED_SERVICES"
    Name     = "HAPROXY-EXTERNAL-PEERING-SG"
  }
}

resource "aws_security_group" "SG_GUIDEWIRE_CLUSTER_NBIO_EVENT_QUEUE" {
  name = "GUIDEWIRE-CLUSTER-NBIO-EVENT-QUEUE-SG"

  ingress {
    from_port = 40000
    to_port   = 40003
    protocol  = "tcp"

    security_groups = ["${aws_security_group.SG_GUIDEWIRE.id}"]

    description = "Cluster NBIO event queue"
  }

  egress {
    from_port = 0
    to_port   = 0
    protocol  = "-1"

    cidr_blocks = ["0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    Contract = "MANAGED_SERVICES"
    Name     = "GUIDEWIRE-CLUSTER-NBIO-EVENT-QUEUE-SG"
  }
}

resource "aws_security_group" "SG_JENKINS_GUIDEWIRE_HEALTH_CHECK" {
  name = "JENKINS-GUIDEWIRE-HEALTH-CHECK-SG"

  ingress {
    from_port = 8080
    to_port   = 8083
    protocol  = "tcp"

    security_groups = ["${aws_security_group.SG_JENKINS_CLIENT.id}"]

    description = "Dev Jenkins Client Health Check"
  }

  egress {
    from_port = 0
    to_port   = 0
    protocol  = "-1"

    cidr_blocks = ["0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    Contract = "MANAGED_SERVICES"
    Name     = "JENKINS-GUIDEWIRE-HEALTH-CHECK-SG"
  }
}

### DATA ###
resource "aws_security_group" "SG_PORTAGE_MSSQL_DB" {
  name = "PORTAGE-MSSQL-DB-SG"

  ingress {
    from_port = 1433
    to_port   = 1433
    protocol  = "tcp"

    cidr_blocks = ["${var.portage_cidr}", "10.170.0.0/16", "10.129.0.0/16", "10.128.0.0/16"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    Name = "PORTAGE-MSSQL-DB-SG"
  }

  ingress {
    from_port   = 1433
    to_port     = 1433
    protocol    = "tcp"
    cidr_blocks = ["10.171.8.171/32"]
    description = "From MS Server (ACS) - to update"
  }
}

#### DATABASES SG #####

resource "aws_security_group" "SG_CLIENT_PING_TEST" {
  name = "CLIENT-PING-TEST"

  ingress {
    from_port = -1
    to_port   = -1
    protocol  = "icmp"

    cidr_blocks = ["${var.portage_cidr}"]
  }

  egress {
    from_port = 0
    to_port   = 0
    protocol  = "-1"

    cidr_blocks = ["0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    Contract = "MANAGED_SERVICES"
    Name     = "CLIENT-PING-TEST"
  }
}

resource "aws_security_group" "SG_MONITORING" {
  name = "MONITORING-SG"

  ingress {
    from_port = 10000
    to_port   = 10001
    protocol  = "tcp"

    cidr_blocks = [
      "${var.portage_dev_cidr}",
      "${var.portage_aviatrix_vpn}",
    ]

    description = "Prometheus/Alertmanager"
  }

  ingress {
    from_port = 3000
    to_port   = 3000
    protocol  = "tcp"

    cidr_blocks = [
      "${var.portage_dev_cidr}",
      "${var.portage_aviatrix_vpn}",
    ]

    description = "Grafana"
  }

  egress {
    from_port = 0
    to_port   = 0
    protocol  = "-1"

    cidr_blocks = ["0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    Contract = "MANAGED_SERVICES"
    Name     = "MONITORING-SG"
  }
}

resource "aws_security_group" "SG_AVIATRIX_RDP" {
  name = "AVIATRIX-RDP-SG"

  ingress {
    from_port = 3389
    to_port   = 3389
    protocol  = "tcp"

    cidr_blocks = ["${var.portage_aviatrix_vpn}"]

    description = "RDP"
  }

  ingress {
    from_port = 3389
    to_port   = 3389
    protocol  = "tcp"

    cidr_blocks = ["10.171.3.229/32"]

    description = "Robin Workspaces"
  }

  egress {
    from_port = 0
    to_port   = 0
    protocol  = "-1"

    cidr_blocks = ["0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    Contract = "MANAGED_SERVICES"
    Name     = "AVIATRIX-RDP-SG"
  }
}

resource "aws_security_group" "SG_AVIATRIX_SSH" {
  name = "AVIATRIX-SSH-SG"

  ingress {
    from_port = 22
    to_port   = 22
    protocol  = "tcp"

    cidr_blocks = ["${var.portage_aviatrix_vpn}"]

    description = "SSH"
  }

  egress {
    from_port = 0
    to_port   = 0
    protocol  = "-1"

    cidr_blocks = ["0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    Contract = "MANAGED_SERVICES"
    Name     = "AVIATRIX-SSH-SG"
  }
}

#### JENKINS SG #####
resource "aws_security_group" "SG_JENKINS_CLIENT" {
  name = "JENKINS-CLIENT-SG"

  ingress {
    from_port = 22
    to_port   = 22
    protocol  = "tcp"

    cidr_blocks = ["10.0.0.0/8"]
  }

  egress {
    from_port = 0
    to_port   = 0
    protocol  = "-1"

    cidr_blocks = ["0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    Contract = "MANAGED_SERVICES"
    Name     = "JENKINS-CLIENT-SG"
  }
}

resource "aws_security_group" "SG_JENKINS_CLIENT_SSH" {
  name = "JENKINS-CLIENT-SSH-SG"

  ingress {
    from_port = 22
    to_port   = 22
    protocol  = "tcp"

    security_groups = ["${aws_security_group.SG_JENKINS_CLIENT.id}"]

    description = "Jenkins Client SSH"
  }

  egress {
    from_port = 0
    to_port   = 0
    protocol  = "-1"

    cidr_blocks = ["0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    Contract = "MANAGED_SERVICES"
    Name     = "JENKINS-CLIENT-SSH-SG"
  }
}

resource "aws_security_group" "SG_AUTOMATION_FRAMEWORK_1" {
  name   = "SG_AUTOMATION_FRAMEWORK_1"
  vpc_id = "${var.vpc_id}"

  ingress {
    from_port   = 8085
    to_port     = 8085
    protocol    = "tcp"
    description = "Automation framework app"

    cidr_blocks = [
      "${var.portage_corp_cidrs}",
      "${var.portage_hub_cidr}",
      "${var.portage_dev_cidr}",
    ]
  }

  ingress {
    from_port   = 8080
    to_port     = 8080
    protocol    = "tcp"
    description = "Reportportal traefik app"

    cidr_blocks = [
      "${var.portage_corp_cidrs}",
      "${var.portage_hub_cidr}",
      "${var.portage_dev_cidr}",
    ]
  }
}

resource "aws_security_group" "SG_AUTOMATION_FRAMEWORK_2" {
  name   = "SG_AUTOMATION_FRAMEWORK_2"
  vpc_id = "${var.vpc_id}"

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = -1
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 8082
    to_port     = 8082
    protocol    = "tcp"
    description = "Reportportal traefik app"

    cidr_blocks = [
      "${var.portage_corp_cidrs}",
      "${var.portage_hub_cidr}",
      "${var.portage_dev_cidr}",
    ]
  }

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    description = "SSH for Automation Framework server"

    cidr_blocks = [
      "${var.portage_corp_cidrs}",
      "${var.portage_hub_cidr}",
      "${var.portage_dev_cidr}",
    ]
  }
}

resource "aws_security_group" "portage_dev_lb_vpc_sg" {
  vpc_id = "${var.vpc_id}"
  name   = "${var.portage_dev_lb_vpc_sg}"

  ingress {
    cidr_blocks = [
      "${var.portage_corp_cidrs}",
      "${var.portage_hub_cidr}",
      "${var.portage_dev_cidr}",
    ]

    from_port = 443
    to_port   = 443
    protocol  = "tcp"
  }

  tags {
    Contract = "MANAGED_SERVICES"
    Name     = "portage_dev_lb_vpc_sg"
  }
}

### Route53 Resolver - IC HUB DNS ###
resource "aws_security_group" "SG_IC_HUB_DNS_RESOLVER_ENDPOINT" {
  name = "IC-HUB-DNS-RESOLVER-ENDPOINT-SG"

  ingress {
    from_port = 53
    to_port   = 53
    protocol  = "tcp"
    cidr_blocks = [
      "${var.vpc_cidr}"
    ]
    description = "DNS"
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    Contract = "MANAGED_SERVICES"
    Name     = "IC-HUB-DNS-RESOLVER-ENDPOINT-SG"
  }
}