
#=======================================================================
# OT Bravada / Index (DEV)
#=======================================================================
resource "aws_volume_attachment" "COMMONWELL_DEV_EXSTREAM_CONTENT_BRAVA_WIN_EC2_EBS_ATTACHMENT" {
  device_name = "/dev/sdh"
  volume_id   = "${aws_ebs_volume.COMMONWELL_DEV_EXSTREAM_CONTENT_BRAVA_WIN_EC2_EBS.id}"
  instance_id = "${aws_instance.ICCW_EXSTREAM_CONTENT_BRAVA_SERVER.id}"
}

resource "aws_ebs_volume" "COMMONWELL_DEV_EXSTREAM_CONTENT_BRAVA_WIN_EC2_EBS" {
  availability_zone = "ca-central-1a"
  size              = 500
  encrypted         = true
  kms_key_id        = "arn:aws:kms:ca-central-1:623885847088:key/796b83a8-4c8d-4605-955f-51986b5dda1b"
 tags = {
    Name = "COMMONWELL_DEV_EXSTREAM_CONTENT_BRAVA_WIN_EC2_VOL"
  }
}

resource "aws_volume_attachment" "COMMONWELL_DEV_EXSTREAM_CONTENT_INDEX_WIN_EC2_EBS_ATTACHMENT" {
  device_name = "/dev/sdh"
  volume_id   = "${aws_ebs_volume.COMMONWELL_DEV_EXSTREAM_CONTENT_INDEX_WIN_EC2_EBS.id}"
  instance_id = "${aws_instance.ICCW_EXSTREAM_CONTENT_INDEX_SERVER.id}"
}

resource "aws_ebs_volume" "COMMONWELL_DEV_EXSTREAM_CONTENT_INDEX_WIN_EC2_EBS" {
  availability_zone = "ca-central-1a"
  size              = 40
  encrypted         = true
  kms_key_id        = "arn:aws:kms:ca-central-1:623885847088:key/796b83a8-4c8d-4605-955f-51986b5dda1b"
 tags = {
    Name = "COMMONWELL_DEV_EXSTREAM_CONTENT_INDEX_WIN_EC2_EBS"
  }
} 
