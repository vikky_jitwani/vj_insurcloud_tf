# #=======================================================================
# # PROD
# #=======================================================================
######### GUIDEWIRE ##############
resource "aws_instance" "ICPEEL_PROD_GUIDEWIRE_ONLINE_1" {
    ami = "${var.ami_id_amazonlinux2base}"
    vpc_security_group_ids = [
        "${aws_security_group.SG_GUIDEWIRE.id}",
        "${aws_security_group.SG_GUIDEWIRE_CLUSTER_NBIO_EVENT_QUEUE.id}",
        "${aws_security_group.SG_JENKINS_GUIDEWIRE_HEALTH_CHECK.id}",
        "${aws_security_group.SG_AVIATRIX_SSH.id}",
        "${aws_security_group.SG_JENKINS_CLIENT_SSH.id}"
    ]
    subnet_id = "${var.peel-prod-private-1a-subnet}"
    instance_type = "r5a.xlarge"
    key_name    = "PeelGuidewireprod"
    associate_public_ip_address = false
    iam_instance_profile = "CMS-SSMInstanceRole"
	ebs_optimized = "true"

    root_block_device {
       volume_size = "100"
    }

    tags {
        Owner = "robhatnagar@deloitte.ca"
        ManagedBy = "CMS-TF"
        SolutionName = "Insurcloud Peel"
        Environment = "Prod"
        ApplicationName = "guidewire, docker"
        DataClassification = "PII, PHI"
        DLM-Backup = "Daily-1"
        DLM-Retention = "90"
        PatchGroup = "AmazonLinux2"
        VendorManaged = "False"
        ResourceType = "Client"
        ResourceSeverity = "Normal"
        Name = "A-MC-CAC-D-A003-PEEL-PROD-GW-ONLINE-1-LNX-EC2"
        AccountID = "${data.aws_caller_identity.current.account_id}"
        Role = "Container"
        CLIENT = "InsurCloud PEEL"
		Account = "PEEL-prod"
		Client = "Peel"
		KeyName = "PeelGuidewireprod"
    }    

    provisioner "local-exec" {
        command = "echo ${aws_instance.ICPEEL_PROD_GUIDEWIRE_ONLINE_1.private_ip} >> prod_hosts.txt"
    }
}

output "ICPEEL_PROD_GUIDEWIRE_ONLINE_1_PRIVATEIP" {
    value = "${aws_instance.ICPEEL_PROD_GUIDEWIRE_ONLINE_1.private_ip}"
}

resource "aws_instance" "ICPEEL_PROD_GUIDEWIRE_ONLINE_2" {
    ami = "${var.ami_id_amazonlinux2base}"
    vpc_security_group_ids = [
        "${aws_security_group.SG_GUIDEWIRE.id}",
        "${aws_security_group.SG_GUIDEWIRE_CLUSTER_NBIO_EVENT_QUEUE.id}",
        "${aws_security_group.SG_JENKINS_GUIDEWIRE_HEALTH_CHECK.id}",
        "${aws_security_group.SG_AVIATRIX_SSH.id}",
        "${aws_security_group.SG_JENKINS_CLIENT_SSH.id}"
    ]
    subnet_id = "${var.peel-prod-private-1b-subnet}"
    instance_type = "r5a.xlarge"
    key_name    = "PeelGuidewireprod"
    associate_public_ip_address = false
    iam_instance_profile = "CMS-SSMInstanceRole"
	ebs_optimized = "true"

    root_block_device {
       volume_size = "100"
    }

    tags {
        Owner = "robhatnagar@deloitte.ca"
        ManagedBy = "CMS-TF"
        SolutionName = "Insurcloud Peel"
        Environment = "Prod"
        ApplicationName = "guidewire, docker"
        DataClassification = "PII, PHI"
        DLM-Backup = "Daily-1"
        DLM-Retention = "90"
        PatchGroup = "AmazonLinux2"
        VendorManaged = "False"
        ResourceType = "Client"
        ResourceSeverity = "Normal"
        Name = "A-MC-CAC-D-A003-PEEL-PROD-GW-ONLINE-2-LNX-EC2"
        AccountID = "${data.aws_caller_identity.current.account_id}"
        Role = "Container"
        CLIENT = "InsurCloud PEEL"
		Account = "PEEL-prod"
		Client = "Peel"
		KeyName = "PeelGuidewireprod"
    }    

    provisioner "local-exec" {
        command = "echo ${aws_instance.ICPEEL_PROD_GUIDEWIRE_ONLINE_2.private_ip} >> prod_hosts.txt"
    }
} 

output "ICPEEL_PROD_GUIDEWIRE_ONLINE_2_PRIVATEIP" {
    value = "${aws_instance.ICPEEL_PROD_GUIDEWIRE_ONLINE_2.private_ip}"
}

resource "aws_instance" "ICPEEL_PROD_GUIDEWIRE_BATCH_1" {
    ami = "${var.ami_id_amazonlinux2base}"
    vpc_security_group_ids = [
        "${aws_security_group.SG_GUIDEWIRE.id}",
        "${aws_security_group.SG_GUIDEWIRE_CLUSTER_NBIO_EVENT_QUEUE.id}",
        "${aws_security_group.SG_JENKINS_GUIDEWIRE_HEALTH_CHECK.id}",
        "${aws_security_group.SG_AVIATRIX_SSH.id}",
        "${aws_security_group.SG_JENKINS_CLIENT_SSH.id}"
    ]
    subnet_id = "${var.peel-prod-private-1a-subnet}"
    instance_type = "r5a.xlarge"
    key_name    = "PeelGuidewireprod"
    associate_public_ip_address = false
    iam_instance_profile = "CMS-SSMInstanceRole"
	ebs_optimized = "true"

    root_block_device {
       volume_size = "100"
    }

    tags {
        Owner = "robhatnagar@deloitte.ca"
        ManagedBy = "CMS-TF"
        SolutionName = "Insurcloud Peel"
        Environment = "Prod"
        ApplicationName = "guidewire, docker"
        DataClassification = "PII, PHI"
        DLM-Backup = "Daily-1"
        DLM-Retention = "90"
        PatchGroup = "AmazonLinux2"
        VendorManaged = "False"
        ResourceType = "Client"
        ResourceSeverity = "Normal"
        Name = "A-MC-CAC-D-A003-PEEL-PROD-GW-BATCH-1-LNX-EC2"
        AccountID = "${data.aws_caller_identity.current.account_id}"
        Role = "Container"
        CLIENT = "InsurCloud PEEL"
		Account = "PEEL-prod"
		Client = "Peel"
		KeyName = "PeelGuidewireprod"
    }    

    provisioner "local-exec" {
        command = "echo ${aws_instance.ICPEEL_PROD_GUIDEWIRE_BATCH_1.private_ip} >> prod_hosts.txt"
    }
}

output "ICPEEL_PROD_GUIDEWIRE_BATCH_1_PRIVATEIP" {
    value = "${aws_instance.ICPEEL_PROD_GUIDEWIRE_BATCH_1.private_ip}"
}

resource "aws_instance" "ICPEEL_PROD_GUIDEWIRE_BATCH_2" {
    ami = "${var.ami_id_amazonlinux2base}"
    vpc_security_group_ids = [
        "${aws_security_group.SG_GUIDEWIRE.id}",
        "${aws_security_group.SG_GUIDEWIRE_CLUSTER_NBIO_EVENT_QUEUE.id}",
        "${aws_security_group.SG_JENKINS_GUIDEWIRE_HEALTH_CHECK.id}",
        "${aws_security_group.SG_AVIATRIX_SSH.id}",
        "${aws_security_group.SG_JENKINS_CLIENT_SSH.id}"
    ]
    subnet_id = "${var.peel-prod-private-1b-subnet}"
    instance_type = "r5a.xlarge"
    key_name    = "PeelGuidewireprod"
    associate_public_ip_address = false
    iam_instance_profile = "CMS-SSMInstanceRole"
	ebs_optimized = "true"

    root_block_device {
       volume_size = "100"
    }

    tags {
        Owner = "robhatnagar@deloitte.ca"
        ManagedBy = "CMS-TF"
        SolutionName = "Insurcloud Peel"
        Environment = "Prod"
        ApplicationName = "guidewire, docker"
        DataClassification = "PII, PHI"
        DLM-Backup = "Daily-1"
        DLM-Retention = "90"
        PatchGroup = "AmazonLinux2"
        VendorManaged = "False"
        ResourceType = "Client"
        ResourceSeverity = "Normal"
        Name = "A-MC-CAC-D-A003-PEEL-PROD-GW-BATCH-2-LNX-EC2"
        AccountID = "${data.aws_caller_identity.current.account_id}"
        Role = "Container"
        CLIENT = "InsurCloud PEEL"
		Account = "PEEL-prod"
		Client = "Peel"
		KeyName = "PeelGuidewireprod"
    }    

    provisioner "local-exec" {
        command = "echo ${aws_instance.ICPEEL_PROD_GUIDEWIRE_BATCH_2.private_ip} >> prod_hosts.txt"
    }
} 

output "ICPEEL_PROD_GUIDEWIRE_BATCH_2_PRIVATEIP" {
    value = "${aws_instance.ICPEEL_PROD_GUIDEWIRE_BATCH_2.private_ip}"
}

data "template_file" "PEEL_DOMAIN_JOIN_10" {
    template = "${file("IC-Peel-PROD/windowsdata.tpl")}"

    vars {
        computer_name = "GW-MSSQL-DB"
        region              = "${var.region}"
        domain              = "${var.ad_domain}"
        dns1                = "${var.ad_dns1}"
        dns2                = "${var.ad_dns2}"
        dns3                = "${var.ad_dns3}"
        ouselection         = "${var.ou_name}"
        pwordparameter      = "${var.pwordparameter}"
        unameparameter      = "${var.unameparameter}"
    }
}

resource "aws_instance" "ICPEEL_PROD_GUIDEWIRE_MSSQL_DATABASE_1" {
    ami = "${var.ami_id_windows2012R2sql14enterprise}"
    vpc_security_group_ids      = [
        "${aws_security_group.SG_MSSQL_DB.id}",
        "${aws_security_group.SG_CLIENT_CONVERSION_GW_SQL_DB.id}",
        "${aws_security_group.SG_AVIATRIX_RDP.id}"
    ]
    subnet_id = "${var.peel-prod-database-1a-subnet}"
    instance_type = "r4.xlarge"
    key_name    = "PeelGuidewireprod"
    associate_public_ip_address = false
    iam_instance_profile = "CMS-SSMInstanceRole"
    user_data = "${data.template_file.PEEL_DOMAIN_JOIN_10.rendered}"
	ebs_optimized = "true"

    lifecycle {
        prevent_destroy = true
    }

    root_block_device {
       volume_size = "200"
       delete_on_termination = false
    }

    tags {
        Owner = "robhatnagar@deloitte.ca"
        ManagedBy = "CMS-TF"
        SolutionName = "Insurcloud Peel"
        Environment = "Prod"
        ApplicationName = "database, mssql, guidewire"
        DataClassification = "PII, PHI"
        DLM-Backup = "Daily-31"
        DLM-Retention = "90"
        PatchGroup = "Windows"
        VendorManaged = "False"
        OSName = "Windows"
        ResourceType = "Client"
        ResourceSeverity = "Normal"
        Name = "A-MC-CAC-D-A003-PEEL-PROD-GW-MSSQL-DB-1-WIN-EC2"
        AccountID = "${data.aws_caller_identity.current.account_id}"
        Role = ""
        CLIENT = "InsurCloud PEEL"
		Account = "PEEL-prod"
		Client = "Peel"
		KeyName = "PeelGuidewireprod"
    }    

    provisioner "local-exec" {
        command = "echo ${aws_instance.ICPEEL_PROD_GUIDEWIRE_MSSQL_DATABASE_1.private_ip} >> prod_hosts.txt"
    } 
}

output "ICPEEL_PROD_GUIDEWIRE_MSSQL_DATABASE_1_PRIVATEIP" {
    value = "${aws_instance.ICPEEL_PROD_GUIDEWIRE_MSSQL_DATABASE_1.private_ip}"
}

######### MICROSERVICES ###########
resource "aws_instance" "ICPEEL_PROD_MICROSERVICES_MANAGER_1" {
    ami = "${var.ami_id_amazonlinux2base}"
    vpc_security_group_ids      = [
        "${aws_security_group.SG_MICROSERVICES.id}",
        "${aws_security_group.SG_AVIATRIX_SSH.id}",
        "${aws_security_group.SG_JENKINS_CLIENT_SSH.id}"
    ]
    subnet_id = "${var.peel-prod-private-1a-subnet}"
    instance_type = "t3a.small"
    key_name    = "PeelMicroservicesprod"
    associate_public_ip_address = false
    iam_instance_profile = "CMS-SSMInstanceRole"
	ebs_optimized = "true"

    root_block_device {
       volume_size = "50"
    }

    tags {
        Owner = "robhatnagar@deloitte.ca"
        ManagedBy = "CMS-TF"
        SolutionName = "Insurcloud Peel"
        Environment = "Prod"
        ApplicationName = "docker, microservices"
        DataClassification = "PII, PHI"
        DLM-Backup = "Daily-1"
        DLM-Retention = "90"
        PatchGroup = "AmazonLinux2-Microsvc1"
        VendorManaged = "False"
        ResourceType = "Client"
        ResourceSeverity = "Normal"
        Name = "A-MC-CAC-D-A003-PEEL-PROD-MICROSERVICES-MANAGER-1-LNX-EC2"
        AccountID = "${data.aws_caller_identity.current.account_id}"
        Role = "Container"
        CLIENT = "InsurCloud PEEL"
		Account = "PEEL-prod"
		Client = "Peel"
		KeyName = "PeelMicroservicesprod"
    }

    provisioner "local-exec" {
        command = "echo ${aws_instance.ICPEEL_PROD_MICROSERVICES_MANAGER_1.private_ip} >> prod_hosts.txt"
    }
}

output "ICPEEL_PROD_MICROSERVICES_MANAGER_1_PRIVATEIP" {
    value = "${aws_instance.ICPEEL_PROD_MICROSERVICES_MANAGER_1.private_ip}"
}

resource "aws_instance" "ICPEEL_PROD_MICROSERVICES_MANAGER_2" {
    ami = "${var.ami_id_amazonlinux2base}"
    vpc_security_group_ids      = [
        "${aws_security_group.SG_MICROSERVICES.id}",
        "${aws_security_group.SG_AVIATRIX_SSH.id}",
        "${aws_security_group.SG_JENKINS_CLIENT_SSH.id}"
    ]
    subnet_id = "${var.peel-prod-private-1b-subnet}"
    instance_type = "t3a.small"
    key_name    = "PeelMicroservicesprod"
    associate_public_ip_address = false
    iam_instance_profile = "CMS-SSMInstanceRole"
	ebs_optimized = "true"

    root_block_device {
       volume_size = "50"
    }

    tags {
        Owner = "robhatnagar@deloitte.ca"
        ManagedBy = "CMS-TF"
        SolutionName = "Insurcloud Peel"
        Environment = "Prod"
        ApplicationName = "docker, microservices"
        DataClassification = "PII, PHI"
        DLM-Backup = "Daily-1"
        DLM-Retention = "90"
        PatchGroup = "AmazonLinux2-Microsvc2"
        VendorManaged = "False"
        ResourceType = "Client"
        ResourceSeverity = "Normal"
        Name = "A-MC-CAC-D-A003-PEEL-PROD-MICROSERVICES-MANAGER-2-LNX-EC2"
        AccountID = "${data.aws_caller_identity.current.account_id}"
        Role = "Container"
        CLIENT = "InsurCloud PEEL"
		Account = "PEEL-prod"
		Client = "Peel"
		KeyName = "PeelMicroservicesprod"
    }

    provisioner "local-exec" {
        command = "echo ${aws_instance.ICPEEL_PROD_MICROSERVICES_MANAGER_2.private_ip} >> prod_hosts.txt"
    }
}

output "ICPEEL_PROD_MICROSERVICES_MANAGER_2_PRIVATEIP" {
    value = "${aws_instance.ICPEEL_PROD_MICROSERVICES_MANAGER_2.private_ip}"
}

resource "aws_instance" "ICPEEL_PROD_MICROSERVICES_MANAGER_3" {
    ami = "${var.ami_id_amazonlinux2base}"
    vpc_security_group_ids      = [
        "${aws_security_group.SG_MICROSERVICES.id}",
        "${aws_security_group.SG_AVIATRIX_SSH.id}",
        "${aws_security_group.SG_JENKINS_CLIENT_SSH.id}"
    ]
    subnet_id = "${var.peel-prod-private-1a-subnet}"
    instance_type = "t3a.small"
    key_name    = "PeelMicroservicesprod"
    associate_public_ip_address = false
    iam_instance_profile = "CMS-SSMInstanceRole"
	ebs_optimized = "true"

    root_block_device {
       volume_size = "50"
    }

    tags {
        Owner = "robhatnagar@deloitte.ca"
        ManagedBy = "CMS-TF"
        SolutionName = "Insurcloud Peel"
        Environment = "Prod"
        ApplicationName = "docker, microservices"
        DataClassification = "PII, PHI"
        DLM-Backup = "Daily-1"
        DLM-Retention = "90"
        PatchGroup = "AmazonLinux2-Microsvc3"
        VendorManaged = "False"
        ResourceType = "Client"
        ResourceSeverity = "Normal"
        Name = "A-MC-CAC-D-A003-PEEL-PROD-MICROSERVICES-MANAGER-3-LNX-EC2"
        AccountID = "${data.aws_caller_identity.current.account_id}"
        Role = "Container"
        CLIENT = "InsurCloud PEEL"
		Account = "PEEL-prod"
		Client = "Peel"
		KeyName = "PeelMicroservicesprod"
    }

    provisioner "local-exec" {
        command = "echo ${aws_instance.ICPEEL_PROD_MICROSERVICES_MANAGER_3.private_ip} >> prod_hosts.txt"
    }
}

output "ICPEEL_PROD_MICROSERVICES_MANAGER_3_PRIVATEIP" {
    value = "${aws_instance.ICPEEL_PROD_MICROSERVICES_MANAGER_3.private_ip}"
}

resource "aws_instance" "ICPEEL_PROD_MICROSERVICES_APPLICATION_1" {
    ami = "${var.ami_id_amazonlinux2base}"
    vpc_security_group_ids = [
        "${aws_security_group.SG_MICROSERVICES.id}",
        "${aws_security_group.SG_MONITORING.id}",
        "${aws_security_group.SG_AVIATRIX_SSH.id}",
        "${aws_security_group.SG_JENKINS_CLIENT_SSH.id}"
    ]
    subnet_id = "${var.peel-prod-private-1a-subnet}"
    instance_type = "t3a.2xlarge"
    key_name    = "PeelMicroservicesprod"
    associate_public_ip_address = false
    iam_instance_profile = "CMS-SSMInstanceRole"
	ebs_optimized = "true"

    root_block_device {
       volume_size = "100"
    }

    tags {
        Owner = "robhatnagar@deloitte.ca"
        ManagedBy = "CMS-TF"
        SolutionName = "Insurcloud Peel"
        Environment = "Prod"
        ApplicationName = "docker, microservices"
        DataClassification = "PII, PHI"
        DLM-Backup = "Daily-7"
        DLM-Retention = "90"
        PatchGroup = "AmazonLinux2-Microsvc4"
        VendorManaged = "False"
        ResourceType = "Client"
        ResourceSeverity = "Normal"
        Name = "A-MC-CAC-D-A003-PEEL-PROD-MICROSERVICES-APP-1-LNX-EC2"
        AccountID = "${data.aws_caller_identity.current.account_id}"
        Role = "Container"
        CLIENT = "InsurCloud PEEL"
		Account = "PEEL-prod"
		Client = "Peel"
		KeyName = "PeelMicroservicesprod"
    }

    provisioner "local-exec" {
        command = "echo ${aws_instance.ICPEEL_PROD_MICROSERVICES_APPLICATION_1.private_ip} >> prod_hosts.txt"
    }
}

output "ICPEEL_PROD_MICROSERVICES_APPLICATION_1_PRIVATEIP" {
    value = "${aws_instance.ICPEEL_PROD_MICROSERVICES_APPLICATION_1.private_ip}"
}

resource "aws_instance" "ICPEEL_PROD_MICROSERVICES_APPLICATION_2" {
    ami = "${var.ami_id_amazonlinux2base}"
    vpc_security_group_ids = [
        "${aws_security_group.SG_MICROSERVICES.id}",
        "${aws_security_group.SG_MONITORING.id}",
        "${aws_security_group.SG_AVIATRIX_SSH.id}",
        "${aws_security_group.SG_JENKINS_CLIENT_SSH.id}"
    ]
    subnet_id = "${var.peel-prod-private-1b-subnet}"
    instance_type = "t3a.2xlarge"
    key_name    = "PeelMicroservicesprod"
    associate_public_ip_address = false
    iam_instance_profile = "CMS-SSMInstanceRole"
	ebs_optimized = "true"

    root_block_device {
       volume_size = "100"
    }
    
    tags {
        Owner = "robhatnagar@deloitte.ca"
        ManagedBy = "CMS-TF"
        SolutionName = "Insurcloud Peel"
        Environment = "Prod"
        ApplicationName = "docker, microservices"
        DataClassification = "PII, PHI"
        DLM-Backup = "Daily-7"
        DLM-Retention = "90"
        PatchGroup = "AmazonLinux2-Microsvc5"
        VendorManaged = "False"
        ResourceType = "Client"
        ResourceSeverity = "Normal"
        Name = "A-MC-CAC-D-A003-PEEL-PROD-MICROSERVICES-APP-2-LNX-EC2"
        AccountID = "${data.aws_caller_identity.current.account_id}"
        Role = "Container"
        CLIENT = "InsurCloud PEEL"
		Account = "PEEL-prod"
		Client = "Peel"
		KeyName = "PeelMicroservicesprod"
    }

    provisioner "local-exec" {
        command = "echo ${aws_instance.ICPEEL_PROD_MICROSERVICES_APPLICATION_2.private_ip} >> prod_hosts.txt"
    }
}

output "ICPEEL_PROD_MICROSERVICES_APPLICATION_2_PRIVATEIP" {
    value = "${aws_instance.ICPEEL_PROD_MICROSERVICES_APPLICATION_2.private_ip}"
}

######### HAPROXY ##############
resource "aws_instance" "ICPEEL_PROD_EXTERNAL_HAPROXY_1" {
    ami = "${var.ami_id_amazonlinux2base}"
    vpc_security_group_ids = [
        "${aws_security_group.SG_EXTERNAL_HAPROXY.id}",
        "${aws_security_group.SG_HAPROXY_EXTERNAL_PEERING.id}",
        "${aws_security_group.SG_AVIATRIX_SSH.id}",
        "${aws_security_group.SG_JENKINS_CLIENT_SSH.id}"
    ]
    subnet_id = "${var.peel-prod-private-1a-subnet}"
    instance_type = "t3a.small"
    key_name    = "PeelHAProxyprod"
    associate_public_ip_address = false
    iam_instance_profile = "CMS-SSMInstanceRole"
	ebs_optimized = "true"

    root_block_device {
       volume_size = "50"
    }

    tags {
        Owner = "robhatnagar@deloitte.ca"
        ManagedBy = "CMS-TF"
        SolutionName = "Insurcloud Peel"
        Environment = "Prod"
        ApplicationName = "docker, haproxy"
        DataClassification = "PII, PHI"
        DLM-Backup = "Daily-1"
        DLM-Retention = "90"
        PatchGroup = "AmazonLinux2"
        VendorManaged = "False"
        ResourceType = "Client"
        ResourceSeverity = "Normal"
        Name = "A-MC-CAC-D-A003-PEEL-PROD-EXTERNAL-HAPROXY-1-EC2"
        AccountID = "${data.aws_caller_identity.current.account_id}"
        Role = "Container"
        CLIENT = "InsurCloud PEEL"
		Account = "PEEL-prod"
		Client = "Peel"
		KeyName = "PeelHAProxyprod"
    }
   
    provisioner "local-exec" {
        command = "echo ${aws_instance.ICPEEL_PROD_EXTERNAL_HAPROXY_1.private_ip} >> prod_hosts.txt"
    }
}

output "ICPEEL_PROD_EXTERNAL_HAPROXY_1_PRIVATEIP" {
    value = "${aws_instance.ICPEEL_PROD_EXTERNAL_HAPROXY_1.private_ip}"
}

resource "aws_instance" "ICPEEL_PROD_EXTERNAL_HAPROXY_2" {
    ami = "${var.ami_id_amazonlinux2base}"
    vpc_security_group_ids = [
        "${aws_security_group.SG_EXTERNAL_HAPROXY.id}",
        "${aws_security_group.SG_HAPROXY_EXTERNAL_PEERING.id}",
        "${aws_security_group.SG_AVIATRIX_SSH.id}",
        "${aws_security_group.SG_JENKINS_CLIENT_SSH.id}"
    ]
    subnet_id = "${var.peel-prod-private-1b-subnet}"
    instance_type = "t3a.small"
    key_name    = "PeelHAProxyprod"
    associate_public_ip_address = false
    iam_instance_profile = "CMS-SSMInstanceRole"
	ebs_optimized = "true"

    root_block_device {
       volume_size = "50"
    }

    tags {
        Owner = "robhatnagar@deloitte.ca"
        ManagedBy = "CMS-TF"
        SolutionName = "Insurcloud Peel"
        Environment = "Prod"
        ApplicationName = "docker, haproxy"
        DataClassification = "NA"
        DLM-Backup = "Daily-1"
        DLM-Retention = "90"
        PatchGroup = "AmazonLinux2"
        VendorManaged = "False"
        ResourceType = "Client"
        ResourceSeverity = "Normal"
        Name = "A-MC-CAC-D-A003-PEEL-PROD-EXTERNAL-HAPROXY-2-EC2"
        AccountID = "${data.aws_caller_identity.current.account_id}"
        Role = "Container"
        CLIENT = "InsurCloud PEEL"
		Account = "PEEL-prod"
		Client = "Peel"
		KeyName = "PeelHAProxyprod"
    }

    provisioner "local-exec" {
        command = "echo ${aws_instance.ICPEEL_PROD_EXTERNAL_HAPROXY_2.private_ip} >> prod_hosts.txt"
    }
}

output "ICPEEL_PROD_EXTERNAL_HAPROXY_2_PRIVATEIP" {
    value = "${aws_instance.ICPEEL_PROD_EXTERNAL_HAPROXY_2.private_ip}"
}

resource "aws_instance" "ICPEEL_PROD_INTERNAL_HAPROXY_1" {
    ami = "${var.ami_id_amazonlinux2base}"
    vpc_security_group_ids = [
        "${aws_security_group.SG_INTERNAL_HAPROXY.id}",
        "${aws_security_group.SG_HAPROXY_INTERNAL_PEERING.id}",
        "${aws_security_group.SG_AVIATRIX_SSH.id}",
        "${aws_security_group.SG_JENKINS_CLIENT_SSH.id}",
        "${aws_security_group.SG_CLIENT_PING_TEST.id}"
    ]
    subnet_id = "${var.peel-prod-private-1a-subnet}"
    instance_type = "t3a.small"
    key_name    = "PeelHAProxyprod"
    associate_public_ip_address = false
    iam_instance_profile = "CMS-SSMInstanceRole"
	ebs_optimized = "true"

    root_block_device {
       volume_size = "50"
    }

    tags {
        Owner = "robhatnagar@deloitte.ca"
        ManagedBy = "CMS-TF"
        SolutionName = "Insurcloud Peel"
        Environment = "Prod"
        ApplicationName = "docker, haproxy"
        DataClassification = "PII, PHI"
        DLM-Backup = "Daily-1"
        DLM-Retention = "90"
        PatchGroup = "AmazonLinux2"
        VendorManaged = "False"
        ResourceType = "Client"
        ResourceSeverity = "Normal"
        Name = "A-MC-CAC-D-A003-PEEL-PROD-INTERNAL-HAPROXY-1-EC2"
        AccountID = "${data.aws_caller_identity.current.account_id}"
        Role = "Container"
        CLIENT = "InsurCloud PEEL"
		Account = "PEEL-prod"
		Client = "Peel"
		KeyName = "PeelHAProxyprod"
    }

    provisioner "local-exec" {
        command = "echo ${aws_instance.ICPEEL_PROD_INTERNAL_HAPROXY_1.private_ip} >> prod_hosts.txt"
    }
}

output "ICPEEL_PROD_INTERNAL_HAPROXY_1_PRIVATEIP" {
    value = "${aws_instance.ICPEEL_PROD_INTERNAL_HAPROXY_1.private_ip}"
}

resource "aws_instance" "ICPEEL_PROD_INTERNAL_HAPROXY_2" {
    ami = "${var.ami_id_amazonlinux2base}"
    vpc_security_group_ids = [
        "${aws_security_group.SG_INTERNAL_HAPROXY.id}",
        "${aws_security_group.SG_HAPROXY_INTERNAL_PEERING.id}",
        "${aws_security_group.SG_AVIATRIX_SSH.id}",
        "${aws_security_group.SG_JENKINS_CLIENT_SSH.id}",
        "${aws_security_group.SG_CLIENT_PING_TEST.id}"
    ]
    subnet_id = "${var.peel-prod-private-1b-subnet}"
    instance_type = "t3a.small"
    key_name    = "PeelHAProxyprod"
    associate_public_ip_address = false
    iam_instance_profile = "CMS-SSMInstanceRole"
	ebs_optimized = "true"

    root_block_device {
       volume_size = "50"
    }

    tags {
        Owner = "robhatnagar@deloitte.ca"
        ManagedBy = "CMS-TF"
        SolutionName = "Insurcloud Peel"
        Environment = "Prod"
        ApplicationName = "docker, haproxy"
        DataClassification = "NA"
        DLM-Backup = "Daily-1"
        DLM-Retention = "90"
        PatchGroup = "AmazonLinux2"
        VendorManaged = "False"
        ResourceType = "Client"
        ResourceSeverity = "Normal"
        Name = "A-MC-CAC-D-A003-PEEL-PROD-INTERNAL-HAPROXY-2-EC2"
        AccountID = "${data.aws_caller_identity.current.account_id}"
        Role = "Container"
        CLIENT = "InsurCloud PEEL"
		Account = "PEEL-prod"
		Client = "Peel"
		KeyName = "PeelHAProxyprod"
    }

    provisioner "local-exec" {
        command = "echo ${aws_instance.ICPEEL_PROD_INTERNAL_HAPROXY_2.private_ip} >> prod_hosts.txt"
    }
}

output "ICPEEL_PROD_INTERNAL_HAPROXY_2_PRIVATEIP" {
    value = "${aws_instance.ICPEEL_PROD_INTERNAL_HAPROXY_2.private_ip}"
}

######### DATA ##############
data "template_file" "PEEL_DOMAIN_JOIN_3" {
    template = "${file("IC-Peel-PROD/windowsdata.tpl")}"

    vars {
        computer_name = "SAP-SERVER-WIN"
        region              = "${var.region}"
        domain              = "${var.ad_domain}"
        dns1                = "${var.ad_dns1}"
        dns2                = "${var.ad_dns2}"
        dns3                = "${var.ad_dns3}"
        ouselection         = "${var.ou_name}"
        pwordparameter      = "${var.pwordparameter}"
        unameparameter      = "${var.unameparameter}"
    }
}

resource "aws_instance" "ICPEEL_PROD_SAP_SERVER" {
    ami = "${var.ami_id_windows2016base}"
    vpc_security_group_ids = [
        "${aws_security_group.SG_SAP.id}",
        "${aws_security_group.SG_AVIATRIX_RDP.id}",
        "${aws_security_group.SG_JOBSCHEDULER_SSH.id}"
    ]
    subnet_id = "${var.peel-prod-private-1a-subnet}"
    instance_type = "t3a.xlarge"
    key_name    = "PeelDataprod"
    associate_public_ip_address = false
    iam_instance_profile = "CMS-SSMInstanceRole"
    user_data = "${data.template_file.PEEL_DOMAIN_JOIN_3.rendered}"
	ebs_optimized = "true"

    lifecycle {
        prevent_destroy = true
    }

    root_block_device {
       volume_size = "200"
    }

    tags {
        Owner = "robhatnagar@deloitte.ca"
        ManagedBy = "CMS-TF"
        SolutionName = "Insurcloud Peel"
        Environment = "Prod"
        ApplicationName = "sap"
        DataClassification = "PII, PHI"
        DLM-Backup = "Daily-7"
        DLM-Retention = "90"
        PatchGroup = "Windows"
        VendorManaged = "False"
        ResourceType = "Client"
        ResourceSeverity = "Normal"
        Name = "A-MC-CAC-D-A003-PEEL-PROD-SAP-SERVER-WIN-EC2"
        AccountID = "${data.aws_caller_identity.current.account_id}"
        Role = ""
        CLIENT = "InsurCloud PEEL"
		Account = "PEEL-prod"
		Client = "Peel"
		KeyName = "PeelDataprod"
    }

    provisioner "local-exec" {
        command = "echo ${aws_instance.ICPEEL_PROD_SAP_SERVER.private_ip} >> prod_hosts.txt"
    }
} 

output "ICPEEL_PROD_SAP_SERVER_PRIVATEIP" {
    value = "${aws_instance.ICPEEL_PROD_SAP_SERVER.private_ip}"
}

data "template_file" "PEEL_DOMAIN_JOIN_11" {
    template = "${file("IC-Peel-PROD/windowsdata.tpl")}"

    vars {
        computer_name = "SAP-SSIS-MSSQL"
        region              = "${var.region}"
        domain              = "${var.ad_domain}"
        dns1                = "${var.ad_dns1}"
        dns2                = "${var.ad_dns2}"
        dns3                = "${var.ad_dns3}"
        ouselection         = "${var.ou_name}"
        pwordparameter      = "${var.pwordparameter}"
        unameparameter      = "${var.unameparameter}"
    }
}

resource "aws_instance" "ICPEEL_PROD_SAP_SSIS_MSSQL_DATABASE_1" {
    ami = "${var.ami_id_windows2012R2sql14enterprise}"
    vpc_security_group_ids      = [
        "${aws_security_group.SG_MSSQL_DB.id}",
        "${aws_security_group.SG_PEEL_MSSQL_DB.id}",
        "${aws_security_group.SG_AVIATRIX_RDP.id}"
    ]
    subnet_id = "${var.peel-prod-database-1a-subnet}"
    instance_type = "r4.xlarge"
    key_name    = "PeelDataprod"
    associate_public_ip_address = false
    iam_instance_profile = "CMS-SSMInstanceRole"
    user_data = "${data.template_file.PEEL_DOMAIN_JOIN_11.rendered}"
	ebs_optimized = "true"

    lifecycle {
        prevent_destroy = true
    }

    root_block_device {
       volume_size = "200"
       delete_on_termination = false
    }

    tags {
        Owner = "robhatnagar@deloitte.ca"
        ManagedBy = "CMS-TF"
        SolutionName = "Insurcloud Peel"
        Environment = "Prod"
        ApplicationName = "database, mssql, ssis, sap"
        DataClassification = "PII, PHI"
        DLM-Backup = "Daily-45"
        DLM-Retention = "90"
        PatchGroup = "Windows"
        VendorManaged = "False"
        OSName = "Windows"
        ResourceType = "Client"
        ResourceSeverity = "Normal"
        Name = "A-MC-CAC-D-A003-PEEL-PROD-SAP-SSIS-MSSQL-DB-1-WIN-EC2"
        AccountID = "${data.aws_caller_identity.current.account_id}"
        Role = ""
        CLIENT = "InsurCloud PEEL"
		Account = "PEEL-prod"
		Client = "Peel"
		KeyName = "PeelDataprod"
    }    

    provisioner "local-exec" {
        command = "echo ${aws_instance.ICPEEL_PROD_SAP_SSIS_MSSQL_DATABASE_1.private_ip} >> prod_hosts.txt"
    } 
}

output "ICPEEL_PROD_SAP_SSIS_MSSQL_DATABASE_1_PRIVATEIP" {
    value = "${aws_instance.ICPEEL_PROD_SAP_SSIS_MSSQL_DATABASE_1.private_ip}"
}

data "template_file" "PEEL_DOMAIN_JOIN_13" {
    template = "${file("IC-Peel-PROD/windowsdata.tpl")}"

    vars {
        computer_name = "DEVSAPSSISSQL-DB"
        region              = "${var.region}"
        domain              = "${var.ad_domain}"
        dns1                = "${var.ad_dns1}"
        dns2                = "${var.ad_dns2}"
        dns3                = "${var.ad_dns3}"
        ouselection         = "${var.ou_name}"
        pwordparameter      = "${var.pwordparameter}"
        unameparameter      = "${var.unameparameter}"
    }
}

resource "aws_instance" "ICPEEL_PPS_SAP_SSIS_MSSQL_DATABASE" {
    ami = "${var.ami_id_windows2012R2base}"
    vpc_security_group_ids      = [
        "${aws_security_group.SG_MSSQL_DB.id}",
        "${aws_security_group.SG_AVIATRIX_RDP.id}"
    ]
    subnet_id = "${var.peel-prod-database-1a-subnet}"
    instance_type = "r5a.large"
    key_name    = "PeelDataprod"
    associate_public_ip_address = false
    iam_instance_profile = "CMS-SSMInstanceRole"
	ebs_optimized = "true"

    lifecycle {
        prevent_destroy = false    
    }

    root_block_device {
        volume_size = "1024"
    }

    user_data = "${data.template_file.PEEL_DOMAIN_JOIN_13.rendered}"

    tags {
        Owner = "robhatnagar@deloitte.ca"
        ManagedBy = "CMS-TF"
        SolutionName = "Insurcloud Peel"
        Environment = "Prod"
        ApplicationName = "database, mssql, sap"
        DataClassification = "PII, PHI"
        DLM-Backup = "Daily-7"
        DLM-Retention = "90"
        PatchGroup = "Windows"
        VendorManaged = "False"
        OSName = "Windows"
        ResourceType = "Client"
        ResourceSeverity = "Normal"
        Name = "A-MC-CAC-D-A003-PEEL-PPS-SAP-SSIS-DATABASE-MSSQL-WIN-EC2"
        AccountID = "${data.aws_caller_identity.current.account_id}"
        Role = ""
        CLIENT = "InsurCloud PEEL"
		Account = "PEEL-prod"
		Client = "Peel"
		KeyName = "PeelDataprod"
    }

    provisioner "local-exec" {
        command = "echo ${aws_instance.ICPEEL_PPS_SAP_SSIS_MSSQL_DATABASE.private_ip} >> prod_hosts.txt"
    }
}

output "ICPEEL_PPS_SAP_SSIS_MSSQL_DATABASE_PRIVATEIP" {
    value = "${aws_instance.ICPEEL_PPS_SAP_SSIS_MSSQL_DATABASE.private_ip}"
}

data "template_file" "PEEL_DOMAIN_JOIN_14" {
    template = "${file("IC-Peel-PROD/windowsdata.tpl")}"

    vars {
        computer_name = "PPS-SAP-WIN"
        region              = "${var.region}"
        domain              = "${var.ad_domain}"
        dns1                = "${var.ad_dns1}"
        dns2                = "${var.ad_dns2}"
        dns3                = "${var.ad_dns3}"
        ouselection         = "${var.ou_name}"
        pwordparameter      = "${var.pwordparameter}"
        unameparameter      = "${var.unameparameter}"
    }
}

resource "aws_instance" "ICPEEL_PPS_SAP_SERVER" {
    ami = "${var.ami_id_windows2016base}"
    vpc_security_group_ids = [
        "${aws_security_group.SG_SAP.id}",
        "${aws_security_group.SG_AVIATRIX_RDP.id}",
        "${aws_security_group.SG_DEV_JOBSCHEDULER_SSH.id}"
    ]
    subnet_id = "${var.peel-prod-private-1a-subnet}"
    instance_type = "t3a.xlarge"
    key_name    = "PeelDataprod"
    associate_public_ip_address = false
    iam_instance_profile = "CMS-SSMInstanceRole"
	ebs_optimized = "true"

    lifecycle {
        prevent_destroy = false
    }

    root_block_device {
       volume_size = "200"
    }

    user_data = "${data.template_file.PEEL_DOMAIN_JOIN_14.rendered}"

    tags {
        Owner = "robhatnagar@deloitte.ca"
        ManagedBy = "CMS-TF"
        SolutionName = "Insurcloud Peel"
        Environment = "Prod"
        ApplicationName = "sap"
        DataClassification = "PII, PHI"
        DLM-Backup = "Daily-7"
        DLM-Retention = "90"
        PatchGroup = "Windows"
        VendorManaged = "False"
        OSName = "Windows"
        ResourceType = "Client"
        ResourceSeverity = "Normal"
        Name = "A-MC-CAC-D-A003-PEEL-PPS-SAP-SERVER-WIN-EC2"
        AccountID = "${data.aws_caller_identity.current.account_id}"
        Role = ""
        CLIENT = "InsurCloud PEEL"
		Account = "PEEL-prod"
		Client = "Peel"
		KeyName = "PeelDataprod"
    }

    provisioner "local-exec" {
        command = "echo ${aws_instance.ICPEEL_PPS_SAP_SERVER.private_ip} >> prod_hosts.txt"
    }
} 

output "ICPEEL_PPS_SAP_SERVER_PRIVATEIP" {
    value = "${aws_instance.ICPEEL_PPS_SAP_SERVER.private_ip}"
}

data "template_file" "PEEL_DOMAIN_JOIN_4" {
    template = "${file("IC-Peel-PROD/windowsdata.tpl")}"

    vars {
        computer_name = "HUBIO"
        region              = "${var.region}"
        domain              = "${var.ad_domain}"
        dns1                = "${var.ad_dns1}"
        dns2                = "${var.ad_dns2}"
        dns3                = "${var.ad_dns3}"
        ouselection         = "${var.ou_name}"
        pwordparameter      = "${var.pwordparameter}"
        unameparameter      = "${var.unameparameter}"
    }
}

resource "aws_instance" "ICPEEL_PROD_HUBIO_SERVER" {
    ami = "${var.ami_id_windows2019container}"
    vpc_security_group_ids      = [
        "${aws_security_group.SG_HUBIO.id}",
        "${aws_security_group.SG_JOBSCHEDULER_SSH.id}",
        "${aws_security_group.SG_AVIATRIX_RDP.id}"
    ]
    subnet_id = "${var.peel-prod-private-1a-subnet}"
    instance_type = "t3a.large"
    key_name    = "PeelDataprod"
    associate_public_ip_address = false
    iam_instance_profile = "CMS-SSMInstanceRole"
    user_data = "${data.template_file.PEEL_DOMAIN_JOIN_4.rendered}"
	ebs_optimized = "true"

    lifecycle {
        prevent_destroy = true
    }

    root_block_device {
        volume_size = "100"
    }

    tags {
        Owner = "robhatnagar@deloitte.ca"
        ManagedBy = "CMS-TF"
        SolutionName = "Insurcloud Peel"
        Environment = "Prod"
        ApplicationName = "docker, hubio"
        DataClassification = "PII, PHI"
        DLM-Backup = "Daily-1"
        DLM-Retention = "90"
        PatchGroup = "Windows"
        VendorManaged = "False"
        ResourceType = "Client"
        ResourceSeverity = "Normal"
        Name = "A-MC-CAC-D-A003-PEEL-PROD-HUBIO-WIN-EC2"
        AccountID = "${data.aws_caller_identity.current.account_id}"
        Role = "Container"
        CLIENT = "InsurCloud PEEL"
		Account = "PEEL-prod"
		Client = "Peel"
		KeyName = "PeelDataprod"
    }

    provisioner "local-exec" {
        command = "echo ${aws_instance.ICPEEL_PROD_HUBIO_SERVER.private_ip} >> prod_hosts.txt"
    }
}

output "ICPEEL_PROD_HUBIO_SERVER_PRIVATEIP" {
    value = "${aws_instance.ICPEEL_PROD_HUBIO_SERVER.private_ip}"
}

data "template_file" "PEEL_DOMAIN_JOIN_8" {
    template = "${file("IC-Peel-PROD/windowsdata.tpl")}"

    vars {
        computer_name = "QLIK-SENSE"
        region              = "${var.region}"
        domain              = "${var.ad_domain}"
        dns1                = "${var.ad_dns1}"
        dns2                = "${var.ad_dns2}"
        dns3                = "${var.ad_dns3}"
        ouselection         = "${var.ou_name}"
        pwordparameter      = "${var.pwordparameter}"
        unameparameter      = "${var.unameparameter}"
    }
}

data "template_file" "PEEL_DOMAIN_JOIN_16" {
    template = "${file("IC-Peel-PROD/windowsdata.tpl")}"

    vars {
        computer_name = "PPS-HUBIO-WIN"
        region              = "${var.region}"
        domain              = "${var.ad_domain}"
        dns1                = "${var.ad_dns1}"
        dns2                = "${var.ad_dns2}"
        dns3                = "${var.ad_dns3}"
        ouselection         = "${var.ou_name}"
        pwordparameter      = "${var.pwordparameter}"
        unameparameter      = "${var.unameparameter}"
    }
}

resource "aws_instance" "ICPEEL_PPS_HUBIO_SERVER" {
    ami = "${var.ami_id_windows2019container}"
    vpc_security_group_ids      = [
        "${aws_security_group.SG_HUBIO.id}",
        "${aws_security_group.SG_DEV_JOBSCHEDULER_SSH.id}",
        "${aws_security_group.SG_AVIATRIX_RDP.id}"
    ]
    subnet_id = "${var.peel-prod-private-1a-subnet}"
    instance_type = "t3a.large"
    key_name    = "PeelDataprod"
    associate_public_ip_address = false
    iam_instance_profile = "CMS-SSMInstanceRole"
	ebs_optimized = "true"

    lifecycle {
        prevent_destroy = false
    }

    root_block_device {
        volume_size = "100"
    }
    user_data = "${data.template_file.PEEL_DOMAIN_JOIN_16.rendered}"

    tags {
        Owner = "robhatnagar@deloitte.ca"
        ManagedBy = "CMS-TF"
        SolutionName = "Insurcloud Peel"
        Environment = "Prod"
        ApplicationName = "docker, hubio"
        DataClassification = "PII, PHI"
        DLM-Backup = "Daily-1"
        DLM-Retention = "90"
        PatchGroup = "Windows"
        VendorManaged = "False"
        OSName = "Windows"
        ResourceType = "Client"
        ResourceSeverity = "Normal"
        Name = "A-MC-CAC-D-A003-PEEL-PPS-HUBIO-WIN-EC2"
        AccountID = "${data.aws_caller_identity.current.account_id}"
        Role = "Container"
        CLIENT = "InsurCloud PEEL"
		Account = "PEEL-prod"
		Client = "Peel"
		KeyName = "PeelDataprod"
    }

    provisioner "local-exec" {
        command = "echo ${aws_instance.ICPEEL_PPS_HUBIO_SERVER.private_ip} >> prod_hosts.txt"
    }
}

output "ICPEEL_PPS_HUBIO_SERVER_PRIVATEIP" {
    value = "${aws_instance.ICPEEL_PPS_HUBIO_SERVER.private_ip}"
}

resource "aws_instance" "ICPEEL_PROD_QLIK_SENSE" {
    ami = "${var.ami_id_windows2016base}"
    vpc_security_group_ids = [
		"${aws_security_group.SG_QLIK_SENSE.id}",
        "${aws_security_group.SG_AVIATRIX_RDP.id}"
	]
    subnet_id = "${var.peel-prod-private-1a-subnet}"
    instance_type = "i3.xlarge"
    key_name    = "PeelDataprod"
    associate_public_ip_address = false
    iam_instance_profile = "CMS-SSMInstanceRole"
    user_data = "${data.template_file.PEEL_DOMAIN_JOIN_8.rendered}"
	ebs_optimized = "true"

    // lifecycle {
    //     prevent_destroy = true
  	// }

    root_block_device {
        volume_size = "130"
		volume_type = "io1"
		iops = "3000"
    }

	tags {
		Owner = "robhatnagar@deloitte.ca"
		ManagedBy = "CMS-TF"
		SolutionName = "Insurcloud Peel"
		Environment = "Prod"
		ApplicationName = "qlik, sense"
		DataClassification = "PII, PHI"
		DLM-Backup = "Daily-7"
		DLM-Retention = "90"
		PatchGroup = "Windows"
        VendorManaged = "False"
		ResourceType = "Client"
		ResourceSeverity = "Critical"
		Name = "A-MC-CAC-D-A003-PEEL-PROD-QLIK-SENSE-WIN-EC2"
		AccountID = "${data.aws_caller_identity.current.account_id}"
		Role = ""
        CLIENT = "InsurCloud PEEL"
		Account = "PEEL-prod"
		Client = "Peel"
		KeyName = "PeelDataprod"
	}
}

output "ICPEEL_PROD_QLIK_SENSE_PRIVATEIP" {
	value = "${aws_instance.ICPEEL_PROD_QLIK_SENSE.private_ip}"
}

data "template_file" "PEEL_DOMAIN_JOIN_15" {
    template = "${file("IC-Peel-PROD/windowsdata.tpl")}"

    vars {
        computer_name = "PPS-QLIK-SENSE"
        region              = "${var.region}"
        domain              = "${var.ad_domain}"
        dns1                = "${var.ad_dns1}"
        dns2                = "${var.ad_dns2}"
        dns3                = "${var.ad_dns3}"
        ouselection         = "${var.ou_name}"
        pwordparameter      = "${var.pwordparameter}"
        unameparameter      = "${var.unameparameter}"
    }
}

######### OPENTEXT ##############
data "template_file" "PEEL_DOMAIN_JOIN_5" {
    template = "${file("IC-Peel-PROD/windowsdata.tpl")}"

    vars {
        computer_name = "EXSTREAM-COMMN"
        region              = "${var.region}"
        domain              = "${var.ad_domain}"
        dns1                = "${var.ad_dns1}"
        dns2                = "${var.ad_dns2}"
        dns3                = "${var.ad_dns3}"
        ouselection         = "${var.ou_name}"
        pwordparameter      = "${var.pwordparameter}"
        unameparameter      = "${var.unameparameter}"
    }
}

resource "aws_instance" "ICPEEL_PROD_EXSTREAM_COM_SERVER" {
    ami                         = "${var.ami_id_windows2016base}"
    vpc_security_group_ids      = [
        "${aws_security_group.SG_EXSTREAM.id}",
        "${aws_security_group.SG_AVIATRIX_RDP.id}"
    ]
    subnet_id                   = "${var.peel-prod-private-1a-subnet}"
    instance_type               = "r5a.large"
    key_name                    = "PeelOpenTextprod"
    associate_public_ip_address = false
    iam_instance_profile        = "CMS-SSMInstanceRole"
    user_data = "${data.template_file.PEEL_DOMAIN_JOIN_5.rendered}"
	ebs_optimized = "true"

    lifecycle {
        prevent_destroy = true
    }

    root_block_device {
        volume_size = "600"
    }

    tags {
        Owner = "robhatnagar@deloitte.ca"
        ManagedBy = "CMS-TF"
        SolutionName = "Insurcloud Peel"
        Environment = "Prod"
        ApplicationName = "opentext, exstream"
        DataClassification = "PII, PHI"
        DLM-Backup = "Daily-7"
        DLM-Retention = "90"
        PatchGroup = "Windows"
        VendorManaged = "False"
        ResourceType = "Client"
        ResourceSeverity = "Normal"
        Name = "A-MC-CAC-D-A003-PEEL-PROD-EXSTREAM-COMMUNICATIONS-WIN-EC2"
        AccountID = "${data.aws_caller_identity.current.account_id}"
        Role = ""
        CLIENT = "InsurCloud PEEL"
		Account = "PEEL-prod"
		Client = "Peel"
		KeyName = "PeelOpenTextprod"
    }
}

output "ICPEEL_PROD_EXSTREAM_COM_SERVER_PRIVATEIP" {
    value = "${aws_instance.ICPEEL_PROD_EXSTREAM_COM_SERVER.private_ip}"
}

data "template_file" "PEEL_DOMAIN_JOIN_12" {
    template = "${file("IC-Peel-PROD/windowsdata.tpl")}"

    vars {
        computer_name = "OT-MSSQL"
        region              = "${var.region}"
        domain              = "${var.ad_domain}"
        dns1                = "${var.ad_dns1}"
        dns2                = "${var.ad_dns2}"
        dns3                = "${var.ad_dns3}"
        ouselection         = "${var.ou_name}"
        pwordparameter      = "${var.pwordparameter}"
        unameparameter      = "${var.unameparameter}"
    }
}

resource "aws_instance" "ICPEEL_PROD_OT_MSSQL_DATABASE_1" {
    ami = "${var.ami_id_windows2012R2sql14standard}"
    vpc_security_group_ids      = [
        "${aws_security_group.SG_OT_MSSQL_DB.id}",
        "${aws_security_group.SG_AVIATRIX_RDP.id}"
    ]
    subnet_id = "${var.peel-prod-database-1a-subnet}"
    instance_type = "r4.xlarge"
    key_name    = "PeelOpenTextprod"
    associate_public_ip_address = false
    iam_instance_profile = "CMS-SSMInstanceRole"
    user_data = "${data.template_file.PEEL_DOMAIN_JOIN_12.rendered}"
	ebs_optimized = "true"

    lifecycle {
        prevent_destroy = true
    }

    root_block_device {
       volume_size = "200"
       delete_on_termination = false
    }

    tags {
        Owner = "robhatnagar@deloitte.ca"
        ManagedBy = "CMS-TF"
        SolutionName = "Insurcloud Peel"
        Environment = "Prod"
        ApplicationName = "database, mssql, opentext, exstream, content suite"
        DataClassification = "PII, PHI"
        DLM-Backup = "Daily-31"
        DLM-Retention = "90"
        PatchGroup = "Windows"
        VendorManaged = "False"
        OSName = "Windows"
        ResourceType = "Client"
        ResourceSeverity = "Normal"
        Name = "A-MC-CAC-D-A003-PEEL-PROD-OT-MSSQL-DB-1-WIN-EC2"
        AccountID = "${data.aws_caller_identity.current.account_id}"
        Role = ""
        CLIENT = "InsurCloud PEEL"
		Account = "PEEL-prod"
		Client = "Peel"
		KeyName = "PeelOpenTextprod"
    }    

    provisioner "local-exec" {
        command = "echo ${aws_instance.ICPEEL_PROD_OT_MSSQL_DATABASE_1.private_ip} >> prod_hosts.txt"
    } 
}

output "ICPEEL_PROD_OT_MSSQL_DATABASE_1_PRIVATEIP" {
    value = "${aws_instance.ICPEEL_PROD_OT_MSSQL_DATABASE_1.private_ip}"
}

######### JOBSCHEDULER ##############
resource "aws_instance" "ICPEEL_PROD_JOB_SCHEDULER" {
    ami = "${var.ami_id_amazonlinux2base}"
    vpc_security_group_ids      = [
        "${aws_security_group.SG_JOB_SCHEDULER.id}",
        "${aws_security_group.SG_AVIATRIX_SSH.id}",
        "${aws_security_group.SG_JENKINS_CLIENT_SSH.id}"
    ]
    subnet_id = "${var.peel-prod-private-1a-subnet}"
    instance_type = "t3a.medium"
    key_name    = "PeelOtherprod"
    associate_public_ip_address = false
    iam_instance_profile = "CMS-SSMInstanceRole"
	ebs_optimized = "true"

    root_block_device {
       volume_size = "40"
    }

    tags {
        Owner = "robhatnagar@deloitte.ca"
        ManagedBy = "CMS-TF"
        SolutionName = "Insurcloud Peel"
        Environment = "Prod"
        ApplicationName = "jobscheduler"
        DataClassification = "PII, PHI"
        DLM-Backup = "Daily-7"
        DLM-Retention = "90"
        PatchGroup = "AmazonLinux2"
        VendorManaged = "False"
        ResourceType = "Client"
        ResourceSeverity = "Normal"
        Name = "A-MC-CAC-D-A003-PEEL-PROD-JOB-SCHEDULER-LNX-EC2"
        AccountID = "${data.aws_caller_identity.current.account_id}"
        Role = "Container"
        CLIENT = "InsurCloud PEEL"
		Account = "PEEL-prod"
		Client = "Peel"
		KeyName = "PeelOtherprod"
    }

    provisioner "local-exec" {
        command = "echo ${aws_instance.ICPEEL_PROD_JOB_SCHEDULER.private_ip} >> prod_hosts.txt"
    }
}

output "ICPEEL_PROD_JOB_SCHEDULER_PRIVATEIP" {
  value = "${aws_instance.ICPEEL_PROD_JOB_SCHEDULER.private_ip}"
}

############# JENKINS CLIENT #################
resource "aws_instance" "ICPEEL_PROD_JENKINS_CLIENT" {
    ami = "${var.ami_id_amazonlinux2base}"
    vpc_security_group_ids = ["${aws_security_group.SG_JENKINS_CLIENT.id}"]
	subnet_id = "${var.peel-prod-private-1a-subnet}"
    instance_type = "t3a.small"
    key_name    = "PeelJenkinsprod"
    associate_public_ip_address = false
    iam_instance_profile = "CMS-SSMInstanceRole"
	ebs_optimized = "true"

	// lifecycle {
    //    prevent_destroy = true
	// }

	root_block_device {
       volume_size = "250"
	}


	tags {
		Owner = "robhatnagar@deloitte.ca"
		ManagedBy = "CMS-TF"
		SolutionName = "Insurcloud Peel"
		Environment = "Prod"
		ApplicationName = "jenkins, docker"
		DataClassification = "NA"
		DLM-Backup = "Daily-1"
		DLM-Retention = "90"
		PatchGroup = "AmazonLinux2"
        VendorManaged = "False"
		ResourceType = "Client"
		ResourceSeverity = "Normal"
		Name = "A-MC-CAC-D-A003-PEEL-PROD-JENKINS-CLIENT-EC2"
		AccountID = "${data.aws_caller_identity.current.account_id}"
		Role = "Container"
        CLIENT = "InsurCloud PEEL"
		Account = "PEEL-prod"
		Client = "Peel"
		KeyName = "PeelJenkinsprod"
	}
}

output "ICPEEL_PROD_JENKINS_CLIENT_PRIVATEIP" {
  value = "${aws_instance.ICPEEL_PROD_JENKINS_CLIENT.private_ip}"
}
