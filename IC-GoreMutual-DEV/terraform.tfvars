#Envrionment specific variables
client_name = "IC-GORE"
enviornment_name = "DEV"
enviornment_type = "NP"
client_number = "A005"
#ad-dc1 = "10.10.4.10"
#ad-dc2 = "10.10.6.10"
volume_key = "arn:aws:kms:ca-central-1:780193124257:key/d0768380-3010-45d6-91a8-1d485ba6932f"
ami_id_windows2012R2sql14enterprise = "ami-0b7e5050eee245b99"
ami_id_windows2012R2sql14standard = "ami-0913a2afb4fa5fc5b"
ami_id_windows2019container = "ami-013453540e1aa0d48"
ami_id_windows2019base = "ami-06b8f69a907cf6bb2"
ami_id_windows2016base = "ami-0f279f997ff6cb1f5"
ami_id_windows2012R2base = "ami-08f7dedaab95e4621"
ami_id_windows2008base = "ami-0446e08d2d99fc8c9"
ami_id_redhatlinux75 = "ami-05c33d286020a47f9"
ami_id_ubuntu1804baseimage = "ami-032172cc5f34b32fc"
ami_id_ubuntu1604baseimage = "ami-04f40cca01b3186ea"
ami_id_amazonlinux201712base = "ami-0a777bbf8ef3f43bf"
ami_id_amazonlinux201709base = "ami-058dba934995c5468"
ami_id_amazonlinux2base = "ami-04978aa25eaba28b7"
ami_id_amazonlinux201803base = "ami-06829694f407e15c1"
ami_id_amazonlinuxbase20200506 = "ami-071594d49d11fcb38"

#ami_id_windows_dev_infocenter_datahub_server = "ami-" # base is ami_id_windows2016base

#VPC Specific variables
vpc_id = "vpc-0a40bf35e48ae95c3"
vpc_cidr = "192.168.28.0/22"
account_cidr = "192.168.28.0/22"
#sg_cidr ="XXX"


gore-dev-database-1a-subnet = "subnet-061946218e3fb3ab6"
gore-dev-database-1b-subnet = "subnet-052bac1f162231053"
gore-dev-private-1a-subnet = "subnet-01382b01fdbbaf850"
gore-dev-private-1b-subnet = "subnet-0da963d9c18d850bd"

#RDS
mssql-rds-password = "2020SeaLion"
mysql-rds-password = "2020SeaLion"
postgres-rds-password = "2020SeaLion"

#Accounts
gore-dev-account = "780193124257"
gore-prod-account = "948423377884"
gore-hub-account = "963473599983"
