#Envrionment specific variables
client_name = "IC-Commonwell"
enviornment_name = "PROD"
enviornment_type = "NP"
client_number = "A003"
#ad-dc1 = "10.10.4.10"
#ad-dc2 = "10.10.6.10"
volume_key = "arn:aws:kms:ca-central-1:942574907772:key/cada27fc-e5f9-4f1c-b85a-3527eb3e75bb"
ami_id_amazonlinux201709base = "ami-0024f7a5443f60bce"
ami_id_redhatlinux75 = "ami-0331f65e26647e9f7"
ami_id_amazonlinux201803base = "ami-033eb978e7ec03b77"
ami_id_amazonlinux2base = "ami-03501a4a3ff25cc1c"
ami_id_amazonlinux2base20200506 = "ami-071594d49d11fcb38"
ami_id_windows2008base = "ami-03b6e09268e30fcca"
ami_id_amazonlinux201712base = "ami-03bcd20cb4070e488"
ami_id_windows2012R2base = "ami-0f2ef3ae4e127b482"
ami_id_windows2012SQLEnterprisebase = "ami-0da07cd61cb3b3518"
ami_id_windows2016base = "ami-01b912743b28aabf9"
ami_id_windows2019base = "ami-0abc1f78b97dd8f59"
ami_id_windows2019cont = "ami-067cc37f8ee2a83d0"
ami_id_ubuntu1604baseimage = "ami-0c5d6a28a2ade058b"
ami_id_ubuntu1804baseimage = "ami-0fa8da332170c5e8e"

#VPC Specific variables
vpc_id = "vpc-056e018cb30996b4e"
vpc_cidr = "10.10.48.0/20"


commonwell-prod-database-1a-subnet = "subnet-0bf6b8dec70c32bd5"
commonwell-prod-database-1b-subnet = "subnet-0b68da997a1d0109e"
commonwell-prod-guidewire-containers-1a-subnet = "subnet-0b429e66da43ba2d4"
commonwell-prod-guidewire-containers-1b-subnet = "subnet-07dd9abc97a305194"
commonwell-prod-guidewire-database-1a-subnet = "subnet-06dfebafff372b848"
commonwell-prod-guidewire-database-1b-subnet = "subnet-05745ff8929d5c5ca"
commonwell-prod-private-1a-subnet = "subnet-0e54a038f58a2c4cf"
commonwell-prod-private-1b-subnet = "subnet-01dadb102542c7a85"
commonwell-prod-spring-microservices-1a-subnet = "subnet-0b718b4f6fc33c1fd"
commonwell-prod-spring-microservices-1b-subnet = "subnet-0118a6e750a5851d4"

#RDS
mssql-rds-password = "6cPBHgP?86RD7*^P!UHL_nXc"
mysql-rds-password = "!2Nu!TLGaE%=e3phk=uf#+&a"
postgres-rds-password = "y?Rzv55+dKBGHWesRxgd&Upr"

#accounts
cmig-prod-account = "942574907772"

