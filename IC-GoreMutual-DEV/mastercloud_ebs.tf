#GoreEBS

# #=======================================================================
# # DEV01
# #=======================================================================

#Microservices
# ############ External HAProxy Volumes ##################
# resource "aws_volume_attachment" "ICGORE_DEV01_EXTERNAL_HAPROXY_DOCKER_EBS_ATTACHMENT" {
#   device_name = "/dev/sdh"
#   volume_id   = "${aws_ebs_volume.ICGORE_DEV01_EXTERNAL_HAPROXY_DOCKER_EBS.id}"
#   instance_id = "${aws_instance.ICGORE_DEV01_EXTERNAL_HAPROXY.id}"
# }

# resource "aws_ebs_volume" "ICGORE_DEV01_EXTERNAL_HAPROXY_DOCKER_EBS" {
#   availability_zone = "ca-central-1a"
#   size              = 20
#   encrypted         = true
#   kms_key_id        = "arn:aws:kms:ca-central-1:780193124257:key/d0768380-3010-45d6-91a8-1d485ba6932f"
#   tags = {
#     Name = "ICGORE_DEV01_EXTERNAL_HAPROXY_DOCKER_EBS"
#     Docker = "True"
#   }
# }


############ Internal HAProxy Volumes ##################
resource "aws_volume_attachment" "ICGORE_DEV01_INTERNAL_HAPROXY_DOCKER_EBS_ATTACHMENT" {
  device_name = "/dev/sdh"
  volume_id   = "${aws_ebs_volume.ICGORE_DEV01_INTERNAL_HAPROXY_DOCKER_EBS.id}"
  instance_id = "${aws_instance.ICGORE_DEV01_INTERNAL_HAPROXY.id}"
}

resource "aws_ebs_volume" "ICGORE_DEV01_INTERNAL_HAPROXY_DOCKER_EBS" {
  availability_zone = "ca-central-1a"
  size              = 20
  encrypted         = true
  kms_key_id        = "arn:aws:kms:ca-central-1:780193124257:key/d0768380-3010-45d6-91a8-1d485ba6932f"
  tags = {
    AccountID = "${var.gore-dev-account}"
    Client = "Gore"
    Name = "ICGORE_DEV01_INTERNAL_HAPROXY_DOCKER_EBS"
    Docker = "True"
  }
}

############ Microservices Volumes ##################
resource "aws_volume_attachment" "ICGORE_DEV01_MICROSERVICES_DOCKER_EBS_ATTACHMENT" {
  device_name = "/dev/sdh"
  volume_id   = "${aws_ebs_volume.ICGORE_DEV01_MICROSERVICES_DOCKER_EBS.id}"
  instance_id = "${aws_instance.ICGORE_DEV01_MICROSERVICES.id}"
}

resource "aws_ebs_volume" "ICGORE_DEV01_MICROSERVICES_DOCKER_EBS" {
  availability_zone = "ca-central-1a"
  size              = 50
  encrypted         = true
  kms_key_id        = "arn:aws:kms:ca-central-1:780193124257:key/d0768380-3010-45d6-91a8-1d485ba6932f"
  tags = {
    AccountID = "${var.gore-dev-account}"
    Client = "Gore"
    Name = "ICGORE_DEV01_MICROSERVICES_DOCKER_EBS"
    Docker = "True"
  }
}


# #=======================================================================
# # PPS/DEV/QA
# #=======================================================================
############ Jenkins Client ##################
resource "aws_volume_attachment" "ICGORE_DEV_JENKINS_CLIENT_DOCKER_EBS_ATTACHMENT" {
  device_name = "/dev/sdh"
  volume_id   = "${aws_ebs_volume.ICGORE_DEV_JENKINS_CLIENT_DOCKER_EBS.id}"
  instance_id = "${aws_instance.ICGORE_DEV_JENKINS_CLIENT.id}"
}

resource "aws_ebs_volume" "ICGORE_DEV_JENKINS_CLIENT_DOCKER_EBS" {
  availability_zone = "${aws_instance.ICGORE_DEV_JENKINS_CLIENT.availability_zone}"
  size              = 50
  type              = "gp2"
  encrypted         = true
  kms_key_id        = "arn:aws:kms:ca-central-1:780193124257:key/d0768380-3010-45d6-91a8-1d485ba6932f"

  tags = {
    AccountID = "${var.gore-dev-account}"
    Client = "Gore"
    Name          = "ICGORE_DEV_JENKINS_CLIENT_DOCKER_EBS"
    Docker        = "True"
  }
}


# #=======================================================================
# # DEV02
# #=======================================================================

#Microservices
# ############ External HAProxy Volumes ##################
# resource "aws_volume_attachment" "ICGORE_DEV01_EXTERNAL_HAPROXY_DOCKER_EBS_ATTACHMENT" {
#   device_name = "/dev/sdh"
#   volume_id   = "${aws_ebs_volume.ICGORE_DEV01_EXTERNAL_HAPROXY_DOCKER_EBS.id}"
#   instance_id = "${aws_instance.ICGORE_DEV01_EXTERNAL_HAPROXY.id}"
# }

# resource "aws_ebs_volume" "ICGORE_DEV01_EXTERNAL_HAPROXY_DOCKER_EBS" {
#   availability_zone = "ca-central-1a"
#   size              = 20
#   encrypted         = true
#   kms_key_id        = "arn:aws:kms:ca-central-1:780193124257:key/d0768380-3010-45d6-91a8-1d485ba6932f"
#   tags = {
#     Name = "ICGORE_DEV01_EXTERNAL_HAPROXY_DOCKER_EBS"
#     Docker = "True"
#   }
# }


############ Internal HAProxy Volumes ##################
resource "aws_volume_attachment" "ICGORE_DEV02_INTERNAL_HAPROXY_DOCKER_EBS_ATTACHMENT" {
  device_name = "/dev/sdh"
  volume_id   = "${aws_ebs_volume.ICGORE_DEV02_INTERNAL_HAPROXY_DOCKER_EBS.id}"
  instance_id = "${aws_instance.ICGORE_DEV02_INTERNAL_HAPROXY.id}"
}

resource "aws_ebs_volume" "ICGORE_DEV02_INTERNAL_HAPROXY_DOCKER_EBS" {
  availability_zone = "ca-central-1a"
  size              = 20
  encrypted         = true
  kms_key_id        = "arn:aws:kms:ca-central-1:780193124257:key/d0768380-3010-45d6-91a8-1d485ba6932f"
  tags = {
    AccountID = "${var.gore-dev-account}"
    Client = "Gore"
    Name = "ICGORE_DEV02_INTERNAL_HAPROXY_DOCKER_EBS"
    Docker = "True"
  }
}

############ Microservices Volumes ##################
resource "aws_volume_attachment" "ICGORE_DEV02_MICROSERVICES_DOCKER_EBS_ATTACHMENT" {
  device_name = "/dev/sdh"
  volume_id   = "${aws_ebs_volume.ICGORE_DEV02_MICROSERVICES_DOCKER_EBS.id}"
  instance_id = "${aws_instance.ICGORE_DEV02_MICROSERVICES.id}"
}

resource "aws_ebs_volume" "ICGORE_DEV02_MICROSERVICES_DOCKER_EBS" {
  availability_zone = "ca-central-1a"
  size              = 50
  encrypted         = true
  kms_key_id        = "arn:aws:kms:ca-central-1:780193124257:key/d0768380-3010-45d6-91a8-1d485ba6932f"
  tags = {
    AccountID = "${var.gore-dev-account}"
    Client = "Gore"
    Name = "ICGORE_DEV02_MICROSERVICES_DOCKER_EBS"
    Docker = "True"
  }
}


# #=======================================================================
# # QA01
# #=======================================================================

#Microservices
# ############ External HAProxy Volumes ##################
# resource "aws_volume_attachment" "ICGORE_DEV01_EXTERNAL_HAPROXY_DOCKER_EBS_ATTACHMENT" {
#   device_name = "/dev/sdh"
#   volume_id   = "${aws_ebs_volume.ICGORE_DEV01_EXTERNAL_HAPROXY_DOCKER_EBS.id}"
#   instance_id = "${aws_instance.ICGORE_DEV01_EXTERNAL_HAPROXY.id}"
# }

# resource "aws_ebs_volume" "ICGORE_DEV01_EXTERNAL_HAPROXY_DOCKER_EBS" {
#   availability_zone = "ca-central-1a"
#   size              = 20
#   encrypted         = true
#   kms_key_id        = "arn:aws:kms:ca-central-1:780193124257:key/d0768380-3010-45d6-91a8-1d485ba6932f"
#   tags = {
#     Name = "ICGORE_DEV01_EXTERNAL_HAPROXY_DOCKER_EBS"
#     Docker = "True"
#   }
# }


############ Internal HAProxy Volumes ##################
resource "aws_volume_attachment" "ICGORE_QA01_INTERNAL_HAPROXY_DOCKER_EBS_ATTACHMENT" {
  device_name = "/dev/sdh"
  volume_id   = "${aws_ebs_volume.ICGORE_QA01_INTERNAL_HAPROXY_DOCKER_EBS.id}"
  instance_id = "${aws_instance.ICGORE_QA01_INTERNAL_HAPROXY.id}"
}

resource "aws_ebs_volume" "ICGORE_QA01_INTERNAL_HAPROXY_DOCKER_EBS" {
  availability_zone = "ca-central-1a"
  size              = 20
  encrypted         = true
  kms_key_id        = "arn:aws:kms:ca-central-1:780193124257:key/d0768380-3010-45d6-91a8-1d485ba6932f"
  tags = {
    AccountID = "${var.gore-dev-account}"
    Client = "Gore"
    Name = "ICGORE_QA01_INTERNAL_HAPROXY_DOCKER_EBS"
    Docker = "True"
  }
}

############ Microservices Volumes ##################
resource "aws_volume_attachment" "ICGORE_QA01_MICROSERVICES_DOCKER_EBS_ATTACHMENT" {
  device_name = "/dev/sdh"
  volume_id   = "${aws_ebs_volume.ICGORE_QA01_MICROSERVICES_DOCKER_EBS.id}"
  instance_id = "${aws_instance.ICGORE_QA01_MICROSERVICES.id}"
}

resource "aws_ebs_volume" "ICGORE_QA01_MICROSERVICES_DOCKER_EBS" {
  availability_zone = "ca-central-1a"
  size              = 50
  encrypted         = true
  kms_key_id        = "arn:aws:kms:ca-central-1:780193124257:key/d0768380-3010-45d6-91a8-1d485ba6932f"
  tags = {
    AccountID = "${var.gore-dev-account}"
    Client = "Gore"
    Name = "ICGORE_QA01_MICROSERVICES_DOCKER_EBS"
    Docker = "True"
  }
}
