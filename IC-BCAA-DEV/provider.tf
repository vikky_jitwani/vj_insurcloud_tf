provider "aws" {
  region     = "ca-central-1"
  version = "~> 2.26"
}

terraform {
 backend "s3" {
    bucket     = "a-c-mc-p-a010-bcaa-tfstate"
    key        = "IC-BCAA-DEV/STATE/current.tf"
    region     = "ca-central-1"
    # role_arn   = "arn:aws:iam::393766723611:role/admin_ops_auto"
	  #encrypt    = true
	  #kms_key_id = "arn:aws:kms:ca-central-1:532382648425:key/e61c5b45-c317-468d-af56-07e566f3c870"
  }
}

