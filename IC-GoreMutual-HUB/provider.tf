provider "aws" {
  region     = "ca-central-1"
}

terraform {
 backend "s3" {
    bucket     = "a-c-mc-p-a004-goremutual-tfstate"
    key        = "IC-GoreMutual-HUB/STATE/current.tf"
    region     = "ca-central-1"
  }
}

data "terraform_remote_state" "dev_remote_state" {
  backend = "s3"
  config = {
    bucket     = "a-c-mc-p-a004-goremutual-tfstate"
    key        = "IC-GoreMutual-DEV/STATE/current.tf"
    region     = "ca-central-1"
  }
}

data "terraform_remote_state" "prod_remote_state" {
  backend = "s3"
  config = {
    bucket     = "a-c-mc-p-a004-goremutual-tfstate"
    key        = "IC-GoreMutual-PROD/STATE/current.tf"
    region     = "ca-central-1"
  }
}