# ---------------------------------------------------------------------------------------------------------------------
# DEPLOY A VAULT SERVER CLUSTER, AN ELB, AND A CONSUL SERVER CLUSTER IN AWS
# This is an example of how to use the vault-cluster and vault-elb modules to deploy a Vault cluster in AWS with an
# Elastic Load Balancer (ELB) in front of it. This cluster uses Consul, running in a separate cluster, as its storage
# backend.
# ---------------------------------------------------------------------------------------------------------------------

# Terraform 0.9.5 suffered from https://github.com/hashicorp/terraform/issues/14399, which causes this template the
# conditionals in this template to fail.
terraform {
  required_version = ">= 0.9.3, != 0.9.5"
}

locals {
  vault_cluster_name = "${var.cluster_name_prefix}-${upper(var.environment)}-VAULT"
  vault_domain_name = "vault.${var.environment}.${var.hosted_zone_domain_name}"
  consul_module_version = "v0.6.1"
}

# ---------------------------------------------------------------------------------------------------------------------
# AUTOMATICALLY LOOK UP THE LATEST PRE-BUILT AMI
# This repo contains a CircleCI job that automatically builds and publishes the latest AMI by building the Packer
# template at /examples/vault-consul-ami upon every new release. The Terraform data source below automatically looks up
# the latest AMI so that a simple "terraform apply" will just work without the user needing to manually build an AMI and
# fill in the right value.
#
# !! WARNING !! These example AMIs are meant only convenience when initially testing this repo. Do NOT use these example
# AMIs in a production setting as those TLS certificate files are publicly available from the Module repo containing
# this code.
#
# NOTE: This Terraform data source must return at least one AMI result or the entire template will fail. See
# /_ci/publish-amis-in-new-account.md for more information.
# ---------------------------------------------------------------------------------------------------------------------
data "aws_ami" "vault" {
  most_recent = true
  owners = ["self"]

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  filter {
    name   = "is-public"
    values = ["false"]
  }

  filter {
    name   = "name"
    values = ["ic_vault-${var.environment}-*"]
  }
}

# ---------------------------------------------------------------------------------------------------------------------
# CREATE KMS AUTO UNSEAL KEY
# ---------------------------------------------------------------------------------------------------------------------

resource "aws_kms_key" "vault" {
  description             = "${title(var.environment)} Vault Auto Unseal Key"
  deletion_window_in_days = 30
  enable_key_rotation     = true

  # Tags
  tags = {
    "Environment" = "${var.environment}"
    "Application" = "${local.vault_cluster_name}"
  }
}

resource "aws_kms_alias" "vault" {
  name          = "alias/${title(var.environment)}-Vault_Key"
  target_key_id = "${aws_kms_key.vault.key_id}"
}

# ---------------------------------------------------------------------------------------------------------------------
# DEPLOY THE VAULT SERVER CLUSTER
# ---------------------------------------------------------------------------------------------------------------------

module "vault_cluster" {
  # When using these modules in your own templates, you will need to use a Git URL with a ref attribute that pins you
  # to a specific version of the modules, such as the following example:
  #source = "github.com/hashicorp/terraform-aws-vault//modules/vault-cluster?ref=v0.12.2"
  source = "./modules/vault-cluster"

  cluster_name  = "${local.vault_cluster_name}"
  cluster_size  = "${var.vault_cluster_size}"
  instance_type = "${var.vault_instance_type}"

  ami_id    = "${data.aws_ami.vault.id}"
  user_data = "${data.template_file.user_data_vault_cluster.rendered}"

  vpc_id     = "${data.aws_vpc.selected.id}"
  subnet_ids = "${data.aws_subnet_ids.selected.ids}"
  api_port   = "${var.api_port}"

  # Do NOT use the ELB for the ASG health check, or the ASG will assume all sealed instances are unhealthy and
  # repeatedly try to redeploy them.
  health_check_type = "EC2"

  enable_auto_unseal = true
  auto_unseal_kms_key_arn = "${aws_kms_key.vault.arn}"
  cross_account_assumed_role = "${var.cross_account_assumed_role}"
  permissions_boundary = "${var.permissions_boundary}"

  # To make testing easier, we allow requests from any IP address here but in a production deployment, we *strongly*
  # recommend you limit this to the IP address ranges of known, trusted servers inside your VPC.

  allowed_ssh_cidr_blocks              = "${var.allowed_ssh_cidr_blocks}"
  allowed_inbound_cidr_blocks          = "${var.allowed_inbound_cidr_blocks}"
  allowed_inbound_security_group_ids   = []
  allowed_inbound_security_group_count = 0
  ssh_key_name                         = "${var.ssh_key_name}"
}

# ---------------------------------------------------------------------------------------------------------------------
# ATTACH IAM POLICIES FOR CONSUL
# To allow our Vault servers to automatically discover the Consul servers, we need to give them the IAM permissions from
# the Consul AWS Module's consul-iam-policies module.
# ---------------------------------------------------------------------------------------------------------------------

module "consul_iam_policies_servers" {
  source = "github.com/hashicorp/terraform-aws-consul//modules/consul-iam-policies?ref=v0.6.1"

  iam_role_id = "${module.vault_cluster.iam_role_id}"
}

# ---------------------------------------------------------------------------------------------------------------------
# ADDS A POLICY TO THE VAULT CLUSTER ROLE SO VAULT CAN QUERY AWS IAM USERS AND ROLES
# ---------------------------------------------------------------------------------------------------------------------

resource "aws_iam_role_policy" "vault_iam" {
  name   = "vault_iam"
  role   = "${module.vault_cluster.iam_role_id}"
  policy = "${data.aws_iam_policy_document.vault_iam.json}"
}

data "aws_iam_policy_document" "vault_iam" {
  statement {
    effect  = "Allow"
    actions = ["iam:GetRole", "iam:GetUser"]

    # List of arns it can query, for more security, it could be set to specific roles or user
    # resources = ["${aws_iam_role.example_instance_role.arn}"]
    resources = [
      "arn:aws:iam::*:user/*",
      "arn:aws:iam::*:role/*",
    ]
  }

  statement {
    effect    = "Allow"
    actions   = ["sts:GetCallerIdentity"]
    resources = ["*"]
  }

  # Cross Account Access policy
  statement {
    effect = "Allow",
    actions = ["sts:AssumeRole"],
    resources = "${var.cross_account_assumed_role}"
  }
}

resource "aws_iam_role_policy_attachment" "patching_policy" {
  role      = "${module.vault_cluster.iam_role_name}"
  policy_arn = "${var.patching_policy_arn}"
}

# ---------------------------------------------------------------------------------------------------------------------
# THE USER DATA SCRIPT THAT WILL RUN ON EACH VAULT SERVER WHEN IT'S BOOTING
# This script will configure and start Vault
# ---------------------------------------------------------------------------------------------------------------------

data "template_file" "user_data_vault_cluster" {
  template = "${file("${path.module}/run-vault.sh")}"

  vars {
    aws_region               = "${data.aws_region.current.name}"
    consul_cluster_tag_key   = "${var.consul_cluster_tag_key}"
    consul_cluster_tag_value = "${var.consul_cluster_tag_value}"

    kms_key_id = "${aws_kms_key.vault.key_id}"
  }
}

# ---------------------------------------------------------------------------------------------------------------------
# PERMIT CONSUL SPECIFIC TRAFFIC IN VAULT CLUSTER
# To allow our Vault servers consul agents to communicate with other consul agents and participate in the LAN gossip,
# we open up the consul specific protocols and ports for consul traffic
# ---------------------------------------------------------------------------------------------------------------------

module "security_group_rules" {
  source = "github.com/hashicorp/terraform-aws-consul.git//modules/consul-client-security-group-rules?ref=v0.6.1"

  security_group_id = "${module.vault_cluster.security_group_id}"

  # To make testing easier, we allow requests from any IP address here but in a production deployment, we *strongly*
  # recommend you limit this to the IP address ranges of known, trusted servers inside your VPC.

  allowed_inbound_cidr_blocks = "${var.allowed_inbound_cidr_blocks}"
}

# ---------------------------------------------------------------------------------------------------------------------
# DEPLOY THE ELB
# ---------------------------------------------------------------------------------------------------------------------
module "vault_nlb" {
  source = "../modules/nlb"
  name = "${local.vault_cluster_name}"

  vpc_id     = "${data.aws_vpc.selected.id}"
  subnet_ids = "${data.aws_subnet_ids.selected.ids}"
  internal   = "true"

  # Associate the ELB with the instances created by the consul Autoscaling group
  asg_name     = "${module.vault_cluster.asg_name}"
  api_protocol = "${var.api_protocol}"
  api_port     = "${var.api_port}"
  lb_port      = "${var.lb_port}"

  # In order to access consul over HTTPS, we need a domain name that matches the TLS cert
  create_dns_entry = "${var.create_dns_entry}"

  # Terraform conditionals are not short-circuiting, so we use join as a workaround to avoid errors when the
  # aws_route53_zone data source isn't actually set: https://github.com/hashicorp/hil/issues/50
  hosted_zone_id = "${var.create_dns_entry ? join("", data.aws_route53_zone.selected.*.zone_id) : ""}"

  domain_name = "${local.vault_domain_name}"
}

# Look up the Route 53 Hosted Zone by domain name
data "aws_route53_zone" "selected" {
  count = "${var.create_dns_entry}"
  name  = "${var.hosted_zone_domain_name}."

  vpc_id = "${data.aws_vpc.selected.id}"
}

# ---------------------------------------------------------------------------------------------------------------------
# DEPLOY THE CLUSTERS IN THE DEFAULT VPC AND AVAILABILITY ZONES
# Using the default VPC and subnets makes this example easy to run and test, but it means Consul and Vault are
# accessible from the public Internet. In a production deployment, we strongly recommend deploying into a custom VPC
# and private subnets. Only the ELB should run in the public subnets.
# ---------------------------------------------------------------------------------------------------------------------

data "aws_vpc" "selected" {
  default = "false"

  filter {
    name   = "tag:Name"
    values = "${var.vpc_name_filter}"
  }
}

data "aws_subnet_ids" "selected" {
  vpc_id = "${data.aws_vpc.selected.id}"

  filter {
    name   = "tag:Name"
    values = "${var.subnet_name_filter}"
  }
}

data "aws_region" "current" {}