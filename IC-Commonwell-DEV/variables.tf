variable "client_name" {
  description = "AWS client name for resources (Ex: MASTERCLOUD)"
  default = ""
}

variable "enviornment_name" {
  description = "AWS enviornment name for resources (Ex: NON-PROD)"
  default = ""
}

variable "enviornment_type" {
  description = "AWS enviornment type for resources (Ex: P=PROD/HUB, D=NON-PROD)"
  default = ""
}

variable "client_number" {
  description = "AWS client number for tagging (Ex: A001)"
  default = "A003"
}

variable "region" {
  description = "AWS region for hosting our your network"
  default = "ca-central-1"
}

variable "volume_key" {
  description = "KMS volume key hub account"
  default = ""
}

variable "ami_id_amazonlinux201709base" {
  description = "Amazon Linux Encypted Root Volume AMI"
  default = ""
}

variable "ami_id_redhatlinux75" {
  description = "Redhat Linux Encypted Root Volume AMI"
  default = ""
}

variable "ami_id_amazonlinux201803base" {
  description = "Amazon Linux 2018 03 Encypted Root Volume AMI"
  default = ""
}

variable "ami_id_amazonlinux2base" {
  description = "Amazon Linux 2 Base Encypted Root Volume AMI"
  default = ""
}

variable "ami_id_windows2008base" {
  description = "Windows 2008 Base Encypted Root Volume AMI"
  default = ""
}

variable "ami_id_amazonlinux201712base" {
  description = "Amazon Linux 2017 12 Base Encypted Root Volume AMI"
  default = ""
}

variable "ami_id_windows2012base" {
  description = "Windows 2012 Base Encypted Root Volume AMI with SQL 2014 Express"
  default = ""
}

variable "ami_id_windows2012_otdb" {
  description = "base windows 2012 image for otdb"
  default = ""
}

variable "ami_id_windows2012R2base" {
  description = "Windows 2012 R2 Base Encypted Root Volume AMI"
  default = ""
}


variable "ami_id_windows2012SQLEnterprisebase" {
  description = "Windows 2012 RTM Base Encypted Root Volume AMI with SQL 2014 Enterprise"
  default = ""
}

variable "ami_id_windows2012R2SQLExpressbase" {
  description = "Windows 2012 R2 RTM Base Encypted Root Volume AMI with SQL 2014 Express"
  default = ""
}

variable "ami_id_windows2016base" {
  description = "Windows 2016 Base Encypted Root Volume AMI"
  default = ""
}

variable "ami_id_windows2019base" {
  description = "Windows 2019 Base Encypted Root Volume AMI"
  default = ""
}

variable "ami_id_windows2019cont" {
  description = "Windows 2019 Base with containers Encypted Root Volume AMI"
  default = ""
}

variable "ami_id_ubuntu1604baseimage" {
  description = "Ubuntu 16 04 Base Encypted Root Volume AMI"
  default = ""
}

variable "ami_id_ubuntu1804baseimage" {
  description = "Ubuntu 18 04 Base Encypted Root Volume AMI"
  default = ""
}

variable "ami_id_windows_dev_infocenter_datahub_server" {
  description = "InfoCenter & Datahub Base Image for Dev. Based on ami_id_windows2016base"
  default = ""
}

variable "ami_id_windows_sap_db" {
  description = "InfoCenter & Datahub DB from restore"
  default = ""
}

variable "ami_id_" {
  description = "Ubuntu 18 04 Base Encypted Root Volume AMI"
  default = ""
}
variable "vpc_id" {
  description = "VPC id for DEV account"
  default = ""
}

variable "vpc_cidr" {
  description = "VPC cidr dev account"
  default = ""
}

variable "commonwell-non-prod-database-1a-subnet" {
  description = "Database subnet one"
  default = ""
}

variable "commonwell-non-prod-database-1b-subnet" {
  description = "Database subnet two"
  default = ""
}

variable "commonwell-non-prod-guidewire-containers-1a-subnet" {
  description = "Guidewire containers Subnet One"
  default = ""
}

variable "commonwell-non-prod-guidewire-containers-1b-subnet" {
  description = "Guidewire Containers Subnet Two"
  default = ""
}

variable "commonwell-non-prod-guidewire-database-1a-subnet" {
  description = "Guidewire Database Subnet One"
  default = ""
}

variable "commonwell-non-prod-guidewire-database-1b-subnet" {
  description = "Guidewire Database Subnet two"
  default = ""
}

variable "commonwell-non-prod-private-1a-subnet" {
  description = "Private Subnet one"
  default = ""
}

variable "commonwell-non-prod-private-1b-subnet" {
  description = "Private Subnet Two"
  default = ""

}

variable "commonwell-non-prod-spring-microservices-1a-subnet" {
  description = "Spring Microservices Subnet One"
  default = ""
}

variable "commonwell-non-prod-spring-microservices-1b-subnet" {
  description = "Spring Microservices Subnet Two"
  default = ""
}

variable "commonwell-non-prod-workspaces-1a-subnet" {
  description = "Workspaces Subnet One"
  default = ""
}

variable "commonwell-non-prod-workspaces-1b-subnet" {
  description = "Workspaces Subnet Two"
  default = ""
}
variable "default_cidr" {
  description = "default cidr used"
  default = ["10.0.0.0/8"]
}

variable "hub_cidr" {
  default = "10.10.4.0/22"
}

variable "ichub_cidr" {
  description = "cidr for ichub"
  type = "list"
  default = ["10.15.16.0/22"]
}


variable "deloitte_cidr" {
    default = "10.10.64.0/20"
}

variable "commonwell_aws_cidr" {
    default = "172.31.0.0/16"
}

variable "commonwell_lindsay_cidr" {
    default = "10.19.0.0/16"
}

variable "commonwell_whitby_cidr" {
    default = "10.20.0.0/16"
}

variable "mssql-rds-password" {
    default = "Canada@123"
}
variable "mysql-rds-password" {
    default = "Canada@123"
}

variable "postgres-rds-password" {
    default = "Canada@123"
}

data "aws_caller_identity" "current" {}

variable "commonwell_aviatrix_vpn_1a" {
    default = "10.10.5.104/32"
}
variable "commonwell_aviatrix_vpn_1b" {
    default = "10.10.7.78/32"
}
variable "cmig-dev-account"{
  default = ""
}