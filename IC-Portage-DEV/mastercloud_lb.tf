# #=======================================================================
# # DEV01
# #=======================================================================
##### ALBs #####

##### NLBs ####################
########## Internal ###########
resource "aws_lb" "ICPORTAGE_DEV01_INTERNAL_NLB" {
  name                             = "ICPORTAGE-DEV01-INTERNAL-NLB"
  internal                         = true
  load_balancer_type               = "network"
  enable_cross_zone_load_balancing = false

  subnets = [
    "${var.portage-dev-private-1a-subnet}",
    "${var.portage-dev-private-1b-subnet}",
  ]

  tags = {
    Name = "ICPORTAGE-DEV01-INTERNAL-NLB"
  }
}

resource "aws_lb_target_group" "ICPORTAGE_DEV01_INTERNAL_NLB_HAPROXY_TARGET_GROUP" {
  name     = "ICPORTAGE-DEV01-HAPROXY-INT-NLB"
  port     = 80
  protocol = "TCP"
  vpc_id   = "${var.vpc_id}"

  health_check {
    protocol = "TCP"
    port     = 8444
  }

  tags = {
    Name = "ICPORTAGE-DEV01-HAPROXY-INTERNAL-NLB-TARGET-GROUP"
  }
}

resource "aws_lb_listener" "ICPORTAGE_DEV01_INTERNAL_NLB_HAPROXY_LISTENER" {
  load_balancer_arn = "${aws_lb.ICPORTAGE_DEV01_INTERNAL_NLB.arn}"
  port              = "80"
  protocol          = "TCP"

  default_action {
    type             = "forward"
    target_group_arn = "${aws_lb_target_group.ICPORTAGE_DEV01_INTERNAL_NLB_HAPROXY_TARGET_GROUP.arn}"
  }
}

resource "aws_lb_target_group_attachment" "ICPORTAGE_DEV01_INTERNAL_NLB_TARGET_GROUP_ATTACHMENT_HAPROXY" {
  target_group_arn = "${aws_lb_target_group.ICPORTAGE_DEV01_INTERNAL_NLB_HAPROXY_TARGET_GROUP.arn}"
  target_id        = "${aws_instance.ICPORTAGE_DEV01_INTERNAL_HAPROXY.id}"
  port             = 80
}

output "ICPORTAGE_DEV01_INTERNAL_NLB_DNS_NAME" {
  value = "${aws_lb.ICPORTAGE_DEV01_INTERNAL_NLB.dns_name}"
}

output "ICPORTAGE_DEV01_INTERNAL_NLB_ZONE_ID" {
  value = "${aws_lb.ICPORTAGE_DEV01_INTERNAL_NLB.zone_id}"
}

# Not being used in Dev01 yet
########## External ###########
# resource "aws_lb" "ICPORTAGE_DEV01_EXTERNAL_NLB" {
#     name               = "ICPORTAGE-DEV01-EXTERNAL-NLB"
#     internal           = true
#     load_balancer_type = "network"
#     enable_cross_zone_load_balancing = false
#     subnets            = [
#         "${var.portage-dev-private-1a-subnet}",
#         "${var.portage-dev-private-1b-subnet}"
#     ] # Changing this value will force a recreation of the resource

#     tags = {
#         Name = "ICPORTAGE-DEV01-EXTERNAL-NLB"
#     }
# }

# output "ICPORTAGE_DEV01_EXTERNAL_NLB_DNS_NAME" {
#   value = "${aws_lb.ICPORTAGE_DEV01_EXTERNAL_NLB.dns_name}"
# }

# output "ICPORTAGE_DEV01_EXTERNAL_NLB_ZONE_ID" {
#   value = "${aws_lb.ICPORTAGE_DEV01_EXTERNAL_NLB.zone_id}"
# }

# resource "aws_lb_target_group" "ICPORTAGE_DEV01_EXTERNAL_NLB_HAPROXY_TARGET_GROUP" {
#     name     = "ICPORTAGE-DEV01-HAPROXY-EXT-NLB"
#     port     = 80
#     protocol = "TCP"
#     vpc_id   = "${var.vpc_id}"

#     health_check {
#         protocol = "TCP"
#         port = 8444
#     }

#     tags = {
#         Name = "ICPORTAGE-DEV01-EXTERNAL-NLB-HAPROXY-TARGET-GROUP"
#     }
# }

# resource "aws_lb_listener" "ICPORTAGE_DEV01_EXTERNAL_NLB_HAPROXY_LISTENER" {
#     load_balancer_arn = "${aws_lb.ICPORTAGE_DEV01_EXTERNAL_NLB.arn}"
#     port              = "80"
#     protocol          = "TCP"

#     default_action {
#         type             = "forward"
#         target_group_arn = "${aws_lb_target_group.ICPORTAGE_DEV01_EXTERNAL_NLB_HAPROXY_TARGET_GROUP.arn}"
#     }
# }

# resource "aws_lb_target_group_attachment" "ICPORTAGE_DEV01_EXTERNAL_NLB_TARGET_GROUP_ATTACHMENT_HAPROXY" {
#     target_group_arn = "${aws_lb_target_group.ICPORTAGE_DEV01_EXTERNAL_NLB_HAPROXY_TARGET_GROUP.arn}"
#     target_id        = "${aws_instance.ICPORTAGE_DEV01_EXTERNAL_HAPROXY.id}"
#     port             = 80
# }

/*
# #=======================================================================
# # PPS
# #=======================================================================
##### ALBs #####

##### NLBs ####################
########## Internal ###########
resource "aws_lb" "ICPORTAGE_PPS_INTERNAL_NLB" {
    name = "ICPORTAGE-PPS-INTERNAL-NLB"
    internal           = true
    load_balancer_type = "network"
    enable_cross_zone_load_balancing = false
    subnets            = [
        "${var.portage-dev-private-1a-subnet}",
        "${var.portage-dev-private-1b-subnet}"
    ]

    tags = {
        Name = "ICPORTAGE-PPS-INTERNAL-NLB"
    }
}

resource "aws_lb_target_group" "ICPORTAGE_PPS_INTERNAL_NLB_HAPROXY_TARGET_GROUP" {
    name     = "ICPORTAGE-PPS-HAPROXY-INT-NLB"
    port     = 443
    protocol = "TCP"
    vpc_id   = "${var.vpc_id}"

    health_check {
        protocol = "TCP"
        port = 8444
    }

    tags = {
        Name = "ICPORTAGE-PPS-HAPROXY-INTERNAL-NLB-TARGET-GROUP"
    }
}

resource "aws_lb_listener" "ICPORTAGE_PPS_INTERNAL_NLB_HAPROXY_LISTENER" {
    load_balancer_arn = "${aws_lb.ICPORTAGE_PPS_INTERNAL_NLB.arn}"
    port              = "443"
    protocol          = "TCP"

    default_action {
        type             = "forward"
        target_group_arn = "${aws_lb_target_group.ICPORTAGE_PPS_INTERNAL_NLB_HAPROXY_TARGET_GROUP.arn}"
    }
}


resource "aws_lb_target_group_attachment" "ICPORTAGE_PPS_INTERNAL_NLB_TARGET_GROUP_ATTACHMENT_HAPROXY_1" {
    target_group_arn = "${aws_lb_target_group.ICPORTAGE_PPS_INTERNAL_NLB_HAPROXY_TARGET_GROUP.arn}"
    target_id        = "${aws_instance.ICPORTAGE_PPS_INTERNAL_HAPROXY_1.id}"
    port             = 443
}

resource "aws_lb_target_group_attachment" "ICPORTAGE_PPS_INTERNAL_NLB_TARGET_GROUP_ATTACHMENT_HAPROXY_2" {
    target_group_arn = "${aws_lb_target_group.ICPORTAGE_PPS_INTERNAL_NLB_HAPROXY_TARGET_GROUP.arn}"
    target_id        = "${aws_instance.ICPORTAGE_PPS_INTERNAL_HAPROXY_2.id}"
    port             = 443
}

output "ICPORTAGE_PPS_INTERNAL_NLB_DNS_NAME" {
    value = "${aws_lb.ICPORTAGE_PPS_INTERNAL_NLB.dns_name}"
}

output "ICPORTAGE_PPS_INTERNAL_NLB_ZONE_ID" {
    value = "${aws_lb.ICPORTAGE_PPS_INTERNAL_NLB.zone_id}"
}

########## External ###########
resource "aws_lb" "ICPORTAGE_PPS_EXTERNAL_NLB" {
    name               = "ICPORTAGE-PPS-EXTERNAL-NLB"
    internal           = true
    load_balancer_type = "network"
    enable_cross_zone_load_balancing = false
    subnets            = [
        "${var.portage-dev-private-1a-subnet}",
        "${var.portage-dev-private-1b-subnet}"
    ] # Changing this value will force a recreation of the resource

    tags = {
        Name = "ICPORTAGE-PPS-EXTERNAL-NLB"
    }
}

output "ICPORTAGE_PPS_EXTERNAL_NLB_DNS_NAME" {
  value = "${aws_lb.ICPORTAGE_PPS_EXTERNAL_NLB.dns_name}"
}

output "ICPORTAGE_PPS_EXTERNAL_NLB_ZONE_ID" {
  value = "${aws_lb.ICPORTAGE_PPS_EXTERNAL_NLB.zone_id}"
}

resource "aws_lb_target_group" "ICPORTAGE_PPS_EXTERNAL_NLB_HAPROXY_TARGET_GROUP" {
    name     = "ICPORTAGE-PPS-HAPROXY-EXTERNAL-NLB"
    port     = 443
    protocol = "TCP"
    vpc_id   = "${var.vpc_id}"

    health_check {
        protocol = "TCP"
        port = 8444
    }

    tags = {
        Name = "ICPORTAGE-PPS-EXTERNAL-NLB-HAPROXY-TARGET-GROUP"
    }
}

resource "aws_lb_listener" "ICPORTAGE_PPS_EXTERNAL_NLB_HAPROXY_LISTENER" {
    load_balancer_arn = "${aws_lb.ICPORTAGE_PPS_EXTERNAL_NLB.arn}"
    port              = "443"
    protocol          = "TCP"

    default_action {
        type             = "forward"
        target_group_arn = "${aws_lb_target_group.ICPORTAGE_PPS_EXTERNAL_NLB_HAPROXY_TARGET_GROUP.arn}"
    }
}

resource "aws_lb_target_group_attachment" "ICPORTAGE_PPS_EXTERNAL_NLB_TARGET_GROUP_ATTACHMENT_HAPROXY_1" {
    target_group_arn = "${aws_lb_target_group.ICPORTAGE_PPS_EXTERNAL_NLB_HAPROXY_TARGET_GROUP.arn}"
    target_id        = "${aws_instance.ICPORTAGE_PPS_EXTERNAL_HAPROXY_1.id}"
    port             = 443
}

resource "aws_lb_target_group_attachment" "ICPORTAGE_PPS_EXTERNAL_NLB_TARGET_GROUP_ATTACHMENT_HAPROXY_2" {
    target_group_arn = "${aws_lb_target_group.ICPORTAGE_PPS_EXTERNAL_NLB_HAPROXY_TARGET_GROUP.arn}"
    target_id        = "${aws_instance.ICPORTAGE_PPS_EXTERNAL_HAPROXY_2.id}"
    port             = 443
}

*/

resource "aws_lb" "nonprod_portage_load_balancer" {
  name                       = "nonprod-portage-load-balancer"
  internal                   = true
  load_balancer_type         = "application"
  security_groups            = ["${aws_security_group.portage_dev_lb_vpc_sg.id}"]
  enable_deletion_protection = false

  subnets = [
    "${var.portage-dev-private-1a-subnet}",
    "${var.portage-dev-private-1b-subnet}",
  ]

  # access_logs {
  #   bucket  = "${aws_s3_bucket.lb_logs.bucket}"
  #   prefix  = "portage_dev_alb_logs"
  #   enabled = true
  # }

  tags = {
    Environment = "development"
    Name        = "portage_dev_alb"
  }
}

resource "aws_alb_listener" "nonprod_portage_load_balancer_listener" {
  load_balancer_arn = "${aws_lb.nonprod_portage_load_balancer.arn}"
  port              = "443"
  protocol          = "HTTPS"
  ssl_policy        = "ELBSecurityPolicy-TLS-1-2-2017-01"
  certificate_arn   = "arn:aws:acm:ca-central-1:091072499599:certificate/48ee95ff-506d-4099-95ab-0c9e1899f3c5"

  default_action {
    type = "fixed-response"

    fixed_response {
      content_type = "text/plain"
      message_body = "Fixed response content"
      status_code  = "200"
    }
  }
}

# Rules

# dev01 gwpc rule

resource "aws_alb_listener_rule" "dev01_guidewire_pc_listener_rule" {
  depends_on   = ["aws_alb_target_group.dev01_guidewire_pc_target_group"]
  listener_arn = "${aws_alb_listener.nonprod_portage_load_balancer_listener.arn}"

  action {
    type             = "forward"
    target_group_arn = "${aws_alb_target_group.dev01_guidewire_pc_target_group.arn}"
  }

  condition {
    host_header {
      values = ["dev01-gwpc.nonprod.portage.insurcloud.ca"]
    }
  }
}

# dev01 gwbc rule

resource "aws_alb_listener_rule" "dev01_guidewire_bc_listener_rule" {
  depends_on   = ["aws_alb_target_group.dev01_guidewire_bc_target_group"]
  listener_arn = "${aws_alb_listener.nonprod_portage_load_balancer_listener.arn}"

  action {
    type             = "forward"
    target_group_arn = "${aws_alb_target_group.dev01_guidewire_bc_target_group.arn}"
  }

  condition {
    host_header {
      values = ["dev01-gwbc.nonprod.portage.insurcloud.ca"]
    }
  }
}

# dev01 gwcm rule

resource "aws_alb_listener_rule" "dev01_guidewire_cm_listener_rule" {
  depends_on   = ["aws_alb_target_group.dev01_guidewire_cm_target_group"]
  listener_arn = "${aws_alb_listener.nonprod_portage_load_balancer_listener.arn}"

  action {
    type             = "forward"
    target_group_arn = "${aws_alb_target_group.dev01_guidewire_cm_target_group.arn}"
  }

  condition {
    host_header {
      values = ["dev01-gwcm.nonprod.portage.insurcloud.ca"]
    }
  }
}

# dev01 microservices admin rule

resource "aws_alb_listener_rule" "dev01_microservices_admin_listener_rule" {
  depends_on   = ["aws_alb_target_group.dev01_microservices_admin_target_group"]
  listener_arn = "${aws_alb_listener.nonprod_portage_load_balancer_listener.arn}"

  action {
    type             = "forward"
    target_group_arn = "${aws_alb_target_group.dev01_microservices_admin_target_group.arn}"
  }

  condition {
    host_header {
      values = ["dev01-ms-admin.nonprod.portage.insurcloud.ca"]
    }
  }
}

# dev02 gwpc rule

resource "aws_alb_listener_rule" "dev02_guidewire_pc_listener_rule" {
  depends_on   = ["aws_alb_target_group.dev02_guidewire_pc_target_group"]
  listener_arn = "${aws_alb_listener.nonprod_portage_load_balancer_listener.arn}"

  action {
    type             = "forward"
    target_group_arn = "${aws_alb_target_group.dev02_guidewire_pc_target_group.arn}"
  }

  condition {
    host_header {
      values = ["dev02-gwpc.nonprod.portage.insurcloud.ca"]
    }
  }
}

# dev02 gwbc rule

resource "aws_alb_listener_rule" "dev02_guidewire_bc_listener_rule" {
  depends_on   = ["aws_alb_target_group.dev02_guidewire_bc_target_group"]
  listener_arn = "${aws_alb_listener.nonprod_portage_load_balancer_listener.arn}"

  action {
    type             = "forward"
    target_group_arn = "${aws_alb_target_group.dev02_guidewire_bc_target_group.arn}"
  }

  condition {
    host_header {
      values = ["dev02-gwbc.nonprod.portage.insurcloud.ca"]
    }
  }
}

# dev02 gwcm rule

resource "aws_alb_listener_rule" "dev02_guidewire_cm_listener_rule" {
  depends_on   = ["aws_alb_target_group.dev02_guidewire_cm_target_group"]
  listener_arn = "${aws_alb_listener.nonprod_portage_load_balancer_listener.arn}"

  action {
    type             = "forward"
    target_group_arn = "${aws_alb_target_group.dev02_guidewire_cm_target_group.arn}"
  }

  condition {
    host_header {
      values = ["dev02-gwcm.nonprod.portage.insurcloud.ca"]
    }
  }
}

# dev02 microservices admin rule

resource "aws_alb_listener_rule" "dev02_microservices_admin_listener_rule" {
  depends_on   = ["aws_alb_target_group.dev02_microservices_admin_target_group"]
  listener_arn = "${aws_alb_listener.nonprod_portage_load_balancer_listener.arn}"

  action {
    type             = "forward"
    target_group_arn = "${aws_alb_target_group.dev02_microservices_admin_target_group.arn}"
  }

  condition {
    host_header {
      values = ["dev02-ms-admin.nonprod.portage.insurcloud.ca"]
    }
  }
}

# cnv01 gwpc rule

resource "aws_alb_listener_rule" "cnv01_guidewire_pc_listener_rule" {
  depends_on   = ["aws_alb_target_group.cnv01_guidewire_pc_target_group"]
  listener_arn = "${aws_alb_listener.nonprod_portage_load_balancer_listener.arn}"

  action {
    type             = "forward"
    target_group_arn = "${aws_alb_target_group.cnv01_guidewire_pc_target_group.arn}"
  }

  condition {
    host_header {
      values = ["cnv01-gwpc.nonprod.portage.insurcloud.ca"]
    }
  }
}

# cnv01 gwbc rule

resource "aws_alb_listener_rule" "cnv01_guidewire_bc_listener_rule" {
  depends_on   = ["aws_alb_target_group.cnv01_guidewire_bc_target_group"]
  listener_arn = "${aws_alb_listener.nonprod_portage_load_balancer_listener.arn}"

  action {
    type             = "forward"
    target_group_arn = "${aws_alb_target_group.cnv01_guidewire_bc_target_group.arn}"
  }

  condition {
    host_header {
      values = ["cnv01-gwbc.nonprod.portage.insurcloud.ca"]
    }
  }
}

# cnv01 gwcm rule

resource "aws_alb_listener_rule" "cnv01_guidewire_cm_listener_rule" {
  depends_on   = ["aws_alb_target_group.cnv01_guidewire_cm_target_group"]
  listener_arn = "${aws_alb_listener.nonprod_portage_load_balancer_listener.arn}"

  action {
    type             = "forward"
    target_group_arn = "${aws_alb_target_group.cnv01_guidewire_cm_target_group.arn}"
  }

  condition {
    host_header {
      values = ["cnv01-gwcm.nonprod.portage.insurcloud.ca"]
    }
  }
}

# cnv01 microservices admin rule

resource "aws_alb_listener_rule" "cnv01_microservices_admin_listener_rule" {
  depends_on   = ["aws_alb_target_group.cnv01_microservices_admin_target_group"]
  listener_arn = "${aws_alb_listener.nonprod_portage_load_balancer_listener.arn}"

  action {
    type             = "forward"
    target_group_arn = "${aws_alb_target_group.cnv01_microservices_admin_target_group.arn}"
  }

  condition {
    host_header {
      values = ["cnv01-ms-admin.nonprod.portage.insurcloud.ca"]
    }
  }
}

# qa01 gwpc rule

resource "aws_alb_listener_rule" "qa01_guidewire_pc_listener_rule" {
  depends_on   = ["aws_alb_target_group.qa01_guidewire_pc_target_group"]
  listener_arn = "${aws_alb_listener.nonprod_portage_load_balancer_listener.arn}"

  action {
    type             = "forward"
    target_group_arn = "${aws_alb_target_group.qa01_guidewire_pc_target_group.arn}"
  }

  condition {
    host_header {
      values = ["qa01-gwpc.nonprod.portage.insurcloud.ca"]
    }
  }
}

# qa01 gwbc rule

resource "aws_alb_listener_rule" "qa01_guidewire_bc_listener_rule" {
  depends_on   = ["aws_alb_target_group.qa01_guidewire_bc_target_group"]
  listener_arn = "${aws_alb_listener.nonprod_portage_load_balancer_listener.arn}"

  action {
    type             = "forward"
    target_group_arn = "${aws_alb_target_group.qa01_guidewire_bc_target_group.arn}"
  }

  condition {
    host_header {
      values = ["qa01-gwbc.nonprod.portage.insurcloud.ca"]
    }
  }
}

# qa01 gwcm rule

resource "aws_alb_listener_rule" "qa01_guidewire_cm_listener_rule" {
  depends_on   = ["aws_alb_target_group.qa01_guidewire_cm_target_group"]
  listener_arn = "${aws_alb_listener.nonprod_portage_load_balancer_listener.arn}"

  action {
    type             = "forward"
    target_group_arn = "${aws_alb_target_group.qa01_guidewire_cm_target_group.arn}"
  }

  condition {
    host_header {
      values = ["qa01-gwcm.nonprod.portage.insurcloud.ca"]
    }
  }
}

# qa01 microservices rule

# qa01 microservices admin rule

resource "aws_alb_listener_rule" "qa01_microservices_admin_listener_rule" {
  depends_on   = ["aws_alb_target_group.qa01_microservices_admin_target_group"]
  listener_arn = "${aws_alb_listener.nonprod_portage_load_balancer_listener.arn}"

  action {
    type             = "forward"
    target_group_arn = "${aws_alb_target_group.qa01_microservices_admin_target_group.arn}"
  }

  condition {
    host_header {
      values = ["qa01-ms-admin.nonprod.portage.insurcloud.ca"]
    }
  }
}

# qa02 gwpc rule

resource "aws_alb_listener_rule" "qa02_guidewire_pc_listener_rule" {
  depends_on   = ["aws_alb_target_group.qa02_guidewire_pc_target_group"]
  listener_arn = "${aws_alb_listener.nonprod_portage_load_balancer_listener.arn}"

  action {
    type             = "forward"
    target_group_arn = "${aws_alb_target_group.qa02_guidewire_pc_target_group.arn}"
  }

  condition {
    host_header {
      values = ["qa02-gwpc.nonprod.portage.insurcloud.ca"]
    }
  }
}

# qa02 gwbc rule

resource "aws_alb_listener_rule" "qa02_guidewire_bc_listener_rule" {
  depends_on   = ["aws_alb_target_group.qa02_guidewire_bc_target_group"]
  listener_arn = "${aws_alb_listener.nonprod_portage_load_balancer_listener.arn}"

  action {
    type             = "forward"
    target_group_arn = "${aws_alb_target_group.qa02_guidewire_bc_target_group.arn}"
  }

  condition {
    host_header {
      values = ["qa02-gwbc.nonprod.portage.insurcloud.ca"]
    }
  }
}

# qa02 gwcm rule

resource "aws_alb_listener_rule" "qa02_guidewire_cm_listener_rule" {
  depends_on   = ["aws_alb_target_group.qa02_guidewire_cm_target_group"]
  listener_arn = "${aws_alb_listener.nonprod_portage_load_balancer_listener.arn}"

  action {
    type             = "forward"
    target_group_arn = "${aws_alb_target_group.qa02_guidewire_cm_target_group.arn}"
  }

  condition {
    host_header {
      values = ["qa02-gwcm.nonprod.portage.insurcloud.ca"]
    }
  }
}

# qa02 microservices admin rule

resource "aws_alb_listener_rule" "qa02_microservices_admin_listener_rule" {
  depends_on   = ["aws_alb_target_group.qa02_microservices_admin_target_group"]
  listener_arn = "${aws_alb_listener.nonprod_portage_load_balancer_listener.arn}"

  action {
    type             = "forward"
    target_group_arn = "${aws_alb_target_group.qa02_microservices_admin_target_group.arn}"
  }

  condition {
    host_header {
      values = ["qa02-ms-admin.nonprod.portage.insurcloud.ca"]
    }
  }
}