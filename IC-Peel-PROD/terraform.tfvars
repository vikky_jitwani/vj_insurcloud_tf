#Envrionment specific variables
client_name = "IC-Peel"
enviornment_name = "PROD"
enviornment_type = "P"
client_number = "A003"
#ad-dc1 = "10.10.4.10"
#ad-dc2 = "10.10.6.10"
volume_key = "arn:aws:kms:ca-central-1:413329031558:key/5cdd3e56-2101-47df-bfe7-c4d1732f684f"
ami_id_windows2012R2sql14enterprise = "ami-07ebb1b4727f89cda"
ami_id_windows2012R2sql14standard = "ami-0913a2afb4fa5fc5b"
ami_id_windows2019container = "ami-013453540e1aa0d48"
ami_id_windows2019base = "ami-06b8f69a907cf6bb2"
ami_id_windows2016base = "ami-0bce38ba7ac166aa9"
ami_id_windows2012R2base = "ami-09b707ce10f2e9cee"
ami_id_windows2008base = "ami-0446e08d2d99fc8c9"
ami_id_redhatlinux75 = "ami-05c33d286020a47f9"
ami_id_ubuntu1804baseimage = "ami-032172cc5f34b32fc"
ami_id_ubuntu1604baseimage = "ami-04f40cca01b3186ea"
ami_id_amazonlinux201712base = "ami-0a777bbf8ef3f43bf"
ami_id_amazonlinux201709base = "ami-058dba934995c5468"
ami_id_amazonlinux2base = "ami-0ad9abdd25c431def"
ami_id_amazonlinux201803base = "ami-06829694f407e15c1"

#VPC Specific variables
vpc_id = "vpc-087fe19b5b60819a4"
vpc_cidr = "10.12.0.0/22"


peel-prod-database-1a-subnet = "subnet-04c99d9796ba614c8"
peel-prod-database-1b-subnet = "subnet-0408645dead4aee3b"
peel-prod-private-1a-subnet = "subnet-0993e8854d704beaf"
peel-prod-private-1b-subnet = "subnet-0102f99fde6adeda1"


#RDS
mssql-rds-password = "Toronto12345"
mysql-rds-password = "Toronto12345"
postgres-rds-password = "Toronto12345"

#Accounts
peel-dev-account = "005810662120"
peel-prod-account = "413329031558"
peel-hub-account = "186239701968"
