## Resources##
resource "aws_lambda_layer_version" "ODBCPy_layer" {
  s3_bucket = "peel-lambda-dependencies"
  s3_key = "ODBCPythonDependencies_1.zip"
  layer_name = "ODBCPythonDependencies_1"
  compatible_runtimes = ["python3.7"]
}

## PPS Lambdas ##
resource "aws_lambda_permission" "apigw_lambda_ppsglfeed" {
  statement_id  = "AllowExecutionFromAPIGateway"
  action        = "lambda:InvokeFunction"
  function_name = "${aws_lambda_function.peel-PPSGLFeed.function_name}"
  principal     = "apigateway.amazonaws.com"

  # More: http://docs.aws.amazon.com/apigateway/latest/developerguide/api-gateway-control-access-using-iam-policies-to-invoke-api.html
  # They are in the hub 
  source_arn = "arn:aws:execute-api:${var.region}:${var.peel-hub-account}:nbfett4mhd/*/*/*"
}

resource "aws_lambda_function" "peel-PPSGLFeed" {

  function_name = "peel-PPSGLFeed"
  role          = "arn:aws:iam::413329031558:role/LambdaAssume"
  handler       = "PeelGLIntegration_get.readFromTable"
  runtime       = "python3.7"
  timeout       = 60
  layers        = ["${aws_lambda_layer_version.ODBCPy_layer.arn}"]
  s3_bucket     = "peel-lambda-dependencies"
  s3_key        = "PeelGLIntegration_get.zip"

  vpc_config    ={
    subnet_ids = ["${var.peel-prod-private-1a-subnet}", "${var.peel-prod-private-1b-subnet}"]
    security_group_ids = ["${aws_security_group.SG_LAMBDA.id}"]
  }

  environment {
    variables = {
      REGION_NAME = "ca-central-1"
      SECRET_NAME = "Peel-PPS-ICUser"
    }
  }
}

#Lambda function for PPS Peel GL Update
resource "aws_lambda_permission" "apigw_lambda_ppsglupdate" {
  statement_id  = "AllowExecutionFromAPIGateway"
  action        = "lambda:InvokeFunction"
  function_name = "${aws_lambda_function.peel-PPSGLUpdate.function_name}"
  principal     = "apigateway.amazonaws.com"

  # More: http://docs.aws.amazon.com/apigateway/latest/developerguide/api-gateway-control-access-using-iam-policies-to-invoke-api.html
  # They are in the hub 
  #source_arn = "arn:aws:execute-api:${var.region}:${var.accountId}:${aws_api_gateway_rest_api.GLCheque.id}/*/${aws_api_gateway_method.PeelGLUpdate.http_method}${aws_api_gateway_resource.PeelGLUpdate.path}"
  source_arn = "arn:aws:execute-api:${var.region}:${var.peel-hub-account}:nbfett4mhd/*/*/*"
}

resource "aws_lambda_function" "peel-PPSGLUpdate" {
  function_name = "peel-PPSGLUpdate"
  role          = "arn:aws:iam::413329031558:role/LambdaAssume"
  handler       = "PeelGLIntegration_post.putInTable"
  runtime       = "python3.7"
  timeout       = 60
  layers        = ["${aws_lambda_layer_version.ODBCPy_layer.arn}"]
  s3_bucket     = "peel-lambda-dependencies"
  s3_key        = "PeelGLIntegration_post.zip"

  vpc_config    ={
    subnet_ids = ["${var.peel-prod-private-1a-subnet}", "${var.peel-prod-private-1b-subnet}"]
    security_group_ids = ["${aws_security_group.SG_LAMBDA.id}"]
  }

  environment {
    variables = {
      REGION_NAME = "ca-central-1"
      SECRET_NAME = "Peel-PPS-ICUser"
    }
  }
}

##PROD LAMBDAs##
resource "aws_lambda_permission" "apigw_lambda_prodglfeed" {
  statement_id  = "AllowExecutionFromAPIGateway"
  action        = "lambda:InvokeFunction"
  function_name = "${aws_lambda_function.peel-ProdGLFeed.function_name}"
  principal     = "apigateway.amazonaws.com"

  # More: http://docs.aws.amazon.com/apigateway/latest/developerguide/api-gateway-control-access-using-iam-policies-to-invoke-api.html
  # They are in the hub 
  # source_arn = "arn:aws:execute-api:${var.region}:${var.accountId}:${aws_api_gateway_rest_api.GLCheque.id}/*/${aws_api_gateway_method.PeelGLFeed.http_method}${aws_api_gateway_resource.PeelGLFeed.path}"
  source_arn = "arn:aws:execute-api:${var.region}:${var.peel-hub-account}:nbfett4mhd/*/*/*"
}

resource "aws_lambda_function" "peel-ProdGLFeed" {
  function_name = "peel-ProdGLFeed"
  role          = "arn:aws:iam::413329031558:role/LambdaAssume"
  handler       = "PeelGLIntegration_get.readFromTable"
  runtime       = "python3.7"
  timeout       = 60
  layers        = ["${aws_lambda_layer_version.ODBCPy_layer.arn}"]
  s3_bucket     = "peel-lambda-dependencies"
  s3_key        = "PeelGLIntegration_get.zip"

  vpc_config    ={
    subnet_ids = ["${var.peel-prod-private-1a-subnet}", "${var.peel-prod-private-1b-subnet}"]
    security_group_ids = ["${aws_security_group.SG_LAMBDA.id}"]
  }

  environment {
    variables = {
      REGION_NAME = "ca-central-1"
      SECRET_NAME = "PEEL_PROD_InfoCenter_DBOwner"
    }
  }
}

#Lambda function for PPS Peel GL Update
resource "aws_lambda_permission" "apigw_lambda_prodglupdate" {
  statement_id  = "AllowExecutionFromAPIGateway"
  action        = "lambda:InvokeFunction"
  function_name = "${aws_lambda_function.peel-ProdGLUpdate.function_name}"
  principal     = "apigateway.amazonaws.com"

  # More: http://docs.aws.amazon.com/apigateway/latest/developerguide/api-gateway-control-access-using-iam-policies-to-invoke-api.html
  # They are in the hub 
  #source_arn = "arn:aws:execute-api:${var.region}:${var.accountId}:${aws_api_gateway_rest_api.GLCheque.id}/*/${aws_api_gateway_method.PeelGLUpdate.http_method}${aws_api_gateway_resource.PeelGLUpdate.path}"
   source_arn = "arn:aws:execute-api:${var.region}:${var.peel-hub-account}:nbfett4mhd/*/*/*"
}

resource "aws_lambda_function" "peel-ProdGLUpdate" {
  function_name = "peel-ProdGLUpdate"
  role          = "arn:aws:iam::413329031558:role/LambdaAssume"
  handler       = "PeelGLIntegration_post.putInTable"
  runtime       = "python3.7"
  timeout       = 60
  layers        = ["${aws_lambda_layer_version.ODBCPy_layer.arn}"]
  s3_bucket     = "peel-lambda-dependencies"
  s3_key        = "PeelGLIntegration_post.zip"

  vpc_config    ={
    subnet_ids = ["${var.peel-prod-private-1a-subnet}", "${var.peel-prod-private-1b-subnet}"]
    security_group_ids = ["${aws_security_group.SG_LAMBDA.id}"]
  }

  environment {
    variables = {
      REGION_NAME = "ca-central-1"
      SECRET_NAME = "PEEL_PROD_InfoCenter_DBOwner"
    }
  }
}