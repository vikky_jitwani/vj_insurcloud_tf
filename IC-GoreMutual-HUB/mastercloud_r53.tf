// #=======================================================================
// # HUB 
// #=======================================================================

resource "aws_route53_record" "ICGORE_NEXUS_REG_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "nexus.gore.insurcloud.ca"
    type    = "A"

    alias {
        name                   = "internal-cicd-internal-alb-2023433918.ca-central-1.elb.amazonaws.com"
        zone_id                = "ZQSVJUPU6J1EY"
        evaluate_target_health = false
    }
}

resource "aws_route53_record" "ICGORE_NEXUS_PROD_REG_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "nexus-prod.gore.insurcloud.ca"
    type    = "A"

    alias {
        name                   = "internal-cicd-internal-alb-2023433918.ca-central-1.elb.amazonaws.com"
        zone_id                = "ZQSVJUPU6J1EY"
        evaluate_target_health = false
    }
}

resource "aws_route53_record" "ICGORE_NEXUS_UI_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "nexus-ui.gore.insurcloud.ca"
    type    = "A"

    alias {
        name                   = "internal-cicd-internal-alb-2023433918.ca-central-1.elb.amazonaws.com"
        zone_id                = "ZQSVJUPU6J1EY"
        evaluate_target_health = false
    }
}

resource "aws_route53_record" "ICGORE_SONARQUBE_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "sonarqube.gore.insurcloud.ca"
    type    = "A"

    alias {
        name                   = "internal-cicd-internal-alb-2023433918.ca-central-1.elb.amazonaws.com"
        zone_id                = "ZQSVJUPU6J1EY"
        evaluate_target_health = false
    }
}

resource "aws_route53_record" "ICGORE_CONSUL_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "consul.gore.insurcloud.ca"
    type    = "A"

    alias {
        name                   = "internal-cicd-internal-alb-2023433918.ca-central-1.elb.amazonaws.com"
        zone_id                = "ZQSVJUPU6J1EY"
        evaluate_target_health = false
    }
}

resource "aws_route53_record" "ICGORE_JENKINS_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "jenkins.gore.insurcloud.ca"
    type    = "A"

    alias {
        name                   = "internal-cicd-internal-alb-2023433918.ca-central-1.elb.amazonaws.com"
        zone_id                = "ZQSVJUPU6J1EY"
        evaluate_target_health = false
    }
}

resource "aws_route53_record" "ICGORE_VAULT_PPS_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "vault.pps.gore.insurcloud.ca"
    type    = "A"

    alias {
        name                   = "a-mc-cac-d-a003-ic-pps-vault-84458cafb01e039e.elb.ca-central-1.amazonaws.com"
        zone_id                = "Z2EPGBW3API2WT"
        evaluate_target_health = false
    }
}

resource "aws_route53_record" "ICGORE_VAULT_PROD_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "vault.prod.gore.insurcloud.ca"
    type    = "A"

    alias {
        name                   = "a-mc-cac-d-a003-ic-prod-vault-7b0fa62bf2511ffc.elb.ca-central-1.amazonaws.com"
        zone_id                = "Z2EPGBW3API2WT"
        evaluate_target_health = false
    }
}

# #=======================================================================
# # DEV01
# #=======================================================================

######### MICROSERVICES ###########
resource "aws_route53_record" "ICGORE_DEV01_MICROSERVICES_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "ms1.dev01.gore.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICGORE_DEV01_MICROSERVICES_PRIVATEIP}"]
}

resource "aws_route53_record" "ICGORE_DEV01_INTERNAL_NLB_MICROSERVICES_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "ms.dev01.gore.insurcloud.ca"
    type    = "A"
    #internet facing alb
    alias {
        #name                   = "${aws_lb.ICGORE_DEV01_EXTERNAL_ALB.dns_name}"
        #zone_id                = "${aws_lb.ICGORE_DEV01_EXTERNAL_ALB.zone_id}"
        name                   = "${data.terraform_remote_state.dev_remote_state.ICGORE_DEV01_INTERNAL_NLB_DNS_NAME}"
        zone_id                = "${data.terraform_remote_state.dev_remote_state.ICGORE_DEV01_INTERNAL_NLB_ZONE_ID}"
        evaluate_target_health = false
    }
}


########## HAPROXY ##############
resource "aws_route53_record" "ICGORE_DEV01_INTERNAL_HAPROXY_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "inthap.dev01.gore.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICGORE_DEV01_INTERNAL_HAPROXY_PRIVATEIP}"]
}

########## EXTERNAL LB ##############
resource "aws_route53_record" "ICGORE_DEV01_EXTERNAL_ALB_MICROSERVICES_RECORD" {
    zone_id = "${var.public_zone_id}"
    name    = "ms-dev01-ext.nonprod.gore.insurcloud.ca"
    type    = "CNAME"
    ttl     = "5"
    #external facing alb

    records = ["${aws_lb.ICGORE_DEV01_NORTH_ALB.dns_name}"]
      
}


# #=======================================================================
# # DEV02
# #=======================================================================


######## MICROSERVICES ###########
resource "aws_route53_record" "ICGORE_DEV02_MICROSERVICES_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "ms1.dev02.gore.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICGORE_DEV02_MICROSERVICES_PRIVATEIP}"]
}

resource "aws_route53_record" "ICGORE_DEV02_INTERNAL_NLB_MICROSERVICES_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "ms.dev02.gore.insurcloud.ca"
    type    = "A"
    alias {
        name                   = "${data.terraform_remote_state.dev_remote_state.ICGORE_DEV02_INTERNAL_NLB_DNS_NAME}"
        zone_id                = "${data.terraform_remote_state.dev_remote_state.ICGORE_DEV02_INTERNAL_NLB_ZONE_ID}"
        evaluate_target_health = false
    }
}

######## HAPROXY ##############
resource "aws_route53_record" "ICGORE_DEV02_INTERNAL_HAPROXY_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "inthap.dev02.GORE.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICGORE_DEV02_INTERNAL_HAPROXY_PRIVATEIP}"]
}

########## EXTERNAL LB ##############
resource "aws_route53_record" "ICGORE_DEV02_EXTERNAL_ALB_MICROSERVICES_RECORD" {
    zone_id = "${var.public_zone_id}"
    name    = "ms-dev02-ext.nonprod.gore.insurcloud.ca"
    type    = "CNAME"
    ttl     = "5"
    #external facing alb

    records = ["${aws_lb.ICGORE_DEV02_NORTH_ALB.dns_name}"]
      
}

# #=======================================================================
# # DEV03
# #=======================================================================


######## MICROSERVICES ###########
resource "aws_route53_record" "ICGORE_DEV03_MICROSERVICES_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "ms1.dev03.gore.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICGORE_DEV03_MICROSERVICES_PRIVATEIP}"]
}

resource "aws_route53_record" "ICGORE_DEV03_INTERNAL_NLB_MICROSERVICES_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "ms.dev03.gore.insurcloud.ca"
    type    = "A"
    alias {
        name                   = "${data.terraform_remote_state.dev_remote_state.ICGORE_DEV03_INTERNAL_NLB_DNS_NAME}"
        zone_id                = "${data.terraform_remote_state.dev_remote_state.ICGORE_DEV03_INTERNAL_NLB_ZONE_ID}"
        evaluate_target_health = false
    }
}

######## HAPROXY ##############
resource "aws_route53_record" "ICGORE_DEV03_INTERNAL_HAPROXY_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "inthap.dev03.GORE.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICGORE_DEV03_INTERNAL_HAPROXY_PRIVATEIP}"]
}

########## EXTERNAL LB ##############
resource "aws_route53_record" "ICGORE_DEV03_EXTERNAL_ALB_MICROSERVICES_RECORD" {
    zone_id = "${var.public_zone_id}"
    name    = "ms-dev03-ext.nonprod.gore.insurcloud.ca"
    type    = "CNAME"
    ttl     = "5"
    #external facing alb

    records = ["${aws_lb.ICGORE_DEV03_NORTH_ALB.dns_name}"]
      
}



# #=======================================================================
# # TBT01
# #=======================================================================


######## MICROSERVICES ###########
resource "aws_route53_record" "ICGORE_TBT01_MICROSERVICES_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "ms1.tbt01.gore.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICGORE_TBT01_MICROSERVICES_PRIVATEIP}"]
}

resource "aws_route53_record" "ICGORE_TBT01_INTERNAL_NLB_MICROSERVICES_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "ms.tbt01.gore.insurcloud.ca"
    type    = "A"
    alias {
        name                   = "${data.terraform_remote_state.dev_remote_state.ICGORE_TBT01_INTERNAL_NLB_DNS_NAME}"
        zone_id                = "${data.terraform_remote_state.dev_remote_state.ICGORE_TBT01_INTERNAL_NLB_ZONE_ID}"
        evaluate_target_health = false
    }
}

######## HAPROXY ##############
resource "aws_route53_record" "ICGORE_TBT01_INTERNAL_HAPROXY_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "inthap.tbt01.GORE.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICGORE_TBT01_INTERNAL_HAPROXY_PRIVATEIP}"]
}

########## EXTERNAL LB ##############
resource "aws_route53_record" "ICGORE_TBT01_EXTERNAL_ALB_MICROSERVICES_RECORD" {
    zone_id = "${var.public_zone_id}"
    name    = "ms-tbt01-ext.nonprod.gore.insurcloud.ca"
    type    = "CNAME"
    ttl     = "5"
    #external facing alb

    records = ["${aws_lb.ICGORE_TBT01_NORTH_ALB.dns_name}"]
      
}


# #=======================================================================
# # QA01
# #=======================================================================


######## MICROSERVICES ###########
resource "aws_route53_record" "ICGORE_QA01_MICROSERVICES_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "ms1.qa01.gore.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICGORE_QA01_MICROSERVICES_PRIVATEIP}"]
}

resource "aws_route53_record" "ICGORE_QA01_INTERNAL_NLB_MICROSERVICES_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "ms.qa01.gore.insurcloud.ca"
    type    = "A"
    alias {
        name                   = "${data.terraform_remote_state.dev_remote_state.ICGORE_QA01_INTERNAL_NLB_DNS_NAME}"
        zone_id                = "${data.terraform_remote_state.dev_remote_state.ICGORE_QA01_INTERNAL_NLB_ZONE_ID}"
        evaluate_target_health = false
    }
}

######## HAPROXY ##############
resource "aws_route53_record" "ICGORE_QA01_INTERNAL_HAPROXY_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "inthap.qa01.GORE.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICGORE_QA01_INTERNAL_HAPROXY_PRIVATEIP}"]
}

########## EXTERNAL LB ##############
resource "aws_route53_record" "ICGORE_QA01_EXTERNAL_ALB_MICROSERVICES_RECORD" {
    zone_id = "${var.public_zone_id}"
    name    = "ms-qa01-ext.nonprod.gore.insurcloud.ca"
    type    = "CNAME"
    ttl     = "5"
    #external facing alb

    records = ["${aws_lb.ICGORE_QA01_NORTH_ALB.dns_name}"]
      
}



# #=======================================================================
# # QA02
# #=======================================================================


######## MICROSERVICES ###########
resource "aws_route53_record" "ICGORE_QA02_MICROSERVICES_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "ms1.qa02.gore.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICGORE_QA02_MICROSERVICES_PRIVATEIP}"]
}

resource "aws_route53_record" "ICGORE_QA02_INTERNAL_NLB_MICROSERVICES_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "ms.qa02.gore.insurcloud.ca"
    type    = "A"
    alias {
        name                   = "${data.terraform_remote_state.dev_remote_state.ICGORE_QA02_INTERNAL_NLB_DNS_NAME}"
        zone_id                = "${data.terraform_remote_state.dev_remote_state.ICGORE_QA02_INTERNAL_NLB_ZONE_ID}"
        evaluate_target_health = false
    }
}

######## HAPROXY ##############
resource "aws_route53_record" "ICGORE_QA02_INTERNAL_HAPROXY_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "inthap.qa02.GORE.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICGORE_QA02_INTERNAL_HAPROXY_PRIVATEIP}"]
}

########## EXTERNAL LB ##############
resource "aws_route53_record" "ICGORE_QA02_EXTERNAL_ALB_MICROSERVICES_RECORD" {
    zone_id = "${var.public_zone_id}"
    name    = "ms-qa02-ext.nonprod.gore.insurcloud.ca"
    type    = "CNAME"
    ttl     = "5"
    #external facing alb

    records = ["${aws_lb.ICGORE_QA02_NORTH_ALB.dns_name}"]
      
}


# #=======================================================================
# # TBT02
# #=======================================================================


######## MICROSERVICES ###########
resource "aws_route53_record" "ICGORE_TBT02_MICROSERVICES_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "ms1.tbt02.gore.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICGORE_TBT02_MICROSERVICES_PRIVATEIP}"]
}

resource "aws_route53_record" "ICGORE_TBT02_INTERNAL_NLB_MICROSERVICES_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "ms.tbt02.gore.insurcloud.ca"
    type    = "A"
    alias {
        name                   = "${data.terraform_remote_state.dev_remote_state.ICGORE_TBT02_INTERNAL_NLB_DNS_NAME}"
        zone_id                = "${data.terraform_remote_state.dev_remote_state.ICGORE_TBT02_INTERNAL_NLB_ZONE_ID}"
        evaluate_target_health = false
    }
}

######## HAPROXY ##############
resource "aws_route53_record" "ICGORE_TBT02_INTERNAL_HAPROXY_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "inthap.tbt02.GORE.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICGORE_TBT02_INTERNAL_HAPROXY_PRIVATEIP}"]
}

########## EXTERNAL LB ##############
resource "aws_route53_record" "ICGORE_TBT02_EXTERNAL_ALB_MICROSERVICES_RECORD" {
    zone_id = "${var.public_zone_id}"
    name    = "ms-tbt02-ext.nonprod.gore.insurcloud.ca"
    type    = "CNAME"
    ttl     = "5"
    #external facing alb

    records = ["${aws_lb.ICGORE_TBT02_NORTH_ALB.dns_name}"]
      
}

# #=======================================================================
# # BAT
# #=======================================================================

######## MICROSERVICES ###########
resource "aws_route53_record" "ICGORE_BAT_MICROSERVICES_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "ms1.bat.gore.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICGORE_BAT_MICROSERVICES_PRIVATEIP}"]
}

resource "aws_route53_record" "ICGORE_BAT_INTERNAL_NLB_MICROSERVICES_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "ms.bat.gore.insurcloud.ca"
    type    = "A"
    alias {
        name                   = "${data.terraform_remote_state.dev_remote_state.ICGORE_BAT_INTERNAL_NLB_DNS_NAME}"
        zone_id                = "${data.terraform_remote_state.dev_remote_state.ICGORE_BAT_INTERNAL_NLB_ZONE_ID}"
        evaluate_target_health = false
    }
}

######## HAPROXY ##############
resource "aws_route53_record" "ICGORE_BAT_INTERNAL_HAPROXY_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "inthap.bat.GORE.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICGORE_BAT_INTERNAL_HAPROXY_PRIVATEIP}"]
}

########## EXTERNAL LB ##############
resource "aws_route53_record" "ICGORE_BAT_EXTERNAL_ALB_MICROSERVICES_RECORD" {
    zone_id = "${var.public_zone_id}"
    name    = "ms-bat-ext.nonprod.gore.insurcloud.ca"
    type    = "CNAME"
    ttl     = "5"
    #external facing alb

    records = ["${aws_lb.ICGORE_BAT_NORTH_ALB.dns_name}"]
      
}

# #=======================================================================
# # PERF
# #=======================================================================

######## MICROSERVICES ###########
resource "aws_route53_record" "ICGORE_PERF_MICROSERVICES_MANAGER_1_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "msman1.perf.gore.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICGORE_PERF_MICROSERVICES_MANAGER_1_PRIVATEIP}"]
}

resource "aws_route53_record" "ICGORE_PERF_MICROSERVICES_MANAGER_2_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "msman2.perf.gore.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICGORE_PERF_MICROSERVICES_MANAGER_2_PRIVATEIP}"]
}

resource "aws_route53_record" "ICGORE_PERF_MICROSERVICES_MANAGER_3_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "msman3.perf.gore.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICGORE_PERF_MICROSERVICES_MANAGER_3_PRIVATEIP}"]
}

resource "aws_route53_record" "ICGORE_PERF_MICROSERVICES_APPLICATION_1_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "ms1.perf.gore.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICGORE_PERF_MICROSERVICES_APPLICATION_1_PRIVATEIP}"]
}

resource "aws_route53_record" "ICGORE_PERF_MICROSERVICES_APPLICATION_2_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "ms2.perf.gore.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICGORE_PERF_MICROSERVICES_APPLICATION_2_PRIVATEIP}"]
}

resource "aws_route53_record" "ICGORE_PERF_INTERNAL_NLB_MICROSERVICES_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "ms.perf.gore.insurcloud.ca"
    type    = "A"
    alias {
        name                   = "${data.terraform_remote_state.dev_remote_state.ICGORE_PERF_INTERNAL_NLB_DNS_NAME}"
        zone_id                = "${data.terraform_remote_state.dev_remote_state.ICGORE_PERF_INTERNAL_NLB_ZONE_ID}"
        evaluate_target_health = false
    }
}

# resource "aws_route53_record" "ICGORE_PERF_MICROSERVICES_DB_RECORD" {
#     zone_id = "${var.zone_id}"
#     name    = "msdb.perf.gore.insurcloud.ca"
#     type    = "A"
#     records = ["${data.terraform_remote_state.dev_remote_state.ICGORE_PERF_GUIDEWIRE_MSSQL_DATABASE_PRIVATEIP}"]
# }

######## HAPROXY ##############
resource "aws_route53_record" "ICGORE_PERF_INTERNAL_HAPROXY_1_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "inthap1.perf.GORE.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICGORE_PERF_INTERNAL_HAPROXY_1_PRIVATEIP}"]
}

resource "aws_route53_record" "ICGORE_PERF_INTERNAL_HAPROXY_2_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "inthap2.perf.GORE.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICGORE_PERF_INTERNAL_HAPROXY_2_PRIVATEIP}"]
}

########## EXTERNAL LB ##############
resource "aws_route53_record" "ICGORE_PERF_EXTERNAL_ALB_MICROSERVICES_RECORD" {
    zone_id = "${var.public_zone_id}"
    name    = "ms-perf-ext.nonprod.gore.insurcloud.ca"
    type    = "CNAME"
    ttl     = "5"
    #external facing alb

    records = ["${aws_lb.ICGORE_PERF_NORTH_ALB.dns_name}"]
      
}

# #=======================================================================
# # DEV/QA
# #=======================================================================
######### OPENTEXT ##############
resource "aws_route53_record" "ICGORE_DEV_OPENTEXT_COM_SERVER_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "otcs.dev.gore.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICGORE_DEV_OPENTEXT_COM_SERVER_PRIVATEIP}"]
}

resource "aws_route53_record" "ICGORE_DEV_OT_MSSQL_DATABASE_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "otdb.dev.gore.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["goredev-mssql-otcs.cdilukawmllg.ca-central-1.rds.amazonaws.com"]
}
