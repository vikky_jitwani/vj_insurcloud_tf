resource "aws_security_group" "SG_JENKINS_MASTER" {
  name = "ICHUB-JENKINS-MASTER-SG"

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = "${var.default_cidr}"
  }

  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = "${var.default_cidr}"
  }

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = "${var.default_cidr}"
  }

  ingress {
    from_port   = 8080
    to_port     = 8080
    protocol    = "tcp"
    cidr_blocks = "${var.default_cidr}"
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    Name = "ICHUB-JENKINS-MASTER-SG"
  }
}

resource "aws_security_group" "SG_NEXUS" {
  name = "ICHUB-NEXUS-SG"

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = "${var.default_cidr}"
  }

  ingress {
    from_port   = 9443
    to_port     = 9456
    protocol    = "tcp"
    cidr_blocks = "${var.default_cidr}"
    description = "Docker Registries"
  }

  ingress {
    from_port   = 8081
    to_port     = 8081
    protocol    = "tcp"
    cidr_blocks = "${var.default_cidr}"
    description = "UI"
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    Name = "ICHUB-NEXUS-SG"
  }
}

resource "aws_security_group" "SG_SONARQUBE" {
  name = "ICHUB-SONARQUBE-SG"

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = "${var.default_cidr}"
  }

  ingress {
    from_port   = 9000
    to_port     = 9000
    protocol    = "tcp"
    cidr_blocks = ["${var.default_cidr}","${var.gore_cidr}","${var.portage_cidr}","${var.bcaa_cidr}"]
    description = "Web Server"
  }
   
  ingress {
    from_port   = 5432
    to_port     = 5432
    protocol    = "tcp"
    cidr_blocks = "${var.default_cidr}"
    description = "Postgres DB"
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    Name = "ICHUB-SONARQUBE-SG"
  }
}

resource "aws_security_group" "SG_CONSUL" {
  name = "ICHUB-CONSUL-SG"

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = "${var.default_cidr}"
  }

  ingress {
    from_port   = 8300
    to_port     = 8302
    protocol    = "tcp"
    cidr_blocks = "${var.default_cidr}"
  }

  ingress {
    from_port   = 8500
    to_port     = 8500
    protocol    = "tcp"
    cidr_blocks = "${var.default_cidr}"
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    Name = "ICHUB-CONSUL-SG"
  }
}

resource "aws_security_group" "SG_PING_TEST" {
  name = "ICHUB-PING-TEST"

  ingress {
    from_port       = -1
    to_port         = -1
    protocol        = "icmp"
    cidr_blocks     = ["${var.default_cidr}","${var.gore_cidr}","${var.portage_cidr}","${var.bcaa_cidr}"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    Name = "ICHUB-PING-TEST"
  }
}

resource "aws_security_group" "SG_HTTPS" {
  name = "ICHUB-HTTPS-SG"

  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["${var.default_cidr}","${var.gore_cidr}","${var.portage_cidr}","${var.bcaa_cidr}"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    Name = "ICHUB-HTTPS-SG"
  }
}

resource "aws_security_group" "SG_AVIATRIX_SSH" {
  name = "ICHUB-AVIATRIX-SSH-SG"

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = [
      "${var.aviatrix_cidr}"
    ]
    description = "SSH"
  }

  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    Name = "ICHUB-AVIATRIX-SSH-SG"
  }
}

resource "aws_security_group" "SG_AVIATRIX_RDP" {
  name = "ICHUB-AVIATRIX-RDP-SG"

  ingress {
    from_port   = 3389
    to_port     = 3389
    protocol    = "tcp"
    cidr_blocks = [
      "${var.aviatrix_cidr}"
    ]
    description = "SSH"
  }

  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    Name = "ICHUB-AVIATRIX-RDP-SG"
  }
}



resource "aws_security_group" "SG_CONDUIT_SERVER" {
    name        = "SG_CONDUIT_SERVER"
    description = "Security group for Conduit Server instances"
    vpc_id      = "${var.vpc_id}"

    ingress {
        from_port       = 8080
        to_port         = 8080
        protocol        = "tcp"
        cidr_blocks     = ["${var.default_cidr}","${var.gore_cidr}","${var.portage_cidr}","${var.bcaa_cidr}"]
        description     = "Web Server"
    }

    ingress {
        from_port       = 5432
        to_port         = 5432
        protocol        = "tcp"
        cidr_blocks     = ["${var.default_cidr}","${var.gore_cidr}","${var.portage_cidr}","${var.bcaa_cidr}"]
        description     = "PostgreSQL"
    }

    ingress {
        from_port       = 445
        to_port         = 445
        protocol        = "tcp"
        cidr_blocks     = ["${var.default_cidr}","${var.gore_cidr}","${var.portage_cidr}","${var.bcaa_cidr}"]
        description     = "Microsoft Active Directory or SMB"
    }

    ingress {
        from_port       = 445
        to_port         = 445
        protocol        = "udp"
        cidr_blocks     = ["${var.default_cidr}","${var.gore_cidr}","${var.portage_cidr}","${var.bcaa_cidr}"]
        description     = "Microsoft Active Directory or SMB"
    }

    ingress {
        from_port       = 139
        to_port         = 139
        protocol        = "tcp"
        cidr_blocks     = ["${var.default_cidr}","${var.gore_cidr}","${var.portage_cidr}","${var.bcaa_cidr}"]
        description     = "NetBIOS Session Service"
    }

    ingress {
        from_port       = 137
        to_port         = 138
        protocol        = "udp"
        cidr_blocks     = ["${var.default_cidr}","${var.gore_cidr}","${var.portage_cidr}","${var.bcaa_cidr}"]
        description     = "NetBIOS Name Service and Datagram Service"
    }

    ingress {
        from_port       = 80
        to_port         = 80
        protocol        = "tcp"
        cidr_blocks     = ["${var.default_cidr}","${var.gore_cidr}","${var.portage_cidr}","${var.bcaa_cidr}"]
        description     = "Dashboard"
    }

    egress {
        from_port       = 0
        to_port         = 0
        protocol        = "-1"
        cidr_blocks     = ["0.0.0.0/0"]
    }

    tags = {
        Application = "Conduit Server"
        BILLINGCODE = "FPE01903-CO-TE-TO-1000"
        BILLINGCONTACT = "marrojas@deloitte.ca"
        CLIENT = "CMS"
        COUNTRY = "CA"
        CSCLASS = "Confidential"
        CSQUAL = "CI/PI Data"
        CSTYPE = "External"
        ENVIRONMENT = "PRD"
        FUNCTION = "CON"
        GROUPCONTACT = "marrojas@deloitte.ca"
        MEMBERFIRM = "CA"
        PRIMARYCONTACT = "1600274034767856414"
        SECONDARYCONTACT = "marrojas@deloitte.ca"
    }
}

resource "aws_security_group" "SG_CONDUIT_DB" {
  name = "SG-CONDUIT-DB"

  ingress {
    from_port   = 5432
    to_port     = 5432
    protocol    = "tcp"
    cidr_blocks = ["${var.default_cidr}","${var.gore_cidr}","${var.portage_cidr}","${var.bcaa_cidr}"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    Name = "SG-CONDUIT-DB"
  }
}

resource "aws_security_group" "SG_CONDUIT_WORKER" {
    name        = "SG_CONDUIT_WORKER"
    description = "Security group for Conduit Worker instances"
    vpc_id      = "${var.vpc_id}"

    ingress {
        from_port       = 8080
        to_port         = 8080
        protocol        = "tcp"
        cidr_blocks     = ["${var.default_cidr}","${var.gore_cidr}","${var.portage_cidr}","${var.bcaa_cidr}"]
        description     = "Web Server"
    }

    egress {
        from_port       = 0
        to_port         = 0
        protocol        = "-1"
        cidr_blocks     = ["0.0.0.0/0"]
    }

    tags {
        "Application" = "Conduit Worker"
    }
}

### Route53 Resolver - IC HUB DNS ###
resource "aws_security_group" "SG_IC_HUB_DNS_RESOLVER_ENDPOINT" {
  name = "IC-HUB-DNS-RESOLVER-ENDPOINT-SG"

  ingress {
    from_port   = 53
    to_port     = 53
    protocol    = "tcp"
    cidr_blocks = [
      "10.0.0.0/8",
      "192.168.0.0/16"
    ]
    description = "DNS (TCP)"
  }

  ingress {
    from_port   = 53
    to_port     = 53
    protocol    = "udp"
    cidr_blocks = [
      "10.0.0.0/8",
      "192.168.0.0/16"
    ]
    description = "DNS (UDP)"
  }

  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    Name = "IC-HUB-DNS-RESOLVER-ENDPOINT-SG"
  }
}