#### MICROSERVICES DB #####
resource "aws_db_subnet_group" "MSSQL_DB_SUBNET" {
    name        = "mssql-db-subnet"
    description = "RDS subnet group"
    subnet_ids  = ["${var.commonwell-non-prod-database-1a-subnet}", "${var.commonwell-non-prod-database-1b-subnet}"]
}

#### JOB SCHEDULER DB #####
resource "aws_db_subnet_group" "MYSQL_DB_SUBNET" {
    name        = "mysql-db-subnet"
    description = "RDS subnet group"
    subnet_ids  = ["${var.commonwell-non-prod-database-1a-subnet}", "${var.commonwell-non-prod-database-1b-subnet}"]
}

#### HUBIO DB #####

resource "aws_db_subnet_group" "POSTGRES_DB_SUBNET" {
    name        = "postgres-db-subnet"
    description = "RDS subnet group"
    subnet_ids  = ["${var.commonwell-non-prod-database-1a-subnet}", "${var.commonwell-non-prod-database-1b-subnet}"]
}

resource "aws_db_instance" "mssqldb" {
  license_model       = "license-included"
  engine              = "sqlserver-web"
  engine_version      = "14.00.3049.1.v1"
  instance_class      = "db.r5.large"
  allocated_storage   = 100
  storage_encrypted   = true
  identifier          = "cmigdev01msdb"
  username            = "sa"
  password            = "${var.mssql-rds-password}" # password
  port                = 1433
  timezone            = "Eastern Standard Time"

  maintenance_window  = "Mon:00:00-Mon:03:00"
  backup_window       = "03:00-06:00"
  deletion_protection = true

  db_subnet_group_name    = "${aws_db_subnet_group.MSSQL_DB_SUBNET.name}"
  parameter_group_name    = "default.sqlserver-web-14.0"
  multi_az                = false # sqlserver-web does not support multi-az
  vpc_security_group_ids  = ["${aws_security_group.SG_MSSQL_DB.id}"]
  storage_type            = "gp2"
  backup_retention_period = 30 # how long you’re going to keep your backups
  kms_key_id              = "arn:aws:kms:ca-central-1:623885847088:key/e16e69e6-e148-4305-bf94-d779bd3fa04c"

  tags = {
    Name        = "cmig-dev01-msdb"
    Environment = "DEV"
  }
}

# JobScheduler Database
resource "aws_db_parameter_group" "jobscheduler" {
  name   = "cmigjobscheduler"
  family = "mysql8.0"

  parameter {
    name  = "time_zone"
    value = "US/Eastern" # MySQL has no America/Toronto time zone
  }

  parameter {
    name = "log_output"
    value = "FILE"
  }

  parameter {
    name = "general_log"
    value = "1"
  }

  parameter {
    name = "slow_query_log"
    value = "1"
  }

  parameter {
    name = "long_query_time"
    value = "2"
  }
}

resource "aws_db_instance" "mysqldb" {

    engine            = "mysql"
    engine_version    = "8.0.17"
    instance_class    = "db.t3.medium"
    allocated_storage = 100
    storage_encrypted = true
    identifier        = "cmigjobscheduler"
    name              = "cmigjobscheduler"
    username          = "sa"
    password          = "${var.mysql-rds-password}" # password
    port              = 3306

    maintenance_window = "Mon:00:00-Mon:03:00"
    backup_window      = "03:00-06:00"
    deletion_protection = true
    enabled_cloudwatch_logs_exports = ["error", "general", "slowquery"]
    apply_immediately   = true

    db_subnet_group_name  = "${aws_db_subnet_group.MYSQL_DB_SUBNET.name}"
    parameter_group_name  = "${aws_db_parameter_group.jobscheduler.id}"
    multi_az              = false # set to true to have high availability: 2 instances synchronized with each other
    vpc_security_group_ids  = ["${aws_security_group.SG_MYSQL_DB.id}"]
    storage_type            = "gp2"
    backup_retention_period = 30 # how long you’re going to keep your backups
    kms_key_id = "arn:aws:kms:ca-central-1:623885847088:key/e16e69e6-e148-4305-bf94-d779bd3fa04c"

    tags = {
        Name = "cmigjobscheduler"
        Environment = "DEV"
    }
}

# Hubio Database
resource "aws_db_parameter_group" "hubio" {
  name   = "cmigdev01hubio"
  family = "postgres10"

  parameter {
    name  = "timezone"
    value = "America/Toronto"
  }
}

resource "aws_db_instance" "postgresdb" {

    engine            = "postgres"
    engine_version    = "10.13"
    instance_class    = "db.t3.medium"
    allocated_storage = 100
    storage_encrypted = true
    identifier        = "cmigdev01hubio"
    name              = "cmigdev01hubio"
    username          = "sa"
    password          = "${var.postgres-rds-password}" # password
    port              = 5432

    maintenance_window = "Mon:00:00-Mon:03:00"
    backup_window      = "03:00-06:00"
    deletion_protection = true
    enabled_cloudwatch_logs_exports = ["postgresql", "upgrade"]

    db_subnet_group_name  = "${aws_db_subnet_group.POSTGRES_DB_SUBNET.name}"
    parameter_group_name  = "${aws_db_parameter_group.hubio.id}"
    multi_az              = false # set to true to have high availability: 2 instances synchronized with each other
    vpc_security_group_ids  = ["${aws_security_group.SG_POSTGRES_DB.id}"]
    storage_type            = "gp2"
    backup_retention_period = 30 # how long you’re going to keep your backups
    kms_key_id = "arn:aws:kms:ca-central-1:623885847088:key/e16e69e6-e148-4305-bf94-d779bd3fa04c"

    tags = {
        Name = "cmig-dev01-hubio"
        Environment = "DEV"
    }
}

