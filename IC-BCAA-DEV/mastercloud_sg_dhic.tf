resource "aws_security_group" "SG_DHIC" {
  name = "DHIC-SG"

  ingress {
    from_port   = 3500
    to_port     = 3500
    protocol    = "tcp"
    cidr_blocks = ["${var.bcaa_hub_cidr}"]
  }

  ingress {
    from_port   = 4000
    to_port     = 4000
    protocol    = "tcp"
    cidr_blocks = ["${var.bcaa_hub_cidr}"]
  }

  ingress {
    from_port   = 4001
    to_port     = 4001
    protocol    = "tcp"
    cidr_blocks = ["${var.bcaa_hub_cidr}"]
  }

  ingress {
    from_port   = 4010
    to_port     = 4010
    protocol    = "tcp"
    cidr_blocks = ["${var.bcaa_hub_cidr}"]
  }

  ingress {
    from_port   = 4011
    to_port     = 4011
    protocol    = "tcp"
    cidr_blocks = ["${var.bcaa_hub_cidr}"]
  }

  ingress {
    from_port   = 4012
    to_port     = 4012
    protocol    = "tcp"
    cidr_blocks = ["${var.bcaa_hub_cidr}"]
  }

  ingress {
    from_port   = 4013
    to_port     = 4013
    protocol    = "tcp"
    cidr_blocks = ["${var.bcaa_hub_cidr}"]
  }

  ingress {
    from_port   = 5001
    to_port     = 5001
    protocol    = "tcp"
    cidr_blocks = ["${var.bcaa_hub_cidr}"]
  }

  ingress {
    from_port   = 6400
    to_port     = 6400
    protocol    = "tcp"
    cidr_blocks = ["${var.bcaa_hub_cidr}"]
  }

  ingress {
    from_port   = 445
    to_port     = 445
    protocol    = "tcp"
    cidr_blocks = ["${var.bcaa_hub_cidr}"]
    description = "SMB - File Sharing"
  }

  ingress {
    from_port   = 445
    to_port     = 445
    protocol    = "tcp"
    cidr_blocks = ["10.41.8.76/32"]
    description = "SAP BODS - Job Server"
  }

  ingress {
    from_port   = 8080
    to_port     = 8080
    protocol    = "tcp"
    cidr_blocks = ["${var.bcaa_hub_cidr}"]
  }

  ingress {
    from_port   = 49152
    to_port     = 65535
    protocol    = "tcp"
    cidr_blocks = ["${var.bcaa_hub_cidr}"]
    description = "Connection between the SAP BODS client and the server"
  }

  # SAP Server #
  ingress {
    from_port   = 3500
    to_port     = 3500
    protocol    = "tcp"
    cidr_blocks = ["10.41.8.76/32"]
    description = "SAP Server"
  }

  ingress {
    from_port   = 4000
    to_port     = 4000
    protocol    = "tcp"
    cidr_blocks = ["10.41.8.76/32"]
    description = "SAP Server"
  }

  ingress {
    from_port   = 4001
    to_port     = 4001
    protocol    = "tcp"
    cidr_blocks = ["10.41.8.76/32"]
    description = "SAP Server"
  }

  ingress {
    from_port   = 4010
    to_port     = 4010
    protocol    = "tcp"
    cidr_blocks = ["10.41.8.76/32"]
    description = "SAP Server"
  }

  ingress {
    from_port   = 4011
    to_port     = 4011
    protocol    = "tcp"
    cidr_blocks = ["10.41.8.76/32"]
    description = "SAP Server"
  }

  ingress {
    from_port   = 4012
    to_port     = 4012
    protocol    = "tcp"
    cidr_blocks = ["10.41.8.76/32"]
    description = "SAP Server"
  }

  ingress {
    from_port   = 4013
    to_port     = 4013
    protocol    = "tcp"
    cidr_blocks = ["10.41.8.76/32"]
    description = "SAP Server"
  }

  ingress {
    from_port   = 5001
    to_port     = 5001
    protocol    = "tcp"
    cidr_blocks = ["10.41.8.76/32"]
    description = "SAP Server"
  }

  ingress {
    from_port   = 6400
    to_port     = 6400
    protocol    = "tcp"
    cidr_blocks = ["10.41.8.76/32"]
    description = "SAP Server"
  }

  ingress {
    from_port   = 8080
    to_port     = 8080
    protocol    = "tcp"
    cidr_blocks = ["10.41.8.76/32"]
    description = "SAP Server"
  }

  ingress {
    from_port   = 49152
    to_port     = 65535
    protocol    = "tcp"
    cidr_blocks = ["10.41.8.76/32"]
    description = "SAP Server"
  }

  ingress {
    from_port   = 1433
    to_port     = 1433
    protocol    = "tcp"
    cidr_blocks = ["10.41.8.76/32"]
    description = "SAP Server"
  }

  ingress {
    from_port   = 1433
    to_port     = 1433
    protocol    = "tcp"
    cidr_blocks = ["${var.bcaa_workspaces}"]
    description = "BCAA AWS Workspaces"
  }


  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    "Name" = "DHIC-SG"
  }

  ingress {
    from_port   = 49152
    to_port     = 65535
    protocol    = "tcp"
    cidr_blocks = ["${var.bcaa_dhic_sap}"]
    description = "SAP client to SAP server"
  }

  tags {
    BILLINGCODE = "FPE01903-CO-TE-TO-1000"
    BILLINGCONTACT = "marrojas@deloitte.ca"
    COUNTRY = "CA"
    CSCLASS = "Confidential"
    CSQUAL = "CI/PI Data"
    CSTYPE = "External"
    ENVIRONMENT = "NPD"
    FUNCTION = "CON"
    MEMBERFIRM = "CA"
    PRIMARYCONTACT = "marrojas@deloitte.ca"
    SECONDARYCONTACT = "Samir Modh"
  }
}