//resource "aws_lambda_function" "peelprod-APIGatewayAuthenrizer_Hub" {
//  function_name = "eco-RStudioExtract"
//  role          = "arn:aws:iam::${var.portage-dev-account}:role/LambdaApplication"
//  handler       = "lambda_function.lambda_handler"
//  runtime       = "python3.8"
//
//  environment {
//    variables = {
//      MAX_ROWS
//    }
//  }
//}

resource "aws_lambda_function" "ic-portage-dev-lambda-ms-lambda-function" {
  role          = "arn:aws:iam::${var.portage-dev-account}:role/Portage-LambdaBasicExecutionRoleForCreatingFunctions"
  handler       = "lambda_function.lambda_handler"
  function_name = "lambda-function-terraform"
  runtime       = "python3.7"
  timeout       = 60
  s3_bucket     = "ic-portage-dev-lambda-ms"
  s3_key        = "documentLambda.zip"
  description   = "Microservices lambda function for upload documents"

  tags = {
    owner = "Managed by Terraform"
  }
}