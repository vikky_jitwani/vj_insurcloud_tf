provider "aws" {
  region     = "ca-central-1"
  assume_role = {
    role_arn = "arn:aws:iam::342339668068:role/admin_ops_auto"
  }
}

terraform {
 backend "s3" {
    bucket     = "a-c-mc-p-a005-portage-tfstate"
    key        = "IC-Portage-HUB/STATE/current.tf"
    region     = "ca-central-1"
    # role_arn       = "arn:aws:iam::393766723611:role/admin_ops_auto"
      #encrypt    = true
      #kms_key_id = "arn:aws:kms:ca-central-1::key/"
  }
}

data "terraform_remote_state" "dev_remote_state" {
  backend = "s3"
  config = {
    bucket     = "a-c-mc-p-a005-portage-tfstate"
    key        = "IC-Portage-DEV/STATE/current.tf"
    region     = "ca-central-1"
    # role_arn   = "arn:aws:iam::393766723611:role/admin_ops_auto"
  }
}

data "terraform_remote_state" "prod_remote_state" {
  backend = "s3"
  config = {
    bucket     = "a-c-mc-p-a005-portage-tfstate"
    key        = "IC-Portage-PROD/STATE/current.tf"
    region     = "ca-central-1"
    # role_arn   = "arn:aws:iam::393766723611:role/admin_ops_auto"
  }
}