resource "aws_s3_bucket" "ICECONOMICAL_HUB_GW_PROPERTIES" {
  bucket = "eco-gw-properties"
  acl = "private"
  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm = "AES256"
      }
    }
  }

  lifecycle_rule {
    enabled = true

    transition {
      days = 30
      storage_class = "STANDARD_IA"
    }

  }
  tags = {
    Contract = "MANAGED_SERVICES"
    Name = "eco-gw-properties"
  }
}

resource "aws_s3_bucket_policy" "ICECONOMICAL_HUB_GW_PROPERTIES_BUCKET_POLICY" {
  bucket = "${aws_s3_bucket.ICECONOMICAL_HUB_GW_PROPERTIES.id}"

  policy = <<POLICY
{
    "Version": "2012-10-17",
    "Id": "ICECONOMICAL_HUB_GW_PROPERTIES_BUCKET_POLICY",
    "Statement": [
        {
            "Sid": "DenyPublicReadACL",
            "Effect": "Deny",
            "Principal": {
                "AWS": "*"
            },
            "Action": [
                "s3:PutObject",
                "s3:PutObjectAcl"
            ],
            "Resource": "arn:aws:s3:::eco-gw-properties/*",
            "Condition": {
                "StringEquals": {
                    "s3:x-amz-acl": [
                        "public-read",
                        "public-read-write",
                        "authenticated-read"
                    ]
                }
            }
        },
        {
            "Sid": "DenyPublicReadGrant",
            "Effect": "Deny",
            "Principal": {
                "AWS": "*"
            },
            "Action": [
                "s3:PutObject",
                "s3:PutObjectAcl"
            ],
            "Resource": "arn:aws:s3:::eco-gw-properties/*",
            "Condition": {
                "StringLike": {
                    "s3:x-amz-grant-read": [
                        "*http://acs.amazonaws.com/groups/global/AllUsers*",
                        "*http://acs.amazonaws.com/groups/global/AuthenticatedUsers*"
                    ]
                }
            }
        },
        {
            "Sid": "DenyPublicListACL",
            "Effect": "Deny",
            "Principal": {
                "AWS": "*"
            },
            "Action": "s3:PutBucketAcl",
            "Resource": "arn:aws:s3:::eco-gw-properties",
            "Condition": {
                "StringEquals": {
                    "s3:x-amz-acl": [
                        "public-read",
                        "public-read-write",
                        "authenticated-read"
                    ]
                }
            }
        },
        {
            "Sid": "DenyPublicListGrant",
            "Effect": "Deny",
            "Principal": {
                "AWS": "*"
            },
            "Action": "s3:PutBucketAcl",
            "Resource": "arn:aws:s3:::eco-gw-properties",
            "Condition": {
                "StringLike": {
                    "s3:x-amz-grant-read": [
                        "*http://acs.amazonaws.com/groups/global/AllUsers*",
                        "*http://acs.amazonaws.com/groups/global/AuthenticatedUsers*"
                    ]
                }
            }
        },
        {
            "Sid": "DenyPublicReadConsole",
            "Effect": "Deny",
            "Principal": {
                "AWS": "*"
            },
            "Action": [
                "s3:PutObjectAcl",
                "s3:PutBucketAcl"
            ],
            "Resource": [
                "arn:aws:s3:::eco-gw-properties/*",
                "arn:aws:s3:::eco-gw-properties"
            ],
            "Condition": {
                "Null": {
                    "s3:x-amz-acl": "true",
                    "s3:x-amz-grant-write-acp": "true",
                    "s3:x-amz-grant-read-acp": "true",
                    "s3:x-amz-grant-full-control": "true",
                    "s3:x-amz-grant-write": "true",
                    "s3:x-amz-grant-read": "true"
                }
            }
        },
        {
            "Sid": "DevInstanceListBucketAllow",
            "Effect": "Allow",
            "Principal": {
                "AWS": "arn:aws:iam::632109065501:role/CMS-SSMRole"
            },
            "Action": "s3:ListBucket",
            "Resource": "arn:aws:s3:::eco-gw-properties"
        },
        {
            "Sid": "DevInstanceObjectAllow",
            "Effect": "Allow",
            "Principal": {
                "AWS": "arn:aws:iam::632109065501:role/CMS-SSMRole"
            },
            "Action": [
                "s3:GetObject",
                "s3:PutObject",
                "s3:DeleteObject"
            ],
            "Resource": [
                "arn:aws:s3:::eco-gw-properties/DEV*/*",
                "arn:aws:s3:::eco-gw-properties/ST*/*",
                "arn:aws:s3:::eco-gw-properties/UAT*/*",
                "arn:aws:s3:::eco-gw-properties/PRF*/*",
                "arn:aws:s3:::eco-gw-properties/SIT*/*",
                "arn:aws:s3:::eco-gw-properties/PPS*/*",
                "arn:aws:s3:::eco-gw-properties/EF*/*"
            ]
        },
        {
            "Sid": "HubInstanceListBucketAllow",
            "Effect": "Allow",
            "Principal": {
                "AWS": "arn:aws:iam::348868695504:role/CMS-SSMRole"
            },
            "Action": "s3:ListBucket",
            "Resource": "arn:aws:s3:::eco-gw-properties"
        },
        {
            "Sid": "HubInstanceGetObjectAllow",
            "Effect": "Allow",
            "Principal": {
                "AWS": "arn:aws:iam::348868695504:role/CMS-SSMRole"
            },
            "Action": [
                "s3:GetObject",
                "s3:PutObject",
                "s3:DeleteObject"
            ],
            "Resource": [
                "arn:aws:s3:::eco-gw-properties/DEV*/*",
                "arn:aws:s3:::eco-gw-properties/ST*/*",
                "arn:aws:s3:::eco-gw-properties/UAT*/*",
                "arn:aws:s3:::eco-gw-properties/PRF*/*",
                "arn:aws:s3:::eco-gw-properties/SIT*/*",
                "arn:aws:s3:::eco-gw-properties/PPS*/*",
                "arn:aws:s3:::eco-gw-properties/EF*/*",
                "arn:aws:s3:::eco-gw-properties/PROD*/*"
            ]
        },
        {
            "Sid": "ProdInstanceListBucketAllow",
            "Effect": "Allow",
            "Principal": {
                "AWS": "arn:aws:iam::852868701745:role/CMS-SSMRole"
            },
            "Action": "s3:ListBucket",
            "Resource": "arn:aws:s3:::eco-gw-properties"
        },
        {
            "Sid": "ProdInstanceObjectAllow",
            "Effect": "Allow",
            "Principal": {
                "AWS": "arn:aws:iam::852868701745:role/CMS-SSMRole"
            },
            "Action": [
                "s3:GetObject",
                "s3:PutObject",
                "s3:DeleteObject"
            ],
            "Resource": "arn:aws:s3:::eco-gw-properties/PROD*/*"
        }
    ]
}
POLICY
}

resource "aws_s3_bucket" "ICECONOMICAL_HUB_MS_PROPERTIES" {
  bucket = "eco-ms-properties"
  acl = "private"
  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm = "AES256"
      }
    }
  }

  lifecycle_rule {
    enabled = true

    transition {
      days = 30
      storage_class = "STANDARD_IA"
    }

  }
  tags = {
    Contract = "MANAGED_SERVICES"
    Name = "eco-ms-properties"
  }
}

resource "aws_s3_bucket_policy" "ICECONOMICAL_HUB_MS_PROPERTIES_BUCKET_POLICY" {
  bucket = "eco-ms-properties"

  policy = <<POLICY
{
    "Version": "2012-10-17",
    "Id": "ICECONOMICAL_HUB_MS_PROPERTIES_BUCKET_POLICY",
    "Statement": [
        {
            "Sid": "DenyPublicReadACL",
            "Effect": "Deny",
            "Principal": {
                "AWS": "*"
            },
            "Action": [
                "s3:PutObject",
                "s3:PutObjectAcl"
            ],
            "Resource": "arn:aws:s3:::eco-ms-properties/*",
            "Condition": {
                "StringEquals": {
                    "s3:x-amz-acl": [
                        "public-read",
                        "public-read-write",
                        "authenticated-read"
                    ]
                }
            }
        },
        {
            "Sid": "DenyPublicReadGrant",
            "Effect": "Deny",
            "Principal": {
                "AWS": "*"
            },
            "Action": [
                "s3:PutObject",
                "s3:PutObjectAcl"
            ],
            "Resource": "arn:aws:s3:::eco-ms-properties/*",
            "Condition": {
                "StringLike": {
                    "s3:x-amz-grant-read": [
                        "*http://acs.amazonaws.com/groups/global/AllUsers*",
                        "*http://acs.amazonaws.com/groups/global/AuthenticatedUsers*"
                    ]
                }
            }
        },
        {
            "Sid": "DenyPublicListACL",
            "Effect": "Deny",
            "Principal": {
                "AWS": "*"
            },
            "Action": "s3:PutBucketAcl",
            "Resource": "arn:aws:s3:::eco-ms-properties",
            "Condition": {
                "StringEquals": {
                    "s3:x-amz-acl": [
                        "public-read",
                        "public-read-write",
                        "authenticated-read"
                    ]
                }
            }
        },
        {
            "Sid": "DenyPublicListGrant",
            "Effect": "Deny",
            "Principal": {
                "AWS": "*"
            },
            "Action": "s3:PutBucketAcl",
            "Resource": "arn:aws:s3:::eco-ms-properties",
            "Condition": {
                "StringLike": {
                    "s3:x-amz-grant-read": [
                        "*http://acs.amazonaws.com/groups/global/AllUsers*",
                        "*http://acs.amazonaws.com/groups/global/AuthenticatedUsers*"
                    ]
                }
            }
        },
        {
            "Sid": "DenyPublicReadConsole",
            "Effect": "Deny",
            "Principal": {
                "AWS": "*"
            },
            "Action": [
                "s3:PutObjectAcl",
                "s3:PutBucketAcl"
            ],
            "Resource": [
                "arn:aws:s3:::eco-ms-properties/*",
                "arn:aws:s3:::eco-ms-properties"
            ],
            "Condition": {
                "Null": {
                    "s3:x-amz-grant-write": "true",
                    "s3:x-amz-grant-read": "true",
                    "s3:x-amz-acl": "true",
                    "s3:x-amz-grant-read-acp": "true",
                    "s3:x-amz-grant-full-control": "true",
                    "s3:x-amz-grant-write-acp": "true"
                }
            }
        },
        {
            "Sid": "DevInstanceListBucketAllow",
            "Effect": "Allow",
            "Principal": {
                "AWS": "arn:aws:iam::632109065501:role/CMS-SSMRole"
            },
            "Action": "s3:ListBucket",
            "Resource": "arn:aws:s3:::eco-ms-properties"
        },
        {
            "Sid": "DevInstanceObjectAllow",
            "Effect": "Allow",
            "Principal": {
                "AWS": "arn:aws:iam::632109065501:role/CMS-SSMRole"
            },
            "Action": [
                "s3:GetObject",
                "s3:PutObject",
                "s3:DeleteObject"
            ],
            "Resource": [
                "arn:aws:s3:::eco-ms-properties/DEV*/*",
                "arn:aws:s3:::eco-ms-properties/ST*/*",
                "arn:aws:s3:::eco-ms-properties/UAT*/*",
                "arn:aws:s3:::eco-ms-properties/PRF*/*",
                "arn:aws:s3:::eco-ms-properties/SIT*/*",
                "arn:aws:s3:::eco-ms-properties/PPS*/*",
                "arn:aws:s3:::eco-ms-properties/EF*/*"
            ]
        },
        {
            "Sid": "HubInstanceListBucketAllow",
            "Effect": "Allow",
            "Principal": {
                "AWS": "arn:aws:iam::348868695504:role/CMS-SSMRole"
            },
            "Action": "s3:ListBucket",
            "Resource": "arn:aws:s3:::eco-ms-properties"
        },
        {
            "Sid": "HubInstanceObjectAllow",
            "Effect": "Allow",
            "Principal": {
                "AWS": "arn:aws:iam::348868695504:role/CMS-SSMRole"
            },
            "Action": [
                "s3:GetObject",
                "s3:PutObject",
                "s3:DeleteObject"
            ],
            "Resource": [
                "arn:aws:s3:::eco-ms-properties/DEV*/*",
                "arn:aws:s3:::eco-ms-properties/ST*/*",
                "arn:aws:s3:::eco-ms-properties/UAT*/*",
                "arn:aws:s3:::eco-ms-properties/PRF*/*",
                "arn:aws:s3:::eco-ms-properties/SIT*/*",
                "arn:aws:s3:::eco-ms-properties/PPS*/*",
                "arn:aws:s3:::eco-ms-properties/EF*/*",
                "arn:aws:s3:::eco-ms-properties/PROD*/*"
            ]
        },
        {
            "Sid": "ProdInstanceListBucketAllow",
            "Effect": "Allow",
            "Principal": {
                "AWS": "arn:aws:iam::852868701745:role/CMS-SSMRole"
            },
            "Action": "s3:ListBucket",
            "Resource": "arn:aws:s3:::eco-ms-properties"
        },
        {
            "Sid": "ProdInstanceObjectAllow",
            "Effect": "Allow",
            "Principal": {
                "AWS": "arn:aws:iam::852868701745:role/CMS-SSMRole"
            },
            "Action": [
                "s3:GetObject",
                "s3:PutObject",
                "s3:DeleteObject"
            ],
            "Resource": [
                "arn:aws:s3:::eco-ms-properties/PROD*/*"
            ]
        }
    ]
}
POLICY
}

resource "aws_s3_bucket" "ICECONOMICAL_HUB_MS_LIB_PROPERTIES" {
  bucket = "eco-ms-lib-properties"
  acl = "private"
  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm = "AES256"
      }
    }
  }

  lifecycle_rule {
    enabled = true

    transition {
      days = 30
      storage_class = "STANDARD_IA"
    }

  }
  tags = {
    Contract = "MANAGED_SERVICES"
    Name = "eco-ms-lib-properties"
  }
}

resource "aws_s3_bucket_policy" "ICECONOMICAL_HUB_MS_LIB_PROPERTIES_BUCKET_POLICY" {
  bucket = "eco-ms-lib-properties"

  policy = <<POLICY
{
    "Version": "2012-10-17",
    "Id": "ICECONOMICAL_HUB_MS_LIB_PROPERTIES_BUCKET_POLICY",
    "Statement": [
        {
            "Sid": "DenyPublicReadACL",
            "Effect": "Deny",
            "Principal": {
                "AWS": "*"
            },
            "Action": [
                "s3:PutObject",
                "s3:PutObjectAcl"
            ],
            "Resource": "arn:aws:s3:::eco-ms-lib-properties/*",
            "Condition": {
                "StringEquals": {
                    "s3:x-amz-acl": [
                        "public-read",
                        "public-read-write",
                        "authenticated-read"
                    ]
                }
            }
        },
        {
            "Sid": "DenyPublicReadGrant",
            "Effect": "Deny",
            "Principal": {
                "AWS": "*"
            },
            "Action": [
                "s3:PutObject",
                "s3:PutObjectAcl"
            ],
            "Resource": "arn:aws:s3:::eco-ms-lib-properties/*",
            "Condition": {
                "StringLike": {
                    "s3:x-amz-grant-read": [
                        "*http://acs.amazonaws.com/groups/global/AllUsers*",
                        "*http://acs.amazonaws.com/groups/global/AuthenticatedUsers*"
                    ]
                }
            }
        },
        {
            "Sid": "DenyPublicListACL",
            "Effect": "Deny",
            "Principal": {
                "AWS": "*"
            },
            "Action": "s3:PutBucketAcl",
            "Resource": "arn:aws:s3:::eco-ms-lib-properties",
            "Condition": {
                "StringEquals": {
                    "s3:x-amz-acl": [
                        "public-read",
                        "public-read-write",
                        "authenticated-read"
                    ]
                }
            }
        },
        {
            "Sid": "DenyPublicListGrant",
            "Effect": "Deny",
            "Principal": {
                "AWS": "*"
            },
            "Action": "s3:PutBucketAcl",
            "Resource": "arn:aws:s3:::eco-ms-lib-properties",
            "Condition": {
                "StringLike": {
                    "s3:x-amz-grant-read": [
                        "*http://acs.amazonaws.com/groups/global/AllUsers*",
                        "*http://acs.amazonaws.com/groups/global/AuthenticatedUsers*"
                    ]
                }
            }
        },
        {
            "Sid": "DenyPublicReadConsole",
            "Effect": "Deny",
            "Principal": {
                "AWS": "*"
            },
            "Action": [
                "s3:PutObjectAcl",
                "s3:PutBucketAcl"
            ],
            "Resource": [
                "arn:aws:s3:::eco-ms-lib-properties/*",
                "arn:aws:s3:::eco-ms-lib-properties"
            ],
            "Condition": {
                "Null": {
                    "s3:x-amz-grant-write": "true",
                    "s3:x-amz-grant-read": "true",
                    "s3:x-amz-acl": "true",
                    "s3:x-amz-grant-read-acp": "true",
                    "s3:x-amz-grant-full-control": "true",
                    "s3:x-amz-grant-write-acp": "true"
                }
            }
        },
        {
            "Sid": "DevInstanceListBucketAllow",
            "Effect": "Allow",
            "Principal": {
                "AWS": "arn:aws:iam::632109065501:role/CMS-SSMRole"
            },
            "Action": "s3:ListBucket",
            "Resource": "arn:aws:s3:::eco-ms-lib-properties"
        },
        {
            "Sid": "DevInstanceObjectAllow",
            "Effect": "Allow",
            "Principal": {
                "AWS": "arn:aws:iam::632109065501:role/CMS-SSMRole"
            },
            "Action": [
                "s3:GetObject",
                "s3:PutObject",
                "s3:DeleteObject"
            ],
            "Resource": [
                "arn:aws:s3:::eco-ms-lib-properties/DEV*/*",
                "arn:aws:s3:::eco-ms-lib-properties/ST*/*",
                "arn:aws:s3:::eco-ms-lib-properties/UAT*/*",
                "arn:aws:s3:::eco-ms-lib-properties/PRF*/*",
                "arn:aws:s3:::eco-ms-lib-properties/SIT*/*",
                "arn:aws:s3:::eco-ms-lib-properties/PPS*/*",
                "arn:aws:s3:::eco-ms-lib-properties/EF*/*"
            ]
        },
        {
            "Sid": "HubInstanceListBucketAllow",
            "Effect": "Allow",
            "Principal": {
                "AWS": "arn:aws:iam::348868695504:role/CMS-SSMRole"
            },
            "Action": "s3:ListBucket",
            "Resource": "arn:aws:s3:::eco-ms-lib-properties"
        },
        {
            "Sid": "HubInstanceObjectAllow",
            "Effect": "Allow",
            "Principal": {
                "AWS": "arn:aws:iam::348868695504:role/CMS-SSMRole"
            },
            "Action": [
                "s3:GetObject",
                "s3:PutObject",
                "s3:DeleteObject"
            ],
            "Resource": [
                "arn:aws:s3:::eco-ms-lib-properties/DEV*/*",
                "arn:aws:s3:::eco-ms-lib-properties/ST*/*",
                "arn:aws:s3:::eco-ms-lib-properties/UAT*/*",
                "arn:aws:s3:::eco-ms-lib-properties/PRF*/*",
                "arn:aws:s3:::eco-ms-lib-properties/SIT*/*",
                "arn:aws:s3:::eco-ms-lib-properties/PPS*/*",
                "arn:aws:s3:::eco-ms-lib-properties/EF*/*",
                "arn:aws:s3:::eco-ms-lib-properties/PROD*/*"
            ]
        },
        {
            "Sid": "ProdInstanceListBucketAllow",
            "Effect": "Allow",
            "Principal": {
                "AWS": "arn:aws:iam::852868701745:role/CMS-SSMRole"
            },
            "Action": "s3:ListBucket",
            "Resource": "arn:aws:s3:::eco-ms-lib-properties"
        },
        {
            "Sid": "ProdInstanceObjectAllow",
            "Effect": "Allow",
            "Principal": {
                "AWS": "arn:aws:iam::852868701745:role/CMS-SSMRole"
            },
            "Action": [
                "s3:GetObject",
                "s3:PutObject",
                "s3:DeleteObject"
            ],
            "Resource": [
                "arn:aws:s3:::eco-ms-lib-properties/PROD*/*"
            ]
        }
    ]
}
POLICY
}

resource "aws_s3_bucket" "ICECONOMICAL_HUB_MON_PROM" {
  bucket = "eco-monitoring-prometheus"
  acl = "private"
  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm = "AES256"
      }
    }
  }

  lifecycle_rule {
    enabled = true

    transition {
      days = 30
      storage_class = "STANDARD_IA"
    }

  }
  tags = {
    Contract = "MANAGED_SERVICES"
    Name = "eco-monitoring-prometheus"
  }
}

resource "aws_s3_bucket_policy" "ICECONOMICAL_HUB_MON_PROM_BUCKET_POLICY" {
  bucket = "eco-monitoring-prometheus"

  policy = <<POLICY
{
    "Version": "2012-10-17",
    "Id": "ICECONOMICAL_HUB_MON_PROM_BUCKET_POLICY",
    "Statement": [
        {
            "Sid": "DenyPublicReadACL",
            "Effect": "Deny",
            "Principal": {
                "AWS": "*"
            },
            "Action": [
                "s3:PutObject",
                "s3:PutObjectAcl"
            ],
            "Resource": "arn:aws:s3:::eco-monitoring-prometheus/*",
            "Condition": {
                "StringEquals": {
                    "s3:x-amz-acl": [
                        "public-read",
                        "public-read-write",
                        "authenticated-read"
                    ]
                }
            }
        },
        {
            "Sid": "DenyPublicReadGrant",
            "Effect": "Deny",
            "Principal": {
                "AWS": "*"
            },
            "Action": [
                "s3:PutObject",
                "s3:PutObjectAcl"
            ],
            "Resource": "arn:aws:s3:::eco-monitoring-prometheus/*",
            "Condition": {
                "StringLike": {
                    "s3:x-amz-grant-read": [
                        "*http://acs.amazonaws.com/groups/global/AllUsers*",
                        "*http://acs.amazonaws.com/groups/global/AuthenticatedUsers*"
                    ]
                }
            }
        },
        {
            "Sid": "DenyPublicListACL",
            "Effect": "Deny",
            "Principal": {
                "AWS": "*"
            },
            "Action": "s3:PutBucketAcl",
            "Resource": "arn:aws:s3:::eco-monitoring-prometheus",
            "Condition": {
                "StringEquals": {
                    "s3:x-amz-acl": [
                        "public-read",
                        "public-read-write",
                        "authenticated-read"
                    ]
                }
            }
        },
        {
            "Sid": "DenyPublicListGrant",
            "Effect": "Deny",
            "Principal": {
                "AWS": "*"
            },
            "Action": "s3:PutBucketAcl",
            "Resource": "arn:aws:s3:::eco-monitoring-prometheus",
            "Condition": {
                "StringLike": {
                    "s3:x-amz-grant-read": [
                        "*http://acs.amazonaws.com/groups/global/AllUsers*",
                        "*http://acs.amazonaws.com/groups/global/AuthenticatedUsers*"
                    ]
                }
            }
        },
        {
            "Sid": "DenyPublicReadConsole",
            "Effect": "Deny",
            "Principal": {
                "AWS": "*"
            },
            "Action": [
                "s3:PutObjectAcl",
                "s3:PutBucketAcl"
            ],
            "Resource": [
                "arn:aws:s3:::eco-monitoring-prometheus/*",
                "arn:aws:s3:::eco-monitoring-prometheus"
            ],
            "Condition": {
                "Null": {
                    "s3:x-amz-grant-write": "true",
                    "s3:x-amz-grant-read": "true",
                    "s3:x-amz-acl": "true",
                    "s3:x-amz-grant-read-acp": "true",
                    "s3:x-amz-grant-full-control": "true",
                    "s3:x-amz-grant-write-acp": "true"
                }
            }
        },
        {
            "Sid": "DevInstanceListBucketAllow",
            "Effect": "Allow",
            "Principal": {
                "AWS": "arn:aws:iam::632109065501:role/CMS-SSMRole"
            },
            "Action": "s3:ListBucket",
            "Resource": "arn:aws:s3:::eco-monitoring-prometheus"
        },
        {
            "Sid": "DevInstanceObjectAllow",
            "Effect": "Allow",
            "Principal": {
                "AWS": "arn:aws:iam::632109065501:role/CMS-SSMRole"
            },
            "Action": [
                "s3:GetObject",
                "s3:PutObject",
                "s3:DeleteObject"
            ],
            "Resource": [
                "arn:aws:s3:::eco-monitoring-prometheus/*"
            ]
        },
        {
            "Sid": "ProdInstanceListBucketAllow",
            "Effect": "Allow",
            "Principal": {
                "AWS": "arn:aws:iam::852868701745:role/CMS-SSMRole"
            },
            "Action": "s3:ListBucket",
            "Resource": "arn:aws:s3:::eco-monitoring-prometheus"
        },
        {
            "Sid": "ProdInstanceObjectAllow",
            "Effect": "Allow",
            "Principal": {
                "AWS": "arn:aws:iam::852868701745:role/CMS-SSMRole"
            },
            "Action": [
                "s3:GetObject",
                "s3:PutObject",
                "s3:DeleteObject"
            ],
            "Resource": [
                "arn:aws:s3:::eco-monitoring-prometheus/*"
            ]
        }
    ]
}
POLICY
}

resource "aws_s3_bucket" "ICECONOMICAL_HUB_RPA_FILES" {
  bucket = "eco-devops-rpa-files"
  acl = "private"
  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm = "AES256"
      }
    }
  }

  lifecycle_rule {
    enabled = true

    transition {
      days = 30
      storage_class = "STANDARD_IA"
    }
  }
  tags = {
    Contract = "MANAGED_SERVICES"
    Name = "eco-devops-rpa-files"
  }
}

resource "aws_s3_bucket_policy" "ICECONOMICAL_HUB_RPA_FILES_BUCKET_POLICY" {
  bucket = "eco-devops-rpa-files"

  depends_on = [
    "aws_s3_bucket.ICECONOMICAL_HUB_RPA_FILES"
  ]

  policy = <<POLICY
{
    "Version": "2012-10-17",
    "Id": "ICECONOMICAL_HUB_RPA_FILES_BUCKET_POLICY",
    "Statement": [
        {
            "Sid": "DenyPublicReadACL",
            "Effect": "Deny",
            "Principal": {
                "AWS": "*"
            },
            "Action": [
                "s3:PutObject",
                "s3:PutObjectAcl"
            ],
            "Resource": "arn:aws:s3:::eco-devops-rpa-files/*",
            "Condition": {
                "StringEquals": {
                    "s3:x-amz-acl": [
                        "public-read",
                        "public-read-write",
                        "authenticated-read"
                    ]
                }
            }
        },
        {
            "Sid": "DenyPublicReadGrant",
            "Effect": "Deny",
            "Principal": {
                "AWS": "*"
            },
            "Action": [
                "s3:PutObject",
                "s3:PutObjectAcl"
            ],
            "Resource": "arn:aws:s3:::eco-devops-rpa-files/*",
            "Condition": {
                "StringLike": {
                    "s3:x-amz-grant-read": [
                        "*http://acs.amazonaws.com/groups/global/AllUsers*",
                        "*http://acs.amazonaws.com/groups/global/AuthenticatedUsers*"
                    ]
                }
            }
        },
        {
            "Sid": "DenyPublicListACL",
            "Effect": "Deny",
            "Principal": {
                "AWS": "*"
            },
            "Action": "s3:PutBucketAcl",
            "Resource": "arn:aws:s3:::eco-devops-rpa-files",
            "Condition": {
                "StringEquals": {
                    "s3:x-amz-acl": [
                        "public-read",
                        "public-read-write",
                        "authenticated-read"
                    ]
                }
            }
        },
        {
            "Sid": "DenyPublicListGrant",
            "Effect": "Deny",
            "Principal": {
                "AWS": "*"
            },
            "Action": "s3:PutBucketAcl",
            "Resource": "arn:aws:s3:::eco-devops-rpa-files",
            "Condition": {
                "StringLike": {
                    "s3:x-amz-grant-read": [
                        "*http://acs.amazonaws.com/groups/global/AllUsers*",
                        "*http://acs.amazonaws.com/groups/global/AuthenticatedUsers*"
                    ]
                }
            }
        },
        {
            "Sid": "DenyPublicReadConsole",
            "Effect": "Deny",
            "Principal": {
                "AWS": "*"
            },
            "Action": [
                "s3:PutObjectAcl",
                "s3:PutBucketAcl"
            ],
            "Resource": [
                "arn:aws:s3:::eco-devops-rpa-files/*",
                "arn:aws:s3:::eco-devops-rpa-files"
            ],
            "Condition": {
                "Null": {
                    "s3:x-amz-grant-write": "true",
                    "s3:x-amz-grant-read-acp": "true",
                    "s3:x-amz-grant-write-acp": "true",
                    "s3:x-amz-grant-full-control": "true",
                    "s3:x-amz-acl": "true",
                    "s3:x-amz-grant-read": "true"
                }
            }
        },
        {
            "Sid": "HubDevelopersListBucketAllow",
            "Effect": "Allow",
            "Principal": {
                "AWS": "arn:aws:iam::348868695504:role/Insurcloud-Developer"
            },
            "Action": "s3:ListBucket",
            "Resource": "arn:aws:s3:::eco-devops-rpa-files"
        },
        {
            "Sid": "HubDevelopersObjectAllow",
            "Effect": "Allow",
            "Principal": {
                "AWS": "arn:aws:iam::348868695504:role/Insurcloud-Developer"
            },
            "Action": [
                "s3:GetObject",
                "s3:PutObject",
                "s3:PutObjectAcl",
                "s3:DeleteObject"
            ],
            "Resource": "arn:aws:s3:::eco-devops-rpa-files/*"
        },
        {
            "Sid": "DevInstanceListBucketAllow",
            "Effect": "Allow",
            "Principal": {
                "AWS": [
                    "arn:aws:iam::632109065501:role/aws-service-role/rds.amazonaws.com/AWSServiceRoleForRDS",
                    "arn:aws:iam::632109065501:role/CMS-SSMRole",
                    "arn:aws:iam::632109065501:role/Eco-RDS-S3-Restore-Role"
                ]
            },
            "Action": "s3:ListBucket",
            "Resource": "arn:aws:s3:::eco-devops-rpa-files"
        },
        {
            "Sid": "DevInstanceObjectAllow",
            "Effect": "Allow",
            "Principal": {
                "AWS": [
                    "arn:aws:iam::632109065501:role/aws-service-role/rds.amazonaws.com/AWSServiceRoleForRDS",
                    "arn:aws:iam::632109065501:role/CMS-SSMRole",
                    "arn:aws:iam::632109065501:role/Eco-RDS-S3-Restore-Role"
                ]
            },
            "Action": [
                "s3:GetObject",
                "s3:PutObject",
                "s3:PutObjectAcl",
                "s3:DeleteObject"
            ],
            "Resource": "arn:aws:s3:::eco-devops-rpa-files/*"
        },
        {
            "Sid": "ProdInstanceListBucketAllow",
            "Effect": "Allow",
            "Principal": {
                "AWS": "arn:aws:iam::852868701745:role/CMS-SSMRole"
            },
            "Action": "s3:ListBucket",
            "Resource": "arn:aws:s3:::eco-devops-rpa-files"
        },
        {
            "Sid": "ProdInstanceObjectAllow",
            "Effect": "Allow",
            "Principal": {
                "AWS": "arn:aws:iam::852868701745:role/CMS-SSMRole"
            },
            "Action": [
                "s3:GetObject",
                "s3:PutObject",
                "s3:PutObjectAcl",
                "s3:DeleteObject"
            ],
            "Resource": "arn:aws:s3:::eco-devops-rpa-files/*"
        }
    ]
}
POLICY
}

resource "aws_s3_bucket" "ICECONOMICAL_HUB_HAPROXY" {
  bucket = "eco-haproxy-deployment"
  acl = "private"
  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm = "AES256"
      }
    }
  }

  lifecycle_rule {
    enabled = true

    transition {
      days = 30
      storage_class = "STANDARD_IA"
    }

  }
  tags = {
    Contract = "MANAGED_SERVICES"
    Name = "eco-haproxy-deployment"
  }
}

resource "aws_s3_bucket_policy" "ICECONOMICAL_HUB_HAPROXY_BUCKET_POLICY" {
  bucket = "eco-haproxy-deployment"

  policy = <<POLICY
{
    "Version": "2012-10-17",
    "Id": "ICECONOMICAL_HUB_HAPROXY_BUCKET_POLICY",
    "Statement": [
        {
            "Sid": "DenyPublicReadACL",
            "Effect": "Deny",
            "Principal": {
                "AWS": "*"
            },
            "Action": [
                "s3:PutObject",
                "s3:PutObjectAcl"
            ],
            "Resource": "arn:aws:s3:::eco-haproxy-deployment/*",
            "Condition": {
                "StringEquals": {
                    "s3:x-amz-acl": [
                        "public-read",
                        "public-read-write",
                        "authenticated-read"
                    ]
                }
            }
        },
        {
            "Sid": "DenyPublicReadGrant",
            "Effect": "Deny",
            "Principal": {
                "AWS": "*"
            },
            "Action": [
                "s3:PutObject",
                "s3:PutObjectAcl"
            ],
            "Resource": "arn:aws:s3:::eco-haproxy-deployment/*",
            "Condition": {
                "StringLike": {
                    "s3:x-amz-grant-read": [
                        "*http://acs.amazonaws.com/groups/global/AllUsers*",
                        "*http://acs.amazonaws.com/groups/global/AuthenticatedUsers*"
                    ]
                }
            }
        },
        {
            "Sid": "DenyPublicListACL",
            "Effect": "Deny",
            "Principal": {
                "AWS": "*"
            },
            "Action": "s3:PutBucketAcl",
            "Resource": "arn:aws:s3:::eco-haproxy-deployment",
            "Condition": {
                "StringEquals": {
                    "s3:x-amz-acl": [
                        "public-read",
                        "public-read-write",
                        "authenticated-read"
                    ]
                }
            }
        },
        {
            "Sid": "DenyPublicListGrant",
            "Effect": "Deny",
            "Principal": {
                "AWS": "*"
            },
            "Action": "s3:PutBucketAcl",
            "Resource": "arn:aws:s3:::eco-haproxy-deployment",
            "Condition": {
                "StringLike": {
                    "s3:x-amz-grant-read": [
                        "*http://acs.amazonaws.com/groups/global/AllUsers*",
                        "*http://acs.amazonaws.com/groups/global/AuthenticatedUsers*"
                    ]
                }
            }
        },
        {
            "Sid": "DenyPublicReadConsole",
            "Effect": "Deny",
            "Principal": {
                "AWS": "*"
            },
            "Action": [
                "s3:PutObjectAcl",
                "s3:PutBucketAcl"
            ],
            "Resource": [
                "arn:aws:s3:::eco-haproxy-deployment/*",
                "arn:aws:s3:::eco-haproxy-deployment"
            ],
            "Condition": {
                "Null": {
                    "s3:x-amz-grant-write": "true",
                    "s3:x-amz-grant-read": "true",
                    "s3:x-amz-acl": "true",
                    "s3:x-amz-grant-read-acp": "true",
                    "s3:x-amz-grant-full-control": "true",
                    "s3:x-amz-grant-write-acp": "true"
                }
            }
        },
        {
            "Sid": "DevInstanceListBucketAllow",
            "Effect": "Allow",
            "Principal": {
                "AWS": "arn:aws:iam::632109065501:role/CMS-SSMRole"
            },
            "Action": "s3:ListBucket",
            "Resource": "arn:aws:s3:::eco-haproxy-deployment"
        },
        {
            "Sid": "DevInstanceObjectAllow",
            "Effect": "Allow",
            "Principal": {
                "AWS": "arn:aws:iam::632109065501:role/CMS-SSMRole"
            },
            "Action": [
                "s3:GetObject",
                "s3:PutObject",
                "s3:DeleteObject"
            ],
            "Resource": [
                "arn:aws:s3:::eco-haproxy-deployment/DEV*/*",
                "arn:aws:s3:::eco-haproxy-deployment/ST*/*",
                "arn:aws:s3:::eco-haproxy-deployment/UAT*/*",
                "arn:aws:s3:::eco-haproxy-deployment/PRF*/*",
                "arn:aws:s3:::eco-haproxy-deployment/SIT*/*",
                "arn:aws:s3:::eco-haproxy-deployment/PPS*/*",
                "arn:aws:s3:::eco-haproxy-deployment/EF*/*"
            ]
        },
        {
            "Sid": "HubInstanceListBucketAllow",
            "Effect": "Allow",
            "Principal": {
                "AWS": "arn:aws:iam::348868695504:role/CMS-SSMRole"
            },
            "Action": "s3:ListBucket",
            "Resource": "arn:aws:s3:::eco-haproxy-deployment"
        },
        {
            "Sid": "HubInstanceObjectAllow",
            "Effect": "Allow",
            "Principal": {
                "AWS": "arn:aws:iam::348868695504:role/CMS-SSMRole"
            },
            "Action": [
                "s3:GetObject",
                "s3:PutObject",
                "s3:DeleteObject"
            ],
            "Resource": [
                "arn:aws:s3:::eco-haproxy-deployment/DEV*/*",
                "arn:aws:s3:::eco-haproxy-deployment/ST*/*",
                "arn:aws:s3:::eco-haproxy-deployment/UAT*/*",
                "arn:aws:s3:::eco-haproxy-deployment/PRF*/*",
                "arn:aws:s3:::eco-haproxy-deployment/SIT*/*",
                "arn:aws:s3:::eco-haproxy-deployment/PPS*/*",
                "arn:aws:s3:::eco-haproxy-deployment/EF*/*",
                "arn:aws:s3:::eco-haproxy-deployment/PROD*/*"
            ]
        },
        {
            "Sid": "ProdInstanceListBucketAllow",
            "Effect": "Allow",
            "Principal": {
                "AWS": "arn:aws:iam::852868701745:role/CMS-SSMRole"
            },
            "Action": "s3:ListBucket",
            "Resource": "arn:aws:s3:::eco-haproxy-deployment"
        },
        {
            "Sid": "ProdInstanceObjectAllow",
            "Effect": "Allow",
            "Principal": {
                "AWS": "arn:aws:iam::852868701745:role/CMS-SSMRole"
            },
            "Action": [
                "s3:GetObject",
                "s3:PutObject",
                "s3:DeleteObject"
            ],
            "Resource": [
                "arn:aws:s3:::eco-haproxy-deployment/PROD*/*"
            ]
        }
    ]
}
POLICY
}
