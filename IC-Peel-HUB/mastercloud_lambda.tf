# Lambda

#Lambda function for PPS Peel GL Update
resource "aws_lambda_permission" "apigw_lambda_authenrizer" {
  statement_id  = "AllowExecutionFromAPIGateway"
  action        = "lambda:InvokeFunction"
  function_name = "${aws_lambda_function.peel-APIGatewayAuthenrizer_Hub.function_name}"
  principal     = "apigateway.amazonaws.com"
  # More: http://docs.aws.amazon.com/apigateway/latest/developerguide/api-gateway-control-access-using-iam-policies-to-invoke-api.html
  source_arn = "arn:aws:execute-api:${var.region}:${var.peel-hub-account}:${aws_api_gateway_rest_api.GLCheque.id}/*"
}

resource "aws_lambda_function" "peel-APIGatewayAuthenrizer_Hub" {
  function_name = "peel-APIGatewayAuthenrizer_Hub"
  role          = "arn:aws:iam::${var.peel-hub-account}:role/LambdaAssume_Hub"
  handler       = "index.handler"
  runtime       = "nodejs10.x"
  s3_bucket     = "peel-custom-auth"
  s3_key        = "custom-authorizer.zip"

  environment {
    variables = {
      AUDIENCE = "https://peel.insurcloud-dev.ca/api/v2/"
      JWKS_URI = "https://insurcloud-dev.auth0.com/.well-known/jwks.json"
      TOKEN_ISSUER = "https://insurcloud-dev.auth0.com/"
    }
  }
}

resource "aws_lambda_permission" "apigw_lambda_authenrizer_prod" {
  statement_id  = "AllowExecutionFromAPIGateway"
  action        = "lambda:InvokeFunction"
  function_name = "${aws_lambda_function.peelprod-APIGatewayAuthenrizer_Hub.function_name}"
  principal     = "apigateway.amazonaws.com"
  # More: http://docs.aws.amazon.com/apigateway/latest/developerguide/api-gateway-control-access-using-iam-policies-to-invoke-api.html
  source_arn = "arn:aws:execute-api:${var.region}:${var.peel-hub-account}:${aws_api_gateway_rest_api.GLCheque.id}/*"
}

resource "aws_lambda_function" "peelprod-APIGatewayAuthenrizer_Hub" {
  function_name = "peelprod-APIGatewayAuthenrizer_Hub"
  role          = "arn:aws:iam::${var.peel-hub-account}:role/LambdaAssume_Hub"
  handler       = "index.handler"
  runtime       = "nodejs10.x"
  s3_bucket     = "peel-custom-auth"
  s3_key        = "custom-authorizer.zip"

  environment {
    variables = {
      AUDIENCE = "https://peel.insurcloud.ca/ms/api/v2"
      JWKS_URI = "https://insurcloud.auth0.com/.well-known/jwks.json"
      TOKEN_ISSUER = "https://insurcloud.auth0.com/"
    }
  }
}



