# #=======================================================================
# # Shared Resource
# #=======================================================================

############ Jenkins Client ##################
resource "aws_volume_attachment" "ICPORTAGE_DEV_JENKINS_CLIENT_DOCKER_EBS_ATTACHMENT" {
  device_name = "/dev/sdh"
  volume_id   = "${aws_ebs_volume.ICPORTAGE_DEV_JENKINS_CLIENT_DOCKER_EBS.id}"
  instance_id = "${aws_instance.ICPORTAGE_DEV_JENKINS_CLIENT.id}"
}

resource "aws_ebs_volume" "ICPORTAGE_DEV_JENKINS_CLIENT_DOCKER_EBS" {
  availability_zone = "${aws_instance.ICPORTAGE_DEV_JENKINS_CLIENT.availability_zone}"
  size              = 25
  type              = "gp2"
  encrypted         = true
  kms_key_id        = "arn:aws:kms:ca-central-1:091072499599:key/9fc4bcc6-13f4-4779-8eb6-2291b5c4a586"

  tags = {
    Name   = "ICPORTAGE_DEV_JENKINS_CLIENT_DOCKER_EBS"
    Docker = "True"
  }
}

# #=======================================================================
# # DEV01
# #=======================================================================

############ Guidewire Volumes ##################
resource "aws_volume_attachment" "ICPORTAGE_DEV01_GUIDEWIRE_SUITE_DOCKER_EBS_ATTACHMENT" {
  device_name = "/dev/sdh"
  volume_id   = "${aws_ebs_volume.ICPORTAGE_DEV01_GUIDEWIRE_SUITE_DOCKER_EBS.id}"
  instance_id = "${aws_instance.ICPORTAGE_DEV01_GUIDEWIRE_SUITE.id}"
}

resource "aws_ebs_volume" "ICPORTAGE_DEV01_GUIDEWIRE_SUITE_DOCKER_EBS" {
  availability_zone = "ca-central-1a"
  size              = 35
  type              = "gp2"
  encrypted         = true
  kms_key_id        = "arn:aws:kms:ca-central-1:091072499599:key/9fc4bcc6-13f4-4779-8eb6-2291b5c4a586"

  tags = {
    Name   = "ICPORTAGE_DEV01_GUIDEWIRE_SUITE_DOCKER_EBS"
    Docker = "True"
  }
}

resource "aws_volume_attachment" "ICPORTAGE_DEV01_GUIDEWIRE_SUITE_LOGS_EBS_ATTACHMENT" {
  device_name = "/dev/sdi"
  volume_id   = "${aws_ebs_volume.ICPORTAGE_DEV01_GUIDEWIRE_SUITE_LOGS_EBS.id}"
  instance_id = "${aws_instance.ICPORTAGE_DEV01_GUIDEWIRE_SUITE.id}"
}

resource "aws_ebs_volume" "ICPORTAGE_DEV01_GUIDEWIRE_SUITE_LOGS_EBS" {
  availability_zone = "ca-central-1a"
  size              = 35
  type              = "gp2"
  encrypted         = true
  kms_key_id        = "arn:aws:kms:ca-central-1:091072499599:key/9fc4bcc6-13f4-4779-8eb6-2291b5c4a586"

  tags = {
    Name   = "ICPORTAGE_DEV01_GUIDEWIRE_SUITE_LOGS_EBS"
    Docker = "True"
  }
}

############ Microservices Volumes ##################
resource "aws_volume_attachment" "ICPORTAGE_DEV01_MICROSERVICES_DOCKER_EBS_ATTACHMENT" {
  device_name = "/dev/sdh"
  volume_id   = "${aws_ebs_volume.ICPORTAGE_DEV01_MICROSERVICES_DOCKER_EBS.id}"
  instance_id = "${aws_instance.ICPORTAGE_DEV01_MICROSERVICES.id}"
}

resource "aws_ebs_volume" "ICPORTAGE_DEV01_MICROSERVICES_DOCKER_EBS" {
  availability_zone = "ca-central-1a"
  size              = 25
  encrypted         = true
  kms_key_id        = "arn:aws:kms:ca-central-1:091072499599:key/9fc4bcc6-13f4-4779-8eb6-2291b5c4a586"

  tags = {
    Name   = "ICPORTAGE_DEV01_MICROSERVICES_DOCKER_EBS"
    Docker = "True"
  }
}


############ Internal HAProxy Volumes ##################
resource "aws_volume_attachment" "ICPORTAGE_DEV01_INTERNAL_HAPROXY_DOCKER_EBS_ATTACHMENT" {
  device_name = "/dev/sdh"
  volume_id   = "${aws_ebs_volume.ICPORTAGE_DEV01_INTERNAL_HAPROXY_DOCKER_EBS.id}"
  instance_id = "${aws_instance.ICPORTAGE_DEV01_INTERNAL_HAPROXY.id}"
}

resource "aws_ebs_volume" "ICPORTAGE_DEV01_INTERNAL_HAPROXY_DOCKER_EBS" {
  availability_zone = "ca-central-1a"
  size              = 20
  encrypted         = true
  kms_key_id        = "arn:aws:kms:ca-central-1:091072499599:key/9fc4bcc6-13f4-4779-8eb6-2291b5c4a586"

  tags = {
    Name   = "ICPORTAGE_DEV01_INTERNAL_HAPROXY_DOCKER_EBS"
    Docker = "True"
  }
}

# #=======================================================================
# # DEV02
# #=======================================================================

############ Guidewire Volumes ##################
resource "aws_volume_attachment" "ICPORTAGE_DEV02_GUIDEWIRE_SUITE_DOCKER_EBS_ATTACHMENT" {
  device_name = "/dev/sdh"
  volume_id   = "${aws_ebs_volume.ICPORTAGE_DEV02_GUIDEWIRE_SUITE_DOCKER_EBS.id}"
  instance_id = "${aws_instance.ICPORTAGE_DEV02_GUIDEWIRE_SUITE.id}"
}

resource "aws_ebs_volume" "ICPORTAGE_DEV02_GUIDEWIRE_SUITE_DOCKER_EBS" {
  availability_zone = "ca-central-1a"
  size              = 35
  type              = "gp2"
  encrypted         = true
  kms_key_id        = "arn:aws:kms:ca-central-1:091072499599:key/9fc4bcc6-13f4-4779-8eb6-2291b5c4a586"

  tags = {
    Name   = "ICPORTAGE_DEV02_GUIDEWIRE_SUITE_DOCKER_EBS"
    Docker = "True"
  }
}

resource "aws_volume_attachment" "ICPORTAGE_DEV02_GUIDEWIRE_SUITE_LOGS_EBS_ATTACHMENT" {
  device_name = "/dev/sdi"
  volume_id   = "${aws_ebs_volume.ICPORTAGE_DEV02_GUIDEWIRE_SUITE_LOGS_EBS.id}"
  instance_id = "${aws_instance.ICPORTAGE_DEV02_GUIDEWIRE_SUITE.id}"
}

resource "aws_ebs_volume" "ICPORTAGE_DEV02_GUIDEWIRE_SUITE_LOGS_EBS" {
  availability_zone = "ca-central-1a"
  size              = 35
  type              = "gp2"
  encrypted         = true
  kms_key_id        = "arn:aws:kms:ca-central-1:091072499599:key/9fc4bcc6-13f4-4779-8eb6-2291b5c4a586"

  tags = {
    Name   = "ICPORTAGE_DEV02_GUIDEWIRE_SUITE_LOGS_EBS"
    Docker = "True"
  }
}

############ Microservices Volumes ##################
resource "aws_volume_attachment" "ICPORTAGE_DEV02_MICROSERVICES_DOCKER_EBS_ATTACHMENT" {
  device_name = "/dev/sdh"
  volume_id   = "${aws_ebs_volume.ICPORTAGE_DEV02_MICROSERVICES_DOCKER_EBS.id}"
  instance_id = "${aws_instance.ICPORTAGE_DEV02_MICROSERVICES.id}"
}

resource "aws_ebs_volume" "ICPORTAGE_DEV02_MICROSERVICES_DOCKER_EBS" {
  availability_zone = "ca-central-1a"
  size              = 25
  encrypted         = true
  kms_key_id        = "arn:aws:kms:ca-central-1:091072499599:key/9fc4bcc6-13f4-4779-8eb6-2291b5c4a586"

  tags = {
    Name   = "ICPORTAGE_DEV02_MICROSERVICES_DOCKER_EBS"
    Docker = "True"
  }
}

# #=======================================================================
# # DEV03
# #=======================================================================

############ Guidewire Volumes ##################
resource "aws_volume_attachment" "ICPORTAGE_DEV03_GUIDEWIRE_SUITE_DOCKER_EBS_ATTACHMENT" {
  device_name = "/dev/sdh"
  volume_id   = "${aws_ebs_volume.ICPORTAGE_DEV03_GUIDEWIRE_SUITE_DOCKER_EBS.id}"
  instance_id = "${aws_instance.ICPORTAGE_DEV03_GUIDEWIRE_SUITE.id}"
}

resource "aws_ebs_volume" "ICPORTAGE_DEV03_GUIDEWIRE_SUITE_DOCKER_EBS" {
  availability_zone = "ca-central-1a"
  size              = 35
  type              = "gp2"
  encrypted         = true
  kms_key_id        = "arn:aws:kms:ca-central-1:091072499599:key/9fc4bcc6-13f4-4779-8eb6-2291b5c4a586"

  tags = {
    Name   = "ICPORTAGE_DEV03_GUIDEWIRE_SUITE_DOCKER_EBS"
    Docker = "True"
  }
}

resource "aws_volume_attachment" "ICPORTAGE_DEV03_GUIDEWIRE_SUITE_LOGS_EBS_ATTACHMENT" {
  device_name = "/dev/sdi"
  volume_id   = "${aws_ebs_volume.ICPORTAGE_DEV03_GUIDEWIRE_SUITE_LOGS_EBS.id}"
  instance_id = "${aws_instance.ICPORTAGE_DEV03_GUIDEWIRE_SUITE.id}"
}

resource "aws_ebs_volume" "ICPORTAGE_DEV03_GUIDEWIRE_SUITE_LOGS_EBS" {
  availability_zone = "ca-central-1a"
  size              = 35
  type              = "gp2"
  encrypted         = true
  kms_key_id        = "arn:aws:kms:ca-central-1:091072499599:key/9fc4bcc6-13f4-4779-8eb6-2291b5c4a586"

  tags = {
    Name   = "ICPORTAGE_DEV03_GUIDEWIRE_SUITE_LOGS_EBS"
    Docker = "True"
  }
}

# #=======================================================================
# # QA01
# #=======================================================================

############ Guidewire Volumes ##################
resource "aws_volume_attachment" "ICPORTAGE_QA01_GUIDEWIRE_SUITE_DOCKER_EBS_ATTACHMENT" {
  device_name = "/dev/sdh"
  volume_id   = "${aws_ebs_volume.ICPORTAGE_QA01_GUIDEWIRE_SUITE_DOCKER_EBS.id}"
  instance_id = "${aws_instance.ICPORTAGE_QA01_GUIDEWIRE_SUITE.id}"
}

resource "aws_ebs_volume" "ICPORTAGE_QA01_GUIDEWIRE_SUITE_DOCKER_EBS" {
  availability_zone = "ca-central-1a"
  size              = 35
  type              = "gp2"
  encrypted         = true
  kms_key_id        = "arn:aws:kms:ca-central-1:091072499599:key/9fc4bcc6-13f4-4779-8eb6-2291b5c4a586"

  tags = {
    Name   = "ICPORTAGE_QA01_GUIDEWIRE_SUITE_DOCKER_EBS"
    Docker = "True"
  }
}

resource "aws_volume_attachment" "ICPORTAGE_QA01_GUIDEWIRE_SUITE_LOGS_EBS_ATTACHMENT" {
  device_name = "/dev/sdi"
  volume_id   = "${aws_ebs_volume.ICPORTAGE_QA01_GUIDEWIRE_SUITE_LOGS_EBS.id}"
  instance_id = "${aws_instance.ICPORTAGE_QA01_GUIDEWIRE_SUITE.id}"
}

resource "aws_ebs_volume" "ICPORTAGE_QA01_GUIDEWIRE_SUITE_LOGS_EBS" {
  availability_zone = "ca-central-1a"
  size              = 35
  type              = "gp2"
  encrypted         = true
  kms_key_id        = "arn:aws:kms:ca-central-1:091072499599:key/9fc4bcc6-13f4-4779-8eb6-2291b5c4a586"

  tags = {
    Name   = "ICPORTAGE_QA01_GUIDEWIRE_SUITE_LOGS_EBS"
    Docker = "True"
  }
}

############ Microservices Volumes ##################
resource "aws_volume_attachment" "ICPORTAGE_QA01_MICROSERVICES_DOCKER_EBS_ATTACHMENT" {
  device_name = "/dev/sdh"
  volume_id   = "${aws_ebs_volume.ICPORTAGE_QA01_MICROSERVICES_DOCKER_EBS.id}"
  instance_id = "${aws_instance.ICPORTAGE_QA01_MICROSERVICES.id}"
}

resource "aws_ebs_volume" "ICPORTAGE_QA01_MICROSERVICES_DOCKER_EBS" {
  availability_zone = "ca-central-1a"
  size              = 25
  encrypted         = true
  kms_key_id        = "arn:aws:kms:ca-central-1:091072499599:key/9fc4bcc6-13f4-4779-8eb6-2291b5c4a586"

  tags = {
    Name   = "ICPORTAGE_QA01_MICROSERVICES_DOCKER_EBS"
    Docker = "True"
  }
}

# #=======================================================================
# # CONVERSION CNV01
# #=======================================================================

############ Guidewire Volumes ##################
resource "aws_volume_attachment" "ICPORTAGE_CNV01_GUIDEWIRE_SUITE_DOCKER_EBS_ATTACHMENT" {
  device_name = "/dev/sdh"
  volume_id   = "${aws_ebs_volume.ICPORTAGE_CNV01_GUIDEWIRE_SUITE_DOCKER_EBS.id}"
  instance_id = "${aws_instance.ICPORTAGE_CNV01_GUIDEWIRE_SUITE.id}"
}

resource "aws_ebs_volume" "ICPORTAGE_CNV01_GUIDEWIRE_SUITE_DOCKER_EBS" {
  availability_zone = "ca-central-1a"
  size              = 35
  type              = "gp2"
  encrypted         = true
  kms_key_id        = "arn:aws:kms:ca-central-1:091072499599:key/9fc4bcc6-13f4-4779-8eb6-2291b5c4a586"

  tags = {
    Name   = "ICPORTAGE_CNV01_GUIDEWIRE_SUITE_DOCKER_EBS"
    Docker = "True"
  }
}

resource "aws_volume_attachment" "ICPORTAGE_CNV01_GUIDEWIRE_SUITE_LOGS_EBS_ATTACHMENT" {
  device_name = "/dev/sdi"
  volume_id   = "${aws_ebs_volume.ICPORTAGE_CNV01_GUIDEWIRE_SUITE_LOGS_EBS.id}"
  instance_id = "${aws_instance.ICPORTAGE_CNV01_GUIDEWIRE_SUITE.id}"
}

resource "aws_ebs_volume" "ICPORTAGE_CNV01_GUIDEWIRE_SUITE_LOGS_EBS" {
  availability_zone = "ca-central-1a"
  size              = 35
  type              = "gp2"
  encrypted         = true
  kms_key_id        = "arn:aws:kms:ca-central-1:091072499599:key/9fc4bcc6-13f4-4779-8eb6-2291b5c4a586"

  tags = {
    Name   = "ICPORTAGE_CNV01_GUIDEWIRE_SUITE_LOGS_EBS"
    Docker = "True"
  }
}

############ Microservices Volumes ##################
# resource "aws_volume_attachment" "ICPORTAGE_CNV01_MICROSERVICES_DOCKER_EBS_ATTACHMENT" {
#   device_name = "/dev/sdh"
#   volume_id   = "${aws_ebs_volume.ICPORTAGE_CNV01_MICROSERVICES_DOCKER_EBS.id}"
#   instance_id = "${aws_instance.ICPORTAGE_CNV01_MICROSERVICES.id}"
# }
#
# resource "aws_ebs_volume" "ICPORTAGE_CNV01_MICROSERVICES_DOCKER_EBS" {
#   availability_zone = "ca-central-1a"
#   size              = 25
#   encrypted         = true
#   kms_key_id        = "arn:aws:kms:ca-central-1:091072499599:key/9fc4bcc6-13f4-4779-8eb6-2291b5c4a586"
#
#   tags = {
#     Name   = "ICPORTAGE_CNV01_MICROSERVICES_DOCKER_EBS"
#     Docker = "True"
#   }
# }
