<powershell>
    Set-NetFirewallProfile -Profile Domain,Public,Private -Enabled False

    # Domain Information
    $domain = "${domain}"
    $ouselection = "${ouselection}"
    $dns1 = "${dns1}"
    $dns2 = "${dns2}"
    $dns3 = "${dns3}"
    $ComputerName = "${computer_name}"

    # ParameterStore Names
    $PwordParameter = "${pwordparameter}"
    $UnameParameter = "${unameparameter}"
    $fulldomain = $domain
    $pword = (Get-SSMparameter -Name $PwordParameter -WithDecryption $true).Value
    $uname = (Get-SSMparameter -Name $UnameParameter -WithDecryption $true).Value
    $username = ($uname + "@" + $domain)
    $password = ConvertTo-SecureString $pword -AsPlainText -Force
    $AdminCredentials = New-Object System.Management.Automation.PSCredential ($username, $password)
 
    # Set DNS servers to point at Active Directory
    # Try all Ethernet names because we can not predict which one will be active in a new EC2
    Set-DnsClientServerAddress -InterfaceAlias "Ethernet" -ServerAddresses ("$dns1","$dns2","$dns3")
    Set-DnsClientServerAddress -InterfaceAlias "Ethernet 1" -ServerAddresses ("$dns1","$dns2","$dns3")
    Set-DnsClientServerAddress -InterfaceAlias "Ethernet 2" -ServerAddresses ("$dns1","$dns2","$dns3")
    Set-DnsClientServerAddress -InterfaceAlias "Ethernet 3" -ServerAddresses ("$dns1","$dns2","$dns3")
    Set-DnsClientServerAddress -InterfaceAlias "Ethernet 4" -ServerAddresses ("$dns1","$dns2","$dns3")
   
    # Join the computer to the Active Directory Domain, with OU sorting
    Add-Computer -DomainName $fulldomain -NewName $ComputerName -Credential $AdminCredentials
    # Rename the computer once it is joined - trying to avoid a reboot

    # Restart the computer to confirm the AD join and name change
    Restart-Computer
</powershell>