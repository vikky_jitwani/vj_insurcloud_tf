provider "aws" {
  region     = "ca-central-1"
}

terraform {
 backend "s3" {
    bucket     = "a-c-mc-p-a010-bcaa-tfstate"
    key        = "IC-BCAA-PROD/STATE/current.tf"
    region     = "ca-central-1"
	  #encrypt    = true
	  #kms_key_id = "arn:aws:kms:ca-central-1:942574907772:key/cada27fc-e5f9-4f1c-b85a-3527eb3e75bb"
  }
}