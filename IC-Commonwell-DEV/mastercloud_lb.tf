##### ALBs #####

##### NLBs ####################
########## Internal ###########
resource "aws_lb" "INTERNAL_NLB" {
    name = "CW-INTERNAL-NLB"
    internal           = true
    load_balancer_type = "network"
    enable_cross_zone_load_balancing = false
    subnets            = [
        "${var.commonwell-non-prod-private-1a-subnet}",
        "${var.commonwell-non-prod-private-1b-subnet}"
    ]

    tags = {
        Name = "CW-INTERNAL-NLB"
    }
}

resource "aws_lb_target_group" "INTERNAL_NLB_TARGET_GROUP" {
    name     = "CW-INTERNAL-NLB"
    port     = 443
    protocol = "TCP"
    vpc_id   = "${var.vpc_id}"

    health_check {
        protocol = "TCP"
        port = 8444
    }

    tags = {
        Name = "CW-INTERNAL-NLB-TARGET-GROUP"
    }
}

resource "aws_lb_listener" "INTERNAL_NLB_LISTENER" {
    load_balancer_arn = "${aws_lb.INTERNAL_NLB.arn}"
    port              = "443"
    protocol          = "TCP"

    default_action {
        type             = "forward"
        target_group_arn = "${aws_lb_target_group.INTERNAL_NLB_TARGET_GROUP.arn}"
    }
}

resource "aws_lb_target_group_attachment" "INTERNAL_NLB_TARGET_GROUP_ATTACHMENT_1" {
    target_group_arn = "${aws_lb_target_group.INTERNAL_NLB_TARGET_GROUP.arn}"
    target_id        = "${aws_instance.ICCW_INTERNAL_HA_PROXY_N1.id}"
    port             = 443
}

resource "aws_lb_target_group_attachment" "INTERNAL_NLB_TARGET_GROUP_ATTACHMENT_2" {
    target_group_arn = "${aws_lb_target_group.INTERNAL_NLB_TARGET_GROUP.arn}"
    target_id        = "${aws_instance.ICCW_INTERNAL_HA_PROXY_N2.id}"
    port             = 443
}

output "INTERNAL_NLB_DNS_NAME" {
    value = "${aws_lb.INTERNAL_NLB.dns_name}"
}

output "INTERNAL_NLB_ZONE_ID" {
    value = "${aws_lb.INTERNAL_NLB.zone_id}"
}

########## External ###########
resource "aws_lb" "EXTERNAL_NLB" {
    name               = "CW-EXTERNAL-NLB"
    internal           = true
    load_balancer_type = "network"
    enable_cross_zone_load_balancing = false
    subnets            = [
        "${var.commonwell-non-prod-private-1a-subnet}",
        "${var.commonwell-non-prod-private-1b-subnet}"
    ] # Changing this value will force a recreation of the resource

    tags = {
        Name = "CW-EXTERNAL-NLB"
    }
}

output "EXTERNAL_NLB_DNS_NAME" {
  value = "${aws_lb.EXTERNAL_NLB.dns_name}"
}

output "EXTERNAL_NLB_ZONE_ID" {
  value = "${aws_lb.EXTERNAL_NLB.zone_id}"
}

resource "aws_lb_target_group" "EXTERNAL_NLB_TARGET_GROUP" {
    name     = "CW-EXTERNAL-NLB"
    port     = 443
    protocol = "TCP"
    vpc_id   = "${var.vpc_id}"

    health_check {
        protocol = "TCP"
        port = 8444
    }

    tags = {
        Name = "CW-EXTERNAL-NLB-TARGET-GROUP"
    }
}

resource "aws_lb_listener" "EXTERNAL_NLB_LISTENER" {
    load_balancer_arn = "${aws_lb.EXTERNAL_NLB.arn}"
    port              = "443"
    protocol          = "TCP"

    default_action {
        type             = "forward"
        target_group_arn = "${aws_lb_target_group.EXTERNAL_NLB_TARGET_GROUP.arn}"
    }
}

resource "aws_lb_target_group_attachment" "EXTERNAL_NLB_TARGET_GROUP_ATTACHMENT_1" {
    target_group_arn = "${aws_lb_target_group.EXTERNAL_NLB_TARGET_GROUP.arn}"
    target_id        = "${aws_instance.ICCW_EXTERNAL_HA_PROXY_N1.id}"
    port             = 443
}

resource "aws_lb_target_group_attachment" "EXTERNAL_NLB_TARGET_GROUP_ATTACHMENT_2" {
    target_group_arn = "${aws_lb_target_group.EXTERNAL_NLB_TARGET_GROUP.arn}"
    target_id        = "${aws_instance.ICCW_EXTERNAL_HA_PROXY_N2.id}"
    port             = 443
}
