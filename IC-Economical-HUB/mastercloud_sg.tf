resource "aws_security_group" "SG_CLIENT_PING_TEST" {
  name = "CLIENT-PING-TEST"

  ingress {
    from_port = -1
    to_port = -1
    protocol = "icmp"
    cidr_blocks = [
      "${var.economical_cidr}"]
  }

  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = [
      "0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    Contract = "MANAGED_SERVICES"
    Name = "CLIENT-PING-TEST"
  }
}

resource "aws_security_group" "SG_AVIATRIX_RDP" {
  name = "AVIATRIX-RDP-SG"

  ingress {
    from_port = 3389
    to_port = 3389
    protocol = "tcp"
    cidr_blocks = [
      "${var.economical_aviatrix_vpn}"]

    description = "RDP"
  }

  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = [
      "0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    Contract = "MANAGED_SERVICES"
    Name = "AVIATRIX-RDP-SG"
  }
}

resource "aws_security_group" "SG_AVIATRIX_SSH" {
  name = "AVIATRIX-SSH-SG"

  ingress {
    from_port = 22
    to_port = 22
    protocol = "tcp"
    cidr_blocks = [
      "${var.economical_aviatrix_vpn}"
    ]
    description = "SSH"
  }

  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = [
      "0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    Contract = "MANAGED_SERVICES"
    Name = "AVIATRIX-SSH-SG"
  }
}

resource "aws_security_group" "SG_JENKINS_CLIENT_SSH" {
  name = "JENKINS-CLIENT-SSH-SG"

  ingress {
    from_port = 22
    to_port = 22
    protocol = "tcp"
    cidr_blocks = [
      "10.11.0.173/32"
    ]
    description = "Jenkins Client SSH"
  }

  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = [
      "0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    Contract = "MANAGED_SERVICES"
    Name = "JENKINS-CLIENT-SSH-SG"
  }
}

resource "aws_security_group" "SG_HTTPS" {
  name = "ICHUB-HTTPS-SG"

  ingress {
    from_port = 443
    to_port = 443
    protocol = "tcp"
    cidr_blocks = [
      "${var.economical_cidr}"]
  }

  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = [
      "0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    Contract = "MANAGED_SERVICES"
    Name = "ICHUB-HTTPS-SG"
  }
}
