data "aws_ami" "sql_server17" {
  most_recent = true
  filter {
    name = "name"
    values = ["Secured SQL Server 2017 Enterprise*"]
  }
  filter {
    name = "virtualization-type"
    values = ["hvm"]
  }
  owners = ["aws-marketplace"]
}
