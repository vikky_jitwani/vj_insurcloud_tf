provider "aws" {
  region     = "ca-central-1"
}

terraform {
 backend "s3" {
   bucket     = "a-c-mc-p-a005-portage-tfstate"
   key        = "IC-Portage-PROD/STATE/current.tf"
   region     = "ca-central-1"
//    bucket     = "aws-cms-terraformstate"
//    key        = "ICPortage/prod.tfstate"
//    region     = "ca-central-1"
	  #encrypt    = true
	  #kms_key_id = "arn:aws:kms:ca-central-1:942574907772:key/cada27fc-e5f9-4f1c-b85a-3527eb3e75bb"
  }
}
