# #=======================================================================
# # Shared Resource PPS/DEV/QA
# #=======================================================================

############# JENKINS CLIENT #################
resource "aws_instance" "ICBCAA_DEV_JENKINS_CLIENT" {
  ami                         = "ami-02496f7e580ca22e3"
  vpc_security_group_ids      = ["${aws_security_group.SG_JENKINS_CLIENT.id}"]
  subnet_id                   = "${var.bcaa-dev-private-1a-subnet}"
  instance_type               = "r5a.xlarge"
  key_name                    = "BcaaJenkinsDev"
  associate_public_ip_address = false
  iam_instance_profile        = "CMS-SSMInstanceRole"
  ebs_optimized               = "true"

  lifecycle {
    prevent_destroy = false
  }

  root_block_device {
    volume_size = "100"
  }

  tags {
    Owner              = "robhatnagar@deloitte.ca"
    ManagedBy          = "CMS-TF"
    SolutionName       = "Insurcloud Bcaa"
    Environment        = "Dev"
    ApplicationName    = "jenkins, docker"
    DataClassification = "NA"
    DLM-Backup         = "Yes"
    DLM-Retention      = "90"
    PatchGroup         = "AmazonLinux2"
    VendorManaged      = "False"
    ResourceType       = "Client"
    ResourceSeverity   = "High"
    Name               = "A-MC-CAC-D-A005-BCAA-DEV-JENKINS-CLIENT-EC2"
    AccountID          = "${data.aws_caller_identity.current.account_id}"
    Role               = "Container"
    CLIENT             = "Bcaa"
    BILLINGCODE = "FPE01903-CO-TE-TO-1000"
    BILLINGCONTACT = "marrojas@deloitte.ca"
    COUNTRY = "CA"
    CSCLASS = "Confidential"
    CSQUAL = "CI/PI Data"
    CSTYPE = "External"
    ENVIRONMENT = "NPD"
    FUNCTION = "CON"
    MEMBERFIRM = "CA"
    PRIMARYCONTACT = "marrojas@deloitte.ca"
    SECONDARYCONTACT = "Samir Modh"
  }
}

output "ICBCAA_DEV_JENKINS_CLIENT_PRIVATEIP" {
  value = "${aws_instance.ICBCAA_DEV_JENKINS_CLIENT.private_ip}"
}

######### DATA ##############

######### DHIC DATABASE SERVER ###########

data "template_file" "BCAA_DOMAIN_JOIN_1" {
  template = "${file("IC-BCAA-DEV/windowsdata.tpl")}"

  vars {
    computer_name  = "DEV01DHIC-DB"
    region         = "${var.region}"
    domain         = "${var.ad_domain}"
    dns1           = "${var.ad_dns1}"
    dns2           = "${var.ad_dns2}"
    dns3           = "${var.ad_dns3}"
    ouselection    = "${var.ou_name}"
    pwordparameter = "${var.pwordparameter}"
    unameparameter = "${var.unameparameter}"
  }
}

resource "aws_instance" "ICBCAA_DEV01_DHIC_MSSQL_DATABASE" {
  ami = "${var.ami_id_windows2016base_2020_12_11}"

  vpc_security_group_ids = [
    "${aws_security_group.SG_MSSQL_DB.id}",
    "${aws_security_group.SG_BCAA_MSSQL_DB.id}",
    "${aws_security_group.SG_AVIATRIX_RDP.id}"
  ]

  subnet_id                   = "${var.bcaa-dev-database-1a-subnet}"
  instance_type               = "r5a.large"
  key_name                    = "BcaaDatabaseDev"
  associate_public_ip_address = false
  iam_instance_profile        = "CMS-SSMInstanceRole"
  ebs_optimized               = "true"

  lifecycle {
    prevent_destroy = false
  }

  root_block_device {
    volume_size = "256"
  }

  user_data = "${data.template_file.BCAA_DOMAIN_JOIN_1.rendered}"

  tags {
    Owner              = "robhatnagar@deloitte.ca"
    ManagedBy          = "CMS-TF"
    SolutionName       = "Insurcloud Bcaa"
    Environment        = "Dev"
    ApplicationName    = "database, mssql, sap, ssis"
    DataClassification = "PII, PHI"
    DLM-Backup         = "Yes"
    DLM-Retention      = "90"
    PatchGroup         = "Windows"
    VendorManaged      = "False"
    OSName             = "Windows"
    ResourceType       = "Client"
    ResourceSeverity   = "Normal"
    Name               = "A-MC-CAC-D-A005-BCAA-DEV01-DHIC-DATABASE-MSSQL-WIN-EC2"
    AccountID          = "${data.aws_caller_identity.current.account_id}"
    Role               = "Container"
    CLIENT             = "Bcaa"
    BILLINGCODE = "FPE01903-CO-TE-TO-1000"
    BILLINGCONTACT = "marrojas@deloitte.ca"
    COUNTRY = "CA"
    CSCLASS = "Confidential"
    CSQUAL = "CI/PI Data"
    CSTYPE = "External"
    ENVIRONMENT = "NPD"
    FUNCTION = "CON"
    MEMBERFIRM = "CA"
    PRIMARYCONTACT = "marrojas@deloitte.ca"
    SECONDARYCONTACT = "Samir Modh"
  }

  provisioner "local-exec" {
    command = "echo ${aws_instance.ICBCAA_DEV01_DHIC_MSSQL_DATABASE.private_ip} >> dev_hosts.txt"
  }
}

output "ICBCAA_DEV01_DHIC_MSSQL_DATABASE_PRIVATEIP" {
  value = "${aws_instance.ICBCAA_DEV01_DHIC_MSSQL_DATABASE.private_ip}"
}

######### DHIC SERVER ###########

data "template_file" "BCAA_DOMAIN_JOIN_2" {
  template = "${file("IC-BCAA-DEV/windowsdata.tpl")}"

  vars {
    computer_name  = "DEV01DHIC"
    region         = "${var.region}"
    domain         = "${var.ad_domain}"
    dns1           = "${var.ad_dns1}"
    dns2           = "${var.ad_dns2}"
    dns3           = "${var.ad_dns3}"
    ouselection    = "${var.ou_name}"
    pwordparameter = "${var.pwordparameter}"
    unameparameter = "${var.unameparameter}"
  }
}

resource "aws_instance" "ICBCAA_DEV01_DHIC_SERVER" {
  ami = "${var.ami_id_windows2016base_2020_12_11}"

  vpc_security_group_ids = [
    "${aws_security_group.SG_DHIC.id}",
    "${aws_security_group.SG_AVIATRIX_RDP.id}"
  ]

  #   "${aws_security_group.SG_DEV_JOBSCHEDULER_SSH.id}"

  subnet_id                   = "${var.bcaa-dev-private-1a-subnet}"
  instance_type               = "t3a.xlarge"
  key_name                    = "BcaaDataDev"
  associate_public_ip_address = false
  iam_instance_profile        = "CMS-SSMInstanceRole"
  ebs_optimized               = "true"
  lifecycle {
    prevent_destroy = false
  }
  root_block_device {
    volume_size = "500"
  }
  user_data = "${data.template_file.BCAA_DOMAIN_JOIN_2.rendered}"
  tags {
    Owner              = "robhatnagar@deloitte.ca"
    ManagedBy          = "CMS-TF"
    SolutionName       = "Insurcloud Bcaa"
    Environment        = "Dev"
    ApplicationName    = "sap"
    DataClassification = "PII, PHI"
    DLM-Backup         = "Yes"
    DLM-Retention      = "90"
    PatchGroup         = "Windows"
    VendorManaged      = "False"
    OSName             = "Windows"
    ResourceType       = "Client"
    ResourceSeverity   = "Normal"
    Name               = "A-MC-CAC-D-A005-BCAA-DEV01-DHIC-SERVER-WIN-EC2"
    AccountID          = "${data.aws_caller_identity.current.account_id}"
    Role               = "Container"
    CLIENT             = "Bcaa"
    BILLINGCODE = "FPE01903-CO-TE-TO-1000"
    BILLINGCONTACT = "marrojas@deloitte.ca"
    COUNTRY = "CA"
    CSCLASS = "Confidential"
    CSQUAL = "CI/PI Data"
    CSTYPE = "External"
    ENVIRONMENT = "NPD"
    FUNCTION = "CON"
    MEMBERFIRM = "CA"
    PRIMARYCONTACT = "marrojas@deloitte.ca"
    SECONDARYCONTACT = "Samir Modh"
  }
  provisioner "local-exec" {
    command = "echo ${aws_instance.ICBCAA_DEV01_DHIC_SERVER.private_ip} >> dev_hosts.txt"
  }
}

output "ICBCAA_DEV01_DHIC_SERVER_PRIVATEIP" {
  value = "${aws_instance.ICBCAA_DEV01_DHIC_SERVER.private_ip}"
}

######################################

# #=======================================================================
# # DEV01
# #=======================================================================

######### GUIDEWIRE ##############
resource "aws_instance" "ICBCAA_DEV01_GUIDEWIRE_SUITE" {
  ami = "${var.ami_id_amazonlinux2base_2020_12_11}"

  vpc_security_group_ids = [
    "${aws_security_group.SG_GUIDEWIRE_DEV.id}",
    "${aws_security_group.SG_JENKINS_GUIDEWIRE_HEALTH_CHECK.id}",
    "${aws_security_group.SG_AVIATRIX_SSH.id}",
    "${aws_security_group.SG_JENKINS_CLIENT_SSH.id}",
    "${aws_security_group.SG_MSSQL_DB.id}"
  ]

  subnet_id                   = "${var.bcaa-dev-private-1a-subnet}"
  instance_type               = "r5a.xlarge"
  key_name                    = "BcaaGuidewireDev"
  associate_public_ip_address = false
  iam_instance_profile        = "CMS-SSMInstanceRole"
  ebs_optimized               = "true"

  root_block_device {
    volume_size = "35"
  }

  tags {
    Owner              = "robhatnagar@deloitte.ca"
    ManagedBy          = "CMS-TF"
    SolutionName       = "Insurcloud Bcaa"
    Environment        = "Dev"
    ApplicationName    = "guidewire, mssql, docker"
    DataClassification = "PII, PHI"
    DLM-Backup         = "Yes"
    DLM-Retention      = "90"
    PatchGroup         = "AmazonLinux2"
    VendorManaged      = "False"
    ResourceType       = "Client"
    ResourceSeverity   = "Normal"
    Name               = "A-MC-CAC-D-A005-BCAA-DEV01-GW-SUITE-LNX-EC2"
    AccountID          = "${data.aws_caller_identity.current.account_id}"
    Role               = "Container"
    CLIENT             = "Bcaa"
    BILLINGCODE = "FPE01903-CO-TE-TO-1000"
    BILLINGCONTACT = "marrojas@deloitte.ca"
    COUNTRY = "CA"
    CSCLASS = "Confidential"
    CSQUAL = "CI/PI Data"
    CSTYPE = "External"
    ENVIRONMENT = "NPD"
    FUNCTION = "CON"
    MEMBERFIRM = "CA"
    PRIMARYCONTACT = "marrojas@deloitte.ca"
    SECONDARYCONTACT = "Samir Modh"
  }

  provisioner "local-exec" {
    command = "echo ${aws_instance.ICBCAA_DEV01_GUIDEWIRE_SUITE.private_ip} >> dev_hosts.txt"
  }
}

output "ICBCAA_DEV01_GUIDEWIRE_SUITE_PRIVATEIP" {
  value = "${aws_instance.ICBCAA_DEV01_GUIDEWIRE_SUITE.private_ip}"
}

######### MICROSERVICES ###########
resource "aws_instance" "ICBCAA_DEV01_MICROSERVICES" {
  ami = "${var.ami_id_amazonlinux2base_2020_12_11}"

  vpc_security_group_ids = [
    "${aws_security_group.SG_MICROSERVICES_DEV.id}",
    "${aws_security_group.SG_MONITORING.id}",
    "${aws_security_group.SG_AVIATRIX_SSH.id}",
    "${aws_security_group.SG_JENKINS_CLIENT_SSH.id}",
    "${aws_security_group.SG_MYSQL_DB.id}",
  ]

  subnet_id                   = "${var.bcaa-dev-private-1a-subnet}"
  instance_type               = "r5a.xlarge"
  key_name                    = "BcaaMicroservicesDev"
  associate_public_ip_address = false
  iam_instance_profile        = "BCAA-S3Upload-Policy-DEV"
  ebs_optimized               = "true"

  root_block_device {
    volume_size = "50"
  }

  tags {
    Owner              = "robhatnagar@deloitte.ca"
    ManagedBy          = "CMS-TF"
    SolutionName       = "Insurcloud Bcaa"
    Environment        = "Dev"
    ApplicationName    = "docker, microservices, mysql"
    DataClassification = "PII, PHI"
    DLM-Backup         = "Yes"
    DLM-Retention      = "90"
    PatchGroup         = "AmazonLinux2"
    VendorManaged      = "False"
    ResourceType       = "Client"
    ResourceSeverity   = "Normal"
    Name               = "A-MC-CAC-D-A005-BCAA-DEV01-MICROSERVICES-LNX-EC2"
    AccountID          = "${data.aws_caller_identity.current.account_id}"
    Role               = "Container"
    CLIENT             = "Bcaa"
    BILLINGCODE = "FPE01903-CO-TE-TO-1000"
    BILLINGCONTACT = "marrojas@deloitte.ca"
    COUNTRY = "CA"
    CSCLASS = "Confidential"
    CSQUAL = "CI/PI Data"
    CSTYPE = "External"
    ENVIRONMENT = "NPD"
    FUNCTION = "CON"
    MEMBERFIRM = "CA"
    PRIMARYCONTACT = "marrojas@deloitte.ca"
    SECONDARYCONTACT = "Samir Modh"
  }

  provisioner "local-exec" {
    command = "echo ${aws_instance.ICBCAA_DEV01_MICROSERVICES.private_ip} >> dev_hosts.txt"
  }
}

output "ICBCAA_DEV01_MICROSERVICES_PRIVATEIP" {
  value = "${aws_instance.ICBCAA_DEV01_MICROSERVICES.private_ip}"
}

######### HAPROXY ##############
resource "aws_instance" "ICBCAA_DEV01_INTERNAL_HAPROXY" {
  ami = "${var.ami_id_amazonlinux2base_2020_12_11}"

  vpc_security_group_ids = [
    "${aws_security_group.SG_INTERNAL_HAPROXY_DEV.id}",
    "${aws_security_group.SG_AVIATRIX_SSH.id}",
    "${aws_security_group.SG_JENKINS_CLIENT_SSH.id}",
    "${aws_security_group.SG_CLIENT_PING_TEST.id}",
  ]

  subnet_id                   = "${var.bcaa-dev-private-1a-subnet}"
  instance_type               = "t3a.small"
  key_name                    = "BcaaHAProxyDev"
  associate_public_ip_address = false
  iam_instance_profile        = "CMS-SSMInstanceRole"
  ebs_optimized               = "true"

  root_block_device {
    volume_size = "20"
  }

  tags {
    Owner              = "robhatnagar@deloitte.ca"
    ManagedBy          = "CMS-TF"
    SolutionName       = "Insurcloud Bcaa"
    Environment        = "Dev"
    ApplicationName    = "docker, haproxy"
    DataClassification = "PII, PHI"
    DLM-Backup         = "Yes"
    DLM-Retention      = "90"
    PatchGroup         = "AmazonLinux2"
    VendorManaged      = "False"
    ResourceType       = "Client"
    ResourceSeverity   = "Normal"
    Name               = "A-MC-CAC-D-A005-BCAA-DEV01-INTERNAL-HAPROXY-EC2"
    AccountID          = "${data.aws_caller_identity.current.account_id}"
    Role               = "Container"
    CLIENT             = "Bcaa"
    BILLINGCODE = "FPE01903-CO-TE-TO-1000"
    BILLINGCONTACT = "marrojas@deloitte.ca"
    COUNTRY = "CA"
    CSCLASS = "Confidential"
    CSQUAL = "CI/PI Data"
    CSTYPE = "External"
    ENVIRONMENT = "NPD"
    FUNCTION = "CON"
    MEMBERFIRM = "CA"
    PRIMARYCONTACT = "marrojas@deloitte.ca"
    SECONDARYCONTACT = "Samir Modh"
  }

  provisioner "local-exec" {
    command = "echo ${aws_instance.ICBCAA_DEV01_INTERNAL_HAPROXY.private_ip} >> dev_hosts.txt"
  }
}

output "ICBCAA_DEV01_INTERNAL_HAPROXY_PRIVATEIP" {
  value = "${aws_instance.ICBCAA_DEV01_INTERNAL_HAPROXY.private_ip}"
}

# End of DEV01


######################################

# #=======================================================================
# # DEV02
# #=======================================================================

######### GUIDEWIRE ##############
resource "aws_instance" "ICBCAA_DEV02_GUIDEWIRE_SUITE" {
  ami = "${var.ami_id_amazonlinux2base_2020_12_11}"

  vpc_security_group_ids = [
    "${aws_security_group.SG_GUIDEWIRE_DEV.id}",
    "${aws_security_group.SG_JENKINS_GUIDEWIRE_HEALTH_CHECK.id}",
    "${aws_security_group.SG_AVIATRIX_SSH.id}",
    "${aws_security_group.SG_JENKINS_CLIENT_SSH.id}",
    "${aws_security_group.SG_MSSQL_DB.id}"
  ]

  subnet_id                   = "${var.bcaa-dev-private-1a-subnet}"
  instance_type               = "r5a.xlarge"
  key_name                    = "BcaaGuidewireDev"
  associate_public_ip_address = false
  iam_instance_profile        = "CMS-SSMInstanceRole"
  ebs_optimized               = "true"

  root_block_device {
    volume_size = "35"
  }

  tags {
    Owner              = "robhatnagar@deloitte.ca"
    ManagedBy          = "CMS-TF"
    SolutionName       = "Insurcloud Bcaa"
    Environment        = "Dev"
    ApplicationName    = "guidewire, mssql, docker"
    DataClassification = "PII, PHI"
    DLM-Backup         = "Yes"
    DLM-Retention      = "90"
    PatchGroup         = "AmazonLinux2"
    VendorManaged      = "False"
    ResourceType       = "Client"
    ResourceSeverity   = "Normal"
    Name               = "A-MC-CAC-D-A005-BCAA-DEV02-GW-SUITE-LNX-EC2"
    AccountID          = "${data.aws_caller_identity.current.account_id}"
    Role               = "Container"
    CLIENT             = "Bcaa"
    BILLINGCODE = "FPE01903-CO-TE-TO-1000"
    BILLINGCONTACT = "marrojas@deloitte.ca"
    COUNTRY = "CA"
    CSCLASS = "Confidential"
    CSQUAL = "CI/PI Data"
    CSTYPE = "External"
    ENVIRONMENT = "NPD"
    FUNCTION = "CON"
    MEMBERFIRM = "CA"
    PRIMARYCONTACT = "marrojas@deloitte.ca"
    SECONDARYCONTACT = "Samir Modh"
  }

  provisioner "local-exec" {
    command = "echo ${aws_instance.ICBCAA_DEV02_GUIDEWIRE_SUITE.private_ip} >> dev_hosts.txt"
  }
}

output "ICBCAA_DEV02_GUIDEWIRE_SUITE_PRIVATEIP" {
  value = "${aws_instance.ICBCAA_DEV02_GUIDEWIRE_SUITE.private_ip}"
}

######### MICROSERVICES ###########
resource "aws_instance" "ICBCAA_DEV02_MICROSERVICES" {
  ami = "${var.ami_id_amazonlinux2base_2020_12_11}"

  vpc_security_group_ids = [
    "${aws_security_group.SG_MICROSERVICES_DEV.id}",
    "${aws_security_group.SG_MONITORING.id}",
    "${aws_security_group.SG_AVIATRIX_SSH.id}",
    "${aws_security_group.SG_JENKINS_CLIENT_SSH.id}",
    "${aws_security_group.SG_MYSQL_DB.id}",
  ]

  subnet_id                   = "${var.bcaa-dev-private-1a-subnet}"
  instance_type               = "r5a.xlarge"
  key_name                    = "BcaaMicroservicesDev"
  associate_public_ip_address = false
  iam_instance_profile        = "CMS-SSMInstanceRole"
  ebs_optimized               = "true"

  root_block_device {
    volume_size = "50"
  }

  tags {
    Owner              = "robhatnagar@deloitte.ca"
    ManagedBy          = "CMS-TF"
    SolutionName       = "Insurcloud Bcaa"
    Environment        = "Dev"
    ApplicationName    = "docker, microservices, mysql"
    DataClassification = "PII, PHI"
    DLM-Backup         = "Yes"
    DLM-Retention      = "90"
    PatchGroup         = "AmazonLinux2"
    VendorManaged      = "False"
    ResourceType       = "Client"
    ResourceSeverity   = "Normal"
    Name               = "A-MC-CAC-D-A005-BCAA-DEV02-MICROSERVICES-LNX-EC2"
    AccountID          = "${data.aws_caller_identity.current.account_id}"
    Role               = "Container"
    CLIENT             = "Bcaa"
    BILLINGCODE = "FPE01903-CO-TE-TO-1000"
    BILLINGCONTACT = "marrojas@deloitte.ca"
    COUNTRY = "CA"
    CSCLASS = "Confidential"
    CSQUAL = "CI/PI Data"
    CSTYPE = "External"
    ENVIRONMENT = "NPD"
    FUNCTION = "CON"
    MEMBERFIRM = "CA"
    PRIMARYCONTACT = "marrojas@deloitte.ca"
    SECONDARYCONTACT = "Samir Modh"
  }

  provisioner "local-exec" {
    command = "echo ${aws_instance.ICBCAA_DEV02_MICROSERVICES.private_ip} >> dev_hosts.txt"
  }
}

output "ICBCAA_DEV02_MICROSERVICES_PRIVATEIP" {
  value = "${aws_instance.ICBCAA_DEV02_MICROSERVICES.private_ip}"
}

# No HAPROXY for DEV02. Assumption is that we use one proxy for all instances
# End of DEV02
#################################################################