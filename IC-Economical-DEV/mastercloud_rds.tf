resource "aws_db_subnet_group" "RPA_MSSQL_DB_SUBNET" {
  name = "rpa-mssql-db-subnet"
  description = "RDS subnet group"
  subnet_ids = [
    "${var.economical-dev-database-1a-subnet}",
    "${var.economical-dev-database-1b-subnet}"]
}

# #=======================================================================
# # DEV01
# #=======================================================================

resource "aws_db_instance" "DEV01_RPA_MSSQL_DB" {
  license_model = "license-included"
  engine = "sqlserver-web"
  engine_version = "14.00.3281.6.v1"
  instance_class = "db.t3.medium"
  allocated_storage = 50
  storage_encrypted = true
  identifier = "economical-dev01-rpa-mssqldb"
  username = "sa"
  password = "${var.mssql-rds-password}"
  # password
  port = 1433
  timezone = "Eastern Standard Time"
  final_snapshot_identifier = "DELETEME"
  skip_final_snapshot = true

  domain = "d-9d6721a4ad"
  domain_iam_role_name = "CMS-SSMRole"

  maintenance_window = "Mon:00:00-Mon:03:00"
  backup_window = "03:00-06:00"
  deletion_protection = true

  db_subnet_group_name = "${aws_db_subnet_group.RPA_MSSQL_DB_SUBNET.name}"
  parameter_group_name = "default.sqlserver-web-14.0"
  multi_az = false
  # sqlserver-web does not support multi-az
  vpc_security_group_ids = [
    "${aws_security_group.SG_MSSQL_DB.id}"]
  storage_type = "gp2"
  backup_retention_period = 30
  # how long you’re going to keep your backups
  kms_key_id = "arn:aws:kms:ca-central-1:632109065501:key/4a629c34-1a27-45b4-a599-01ae15a45c0b"

  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "MANAGED_SERVICES"
    Name = "economical-dev01-rpa-mssqldb"
    Environment = "Dev"
  }
}

output "ICECONOMICAL_DEV01_RPA_DB_PRIVATEIP" {
  value = "${aws_db_instance.DEV01_RPA_MSSQL_DB.address}"
}


# #=======================================================================
# # DEV02
# #=======================================================================

resource "aws_db_instance" "DEV02_RPA_MSSQL_DB" {
  license_model = "license-included"
  engine = "sqlserver-web"
  engine_version = "14.00.3281.6.v1"
  instance_class = "db.t3.medium"
  allocated_storage = 100
  storage_encrypted = true
  kms_key_id = "arn:aws:kms:ca-central-1:632109065501:key/4a629c34-1a27-45b4-a599-01ae15a45c0b"
  identifier = "economical-dev02-rpa-mssqldb"
  username = "sa"
  password = "${var.mssql-rds-password}"
  # password
  port = 1433
  timezone = "Eastern Standard Time"
  final_snapshot_identifier = "DELETEME"
  skip_final_snapshot = true

  domain = "d-9d6721a4ad"
  domain_iam_role_name = "CMS-SSMRole"

  maintenance_window = "Mon:00:00-Mon:03:00"
  backup_window = "03:00-06:00"
  deletion_protection = true

  db_subnet_group_name = "${aws_db_subnet_group.RPA_MSSQL_DB_SUBNET.name}"
  parameter_group_name = "default.sqlserver-web-14.0"
  multi_az = false
  # sqlserver-web does not support multi-az
  vpc_security_group_ids = [
    "${aws_security_group.SG_MSSQL_DB.id}"]
  storage_type = "gp2"
  backup_retention_period = 30
  # how long you’re going to keep your backups

  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "PROJECT"
    Name = "economical-dev02-rpa-mssqldb"
    Environment = "DEV02"
  }
}

output "ICECONOMICAL_DEV02_RPA_DB_PRIVATEIP" {
  value = "${aws_db_instance.DEV02_RPA_MSSQL_DB.address}"
}


# #=======================================================================
# # ST01
# #=======================================================================

resource "aws_db_instance" "ST01_RPA_MSSQL_DB" {
  license_model = "license-included"
  engine = "sqlserver-web"
  engine_version = "14.00.3281.6.v1"
  instance_class = "db.t3.medium"
  allocated_storage = 50
  storage_encrypted = true
  identifier = "economical-st01-rpa-mssqldb"
  username = "sa"
  password = "${var.mssql-rds-password}"
  # password
  port = 1433
  timezone = "Eastern Standard Time"
  final_snapshot_identifier = "DELETEME"
  skip_final_snapshot = true

  domain = "d-9d6721a4ad"
  domain_iam_role_name = "CMS-SSMRole"

  maintenance_window = "Mon:00:00-Mon:03:00"
  backup_window = "03:00-06:00"
  deletion_protection = true

  db_subnet_group_name = "${aws_db_subnet_group.RPA_MSSQL_DB_SUBNET.name}"
  parameter_group_name = "default.sqlserver-web-14.0"
  multi_az = false
  # sqlserver-web does not support multi-az
  vpc_security_group_ids = [
    "${aws_security_group.SG_MSSQL_DB.id}"]
  storage_type = "gp2"
  backup_retention_period = 30
  # how long you’re going to keep your backups
  kms_key_id = "arn:aws:kms:ca-central-1:632109065501:key/4a629c34-1a27-45b4-a599-01ae15a45c0b"

  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "PROJECT"
    Name = "economical-st01-rpa-mssqldb"
    Environment = "ST01"
  }
}

output "ICECONOMICAL_ST01_RPA_DB_PRIVATEIP" {
  value = "${aws_db_instance.ST01_RPA_MSSQL_DB.address}"
}


# #=======================================================================
# # UAT01
# #=======================================================================

resource "aws_db_instance" "UAT01_RPA_MSSQL_DB" {
  license_model = "license-included"
  engine = "sqlserver-web"
  engine_version = "14.00.3281.6.v1"
  instance_class = "db.t3.medium"
  allocated_storage = 100
  storage_encrypted = true
  kms_key_id = "arn:aws:kms:ca-central-1:632109065501:key/4a629c34-1a27-45b4-a599-01ae15a45c0b"
  # sql express does not support storage encryption
  identifier = "economical-uat01-rpa-mssqldb"
  username = "sa"
  password = "${var.mssql-rds-password}"
  # password
  port = 1433
  timezone = "Eastern Standard Time"
  final_snapshot_identifier = "DELETEME"
  skip_final_snapshot = true

  domain = "d-9d6721a4ad"
  domain_iam_role_name = "CMS-SSMRole"

  maintenance_window = "Mon:00:00-Mon:03:00"
  backup_window = "03:00-06:00"
  deletion_protection = true

  db_subnet_group_name = "${aws_db_subnet_group.RPA_MSSQL_DB_SUBNET.name}"
  parameter_group_name = "default.sqlserver-web-14.0"
  multi_az = false
  # sqlserver-web does not support multi-az
  vpc_security_group_ids = [
    "${aws_security_group.SG_MSSQL_DB.id}"]
  storage_type = "gp2"
  backup_retention_period = 30
  # how long you’re going to keep your backups

  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "PROJECT"
    Name = "economical-uat01-rpa-mssqldb"
    Environment = "UAT01"
  }
}

output "ICECONOMICAL_UAT01_RPA_DB_PRIVATEIP" {
  value = "${aws_db_instance.UAT01_RPA_MSSQL_DB.address}"
}


# #=======================================================================
# # PRF01
# #=======================================================================

resource "aws_db_instance" "PRF01_RPA_MSSQL_DB" {
  license_model = "license-included"
  engine = "sqlserver-web"
  engine_version = "14.00.3281.6.v1"
  instance_class = "db.t3.medium"
  allocated_storage = 100
  storage_encrypted = true
  # sql express does not support storage encryption
  identifier = "economical-prf01-rpa-mssqldb"
  username = "sa"
  password = "${var.mssql-rds-password}"
  # password
  port = 1433
  timezone = "Eastern Standard Time"
  final_snapshot_identifier = "DELETEME"
  skip_final_snapshot = true

  domain = "d-9d6721a4ad"
  domain_iam_role_name = "CMS-SSMRole"

  maintenance_window = "Mon:00:00-Mon:03:00"
  backup_window = "03:00-06:00"
  deletion_protection = true

  db_subnet_group_name = "${aws_db_subnet_group.RPA_MSSQL_DB_SUBNET.name}"
  parameter_group_name = "default.sqlserver-web-14.0"
  multi_az = false
  # sqlserver-web does not support multi-az
  vpc_security_group_ids = [
    "${aws_security_group.SG_MSSQL_DB.id}"]
  storage_type = "gp2"
  backup_retention_period = 30
  # how long you’re going to keep your backups

  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "PROJECT"
    Name = "economical-prf01-rpa-mssqldb"
    Environment = "PRF01"
  }
}

output "ICECONOMICAL_PRF01_RPA_DB_PRIVATEIP" {
  value = "${aws_db_instance.PRF01_RPA_MSSQL_DB.address}"
}

# #=======================================================================
# # SIT01
# #=======================================================================

resource "aws_db_instance" "SIT01_RPA_MSSQL_DB" {
  license_model = "license-included"
  engine = "sqlserver-web"
  engine_version = "14.00.3281.6.v1"
  instance_class = "db.t3.medium"
  allocated_storage = 100
  storage_encrypted = true
  kms_key_id = "arn:aws:kms:ca-central-1:632109065501:key/4a629c34-1a27-45b4-a599-01ae15a45c0b"
  # sql express does not support storage encryption
  identifier = "economical-sit01-rpa-mssqldb"
  username = "sa"
  password = "${var.mssql-rds-password}"
  # password
  port = 1433
  timezone = "Eastern Standard Time"
  final_snapshot_identifier = "DELETEME"
  skip_final_snapshot = true

  domain = "d-9d6721a4ad"
  domain_iam_role_name = "CMS-SSMRole"

  maintenance_window = "Mon:00:00-Mon:03:00"
  backup_window = "03:00-06:00"
  deletion_protection = true

  db_subnet_group_name = "${aws_db_subnet_group.RPA_MSSQL_DB_SUBNET.name}"
  parameter_group_name = "default.sqlserver-web-14.0"
  multi_az = false
  # sqlserver-web does not support multi-az
  vpc_security_group_ids = [
    "${aws_security_group.SG_MSSQL_DB.id}"]
  storage_type = "gp2"
  backup_retention_period = 30
  # how long you’re going to keep your backups

  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "PROJECT"
    Name = "economical-sit01-rpa-mssqldb"
    Environment = "SIT01"
  }
}

output "ICECONOMICAL_SIT01_RPA_DB_PRIVATEIP" {
  value = "${aws_db_instance.SIT01_RPA_MSSQL_DB.address}"
}

# #=======================================================================
# # PPS01
# #=======================================================================

resource "aws_db_instance" "PPS01_RPA_MSSQL_DB" {
  license_model = "license-included"
  engine = "sqlserver-web"
  engine_version = "14.00.3281.6.v1"
  instance_class = "db.m5.large"
  allocated_storage = 100
  storage_encrypted = true
  # sql express does not support storage encryption
  identifier = "economical-pps01-rpa-mssqldb"
  username = "sa"
  password = "${var.mssql-rds-password}"
  # password
  port = 1433
  timezone = "Eastern Standard Time"
  final_snapshot_identifier = "DELETEME"
  skip_final_snapshot = true
  snapshot_identifier = "economical-pps01-rpa-mssqldb-dec-08-2020-web"

  domain = "d-9d6721a4ad"
  domain_iam_role_name = "CMS-SSMRole"

  maintenance_window = "Mon:00:00-Mon:03:00"
  backup_window = "03:00-06:00"
  deletion_protection = false

  db_subnet_group_name = "${aws_db_subnet_group.RPA_MSSQL_DB_SUBNET.name}"
  parameter_group_name = "default.sqlserver-web-14.0"
  multi_az = false
  vpc_security_group_ids = [
    "${aws_security_group.SG_MSSQL_DB.id}"]
  storage_type = "gp2"
  backup_retention_period = 30
  # how long you’re going to keep your backups

  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "MANAGED_SERVICES"
    Name = "economical-pps01-rpa-mssqldb"
    Environment = "PPS01"
  }
}

output "ICECONOMICAL_PPS01_RPA_DB_PRIVATEIP" {
  value = "${aws_db_instance.PPS01_RPA_MSSQL_DB.address}"
}
