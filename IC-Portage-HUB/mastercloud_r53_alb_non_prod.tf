
# #=======================================================================
# # ALB NON PROD
# #=======================================================================

# DEV 02

resource "aws_route53_record" "ICPORTAGE_ALB_DEV02_GWPC_RECORD" {
  zone_id = "${var.zone_id}"
  name    = "dev02-gwpc.nonprod.portage.insurcloud.ca"
  type    = "A"

  alias {
    name                   = "${data.terraform_remote_state.dev_remote_state.aws_lb_nonprod_portage_load_balancer_dns}"
    zone_id                = "${data.terraform_remote_state.dev_remote_state.aws_lb_nonprod_portage_load_balancer_zone_id}"
    evaluate_target_health = true
  }
}

resource "aws_route53_record" "ICPORTAGE_ALB_DEV02_GWBC_RECORD" {
  zone_id = "${var.zone_id}"
  name    = "dev02-gwbc.nonprod.portage.insurcloud.ca"
  type    = "A"

  alias {
    name                   = "${data.terraform_remote_state.dev_remote_state.aws_lb_nonprod_portage_load_balancer_dns}"
    zone_id                = "${data.terraform_remote_state.dev_remote_state.aws_lb_nonprod_portage_load_balancer_zone_id}"
    evaluate_target_health = true
  }
}

resource "aws_route53_record" "ICPORTAGE_ALB_DEV02_GWCM_RECORD" {
  zone_id = "${var.zone_id}"
  name    = "dev02-gwcm.nonprod.portage.insurcloud.ca"
  type    = "A"

  alias {
    name                   = "${data.terraform_remote_state.dev_remote_state.aws_lb_nonprod_portage_load_balancer_dns}"
    zone_id                = "${data.terraform_remote_state.dev_remote_state.aws_lb_nonprod_portage_load_balancer_zone_id}"
    evaluate_target_health = true
  }
}

resource "aws_route53_record" "ICPORTAGE_ALB_DEV02_MSADM_RECORD" {
  zone_id = "${var.zone_id}"
  name    = "dev02-ms-admin.nonprod.portage.insurcloud.ca"
  type    = "A"

  alias {
    name                   = "${data.terraform_remote_state.dev_remote_state.aws_lb_nonprod_portage_load_balancer_dns}"
    zone_id                = "${data.terraform_remote_state.dev_remote_state.aws_lb_nonprod_portage_load_balancer_zone_id}"
    evaluate_target_health = true
  }
}

resource "aws_route53_record" "ICPORTAGE_ALB_DEV02_MSEDGE_RECORD" {
  zone_id = "${var.zone_id}"
  name    = "dev02-ms-edge.nonprod.portage.insurcloud.ca"
  type    = "A"

  alias {
    name                   = "${data.terraform_remote_state.dev_remote_state.aws_lb_nonprod_portage_load_balancer_dns}"
    zone_id                = "${data.terraform_remote_state.dev_remote_state.aws_lb_nonprod_portage_load_balancer_zone_id}"
    evaluate_target_health = true
  }
}

# DEV 01

resource "aws_route53_record" "ICPORTAGE_ALB_DEV01_GWPC_RECORD" {
  zone_id = "${var.zone_id}"
  name    = "dev01-gwpc.nonprod.portage.insurcloud.ca"
  type    = "A"

  alias {
    name                   = "${data.terraform_remote_state.dev_remote_state.aws_lb_nonprod_portage_load_balancer_dns}"
    zone_id                = "${data.terraform_remote_state.dev_remote_state.aws_lb_nonprod_portage_load_balancer_zone_id}"
    evaluate_target_health = true
  }
}

resource "aws_route53_record" "ICPORTAGE_ALB_DEV01_GWBC_RECORD" {
  zone_id = "${var.zone_id}"
  name    = "dev01-gwbc.nonprod.portage.insurcloud.ca"
  type    = "A"

  alias {
    name                   = "${data.terraform_remote_state.dev_remote_state.aws_lb_nonprod_portage_load_balancer_dns}"
    zone_id                = "${data.terraform_remote_state.dev_remote_state.aws_lb_nonprod_portage_load_balancer_zone_id}"
    evaluate_target_health = true
  }
}

resource "aws_route53_record" "ICPORTAGE_ALB_DEV01_GWCM_RECORD" {
  zone_id = "${var.zone_id}"
  name    = "dev01-gwcm.nonprod.portage.insurcloud.ca"
  type    = "A"

  alias {
    name                   = "${data.terraform_remote_state.dev_remote_state.aws_lb_nonprod_portage_load_balancer_dns}"
    zone_id                = "${data.terraform_remote_state.dev_remote_state.aws_lb_nonprod_portage_load_balancer_zone_id}"
    evaluate_target_health = true
  }
}

resource "aws_route53_record" "ICPORTAGE_ALB_DEV01_MSADM_RECORD" {
  zone_id = "${var.zone_id}"
  name    = "dev01-ms-admin.nonprod.portage.insurcloud.ca"
  type    = "A"

  alias {
    name                   = "${data.terraform_remote_state.dev_remote_state.aws_lb_nonprod_portage_load_balancer_dns}"
    zone_id                = "${data.terraform_remote_state.dev_remote_state.aws_lb_nonprod_portage_load_balancer_zone_id}"
    evaluate_target_health = true
  }
}

resource "aws_route53_record" "ICPORTAGE_ALB_DEV01_MSEDGE_RECORD" {
  zone_id = "${var.zone_id}"
  name    = "dev01-ms-edge.nonprod.portage.insurcloud.ca"
  type    = "A"

  alias {
    name                   = "${data.terraform_remote_state.dev_remote_state.aws_lb_nonprod_portage_load_balancer_dns}"
    zone_id                = "${data.terraform_remote_state.dev_remote_state.aws_lb_nonprod_portage_load_balancer_zone_id}"
    evaluate_target_health = true
  }
}

# CNV 01

resource "aws_route53_record" "ICPORTAGE_ALB_CNV01_GWPC_RECORD" {
  zone_id = "${var.zone_id}"
  name    = "cnv01-gwpc.nonprod.portage.insurcloud.ca"
  type    = "A"

  alias {
    name                   = "${data.terraform_remote_state.dev_remote_state.aws_lb_nonprod_portage_load_balancer_dns}"
    zone_id                = "${data.terraform_remote_state.dev_remote_state.aws_lb_nonprod_portage_load_balancer_zone_id}"
    evaluate_target_health = true
  }
}

resource "aws_route53_record" "ICPORTAGE_ALB_CNV01_GWBC_RECORD" {
  zone_id = "${var.zone_id}"
  name    = "cnv01-gwbc.nonprod.portage.insurcloud.ca"
  type    = "A"

  alias {
    name                   = "${data.terraform_remote_state.dev_remote_state.aws_lb_nonprod_portage_load_balancer_dns}"
    zone_id                = "${data.terraform_remote_state.dev_remote_state.aws_lb_nonprod_portage_load_balancer_zone_id}"
    evaluate_target_health = true
  }
}

resource "aws_route53_record" "ICPORTAGE_ALB_CNV01_GWCM_RECORD" {
  zone_id = "${var.zone_id}"
  name    = "cnv01-gwcm.nonprod.portage.insurcloud.ca"
  type    = "A"

  alias {
    name                   = "${data.terraform_remote_state.dev_remote_state.aws_lb_nonprod_portage_load_balancer_dns}"
    zone_id                = "${data.terraform_remote_state.dev_remote_state.aws_lb_nonprod_portage_load_balancer_zone_id}"
    evaluate_target_health = true
  }
}

resource "aws_route53_record" "ICPORTAGE_ALB_CNV01_MSADM_RECORD" {
  zone_id = "${var.zone_id}"
  name    = "cnv01-ms-admin.nonprod.portage.insurcloud.ca"
  type    = "A"

  alias {
    name                   = "${data.terraform_remote_state.dev_remote_state.aws_lb_nonprod_portage_load_balancer_dns}"
    zone_id                = "${data.terraform_remote_state.dev_remote_state.aws_lb_nonprod_portage_load_balancer_zone_id}"
    evaluate_target_health = true
  }
}

resource "aws_route53_record" "ICPORTAGE_ALB_CNV01_MSEDGE_RECORD" {
  zone_id = "${var.zone_id}"
  name    = "cnv01-ms-edge.nonprod.portage.insurcloud.ca"
  type    = "A"

  alias {
    name                   = "${data.terraform_remote_state.dev_remote_state.aws_lb_nonprod_portage_load_balancer_dns}"
    zone_id                = "${data.terraform_remote_state.dev_remote_state.aws_lb_nonprod_portage_load_balancer_zone_id}"
    evaluate_target_health = true
  }
}

# QA 01

resource "aws_route53_record" "ICPORTAGE_ALB_QA01_GWPC_RECORD" {
  zone_id = "${var.zone_id}"
  name    = "qa01-gwpc.nonprod.portage.insurcloud.ca"
  type    = "A"

  alias {
    name                   = "${data.terraform_remote_state.dev_remote_state.aws_lb_nonprod_portage_load_balancer_dns}"
    zone_id                = "${data.terraform_remote_state.dev_remote_state.aws_lb_nonprod_portage_load_balancer_zone_id}"
    evaluate_target_health = true
  }
}

resource "aws_route53_record" "ICPORTAGE_ALB_QA01_GWBC_RECORD" {
  zone_id = "${var.zone_id}"
  name    = "qa01-gwbc.nonprod.portage.insurcloud.ca"
  type    = "A"

  alias {
    name                   = "${data.terraform_remote_state.dev_remote_state.aws_lb_nonprod_portage_load_balancer_dns}"
    zone_id                = "${data.terraform_remote_state.dev_remote_state.aws_lb_nonprod_portage_load_balancer_zone_id}"
    evaluate_target_health = true
  }
}

resource "aws_route53_record" "ICPORTAGE_ALB_QA01_GWCM_RECORD" {
  zone_id = "${var.zone_id}"
  name    = "qa01-gwcm.nonprod.portage.insurcloud.ca"
  type    = "A"

  alias {
    name                   = "${data.terraform_remote_state.dev_remote_state.aws_lb_nonprod_portage_load_balancer_dns}"
    zone_id                = "${data.terraform_remote_state.dev_remote_state.aws_lb_nonprod_portage_load_balancer_zone_id}"
    evaluate_target_health = true
  }
}

resource "aws_route53_record" "ICPORTAGE_ALB_QA01_MSADM_RECORD" {
  zone_id = "${var.zone_id}"
  name    = "qa01-ms-admin.nonprod.portage.insurcloud.ca"
  type    = "A"

  alias {
    name                   = "${data.terraform_remote_state.dev_remote_state.aws_lb_nonprod_portage_load_balancer_dns}"
    zone_id                = "${data.terraform_remote_state.dev_remote_state.aws_lb_nonprod_portage_load_balancer_zone_id}"
    evaluate_target_health = true
  }
}

resource "aws_route53_record" "ICPORTAGE_ALB_QA01_MSEDGE_RECORD" {
  zone_id = "${var.zone_id}"
  name    = "qa01-ms-edge.nonprod.portage.insurcloud.ca"
  type    = "A"

  alias {
    name                   = "${data.terraform_remote_state.dev_remote_state.aws_lb_nonprod_portage_load_balancer_dns}"
    zone_id                = "${data.terraform_remote_state.dev_remote_state.aws_lb_nonprod_portage_load_balancer_zone_id}"
    evaluate_target_health = true
  }
}

# QA 02

resource "aws_route53_record" "ICPORTAGE_ALB_QA02_GWPC_RECORD" {
  zone_id = "${var.zone_id}"
  name    = "qa02-gwpc.nonprod.portage.insurcloud.ca"
  type    = "A"

  alias {
    name                   = "${data.terraform_remote_state.dev_remote_state.aws_lb_nonprod_portage_load_balancer_dns}"
    zone_id                = "${data.terraform_remote_state.dev_remote_state.aws_lb_nonprod_portage_load_balancer_zone_id}"
    evaluate_target_health = true
  }
}

resource "aws_route53_record" "ICPORTAGE_ALB_QA02_GWBC_RECORD" {
  zone_id = "${var.zone_id}"
  name    = "qa02-gwbc.nonprod.portage.insurcloud.ca"
  type    = "A"

  alias {
    name                   = "${data.terraform_remote_state.dev_remote_state.aws_lb_nonprod_portage_load_balancer_dns}"
    zone_id                = "${data.terraform_remote_state.dev_remote_state.aws_lb_nonprod_portage_load_balancer_zone_id}"
    evaluate_target_health = true
  }
}

resource "aws_route53_record" "ICPORTAGE_ALB_QA02_GWCM_RECORD" {
  zone_id = "${var.zone_id}"
  name    = "qa02-gwcm.nonprod.portage.insurcloud.ca"
  type    = "A"

  alias {
    name                   = "${data.terraform_remote_state.dev_remote_state.aws_lb_nonprod_portage_load_balancer_dns}"
    zone_id                = "${data.terraform_remote_state.dev_remote_state.aws_lb_nonprod_portage_load_balancer_zone_id}"
    evaluate_target_health = true
  }
}

resource "aws_route53_record" "ICPORTAGE_ALB_QA02_MSADM_RECORD" {
  zone_id = "${var.zone_id}"
  name    = "qa02-ms-admin.nonprod.portage.insurcloud.ca"
  type    = "A"

  alias {
    name                   = "${data.terraform_remote_state.dev_remote_state.aws_lb_nonprod_portage_load_balancer_dns}"
    zone_id                = "${data.terraform_remote_state.dev_remote_state.aws_lb_nonprod_portage_load_balancer_zone_id}"
    evaluate_target_health = true
  }
}

resource "aws_route53_record" "ICPORTAGE_ALB_QA02_MSEDGE_RECORD" {
  zone_id = "${var.zone_id}"
  name    = "qa02-ms-edge.nonprod.portage.insurcloud.ca"
  type    = "A"

  alias {
    name                   = "${data.terraform_remote_state.dev_remote_state.aws_lb_nonprod_portage_load_balancer_dns}"
    zone_id                = "${data.terraform_remote_state.dev_remote_state.aws_lb_nonprod_portage_load_balancer_zone_id}"
    evaluate_target_health = true
  }
}