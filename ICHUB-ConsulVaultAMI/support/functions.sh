# Create private key for the service
# Arguments
#   $1 Name and location of the private key. For e.g /var/tmp/secrets/api.service.key.pem
function generate-rsa-key() {
  openssl genrsa -out $1 2048
}

# Generate certificate signin request
# Arguments
#   $1 Name and location of the private key. For e.g /var/tmp/secrets/api.service.key.pem
#.  $2 Fully qualified name and location of the csr For e.g /var/tmp/secrets/api.csr.pem
function generate-csr() {
  openssl req -new -config $1 -key $2 -out $3
}

function get-certificate() {
  aws acm-pca get-certificate \
    --certificate-authority-arn ${CERT_AUHTORITY_ARN} \
    --certificate-arn $1 \
    --output json \
    > $2
}

function issue-certificate() {
  aws acm-pca issue-certificate \
    --certificate-authority-arn ${CERT_AUHTORITY_ARN} \
    --csr file://$1 \
    --signing-algorithm "SHA256WITHRSA" \
    --validity Value=$2,Type="DAYS" \
    --output text
}
