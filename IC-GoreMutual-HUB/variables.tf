variable "client_name" {
  description = "AWS client name for resources (Ex: MASTERCLOUD)"
  default = "GORE"
}

variable "environment_name" {
  description = "AWS environment name for resources (Ex: NON-PROD)"
  default = ""
}

variable "environment_type" {
  description = "AWS environment type for resources (Ex: P=PROD/HUB, D=NON-PROD)"
  default = ""
}

variable "client_number" {
  description = "AWS client number for tagging (Ex: A001)"
  default = "A004"
}

variable "region" {
  description = "AWS region for hosting our your network"
  default = "ca-central-1"
}

variable "volume_key" {
  description = "KMS volume key hub account"
  default = ""
}

variable "ami_id_windows2012sql14enterprise" {
  description = "Windows 2012 SQL 2014 Enterprise Encypted Root Volume AMI"
  default = ""
}

variable "ami_id_windows2019container" {
  description = "Windows 2019 Container Encypted Root Volume AMI"
  default = ""
}

variable "ami_id_windows2019base" {
  description = "Windows 2019 Base Encypted Root Volume AMI"
  default = ""
}

variable "ami_id_windows2016base" {
  description = "Windows 2016 Base Encypted Root Volume AMI"
  default = ""
}

variable "ami_id_windows2012R2base" {
  description = "Windows 2012 R2 Base Encypted Root Volume AMI"
  default = ""
}

variable "ami_id_windows2008base" {
  description = "Windows 2008 Base Encypted Root Volume AMI"
  default = ""
}

variable "ami_id_redhatlinux75" {
  description = "Redhat Linux 7.5 Encypted Root Volume AMI"
  default = ""
}


variable "ami_id_ubuntu1804baseimage" {
  description = "Ubuntu 18.04 Base Encypted Root Volume AMI"
  default = ""
}


variable "ami_id_ubuntu1604baseimage" {
  description = "Ubuntu 16.04 Base Encypted Root Volume AMI"
  default = ""
}

variable "ami_id_amazonlinux201712base" {
  description = "Amazon Linux 2017 12 Base Encypted Root Volume AMI"
  default = ""
}

variable "ami_id_amazonlinux201709base" {
  description = "Amazon Linux 2017 09 Base Encypted Root Volume AMI"
  default = ""
}

variable "ami_id_amazonlinux2base" {
  description = "Amazon Linux 2 Base Encypted Root Volume AMI"
  default = ""
}

variable "ami_id_amazonlinux201803base" {
  description = "Amazon Linux 2018 03 Base Encypted Root Volume AMI"
  default = ""
}


variable "vpc_id" {
  description = "VPC id for HUB account"
  default = "vpc-0cf5ca59ac23bcbc8"
}

variable "vpc_cidr" {
  description = "VPC cidr dev account"
  default = ""
}

variable "conduit-rds-password" {
  description = "Conduit DB Password"
  default = ""
}

variable "gore-hub-database-1a-subnet" {
  description = "Database subnet one"
  default = ""
}

variable "gore-hub-database-1b-subnet" {
  description = "Database subnet two"
  default = ""
}

variable "gore-hub-private-1a-subnet" {
  description = "Private Subnet One"
  default = ""
}

variable "gore-hub-private-1b-subnet" {
  description = "Private Subnet Two"
  default = ""
}

variable "gore-hub-public-1a-subnet" {
  description = "Public Subnet One"
  default = ""
}

variable "gore-hub-public-1b-subnet" {
  description = "Public Subnet two"
  default = ""
}

variable "default_cidr" {
  description = "default cidr used"
  default = ["10.0.0.0/8"]
}

variable "deloitte_cidr" {
    default = "10.10.4.0/22"
}

variable "gore_cidr" {
    default = "192.168.24.0/23"
}

variable "zone_id" {
  description = "default zone id"
  default = ""
}

variable "public_zone_id" {
  description = "default public route zone id"
  default = ""
}

variable "gore_dev_cidr" {
 description = "default cidr used"
 type = "list"
 default = ["192.168.28.0/22"]
}
 
variable "gore_prod_cidr" {
 description = "default cidr used"
 type = "list"
 default = ["192.168.24.0/22"]
}
 
variable "gore_hub_cidr" {
 description = "default cidr used"
 type = "list"
 default = ["192.168.20.0/22"]
}
variable "gore_hub_private_cidr" {
  description = "default cidr used"
  type = "list"
  default = ["192.168.20.0/24", "192.168.21.0/24"]
}
variable "gore_aviatrix_vpn" {
    default = "192.168.22.19/32"
}

variable "ad_dns1" {
    default = "10.227.0.139"
}
variable "ad_dns2" {
    default = "10.227.1.6"
}
variable "ad_dns3" {
    default = "10.227.0.2"
}
variable "ou_name" {
    default = "dev"
}
variable "pwordparameter" {
    default = "domain_password"
}
variable "unameparameter" {
    default = "domain_username"
}
variable "ad_domain" {
    default = "internal.gore.insurcloud.ca"
}
data "aws_caller_identity" "current" {}

variable "gore-dev-account" {
  default = "780193124257"
}

variable "gore-prod-account" {
  default = "948423377884"
}

variable "gore-hub-account" {
  default = "963473599983"
}
