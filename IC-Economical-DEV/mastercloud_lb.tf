# #=======================================================================
# # DEV01
# #=======================================================================
##### ALBs #####

##### NLBs ####################
########## Internal ###########
resource "aws_lb" "ICECONOMICAL_DEV01_INTERNAL_NLB" {
  name = "ICECONOMICAL-DEV01-INTERNAL-NLB"
  internal = true
  load_balancer_type = "network"
  enable_cross_zone_load_balancing = false
  subnets = [
    "${var.economical-dev-private-1a-subnet}",
    "${var.economical-dev-private-1b-subnet}"
  ]

  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "MANAGED_SERVICES"
    Name = "ICECONOMICAL-DEV01-INTERNAL-NLB"
  }
}

resource "aws_lb_target_group" "ICECO_DEV01_INT_NLB_HAPROXY_TG" {
  name = "ICECO-DEV01-HAPROXY-INT-NLB"
  port = 80
  protocol = "TCP"
  vpc_id = "${var.vpc_id}"

  health_check {
    protocol = "TCP"
    port = 8444
  }

  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "MANAGED_SERVICES"
    Name = "ICECO-DEV01-HAPROXY-INT-NLB-TG"
  }
}

resource "aws_lb_target_group" "ICECO_DEV01_INT_NLB_HAPROXY_TG2" {
  name = "ICECO-DEV01-HAPROXY-INT-NLB2"
  port = 443
  protocol = "TCP"
  vpc_id = "${var.vpc_id}"

  health_check {
    protocol = "TCP"
    port = 8444
  }

  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "MANAGED_SERVICES"
    Name = "ICECO-DEV01-HAPROXY-INT-NLB-TG2"
  }
}

resource "aws_lb_listener" "ICECONOMICAL_DEV01_INTERNAL_NLB_HAPROXY_LISTENER" {
  load_balancer_arn = "${aws_lb.ICECONOMICAL_DEV01_INTERNAL_NLB.arn}"
  port = "80"
  protocol = "TCP"

  default_action {
    type = "forward"
    target_group_arn = "${aws_lb_target_group.ICECO_DEV01_INT_NLB_HAPROXY_TG.arn}"
  }
}

resource "aws_lb_listener" "ICECONOMICAL_DEV01_INTERNAL_NLB_HAPROXY_LISTENER2" {
  load_balancer_arn = "${aws_lb.ICECONOMICAL_DEV01_INTERNAL_NLB.arn}"
  port = "443"
  protocol = "TCP"

  default_action {
    type = "forward"
    target_group_arn = "${aws_lb_target_group.ICECO_DEV01_INT_NLB_HAPROXY_TG2.arn}"
  }
}

resource "aws_lb_target_group_attachment" "ICECONOMICAL_DEV01_INTERNAL_NLB_TARGET_GROUP_ATTACHMENT_HAPROXY" {
  target_group_arn = "${aws_lb_target_group.ICECO_DEV01_INT_NLB_HAPROXY_TG.arn}"
  target_id = "${aws_instance.ICECONOMICAL_DEV01_INTERNAL_HAPROXY.id}"
  port = 80
}

resource "aws_lb_target_group_attachment" "ICECONOMICAL_DEV01_INTERNAL_NLB_TARGET_GROUP_ATTACHMENT_HAPROXY2" {
  target_group_arn = "${aws_lb_target_group.ICECO_DEV01_INT_NLB_HAPROXY_TG2.arn}"
  target_id = "${aws_instance.ICECONOMICAL_DEV01_INTERNAL_HAPROXY.id}"
  port = 443
}

output "ICECONOMICAL_DEV01_INTERNAL_NLB_DNS_NAME" {
  value = "${aws_lb.ICECONOMICAL_DEV01_INTERNAL_NLB.dns_name}"
}

output "ICECONOMICAL_DEV01_INTERNAL_NLB_ZONE_ID" {
  value = "${aws_lb.ICECONOMICAL_DEV01_INTERNAL_NLB.zone_id}"
}

########## External ###########
resource "aws_lb" "ICECONOMICAL_DEV01_EXTERNAL_NLB" {
  name = "ICECONOMICAL-DEV01-EXTERNAL-NLB"
  internal = true
  load_balancer_type = "network"
  enable_cross_zone_load_balancing = false
  subnets = [
    "${var.economical-dev-private-1a-subnet}",
    "${var.economical-dev-private-1b-subnet}"
  ]
  # Changing this value will force a recreation of the resource

  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "MANAGED_SERVICES"
    Name = "ICECONOMICAL-DEV01-EXTERNAL-NLB"
  }
}

resource "aws_lb_target_group" "ICECO_DEV01_EXT_NLB_HAPROXY_TG" {
  name = "ICECO-DEV01-HAPROXY-EXT-NLB"
  port = 80
  protocol = "TCP"
  vpc_id = "${var.vpc_id}"

  health_check {
    protocol = "TCP"
    port = 8444
  }

  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "MANAGED_SERVICES"
    Name = "ICECO-DEV01-EXT-NLB-HAPROXY-TG"
  }
}

resource "aws_lb_target_group" "ICECO_DEV01_EXT_NLB_HAPROXY_TG2" {
  name = "ICECO-DEV01-HAPROXY-EXT-NLB2"
  port = 443
  protocol = "TCP"
  vpc_id = "${var.vpc_id}"

  health_check {
    protocol = "TCP"
    port = 8444
  }

  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "MANAGED_SERVICES"
    Name = "ICECO-DEV01-EXT-NLB-HAPROXY-TG2"
  }
}

resource "aws_lb_listener" "ICECONOMICAL_DEV01_EXTERNAL_NLB_HAPROXY_LISTENER" {
  load_balancer_arn = "${aws_lb.ICECONOMICAL_DEV01_EXTERNAL_NLB.arn}"
  port = "80"
  protocol = "TCP"

  default_action {
    type = "forward"
    target_group_arn = "${aws_lb_target_group.ICECO_DEV01_EXT_NLB_HAPROXY_TG.arn}"
  }
}

resource "aws_lb_listener" "ICECONOMICAL_DEV01_EXTERNAL_NLB_HAPROXY_LISTENER2" {
  load_balancer_arn = "${aws_lb.ICECONOMICAL_DEV01_EXTERNAL_NLB.arn}"
  port = "443"
  protocol = "TCP"

  default_action {
    type = "forward"
    target_group_arn = "${aws_lb_target_group.ICECO_DEV01_EXT_NLB_HAPROXY_TG2.arn}"
  }
}

resource "aws_lb_target_group_attachment" "ICECONOMICAL_DEV01_EXTERNAL_NLB_TARGET_GROUP_ATTACHMENT_HAPROXY" {
  target_group_arn = "${aws_lb_target_group.ICECO_DEV01_EXT_NLB_HAPROXY_TG.arn}"
  target_id = "${aws_instance.ICECONOMICAL_DEV01_EXTERNAL_HAPROXY.id}"
  port = 80
}

resource "aws_lb_target_group_attachment" "ICECONOMICAL_DEV01_EXTERNAL_NLB_TARGET_GROUP_ATTACHMENT_HAPROXY2" {
  target_group_arn = "${aws_lb_target_group.ICECO_DEV01_EXT_NLB_HAPROXY_TG2.arn}"
  target_id = "${aws_instance.ICECONOMICAL_DEV01_EXTERNAL_HAPROXY.id}"
  port = 443
}

output "ICECONOMICAL_DEV01_EXTERNAL_NLB_DNS_NAME" {
  value = "${aws_lb.ICECONOMICAL_DEV01_EXTERNAL_NLB.dns_name}"
}

output "ICECONOMICAL_DEV01_EXTERNAL_NLB_ZONE_ID" {
  value = "${aws_lb.ICECONOMICAL_DEV01_EXTERNAL_NLB.zone_id}"
}


# #=======================================================================
# # DEV02
# #=======================================================================
##### ALBs #####

##### NLBs ####################
########## Internal ###########
resource "aws_lb" "ICECONOMICAL_DEV02_INTERNAL_NLB" {
  name = "ICECONOMICAL-DEV02-INTERNAL-NLB"
  internal = true
  load_balancer_type = "network"
  enable_cross_zone_load_balancing = false
  subnets = [
    "${var.economical-dev-private-1a-subnet}",
    "${var.economical-dev-private-1b-subnet}"
  ]

  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "PROJECT"
    Name = "ICECONOMICAL-DEV02-INTERNAL-NLB"
  }
}

resource "aws_lb_target_group" "ICECO_DEV02_INT_NLB_HAPROXY_TG" {
  name = "ICECO-DEV02-HAPROXY-INT-NLB"
  port = 80
  protocol = "TCP"
  vpc_id = "${var.vpc_id}"

  health_check {
    protocol = "TCP"
    port = 8444
  }

  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "MANAGED_SERVICES"
    Name = "ICECO-DEV02-HAPROXY-INT-NLB-TG"
  }
}

resource "aws_lb_target_group" "ICECO_DEV02_INT_NLB_HAPROXY_TG2" {
  name = "ICECO-DEV02-HAPROXY-INT-NLB2"
  port = 443
  protocol = "TCP"
  vpc_id = "${var.vpc_id}"

  health_check {
    protocol = "TCP"
    port = 8444
  }

  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "MANAGED_SERVICES"
    Name = "ICECO-DEV02-HAPROXY-INT-NLB-TG2"
  }
}

resource "aws_lb_listener" "ICECONOMICAL_DEV02_INTERNAL_NLB_HAPROXY_LISTENER" {
  load_balancer_arn = "${aws_lb.ICECONOMICAL_DEV02_INTERNAL_NLB.arn}"
  port = "80"
  protocol = "TCP"

  default_action {
    type = "forward"
    target_group_arn = "${aws_lb_target_group.ICECO_DEV02_INT_NLB_HAPROXY_TG.arn}"
  }
}

resource "aws_lb_listener" "ICECONOMICAL_DEV02_INTERNAL_NLB_HAPROXY_LISTENER2" {
  load_balancer_arn = "${aws_lb.ICECONOMICAL_DEV02_INTERNAL_NLB.arn}"
  port = "443"
  protocol = "TCP"

  default_action {
    type = "forward"
    target_group_arn = "${aws_lb_target_group.ICECO_DEV02_INT_NLB_HAPROXY_TG2.arn}"
  }
}

resource "aws_lb_target_group_attachment" "ICECONOMICAL_DEV02_INTERNAL_NLB_TARGET_GROUP_ATTACHMENT_HAPROXY" {
  target_group_arn = "${aws_lb_target_group.ICECO_DEV02_INT_NLB_HAPROXY_TG.arn}"
  target_id = "${aws_instance.ICECONOMICAL_DEV02_INTERNAL_HAPROXY.id}"
  port = 80
}

resource "aws_lb_target_group_attachment" "ICECONOMICAL_DEV02_INTERNAL_NLB_TARGET_GROUP_ATTACHMENT_HAPROXY2" {
  target_group_arn = "${aws_lb_target_group.ICECO_DEV02_INT_NLB_HAPROXY_TG2.arn}"
  target_id = "${aws_instance.ICECONOMICAL_DEV02_INTERNAL_HAPROXY.id}"
  port = 443
}

output "ICECONOMICAL_DEV02_INTERNAL_NLB_DNS_NAME" {
  value = "${aws_lb.ICECONOMICAL_DEV02_INTERNAL_NLB.dns_name}"
}

output "ICECONOMICAL_DEV02_INTERNAL_NLB_ZONE_ID" {
  value = "${aws_lb.ICECONOMICAL_DEV02_INTERNAL_NLB.zone_id}"
}

########## External ###########

# #=======================================================================
# # ST01
# #=======================================================================
##### ALBs #####

##### NLBs ####################
########## Internal ###########
resource "aws_lb" "ICECONOMICAL_ST01_INTERNAL_NLB" {
  name = "ICECONOMICAL-ST01-INTERNAL-NLB"
  internal = true
  load_balancer_type = "network"
  enable_cross_zone_load_balancing = false
  subnets = [
    "${var.economical-dev-private-1a-subnet}",
    "${var.economical-dev-private-1b-subnet}"
  ]

  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "PROJECT"
    Name = "ICECONOMICAL-ST01-INTERNAL-NLB"
  }
}

resource "aws_lb_target_group" "ICECO_ST01_INT_NLB_HAPROXY_TG" {
  name = "ICECO-ST01-HAPROXY-INT-NLB"
  port = 80
  protocol = "TCP"
  vpc_id = "${var.vpc_id}"

  health_check {
    protocol = "TCP"
    port = 8444
  }

  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "MANAGED_SERVICES"
    Name = "ICECO-ST01-HAPROXY-INT-NLB-TG"
  }
}

resource "aws_lb_target_group" "ICECO_ST01_INT_NLB_HAPROXY_TG2" {
  name = "ICECO-ST01-HAPROXY-INT-NLB2"
  port = 443
  protocol = "TCP"
  vpc_id = "${var.vpc_id}"

  health_check {
    protocol = "TCP"
    port = 8444
  }

  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "MANAGED_SERVICES"
    Name = "ICECO-ST01-HAPROXY-INT-NLB-TG2"
  }
}

resource "aws_lb_listener" "ICECONOMICAL_ST01_INTERNAL_NLB_HAPROXY_LISTENER" {
  load_balancer_arn = "${aws_lb.ICECONOMICAL_ST01_INTERNAL_NLB.arn}"
  port = "80"
  protocol = "TCP"

  default_action {
    type = "forward"
    target_group_arn = "${aws_lb_target_group.ICECO_ST01_INT_NLB_HAPROXY_TG.arn}"
  }
}

resource "aws_lb_listener" "ICECONOMICAL_ST01_INTERNAL_NLB_HAPROXY_LISTENER2" {
  load_balancer_arn = "${aws_lb.ICECONOMICAL_ST01_INTERNAL_NLB.arn}"
  port = "443"
  protocol = "TCP"

  default_action {
    type = "forward"
    target_group_arn = "${aws_lb_target_group.ICECO_ST01_INT_NLB_HAPROXY_TG2.arn}"
  }
}

resource "aws_lb_target_group_attachment" "ICECONOMICAL_ST01_INTERNAL_NLB_TARGET_GROUP_ATTACHMENT_HAPROXY" {
  target_group_arn = "${aws_lb_target_group.ICECO_ST01_INT_NLB_HAPROXY_TG.arn}"
  target_id = "${aws_instance.ICECONOMICAL_ST01_INTERNAL_HAPROXY.id}"
  port = 80
}

resource "aws_lb_target_group_attachment" "ICECONOMICAL_ST01_INTERNAL_NLB_TARGET_GROUP_ATTACHMENT_HAPROXY2" {
  target_group_arn = "${aws_lb_target_group.ICECO_ST01_INT_NLB_HAPROXY_TG2.arn}"
  target_id = "${aws_instance.ICECONOMICAL_ST01_INTERNAL_HAPROXY.id}"
  port = 443
}

output "ICECONOMICAL_ST01_INTERNAL_NLB_DNS_NAME" {
  value = "${aws_lb.ICECONOMICAL_ST01_INTERNAL_NLB.dns_name}"
}

output "ICECONOMICAL_ST01_INTERNAL_NLB_ZONE_ID" {
  value = "${aws_lb.ICECONOMICAL_ST01_INTERNAL_NLB.zone_id}"
}


########## External ###########

# #=======================================================================
# # UAT01
# #=======================================================================
##### ALBs #####

##### NLBs ####################
########## Internal ###########
resource "aws_lb" "ICECONOMICAL_UAT01_INTERNAL_NLB" {
  name = "ICECONOMICAL-UAT01-INTERNAL-NLB"
  internal = true
  load_balancer_type = "network"
  enable_cross_zone_load_balancing = false
  subnets = [
    "${var.economical-dev-private-1a-subnet}",
    "${var.economical-dev-private-1b-subnet}"
  ]

  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "PROJECT"
    Name = "ICECONOMICAL-UAT01-INTERNAL-NLB"
  }
}

resource "aws_lb_target_group" "ICECO_UAT01_INT_NLB_HAPROXY_TG" {
  name = "ICECO-UAT01-HAPROXY-INT-NLB"
  port = 80
  protocol = "TCP"
  vpc_id = "${var.vpc_id}"

  health_check {
    protocol = "TCP"
    port = 8444
  }

  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "MANAGED_SERVICES"
    Name = "ICECO-UAT01-HAPROXY-INT-NLB-TG"
  }
}

resource "aws_lb_target_group" "ICECO_UAT01_INT_NLB_HAPROXY_TG2" {
  name = "ICECO-UAT01-HAPROXY-INT-NLB2"
  port = 443
  protocol = "TCP"
  vpc_id = "${var.vpc_id}"

  health_check {
    protocol = "TCP"
    port = 8444
  }

  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "MANAGED_SERVICES"
    Name = "ICECO-UAT01-HAPROXY-INT-NLB-TG2"
  }
}

resource "aws_lb_listener" "ICECONOMICAL_UAT01_INTERNAL_NLB_HAPROXY_LISTENER" {
  load_balancer_arn = "${aws_lb.ICECONOMICAL_UAT01_INTERNAL_NLB.arn}"
  port = "80"
  protocol = "TCP"

  default_action {
    type = "forward"
    target_group_arn = "${aws_lb_target_group.ICECO_UAT01_INT_NLB_HAPROXY_TG.arn}"
  }
}

resource "aws_lb_listener" "ICECONOMICAL_UAT01_INTERNAL_NLB_HAPROXY_LISTENER2" {
  load_balancer_arn = "${aws_lb.ICECONOMICAL_UAT01_INTERNAL_NLB.arn}"
  port = "443"
  protocol = "TCP"

  default_action {
    type = "forward"
    target_group_arn = "${aws_lb_target_group.ICECO_UAT01_INT_NLB_HAPROXY_TG2.arn}"
  }
}

resource "aws_lb_target_group_attachment" "ICECONOMICAL_UAT01_INTERNAL_NLB_TARGET_GROUP_ATTACHMENT_HAPROXY" {
  target_group_arn = "${aws_lb_target_group.ICECO_UAT01_INT_NLB_HAPROXY_TG.arn}"
  target_id = "${aws_instance.ICECONOMICAL_UAT01_INTERNAL_HAPROXY.id}"
  port = 80
}

resource "aws_lb_target_group_attachment" "ICECONOMICAL_UAT01_INTERNAL_NLB_TARGET_GROUP_ATTACHMENT_HAPROXY2" {
  target_group_arn = "${aws_lb_target_group.ICECO_UAT01_INT_NLB_HAPROXY_TG2.arn}"
  target_id = "${aws_instance.ICECONOMICAL_UAT01_INTERNAL_HAPROXY.id}"
  port = 443
}

output "ICECONOMICAL_UAT01_INTERNAL_NLB_DNS_NAME" {
  value = "${aws_lb.ICECONOMICAL_UAT01_INTERNAL_NLB.dns_name}"
}

output "ICECONOMICAL_UAT01_INTERNAL_NLB_ZONE_ID" {
  value = "${aws_lb.ICECONOMICAL_UAT01_INTERNAL_NLB.zone_id}"
}

resource "aws_lb" "ICECONOMICAL_UAT01_RPA_NLB" {
  name = "ICECONOMICAL-UAT01-RPA-NLB"
  internal = true
  load_balancer_type = "network"
  enable_cross_zone_load_balancing = false
  subnets = [
    "${var.economical-dev-private-1a-subnet}",
    "${var.economical-dev-private-1b-subnet}"
  ]

  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "PROJECT"
    Name = "ICECONOMICAL-UAT01-RPA-NLB"
  }
}

resource "aws_lb_target_group" "ICECONOMICAL_UAT01_RPA_NLB_TG" {
  name = "ICECONOMICAL-UAT01-RPA-NLB-TG"
  port = 8181
  protocol = "TCP"
  vpc_id = "${var.vpc_id}"

  health_check {
    protocol = "TCP"
    port = 8181
  }

  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "PROJECT"
    Name = "ICECONOMICAL-UAT01-RPA-NLB-TG"
  }
}

resource "aws_lb_listener" "ICECONOMICAL_UAT01_RPA_NLB_LISTENER" {
  load_balancer_arn = "${aws_lb.ICECONOMICAL_UAT01_RPA_NLB.arn}"
  port = "80"
  protocol = "TCP"

  default_action {
    type = "forward"
    target_group_arn = "${aws_lb_target_group.ICECONOMICAL_UAT01_RPA_NLB_TG.arn}"
  }
}

resource "aws_lb_target_group_attachment" "ICECONOMICAL_UAT01_RPA_NLB_TG_ATTACHMENT_C01" {
  target_group_arn = "${aws_lb_target_group.ICECONOMICAL_UAT01_RPA_NLB_TG.arn}"
  target_id = "${aws_instance.ICECONOMICAL_UAT01_RPA_CLIENT01.id}"
  port = 8181
}

resource "aws_lb_target_group_attachment" "ICECONOMICAL_UAT01_RPA_NLB_TG_ATTACHMENT_C02" {
  target_group_arn = "${aws_lb_target_group.ICECONOMICAL_UAT01_RPA_NLB_TG.arn}"
  target_id = "${aws_instance.ICECONOMICAL_UAT01_RPA_CLIENT02.id}"
  port = 8181
}

output "ICECONOMICAL_UAT01_RPA_NLB_DNS_NAME" {
  value = "${aws_lb.ICECONOMICAL_UAT01_RPA_NLB.dns_name}"
}

output "ICECONOMICAL_UAT01_RPA_NLB_ZONE_ID" {
  value = "${aws_lb.ICECONOMICAL_UAT01_RPA_NLB.zone_id}"
}


########## External ###########


# #=======================================================================
# # PRF01
# #=======================================================================
##### ALBs #####

##### NLBs ####################
########## Internal ###########
resource "aws_lb" "ICECONOMICAL_PRF01_INTERNAL_NLB" {
  name = "ICECONOMICAL-PRF01-INTERNAL-NLB"
  internal = true
  load_balancer_type = "network"
  enable_cross_zone_load_balancing = false
  subnets = [
    "${var.economical-dev-private-1a-subnet}",
    "${var.economical-dev-private-1b-subnet}"
  ]

  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "PROJECT"
    Name = "ICECONOMICAL-PRF01-INTERNAL-NLB"
  }
}

resource "aws_lb_target_group" "ICECO_PRF01_INT_NLB_HAPROXY_TG" {
  name = "ICECO-PRF01-HAPROXY-INT-NLB"
  port = 80
  protocol = "TCP"
  vpc_id = "${var.vpc_id}"

  health_check {
    protocol = "TCP"
    port = 8444
  }

  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "MANAGED_SERVICES"
    Name = "ICECO-PRF01-HAPROXY-INT-NLB-TG"
  }
}

resource "aws_lb_target_group" "ICECO_PRF01_INT_NLB_HAPROXY_TG2" {
  name = "ICECO-PRF01-HAPROXY-INT-NLB2"
  port = 443
  protocol = "TCP"
  vpc_id = "${var.vpc_id}"

  health_check {
    protocol = "TCP"
    port = 8444
  }

  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "MANAGED_SERVICES"
    Name = "ICECO-PRF01-HAPROXY-INT-NLB-TG2"
  }
}

resource "aws_lb_listener" "ICECONOMICAL_PRF01_INTERNAL_NLB_HAPROXY_LISTENER" {
  load_balancer_arn = "${aws_lb.ICECONOMICAL_PRF01_INTERNAL_NLB.arn}"
  port = "80"
  protocol = "TCP"

  default_action {
    type = "forward"
    target_group_arn = "${aws_lb_target_group.ICECO_PRF01_INT_NLB_HAPROXY_TG.arn}"
  }
}

resource "aws_lb_listener" "ICECONOMICAL_PRF01_INTERNAL_NLB_HAPROXY_LISTENER2" {
  load_balancer_arn = "${aws_lb.ICECONOMICAL_PRF01_INTERNAL_NLB.arn}"
  port = "443"
  protocol = "TCP"

  default_action {
    type = "forward"
    target_group_arn = "${aws_lb_target_group.ICECO_PRF01_INT_NLB_HAPROXY_TG2.arn}"
  }
}

resource "aws_lb_target_group_attachment" "ICECONOMICAL_PRF01_INTERNAL_NLB01_TARGET_GROUP_ATTACHMENT_HAPROXY" {
  target_group_arn = "${aws_lb_target_group.ICECO_PRF01_INT_NLB_HAPROXY_TG.arn}"
  target_id = "${aws_instance.ICECONOMICAL_PRF01_INTERNAL_HAPROXY01.id}"
  port = 80
}

resource "aws_lb_target_group_attachment" "ICECONOMICAL_PRF01_INTERNAL_NLB02_TARGET_GROUP_ATTACHMENT_HAPROXY" {
  target_group_arn = "${aws_lb_target_group.ICECO_PRF01_INT_NLB_HAPROXY_TG.arn}"
  target_id = "${aws_instance.ICECONOMICAL_PRF01_INTERNAL_HAPROXY02.id}"
  port = 80
}

resource "aws_lb_target_group_attachment" "ICECONOMICAL_PRF01_INTERNAL_NLB01_TARGET_GROUP_ATTACHMENT_HAPROXY2" {
  target_group_arn = "${aws_lb_target_group.ICECO_PRF01_INT_NLB_HAPROXY_TG2.arn}"
  target_id = "${aws_instance.ICECONOMICAL_PRF01_INTERNAL_HAPROXY01.id}"
  port = 443
}

resource "aws_lb_target_group_attachment" "ICECONOMICAL_PRF01_INTERNAL_NLB02_TARGET_GROUP_ATTACHMENT_HAPROXY2" {
  target_group_arn = "${aws_lb_target_group.ICECO_PRF01_INT_NLB_HAPROXY_TG2.arn}"
  target_id = "${aws_instance.ICECONOMICAL_PRF01_INTERNAL_HAPROXY02.id}"
  port = 443
}

output "ICECONOMICAL_PRF01_INTERNAL_NLB_DNS_NAME" {
  value = "${aws_lb.ICECONOMICAL_PRF01_INTERNAL_NLB.dns_name}"
}

output "ICECONOMICAL_PRF01_INTERNAL_NLB_ZONE_ID" {
  value = "${aws_lb.ICECONOMICAL_PRF01_INTERNAL_NLB.zone_id}"
}

resource "aws_lb" "ICECONOMICAL_PRF01_RPA_NLB" {
  name = "ICECONOMICAL-PRF01-RPA-NLB"
  internal = true
  load_balancer_type = "network"
  enable_cross_zone_load_balancing = false
  subnets = [
    "${var.economical-dev-private-1a-subnet}",
    "${var.economical-dev-private-1b-subnet}"
  ]

  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "PROJECT"
    Name = "ICECONOMICAL-PRF01-RPA-NLB"
  }
}

resource "aws_lb_target_group" "ICECONOMICAL_PRF01_RPA_NLB_TG" {
  name = "ICECONOMICAL-PRF01-RPA-NLB-TG"
  port = 8181
  protocol = "TCP"
  vpc_id = "${var.vpc_id}"

  health_check {
    protocol = "TCP"
    port = 8181
  }

  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "MANAGED_SERVICES"
    Name = "ICECONOMICAL-PRF01-RPA-NLB-TG"
  }
}

resource "aws_lb_listener" "ICECONOMICAL_PRF01_RPA_NLB_LISTENER" {
  load_balancer_arn = "${aws_lb.ICECONOMICAL_PRF01_RPA_NLB.arn}"
  port = "80"
  protocol = "TCP"

  default_action {
    type = "forward"
    target_group_arn = "${aws_lb_target_group.ICECONOMICAL_PRF01_RPA_NLB_TG.arn}"
  }
}

resource "aws_lb_target_group_attachment" "ICECONOMICAL_PRF01_RPA_NLB_TG_ATTACHMENT_C01" {
  target_group_arn = "${aws_lb_target_group.ICECONOMICAL_PRF01_RPA_NLB_TG.arn}"
  target_id = "${aws_instance.ICECONOMICAL_PRF01_RPA_CLIENT01.id}"
  port = 8181
}

resource "aws_lb_target_group_attachment" "ICECONOMICAL_PRF01_RPA_NLB_TG_ATTACHMENT_C02" {
  target_group_arn = "${aws_lb_target_group.ICECONOMICAL_PRF01_RPA_NLB_TG.arn}"
  target_id = "${aws_instance.ICECONOMICAL_PRF01_RPA_CLIENT02.id}"
  port = 8181
}

output "ICECONOMICAL_PRF01_RPA_NLB_DNS_NAME" {
  value = "${aws_lb.ICECONOMICAL_PRF01_RPA_NLB.dns_name}"
}

output "ICECONOMICAL_PRF01_RPA_NLB_ZONE_ID" {
  value = "${aws_lb.ICECONOMICAL_PRF01_RPA_NLB.zone_id}"
}


# #=======================================================================
# # SIT01
# #=======================================================================
##### ALBs #####

##### NLBs ####################
########## Internal ###########
resource "aws_lb" "ICECONOMICAL_SIT01_INTERNAL_NLB" {
  name = "ICECO-SIT01-INTERNAL-NLB"
  internal = true
  load_balancer_type = "network"
  enable_cross_zone_load_balancing = false
  subnets = [
    "${var.economical-dev-private-1a-subnet}",
    "${var.economical-dev-private-1b-subnet}"
  ]

  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "PROJECT"
    Name = "ICECO-SIT01-INTERNAL-NLB"
  }
}

resource "aws_lb_target_group" "ICECO_SIT01_INT_NLB_HAPROXY_TG" {
  name = "ICECO-SIT01-HAPROXY-INT-NLB-TG"
  port = 80
  protocol = "TCP"
  vpc_id = "${var.vpc_id}"

  health_check {
    protocol = "TCP"
    port = 8444
  }

  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "MANAGED_SERVICES"
    Name = "ICECO-SIT01-HAPROXY-INT-NLB-TG"
  }
}

resource "aws_lb_target_group" "ICECO_SIT01_INT_NLB_HAPROXY_TG2" {
  name = "ICECO-SIT01-HAPROXY-INT-NLB2"
  port = 443
  protocol = "TCP"
  vpc_id = "${var.vpc_id}"

  health_check {
    protocol = "TCP"
    port = 8444
  }

  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "MANAGED_SERVICES"
    Name = "ICECO-SIT01-HAPROXY-INT-NLB-TG2"
  }
}

resource "aws_lb_listener" "ICECONOMICAL_SIT01_INTERNAL_NLB_HAPROXY_LISTENER" {
  load_balancer_arn = "${aws_lb.ICECONOMICAL_SIT01_INTERNAL_NLB.arn}"
  port = "80"
  protocol = "TCP"

  default_action {
    type = "forward"
    target_group_arn = "${aws_lb_target_group.ICECO_SIT01_INT_NLB_HAPROXY_TG.arn}"
  }
}

resource "aws_lb_listener" "ICECONOMICAL_SIT01_INTERNAL_NLB_HAPROXY_LISTENER2" {
  load_balancer_arn = "${aws_lb.ICECONOMICAL_SIT01_INTERNAL_NLB.arn}"
  port = "443"
  protocol = "TCP"

  default_action {
    type = "forward"
    target_group_arn = "${aws_lb_target_group.ICECO_SIT01_INT_NLB_HAPROXY_TG2.arn}"
  }
}

resource "aws_lb_target_group_attachment" "ICECONOMICAL_SIT01_INTERNAL_NLB01_TARGET_GROUP_ATTACHMENT_HAPROXY" {
  target_group_arn = "${aws_lb_target_group.ICECO_SIT01_INT_NLB_HAPROXY_TG.arn}"
  target_id = "${aws_instance.ICECONOMICAL_SIT01_INTERNAL_HAPROXY01.id}"
  port = 80
}

resource "aws_lb_target_group_attachment" "ICECONOMICAL_SIT01_INTERNAL_NLB02_TARGET_GROUP_ATTACHMENT_HAPROXY" {
  target_group_arn = "${aws_lb_target_group.ICECO_SIT01_INT_NLB_HAPROXY_TG.arn}"
  target_id = "${aws_instance.ICECONOMICAL_SIT01_INTERNAL_HAPROXY02.id}"
  port = 80
}

resource "aws_lb_target_group_attachment" "ICECONOMICAL_SIT01_INTERNAL_NLB01_TARGET_GROUP_ATTACHMENT_HAPROXY2" {
  target_group_arn = "${aws_lb_target_group.ICECO_SIT01_INT_NLB_HAPROXY_TG2.arn}"
  target_id = "${aws_instance.ICECONOMICAL_SIT01_INTERNAL_HAPROXY01.id}"
  port = 443
}

resource "aws_lb_target_group_attachment" "ICECONOMICAL_SIT01_INTERNAL_NLB02_TARGET_GROUP_ATTACHMENT_HAPROXY2" {
  target_group_arn = "${aws_lb_target_group.ICECO_SIT01_INT_NLB_HAPROXY_TG2.arn}"
  target_id = "${aws_instance.ICECONOMICAL_SIT01_INTERNAL_HAPROXY02.id}"
  port = 443
}

output "ICECONOMICAL_SIT01_INTERNAL_NLB_DNS_NAME" {
  value = "${aws_lb.ICECONOMICAL_SIT01_INTERNAL_NLB.dns_name}"
}

output "ICECONOMICAL_SIT01_INTERNAL_NLB_ZONE_ID" {
  value = "${aws_lb.ICECONOMICAL_SIT01_INTERNAL_NLB.zone_id}"
}

resource "aws_lb" "ICECONOMICAL_SIT01_RPA_NLB" {
  name = "ICECONOMICAL-SIT01-RPA-NLB"
  internal = true
  load_balancer_type = "network"
  enable_cross_zone_load_balancing = false
  subnets = [
    "${var.economical-dev-private-1a-subnet}",
    "${var.economical-dev-private-1b-subnet}"
  ]

  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "PROJECT"
    Name = "ICECONOMICAL-SIT01-RPA-NLB"
  }
}

resource "aws_lb_target_group" "ICECONOMICAL_SIT01_RPA_NLB_TG" {
  name = "ICECONOMICAL-SIT01-RPA-NLB-TG"
  port = 8181
  protocol = "TCP"
  vpc_id = "${var.vpc_id}"

  health_check {
    protocol = "TCP"
    port = 8181
  }

  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "MANAGED_SERVICES"
    Name = "ICECONOMICAL-SIT01-RPA-NLB-TG"
  }
}

resource "aws_lb_listener" "ICECONOMICAL_SIT01_RPA_NLB_LISTENER" {
  load_balancer_arn = "${aws_lb.ICECONOMICAL_SIT01_RPA_NLB.arn}"
  port = "80"
  protocol = "TCP"

  default_action {
    type = "forward"
    target_group_arn = "${aws_lb_target_group.ICECONOMICAL_SIT01_RPA_NLB_TG.arn}"
  }
}

resource "aws_lb_target_group_attachment" "ICECONOMICAL_SIT01_RPA_NLB_TG_ATTACHMENT_C01" {
  target_group_arn = "${aws_lb_target_group.ICECONOMICAL_SIT01_RPA_NLB_TG.arn}"
  target_id = "${aws_instance.ICECONOMICAL_SIT01_RPA_CLIENT01.id}"
  port = 8181
}

resource "aws_lb_target_group_attachment" "ICECONOMICAL_SIT01_RPA_NLB_TG_ATTACHMENT_C02" {
  target_group_arn = "${aws_lb_target_group.ICECONOMICAL_SIT01_RPA_NLB_TG.arn}"
  target_id = "${aws_instance.ICECONOMICAL_SIT01_RPA_CLIENT02.id}"
  port = 8181
}

output "ICECONOMICAL_SIT01_RPA_NLB_DNS_NAME" {
  value = "${aws_lb.ICECONOMICAL_SIT01_RPA_NLB.dns_name}"
}

output "ICECONOMICAL_SIT01_RPA_NLB_ZONE_ID" {
  value = "${aws_lb.ICECONOMICAL_SIT01_RPA_NLB.zone_id}"
}


# #=======================================================================
# # PPS01
# #=======================================================================
##### ALBs #####

##### NLBs ####################
########## Internal ###########
resource "aws_lb" "ICECONOMICAL_PPS01_INTERNAL_NLB" {
  name = "ICECO-PPS01-INTERNAL-NLB"
  internal = true
  load_balancer_type = "network"
  enable_cross_zone_load_balancing = false
  subnets = [
    "${var.economical-dev-private-1a-subnet}",
    "${var.economical-dev-private-1b-subnet}"
  ]

  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "MANAGED_SERVICES"
    Name = "ICECO-PPS01-INTERNAL-NLB"
  }
}

resource "aws_lb_target_group" "ICECO_PPS01_INT_NLB_HAPROXY_TG" {
  name = "ICECO-PPS01-HAPROXY-INT-NLB-TG"
  port = 80
  protocol = "TCP"
  vpc_id = "${var.vpc_id}"

  health_check {
    protocol = "TCP"
    port = 8444
  }

  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "MANAGED_SERVICES"
    Name = "ICECO-PPS01-HAPROXY-INT-NLB-TG"
  }
}

resource "aws_lb_target_group" "ICECO_PPS01_INT_NLB_HAPROXY_TG2" {
  name = "ICECO-PPS01-HAPROXY-INT-NLB2"
  port = 443
  protocol = "TCP"
  vpc_id = "${var.vpc_id}"

  health_check {
    protocol = "TCP"
    port = 8444
  }

  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "MANAGED_SERVICES"
    Name = "ICECO-PPS01-HAPROXY-INT-NLB-TG2"
  }
}

resource "aws_lb_listener" "ICECONOMICAL_PPS01_INTERNAL_NLB_HAPROXY_LISTENER" {
  load_balancer_arn = "${aws_lb.ICECONOMICAL_PPS01_INTERNAL_NLB.arn}"
  port = "80"
  protocol = "TCP"

  default_action {
    type = "forward"
    target_group_arn = "${aws_lb_target_group.ICECO_PPS01_INT_NLB_HAPROXY_TG.arn}"
  }
}

resource "aws_lb_listener" "ICECONOMICAL_PPS01_INTERNAL_NLB_HAPROXY_LISTENER2" {
  load_balancer_arn = "${aws_lb.ICECONOMICAL_PPS01_INTERNAL_NLB.arn}"
  port = "443"
  protocol = "TCP"

  default_action {
    type = "forward"
    target_group_arn = "${aws_lb_target_group.ICECO_PPS01_INT_NLB_HAPROXY_TG2.arn}"
  }
}

resource "aws_lb_target_group_attachment" "ICECONOMICAL_PPS01_INTERNAL_NLB01_TARGET_GROUP_ATTACHMENT_HAPROXY" {
  target_group_arn = "${aws_lb_target_group.ICECO_PPS01_INT_NLB_HAPROXY_TG.arn}"
  target_id = "${aws_instance.ICECONOMICAL_PPS01_INTERNAL_HAPROXY01.id}"
  port = 80
}

resource "aws_lb_target_group_attachment" "ICECONOMICAL_PPS01_INTERNAL_NLB02_TARGET_GROUP_ATTACHMENT_HAPROXY" {
  target_group_arn = "${aws_lb_target_group.ICECO_PPS01_INT_NLB_HAPROXY_TG.arn}"
  target_id = "${aws_instance.ICECONOMICAL_PPS01_INTERNAL_HAPROXY02.id}"
  port = 80
}

resource "aws_lb_target_group_attachment" "ICECONOMICAL_PPS01_INTERNAL_NLB01_TARGET_GROUP_ATTACHMENT_HAPROXY2" {
  target_group_arn = "${aws_lb_target_group.ICECO_PPS01_INT_NLB_HAPROXY_TG2.arn}"
  target_id = "${aws_instance.ICECONOMICAL_PPS01_INTERNAL_HAPROXY01.id}"
  port = 443
}

resource "aws_lb_target_group_attachment" "ICECONOMICAL_PPS01_INTERNAL_NLB02_TARGET_GROUP_ATTACHMENT_HAPROXY2" {
  target_group_arn = "${aws_lb_target_group.ICECO_PPS01_INT_NLB_HAPROXY_TG2.arn}"
  target_id = "${aws_instance.ICECONOMICAL_PPS01_INTERNAL_HAPROXY02.id}"
  port = 443
}

output "ICECONOMICAL_PPS01_INTERNAL_NLB_DNS_NAME" {
  value = "${aws_lb.ICECONOMICAL_PPS01_INTERNAL_NLB.dns_name}"
}

output "ICECONOMICAL_PPS01_INTERNAL_NLB_ZONE_ID" {
  value = "${aws_lb.ICECONOMICAL_PPS01_INTERNAL_NLB.zone_id}"
}

resource "aws_lb" "ICECONOMICAL_PPS01_RPA_NLB" {
  name = "ICECONOMICAL-PPS01-RPA-NLB"
  internal = true
  load_balancer_type = "network"
  enable_cross_zone_load_balancing = false
  subnets = [
    "${var.economical-dev-private-1a-subnet}",
    "${var.economical-dev-private-1b-subnet}"
  ]

  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "MANAGED_SERVICES"
    Name = "ICECONOMICAL-PPS01-RPA-NLB"
  }
}

resource "aws_lb_target_group" "ICECONOMICAL_PPS01_RPA_NLB_TG" {
  name = "ICECONOMICAL-PPS01-RPA-NLB-TG"
  port = 8181
  protocol = "TCP"
  vpc_id = "${var.vpc_id}"

  health_check {
    protocol = "TCP"
    port = 8181
  }

  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "MANAGED_SERVICES"
    Name = "ICECONOMICAL-PPS01-RPA-NLB-TG"
  }
}

resource "aws_lb_listener" "ICECONOMICAL_PPS01_RPA_NLB_LISTENER" {
  load_balancer_arn = "${aws_lb.ICECONOMICAL_PPS01_RPA_NLB.arn}"
  port = "80"
  protocol = "TCP"

  default_action {
    type = "forward"
    target_group_arn = "${aws_lb_target_group.ICECONOMICAL_PPS01_RPA_NLB_TG.arn}"
  }
}

resource "aws_lb_target_group_attachment" "ICECONOMICAL_PPS01_RPA_NLB_TG_ATTACHMENT_C01" {
  target_group_arn = "${aws_lb_target_group.ICECONOMICAL_PPS01_RPA_NLB_TG.arn}"
  target_id = "${aws_instance.ICECONOMICAL_PPS01_RPA_CLIENT01.id}"
  port = 8181
}

resource "aws_lb_target_group_attachment" "ICECONOMICAL_PPS01_RPA_NLB_TG_ATTACHMENT_C02" {
  target_group_arn = "${aws_lb_target_group.ICECONOMICAL_PPS01_RPA_NLB_TG.arn}"
  target_id = "${aws_instance.ICECONOMICAL_PPS01_RPA_CLIENT02.id}"
  port = 8181
}

output "ICECONOMICAL_PPS01_RPA_NLB_DNS_NAME" {
  value = "${aws_lb.ICECONOMICAL_PPS01_RPA_NLB.dns_name}"
}

output "ICECONOMICAL_PPS01_RPA_NLB_ZONE_ID" {
  value = "${aws_lb.ICECONOMICAL_PPS01_RPA_NLB.zone_id}"
}
