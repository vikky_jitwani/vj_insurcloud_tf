set -e

. /tmp/functions.sh

#Set environment vars
export DOMAIN_NAME="$APP.$(echo $ENV | tr '[:upper:]' '[:lower:]').insurcloud.ca"
export CERT_AUHTORITY_ARN="arn:aws:acm-pca:ca-central-1:020266858005:certificate-authority/49bebe34-3c02-4afb-9569-42e45da555c2"
export VALID_DAYS="760"
export CA_PATH="/tmp/ca.crt.pem"
export CERT_PATH="/tmp/$APP.crt.pem"
export KEY_PATH="/tmp/$APP.key.pem"


#Set local vars
csr=cert.csr.pem
issued_cert=result.json

#Update CN for CSR
sed -i "s|{cn_name}|$DOMAIN_NAME|g" /tmp/default.cnf

#Generate key and csr
generate-rsa-key $KEY_PATH
generate-csr /tmp/default.cnf $KEY_PATH $csr

#Sign csr and create certificate files
certificate_arn=$(issue-certificate $csr $VALID_DAYS)
sleep 60
get-certificate $certificate_arn $issued_cert

jq -r '.Certificate' $issued_cert > $CERT_PATH
jq -r '.CertificateChain' $issued_cert > $CA_PATH