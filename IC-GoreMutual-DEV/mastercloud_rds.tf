#### MICROSERVICES DB #####
resource "aws_db_subnet_group" "MYSQL_DB_SUBNET" {
    name        = "mysql-db-subnet"
    description = "RDS subnet group"
    subnet_ids  = ["${var.gore-dev-database-1a-subnet}", "${var.gore-dev-database-1b-subnet}"]
}

resource "aws_db_parameter_group" "microservices" {
  name   = "gore-microservices"
  family = "mysql8.0"

  parameter {
    name  = "time_zone"
    value = "US/Eastern" # MySQL has no America/Toronto time zone
  }

  parameter {
    name = "log_output"
    value = "FILE"
  }

  parameter {
    name = "general_log"
    value = "1"
  }

  parameter {
    name = "slow_query_log"
    value = "1"
  }

  parameter {
    name = "long_query_time"
    value = "2"
  }
}

#PPS not needed yet
resource "aws_db_instance" "PERF_microservicesmysqldb" {

    engine            = "mysql"
    engine_version    = "8.0.17"
    instance_class    = "db.t3.medium"
    allocated_storage = 100
    storage_encrypted = true
    identifier        = "goreperfmicroservices"
    name              = "goreperfmicroservices"
    username          = "sa"
    password          = "${var.mysql-rds-password}" # password
    port              = 3306

    maintenance_window = "Mon:00:00-Mon:03:00"
    backup_window      = "03:00-06:01"
    deletion_protection = true
    enabled_cloudwatch_logs_exports = ["general", "error", "slowquery"]
    apply_immediately  = true

    db_subnet_group_name  = "${aws_db_subnet_group.MYSQL_DB_SUBNET.name}"
    parameter_group_name  = "${aws_db_parameter_group.microservices.id}"
    multi_az              = true # set to true to have high availability: 2 instances synchronized with each other
    vpc_security_group_ids  = ["${aws_security_group.SG_MYSQL_DB.id}"]
    storage_type            = "gp2"
    backup_retention_period = 30 # how long you’re going to keep your backups
    kms_key_id = "arn:aws:kms:ca-central-1:780193124257:key/bd1b3c2f-e600-4524-a251-4b6d36675659"

    tags = {
        Name = "gore-perf-microservices"
        Environment = "PERF"
    }
}

#### OPENTEXT Content Server DB #####
# DEV Content Server Database
# #Dont need for now.
# resource "aws_db_subnet_group" "MSSQL_DB_SUBNET" {
#     name        = "mssql-db-subnet"
#     description = "RDS subnet group"
#     subnet_ids  = ["${var.gore-dev-database-1a-subnet}", "${var.gore-dev-database-1b-subnet}"]
# }

# resource "aws_db_instance" "ICGORE-DEV-OTCS-MSSQL-DB" {

#     license_model     = "license-included"
#     engine            = "sqlserver-web"
#     engine_version    = "14.00.3281.6.v1"
#     instance_class    = "db.r5.large"
#     allocated_storage   = 100
#     storage_encrypted   = true
#     identifier          = "goredev-mssql-otcs"
#     username            = "sa"
#     password            = "${var.mssql-rds-password}" # password
#     port                = 1433
#     timezone            = "Eastern Standard Time"

#     maintenance_window  = "Mon:00:00-Mon:03:00"
#     backup_window       = "03:00-06:00"
#     deletion_protection = true

#     db_subnet_group_name  = "${aws_db_subnet_group.MSSQL_DB_SUBNET.name}"
#     parameter_group_name  = "default.sqlserver-web-14.0"
#     multi_az              = false # set to true to have high availability: 2 instances synchronized with each other
#     vpc_security_group_ids  = ["${aws_security_group.SG_MSSQL_DB.id}"]
#     storage_type            = "gp2"
#     backup_retention_period = 7 # how long you’re going to keep your backups
#     kms_key_id = "arn:aws:kms:ca-central-1:780193124257:key/bd1b3c2f-e600-4524-a251-4b6d36675659"

#     tags = {
#         Name = "gore-dev01-otcs-db"
#         Environment = "Dev"
#     }
# }

