##### ALBs #####

##### NLBs ####################
########## Internal ###########
resource "aws_lb" "ICGORE_PROD_INTERNAL_NLB" {
    name = "ICGORE-PROD-INTERNAL-NLB"
    internal           = true
    load_balancer_type = "network"
    enable_cross_zone_load_balancing = false
    subnets            = [
        "${var.peel-prod-private-1a-subnet}",
        "${var.peel-prod-private-1b-subnet}"
    ]

    tags = {
        Name = "ICGORE-PROD-INTERNAL-NLB"
    }
}

resource "aws_lb_target_group" "ICGORE_PROD_INTERNAL_NLB_HAPROXY_TARGET_GROUP" {
    name     = "ICGORE-PROD-HAPROXY-INTERNAL-NLB"
    port     = 443
    protocol = "TCP"
    vpc_id   = "${var.vpc_id}"

    health_check {
        protocol = "TCP"
        port = 8444
    }

    tags = {
        Name = "ICGORE-PROD-HAPROXY-INTERNAL-NLB-TARGET-GROUP"
    }
}

resource "aws_lb_listener" "ICGORE_PROD_INTERNAL_NLB_HAPROXY_LISTENER" {
    load_balancer_arn = "${aws_lb.ICGORE_PROD_INTERNAL_NLB.arn}"
    port              = "443"
    protocol          = "TCP"

    default_action {
        type             = "forward"
        target_group_arn = "${aws_lb_target_group.ICGORE_PROD_INTERNAL_NLB_HAPROXY_TARGET_GROUP.arn}"
    }
}

resource "aws_lb_target_group_attachment" "ICGORE_PROD_INTERNAL_NLB_TARGET_GROUP_ATTACHMENT_HAPROXY_1" {
    target_group_arn = "${aws_lb_target_group.ICGORE_PROD_INTERNAL_NLB_HAPROXY_TARGET_GROUP.arn}"
    target_id        = "${aws_instance.ICGORE_PROD_INTERNAL_HAPROXY_1.id}"
    port             = 443
}

resource "aws_lb_target_group_attachment" "ICGORE_PROD_INTERNAL_NLB_TARGET_GROUP_ATTACHMENT_HAPROXY_2" {
    target_group_arn = "${aws_lb_target_group.ICGORE_PROD_INTERNAL_NLB_HAPROXY_TARGET_GROUP.arn}"
    target_id        = "${aws_instance.ICGORE_PROD_INTERNAL_HAPROXY_2.id}"
    port             = 443
}

output "ICGORE_PROD_INTERNAL_NLB_DNS_NAME" {
    value = "${aws_lb.ICGORE_PROD_INTERNAL_NLB.dns_name}"
}

output "ICGORE_PROD_INTERNAL_NLB_ZONE_ID" {
    value = "${aws_lb.ICGORE_PROD_INTERNAL_NLB.zone_id}"
}

########## External ###########
resource "aws_lb" "ICGORE_PROD_EXTERNAL_NLB" {
    name               = "ICGORE-PROD-EXTERNAL-NLB"
    internal           = true
    load_balancer_type = "network"
    enable_cross_zone_load_balancing = false
    subnets            = [
        "${var.peel-prod-private-1a-subnet}",
        "${var.peel-prod-private-1b-subnet}"
    ] # Changing this value will force a recreation of the resource

    tags = {
        Name = "ICGORE-PROD-EXTERNAL-NLB"
    }
}

output "ICGORE_PROD_EXTERNAL_NLB_DNS_NAME" {
  value = "${aws_lb.ICGORE_PROD_EXTERNAL_NLB.dns_name}"
}

output "ICGORE_PROD_EXTERNAL_NLB_ZONE_ID" {
  value = "${aws_lb.ICGORE_PROD_EXTERNAL_NLB.zone_id}"
}

resource "aws_lb_target_group" "ICGORE_PROD_EXTERNAL_NLB_HAPROXY_TARGET_GROUP" {
    name     = "ICGORE-PROD-HAPROXY-EXTERNAL-NLB"
    port     = 443
    protocol = "TCP"
    vpc_id   = "${var.vpc_id}"

    health_check {
        protocol = "TCP"
        port = 8444
    }

    tags = {
        Name = "ICGORE-PROD-EXTERNAL-NLB-HAPROXY-TARGET-GROUP"
    }
}

resource "aws_lb_listener" "ICGORE_PROD_EXTERNAL_NLB_HAPROXY_LISTENER" {
    load_balancer_arn = "${aws_lb.ICGORE_PROD_EXTERNAL_NLB.arn}"
    port              = "443"
    protocol          = "TCP"

    default_action {
        type             = "forward"
        target_group_arn = "${aws_lb_target_group.ICGORE_PROD_EXTERNAL_NLB_HAPROXY_TARGET_GROUP.arn}"
    }
}

resource "aws_lb_target_group_attachment" "ICGORE_PROD_EXTERNAL_NLB_TARGET_GROUP_ATTACHMENT_HAPROXY_1" {
    target_group_arn = "${aws_lb_target_group.ICGORE_PROD_EXTERNAL_NLB_HAPROXY_TARGET_GROUP.arn}"
    target_id        = "${aws_instance.ICGORE_PROD_EXTERNAL_HAPROXY_1.id}"
    port             = 443
}

resource "aws_lb_target_group_attachment" "ICGORE_PROD_EXTERNAL_NLB_TARGET_GROUP_ATTACHMENT_HAPROXY_2" {
    target_group_arn = "${aws_lb_target_group.ICGORE_PROD_EXTERNAL_NLB_HAPROXY_TARGET_GROUP.arn}"
    target_id        = "${aws_instance.ICGORE_PROD_EXTERNAL_HAPROXY_2.id}"
    port             = 443
}
