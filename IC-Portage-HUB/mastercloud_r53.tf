// #=======================================================================
// # HUB 
// #=======================================================================
resource "aws_route53_record" "ICPORTAGE_NEXUS_REG_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "nexus.portage.insurcloud.ca"
    type    = "A"

    alias {
        name                   = "internal-cicd-internal-alb-2023433918.ca-central-1.elb.amazonaws.com"
        zone_id                = "ZQSVJUPU6J1EY"
        evaluate_target_health = false
    }
}

resource "aws_route53_record" "ICPORTAGE_NEXUS_PROD_REG_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "nexus-prod.portage.insurcloud.ca"
    type    = "A"

    alias {
        name                   = "internal-cicd-internal-alb-2023433918.ca-central-1.elb.amazonaws.com"
        zone_id                = "ZQSVJUPU6J1EY"
        evaluate_target_health = false
    }
}

resource "aws_route53_record" "ICPORTAGE_NEXUS_UI_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "nexus-ui.portage.insurcloud.ca"
    type    = "A"

    alias {
        name                   = "internal-cicd-internal-alb-2023433918.ca-central-1.elb.amazonaws.com"
        zone_id                = "ZQSVJUPU6J1EY"
        evaluate_target_health = false
    }
}

resource "aws_route53_record" "ICPORTAGE_SONARQUBE_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "sonarqube.portage.insurcloud.ca"
    type    = "A"

    alias {
        name                   = "internal-cicd-internal-alb-2023433918.ca-central-1.elb.amazonaws.com"
        zone_id                = "ZQSVJUPU6J1EY"
        evaluate_target_health = false
    }
}

resource "aws_route53_record" "ICPORTAGE_CONSUL_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "consul.portage.insurcloud.ca"
    type    = "A"

    alias {
        name                   = "internal-cicd-internal-alb-2023433918.ca-central-1.elb.amazonaws.com"
        zone_id                = "ZQSVJUPU6J1EY"
        evaluate_target_health = false
    }
}

resource "aws_route53_record" "ICPORTAGE_JENKINS_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "jenkins.portage.insurcloud.ca"
    type    = "A"

    alias {
        name                   = "internal-cicd-internal-alb-2023433918.ca-central-1.elb.amazonaws.com"
        zone_id                = "ZQSVJUPU6J1EY"
        evaluate_target_health = false
    }
}

resource "aws_route53_record" "ICPORTAGE_VAULT_PPS_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "vault.pps.portage.insurcloud.ca"
    type    = "A"

    alias {
        name                   = "a-mc-cac-d-a005-ic-pps-vault-84458cafb01e039e.elb.ca-central-1.amazonaws.com"
        zone_id                = "Z2EPGBW3API2WT"
        evaluate_target_health = false
    }
}

resource "aws_route53_record" "ICPORTAGE_VAULT_PROD_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "vault.prod.portage.insurcloud.ca"
    type    = "A"

    alias {
        name                   = "a-mc-cac-d-a005-ic-prod-vault-7b0fa62bf2511ffc.elb.ca-central-1.amazonaws.com"
        zone_id                = "Z2EPGBW3API2WT"
        evaluate_target_health = false
    }
}

# #=======================================================================
# # DEV01
# #=======================================================================

######### DATA ##############
resource "aws_route53_record" "ICPORTAGE_DEV01_DHIC_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "dhic.dev01.portage.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICPORTAGE_DEV01_DHIC_SERVER_PRIVATEIP}"]
}

resource "aws_route53_record" "ICPORTAGE_DEV01_DHIC_DB_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "dhicdb.dev01.portage.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICPORTAGE_DEV01_DHIC_MSSQL_DATABASE_PRIVATEIP}"]
}

resource "aws_route53_record" "ICPORTAGE_DEV01_GW_SSIS_MSSQL_DATABASE_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "ssisdb.dev01.portage.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICPORTAGE_DEV01_DHIC_MSSQL_DATABASE_PRIVATEIP}"]
}

resource "aws_route53_record" "ICPORTAGE_DEV01_QLIK_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "qlik.dev01.portage.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICPORTAGE_DEV01_DHIC_MSSQL_DATABASE_PRIVATEIP}"]
}

######### GUIDEWIRE ##############
resource "aws_route53_record" "ICPORTAGE_DEV01_GUIDEWIRE_SUITE_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "gw1.dev01.portage.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICPORTAGE_DEV01_GUIDEWIRE_SUITE_PRIVATEIP}"]
}

resource "aws_route53_record" "ICPORTAGE_DEV01_GUIDEWIRE_DB_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "gwdb.dev01.portage.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICPORTAGE_DEV01_GUIDEWIRE_SUITE_PRIVATEIP}"]
}

resource "aws_route53_record" "ICPORTAGE_DEV01_INTERNAL_NLB_GUIDEWIRE_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "gw.dev01.portage.insurcloud.ca"
    type    = "A"
    alias {
        name                   = "${data.terraform_remote_state.dev_remote_state.ICPORTAGE_DEV01_INTERNAL_NLB_DNS_NAME}"
        zone_id                = "${data.terraform_remote_state.dev_remote_state.ICPORTAGE_DEV01_INTERNAL_NLB_ZONE_ID}"
        evaluate_target_health = false
    }
}

######### MICROSERVICES ###########
resource "aws_route53_record" "ICPORTAGE_DEV01_MICROSERVICES_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "ms1.dev01.portage.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICPORTAGE_DEV01_MICROSERVICES_PRIVATEIP}"]
}

resource "aws_route53_record" "ICPORTAGE_DEV01_MICROSERVICES_DB_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "msdb.dev01.portage.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICPORTAGE_DEV01_MICROSERVICES_PRIVATEIP}"]
}

resource "aws_route53_record" "ICPORTAGE_DEV01_ACS_APP_DB_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "msdb.dev01_app.portage.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICPORTAGE_DEV01_MICROSERVICES_PRIVATEIP}"]
}

resource "aws_route53_record" "ICPORTAGE_DEV01_INTERNAL_NLB_MICROSERVICES_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "ms.dev01.portage.insurcloud.ca"
    type    = "A"
    alias {
        name                   = "${data.terraform_remote_state.dev_remote_state.ICPORTAGE_DEV01_INTERNAL_NLB_DNS_NAME}"
        zone_id                = "${data.terraform_remote_state.dev_remote_state.ICPORTAGE_DEV01_INTERNAL_NLB_ZONE_ID}"
        evaluate_target_health = false
    }
}

# resource "aws_route53_record" "ICPORTAGE_DEV01_WIREMOCK_RECORD" {
#     zone_id = "${var.zone_id}"
#     name    = "mockdata.dev01.portage.insurcloud.ca"
#     ttl = "300"
#     type    = "A"
#     records = ["${data.terraform_remote_state.dev_remote_state.ICPORTAGE_DEV01_MICROSERVICES_PRIVATEIP}"]
# }

######### HAPROXY ##############
resource "aws_route53_record" "ICPORTAGE_DEV01_INTERNAL_HAPROXY_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "inthap.dev01.portage.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICPORTAGE_DEV01_INTERNAL_HAPROXY_PRIVATEIP}"]
}

# #=======================================================================
# # DEV02
# #=======================================================================

######### GUIDEWIRE ##############
resource "aws_route53_record" "ICPORTAGE_DEV02_GUIDEWIRE_SUITE_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "gw1.dev02.portage.insurcloud.ca"
    ttl = "300"
    type    = "A"
    # records = ["${data.terraform_remote_state.dev_remote_state.ICPORTAGE_DEV02_GUIDEWIRE_SUITE_PRIVATEIP}"]
    records = ["10.171.8.131"]
}

resource "aws_route53_record" "ICPORTAGE_DEV02_GUIDEWIRE_DB_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "gwdb.dev02.portage.insurcloud.ca"
    ttl = "300"
    type    = "A"
    # records = ["${data.terraform_remote_state.dev_remote_state.ICPORTAGE_DEV02_GUIDEWIRE_SUITE_PRIVATEIP}"]
    records = ["10.171.8.131"]
}

resource "aws_route53_record" "ICPORTAGE_DEV02_INTERNAL_NLB_GUIDEWIRE_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "gw.dev02.portage.insurcloud.ca"
    type    = "A"
    alias {
        name                   = "${data.terraform_remote_state.dev_remote_state.ICPORTAGE_DEV01_INTERNAL_NLB_DNS_NAME}"
        zone_id                = "${data.terraform_remote_state.dev_remote_state.ICPORTAGE_DEV01_INTERNAL_NLB_ZONE_ID}"
        evaluate_target_health = false
    }
}

######### MICROSERVICES ###########
resource "aws_route53_record" "ICPORTAGE_DEV02_MICROSERVICES_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "ms1.dev02.portage.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICPORTAGE_DEV02_MICROSERVICES_PRIVATEIP}"]
}

resource "aws_route53_record" "ICPORTAGE_DEV02_MICROSERVICES_DB_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "msdb.dev02.portage.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICPORTAGE_DEV02_MICROSERVICES_PRIVATEIP}"]
}

resource "aws_route53_record" "ICPORTAGE_DEV02_INTERNAL_NLB_MICROSERVICES_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "ms.dev02.portage.insurcloud.ca"
    type    = "A"
    alias {
        name                   = "${data.terraform_remote_state.dev_remote_state.ICPORTAGE_DEV01_INTERNAL_NLB_DNS_NAME}"
        zone_id                = "${data.terraform_remote_state.dev_remote_state.ICPORTAGE_DEV01_INTERNAL_NLB_ZONE_ID}"
        evaluate_target_health = false
    }
}

# #=======================================================================
# # DEV03
# #=======================================================================

######### GUIDEWIRE ##############
resource "aws_route53_record" "ICPORTAGE_DEV03_GUIDEWIRE_SUITE_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "gw1.dev03.portage.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICPORTAGE_DEV03_GUIDEWIRE_SUITE_PRIVATEIP}"]
}

resource "aws_route53_record" "ICPORTAGE_DEV03_GUIDEWIRE_DB_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "gwdb.dev03.portage.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICPORTAGE_DEV03_GUIDEWIRE_SUITE_PRIVATEIP}"]
}

resource "aws_route53_record" "ICPORTAGE_DEV03_INTERNAL_NLB_GUIDEWIRE_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "gw.dev03.portage.insurcloud.ca"
    type    = "A"
    alias {
        name                   = "${data.terraform_remote_state.dev_remote_state.ICPORTAGE_DEV01_INTERNAL_NLB_DNS_NAME}"
        zone_id                = "${data.terraform_remote_state.dev_remote_state.ICPORTAGE_DEV01_INTERNAL_NLB_ZONE_ID}"
        evaluate_target_health = false
    }
}

# #=======================================================================
# # QA01
# #=======================================================================

######### GUIDEWIRE ##############
resource "aws_route53_record" "ICPORTAGE_QA01_GUIDEWIRE_SUITE_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "gw1.qa01.portage.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICPORTAGE_QA01_GUIDEWIRE_SUITE_PRIVATEIP}"]
}

resource "aws_route53_record" "ICPORTAGE_QA01_GUIDEWIRE_DB_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "gwdb.qa01.portage.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICPORTAGE_QA01_GUIDEWIRE_SUITE_PRIVATEIP}"]
}

resource "aws_route53_record" "ICPORTAGE_QA01_INTERNAL_NLB_GUIDEWIRE_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "gw.qa01.portage.insurcloud.ca"
    type    = "A"
    alias {
        name                   = "${data.terraform_remote_state.dev_remote_state.ICPORTAGE_DEV01_INTERNAL_NLB_DNS_NAME}"
        zone_id                = "${data.terraform_remote_state.dev_remote_state.ICPORTAGE_DEV01_INTERNAL_NLB_ZONE_ID}"
        evaluate_target_health = false
    }
}

######### MICROSERVICES ###########
resource "aws_route53_record" "ICPORTAGE_QA01_MICROSERVICES_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "ms1.qa01.portage.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICPORTAGE_QA01_MICROSERVICES_PRIVATEIP}"]
}

resource "aws_route53_record" "ICPORTAGE_QA01_MICROSERVICES_DB_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "msdb.qa01.portage.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICPORTAGE_QA01_MICROSERVICES_PRIVATEIP}"]
}

resource "aws_route53_record" "ICPORTAGE_QA01_INTERNAL_NLB_MICROSERVICES_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "ms.qa01.portage.insurcloud.ca"
    type    = "A"
    alias {
        name                   = "${data.terraform_remote_state.dev_remote_state.ICPORTAGE_DEV01_INTERNAL_NLB_DNS_NAME}"
        zone_id                = "${data.terraform_remote_state.dev_remote_state.ICPORTAGE_DEV01_INTERNAL_NLB_ZONE_ID}"
        evaluate_target_health = false
    }
}

# #=======================================================================
# # CNV01
# #=======================================================================

######### DATA ##############
# resource "aws_route53_record" "ICPORTAGE_CNV01_DHIC_RECORD" {
#     zone_id = "${var.zone_id}"
#     name    = "dhic.cnv01.portage.insurcloud.ca"
#     ttl = "300"
#     type    = "A"
#     records = ["${data.terraform_remote_state.dev_remote_state.ICPORTAGE_CNV01_DHIC_SERVER_PRIVATEIP}"]
# }

# resource "aws_route53_record" "ICPORTAGE_CNV01_DHIC_DB_RECORD" {
#     zone_id = "${var.zone_id}"
#     name    = "dhicdb.cnv01.portage.insurcloud.ca"
#     ttl = "300"
#     type    = "A"
#     records = ["${data.terraform_remote_state.dev_remote_state.ICPORTAGE_CNV01_DHIC_MSSQL_DATABASE_PRIVATEIP}"]
# }

# resource "aws_route53_record" "ICPORTAGE_CNV01_GW_SSIS_MSSQL_DATABASE_RECORD" {
#     zone_id = "${var.zone_id}"
#     name    = "ssisdb.cnv01.portage.insurcloud.ca"
#     ttl = "300"
#     type    = "A"
#     records = ["${data.terraform_remote_state.dev_remote_state.ICPORTAGE_CNV01_DHIC_MSSQL_DATABASE_PRIVATEIP}"]
# }

# resource "aws_route53_record" "ICPORTAGE_CNV01_QLIK_SERVER" {
#     zone_id = "${var.zone_id}"
#     name    = "qlik.cnv01.portage.insurcloud.ca	"
#     ttl = "300"
#     type    = "A"
#     records = ["${data.terraform_remote_state.dev_remote_state.ICPORTAGE_CNV01_DHIC_MSSQL_DATABASE_PRIVATEIP}"]
# }

######### GUIDEWIRE ##############
resource "aws_route53_record" "ICPORTAGE_CNV01_GUIDEWIRE_SUITE_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "gw1.cnv01.portage.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICPORTAGE_CNV01_GUIDEWIRE_SUITE_PRIVATEIP}"]
}

resource "aws_route53_record" "ICPORTAGE_CNV01_GUIDEWIRE_DB_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "gwdb.cnv01.portage.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICPORTAGE_CNV01_GUIDEWIRE_SUITE_PRIVATEIP}"]
}

resource "aws_route53_record" "ICPORTAGE_CNV01_INTERNAL_NLB_GUIDEWIRE_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "gw.cnv01.portage.insurcloud.ca"
    type    = "A"
    alias {
        name                   = "${data.terraform_remote_state.dev_remote_state.ICPORTAGE_DEV01_INTERNAL_NLB_DNS_NAME}"
        zone_id                = "${data.terraform_remote_state.dev_remote_state.ICPORTAGE_DEV01_INTERNAL_NLB_ZONE_ID}"
        evaluate_target_health = false
    }
}

######### MICROSERVICES ###########
resource "aws_route53_record" "ICPORTAGE_CNV01_MICROSERVICES_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "ms1.cnv01.portage.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICPORTAGE_CNV01_MICROSERVICES_PRIVATEIP}"]
}

resource "aws_route53_record" "ICPORTAGE_CNV01_MICROSERVICES_DB_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "msdb.cnv01.portage.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICPORTAGE_CNV01_MICROSERVICES_PRIVATEIP}"]
}

resource "aws_route53_record" "ICPORTAGE_CNV01_INTERNAL_NLB_MICROSERVICES_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "ms.cnv01.portage.insurcloud.ca"
    type    = "A"
    alias {
        name                   = "${data.terraform_remote_state.dev_remote_state.ICPORTAGE_DEV01_INTERNAL_NLB_DNS_NAME}"
        zone_id                = "${data.terraform_remote_state.dev_remote_state.ICPORTAGE_DEV01_INTERNAL_NLB_ZONE_ID}"
        evaluate_target_health = false
    }
}

######### DATA ##############
resource "aws_route53_record" "ICPORTAGE_CNV01_DHIC_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "dhic.cnv01.portage.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICPORTAGE_CNV01_DHIC_SERVER_PRIVATEIP}"]
}

resource "aws_route53_record" "ICPORTAGE_CNV01_DHIC_DB_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "dhicdb.cnv01.portage.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICPORTAGE_CNV01_DHIC_MSSQL_DATABASE_PRIVATEIP}"]
}

######### HAPROXY ##############
# resource "aws_route53_record" "ICPORTAGE_CNV01_INTERNAL_HAPROXY_RECORD" {
#     zone_id = "${var.zone_id}"
#     name    = "inthap.cnv01.portage.insurcloud.ca"
#     ttl = "300"
#     type    = "A"
#     records = ["${data.terraform_remote_state.dev_remote_state.ICPORTAGE_CNV01_INTERNAL_HAPROXY_PRIVATEIP}"]
# }


# Not being used in Dev01 yet
# resource "aws_route53_record" "ICPORTAGE_DEV01_EXTERNAL_HAPROXY_RECORD" {
#     zone_id = "${var.zone_id}"
#     name    = "exthap.dev01.portage.insurcloud.ca"
#     ttl = "300"
#     type    = "A"
#     records = ["${data.terraform_remote_state.dev_remote_state.ICPORTAGE_DEV01_EXTERNAL_HAPROXY_PRIVATEIP}"]
# }

# #=======================================================================
# # PPS
# #=======================================================================
######### GUIDEWIRE ##############

######### MICROSERVICES ###########

######### HAPROXY ##############

######### DATA ##############



# #=======================================================================
# # PPS/DEV/QA
# #=======================================================================

######### JOBSCHEDULER ##############

############# JENKINS CLIENT #################
resource "aws_route53_record" "ICPORTAGE_DEV_JENKINS_CLIENT_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "jenkins-client.dev.portage.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICPORTAGE_DEV_JENKINS_CLIENT_PRIVATEIP}"]
}

#=======================================================================
# PROD
#=======================================================================
######### GUIDEWIRE ##############

######### MICROSERVICES ###########

######### HAPROXY ##############

######### DATA ##############

######### JOBSCHEDULER ##############

############# JENKINS CLIENT #################
