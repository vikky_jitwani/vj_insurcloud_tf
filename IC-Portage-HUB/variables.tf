variable "client_name" {
  description = "AWS client name for resources (Ex: MASTERCLOUD)"
  default = ""
}

variable "environment_name" {
  description = "AWS environment name for resources (Ex: NON-PROD)"
  default = ""
}

variable "environment_type" {
  description = "AWS environment type for resources (Ex: P=PROD/HUB, D=NON-PROD)"
  default = ""
}

variable "client_number" {
  description = "AWS client number for tagging (Ex: A001)"
  default = "A005"
}

variable "region" {
  description = "AWS region for hosting our your network"
  default = "ca-central-1"
}

variable "volume_key" {
  description = "KMS volume key hub account"
  default = ""
}

variable "ami_id_amazonlinux201709base" {
  description = "Amazon Linux Encypted Root Volume AMI"
  default = ""
}

variable "ami_id_redhatlinux75" {
  description = "Redhat Linux Encypted Root Volume AMI"
  default = ""
}

variable "ami_id_amazonlinux201803base" {
  description = "Amazon Linux 2018 03 Encypted Root Volume AMI"
  default = ""
}

variable "ami_id_amazonlinux2base" {
  description = "Amazon Linux 2 Base Encypted Root Volume AMI"
  default = ""
}

variable "ami_id_windows2008base" {
  description = "Windows 2008 Base Encypted Root Volume AMI"
  default = ""
}

variable "ami_id_windows2016base20200806" {
  description = "Updated Windows 2016 Base Encypted Root Volume AMI 06 August 2020"
  default = ""
}

variable "ami_id_amazonlinuxbase20200806" {
  description = "Updated Amazon Linux 2 Base Encypted Root Volume AMI 6 August 2020"
  default = ""
}

variable "ami_id_amazonlinux201712base" {
  description = "Amazon Linux 2017 12 Base Encypted Root Volume AMI"
  default = ""
}

variable "ami_id_windows2016base" {
  description = "Windows 2016 Base Encypted Root Volume AMI"
  default = ""
}

variable "ami_id_windows2012base" {
  description = "Windows 2012 Base Encypted Root Volume AMI"
  default = ""
}

variable "ami_id_windows2012R2base" {
  description = "Windows 2012 R2 Base Encypted Root Volume AMI"
  default = ""
}

variable "ami_id_ubuntu1604baseimage" {
  description = "Ubuntu 16 04 Base Encypted Root Volume AMI"
  default = ""
}

variable "ami_id_ubuntu1804baseimage" {
  description = "Ubuntu 18 04 Base Encypted Root Volume AMI"
  default = ""
}

variable "ami_id_rhel75baseimage" {
  description = "RHEL 7.5 Base image from the cloud"
  default = ""
}


variable "vpc_id" {
  description = "VPC id for HUB account"
  default = "vpc-05cfa2b3f4a5adeb2"
}

variable "vpc_cidr" {
  description = "VPC cidr hub account"
  default = ""
}

variable "conduit-rds-password" {
  description = "Conduit DB Password"
  default = ""
}

variable "portage-hub-database-1a-subnet" {
  description = "Database subnet one"
  default = ""
}

variable "portage-hub-database-1b-subnet" {
  description = "Database subnet two"
  default = ""
}

variable "portage-hub-private-1a-subnet" {
  description = "Private Subnet One"
  default = ""
}

variable "portage-hub-private-1b-subnet" {
  description = "Private Subnet Two"
  default = ""
}

variable "portage-hub-public-1a-subnet" {
  description = "Public Subnet One"
  default = ""
}

variable "portage-hub-public-1b-subnet" {
  description = "Public Subnet two"
  default = ""
}

variable "default_cidr" {
  description = "default cidr used"
  default = ["10.0.0.0/8"]
}

variable "deloitte_cidr" {
    default = "10.10.4.0/22"
}

variable "portage_cidr" {
    default = "10.171.0.0/23"
}

variable "portage_watchguard_cidr" {
    default = "192.168.113.0/24"
}

variable "portage_dmz_cidr" {
    default = "192.168.100.0/24"
}

variable "portage_wireless_cidr" {
    default = "192.168.101.0/24"
}

variable "zone_id" {
  description = "default zone id"
  default = ""
}

variable "portage_dev_cidr" {
  description = "default cidr used"
  type = "list"
  default = ["10.171.8.0/22"]
}

variable "portage_prod_cidr" {
  description = "default cidr used"
  type = "list"
  default = ["10.171.4.0/22"]
}

variable "portage_hub_cidr" {
  description = "default cidr used"
  type = "list"
  default = ["10.171.0.0/22"]
}

variable "portage-hub_private_cidr" {
  description = "default cidr used"
  type = "list"
  default = ["10.171.0.0/24", "10.171.1.0/24"]
}
variable "portage_aviatrix_vpn" {
    default = "10.171.2.61/32"
}

variable "ad_dns1" {
    default = "10.171.0.80"
}
variable "ad_dns2" {
    default = "10.171.1.41"
}
variable "ad_dns3" {
    default = "10.171.0.2"
}
variable "ou_name" {
    default = "hub"
}
variable "pwordparameter" {
    default = "domain_password"
}
variable "unameparameter" {
    default = "domain_username"
}
variable "ad_domain" {
    default = "internal.portage.insurcloud.ca"
}
data "aws_caller_identity" "current" {}

variable "portage-dev-account" {
  default = "091072499599"
}

variable "portage-prod-account" {
  default = "625725589452"
}

variable "portage-hub-account" {
  default = "342339668068"
}