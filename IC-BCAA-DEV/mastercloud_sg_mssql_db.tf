resource "aws_security_group" "SG_MSSQL_DB" {
  name = "MSSQL-DB-SG"

  ingress {
    from_port = 1433
    to_port   = 1433
    protocol  = "tcp"
    cidr_blocks = ["${var.bcaa_hub_cidr}"]
    description = "BCAA Hub CIDR"
  }

  ingress {
    from_port = 1433
    to_port   = 1433
    protocol  = "tcp"
    cidr_blocks = ["${var.bcaa_dev01_dhic_sap}"]
    description = "DHIC SAP Server"
  }

  ingress {
    from_port = 80
    to_port   = 80
    protocol  = "tcp"
    cidr_blocks = ["${var.bcaa_aviatrix_vpn}"]
    description = "BCAA Aviatrix VPN"
  }

  ingress {
    from_port = 443
    to_port   = 443
    protocol  = "tcp"
    cidr_blocks = ["${var.bcaa_dev_cidr}"]
    description = "BCAA Dev CIDR"
  }

  ingress {
    from_port = 1434
    to_port   = 1434
    protocol  = "udp"
    cidr_blocks = ["${var.bcaa_hub_cidr}"]
    description = "BCAA Hub CIDR"
  }

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    description = "Qliksense"
    cidr_blocks = ["${var.bcaa_hub_cidr}"]
  }

  ingress {
    from_port   = 1433
    to_port     = 1433
    protocol    = "tcp"
    description = "Qliksense and Jenkins connection"
    cidr_blocks = ["${var.bcaa_jenkins_client}"]
  }

  egress {
    from_port = 0
    to_port   = 0
    protocol  = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    Contract = "MANAGED_SERVICES"
    Name     = "MSSQL-DB-SG"
  }
}
