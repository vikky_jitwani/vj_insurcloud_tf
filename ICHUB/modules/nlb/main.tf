terraform {
  required_version = ">= 0.9.3"
}

# ---------------------------------------------------------------------------------------------------------------------
# CREATE NLB
# ---------------------------------------------------------------------------------------------------------------------
resource "aws_lb" "main" {
  name               = "${var.name}"
  internal           = "${var.internal}"
  load_balancer_type = "network"
  idle_timeout       = "${var.idle_timeout}"
  subnets            = ["${var.subnet_ids}"]
  enable_http2       = true

  enable_cross_zone_load_balancing = "${var.cross_zone_load_balancing}"
  
  tags = "${merge(var.load_balancer_tags, map("Name", var.name))}"
}

resource "aws_lb_listener" "main" {
  load_balancer_arn = "${aws_lb.main.id}"
  port              = "${var.lb_port}"
  protocol          = "TCP"

  default_action {
    target_group_arn = "${aws_lb_target_group.main.id}"
    type             = "forward"
  }
}

resource "aws_lb_target_group" "main" {
  name     = "${var.name}"
  port     = "${var.api_port}"
  protocol = "TCP"
  vpc_id   = "${var.vpc_id}"

  health_check {
    interval            = "${var.health_check_interval}"
    path                = "${var.health_check_path}"
    port                = "${var.health_check_port == 0 ? var.api_port : var.health_check_port}"
    protocol            = "${var.health_check_protocol == "" ? var.api_protocol : var.health_check_protocol}"
    healthy_threshold   = "${var.health_check_healthy_threshold}"
    unhealthy_threshold = "${var.health_check_unhealthy_threshold}"
  }
}

# ---------------------------------------------------------------------------------------------------------------------
# ATTACH THE NLB TO THE ASG
# ---------------------------------------------------------------------------------------------------------------------
resource "aws_autoscaling_attachment" "main" {
  autoscaling_group_name = "${var.asg_name}"
  alb_target_group_arn   = "${aws_lb_target_group.main.id}"
}

# ---------------------------------------------------------------------------------------------------------------------
# OPTIONALLY CREATE A ROUTE 53 ENTRY FOR THE NLB
# ---------------------------------------------------------------------------------------------------------------------
resource "aws_route53_record" "main_nlb" {
  count = "${var.create_dns_entry}"

  zone_id = "${var.hosted_zone_id}"
  name    = "${var.domain_name}"
  type    = "A"

  alias {
    name    = "${aws_lb.main.dns_name}"
    zone_id = "${aws_lb.main.zone_id}"

    # When set to true, if either none of the NLB's EC2 instances are healthy or the NLB itself is unhealthy,
    # Route 53 routes queries to "other resources." But since we haven't defined any other resources, we'd rather
    # avoid any latency due to switchovers and just wait for the NLB and consul instances to come back online.
    # For more info, see http://docs.aws.amazon.com/Route53/latest/DeveloperGuide/resource-record-sets-values-alias.html#rrsets-values-alias-evaluate-target-health
    evaluate_target_health = false
  }
}
