resource "aws_security_group" "SG_CLIENT_PING_TEST" {
  name = "CLIENT-PING-TEST"

  ingress {
    from_port       = -1
    to_port         = -1
    protocol        = "icmp"
    cidr_blocks     = [
      "${var.bcaa_cidr}",
      "${var.bcaa_watchguard_cidr}",
      "${var.bcaa_dmz_cidr}",
      "${var.bcaa_wireless_cidr}"
    ]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    Name = "CLIENT-PING-TEST"
  }
}

resource "aws_security_group" "SG_AVIATRIX_RDP" {
  name = "AVIATRIX-RDP-SG"

  ingress {
    from_port   = 3389
    to_port     = 3389
    protocol    = "tcp"
    cidr_blocks = ["${var.bcaa_aviatrix_vpn}"]
  
    description = "RDP"
  }

  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    Name = "AVIATRIX-RDP-SG"
  }
}

resource "aws_security_group" "SG_AVIATRIX_SSH" {
  name = "AVIATRIX-SSH-SG"

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = [
      "${var.bcaa_aviatrix_vpn}"
    ]
    description = "SSH"
  }

  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    Name = "AVIATRIX-SSH-SG"
  }
}

resource "aws_security_group" "SG_JOB_SCHEDULER" {
  name = "JOB-SCHEDULER-SG"

  ingress {
    from_port       = 4444
    to_port         = 4444
    protocol        = "tcp"
    cidr_blocks = [
      "${var.bcaa_hub_cidr}",
      "${var.bcaa_aviatrix_vpn}"
    ]
  }

  ingress {
    from_port       = 4444
    to_port         = 4444
    protocol        = "udp"
    cidr_blocks = [
      "${var.bcaa_hub_cidr}",
      "${var.bcaa_aviatrix_vpn}"
    ]
  }

  ingress {
    from_port       = 4446
    to_port         = 4446
    protocol        = "tcp"
    cidr_blocks = [
      "${var.bcaa_hub_cidr}",
      "${var.bcaa_aviatrix_vpn}"
    ]
  }

  ingress {
    from_port       = 4446
    to_port         = 4446
    protocol        = "udp"
    cidr_blocks = [
      "${var.bcaa_hub_cidr}",
      "${var.bcaa_aviatrix_vpn}"
    ]
  }

  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    Name = "JOB-SCHEDULER-SG"
  }
}

resource "aws_security_group" "SG_JENKINS_CLIENT_SSH" {
  name = "JENKINS-CLIENT-SSH-SG"
 
  ingress {
    from_port       = 22
    to_port         = 22
    protocol        = "tcp"
    cidr_blocks = [
      "10.11.0.173/32" 
    ]
    description = "Jenkins Client SSH"
  }

  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    Name = "JENKINS-CLIENT-SSH-SG"
  }
}
