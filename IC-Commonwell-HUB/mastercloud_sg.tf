
resource "aws_security_group" "SG_JENKINS_CLIENT" {
  name = "COMMONWELL-JENKINS-CLIENT-SG"

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = "${var.default_cidr}"
  }

  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = "${var.default_cidr}"
  }

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = "${var.default_cidr}"
  }

    ingress {
    from_port   = 1433
    to_port     = 1433
    protocol    = "tcp"
    cidr_blocks = "${var.default_cidr}"
  }

    ingress {
    from_port   = 9443
    to_port     = 9443
    protocol    = "tcp"
    cidr_blocks = "${var.default_cidr}"
  }

  ingress {
    from_port       = -1
    to_port         = -1
    protocol        = "icmp"
    cidr_blocks     = ["10.0.0.0/8"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    Name = "COMMONWELL-JENKINS-CLIENT-SG"
  }
}

resource "aws_security_group" "SG_MSSQL_DB" {
  name = "COMMONWELL-MSSQL-DB-SG"

  ingress {
    from_port   = 1433
    to_port     = 1433
    protocol    = "tcp"
    cidr_blocks = "${var.default_cidr}"
    # Should narrow down to just instances with Guidewire SG
  }

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = "${var.default_cidr}"
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    Name = "COMMONWELL-MSSQL-DB-SG"
  }
}

resource "aws_security_group" "SG_POSTGRES_DB" {
  name = "COMMONWELL-POSTGRES-DB-SG"

  ingress {
    from_port   = 5432
    to_port     = 5432
    protocol    = "tcp"
    cidr_blocks = "${var.default_cidr}"
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    Name = "COMMONWELL-POSTGRES-DB-SG"
  }
}

resource "aws_security_group" "SG_MYSQL_DB" {
  name = "COMMONWELL-MYSQL-DB-SG"

  ingress {
    from_port   = 3306
    to_port     = 3306
    protocol    = "tcp"
    cidr_blocks = "${var.default_cidr}"
  }

  ingress {
    from_port   = 3301
    to_port     = 3301
    protocol    = "tcp"
    cidr_blocks = "${var.default_cidr}"
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    Name = "COMMONWELL-MYSQL-DB-SG"
  }
}

resource "aws_security_group" "SG_HTTPS" {
  name = "COMMONWELL-HTTPS-SG"

  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = "${var.default_cidr}"
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    Name = "COMMONWELL-HTTPS-SG"
  }
}

resource "aws_security_group" "SG_CONDUIT_SERVER" {
    name        = "SG_CONDUIT_SERVER"
    description = "Security group for Conduit Server instances"
    vpc_id      = "${var.vpc_id}"

    ingress {
        from_port       = 8080
        to_port         = 8080
        protocol        = "tcp"
        cidr_blocks     = "${var.default_cidr}"
        description     = "Web Server"
    }

    ingress {
        from_port       = 5432
        to_port         = 5432
        protocol        = "tcp"
        cidr_blocks     = "${var.default_cidr}"
        description     = "PostgreSQL"
    }

    ingress {
        from_port       = 445
        to_port         = 445
        protocol        = "tcp"
        cidr_blocks     = "${var.default_cidr}"
        description     = "Microsoft Active Directory or SMB"
    }

    ingress {
        from_port       = 445
        to_port         = 445
        protocol        = "udp"
        cidr_blocks     = "${var.default_cidr}"
        description     = "Microsoft Active Directory or SMB"
    }

    ingress {
        from_port       = 139
        to_port         = 139
        protocol        = "tcp"
        cidr_blocks     = "${var.default_cidr}"
        description     = "NetBIOS Session Service"
    }

    ingress {
        from_port       = 137
        to_port         = 138
        protocol        = "udp"
        cidr_blocks     = "${var.default_cidr}"
        description     = "NetBIOS Name Service and Datagram Service"
    }

    ingress {
        from_port       = 80
        to_port         = 80
        protocol        = "tcp"
        cidr_blocks     = "${var.default_cidr}"
        description     = "Dashboard"
    }

    ingress {
        from_port       = 22
        to_port         = 22
        protocol        = "tcp"
        cidr_blocks     = "${var.default_cidr}"
        description     = "SSH"
    }

    egress {
        from_port       = 0
        to_port         = 0
        protocol        = "-1"
        cidr_blocks     = ["0.0.0.0/0"]
    }

    tags {
        "Application" = "Conduit Server"
    }
}

resource "aws_security_group" "SG_CONDUIT_DB" {
  name = "SG-CONDUIT-DB"

  ingress {
    from_port   = 5432
    to_port     = 5432
    protocol    = "tcp"
    cidr_blocks = "${var.default_cidr}"
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    Name = "SG-CONDUIT-DB"
  }
}

resource "aws_security_group" "SG_CONDUIT_WORKER" {
    name        = "SG_CONDUIT_WORKER"
    description = "Security group for Conduit Worker instances"
    vpc_id      = "${var.vpc_id}"

    ingress {
        from_port       = 8080
        to_port         = 8080
        protocol        = "tcp"
        cidr_blocks     = "${var.default_cidr}"
        description     = "Web Server"
    }

    ingress {
        from_port       = 3389
        to_port         = 3389
        protocol        = "tcp"
        cidr_blocks     = "${var.default_cidr}"
        description     = "RDP"
    }

    egress {
        from_port       = 0
        to_port         = 0
        protocol        = "-1"
        cidr_blocks     = ["0.0.0.0/0"]
    }

    tags {
        "Application" = "Conduit Worker"
    }
}


resource "aws_security_group" "SG_COMMONWELL_EXSTREAM" {
  name = "COMMONWELL-EXSTREAM-SG"

  ingress {
    from_port   = 3389
    to_port     = 3389
    protocol    = "tcp"
    cidr_blocks = "${var.default_cidr}"
  }

  ingress {
    from_port   = 28700
    to_port     = 28700
    protocol    = "tcp"
    cidr_blocks = "${var.default_cidr}"
  }

  ingress {
    from_port   = 27000
    to_port     = 27000
    protocol    = "tcp"
    cidr_blocks = "${var.default_cidr}"
  }

  ingress {
    from_port   = 2701
    to_port     = 2702
    protocol    = "tcp"
    cidr_blocks = "${var.default_cidr}"
  }

  ingress {
    from_port   = 8443
    to_port     = 8443
    protocol    = "tcp"
    cidr_blocks = "${var.default_cidr}"
  }

  ingress {
    from_port   = 1433
    to_port     = 1433
    protocol    = "tcp"
    cidr_blocks = "${var.default_cidr}"
  }

  ingress {
    from_port   = 8080
    to_port     = 8080
    protocol    = "tcp"
    cidr_blocks = "${var.default_cidr}"
  }

  ingress {
    from_port   = 28701
    to_port     = 28702
    protocol    = "tcp"
    cidr_blocks = "${var.default_cidr}"
  }

  ingress {
    from_port   = 2718
    to_port     = 2719
    protocol    = "tcp"
    cidr_blocks = "${var.default_cidr}"
  }

  ingress {
    from_port   = 54415
    to_port     = 54415
    protocol    = "tcp"
    cidr_blocks = "${var.default_cidr}"
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    Name = "COMMONWELL-EXSTREAM-SG"
  }
}

