######################NLBs ##############################
########## External Load Balancers: North of firewall ###########
#=======================================================================
# PROD
#=======================================================================
//resource "aws_lb" "ICPEEL_PROD_BROKER_NLB" {
//    name               = "peel-insurcloud"
//    internal           = false
//    load_balancer_type = "network"
//    enable_cross_zone_load_balancing = false
//    subnets            = [
//        "${var.GORE-hub-public-1a-subnet}",
//        "${var.GORE-hub-public-1b-subnet}"
//    ] # Changing this value will force a recreation of the resource
//
//    enable_deletion_protection = false
//
//    # access_logs {
//    #     bucket  = "${aws_s3_bucket.ICPEEL_PROD_BROKER_NLB.bucket}"
//    #     enabled = true
//    # }
//
//    tags = {
//        Name = "PEEL-PROD-BROKER-NLB"
//    }
//}
//
//resource "aws_lb_target_group" "ICPEEL_PROD_BROKER_NLB_TARGET_GROUP" {
//    name        = "CW-PROD-BROKER-NLB"
//    port        = 443
//    protocol    = "TCP"
//    target_type = "ip"
//    vpc_id      = "${var.vpc_id}"
//
//    health_check {
//        protocol = "TCP"
//        port = 8444
//    }
//
//    tags = {
//        Name = "PEEL-PROD-BROKER-NLB-TARGET-GROUP"
//    }
//}
//
//resource "aws_lb_listener" "ICPEEL_PROD_BROKER_NLB_LISTENER" {
//    load_balancer_arn = "${aws_lb.ICPEEL_PROD_BROKER_NLB.arn}"
//    port              = "443"
//    protocol          = "TCP"
//
//    default_action {
//        type             = "forward"
//        target_group_arn = "${aws_lb_target_group.ICPEEL_PROD_BROKER_NLB_TARGET_GROUP.arn}"
//    }
//}

#=======================================================================
# PPS
#=======================================================================
//resource "aws_lb" "ICPEEL_PPS_BROKER_NLB" {
//    name               = "peel-insurcloud-pps"
//    internal           = false
//    load_balancer_type = "network"
//    enable_cross_zone_load_balancing = false
//    subnets            = [
//        "${var.peel-hub-public-1a-subnet}",
//        "${var.peel-hub-public-1b-subnet}"
//    ] # Changing this value will force a recreation of the resource
//
//    enable_deletion_protection = false
//
//    # access_logs {
//    #     bucket  = "${aws_s3_bucket.ICPEEL_PPS_BROKER_NLB.bucket}"
//    #     enabled = true
//    # }
//
//    tags = {
//        Name = "PEEL-PPS-BROKER-NLB"
//    }
//}
//
//resource "aws_lb_target_group" "ICPEEL_PPS_BROKER_NLB_TARGET_GROUP" {
//    name        = "PEEL-PPS-BROKER-NLB"
//    port        = 443
//    protocol    = "TCP"
//    target_type = "ip"
//    vpc_id      = "${var.vpc_id}"
//
//    health_check {
//        protocol = "TCP"
//        port = 8444
//    }
//
//    tags = {
//        Name = "PEEL-PPS-BROKER-NLB-TARGET-GROUP"
//    }
//}
//
//resource "aws_lb_listener" "ICPEEL_PPS_BROKER_NLB_LISTENER" {
//    load_balancer_arn = "${aws_lb.ICPEEL_PPS_BROKER_NLB.arn}"
//    port              = "443"
//    protocol          = "TCP"
//
//    default_action {
//        type             = "forward"
//        target_group_arn = "${aws_lb_target_group.ICPEEL_PPS_BROKER_NLB_TARGET_GROUP.arn}"
//    }
//}

#=======================================================================
# DEV01
#=======================================================================
resource "aws_lb" "ICGORE_DEV01_NORTH_ALB" {
    name               = "gore-insurcloud-dev01-north"
    internal           = false
    load_balancer_type = "application"
    subnets            = [
        "${var.gore-hub-public-1b-subnet}",
        "${var.gore-hub-public-1a-subnet}"
    ] # Changing this value will force a recreation of the resource

    enable_deletion_protection = true

    access_logs {
        bucket  = "${aws_s3_bucket.ICGORE_HUB_NORTH_LB_ACCESS_LOG.bucket}"
        prefix = "DEV01"
        enabled = true
    }

    tags             = {
        Name = "GORE-DEV01-EXTERNAL-ALB"
    }
}

resource "aws_lb_target_group" "ICGORE_DEV01_NORTH_ALB_TARGET_GROUP" {
    name        = "ICGORE-DEV01-NORTH-ALB"
    port        = 80
    protocol    = "HTTP"
    target_type = "ip"
    vpc_id      = "${var.vpc_id}"

    tags = {
        Name = "GORE-DEV01-NORTH-ALB-TARGET-GROUP"
    }
}

resource "aws_lb_listener" "ICGORE_DEV01_NORTH_ALB_LISTENER_SECURED" {
    load_balancer_arn = "${aws_lb.ICGORE_DEV01_NORTH_ALB.arn}"
    port              = "443"
    protocol          = "HTTPS"
    ssl_policy        = "ELBSecurityPolicy-TLS-1-2-2017-01"
    certificate_arn   = "arn:aws:acm:ca-central-1:963473599983:certificate/b026c039-cf5d-41ee-b72b-0807a255a47a"


#Point to PAN if it has license, point to MS gateway api if not. 
    default_action {
        type = "fixed-response"

        fixed_response {
        content_type = "text/plain"
        message_body = "Fixed response content"
        status_code  = "200"
        }
    }
}

####VPC Link ####
resource "aws_lb" "ICGORE_DEV01_APW_NLB" {
    name               = "gore-insurcloud-dev01-apw"
    internal           = true
    load_balancer_type = "network"
    subnets            = [
        "${var.gore-hub-private-1b-subnet}",
        "${var.gore-hub-private-1a-subnet}"
    ] # Changing this value will force a recreation of the resource

    enable_deletion_protection = true

    #access_logs {
    #     bucket  = "${aws_s3_bucket.ICGORE_DEV01_NORTH_ALB.bucket}"
    #     enabled = true
    #}

    tags             = {
        Name = "GORE-DEV01-APW-NLB"
    }
}

resource "aws_lb_target_group" "ICGORE_DEV01_APW_NLB_TARGET_GROUP" {
    name        = "ICGORE-DEV01-APW-NLB"
    port        = 80
    protocol    = "TCP"
    target_type = "ip"
    vpc_id      = "${var.vpc_id}"

    tags = {
        Name = "GORE-DEV01-APW-NLB-TARGET-GROUP"
    }
}

resource "aws_lb_listener" "ICGORE_DEV01_APW_NLB_LISTENER" {
    load_balancer_arn = "${aws_lb.ICGORE_DEV01_APW_NLB.arn}"
    port              = "80"
    protocol          = "TCP"

    default_action {
        type = "forward"

    target_group_arn = "${aws_lb_target_group.ICGORE_DEV01_APW_NLB_TARGET_GROUP.arn}"
    }
}

#=======================================================================
# DEV02
#=======================================================================
resource "aws_lb" "ICGORE_DEV02_NORTH_ALB" {
    name               = "gore-insurcloud-dev02-north"
    internal           = false
    load_balancer_type = "application"
    subnets            = [
        "${var.gore-hub-public-1b-subnet}",
        "${var.gore-hub-public-1a-subnet}"
    ] # Changing this value will force a recreation of the resource

    enable_deletion_protection = true

    access_logs {
        bucket  = "${aws_s3_bucket.ICGORE_HUB_NORTH_LB_ACCESS_LOG.bucket}"
        prefix = "DEV02"
        enabled = true
    }

    tags             = {
        Name = "GORE-DEV02-EXTERNAL-ALB"
    }
}

resource "aws_lb_target_group" "ICGORE_DEV02_NORTH_ALB_TARGET_GROUP" {
    name        = "ICGORE-DEV02-NORTH-ALB"
    port        = 80
    protocol    = "HTTP"
    target_type = "ip"
    vpc_id      = "${var.vpc_id}"

    tags = {
        Name = "GORE-DEV02-NORTH-ALB-TARGET-GROUP"
    }
}

resource "aws_lb_listener" "ICGORE_DEV02_NORTH_ALB_LISTENER_SECURED" {
    load_balancer_arn = "${aws_lb.ICGORE_DEV02_NORTH_ALB.arn}"
    port              = "443"
    protocol          = "HTTPS"
    ssl_policy        = "ELBSecurityPolicy-TLS-1-2-2017-01"
    certificate_arn   = "arn:aws:acm:ca-central-1:963473599983:certificate/b026c039-cf5d-41ee-b72b-0807a255a47a"


#Point to PAN if it has license, point to MS gateway api if not. 
    default_action {
        type = "fixed-response"

        fixed_response {
        content_type = "text/plain"
        message_body = "Fixed response content"
        status_code  = "200"
        }
    }
}

####VPC Link DEV02####
resource "aws_lb" "ICGORE_DEV02_APW_NLB" {
    name               = "gore-insurcloud-dev02-apw"
    internal           = true
    load_balancer_type = "network"
    subnets            = [
        "${var.gore-hub-private-1b-subnet}",
        "${var.gore-hub-private-1a-subnet}"
    ] # Changing this value will force a recreation of the resource

    enable_deletion_protection = true

    # access_logs {
    #     bucket  = "${aws_s3_bucket.gore-north-lb-accesslog.bucket}"
    #     prefix = "DEV02"
    #     enabled = true
    # }

    tags             = {
        Name = "GORE-DEV02-APW-NLB"
    }
}

resource "aws_lb_target_group" "ICGORE_DEV02_APW_NLB_TARGET_GROUP" {
    name        = "ICGORE-DEV02-APW-NLB"
    port        = 80
    protocol    = "TCP"
    target_type = "ip"
    vpc_id      = "${var.vpc_id}"

    tags = {
        Name = "GORE-DEV02-APW-NLB-TARGET-GROUP"
    }
}

resource "aws_lb_listener" "ICGORE_DEV02_APW_NLB_LISTENER" {
    load_balancer_arn = "${aws_lb.ICGORE_DEV02_APW_NLB.arn}"
    port              = "80"
    protocol          = "TCP"

    default_action {
        type = "forward"

    target_group_arn = "${aws_lb_target_group.ICGORE_DEV02_APW_NLB_TARGET_GROUP.arn}"
    }
    
}


# #=======================================================================
# # DEV03
# #=======================================================================
resource "aws_lb" "ICGORE_DEV03_NORTH_ALB" {
    name               = "gore-insurcloud-dev03-north"
    internal           = false
    load_balancer_type = "application"
    subnets            = [
        "${var.gore-hub-public-1b-subnet}",
        "${var.gore-hub-public-1a-subnet}"
    ] # Changing this value will force a recreation of the resource

    enable_deletion_protection = true

    access_logs {
        bucket  = "${aws_s3_bucket.ICGORE_HUB_NORTH_LB_ACCESS_LOG.bucket}"
        prefix  = "DEV03"
        enabled = true
    }

    tags             = {
        Name = "GORE-DEV03-EXTERNAL-ALB"
    }
}

resource "aws_lb_target_group" "ICGORE_DEV03_NORTH_ALB_TARGET_GROUP" {
    name        = "ICGORE-DEV03-NORTH-ALB"
    port        = 80
    protocol    = "HTTP"
    target_type = "ip"
    vpc_id      = "${var.vpc_id}"

    tags = {
        Name = "GORE-DEV03-NORTH-ALB-TARGET-GROUP"
    }
}

resource "aws_lb_listener" "ICGORE_DEV03_NORTH_ALB_LISTENER_SECURED" {
    load_balancer_arn = "${aws_lb.ICGORE_DEV03_NORTH_ALB.arn}"
    port              = "443"
    protocol          = "HTTPS"
    ssl_policy        = "ELBSecurityPolicy-TLS-1-2-2017-01"
    certificate_arn   = "arn:aws:acm:ca-central-1:963473599983:certificate/b026c039-cf5d-41ee-b72b-0807a255a47a"


#Point to PAN if it has license, point to MS gateway api if not. 
    default_action {
        type = "fixed-response"

        fixed_response {
        content_type = "text/plain"
        message_body = "Fixed response content"
        status_code  = "200"
        }
    }
}
####VPC Link DEV03####
resource "aws_lb" "ICGORE_DEV03_APW_NLB" {
    name               = "gore-insurcloud-dev03-apw"
    internal           = true
    load_balancer_type = "network"
    subnets            = [
        "${var.gore-hub-private-1b-subnet}",
        "${var.gore-hub-private-1a-subnet}"
    ] # Changing this value will force a recreation of the resource

    enable_deletion_protection = true

    #access_logs {
    #     bucket  = "${aws_s3_bucket.ICGORE_DEV03_NORTH_ALB.bucket}"
    #     enabled = true
    #}

    tags             = {
        Name = "GORE-DEV03-APW-NLB"
    }
}

resource "aws_lb_target_group" "ICGORE_DEV03_APW_NLB_TARGET_GROUP" {
    name        = "ICGORE-DEV03-APW-NLB"
    port        = 80
    protocol    = "TCP"
    target_type = "ip"
    vpc_id      = "${var.vpc_id}"

    tags = {
        Name = "GORE-DEV03-APW-NLB-TARGET-GROUP"
    }
}

resource "aws_lb_listener" "ICGORE_DEV03_APW_NLB_LISTENER" {
    load_balancer_arn = "${aws_lb.ICGORE_DEV03_APW_NLB.arn}"
    port              = "80"
    protocol          = "TCP"

    default_action {
        type = "forward"

    target_group_arn = "${aws_lb_target_group.ICGORE_DEV03_APW_NLB_TARGET_GROUP.arn}"
    }
}


#=======================================================================
# TBT01
#=======================================================================
resource "aws_lb" "ICGORE_TBT01_NORTH_ALB" {
    name               = "gore-insurcloud-tbt01-north"
    internal           = false
    load_balancer_type = "application"
    subnets            = [
        "${var.gore-hub-public-1b-subnet}",
        "${var.gore-hub-public-1a-subnet}"
    ] # Changing this value will force a recreation of the resource

    enable_deletion_protection = true

    access_logs {
        bucket  = "${aws_s3_bucket.ICGORE_HUB_NORTH_LB_ACCESS_LOG.bucket}"
        prefix = "TBT01"
        enabled = true
    }

    tags             = {
        Name = "GORE-TBT01-EXTERNAL-ALB"
    }
}

resource "aws_lb_target_group" "ICGORE_TBT01_NORTH_ALB_TARGET_GROUP" {
    name        = "ICGORE-TBT01-NORTH-ALB"
    port        = 80
    protocol    = "HTTP"
    target_type = "ip"
    vpc_id      = "${var.vpc_id}"

    tags = {
        Name = "GORE-TBT01-NORTH-ALB-TARGET-GROUP"
    }
}

resource "aws_lb_listener" "ICGORE_TBT01_NORTH_ALB_LISTENER_SECURED" {
    load_balancer_arn = "${aws_lb.ICGORE_TBT01_NORTH_ALB.arn}"
    port              = "443"
    protocol          = "HTTPS"
    ssl_policy        = "ELBSecurityPolicy-TLS-1-2-2017-01"
    certificate_arn   = "arn:aws:acm:ca-central-1:963473599983:certificate/b026c039-cf5d-41ee-b72b-0807a255a47a"


#Point to PAN if it has license, point to MS gateway api if not. 
    default_action {
        type = "fixed-response"

        fixed_response {
        content_type = "text/plain"
        message_body = "Fixed response content"
        status_code  = "200"
        }
    }
}

####VPC Link TBT01####
resource "aws_lb" "ICGORE_TBT01_APW_NLB" {
    name               = "gore-insurcloud-tbt01-apw"
    internal           = true
    load_balancer_type = "network"
    subnets            = [
        "${var.gore-hub-private-1b-subnet}",
        "${var.gore-hub-private-1a-subnet}"
    ] # Changing this value will force a recreation of the resource

    enable_deletion_protection = true

    #access_logs {
    #     bucket  = "${aws_s3_bucket.ICGORE_TBT01_NORTH_ALB.bucket}"
    #     enabled = true
    #}

    tags             = {
        Name = "GORE-TBT01-APW-NLB"
    }
}

resource "aws_lb_target_group" "ICGORE_TBT01_APW_NLB_TARGET_GROUP" {
    name        = "ICGORE-TBT01-APW-NLB"
    port        = 80
    protocol    = "TCP"
    target_type = "ip"
    vpc_id      = "${var.vpc_id}"

    tags = {
        Name = "GORE-TBT01-APW-NLB-TARGET-GROUP"
    }
}

resource "aws_lb_listener" "ICGORE_TBT01_APW_NLB_LISTENER" {
    load_balancer_arn = "${aws_lb.ICGORE_TBT01_APW_NLB.arn}"
    port              = "80"
    protocol          = "TCP"

    default_action {
        type = "forward"

    target_group_arn = "${aws_lb_target_group.ICGORE_TBT01_APW_NLB_TARGET_GROUP.arn}"
    }
}

#=======================================================================
# QA01
#=======================================================================
resource "aws_lb" "ICGORE_QA01_NORTH_ALB" {
    name               = "gore-insurcloud-qa01-north"
    internal           = false
    load_balancer_type = "application"
    subnets            = [
        "${var.gore-hub-public-1b-subnet}",
        "${var.gore-hub-public-1a-subnet}"
    ] # Changing this value will force a recreation of the resource

    enable_deletion_protection = true

    access_logs {
        bucket  = "${aws_s3_bucket.ICGORE_HUB_NORTH_LB_ACCESS_LOG.bucket}"
        prefix  = "QA01"
        enabled = true
    }

    tags             = {
        Name = "GORE-QA01-EXTERNAL-ALB"
    }
}

resource "aws_lb_target_group" "ICGORE_QA01_NORTH_ALB_TARGET_GROUP" {
    name        = "ICGORE-QA01-NORTH-ALB"
    port        = 80
    protocol    = "HTTP"
    target_type = "ip"
    vpc_id      = "${var.vpc_id}"

    tags = {
        Name = "GORE-QA01-NORTH-ALB-TARGET-GROUP"
    }
}

resource "aws_lb_listener" "ICGORE_QA01_NORTH_ALB_LISTENER_SECURED" {
    load_balancer_arn = "${aws_lb.ICGORE_QA01_NORTH_ALB.arn}"
    port              = "443"
    protocol          = "HTTPS"
    ssl_policy        = "ELBSecurityPolicy-TLS-1-2-2017-01"
    certificate_arn   = "arn:aws:acm:ca-central-1:963473599983:certificate/b026c039-cf5d-41ee-b72b-0807a255a47a"


#Point to PAN if it has license, point to MS gateway api if not. 
    default_action {
        type = "fixed-response"

        fixed_response {
        content_type = "text/plain"
        message_body = "Fixed response content"
        status_code  = "200"
        }
    }
}

####VPC Link ####
resource "aws_lb" "ICGORE_QA01_APW_NLB" {
    name               = "gore-insurcloud-qa01-apw"
    internal           = true
    load_balancer_type = "network"
    subnets            = [
        "${var.gore-hub-private-1b-subnet}",
        "${var.gore-hub-private-1a-subnet}"
    ] # Changing this value will force a recreation of the resource

    enable_deletion_protection = true

    # access_logs {
    #     bucket  = "${aws_s3_bucket.gore-north-lb-accesslog.bucket}"
    #     prefix = "QA01"
    #     enabled = true
    # }

    tags             = {
        Name = "GORE-QA01-APW-NLB"
    }
}

resource "aws_lb_target_group" "ICGORE_QA01_APW_NLB_TARGET_GROUP" {
    name        = "ICGORE-QA01-APW-NLB"
    port        = 80
    protocol    = "TCP"
    target_type = "ip"
    vpc_id      = "${var.vpc_id}"

    tags = {
        Name = "GORE-QA01-APW-NLB-TARGET-GROUP"
    }
}

resource "aws_lb_listener" "ICGORE_QA01_APW_NLB_LISTENER" {
    load_balancer_arn = "${aws_lb.ICGORE_QA01_APW_NLB.arn}"
    port              = "80"
    protocol          = "TCP"

    default_action {
        type = "forward"

    target_group_arn = "${aws_lb_target_group.ICGORE_QA01_APW_NLB_TARGET_GROUP.arn}"
    }
    
}

#=======================================================================
# QA02
#=======================================================================
resource "aws_lb" "ICGORE_QA02_NORTH_ALB" {
    name               = "gore-insurcloud-qa02-north"
    internal           = false
    load_balancer_type = "application"
    subnets            = [
        "${var.gore-hub-public-1b-subnet}",
        "${var.gore-hub-public-1a-subnet}"
    ] # Changing this value will force a recreation of the resource

    enable_deletion_protection = true

    access_logs {
        bucket  = "${aws_s3_bucket.ICGORE_HUB_NORTH_LB_ACCESS_LOG.bucket}"
        prefix  = "QA02"
        enabled = true
    }

    tags             = {
        Name = "GORE-QA02-EXTERNAL-ALB"
    }
}

resource "aws_lb_target_group" "ICGORE_QA02_NORTH_ALB_TARGET_GROUP" {
    name        = "ICGORE-QA02-NORTH-ALB"
    port        = 80
    protocol    = "HTTP"
    target_type = "ip"
    vpc_id      = "${var.vpc_id}"

    tags = {
        Name = "GORE-QA02-NORTH-ALB-TARGET-GROUP"
    }
}

resource "aws_lb_listener" "ICGORE_QA02_NORTH_ALB_LISTENER_SECURED" {
    load_balancer_arn = "${aws_lb.ICGORE_QA02_NORTH_ALB.arn}"
    port              = "443"
    protocol          = "HTTPS"
    ssl_policy        = "ELBSecurityPolicy-TLS-1-2-2017-01"
    certificate_arn   = "arn:aws:acm:ca-central-1:963473599983:certificate/b026c039-cf5d-41ee-b72b-0807a255a47a"


#Point to PAN if it has license, point to MS gateway api if not. 
    default_action {
        type = "fixed-response"

        fixed_response {
        content_type = "text/plain"
        message_body = "Fixed response content"
        status_code  = "200"
        }
    }
}
####VPC Link ####
resource "aws_lb" "ICGORE_QA02_APW_NLB" {
    name               = "gore-insurcloud-qa02-apw"
    internal           = true
    load_balancer_type = "network"
    subnets            = [
        "${var.gore-hub-private-1b-subnet}",
        "${var.gore-hub-private-1a-subnet}"
    ] # Changing this value will force a recreation of the resource

    enable_deletion_protection = true

    # access_logs {
    #     bucket  = "${aws_s3_bucket.gore-north-lb-accesslog.bucket}"
    #     prefix = "QA02"
    #     enabled = true
    # }

    tags             = {
        Name = "GORE-QA02-APW-NLB"
    }
}

resource "aws_lb_target_group" "ICGORE_QA02_APW_NLB_TARGET_GROUP" {
    name        = "ICGORE-QA02-APW-NLB"
    port        = 80
    protocol    = "TCP"
    target_type = "ip"
    vpc_id      = "${var.vpc_id}"

    tags = {
        Name = "GORE-QA02-APW-NLB-TARGET-GROUP"
    }
}

resource "aws_lb_listener" "ICGORE_QA02_APW_NLB_LISTENER" {
    load_balancer_arn = "${aws_lb.ICGORE_QA02_APW_NLB.arn}"
    port              = "80"
    protocol          = "TCP"

    default_action {
        type = "forward"

    target_group_arn = "${aws_lb_target_group.ICGORE_QA02_APW_NLB_TARGET_GROUP.arn}"
    }
    
}


#=======================================================================
# TBT02
#=======================================================================
resource "aws_lb" "ICGORE_TBT02_NORTH_ALB" {
    name               = "gore-insurcloud-tbt02-north"
    internal           = false
    load_balancer_type = "application"
    subnets            = [
        "${var.gore-hub-public-1b-subnet}",
        "${var.gore-hub-public-1a-subnet}"
    ] # Changing this value will force a recreation of the resource

    enable_deletion_protection = true

    access_logs {
        bucket  = "${aws_s3_bucket.ICGORE_HUB_NORTH_LB_ACCESS_LOG.bucket}"
        prefix  = "TBT02"
        enabled = true
    }

    tags             = {
        Name = "GORE-TBT02-EXTERNAL-ALB"
    }
}

resource "aws_lb_target_group" "ICGORE_TBT02_NORTH_ALB_TARGET_GROUP" {
    name        = "ICGORE-TBT02-NORTH-ALB"
    port        = 80
    protocol    = "HTTP"
    target_type = "ip"
    vpc_id      = "${var.vpc_id}"

    tags = {
        Name = "GORE-TBT02-NORTH-ALB-TARGET-GROUP"
    }
}

resource "aws_lb_listener" "ICGORE_TBT02_NORTH_ALB_LISTENER_SECURED" {
    load_balancer_arn = "${aws_lb.ICGORE_TBT02_NORTH_ALB.arn}"
    port              = "443"
    protocol          = "HTTPS"
    ssl_policy        = "ELBSecurityPolicy-TLS-1-2-2017-01"
    certificate_arn   = "arn:aws:acm:ca-central-1:963473599983:certificate/b026c039-cf5d-41ee-b72b-0807a255a47a"


#Point to PAN if it has license, point to MS gateway api if not. 
    default_action {
        type = "fixed-response"

        fixed_response {
        content_type = "text/plain"
        message_body = "Fixed response content"
        status_code  = "200"
        }
    }
}
####VPC Link TBT01####
resource "aws_lb" "ICGORE_TBT02_APW_NLB" {
    name               = "gore-insurcloud-tbt02-apw"
    internal           = true
    load_balancer_type = "network"
    subnets            = [
        "${var.gore-hub-private-1b-subnet}",
        "${var.gore-hub-private-1a-subnet}"
    ] # Changing this value will force a recreation of the resource

    enable_deletion_protection = true

    #access_logs {
    #     bucket  = "${aws_s3_bucket.ICGORE_TBT02_NORTH_ALB.bucket}"
    #     enabled = true
    #}

    tags             = {
        Name = "GORE-TBT02-APW-NLB"
    }
}

resource "aws_lb_target_group" "ICGORE_TBT02_APW_NLB_TARGET_GROUP" {
    name        = "ICGORE-TBT02-APW-NLB"
    port        = 80
    protocol    = "TCP"
    target_type = "ip"
    vpc_id      = "${var.vpc_id}"

    tags = {
        Name = "GORE-TBT02-APW-NLB-TARGET-GROUP"
    }
}

resource "aws_lb_listener" "ICGORE_TBT02_APW_NLB_LISTENER" {
    load_balancer_arn = "${aws_lb.ICGORE_TBT02_APW_NLB.arn}"
    port              = "80"
    protocol          = "TCP"

    default_action {
        type = "forward"

    target_group_arn = "${aws_lb_target_group.ICGORE_TBT02_APW_NLB_TARGET_GROUP.arn}"
    }
}



#=======================================================================
# BAT
#=======================================================================
resource "aws_lb" "ICGORE_BAT_NORTH_ALB" {
    name               = "gore-insurcloud-bat-north"
    internal           = false
    load_balancer_type = "application"
    subnets            = [
        "${var.gore-hub-public-1b-subnet}",
        "${var.gore-hub-public-1a-subnet}"
    ] # Changing this value will force a recreation of the resource

    enable_deletion_protection = true

    access_logs {
        bucket  = "${aws_s3_bucket.ICGORE_HUB_NORTH_LB_ACCESS_LOG.bucket}"
        prefix  = "BAT"
        enabled = true
    }

    tags             = {
        Name = "GORE-BAT-EXTERNAL-ALB"
    }
}

resource "aws_lb_target_group" "ICGORE_BAT_NORTH_ALB_TARGET_GROUP" {
    name        = "ICGORE-BAT-NORTH-ALB"
    port        = 80
    protocol    = "HTTP"
    target_type = "ip"
    vpc_id      = "${var.vpc_id}"

    tags = {
        Name = "GORE-BAT-NORTH-ALB-TARGET-GROUP"
    }
}

resource "aws_lb_listener" "ICGORE_BAT_NORTH_ALB_LISTENER_SECURED" {
    load_balancer_arn = "${aws_lb.ICGORE_BAT_NORTH_ALB.arn}"
    port              = "443"
    protocol          = "HTTPS"
    ssl_policy        = "ELBSecurityPolicy-TLS-1-2-2017-01"
    certificate_arn   = "arn:aws:acm:ca-central-1:963473599983:certificate/b026c039-cf5d-41ee-b72b-0807a255a47a"


#Point to PAN if it has license, point to MS gateway api if not. 
    default_action {
        type = "fixed-response"

        fixed_response {
        content_type = "text/plain"
        message_body = "Fixed response content"
        status_code  = "200"
        }
    }
}

####VPC Link ####
resource "aws_lb" "ICGORE_BAT_APW_NLB" {
    name               = "gore-insurcloud-bat-apw"
    internal           = true
    load_balancer_type = "network"
    subnets            = [
        "${var.gore-hub-private-1b-subnet}",
        "${var.gore-hub-private-1a-subnet}"
    ] # Changing this value will force a recreation of the resource

    enable_deletion_protection = true

    # access_logs {
    #     bucket  = "${aws_s3_bucket.gore-north-lb-accesslog.bucket}"
    #     prefix = "BAT"
    #     enabled = true
    # }

    tags             = {
        Name = "GORE-BAT-APW-NLB"
    }
}

resource "aws_lb_target_group" "ICGORE_BAT_APW_NLB_TARGET_GROUP" {
    name        = "ICGORE-BAT-APW-NLB"
    port        = 80
    protocol    = "TCP"
    target_type = "ip"
    vpc_id      = "${var.vpc_id}"

    tags = {
        Name = "GORE-BAT-APW-NLB-TARGET-GROUP"
    }
}

resource "aws_lb_listener" "ICGORE_BAT_APW_NLB_LISTENER" {
    load_balancer_arn = "${aws_lb.ICGORE_BAT_APW_NLB.arn}"
    port              = "80"
    protocol          = "TCP"

    default_action {
        type = "forward"

    target_group_arn = "${aws_lb_target_group.ICGORE_BAT_APW_NLB_TARGET_GROUP.arn}"
    }
    
}


#=======================================================================
# PERF
#=======================================================================
resource "aws_lb" "ICGORE_PERF_NORTH_ALB" {
    name               = "gore-insurcloud-perf-north"
    internal           = false
    load_balancer_type = "application"
    subnets            = [
        "${var.gore-hub-public-1b-subnet}",
        "${var.gore-hub-public-1a-subnet}"
    ] # Changing this value will force a recreation of the resource

    enable_deletion_protection = true

    access_logs {
        bucket  = "${aws_s3_bucket.ICGORE_HUB_NORTH_LB_ACCESS_LOG.bucket}"
        prefix  = "PERF"
        enabled = true
    }

    tags             = {
        Name = "GORE-PERF-EXTERNAL-ALB"
    }
}

resource "aws_lb_target_group" "ICGORE_PERF_NORTH_ALB_TARGET_GROUP" {
    name        = "ICGORE-PERF-NORTH-ALB"
    port        = 80
    protocol    = "HTTP"
    target_type = "ip"
    vpc_id      = "${var.vpc_id}"

    tags = {
        Name = "GORE-PERF-NORTH-ALB-TARGET-GROUP"
    }
}

resource "aws_lb_listener" "ICGORE_PERF_NORTH_ALB_LISTENER_SECURED" {
    load_balancer_arn = "${aws_lb.ICGORE_PERF_NORTH_ALB.arn}"
    port              = "443"
    protocol          = "HTTPS"
    ssl_policy        = "ELBSecurityPolicy-TLS-1-2-2017-01"
    certificate_arn   = "arn:aws:acm:ca-central-1:963473599983:certificate/b026c039-cf5d-41ee-b72b-0807a255a47a"


#Point to PAN if it has license, point to MS gateway api if not. 
    default_action {
        type = "fixed-response"

        fixed_response {
        content_type = "text/plain"
        message_body = "Fixed response content"
        status_code  = "200"
        }
    }
}

####VPC Link ####
resource "aws_lb" "ICGORE_PERF_APW_NLB" {
    name               = "gore-insurcloud-perf-apw"
    internal           = true
    load_balancer_type = "network"
    subnets            = [
        "${var.gore-hub-private-1b-subnet}",
        "${var.gore-hub-private-1a-subnet}"
    ] # Changing this value will force a recreation of the resource

    enable_deletion_protection = true

    # access_logs {
    #     bucket  = "${aws_s3_bucket.gore-north-lb-accesslog.bucket}"
    #     prefix = "BAT"
    #     enabled = true
    # }

    tags             = {
        Name = "GORE-PERF-APW-NLB"
    }
}

resource "aws_lb_target_group" "ICGORE_PERF_APW_NLB_TARGET_GROUP" {
    name        = "ICGORE-PERF-APW-NLB"
    port        = 80
    protocol    = "TCP"
    target_type = "ip"
    vpc_id      = "${var.vpc_id}"

    tags = {
        Name = "GORE-PERF-APW-NLB-TARGET-GROUP"
    }
}

resource "aws_lb_listener" "ICGORE_PERF_APW_NLB_LISTENER" {
    load_balancer_arn = "${aws_lb.ICGORE_PERF_APW_NLB.arn}"
    port              = "80"
    protocol          = "TCP"

    default_action {
        type = "forward"

    target_group_arn = "${aws_lb_target_group.ICGORE_PERF_APW_NLB_TARGET_GROUP.arn}"
    }
    
}