resource "aws_security_group" "SG_JOB_SCHEDULER" {
  name = "JOB-SCHEDULER-SG"

  ingress {
    from_port       = 4444
    to_port         = 4444
    protocol        = "tcp"
    cidr_blocks = [
      "${var.peel_prod_cidr}",
      "${var.peel_aviatrix_vpn}"
    ]
  }

  ingress {
    from_port       = 4444
    to_port         = 4444
    protocol        = "udp"
    cidr_blocks = [
      "${var.peel_prod_cidr}",
      "${var.peel_aviatrix_vpn}",
    ]
  }

  ingress {
    from_port       = 4446
    to_port         = 4446
    protocol        = "tcp"
    cidr_blocks = [
      "${var.peel_prod_cidr}",
      "${var.peel_aviatrix_vpn}"
    ]
  }

  ingress {
    from_port       = 4446
    to_port         = 4446
    protocol        = "udp"
    cidr_blocks = [
      "${var.peel_prod_cidr}",
      "${var.peel_aviatrix_vpn}"
    ]
  }

  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    Name = "JOB-SCHEDULER-SG"
  }
}

resource "aws_security_group" "SG_MICROSERVICES" {
  name = "MICROSERVICES-SG"

  ingress {
    from_port   = 8443
    to_port     = 8443
    protocol    = "tcp"
    security_groups = [
      "${aws_security_group.SG_EXTERNAL_HAPROXY.id}"
    ]
    description = "Orchestrator"
  }

  ingress {
    from_port   = 15672
    to_port     = 15672
    protocol    = "tcp"
    cidr_blocks = [
      "${var.peel_aviatrix_vpn}"
    ]
    security_groups = [
      "${aws_security_group.SG_INTERNAL_HAPROXY.id}"
    ]
    description = "RabbitMQ Management UI"
  }

  ingress {
    from_port   = 9002
    to_port     = 9002
    protocol    = "tcp"
    security_groups = [
      "${aws_security_group.SG_INTERNAL_HAPROXY.id}"
    ]
    description = "Edge"
  }

  ingress {
    from_port   = 2377
    to_port     = 2377
    protocol    = "tcp"
    cidr_blocks = [
      "${var.peel_prod_cidr}"
    ]
    description = "Cluster Management Communications"
  }

  ingress {
    from_port   = 7946
    to_port     = 7946
    protocol    = "udp"
    cidr_blocks = [
      "${var.peel_prod_cidr}"
    ]
    description = "Communication Among Nodes"
  }

  ingress {
    from_port   = 7946
    to_port     = 7946
    protocol    = "tcp"
    cidr_blocks = [
      "${var.peel_prod_cidr}"
    ]
    description = "Communication Among Nodes"
  }

  ingress {
    from_port   = 4789
    to_port     = 4789
    protocol    = "udp"
    cidr_blocks = [
      "${var.peel_prod_cidr}"
    ]
    description = "Overlay Network Traffic"
  }

  ingress {
    from_port   = 8500
    to_port     = 8500
    protocol    = "tcp"
    cidr_blocks = [
      "${var.peel_aviatrix_vpn}"
    ]
    description = "Consul UI"
  }

  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    Name = "MICROSERVICES-SG"
  }
}


resource "aws_security_group" "SG_MONITORING" {
  name = "MONITORING-SG"

  ingress {
    from_port   = 10000
    to_port     = 10001
    protocol    = "tcp"
    cidr_blocks = [
      "${var.peel_prod_cidr}",
      "${var.peel_aviatrix_vpn}"
    ]
    description = "Prometheus/Alertmanager"
  }

  ingress {
    from_port   = 3000
    to_port     = 3000
    protocol    = "tcp"
    cidr_blocks = [
      "${var.peel_prod_cidr}",
      "${var.peel_aviatrix_vpn}"
    ]
    description = "Grafana"
  }

  ingress {
    from_port   = 19191
    to_port     = 19191
    protocol    = "tcp"
    cidr_blocks = [
      "${var.peel_prod_cidr}",
      "${var.peel_aviatrix_vpn}"
    ]
    description = "Thanos UI"
  }

  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    Name = "MONITORING-SG"
  }
}

resource "aws_security_group" "SG_EXSTREAM" {
  name = "EXSTREAM-SG"

  ingress {
    from_port   = 28700
    to_port     = 28702
    protocol    = "tcp"
    cidr_blocks = [
      "${var.peel_prod_cidr}",
      "${var.peel_hub_cidr}"
    ]
  }

  ingress {
    from_port   = 2701
    to_port     = 2702
    protocol    = "tcp"
    cidr_blocks = [
      "${var.peel_prod_cidr}",
      "${var.peel_hub_cidr}"
    ]
  }

  ingress {
    from_port   = 8080
    to_port     = 8080
    protocol    = "tcp"
    cidr_blocks = [
      "${var.peel_prod_cidr}",
      "${var.peel_hub_cidr}"
    ]
  }

  ingress {
    from_port   = 8443
    to_port     = 8443
    protocol    = "tcp"
    cidr_blocks = [
      "${var.peel_prod_cidr}",
      "${var.peel_hub_cidr}"
    ]
  }

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = [
      "${var.peel_prod_cidr}",
      "${var.peel_hub_cidr}"
    ]
  }

  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = [
      "${var.peel_prod_cidr}",
      "${var.peel_hub_cidr}"
    ]
  }

  ingress {
    from_port   = 5858
    to_port     = 5858
    protocol    = "tcp"
    cidr_blocks = [
      "${var.peel_prod_cidr}",
      "${var.peel_hub_cidr}"
    ]
  }

  ingress {
    from_port   = 3099
    to_port     = 3099
    protocol    = "tcp"
    cidr_blocks = [
      "${var.peel_prod_cidr}",
      "${var.peel_hub_cidr}"
    ]
  }

  ingress {
    from_port   = 2099
    to_port     = 2099
    protocol    = "tcp"
    cidr_blocks = [
      "${var.peel_prod_cidr}",
      "${var.peel_hub_cidr}"
    ]
  }

  ingress {
    from_port   = 389
    to_port     = 389
    protocol    = "tcp"
    cidr_blocks = [
      "${var.peel_prod_cidr}",
      "${var.peel_hub_cidr}"
    ]
  }

  ingress {
    from_port   = 4440
    to_port     = 4440
    protocol    = "tcp"
    cidr_blocks = [
      "${var.peel_prod_cidr}",
      "${var.peel_hub_cidr}"
    ]
  }

  ingress {
    from_port   = 8009
    to_port     = 8009
    protocol    = "tcp"
    cidr_blocks = [
      "${var.peel_prod_cidr}",
      "${var.peel_hub_cidr}"
    ]
  }

  ingress {
    from_port   = 4040
    to_port     = 4040
    protocol    = "tcp"
    cidr_blocks = [
      "${var.peel_prod_cidr}",
      "${var.peel_hub_cidr}"
    ]
  }

  ingress {
    from_port   = 8989
    to_port     = 8989
    protocol    = "tcp"
    cidr_blocks = [
      "${var.peel_prod_cidr}",
      "${var.peel_hub_cidr}"
    ]
  }

  ingress {
    from_port   = 41616
    to_port     = 41616
    protocol    = "tcp"
    cidr_blocks = [
      "${var.peel_prod_cidr}",
      "${var.peel_hub_cidr}"
    ]
  }

  ingress {
    from_port   = 5868
    to_port     = 5869
    protocol    = "tcp"
    cidr_blocks = [
      "${var.peel_prod_cidr}",
      "${var.peel_hub_cidr}"
    ]
  }

  ingress {
    from_port   = 8100
    to_port     = 8100
    protocol    = "tcp"
    cidr_blocks = [
      "${var.peel_prod_cidr}",
      "${var.peel_hub_cidr}"
    ]
  }

  ingress {
    from_port   = 8300
    to_port     = 8300
    protocol    = "tcp"
    cidr_blocks = [
      "${var.peel_prod_cidr}",
      "${var.peel_hub_cidr}"
    ]
  }

  ingress {
    from_port   = 8500
    to_port     = 8500
    protocol    = "tcp"
    cidr_blocks = [
      "${var.peel_prod_cidr}",
      "${var.peel_hub_cidr}"
    ]
  }

  ingress {
    from_port   = 8600
    to_port     = 8603
    protocol    = "tcp"
    cidr_blocks = [
      "${var.peel_prod_cidr}",
      "${var.peel_hub_cidr}"
    ]
  }

  ingress {
    from_port   = 8512
    to_port     = 8515
    protocol    = "tcp"
    cidr_blocks = [
      "${var.peel_prod_cidr}",
      "${var.peel_hub_cidr}"
    ]
  }

  ingress {
    from_port   = 2719
    to_port     = 2719
    protocol    = "tcp"
    cidr_blocks = [
      "${var.peel_prod_cidr}",
      "${var.peel_hub_cidr}"
    ]
  }

  ingress {
    from_port   = 135
    to_port     = 139
    protocol    = "tcp"
    cidr_blocks = [
      "${var.peel_prod_cidr}",
      "${var.peel_hub_cidr}"
    ]
  }

  ingress {
    from_port   = 135
    to_port     = 139
    protocol    = "udp"
    cidr_blocks = [
      "${var.peel_prod_cidr}",
      "${var.peel_hub_cidr}"
    ]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    Name = "EXSTREAM-SG"
  }
}

resource "aws_security_group" "SG_EXTERNAL_HAPROXY" {
  name = "EXTERNAL-HAPROXY-SG"

  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = [
      "${var.peel_hub_cidr}"
    ]
    description = "HTTPS"
  }

  ingress {
    from_port   = 8444
    to_port     = 8444
    protocol    = "tcp"
    cidr_blocks = [
      "${var.peel_prod_cidr}"
    ]
    description = "Health"
  }

  ingress {
    from_port   = 8404
    to_port     = 8404
    protocol    = "tcp"
    cidr_blocks = [
      "${var.peel_prod_cidr}",
      "${var.peel_aviatrix_vpn}"
    ]
    description = "Stats"
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    Name = "EXTERNAL-HAPROXY-SG"
  }
}

resource "aws_security_group" "SG_INTERNAL_HAPROXY" {
  name = "INTERNAL-HAPROXY-SG"

  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = [
      "${var.peel_prod_cidr}",
      "${var.peel_aviatrix_vpn}",
      "${var.peel_hub_cidr}", # API Gateway
      "${var.peel_cidr}",
      "${var.peel_watchguard_cidr}",
      "${var.peel_dmz_cidr}",
      "${var.peel_wireless_cidr}"
    ]
    description = "HTTPS"
  }

  ingress {
    from_port   = 8444
    to_port     = 8444
    protocol    = "tcp"
    cidr_blocks = [
      "${var.peel_prod_cidr}",
      "${var.peel_hub_cidr}"
    ]
    description = "Health"
  }

  ingress {
    from_port   = 8404
    to_port     = 8404
    protocol    = "tcp"
    cidr_blocks = [
      "${var.peel_prod_cidr}",
      "${var.peel_aviatrix_vpn}"
    ]
    description = "Stats"
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    Name = "INTERNAL-HAPROXY-SG"
  }
}

resource "aws_security_group" "SG_HAPROXY_INTERNAL_PEERING" {
  name = "HAPROXY-INTERNAL-PEERING-SG"

  ingress {
    from_port       = 1024
    to_port         = 1024
    protocol        = "tcp"
    security_groups = ["${aws_security_group.SG_INTERNAL_HAPROXY.id}"]
    description     = "HAProxy Peering"
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    Name = "HAPROXY-INTERNAL-PEERING-SG"
  }
}

resource "aws_security_group" "SG_HAPROXY_EXTERNAL_PEERING" {
  name = "HAPROXY-EXTERNAL-PEERING-SG"

  ingress {
    from_port       = 1024
    to_port         = 1024
    protocol        = "tcp"
    security_groups = ["${aws_security_group.SG_EXTERNAL_HAPROXY.id}"]
    description     = "HAProxy Peering"
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    Name = "HAPROXY-EXTERNAL-PEERING-SG"
  }
}

resource "aws_security_group" "SG_GUIDEWIRE" {
  name = "GUIDEWIRE-SG"

  ingress {
    from_port   = 8080
    to_port     = 8083
    protocol    = "tcp"
    security_groups = [
      "${aws_security_group.SG_INTERNAL_HAPROXY.id}"
    ]

    description = "HTTP - Internal"
  }

  ingress {
    from_port   = 8080
    to_port     = 8080
    protocol    = "tcp"
    security_groups = [
      "${aws_security_group.SG_EXTERNAL_HAPROXY.id}"
    ]

    description = "HTTP - External"
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    Name = "GUIDEWIRE-SG"
  }
}

resource "aws_security_group" "SG_GUIDEWIRE_CLUSTER_NBIO_EVENT_QUEUE" {
  name = "GUIDEWIRE-CLUSTER-NBIO-EVENT-QUEUE-SG"

  ingress {
    from_port   = 40000
    to_port     = 40003
    protocol    = "tcp"
    security_groups = ["${aws_security_group.SG_GUIDEWIRE.id}"]
    description = "Cluster NBIO event queue"
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    Name = "GUIDEWIRE-CLUSTER-NBIO-EVENT-QUEUE-SG"
  }
}

resource "aws_security_group" "SG_JENKINS_GUIDEWIRE_HEALTH_CHECK" {
  name = "JENKINS-GUIDEWIRE-HEALTH-CHECK-SG"

  ingress {
    from_port   = 8080
    to_port     = 8083
    protocol    = "tcp"
    security_groups = [
      "${aws_security_group.SG_JENKINS_CLIENT.id}"
    ]
    description = "Dev Jenkins Client Health Check"
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    Name = "JENKINS-GUIDEWIRE-HEALTH-CHECK-SG"
  }
}

resource "aws_security_group" "SG_MSSQL_DB" {
  name = "MSSQL-DB-SG"

  ingress {
    from_port   = 1433
    to_port     = 1433
    protocol    = "tcp"
    cidr_blocks = [
      "${var.peel_prod_cidr}",
      "${var.peel_aviatrix_vpn}"
    ]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    Name = "MSSQL-DB-SG"
  }
}

resource "aws_security_group" "SG_PEEL_MSSQL_DB" {
  name = "PEEL-MSSQL-DB-SG"

  ingress {
    from_port   = 1433
    to_port     = 1433
    protocol    = "tcp"
    cidr_blocks = [
      "${var.peel_cidr}",
      "${var.peel_wireless_cidr}",
      "${var.peel_dmz_cidr}"
    ]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    Name = "PEEL-MSSQL-DB-SG"
  }
}

resource "aws_security_group" "SG_CLIENT_CONVERSION_GW_SQL_DB" {
  name = "CLIENT-CONVERSION-GW-SQL-DB-SG"

  ingress {
    from_port   = 1433
    to_port     = 1433
    protocol    = "tcp"
    cidr_blocks = [
      "10.10.10.24/32" # PEEL-HUBIO03
    ]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    Name = "CLIENT-CONVERSION-GW-SQL-DB-SG"
  }
}

resource "aws_security_group" "SG_OT_MSSQL_DB" {
  name = "OT-MSSQL-DB-SG"

  ingress {
    from_port   = 1433
    to_port     = 1433
    protocol    = "tcp"
    cidr_blocks = [
      "${var.peel_prod_cidr}",
      "${var.peel_aviatrix_vpn}"
    ]
  }

  ingress {
    from_port   = 1434
    to_port     = 1434
    protocol    = "udp"
    cidr_blocks = [
      "${var.peel_prod_cidr}",
      "${var.peel_aviatrix_vpn}"
    ]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    Name = "OT-MSSQL-DB-SG"
  }
}

resource "aws_security_group" "SG_POSTGRES_DB" {
  name = "POSTGRES-DB-SG"

  ingress {
    from_port   = 5432
    to_port     = 5432
    protocol    = "tcp"
    cidr_blocks = [
      "${var.peel_prod_cidr}",
      "${var.peel_aviatrix_vpn}"
    ]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    Name = "POSTGRES-DB-SG"
  }
}

resource "aws_security_group" "SG_MYSQL_DB" {
  name = "MYSQL-DB-SG"

  ingress {
    from_port   = 3306
    to_port     = 3306
    protocol    = "tcp"
    cidr_blocks = [
      "${var.peel_prod_cidr}",
      "${var.peel_aviatrix_vpn}"
    ]
  }

  ingress {
    from_port   = 3301
    to_port     = 3301
    protocol    = "tcp"
    cidr_blocks = [
      "${var.peel_prod_cidr}",
      "${var.peel_aviatrix_vpn}"
    ]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    Name = "MYSQL-DB-SG"
  }
}

resource "aws_security_group" "SG_CLIENT_PING_TEST" {
  name = "CLIENT-PING-TEST"

  ingress {
    from_port       = -1
    to_port         = -1
    protocol        = "icmp"
    cidr_blocks     = [
      "${var.peel_cidr}",
      "${var.peel_watchguard_cidr}",
      "${var.peel_dmz_cidr}",
      "${var.peel_wireless_cidr}"
    ]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    Name = "CLIENT-PING-TEST"
  }
}

resource "aws_security_group" "SG_SAP" {
  name = "SAP-SG"

  ingress {
    from_port       = 4444
    to_port         = 4444
    protocol        = "tcp"
    cidr_blocks = [
      "${var.peel_prod_cidr}",
      "${var.peel_aviatrix_vpn}"
    ]
  }

  ingress {
    from_port       = 4444
    to_port         = 4444
    protocol        = "udp"
    cidr_blocks = [
      "${var.peel_prod_cidr}",
      "${var.peel_aviatrix_vpn}"
    ]
  }

  ingress {
    from_port       = 4446
    to_port         = 4446
    protocol        = "tcp"
    cidr_blocks = [
      "${var.peel_prod_cidr}",
      "${var.peel_aviatrix_vpn}"
    ]
  }

  ingress {
    from_port       = 4446
    to_port         = 4446
    protocol        = "udp"
    cidr_blocks = [
      "${var.peel_prod_cidr}",
      "${var.peel_aviatrix_vpn}"
    ]
  }

  ingress {
    from_port       = 8080
    to_port         = 8080
    protocol        = "tcp"
    cidr_blocks = [
      "${var.peel_prod_cidr}",
      "${var.peel_aviatrix_vpn}"
    ]
  }

  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    "Name" = "SAP-SG"
  }
}

resource "aws_security_group" "SG_HUBIO" {
  name = "HUBIO-SG"

  ingress {
    from_port   = 8080
    to_port     = 8080
    protocol    = "tcp"
    cidr_blocks = [
      "${var.peel_prod_cidr}",
      "${var.peel_aviatrix_vpn}"
    ]
  }

  ingress {
    from_port   = 9000
    to_port     = 9006
    protocol    = "tcp"
    cidr_blocks = [
      "${var.peel_prod_cidr}",
      "${var.peel_aviatrix_vpn}"
    ]
  }

  ingress {
    from_port   = 4200
    to_port     = 4200
    protocol    = "tcp"
    cidr_blocks = [
      "${var.peel_prod_cidr}",
      "${var.peel_aviatrix_vpn}"
    ]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    Name = "HUBIO-SG"
  }
}

resource "aws_security_group" "SG_QLIK_SENSE" {
  name = "PEEL-QLIK-SENSE-SG"

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = [
      "${var.peel_prod_cidr}",
      "${var.peel_aviatrix_vpn}",
      "${var.peel_cidr}",
      "${var.peel_dmz_cidr}",
      "${var.peel_wireless_cidr}"
    ]
  }

  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = [
      "${var.peel_prod_cidr}",
      "${var.peel_aviatrix_vpn}",
      "${var.peel_cidr}",
      "${var.peel_dmz_cidr}",
      "${var.peel_wireless_cidr}"
    ]
  }

  ingress {
    from_port   = 4244
    to_port     = 4244
    protocol    = "tcp"
    cidr_blocks = [
      "${var.peel_prod_cidr}",
      "${var.peel_aviatrix_vpn}",
      "${var.peel_cidr}",
      "${var.peel_dmz_cidr}",
      "${var.peel_wireless_cidr}"
    ]
  }

  ingress {
    from_port   = 4248
    to_port     = 4248
    protocol    = "tcp"
    cidr_blocks = [
      "${var.peel_prod_cidr}",
      "${var.peel_aviatrix_vpn}",
      "${var.peel_cidr}",
      "${var.peel_dmz_cidr}",
      "${var.peel_wireless_cidr}"
    ]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    Name = "PEEL-QLIK-SENSE-SG"
  }
}

resource "aws_security_group" "SG_AVIATRIX_RDP" {
  name = "AVIATRIX-RDP-SG"

  ingress {
    from_port   = 3389
    to_port     = 3389
    protocol    = "tcp"
    cidr_blocks = [
      "${var.peel_aviatrix_vpn}"
    ]
    description = "RDP"
  }

  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    Name = "AVIATRIX-RDP-SG"
  }
}

resource "aws_security_group" "SG_AVIATRIX_SSH" {
  name = "AVIATRIX-SSH-SG"

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = [
      "${var.peel_aviatrix_vpn}"
    ]
    description = "SSH"
  }

  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    Name = "AVIATRIX-SSH-SG"
  }
}

resource "aws_security_group" "SG_JOBSCHEDULER_SSH" {
  name = "JOBSCHEDULER-SSH-SG"
 
 ingress {
    from_port       = 22
    to_port         = 22
    protocol        = "tcp"
    security_groups = [
      "${aws_security_group.SG_JOB_SCHEDULER.id}"
    ]
  }

  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    Name = "JOBSCHEDULER-SSH-SG"
  }
}

resource "aws_security_group" "SG_DEV_JOBSCHEDULER_SSH" {
  name = "DEV-JOBSCHEDULER-SSH-SG"
 
 ingress {
    from_port       = 22
    to_port         = 22
    protocol        = "tcp"
    cidr_blocks = [
      "${var.peel_dev_jobscheduler}" # change to Dev Jobscheduler IP when it is in the Hub
    ]
  }

  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    Name = "DEV-JOBSCHEDULER-SSH-SG"
  }
}

resource "aws_security_group" "SG_JENKINS_CLIENT" {
  name = "JENKINS-CLIENT-SG"

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = [
      "10.0.0.0/8"
    ]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    Name = "JENKINS-CLIENT-SG"
  }
}

resource "aws_security_group" "SG_JENKINS_CLIENT_SSH" {
  name = "JENKINS-CLIENT-SSH-SG"
 
  ingress {
    from_port       = 22
    to_port         = 22
    protocol        = "tcp"
    security_groups = [
      "${aws_security_group.SG_JENKINS_CLIENT.id}"
    ]
    description = "Jenkins Client SSH"
  }

  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    Name = "JENKINS-CLIENT-SSH-SG"
  }
}

resource "aws_security_group" "SG_LAMBDA" {
  name = "LAMBDA-FUNCTION-SG"
 
  ingress {
    from_port       = 0
    to_port         = 65535
    protocol        = "tcp"
    cidr_blocks     = [
      "${var.peel_hub_cidr}"
    ]
    description = "API Gateway from the hub"
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    Name = "LAMBDA-FUNCTION-SG"
  }
}