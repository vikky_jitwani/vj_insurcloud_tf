variable "client_name" {
  description = "AWS client name for resources (Ex: MASTERCLOUD)"
  default     = ""
}

variable "environment_name" {
  description = "AWS environment name for resources (Ex: NON-PROD)"
  default     = ""
}

variable "environment_type" {
  description = "AWS environment type for resources (Ex: P=PROD/HUB, D=NON-PROD)"
  default     = ""
}

variable "client_number" {
  description = "AWS client number for tagging (Ex: A001)"
  default     = "A005"
}

variable "region" {
  description = "AWS region for hosting our your network"
  default     = "ca-central-1"
}

variable "volume_key" {
  description = "KMS volume key dev account"
  default     = ""
}

variable "ami_id_windows2012sql14enterprise" {
  description = "Windows 2012 SQL 2014 Enterprise Encypted Root Volume AMI"
  default     = ""
}

variable "ami_id_windows2019container" {
  description = "Windows 2019 Container Encypted Root Volume AMI"
  default     = ""
}

variable "ami_id_windows2019base" {
  description = "Windows 2019 Base Encypted Root Volume AMI"
  default     = ""
}

variable "ami_id_windows2016base" {
  description = "Windows 2016 Base Encypted Root Volume AMI"
  default     = ""
}

variable "ami_id_windows2012R2base" {
  description = "Windows 2012 R2 Base Encypted Root Volume AMI"
  default     = ""
}

variable "ami_id_windows2008base" {
  description = "Windows 2008 Base Encypted Root Volume AMI"
  default     = ""
}

variable "ami_id_redhatlinux75" {
  description = "Redhat Linux 7.5 Encypted Root Volume AMI"
  default     = ""
}

variable "ami_id_ubuntu1804baseimage" {
  description = "Ubuntu 18.04 Base Encypted Root Volume AMI"
  default     = ""
}

variable "ami_id_ubuntu1604baseimage" {
  description = "Ubuntu 16.04 Base Encypted Root Volume AMI"
  default     = ""
}

variable "ami_id_amazonlinux201712base" {
  description = "Amazon Linux 2017 12 Base Encypted Root Volume AMI"
  default     = ""
}

variable "ami_id_windowsdhicsap" {
  description = "Snapshot of the DHIC SAP server"
  default     = ""
}

variable "ami_id_windowsdhicdb" {
  description = "Snapshot of the DHIC DB server"
  default     = ""
}

variable "ami_id_amazonlinux201709base" {
  description = "Amazon Linux 2017 09 Base Encypted Root Volume AMI"
  default     = ""
}

variable "ami_id_windows2016base20200806" {
  description = "Updated Windows 2016 Base Encypted Root Volume AMI 06 August 2020"
  default     = ""
}

variable "ami_id_amazonlinuxbase20200806" {
  description = "Updated Amazon Linux 2 Base Encypted Root Volume AMI 6 August 2020"
  default     = ""
}

variable "ami_id_amazonlinux2base" {
  description = "Amazon Linux 2 Base Encypted Root Volume AMI"
  default     = ""
}

variable "ami_id_amazonlinux201803base" {
  description = "Amazon Linux 2018 03 Base Encypted Root Volume AMI"
  default     = ""
}

variable "ami_id_amazonlinuxbase20200506" {
  description = "CMS CIS Hardened Amazon Linux with Docker 19 and Encrypted Root Volume AMI"
  default     = ""
}

variable "vpc_id" {
  description = "VPC id for DEV account"
  default     = "vpc-0d721640c30de46fa"
}

variable "vpc_cidr" {
  description = "VPC cidr dev account"
  default     = ""
}

variable "portage-dev-database-1a-subnet" {
  description = "Database subnet one"
  default     = ""
}

variable "portage-dev-database-1b-subnet" {
  description = "Database subnet two"
  default     = ""
}

variable "portage-dev-private-1a-subnet" {
  description = "Private Subnet One"
  default     = ""
}

variable "portage-dev-private-1b-subnet" {
  description = "Private Subnet Two"
  default     = ""
}

variable "mssql-rds-password" {
  default = "Toronto12345"
}

variable "mysql-rds-password" {
  default = "Toronto12345"
}

variable "postgres-rds-password" {
  default = "Toronto12345"
}

variable "zone_id" {
  description = "default zone id"
  default     = ""
}

variable "ad_dns1" {
  default = "10.171.0.80"
}

variable "ad_dns2" {
  default = "10.171.1.41"
}

variable "ad_dns3" {
  default = "10.171.0.2"
}

variable "ou_name" {
  default = "dev"
}

variable "pwordparameter" {
  default = "domain_password"
}

variable "unameparameter" {
  default = "domain_username"
}

variable "ad_domain" {
  default = "internal.portage.insurcloud.ca"
}

data "aws_caller_identity" "current" {}

variable "portage-dev-account" {
  default = "091072499599"
}

variable "portage-prod-account" {
  default = "625725589452"
}

variable "portage-hub-account" {
  default = "342339668068"
}

variable "default_cidr" {
  description = "default cidr used"

  default = [
    "10.0.0.0/8",
  ]
}

variable "portage_cidr" {
  default = "10.171.0.0/23"
}

variable "portage_dev_cidr" {
  description = "default cidr used"
  type        = "list"

  default = [
    "10.171.8.0/22",
  ]
}

variable "portage_prod_cidr" {
  description = "default cidr used"
  type        = "list"

  default = [
    "10.171.4.0/22",
  ]
}

variable "portage_hub_cidr" {
  description = "default cidr used"
  type        = "list"

  default = [
    "10.171.0.0/22",
  ]
}

variable "portage_dev01_microservices" {
  description = "default cidr used"
  type        = "list"

  default = [
    "10.171.8.171/32",
  ]
}

variable "portage-hub_private_cidr" {
  description = "default cidr used"
  type        = "list"

  default = [
    "10.171.0.0/24",
    "10.171.1.0/24",
  ]
}

variable "portage_aviatrix_vpn" {
  default = "10.171.2.61/32"
}

variable "portage_dhic_sap" {
  default = "10.171.8.11/32"
}

variable "portage_corp_cidrs" {
  description = "default portage corporate cidrs"
  type        = "list"

  default = [
    "10.45.0.0/16",
    "10.134.0.0/16",
    "10.253.243.0/24",
    "10.253.242.0/24",
    "10.137.0.0/16",
    "10.70.0.0/16",
    "10.136.0.0/16",
    "10.30.0.0/16",
    "10.40.0.0/16",
    "10.128.0.0/16",
    "10.192.0.0/16",
    "10.20.0.0/16",
    "10.132.0.0/16",
    "10.129.0.0/16",
    "10.193.0.0/16",
    "10.131.0.0/16",
    "10.170.0.0/16",
  ]
}

variable "target_group_sticky" {
  default = true
}

variable "portage_dev_lb_vpc_sg" {
  default = "portage_dev_lb_vpc_sg"
}

variable "portage_dev02_microservices_ip" {
  default = "10.171.8.64/32"
}

variable "ami_id_windows2016base20201216_win_ec2" {
  description = "Updated Windows 2016 Base Encypted Root Volume AMI 06 August 2020"
  default     = ""
}

variable "ami_id_windows2016base20201216_mssql_win_ec2" {
  description = "Updated Windows 2016 Base Encypted Root Volume AMI 06 August 2020"
  default     = ""
}

variable "portage_workspaces" {
  description = "portage aws workspaces"
  type        = "list"

  default = [
    "10.171.3.193/32",
    "10.171.3.147/32",
    "10.171.3.149/32",
    "10.171.2.139/32"
  ]
}

variable "ami_id_windows_base" {
  description = "Base image for Windows Server"
  default     = ""
}

variable "ami_id_windows_mssql_base" {
  description = "Base image for Windows MS Sql Server"
  default     = ""
}

variable "data_and_reporting_workspaces" {
  description = "data and reporting workspaces"
  type        = "list"

  default = [
    "10.171.2.182/32",
    "10.171.3.173/32",
    "10.171.2.221/32",
    "10.171.2.253/32",
    "10.171.2.177/32",
    "10.171.2.202/32",
    "10.171.2.248/32",
    "10.171.2.183/32"
  ]
}

variable "dev02-gw-instance-id" {
  default = "i-0519bcf4357913efd"
}

variable "nonpro-alb-segurity-group-id" {
  default = "sg-0fedbf3397fd5ecb8"
}