/*

#### JOB SCHEDULER DB #####
resource "aws_db_subnet_group" "MYSQL_DB_SUBNET" {
    name        = "mysql-db-subnet"
    description = "RDS subnet group"
    subnet_ids  = ["${var.portage-dev-database-1a-subnet}", "${var.portage-dev-database-1b-subnet}"]
}

# Microservices Database
resource "aws_db_parameter_group" "microservices" {
  name   = "portagemicroservices"
  family = "mysql8.0"

  parameter {
    name  = "time_zone"
    value = "US/Eastern" # MySQL has no America/Toronto time zone
  }

  parameter {
    name = "log_output"
    value = "FILE"
  }

  parameter {
    name = "general_log"
    value = "1"
  }

  parameter {
    name = "slow_query_log"
    value = "1"
  }

  parameter {
    name = "long_query_time"
    value = "2"
  }
}

resource "aws_db_instance" "microservicesmysqldb" {

    engine            = "mysql"
    engine_version    = "8.0.17"
    instance_class    = "db.t3.medium"
    allocated_storage = 100
    storage_encrypted = true
    identifier        = "portageppsmicroservices"
    name              = "portageppsmicroservices"
    username          = "sa"
    password          = "${var.mysql-rds-password}" # password
    port              = 3306

    maintenance_window = "Mon:00:00-Mon:03:00"
    backup_window      = "03:00-06:01"
    deletion_protection = true
    enabled_cloudwatch_logs_exports = ["general", "error", "slowquery"]
    apply_immediately  = true

    db_subnet_group_name  = "${aws_db_subnet_group.MYSQL_DB_SUBNET.name}"
    parameter_group_name  = "${aws_db_parameter_group.microservices.id}"
    multi_az              = false # set to true to have high availability: 2 instances synchronized with each other
    vpc_security_group_ids  = ["${aws_security_group.SG_MYSQL_DB.id}"]
    storage_type            = "gp2"
    backup_retention_period = 30 # how long you’re going to keep your backups
    kms_key_id = "arn:aws:kms:ca-central-1:091072499599:key/9fc4bcc6-13f4-4779-8eb6-2291b5c4a586"

    tags = {
        Name = "portage-pps-microservices"
        Environment = "PPS"
    }
}

# JobScheduler Database
resource "aws_db_parameter_group" "jobscheduler" {
  name   = "portagejobscheduler"
  family = "mysql8.0"

  parameter {
    name  = "time_zone"
    value = "US/Eastern" # MySQL has no America/Toronto time zone
  }

  parameter {
    name = "log_output"
    value = "FILE"
  }

  parameter {
    name = "general_log"
    value = "1"
  }

  parameter {
    name = "slow_query_log"
    value = "1"
  }

  parameter {
    name = "long_query_time"
    value = "2"
  }
}

resource "aws_db_instance" "mysqldb" {

    engine            = "mysql"
    engine_version    = "8.0.17"
    instance_class    = "db.t3.small"
    allocated_storage = 100
    storage_encrypted = true
    identifier        = "portageppsjobscheduler"
    name              = "portageppsjobscheduler"
    username          = "sa"
    password          = "${var.mysql-rds-password}" # password
    port              = 3306

    maintenance_window = "Mon:00:00-Mon:03:00"
    backup_window      = "03:00-06:00"
    deletion_protection = true
    enabled_cloudwatch_logs_exports = ["general", "error", "slowquery"]
    apply_immediately   = true

    db_subnet_group_name  = "${aws_db_subnet_group.MYSQL_DB_SUBNET.name}"
    parameter_group_name  = "${aws_db_parameter_group.jobscheduler.id}"
    multi_az              = false # set to true to have high availability: 2 instances synchronized with each other
    vpc_security_group_ids  = ["${aws_security_group.SG_MYSQL_DB.id}"]
    storage_type            = "gp2"
    backup_retention_period = 30 # how long you’re going to keep your backups
    kms_key_id = "arn:aws:kms:ca-central-1:091072499599:key/9fc4bcc6-13f4-4779-8eb6-2291b5c4a586"

    tags = {
        Name = "portage-pps-jobscheduler"
        Environment = "PPS"
    }
}

*/

