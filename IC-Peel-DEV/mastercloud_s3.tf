resource "aws_s3_bucket" "ICPEEL_DEV_MS_BMS_INITIAL_LOAD" {
  bucket = "peel-microservices-bms-initial-load-dev"
  acl = "private"

  lifecycle_rule {
    enabled = true

    transition {
      days = 30
      storage_class = "STANDARD_IA"
    }

  }
  
  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm     = "aws:kms"
        kms_master_key_id = "arn:aws:kms:ca-central-1:005810662120:key/fc48cb18-60b3-4578-a32e-b67c774a63dd"
      }
    }
  }
  
  tags {
    Name = "peel-microservices-bms-initial-load-dev"
  }
}

resource "aws_s3_bucket_policy" "ICPEEL_DEV_MS_BMS_INITIAL_LOAD_BUCKET_POLICY" {
  bucket = "${aws_s3_bucket.ICPEEL_DEV_MS_BMS_INITIAL_LOAD.id}"

  policy = <<POLICY
{
    "Version": "2012-10-17",
    "Id": "ICPEEL_DEV_MS_BMS_INITIAL_LOAD",
    "Statement": [
        {
            "Sid": "DenyPublicReadACL",
            "Effect": "Deny",
            "Principal": {
                "AWS": "*"
            },
            "Action": [
                "s3:PutObject",
                "s3:PutObjectAcl"
            ],
            "Resource": "arn:aws:s3:::${aws_s3_bucket.ICPEEL_DEV_MS_BMS_INITIAL_LOAD.id}/*",
            "Condition": {
                "StringEquals": {
                    "s3:x-amz-acl": [
                        "public-read",
                        "public-read-write",
                        "authenticated-read"
                    ]
                }
            }
        },
        {
            "Sid": "DenyPublicReadGrant",
            "Effect": "Deny",
            "Principal": {
                "AWS": "*"
            },
            "Action": [
                "s3:PutObject",
                "s3:PutObjectAcl"
            ],
            "Resource": "arn:aws:s3:::${aws_s3_bucket.ICPEEL_DEV_MS_BMS_INITIAL_LOAD.id}/*",
            "Condition": {
                "StringLike": {
                    "s3:x-amz-grant-read": [
                        "*http://acs.amazonaws.com/groups/global/AllUsers*",
                        "*http://acs.amazonaws.com/groups/global/AuthenticatedUsers*"
                    ]
                }
            }
        },
        {
            "Sid": "DenyPublicListACL",
            "Effect": "Deny",
            "Principal": {
                "AWS": "*"
            },
            "Action": "s3:PutBucketAcl",
            "Resource": "arn:aws:s3:::${aws_s3_bucket.ICPEEL_DEV_MS_BMS_INITIAL_LOAD.id}",
            "Condition": {
                "StringEquals": {
                    "s3:x-amz-acl": [
                        "public-read",
                        "public-read-write",
                        "authenticated-read"
                    ]
                }
            }
        },
        {
            "Sid": "DenyPublicListGrant",
            "Effect": "Deny",
            "Principal": {
                "AWS": "*"
            },
            "Action": "s3:PutBucketAcl",
            "Resource": "arn:aws:s3:::${aws_s3_bucket.ICPEEL_DEV_MS_BMS_INITIAL_LOAD.id}",
            "Condition": {
                "StringLike": {
                    "s3:x-amz-grant-read": [
                        "*http://acs.amazonaws.com/groups/global/AllUsers*",
                        "*http://acs.amazonaws.com/groups/global/AuthenticatedUsers*"
                    ]
                }
            }
        },
        {
            "Sid": "DenyPublicReadConsole",
            "Effect": "Deny",
            "Principal": {
                "AWS": "*"
            },
            "Action": [
                "s3:PutObjectAcl",
                "s3:PutBucketAcl"
            ],
            "Resource": [
                "arn:aws:s3:::${aws_s3_bucket.ICPEEL_DEV_MS_BMS_INITIAL_LOAD.id}/*",
                "arn:aws:s3:::${aws_s3_bucket.ICPEEL_DEV_MS_BMS_INITIAL_LOAD.id}"
            ],
            "Condition": {
                "Null": {
                    "s3:x-amz-grant-write": "true",
                    "s3:x-amz-grant-read": "true",
                    "s3:x-amz-acl": "true",
                    "s3:x-amz-grant-read-acp": "true",
                    "s3:x-amz-grant-full-control": "true",
                    "s3:x-amz-grant-write-acp": "true"
                }
            }
        },
        {
            "Sid": "InstanceListBucketAllow",
            "Effect": "Allow",
            "Principal": {
                "AWS": "arn:aws:iam::${var.peel-dev-account}:user/peel-application-user"
            },
            "Action": "s3:ListBucket",
            "Resource": "arn:aws:s3:::${aws_s3_bucket.ICPEEL_DEV_MS_BMS_INITIAL_LOAD.id}"
        },
        {
            "Sid": "InstanceObjectAllow",
            "Effect": "Allow",
            "Principal": {
                "AWS": "arn:aws:iam::${var.peel-dev-account}:user/peel-application-user"
            },
            "Action": [
                "s3:GetObject",
                "s3:PutObject",
                "s3:DeleteObject"
            ],
            "Resource": "arn:aws:s3:::${aws_s3_bucket.ICPEEL_DEV_MS_BMS_INITIAL_LOAD.id}/*"
        }
    ]
}
POLICY
}

resource "aws_s3_bucket" "ICPEEL_DEV_MS_DOCUMENTS" {
  bucket = "peel-microservices-documents-dev"
  acl = "private"

  lifecycle_rule {
    enabled = true

    transition {
      days = 30
      storage_class = "STANDARD_IA"
    }

  }
  
  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm     = "aws:kms"
        kms_master_key_id = "arn:aws:kms:ca-central-1:005810662120:key/fc48cb18-60b3-4578-a32e-b67c774a63dd"
      }
    }
  }
  
  tags {
    Name = "peel-microservices-documents-dev"
  }
}

resource "aws_s3_bucket_policy" "ICPEEL_DEV_MS_DOCUMENTS_BUCKET_POLICY" {
  bucket = "${aws_s3_bucket.ICPEEL_DEV_MS_DOCUMENTS.id}"

  policy = <<POLICY
{
    "Version": "2012-10-17",
    "Id": "ICPEEL_DEV_MS_DOCUMENTS",
    "Statement": [
        {
            "Sid": "DenyPublicReadACL",
            "Effect": "Deny",
            "Principal": {
                "AWS": "*"
            },
            "Action": [
                "s3:PutObject",
                "s3:PutObjectAcl"
            ],
            "Resource": "arn:aws:s3:::${aws_s3_bucket.ICPEEL_DEV_MS_DOCUMENTS.id}/*",
            "Condition": {
                "StringEquals": {
                    "s3:x-amz-acl": [
                        "public-read",
                        "public-read-write",
                        "authenticated-read"
                    ]
                }
            }
        },
        {
            "Sid": "DenyPublicReadGrant",
            "Effect": "Deny",
            "Principal": {
                "AWS": "*"
            },
            "Action": [
                "s3:PutObject",
                "s3:PutObjectAcl"
            ],
            "Resource": "arn:aws:s3:::${aws_s3_bucket.ICPEEL_DEV_MS_DOCUMENTS.id}/*",
            "Condition": {
                "StringLike": {
                    "s3:x-amz-grant-read": [
                        "*http://acs.amazonaws.com/groups/global/AllUsers*",
                        "*http://acs.amazonaws.com/groups/global/AuthenticatedUsers*"
                    ]
                }
            }
        },
        {
            "Sid": "DenyPublicListACL",
            "Effect": "Deny",
            "Principal": {
                "AWS": "*"
            },
            "Action": "s3:PutBucketAcl",
            "Resource": "arn:aws:s3:::${aws_s3_bucket.ICPEEL_DEV_MS_DOCUMENTS.id}",
            "Condition": {
                "StringEquals": {
                    "s3:x-amz-acl": [
                        "public-read",
                        "public-read-write",
                        "authenticated-read"
                    ]
                }
            }
        },
        {
            "Sid": "DenyPublicListGrant",
            "Effect": "Deny",
            "Principal": {
                "AWS": "*"
            },
            "Action": "s3:PutBucketAcl",
            "Resource": "arn:aws:s3:::${aws_s3_bucket.ICPEEL_DEV_MS_DOCUMENTS.id}",
            "Condition": {
                "StringLike": {
                    "s3:x-amz-grant-read": [
                        "*http://acs.amazonaws.com/groups/global/AllUsers*",
                        "*http://acs.amazonaws.com/groups/global/AuthenticatedUsers*"
                    ]
                }
            }
        },
        {
            "Sid": "DenyPublicReadConsole",
            "Effect": "Deny",
            "Principal": {
                "AWS": "*"
            },
            "Action": [
                "s3:PutObjectAcl",
                "s3:PutBucketAcl"
            ],
            "Resource": [
                "arn:aws:s3:::${aws_s3_bucket.ICPEEL_DEV_MS_DOCUMENTS.id}/*",
                "arn:aws:s3:::${aws_s3_bucket.ICPEEL_DEV_MS_DOCUMENTS.id}"
            ],
            "Condition": {
                "Null": {
                    "s3:x-amz-grant-write": "true",
                    "s3:x-amz-grant-read": "true",
                    "s3:x-amz-acl": "true",
                    "s3:x-amz-grant-read-acp": "true",
                    "s3:x-amz-grant-full-control": "true",
                    "s3:x-amz-grant-write-acp": "true"
                }
            }
        },
        {
            "Sid": "InstanceListBucketAllow",
            "Effect": "Allow",
            "Principal": {
                "AWS": "arn:aws:iam::${var.peel-dev-account}:user/peel-application-user"
            },
            "Action": "s3:ListBucket",
            "Resource": "arn:aws:s3:::${aws_s3_bucket.ICPEEL_DEV_MS_DOCUMENTS.id}"
        },
        {
            "Sid": "InstanceObjectAllow",
            "Effect": "Allow",
            "Principal": {
                "AWS": "arn:aws:iam::${var.peel-dev-account}:user/peel-application-user"
            },
            "Action": [
                "s3:GetObject",
                "s3:PutObject",
                "s3:DeleteObject"
            ],
            "Resource": "arn:aws:s3:::${aws_s3_bucket.ICPEEL_DEV_MS_DOCUMENTS.id}/*"
        }
    ]
}
POLICY
}

resource "aws_s3_bucket" "ICPEEL_DEV01_MS_EFT" {
  bucket = "peel-microservices-eft-dev01"
  acl = "private"

  lifecycle_rule {
    enabled = true

    transition {
      days = 30
      storage_class = "STANDARD_IA"
    }

  }

  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm     = "aws:kms"
        kms_master_key_id = "arn:aws:kms:ca-central-1:005810662120:key/fc48cb18-60b3-4578-a32e-b67c774a63dd"
      }
    }
  }
  
  tags {
    Name = "peel-microservices-documents-dev01"
  }
}

resource "aws_s3_bucket_policy" "ICPEEL_DEV01_MS_EFT_BUCKET_POLICY" {
  bucket = "${aws_s3_bucket.ICPEEL_DEV01_MS_EFT.id}"

  policy = <<POLICY
{
    "Version": "2012-10-17",
    "Id": "ICPEEL_DEV01_MS_EFT",
    "Statement": [
        {
            "Sid": "DenyPublicReadACL",
            "Effect": "Deny",
            "Principal": {
                "AWS": "*"
            },
            "Action": [
                "s3:PutObject",
                "s3:PutObjectAcl"
            ],
            "Resource": "arn:aws:s3:::${aws_s3_bucket.ICPEEL_DEV01_MS_EFT.id}/*",
            "Condition": {
                "StringEquals": {
                    "s3:x-amz-acl": [
                        "public-read",
                        "public-read-write",
                        "authenticated-read"
                    ]
                }
            }
        },
        {
            "Sid": "DenyPublicReadGrant",
            "Effect": "Deny",
            "Principal": {
                "AWS": "*"
            },
            "Action": [
                "s3:PutObject",
                "s3:PutObjectAcl"
            ],
            "Resource": "arn:aws:s3:::${aws_s3_bucket.ICPEEL_DEV01_MS_EFT.id}/*",
            "Condition": {
                "StringLike": {
                    "s3:x-amz-grant-read": [
                        "*http://acs.amazonaws.com/groups/global/AllUsers*",
                        "*http://acs.amazonaws.com/groups/global/AuthenticatedUsers*"
                    ]
                }
            }
        },
        {
            "Sid": "DenyPublicListACL",
            "Effect": "Deny",
            "Principal": {
                "AWS": "*"
            },
            "Action": "s3:PutBucketAcl",
            "Resource": "arn:aws:s3:::${aws_s3_bucket.ICPEEL_DEV01_MS_EFT.id}",
            "Condition": {
                "StringEquals": {
                    "s3:x-amz-acl": [
                        "public-read",
                        "public-read-write",
                        "authenticated-read"
                    ]
                }
            }
        },
        {
            "Sid": "DenyPublicListGrant",
            "Effect": "Deny",
            "Principal": {
                "AWS": "*"
            },
            "Action": "s3:PutBucketAcl",
            "Resource": "arn:aws:s3:::${aws_s3_bucket.ICPEEL_DEV01_MS_EFT.id}",
            "Condition": {
                "StringLike": {
                    "s3:x-amz-grant-read": [
                        "*http://acs.amazonaws.com/groups/global/AllUsers*",
                        "*http://acs.amazonaws.com/groups/global/AuthenticatedUsers*"
                    ]
                }
            }
        },
        {
            "Sid": "DenyPublicReadConsole",
            "Effect": "Deny",
            "Principal": {
                "AWS": "*"
            },
            "Action": [
                "s3:PutObjectAcl",
                "s3:PutBucketAcl"
            ],
            "Resource": [
                "arn:aws:s3:::${aws_s3_bucket.ICPEEL_DEV01_MS_EFT.id}/*",
                "arn:aws:s3:::${aws_s3_bucket.ICPEEL_DEV01_MS_EFT.id}"
            ],
            "Condition": {
                "Null": {
                    "s3:x-amz-grant-write": "true",
                    "s3:x-amz-grant-read": "true",
                    "s3:x-amz-acl": "true",
                    "s3:x-amz-grant-read-acp": "true",
                    "s3:x-amz-grant-full-control": "true",
                    "s3:x-amz-grant-write-acp": "true"
                }
            }
        },
        {
            "Sid": "InstanceListBucketAllow",
            "Effect": "Allow",
            "Principal": {
                "AWS": "arn:aws:iam::${var.peel-dev-account}:user/peel-application-user"
            },
            "Action": "s3:ListBucket",
            "Resource": "arn:aws:s3:::${aws_s3_bucket.ICPEEL_DEV01_MS_EFT.id}"
        },
        {
            "Sid": "InstanceObjectAllow",
            "Effect": "Allow",
            "Principal": {
                "AWS": "arn:aws:iam::${var.peel-dev-account}:user/peel-application-user"
            },
            "Action": [
                "s3:GetObject",
                "s3:PutObject",
                "s3:DeleteObject"
            ],
            "Resource": "arn:aws:s3:::${aws_s3_bucket.ICPEEL_DEV01_MS_EFT.id}/*"
        }
    ]
}
POLICY
}

resource "aws_s3_bucket" "ICPEEL_PPS_MS_EFT" {
  bucket = "peel-microservices-eft-pps"
  acl = "private"

  lifecycle_rule {
    enabled = true

    transition {
      days = 30
      storage_class = "STANDARD_IA"
    }

  }
  
  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm     = "aws:kms"
        kms_master_key_id = "arn:aws:kms:ca-central-1:005810662120:key/fc48cb18-60b3-4578-a32e-b67c774a63dd"
      }
    }
  }
  
  tags {
    Name = "peel-microservices-documents-pps"
  }
}

resource "aws_s3_bucket_policy" "ICPEEL_PPS_MS_EFT" {
  bucket = "${aws_s3_bucket.ICPEEL_PPS_MS_EFT.id}"

  policy = <<POLICY
{
    "Version": "2012-10-17",
    "Id": "ICPEEL_PPS_MS_EFT",
    "Statement": [
        {
            "Sid": "DenyPublicReadACL",
            "Effect": "Deny",
            "Principal": {
                "AWS": "*"
            },
            "Action": [
                "s3:PutObject",
                "s3:PutObjectAcl"
            ],
            "Resource": "arn:aws:s3:::${aws_s3_bucket.ICPEEL_PPS_MS_EFT.id}/*",
            "Condition": {
                "StringEquals": {
                    "s3:x-amz-acl": [
                        "public-read",
                        "public-read-write",
                        "authenticated-read"
                    ]
                }
            }
        },
        {
            "Sid": "DenyPublicReadGrant",
            "Effect": "Deny",
            "Principal": {
                "AWS": "*"
            },
            "Action": [
                "s3:PutObject",
                "s3:PutObjectAcl"
            ],
            "Resource": "arn:aws:s3:::${aws_s3_bucket.ICPEEL_PPS_MS_EFT.id}/*",
            "Condition": {
                "StringLike": {
                    "s3:x-amz-grant-read": [
                        "*http://acs.amazonaws.com/groups/global/AllUsers*",
                        "*http://acs.amazonaws.com/groups/global/AuthenticatedUsers*"
                    ]
                }
            }
        },
        {
            "Sid": "DenyPublicListACL",
            "Effect": "Deny",
            "Principal": {
                "AWS": "*"
            },
            "Action": "s3:PutBucketAcl",
            "Resource": "arn:aws:s3:::${aws_s3_bucket.ICPEEL_PPS_MS_EFT.id}",
            "Condition": {
                "StringEquals": {
                    "s3:x-amz-acl": [
                        "public-read",
                        "public-read-write",
                        "authenticated-read"
                    ]
                }
            }
        },
        {
            "Sid": "DenyPublicListGrant",
            "Effect": "Deny",
            "Principal": {
                "AWS": "*"
            },
            "Action": "s3:PutBucketAcl",
            "Resource": "arn:aws:s3:::${aws_s3_bucket.ICPEEL_PPS_MS_EFT.id}",
            "Condition": {
                "StringLike": {
                    "s3:x-amz-grant-read": [
                        "*http://acs.amazonaws.com/groups/global/AllUsers*",
                        "*http://acs.amazonaws.com/groups/global/AuthenticatedUsers*"
                    ]
                }
            }
        },
        {
            "Sid": "DenyPublicReadConsole",
            "Effect": "Deny",
            "Principal": {
                "AWS": "*"
            },
            "Action": [
                "s3:PutObjectAcl",
                "s3:PutBucketAcl"
            ],
            "Resource": [
                "arn:aws:s3:::${aws_s3_bucket.ICPEEL_PPS_MS_EFT.id}/*",
                "arn:aws:s3:::${aws_s3_bucket.ICPEEL_PPS_MS_EFT.id}"
            ],
            "Condition": {
                "Null": {
                    "s3:x-amz-grant-write": "true",
                    "s3:x-amz-grant-read": "true",
                    "s3:x-amz-acl": "true",
                    "s3:x-amz-grant-read-acp": "true",
                    "s3:x-amz-grant-full-control": "true",
                    "s3:x-amz-grant-write-acp": "true"
                }
            }
        },
        {
            "Sid": "InstanceListBucketAllow",
            "Effect": "Allow",
            "Principal": {
                "AWS": "arn:aws:iam::${var.peel-dev-account}:user/peel-application-user"
            },
            "Action": "s3:ListBucket",
            "Resource": "arn:aws:s3:::${aws_s3_bucket.ICPEEL_PPS_MS_EFT.id}"
        },
        {
            "Sid": "InstanceObjectAllow",
            "Effect": "Allow",
            "Principal": {
                "AWS": "arn:aws:iam::${var.peel-dev-account}:user/peel-application-user"
            },
            "Action": [
                "s3:GetObject",
                "s3:PutObject",
                "s3:DeleteObject"
            ],
            "Resource": "arn:aws:s3:::${aws_s3_bucket.ICPEEL_PPS_MS_EFT.id}/*"
        }
    ]
}
POLICY
}

resource "aws_s3_bucket" "ICPEEL_PROD_GWDB_BACKUPS" {
  bucket = "peel-prod-gwdb-backups"
  acl = "private"

  lifecycle_rule {
    enabled = true

    transition {
      days = 30
      storage_class = "STANDARD_IA"
    }

  }
  
  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm     = "aws:kms"
        kms_master_key_id = "arn:aws:kms:ca-central-1:005810662120:key/fc48cb18-60b3-4578-a32e-b67c774a63dd"
      }
    }
  }
  
  tags {
    Name = "peel-prod-gwdb-backups"
  }
}

resource "aws_s3_bucket_policy" "ICPEEL_PROD_GWDB_BACKUPS" {
  bucket = "${aws_s3_bucket.ICPEEL_PROD_GWDB_BACKUPS.id}"

  policy = <<POLICY
{
    "Version": "2012-10-17",
    "Id": "ICPEEL_PROD_GWDB_BACKUPS_BUCKET_POLICY",
    "Statement": [
        {
            "Sid": "DenyPublicReadACL",
            "Effect": "Deny",
            "Principal": {
                "AWS": "*"
            },
            "Action": [
                "s3:PutObject",
                "s3:PutObjectAcl"
            ],
            "Resource": "arn:aws:s3:::${aws_s3_bucket.ICPEEL_PROD_GWDB_BACKUPS.id}/*",
            "Condition": {
                "StringEquals": {
                    "s3:x-amz-acl": [
                        "public-read",
                        "public-read-write",
                        "authenticated-read"
                    ]
                }
            }
        },
        {
            "Sid": "DenyPublicReadGrant",
            "Effect": "Deny",
            "Principal": {
                "AWS": "*"
            },
            "Action": [
                "s3:PutObject",
                "s3:PutObjectAcl"
            ],
            "Resource": "arn:aws:s3:::${aws_s3_bucket.ICPEEL_PROD_GWDB_BACKUPS.id}/*",
            "Condition": {
                "StringLike": {
                    "s3:x-amz-grant-read": [
                        "*http://acs.amazonaws.com/groups/global/AllUsers*",
                        "*http://acs.amazonaws.com/groups/global/AuthenticatedUsers*"
                    ]
                }
            }
        },
        {
            "Sid": "DenyPublicListACL",
            "Effect": "Deny",
            "Principal": {
                "AWS": "*"
            },
            "Action": "s3:PutBucketAcl",
            "Resource": "arn:aws:s3:::${aws_s3_bucket.ICPEEL_PROD_GWDB_BACKUPS.id}",
            "Condition": {
                "StringEquals": {
                    "s3:x-amz-acl": [
                        "public-read",
                        "public-read-write",
                        "authenticated-read"
                    ]
                }
            }
        },
        {
            "Sid": "DenyPublicListGrant",
            "Effect": "Deny",
            "Principal": {
                "AWS": "*"
            },
            "Action": "s3:PutBucketAcl",
            "Resource": "arn:aws:s3:::${aws_s3_bucket.ICPEEL_PROD_GWDB_BACKUPS.id}",
            "Condition": {
                "StringLike": {
                    "s3:x-amz-grant-read": [
                        "*http://acs.amazonaws.com/groups/global/AllUsers*",
                        "*http://acs.amazonaws.com/groups/global/AuthenticatedUsers*"
                    ]
                }
            }
        },
        {
            "Sid": "DenyPublicReadConsole",
            "Effect": "Deny",
            "Principal": {
                "AWS": "*"
            },
            "Action": [
                "s3:PutObjectAcl",
                "s3:PutBucketAcl"
            ],
            "Resource": [
                "arn:aws:s3:::${aws_s3_bucket.ICPEEL_PROD_GWDB_BACKUPS.id}/*",
                "arn:aws:s3:::${aws_s3_bucket.ICPEEL_PROD_GWDB_BACKUPS.id}"
            ],
            "Condition": {
                "Null": {
                    "s3:x-amz-acl": "true",
                    "s3:x-amz-grant-read": "true",
                    "s3:x-amz-grant-write": "true",
                    "s3:x-amz-grant-read-acp": "true",
                    "s3:x-amz-grant-full-control": "true",
                    "s3:x-amz-grant-write-acp": "true"
                }
            }
        },
        {
            "Sid": "DevInstanceListBucketAllow",
            "Effect": "Allow",
            "Principal": {
                "AWS": "arn:aws:iam::${var.peel-dev-account}:role/CMS-SSMRole"
            },
            "Action": "s3:ListBucket",
            "Resource": "arn:aws:s3:::${aws_s3_bucket.ICPEEL_PROD_GWDB_BACKUPS.id}"
        },
        {
            "Sid": "DevInstanceObjectAllow",
            "Effect": "Allow",
            "Principal": {
                "AWS": "arn:aws:iam::${var.peel-dev-account}:role/CMS-SSMRole"
            },
            "Action": "s3:GetObject",
            "Resource": "arn:aws:s3:::${aws_s3_bucket.ICPEEL_PROD_GWDB_BACKUPS.id}/*"
        },
        {
            "Sid": "ProdInstanceListBucketAllow",
            "Effect": "Allow",
            "Principal": {
                "AWS": "arn:aws:iam::${var.peel-prod-account}:role/CMS-SSMRole"
            },
            "Action": "s3:ListBucket",
            "Resource": "arn:aws:s3:::${aws_s3_bucket.ICPEEL_PROD_GWDB_BACKUPS.id}"
        },
        {
            "Sid": "ProdInstanceObjectAllow",
            "Effect": "Allow",
            "Principal": {
                "AWS": "arn:aws:iam::${var.peel-prod-account}:role/CMS-SSMRole"
            },
            "Action": "s3:PutObject",
            "Resource": "arn:aws:s3:::${aws_s3_bucket.ICPEEL_PROD_GWDB_BACKUPS.id}/*"
        }
    ]
}
POLICY
}

resource "aws_s3_bucket" "ICPEEL_EC2_DB_BACKUPS" {
  bucket = "peel-ec2-db-backups"
  acl = "private"

  lifecycle_rule {
    enabled = true

    transition {
      days = 30
      storage_class = "STANDARD_IA"
    }

  }
  
  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm     = "AES256"
      }
    }
  }
  
  tags {
    Name = "peel-ec2-db-backups"
  }
}

resource "aws_s3_bucket_policy" "ICPEEL_EC2_DB_BACKUPS" {
  bucket = "${aws_s3_bucket.ICPEEL_EC2_DB_BACKUPS.id}"

    policy = <<POLICY
{
    "Version": "2012-10-17",
    "Id": "ICPEEL_EC2_DB_BACKUPS",
    "Statement": [
        {
            "Sid": "DenyPublicReadACL",
            "Effect": "Deny",
            "Principal": {
                "AWS": "*"
            },
            "Action": [
                "s3:PutObject",
                "s3:PutObjectAcl"
            ],
            "Resource": "arn:aws:s3:::${aws_s3_bucket.ICPEEL_EC2_DB_BACKUPS.id}/*",
            "Condition": {
                "StringEquals": {
                    "s3:x-amz-acl": [
                        "public-read",
                        "public-read-write",
                        "authenticated-read"
                    ]
                }
            }
        },
        {
            "Sid": "DenyPublicReadGrant",
            "Effect": "Deny",
            "Principal": {
                "AWS": "*"
            },
            "Action": [
                "s3:PutObject",
                "s3:PutObjectAcl"
            ],
            "Resource": "arn:aws:s3:::${aws_s3_bucket.ICPEEL_EC2_DB_BACKUPS.id}/*",
            "Condition": {
                "StringLike": {
                    "s3:x-amz-grant-read": [
                        "*http://acs.amazonaws.com/groups/global/AllUsers*",
                        "*http://acs.amazonaws.com/groups/global/AuthenticatedUsers*"
                    ]
                }
            }
        },
        {
            "Sid": "DenyPublicListACL",
            "Effect": "Deny",
            "Principal": {
                "AWS": "*"
            },
            "Action": "s3:PutBucketAcl",
            "Resource": "arn:aws:s3:::${aws_s3_bucket.ICPEEL_EC2_DB_BACKUPS.id}",
            "Condition": {
                "StringEquals": {
                    "s3:x-amz-acl": [
                        "public-read",
                        "public-read-write",
                        "authenticated-read"
                    ]
                }
            }
        },
        {
            "Sid": "DenyPublicListGrant",
            "Effect": "Deny",
            "Principal": {
                "AWS": "*"
            },
            "Action": "s3:PutBucketAcl",
            "Resource": "arn:aws:s3:::${aws_s3_bucket.ICPEEL_EC2_DB_BACKUPS.id}",
            "Condition": {
                "StringLike": {
                    "s3:x-amz-grant-read": [
                        "*http://acs.amazonaws.com/groups/global/AllUsers*",
                        "*http://acs.amazonaws.com/groups/global/AuthenticatedUsers*"
                    ]
                }
            }
        },
        {
            "Sid": "DenyPublicReadConsole",
            "Effect": "Deny",
            "Principal": {
                "AWS": "*"
            },
            "Action": [
                "s3:PutObjectAcl",
                "s3:PutBucketAcl"
            ],
            "Resource": [
                "arn:aws:s3:::${aws_s3_bucket.ICPEEL_EC2_DB_BACKUPS.id}/*",
                "arn:aws:s3:::${aws_s3_bucket.ICPEEL_EC2_DB_BACKUPS.id}"
            ],
            "Condition": {
                "Null": {
                    "s3:x-amz-grant-write": "true",
                    "s3:x-amz-grant-read": "true",
                    "s3:x-amz-acl": "true",
                    "s3:x-amz-grant-read-acp": "true",
                    "s3:x-amz-grant-full-control": "true",
                    "s3:x-amz-grant-write-acp": "true"
                }
            }
        },
        {
            "Sid": "InstanceListBucketAllow",
            "Effect": "Allow",
            "Principal": {
                "AWS": "arn:aws:iam::${var.peel-dev-account}:role/CMS-SSMRole"
            },
            "Action": "s3:ListBucket",
            "Resource": "arn:aws:s3:::${aws_s3_bucket.ICPEEL_EC2_DB_BACKUPS.id}"
        },
        {
            "Sid": "InstanceObjectAllow",
            "Effect": "Allow",
            "Principal": {
                "AWS": "arn:aws:iam::${var.peel-dev-account}:role/CMS-SSMRole"
            },
            "Action": [
                "s3:GetObject",
                "s3:PutObject",
                "s3:DeleteObject"
            ],
            "Resource": "arn:aws:s3:::${aws_s3_bucket.ICPEEL_EC2_DB_BACKUPS.id}/*"
        }
    ]
}
POLICY
}

resource "aws_s3_bucket" "ICPEEL_DEV01_PROM_THANOS" {
  bucket = "peel-monitoring-prometheus-dev01"
  acl = "private"

  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm     = "aws:kms"
        kms_master_key_id = "arn:aws:kms:ca-central-1:005810662120:key/fc48cb18-60b3-4578-a32e-b67c774a63dd"
      }
    }
  }
  
  tags {
    Name = "peel-microservices-documents-dev01"
  }
}

resource "aws_s3_bucket_policy" "ICPEEL_DEV01_PROM_THANOS_BUCKET_POLICY" {
  bucket = "${aws_s3_bucket.ICPEEL_DEV01_PROM_THANOS.id}"

  policy = <<POLICY
{
    "Version": "2012-10-17",
    "Id": "ICPEEL_DEV01_PROM_THANOS",
    "Statement": [
        {
            "Sid": "DenyPublicReadACL",
            "Effect": "Deny",
            "Principal": {
                "AWS": "*"
            },
            "Action": [
                "s3:PutObject",
                "s3:PutObjectAcl"
            ],
            "Resource": "arn:aws:s3:::${aws_s3_bucket.ICPEEL_DEV01_PROM_THANOS.id}/*",
            "Condition": {
                "StringEquals": {
                    "s3:x-amz-acl": [
                        "public-read",
                        "public-read-write",
                        "authenticated-read"
                    ]
                }
            }
        },
        {
            "Sid": "DenyPublicReadGrant",
            "Effect": "Deny",
            "Principal": {
                "AWS": "*"
            },
            "Action": [
                "s3:PutObject",
                "s3:PutObjectAcl"
            ],
            "Resource": "arn:aws:s3:::${aws_s3_bucket.ICPEEL_DEV01_PROM_THANOS.id}/*",
            "Condition": {
                "StringLike": {
                    "s3:x-amz-grant-read": [
                        "*http://acs.amazonaws.com/groups/global/AllUsers*",
                        "*http://acs.amazonaws.com/groups/global/AuthenticatedUsers*"
                    ]
                }
            }
        },
        {
            "Sid": "DenyPublicListACL",
            "Effect": "Deny",
            "Principal": {
                "AWS": "*"
            },
            "Action": "s3:PutBucketAcl",
            "Resource": "arn:aws:s3:::${aws_s3_bucket.ICPEEL_DEV01_PROM_THANOS.id}",
            "Condition": {
                "StringEquals": {
                    "s3:x-amz-acl": [
                        "public-read",
                        "public-read-write",
                        "authenticated-read"
                    ]
                }
            }
        },
        {
            "Sid": "DenyPublicListGrant",
            "Effect": "Deny",
            "Principal": {
                "AWS": "*"
            },
            "Action": "s3:PutBucketAcl",
            "Resource": "arn:aws:s3:::${aws_s3_bucket.ICPEEL_DEV01_PROM_THANOS.id}",
            "Condition": {
                "StringLike": {
                    "s3:x-amz-grant-read": [
                        "*http://acs.amazonaws.com/groups/global/AllUsers*",
                        "*http://acs.amazonaws.com/groups/global/AuthenticatedUsers*"
                    ]
                }
            }
        },
        {
            "Sid": "DenyPublicReadConsole",
            "Effect": "Deny",
            "Principal": {
                "AWS": "*"
            },
            "Action": [
                "s3:PutObjectAcl",
                "s3:PutBucketAcl"
            ],
            "Resource": [
                "arn:aws:s3:::${aws_s3_bucket.ICPEEL_DEV01_PROM_THANOS.id}/*",
                "arn:aws:s3:::${aws_s3_bucket.ICPEEL_DEV01_PROM_THANOS.id}"
            ],
            "Condition": {
                "Null": {
                    "s3:x-amz-grant-write": "true",
                    "s3:x-amz-grant-read": "true",
                    "s3:x-amz-acl": "true",
                    "s3:x-amz-grant-read-acp": "true",
                    "s3:x-amz-grant-full-control": "true",
                    "s3:x-amz-grant-write-acp": "true"
                }
            }
        },
        {
            "Sid": "InstanceListBucketAllow",
            "Effect": "Allow",
            "Principal": {
                "AWS": "arn:aws:iam::${var.peel-dev-account}:user/peel-application-user"
            },
            "Action": "s3:ListBucket",
            "Resource": "arn:aws:s3:::${aws_s3_bucket.ICPEEL_DEV01_PROM_THANOS.id}"
        },
        {
            "Sid": "InstanceObjectAllow",
            "Effect": "Allow",
            "Principal": {
                "AWS": "arn:aws:iam::${var.peel-dev-account}:user/peel-application-user"
            },
            "Action": [
                "s3:GetObject",
                "s3:PutObject",
                "s3:DeleteObject"
            ],
            "Resource": "arn:aws:s3:::${aws_s3_bucket.ICPEEL_DEV01_PROM_THANOS.id}/*"
        }
    ]
}
POLICY
}

resource "aws_s3_bucket" "ICPEEL_PPS_PROM_THANOS" {
  bucket = "peel-monitoring-prometheus-pps"
  acl = "private"

  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm     = "aws:kms"
        kms_master_key_id = "arn:aws:kms:ca-central-1:005810662120:key/fc48cb18-60b3-4578-a32e-b67c774a63dd"
      }
    }
  }
  
  tags {
    Name = "peel-microservices-documents-pps"
  }
}

resource "aws_s3_bucket_policy" "ICPEEL_PPS_PROM_THANOS" {
  bucket = "${aws_s3_bucket.ICPEEL_PPS_PROM_THANOS.id}"

  policy = <<POLICY
{
    "Version": "2012-10-17",
    "Id": "ICPEEL_PPS_PROM_THANOS",
    "Statement": [
        {
            "Sid": "DenyPublicReadACL",
            "Effect": "Deny",
            "Principal": {
                "AWS": "*"
            },
            "Action": [
                "s3:PutObject",
                "s3:PutObjectAcl"
            ],
            "Resource": "arn:aws:s3:::${aws_s3_bucket.ICPEEL_PPS_PROM_THANOS.id}/*",
            "Condition": {
                "StringEquals": {
                    "s3:x-amz-acl": [
                        "public-read",
                        "public-read-write",
                        "authenticated-read"
                    ]
                }
            }
        },
        {
            "Sid": "DenyPublicReadGrant",
            "Effect": "Deny",
            "Principal": {
                "AWS": "*"
            },
            "Action": [
                "s3:PutObject",
                "s3:PutObjectAcl"
            ],
            "Resource": "arn:aws:s3:::${aws_s3_bucket.ICPEEL_PPS_PROM_THANOS.id}/*",
            "Condition": {
                "StringLike": {
                    "s3:x-amz-grant-read": [
                        "*http://acs.amazonaws.com/groups/global/AllUsers*",
                        "*http://acs.amazonaws.com/groups/global/AuthenticatedUsers*"
                    ]
                }
            }
        },
        {
            "Sid": "DenyPublicListACL",
            "Effect": "Deny",
            "Principal": {
                "AWS": "*"
            },
            "Action": "s3:PutBucketAcl",
            "Resource": "arn:aws:s3:::${aws_s3_bucket.ICPEEL_PPS_PROM_THANOS.id}",
            "Condition": {
                "StringEquals": {
                    "s3:x-amz-acl": [
                        "public-read",
                        "public-read-write",
                        "authenticated-read"
                    ]
                }
            }
        },
        {
            "Sid": "DenyPublicListGrant",
            "Effect": "Deny",
            "Principal": {
                "AWS": "*"
            },
            "Action": "s3:PutBucketAcl",
            "Resource": "arn:aws:s3:::${aws_s3_bucket.ICPEEL_PPS_PROM_THANOS.id}",
            "Condition": {
                "StringLike": {
                    "s3:x-amz-grant-read": [
                        "*http://acs.amazonaws.com/groups/global/AllUsers*",
                        "*http://acs.amazonaws.com/groups/global/AuthenticatedUsers*"
                    ]
                }
            }
        },
        {
            "Sid": "DenyPublicReadConsole",
            "Effect": "Deny",
            "Principal": {
                "AWS": "*"
            },
            "Action": [
                "s3:PutObjectAcl",
                "s3:PutBucketAcl"
            ],
            "Resource": [
                "arn:aws:s3:::${aws_s3_bucket.ICPEEL_PPS_PROM_THANOS.id}/*",
                "arn:aws:s3:::${aws_s3_bucket.ICPEEL_PPS_PROM_THANOS.id}"
            ],
            "Condition": {
                "Null": {
                    "s3:x-amz-grant-write": "true",
                    "s3:x-amz-grant-read": "true",
                    "s3:x-amz-acl": "true",
                    "s3:x-amz-grant-read-acp": "true",
                    "s3:x-amz-grant-full-control": "true",
                    "s3:x-amz-grant-write-acp": "true"
                }
            }
        },
        {
            "Sid": "InstanceListBucketAllow",
            "Effect": "Allow",
            "Principal": {
                "AWS": "arn:aws:iam::${var.peel-dev-account}:user/peel-application-user"
            },
            "Action": "s3:ListBucket",
            "Resource": "arn:aws:s3:::${aws_s3_bucket.ICPEEL_PPS_PROM_THANOS.id}"
        },
        {
            "Sid": "InstanceObjectAllow",
            "Effect": "Allow",
            "Principal": {
                "AWS": "arn:aws:iam::${var.peel-dev-account}:user/peel-application-user"
            },
            "Action": [
                "s3:GetObject",
                "s3:PutObject",
                "s3:DeleteObject"
            ],
            "Resource": "arn:aws:s3:::${aws_s3_bucket.ICPEEL_PPS_PROM_THANOS.id}/*"
        }
    ]
}
POLICY
}