resource "aws_security_group" "SG_MSSQL_DB" {
  name = "MSSQL-DB-SG"

  ingress {
    from_port = 1433
    to_port   = 1433
    protocol  = "tcp"

    cidr_blocks = ["${var.portage_hub_cidr}"]
  }

  ingress {
    from_port = 1433
    to_port   = 1433
    protocol  = "tcp"

    cidr_blocks = ["10.171.8.11/32"]
    description = "DHIC SAP Server"
  }

  ingress {
    from_port = 80
    to_port   = 80
    protocol  = "tcp"

    cidr_blocks = ["${var.portage_aviatrix_vpn}"]
  }

  ingress {
    from_port = 443
    to_port   = 443
    protocol  = "tcp"

    cidr_blocks = ["${var.portage_dev_cidr}"]
  }

  ingress {
    from_port = 1434
    to_port   = 1434
    protocol  = "udp"

    cidr_blocks = ["${var.portage_hub_cidr}"]
  }

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    description = "Qliksense"
    cidr_blocks = ["${var.portage_hub_cidr}"]
  }

  ingress {
    from_port   = 1433
    to_port     = 1433
    protocol    = "tcp"
    description = "Qliksense"
    cidr_blocks = ["10.171.8.37/32"]
  }

  ingress {
    from_port   = 1433
    to_port     = 1433
    protocol    = "tcp"
    description = "From Jenkins"
    cidr_blocks = ["10.171.8.17/32"]
  }

  ingress {
    from_port   = 1433
    to_port     = 1433
    protocol    = "tcp"
    description = "New rule - Carlos - access to DB from 10.171.9.81"
    cidr_blocks = ["10.171.8.90/32"]
  }

  egress {
    from_port = 0
    to_port   = 0
    protocol  = "-1"

    cidr_blocks = ["0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    Contract = "MANAGED_SERVICES"
    Name     = "MSSQL-DB-SG"
  }

  ingress {
    from_port   = 1433
    to_port     = 1433
    protocol    = "tcp"
    description = "From AWS Workspaces"
    cidr_blocks = ["${var.portage_workspaces}"]
  }

}
