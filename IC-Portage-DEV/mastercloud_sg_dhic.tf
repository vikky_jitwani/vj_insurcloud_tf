resource "aws_security_group" "SG_DHIC" {
  name = "DHIC-SG"

  ingress {
    from_port   = 3500
    to_port     = 3500
    protocol    = "tcp"
    cidr_blocks = ["${var.portage_hub_cidr}"]
  }

  ingress {
    from_port   = 4000
    to_port     = 4000
    protocol    = "tcp"
    cidr_blocks = ["${var.portage_hub_cidr}"]
  }

  ingress {
    from_port   = 4001
    to_port     = 4001
    protocol    = "tcp"
    cidr_blocks = ["${var.portage_hub_cidr}"]
  }

  ingress {
    from_port   = 4010
    to_port     = 4010
    protocol    = "tcp"
    cidr_blocks = ["${var.portage_hub_cidr}"]
  }

  ingress {
    from_port   = 4011
    to_port     = 4011
    protocol    = "tcp"
    cidr_blocks = ["${var.portage_hub_cidr}"]
  }

  ingress {
    from_port   = 4012
    to_port     = 4012
    protocol    = "tcp"
    cidr_blocks = ["${var.portage_hub_cidr}"]
  }

  ingress {
    from_port   = 4013
    to_port     = 4013
    protocol    = "tcp"
    cidr_blocks = ["${var.portage_hub_cidr}"]
  }

  ingress {
    from_port   = 5001
    to_port     = 5001
    protocol    = "tcp"
    cidr_blocks = ["${var.portage_hub_cidr}"]
  }

  ingress {
    from_port   = 6400
    to_port     = 6400
    protocol    = "tcp"
    cidr_blocks = ["${var.portage_hub_cidr}"]
  }

  ingress {
    from_port   = 8080
    to_port     = 8080
    protocol    = "tcp"
    cidr_blocks = ["${var.portage_hub_cidr}"]
  }

  ingress {
    from_port   = 49152
    to_port     = 65535
    protocol    = "tcp"
    cidr_blocks = ["${var.portage_hub_cidr}"]
    description = "Connection between the SAP BODS client and the server"
  }

  # SAP Server #
  ingress {
    from_port   = 3500
    to_port     = 3500
    protocol    = "tcp"
    cidr_blocks = ["10.171.8.37/32"]
    description = "SAP Server"
  }

  ingress {
    from_port   = 4000
    to_port     = 4000
    protocol    = "tcp"
    cidr_blocks = ["10.171.8.37/32"]
    description = "SAP Server"
  }

  ingress {
    from_port   = 4001
    to_port     = 4001
    protocol    = "tcp"
    cidr_blocks = ["10.171.8.37/32"]
    description = "SAP Server"
  }

  ingress {
    from_port   = 4010
    to_port     = 4010
    protocol    = "tcp"
    cidr_blocks = ["10.171.8.37/32"]
    description = "SAP Server"
  }

  ingress {
    from_port   = 4011
    to_port     = 4011
    protocol    = "tcp"
    cidr_blocks = ["10.171.8.37/32"]
    description = "SAP Server"
  }

  ingress {
    from_port   = 4012
    to_port     = 4012
    protocol    = "tcp"
    cidr_blocks = ["10.171.8.37/32"]
    description = "SAP Server"
  }

  ingress {
    from_port   = 4013
    to_port     = 4013
    protocol    = "tcp"
    cidr_blocks = ["10.171.8.37/32"]
    description = "SAP Server"
  }

  ingress {
    from_port   = 5001
    to_port     = 5001
    protocol    = "tcp"
    cidr_blocks = ["10.171.8.37/32"]
    description = "SAP Server"
  }

  ingress {
    from_port   = 6400
    to_port     = 6400
    protocol    = "tcp"
    cidr_blocks = ["10.171.8.37/32"]
    description = "SAP Server"
  }

  ingress {
    from_port   = 8080
    to_port     = 8080
    protocol    = "tcp"
    cidr_blocks = ["10.171.8.37/32"]
    description = "SAP Server"
  }

  ingress {
    from_port   = 49152
    to_port     = 65535
    protocol    = "tcp"
    cidr_blocks = ["10.171.8.37/32"]
    description = "SAP Server"
  }

  ingress {
    from_port   = 1433
    to_port     = 1433
    protocol    = "tcp"
    cidr_blocks = ["10.171.8.37/32"]
    description = "SAP Server"
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    "Name" = "DHIC-SG"
  }

  ingress {
    from_port   = 49152
    to_port     = 65535
    protocol    = "tcp"
    cidr_blocks = ["${var.portage_dhic_sap}"]
    description = "SAP client to SAP server"
  }

  ingress {
    from_port   = 445
    to_port     = 445
    protocol    = "tcp"
    cidr_blocks = ["10.171.8.37/32"]
    description = "Network drive share from .37 - .11"
  }

  ingress {
    from_port   = 6400
    to_port     = 6400
    protocol    = "tcp"
    cidr_blocks = ["10.171.8.119/32"]
    description = "Strange IP from .119 to .11"
  }
}