provider "aws" {
  region     = "ca-central-1"
  version = "~> 2.26"
}

terraform {
 backend "s3" {
    bucket     = "a-c-mc-p-a004-goremutual-tfstate"
    key        = "IC-GoreMutual-DEV/STATE/current.tf"
    region     = "ca-central-1"
	  #encrypt    = true
	  #kms_key_id = "arn:aws:kms:ca-central-1:532382648425:key/e61c5b45-c317-468d-af56-07e566f3c870"
  }
}