/*
resource "aws_security_group" "SG_CLIENT_PING_TEST" {
  name = "CLIENT-PING-TEST"

  ingress {
    from_port       = -1
    to_port         = -1
    protocol        = "icmp"
    cidr_blocks     = [
      "${var.portage_cidr}",
      "${var.portage_watchguard_cidr}",
      "${var.portage_dmz_cidr}",
      "${var.portage_wireless_cidr}"
    ]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    Name = "CLIENT-PING-TEST"
  }
}
*/

resource "aws_security_group" "SG_AVIATRIX_RDP" {
  name = "AVIATRIX-RDP-SG"

  ingress {
    from_port   = 3389
    to_port     = 3389
    protocol    = "tcp"
    cidr_blocks = ["${var.portage_aviatrix_vpn}"]
  
    description = "RDP"
  }

  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    Name = "AVIATRIX-RDP-SG"
  }
}

resource "aws_security_group" "SG_AVIATRIX_SSH" {
  name = "AVIATRIX-SSH-SG"

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = [
      "${var.portage_aviatrix_vpn}"
    ]
    description = "SSH"
  }

  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    Name = "AVIATRIX-SSH-SG"
  }
}
