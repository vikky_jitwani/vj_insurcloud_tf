# #=======================================================================
# # DEV01
# #=======================================================================
resource "aws_volume_attachment" "ICPEEL_DEV01_GUIDEWIRE_SUITE_DOCKER_EBS_ATTACHMENT" {
  device_name = "/dev/sdh"
  volume_id   = "${aws_ebs_volume.ICPEEL_DEV01_GUIDEWIRE_SUITE_DOCKER_EBS.id}"
  instance_id = "${aws_instance.ICPEEL_DEV01_GUIDEWIRE_SUITE.id}"
}

resource "aws_ebs_volume" "ICPEEL_DEV01_GUIDEWIRE_SUITE_DOCKER_EBS" {
  availability_zone = "ca-central-1a"
  size              = 150
  type              = "gp3"
  encrypted         = true
  kms_key_id        = "arn:aws:kms:ca-central-1:005810662120:key/8f33a54d-e8c3-44fc-b083-69e56ffc28a2"
  tags = {
    Name = "ICPEEL_DEV01_GUIDEWIRE_SUITE_DOCKER_EBS"
    Docker = "True"
  }
}

resource "aws_volume_attachment" "ICPEEL_DEV01_GUIDEWIRE_SUITE_LOGS_EBS_ATTACHMENT" {
  device_name = "/dev/sdi"
  volume_id   = "${aws_ebs_volume.ICPEEL_DEV01_GUIDEWIRE_SUITE_LOGS_EBS.id}"
  instance_id = "${aws_instance.ICPEEL_DEV01_GUIDEWIRE_SUITE.id}"
}

resource "aws_ebs_volume" "ICPEEL_DEV01_GUIDEWIRE_SUITE_LOGS_EBS" {
  availability_zone = "ca-central-1a"
  size              = 150
  type              = "gp3"
  encrypted         = true
  kms_key_id        = "arn:aws:kms:ca-central-1:005810662120:key/8f33a54d-e8c3-44fc-b083-69e56ffc28a2"
  tags = {
    Name = "ICPEEL_DEV01_GUIDEWIRE_SUITE_LOGS_EBS"
    Docker = "True"
  }
}

############ External HAProxy Volumes ##################
# Not being used in Dev01 yet
# resource "aws_volume_attachment" "ICPEEL_DEV01_EXTERNAL_HAPROXY_DOCKER_EBS_ATTACHMENT" {
#   device_name = "/dev/sdh"
#   volume_id   = "${aws_ebs_volume.ICPEEL_DEV01_EXTERNAL_HAPROXY_DOCKER_EBS.id}"
#   instance_id = "${aws_instance.ICPEEL_DEV01_EXTERNAL_HAPROXY.id}"
# }

# resource "aws_ebs_volume" "ICPEEL_DEV01_EXTERNAL_HAPROXY_DOCKER_EBS" {
#   availability_zone = "ca-central-1a"
#   size              = 20
#   type              = "gp3"
#   encrypted         = true
#   kms_key_id        = "arn:aws:kms:ca-central-1:005810662120:key/8f33a54d-e8c3-44fc-b083-69e56ffc28a2"
#   tags = {
#     Name = "ICPEEL_DEV01_EXTERNAL_HAPROXY_DOCKER_EBS"
#     Docker = "True"
#   }
# }

############ Internal HAProxy Volumes ##################
resource "aws_volume_attachment" "ICPEEL_DEV01_INTERNAL_HAPROXY_DOCKER_EBS_ATTACHMENT" {
  device_name = "/dev/sdh"
  volume_id   = "${aws_ebs_volume.ICPEEL_DEV01_INTERNAL_HAPROXY_DOCKER_EBS.id}"
  instance_id = "${aws_instance.ICPEEL_DEV01_INTERNAL_HAPROXY.id}"
}

resource "aws_ebs_volume" "ICPEEL_DEV01_INTERNAL_HAPROXY_DOCKER_EBS" {
  availability_zone = "ca-central-1a"
  size              = 20
  type              = "gp3"
  encrypted         = true
  kms_key_id        = "arn:aws:kms:ca-central-1:005810662120:key/8f33a54d-e8c3-44fc-b083-69e56ffc28a2"
  tags = {
    Name = "ICPEEL_DEV01_INTERNAL_HAPROXY_DOCKER_EBS"
    Docker = "True"
  }
}

############ Microservices Volumes ##################
resource "aws_volume_attachment" "ICPEEL_DEV01_MICROSERVICES_DOCKER_EBS_ATTACHMENT" {
  device_name = "/dev/sdh"
  volume_id   = "${aws_ebs_volume.ICPEEL_DEV01_MICROSERVICES_DOCKER_EBS.id}"
  instance_id = "${aws_instance.ICPEEL_DEV01_MICROSERVICES.id}"
}

resource "aws_ebs_volume" "ICPEEL_DEV01_MICROSERVICES_DOCKER_EBS" {
  availability_zone = "ca-central-1a"
  size              = 50
  type              = "gp3"
  encrypted         = true
  kms_key_id        = "arn:aws:kms:ca-central-1:005810662120:key/8f33a54d-e8c3-44fc-b083-69e56ffc28a2"
  tags = {
    Name = "ICPEEL_DEV01_MICROSERVICES_DOCKER_EBS"
    Docker = "True"
  }
}

# #=======================================================================
# # PPS
# #=======================================================================
####### GW VOLUMES #############
resource "aws_volume_attachment" "ICPEEL_PPS_GUIDEWIRE_APPLICATION_1_DOCKER_EBS_ATTACHMENT" {
  device_name = "/dev/sdh"
  volume_id   = "${aws_ebs_volume.ICPEEL_PPS_GUIDEWIRE_APPLICATION_1_DOCKER_EBS.id}"
  instance_id = "${aws_instance.ICPEEL_PPS_GUIDEWIRE_APPLICATION_1.id}"
}

resource "aws_ebs_volume" "ICPEEL_PPS_GUIDEWIRE_APPLICATION_1_DOCKER_EBS" {
  availability_zone = "ca-central-1a"
  size              = 150
  type              = "gp3"
  encrypted         = true
  kms_key_id        = "arn:aws:kms:ca-central-1:005810662120:key/8f33a54d-e8c3-44fc-b083-69e56ffc28a2"
  tags = {
    Name = "ICPEEL_PPS_GUIDEWIRE_APPLICATION_1_DOCKER_EBS"
    Docker = "True"
  }
}

resource "aws_volume_attachment" "ICPEEL_PPS_GUIDEWIRE_APPLICATION_1_LOGS_EBS_ATTACHMENT" {
  device_name = "/dev/sdi"
  volume_id   = "${aws_ebs_volume.ICPEEL_PPS_GUIDEWIRE_APPLICATION_1_LOGS_EBS.id}"
  instance_id = "${aws_instance.ICPEEL_PPS_GUIDEWIRE_APPLICATION_1.id}"
}

resource "aws_ebs_volume" "ICPEEL_PPS_GUIDEWIRE_APPLICATION_1_LOGS_EBS" {
  availability_zone = "ca-central-1a"
  size              = 150
  type              = "gp3"
  encrypted         = true
  kms_key_id        = "arn:aws:kms:ca-central-1:005810662120:key/8f33a54d-e8c3-44fc-b083-69e56ffc28a2"
  tags = {
    Name = "ICPEEL_PPS_GUIDEWIRE_APPLICATION_1_LOGS_EBS"
    Docker = "True"
  }
}

resource "aws_volume_attachment" "ICPEEL_PPS_GUIDEWIRE_APPLICATION_2_DOCKER_EBS_ATTACHMENT" {
  device_name = "/dev/sdh"
  volume_id   = "${aws_ebs_volume.ICPEEL_PPS_GUIDEWIRE_APPLICATION_2_DOCKER_EBS.id}"
  instance_id = "${aws_instance.ICPEEL_PPS_GUIDEWIRE_APPLICATION_2.id}"
}

resource "aws_ebs_volume" "ICPEEL_PPS_GUIDEWIRE_APPLICATION_2_DOCKER_EBS" {
  availability_zone = "ca-central-1b"
  size              = 150
  type              = "gp3"
  encrypted         = true
  kms_key_id        = "arn:aws:kms:ca-central-1:005810662120:key/8f33a54d-e8c3-44fc-b083-69e56ffc28a2"
  tags = {
    Name = "ICPEEL_PPS_GUIDEWIRE_APPLICATION_2_DOCKER_EBS"
    Docker = "True"
  }
}

resource "aws_volume_attachment" "ICPEEL_PPS_GUIDEWIRE_APPLICATION_2_LOGS_EBS_ATTACHMENT" {
  device_name = "/dev/sdi"
  volume_id   = "${aws_ebs_volume.ICPEEL_PPS_GUIDEWIRE_APPLICATION_2_LOGS_EBS.id}"
  instance_id = "${aws_instance.ICPEEL_PPS_GUIDEWIRE_APPLICATION_2.id}"
}

resource "aws_ebs_volume" "ICPEEL_PPS_GUIDEWIRE_APPLICATION_2_LOGS_EBS" {
  availability_zone = "ca-central-1b"
  size              = 150
  type              = "gp3"
  encrypted         = true
  kms_key_id        = "arn:aws:kms:ca-central-1:005810662120:key/8f33a54d-e8c3-44fc-b083-69e56ffc28a2"
  tags = {
    Name = "ICPEEL_PPS_GUIDEWIRE_APPLICATION_2_LOGS_EBS"
    Docker = "True"
  }
}

############ External HAProxy Volumes ##################
resource "aws_volume_attachment" "ICPEEL_PPS_EXTERNAL_HAPROXY_1_DOCKER_EBS_ATTACHMENT" {
  device_name = "/dev/sdh"
  volume_id   = "${aws_ebs_volume.ICPEEL_PPS_EXTERNAL_HAPROXY_1_DOCKER_EBS.id}"
  instance_id = "${aws_instance.ICPEEL_PPS_EXTERNAL_HAPROXY_1.id}"
}

resource "aws_ebs_volume" "ICPEEL_PPS_EXTERNAL_HAPROXY_1_DOCKER_EBS" {
  availability_zone = "ca-central-1a"
  size              = 20
  type              = "gp3"
  encrypted         = true
  kms_key_id        = "arn:aws:kms:ca-central-1:005810662120:key/8f33a54d-e8c3-44fc-b083-69e56ffc28a2"
  tags = {
    Name = "ICPEEL_PPS_EXTERNAL_HAPROXY_1_DOCKER_EBS"
    Docker = "True"
  }
}

resource "aws_volume_attachment" "ICPEEL_PPS_EXTERNAL_HAPROXY_2_DOCKER_EBS_ATTACHMENT" {
  device_name = "/dev/sdh"
  volume_id   = "${aws_ebs_volume.ICPEEL_PPS_EXTERNAL_HAPROXY_2_DOCKER_EBS.id}"
  instance_id = "${aws_instance.ICPEEL_PPS_EXTERNAL_HAPROXY_2.id}"
}

resource "aws_ebs_volume" "ICPEEL_PPS_EXTERNAL_HAPROXY_2_DOCKER_EBS" {
  availability_zone = "ca-central-1b"
  size              = 20
  type              = "gp3"
  encrypted         = true
  kms_key_id        = "arn:aws:kms:ca-central-1:005810662120:key/8f33a54d-e8c3-44fc-b083-69e56ffc28a2"
  tags = {
    Name = "ICPEEL_PPS_EXTERNAL_HAPROXY_2_DOCKER_EBS"
    Docker = "True"
  }
}

############ Internal HAProxy Volumes ##################
resource "aws_volume_attachment" "ICPEEL_PPS_INTERNAL_HAPROXY_1_DOCKER_EBS_ATTACHMENT" {
  device_name = "/dev/sdh"
  volume_id   = "${aws_ebs_volume.ICPEEL_PPS_INTERNAL_HAPROXY_1_DOCKER_EBS.id}"
  instance_id = "${aws_instance.ICPEEL_PPS_INTERNAL_HAPROXY_1.id}"
}

resource "aws_ebs_volume" "ICPEEL_PPS_INTERNAL_HAPROXY_1_DOCKER_EBS" {
  availability_zone = "ca-central-1a"
  size              = 20
  type              = "gp3"
  encrypted         = true
  kms_key_id        = "arn:aws:kms:ca-central-1:005810662120:key/8f33a54d-e8c3-44fc-b083-69e56ffc28a2"
  tags = {
    Name = "ICPEEL_PPS_INTERNAL_HAPROXY_1_DOCKER_EBS"
    Docker = "True"
  }
}

resource "aws_volume_attachment" "ICPEEL_PPS_INTERNAL_HAPROXY_2_DOCKER_EBS_ATTACHMENT" {
  device_name = "/dev/sdh"
  volume_id   = "${aws_ebs_volume.ICPEEL_PPS_INTERNAL_HAPROXY_2_DOCKER_EBS.id}"
  instance_id = "${aws_instance.ICPEEL_PPS_INTERNAL_HAPROXY_2.id}"
}

resource "aws_ebs_volume" "ICPEEL_PPS_INTERNAL_HAPROXY_2_DOCKER_EBS" {
  availability_zone = "ca-central-1b"
  size              = 20
  type              = "gp3"
  encrypted         = true
  kms_key_id        = "arn:aws:kms:ca-central-1:005810662120:key/8f33a54d-e8c3-44fc-b083-69e56ffc28a2"
  tags = {
    Name = "ICPEEL_PPS_INTERNAL_HAPROXY_2_DOCKER_EBS"
    Docker = "True"
  }
}

############ Microservices Volumes ##################

resource "aws_volume_attachment" "ICPEEL_PPS_MICROSERVICES_MANAGER_1_DOCKER_EBS_ATTACHMENT" {
  device_name = "/dev/sdh"
  volume_id   = "${aws_ebs_volume.ICPEEL_PPS_MICROSERVICES_MANAGER_1_DOCKER_EBS.id}"
  instance_id = "${aws_instance.ICPEEL_PPS_MICROSERVICES_MANAGER_1.id}"
}

resource "aws_ebs_volume" "ICPEEL_PPS_MICROSERVICES_MANAGER_1_DOCKER_EBS" {
  availability_zone = "ca-central-1a"
  size              = 50
  type              = "gp3"
  encrypted         = true
  kms_key_id        = "arn:aws:kms:ca-central-1:005810662120:key/8f33a54d-e8c3-44fc-b083-69e56ffc28a2"
  tags = {
    Name = "ICPEEL_PPS_MICROSERVICES_MANAGER_1_DOCKER_EBS"
    Docker = "True"
  }
}

resource "aws_volume_attachment" "ICPEEL_PPS_MICROSERVICES_MANAGER_2_DOCKER_EBS_ATTACHMENT" {
  device_name = "/dev/sdh"
  volume_id   = "${aws_ebs_volume.ICPEEL_PPS_MICROSERVICES_MANAGER_2_DOCKER_EBS.id}"
  instance_id = "${aws_instance.ICPEEL_PPS_MICROSERVICES_MANAGER_2.id}"
}

resource "aws_ebs_volume" "ICPEEL_PPS_MICROSERVICES_MANAGER_2_DOCKER_EBS" {
  availability_zone = "ca-central-1b"
  size              = 50
  type              = "gp3"
  encrypted         = true
  kms_key_id        = "arn:aws:kms:ca-central-1:005810662120:key/8f33a54d-e8c3-44fc-b083-69e56ffc28a2"
  tags = {
    Name = "ICPEEL_PPS_MICROSERVICES_MANAGER_2_DOCKER_EBS"
    Docker = "True"
  }
}

resource "aws_volume_attachment" "ICPEEL_PPS_MICROSERVICES_MANAGER_3_DOCKER_EBS_ATTACHMENT" {
  device_name = "/dev/sdh"
  volume_id   = "${aws_ebs_volume.ICPEEL_PPS_MICROSERVICES_MANAGER_3_DOCKER_EBS.id}"
  instance_id = "${aws_instance.ICPEEL_PPS_MICROSERVICES_MANAGER_3.id}"
}

resource "aws_ebs_volume" "ICPEEL_PPS_MICROSERVICES_MANAGER_3_DOCKER_EBS" {
  availability_zone = "ca-central-1a"
  size              = 50
  type              = "gp3"
  encrypted         = true
  kms_key_id        = "arn:aws:kms:ca-central-1:005810662120:key/8f33a54d-e8c3-44fc-b083-69e56ffc28a2"
  tags = {
    Name = "ICPEEL_PPS_MICROSERVICES_MANAGER_3_DOCKER_EBS"
    Docker = "True"
  }
}

resource "aws_volume_attachment" "ICPEEL_PPS_MICROSERVICES_WORKER_1_DOCKER_EBS_ATTACHMENT" {
  device_name = "/dev/sdh"
  volume_id   = "${aws_ebs_volume.ICPEEL_PPS_MICROSERVICES_APPLICATION_1_DOCKER_EBS.id}"
  instance_id = "${aws_instance.ICPEEL_PPS_MICROSERVICES_APPLICATION_1.id}"
}

resource "aws_ebs_volume" "ICPEEL_PPS_MICROSERVICES_APPLICATION_1_DOCKER_EBS" {
  availability_zone = "ca-central-1a"
  size              = 150
  type              = "gp3"
  encrypted         = true
  kms_key_id        = "arn:aws:kms:ca-central-1:005810662120:key/8f33a54d-e8c3-44fc-b083-69e56ffc28a2"
  tags = {
    Name = "ICPEEL_PPS_MICROSERVICES_APPLICATION_1_DOCKER_EBS"
    Docker = "True"
  }
}

resource "aws_volume_attachment" "ICPEEL_PPS_MICROSERVICES_WORKER_2_DOCKER_EBS_ATTACHMENT" {
  device_name = "/dev/sdh"
  volume_id   = "${aws_ebs_volume.ICPEEL_PPS_MICROSERVICES_APPLICATION_2_DOCKER_EBS.id}"
  instance_id = "${aws_instance.ICPEEL_PPS_MICROSERVICES_APPLICATION_2.id}"
}

resource "aws_ebs_volume" "ICPEEL_PPS_MICROSERVICES_APPLICATION_2_DOCKER_EBS" {
  availability_zone = "ca-central-1b"
  size              = 150
  type              = "gp3"
  encrypted         = true
  kms_key_id        = "arn:aws:kms:ca-central-1:005810662120:key/8f33a54d-e8c3-44fc-b083-69e56ffc28a2"
  tags = {
    Name = "ICPEEL_PPS_MICROSERVICES_APPLICATION_2_DOCKER_EBS"
    Docker = "True"
  }
}

# #=======================================================================
# # PPS/DEV/QA
# #=======================================================================
############ Jenkins Client ##################
resource "aws_volume_attachment" "ICPEEL_DEV_JENKINS_CLIENT_DOCKER_EBS_ATTACHMENT" {
  device_name = "/dev/sdh"
  volume_id   = "${aws_ebs_volume.ICPEEL_DEV_JENKINS_CLIENT_DOCKER_EBS.id}"
  instance_id = "${aws_instance.ICPEEL_DEV_JENKINS_CLIENT.id}"
}

resource "aws_ebs_volume" "ICPEEL_DEV_JENKINS_CLIENT_DOCKER_EBS" {
  availability_zone = "${aws_instance.ICPEEL_DEV_JENKINS_CLIENT.availability_zone}"
  size              = 50
  type              = "gp3"
  encrypted         = true
  kms_key_id        = "arn:aws:kms:ca-central-1:005810662120:key/8f33a54d-e8c3-44fc-b083-69e56ffc28a2"

  tags = {
    Name          = "ICPEEL_DEV_JENKINS_CLIENT_DOCKER_EBS"
    Docker        = "True"
  }
}
