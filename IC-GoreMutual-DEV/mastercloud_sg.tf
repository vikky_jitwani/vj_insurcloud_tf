resource "aws_security_group" "SG_JOB_SCHEDULER" {
  name = "JOB-SCHEDULER-SG"

  ingress {
    from_port       = 4444
    to_port         = 4444
    protocol        = "tcp"
    cidr_blocks = [
      "${var.gore_dev_cidr}",
      "${var.gore_aviatrix_vpn}"
    ]
  }

  ingress {
    from_port       = 4444
    to_port         = 4444
    protocol        = "udp"
    cidr_blocks = [
      "${var.gore_dev_cidr}",
      "${var.gore_aviatrix_vpn}"
    ]
  }

  ingress {
    from_port       = 4446
    to_port         = 4446
    protocol        = "tcp"
    cidr_blocks = [
      "${var.gore_dev_cidr}",
      "${var.gore_aviatrix_vpn}"
    ]
  }

  ingress {
    from_port       = 4446
    to_port         = 4446
    protocol        = "udp"
    cidr_blocks = [
      "${var.gore_dev_cidr}",
      "${var.gore_aviatrix_vpn}"
    ]
  }

  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    Name = "JOB-SCHEDULER-SG"
  }
}

resource "aws_security_group" "SG_MICROSERVICES" {
  name = "MICROSERVICES-SG"

  ingress {
    from_port   = 8443
    to_port     = 8443
    protocol    = "tcp"
    security_groups = [
      "${aws_security_group.SG_EXTERNAL_HAPROXY.id}"
    ]
    description = "Orchestrator"
  }

  ingress {
    from_port   = 15672
    to_port     = 15672
    protocol    = "tcp"
    cidr_blocks = [
      "${var.gore_aviatrix_vpn}"
    ]
    security_groups = [
      "${aws_security_group.SG_INTERNAL_HAPROXY.id}"
    ]
    description = "RabbitMQ Management UI"
  }

  ingress {
    from_port   = 9002
    to_port     = 9002
    protocol    = "tcp"
    security_groups = [
      "${aws_security_group.SG_INTERNAL_HAPROXY.id}"
    ]
    description = "Edge"
  }

  ingress {
    from_port   = 2377
    to_port     = 2377
    protocol    = "tcp"
    cidr_blocks = [
      "${var.gore_dev_cidr}"
    ]
    description = "Cluster Management Communications"
  }

  ingress {
    from_port   = 7946
    to_port     = 7946
    protocol    = "udp"
    cidr_blocks = [
      "${var.gore_dev_cidr}"
    ]
    description = "Communication Among Nodes"
  }

  ingress {
    from_port   = 7946
    to_port     = 7946
    protocol    = "tcp"
    cidr_blocks = [
      "${var.gore_dev_cidr}"
    ]
    description = "Communication Among Nodes"
  }

  ingress {
    from_port   = 4789
    to_port     = 4789
    protocol    = "udp"
    cidr_blocks = [
      "${var.gore_dev_cidr}"
    ]
    description = "Overlay Network Traffic"
  }

  ingress {
    from_port   = 8500
    to_port     = 8500
    protocol    = "tcp"
    cidr_blocks = [
      "${var.gore_aviatrix_vpn}"
    ]
    description = "Consul UI"
  }

  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    Name = "MICROSERVICES-SG"
  }
}

resource "aws_security_group" "SG_MICROSERVICES_DEV" {
  name = "MICROSERVICES-DEV-SG"

  ingress {
    from_port   = 8443
    to_port     = 8443
    protocol    = "tcp"
    security_groups = [
      "${aws_security_group.SG_EXTERNAL_HAPROXY_DEV.id}"
    ]
    description = "Orchestrator"
  }
  ingress {
    from_port   = 8079
    to_port     = 8079
    protocol    = "tcp"
    cidr_blocks = [
      "${var.gore_aviatrix_vpn}"
    ]
    description = "mock-server"
  }

  ingress {
    from_port   = 15672
    to_port     = 15672
    protocol    = "tcp"
    cidr_blocks = [
      "${var.gore_aviatrix_vpn}" #TODO - review
    ]
    security_groups = [
      "${aws_security_group.SG_INTERNAL_HAPROXY_DEV.id}"
    ]
    description = "RabbitMQ Management UI"
  }

  ingress {
    from_port   = 9002
    to_port     = 9002
    protocol    = "tcp"
    security_groups = [
      "${aws_security_group.SG_INTERNAL_HAPROXY_DEV.id}"
    ]
    description = "Edge"
  }

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    security_groups = [
      "${aws_security_group.SG_INTERNAL_HAPROXY_DEV.id}"
    ]
    description = "Grafana"
  }

   ingress {
    from_port   = 9050
    to_port     = 9050
    protocol    = "tcp"
    security_groups = [
      "${aws_security_group.SG_INTERNAL_HAPROXY_DEV.id}"
    ]
    description = "Admin Service"
  }

  ingress {
    from_port   = 2377
    to_port     = 2377
    protocol    = "tcp"
    cidr_blocks = [
      "${var.gore_dev_cidr}"
    ]
    description = "Cluster Management Communications"
  }

  ingress {
    from_port   = 7946
    to_port     = 7946
    protocol    = "udp"
    cidr_blocks = [
      "${var.gore_dev_cidr}"
    ]
    description = "Communication Among Nodes"
  }

  ingress {
    from_port   = 7946
    to_port     = 7946
    protocol    = "tcp"
    cidr_blocks = [
      "${var.gore_dev_cidr}"
    ]
    description = "Communication Among Nodes"
  }

  ingress {
    from_port   = 4789
    to_port     = 4789
    protocol    = "udp"
    cidr_blocks = [
      "${var.gore_dev_cidr}"
    ]
    description = "Overlay Network Traffic"
  }

  ingress {
    from_port   = 8500
    to_port     = 8500
    protocol    = "tcp"
    cidr_blocks = [
      "${var.gore_aviatrix_vpn}"
    ]
    description = "Consul UI"
  }

  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    Name = "MICROSERVICES-DEV-SG"
  }
}

resource "aws_security_group" "SG_EXTERNAL_HAPROXY_DEV" {
  name = "EXTERNAL-HAPROXY-DEV-SG"

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = [
      "${var.gore_hub_cidr}" #TODO - review
    ]
    description = "HTTP"
  }

  ingress {
    from_port   = 8444
    to_port     = 8444
    protocol    = "tcp"
    cidr_blocks = [
      "${var.gore_dev_cidr}"
    ]
    description = "Health"
  }

  ingress {
    from_port   = 8404
    to_port     = 8404
    protocol    = "tcp"
    cidr_blocks = [
      "${var.gore_dev_cidr}",
      "${var.gore_aviatrix_vpn}"
    ]
    description = "Stats"
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    Name = "EXTERNAL-HAPROXY-DEV-SG"
  }
}

resource "aws_security_group" "SG_INTERNAL_HAPROXY_DEV" {
  name = "INTERNAL-HAPROXY-DEV-SG"

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = [
      "${var.gore_dev_cidr}",
      "${var.gore_aviatrix_vpn}",
      "${var.gore_cidr}",
      #"${var.gore_watchguard_cidr}",
      #"${var.gore_dmz_cidr}", # TODO - What's DMZ?
      #"${var.gore_wireless_cidr}",
      "${var.gore_hub_cidr}" # For Jobscheduler
    ]
    description = "HTTP"
  }

  ingress {
    from_port   = 8444
    to_port     = 8444
    protocol    = "tcp"
    cidr_blocks = [
      "${var.gore_dev_cidr}",
      "${var.gore_hub_cidr}"
    ]
    description = "Health"
  }

  ingress {
    from_port   = 8404
    to_port     = 8404
    protocol    = "tcp"
    cidr_blocks = [
      "${var.gore_dev_cidr}",
      "${var.gore_aviatrix_vpn}"
    ]
    description = "Stats"
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    Name = "INTERNAL-HAPROXY-DEV-SG"
  }
}

resource "aws_security_group" "SG_EXTERNAL_HAPROXY" {
  name = "EXTERNAL-HAPROXY-SG"

  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = [
      "${var.gore_hub_cidr}"
    ]
    description = "HTTPS"
  }

  ingress {
    from_port   = 8444
    to_port     = 8444
    protocol    = "tcp"
    cidr_blocks = [
      "${var.gore_dev_cidr}"
    ]
    description = "Health"
  }

  ingress {
    from_port   = 8404
    to_port     = 8404
    protocol    = "tcp"
    cidr_blocks = [
      "${var.gore_dev_cidr}",
      "${var.gore_aviatrix_vpn}"
    ]
    description = "Stats"
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    Name = "EXTERNAL-HAPROXY-SG"
  }
}

resource "aws_security_group" "SG_INTERNAL_HAPROXY" {
  name = "INTERNAL-HAPROXY-SG"

  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = [
      "${var.gore_dev_cidr}",
      "${var.gore_aviatrix_vpn}",
      "${var.gore_cidr}",
      #"${var.gore_watchguard_cidr}",
      #"${var.gore_dmz_cidr}",
      #"${var.gore_wireless_cidr}",
      "${var.gore_hub_cidr}" # For Jobscheduler
    ]
    description = "HTTPS"
  }

  ingress {
    from_port   = 8444
    to_port     = 8444
    protocol    = "tcp"
    cidr_blocks = [
      "${var.gore_dev_cidr}",
      "${var.gore_hub_cidr}"
    ]
    description = "Health"
  }

  ingress {
    from_port   = 8404
    to_port     = 8404
    protocol    = "tcp"
    cidr_blocks = [
      "${var.gore_dev_cidr}",
      "${var.gore_aviatrix_vpn}"
    ]
    description = "Stats"
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    Name = "INTERNAL-HAPROXY-SG"
  }
}

resource "aws_security_group" "SG_HAPROXY_INTERNAL_PEERING" {
  name = "HAPROXY-INTERNAL-PEERING-SG"

  ingress {
    from_port       = 1024
    to_port         = 1024
    protocol        = "tcp"
    security_groups = ["${aws_security_group.SG_INTERNAL_HAPROXY.id}"]
    description     = "HAProxy Peering"
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    Name = "HAPROXY-INTERNAL-PEERING-SG"
  }
}

resource "aws_security_group" "SG_HAPROXY_EXTERNAL_PEERING" {
  name = "HAPROXY-EXTERNAL-PEERING-SG"

  ingress {
    from_port       = 1024
    to_port         = 1024
    protocol        = "tcp"
    security_groups = ["${aws_security_group.SG_EXTERNAL_HAPROXY.id}"]
    description     = "HAProxy Peering"
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    Name = "HAPROXY-EXTERNAL-PEERING-SG"
  }
}

resource "aws_security_group" "SG_MYSQL_DB" {
  name = "MYSQL-DB-SG"

  ingress {
    from_port   = 3306
    to_port     = 3306
    protocol    = "tcp"
    cidr_blocks = [
      "${var.gore_dev_cidr}",
      "${var.gore_hub_cidr}",
      "${var.gore_aviatrix_vpn}"
    ]
  }

  ingress {
    from_port   = 3301
    to_port     = 3301
    protocol    = "tcp"
    cidr_blocks = [
      "${var.gore_dev_cidr}",
      "${var.gore_aviatrix_vpn}"
    ]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    Name = "MYSQL-DB-SG"
  }
}

resource "aws_security_group" "SG_MSSQL_DB" {
  name = "MSSQL-DB-SG"

  ingress {
    from_port   = 1433
    to_port     = 1433
    protocol    = "tcp"
    cidr_blocks = [
      "${var.gore_dev_cidr}",
      "${var.gore_hub_cidr}",
      "${var.gore_aviatrix_vpn}"
    ]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    Name = "MSSQL-DB-SG"
  }
}

resource "aws_security_group" "SG_MYSQL_DB_DEV" {
  name = "MYSQL-DB-SG-DEV"

  ingress {
    from_port   = 3306
    to_port     = 3306
    protocol    = "tcp"
    cidr_blocks = [
      "${var.gore_dev_cidr}",
      "${var.gore_hub_cidr}",
      "${var.gore_cidr}",
      "${var.gore_aviatrix_vpn}"
    ]
  }

  ingress {
    from_port   = 3301
    to_port     = 3301
    protocol    = "tcp"
    cidr_blocks = [
      "${var.gore_dev_cidr}",
      "${var.gore_aviatrix_vpn}"
    ]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    Name = "MYSQL-DB-SG"
  }
}

resource "aws_security_group" "SG_CLIENT_PING_TEST" {
  name = "CLIENT-PING-TEST"

  ingress {
    from_port       = -1
    to_port         = -1
    protocol        = "icmp"
    cidr_blocks     = [
      "${var.gore_cidr}",
      #"${var.gore_watchguard_cidr}",
      #"${var.gore_dmz_cidr}",
      #"${var.gore_wireless_cidr}"
    ]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    Name = "CLIENT-PING-TEST"
  }
}

resource "aws_security_group" "SG_MONITORING" {
  name = "MONITORING-SG"

  ingress {
    from_port   = 10000
    to_port     = 10001
    protocol    = "tcp"
    cidr_blocks = [
      "${var.gore_dev_cidr}",
      "${var.gore_aviatrix_vpn}"
    ]
    description = "Prometheus/Alertmanager"
  }

  ingress {
    from_port   = 3000
    to_port     = 3000
    protocol    = "tcp"
    cidr_blocks = [
      "${var.gore_dev_cidr}",
      "${var.gore_aviatrix_vpn}"
    ]
    description = "Grafana"
  }

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = [
      "${var.gore_dev_cidr}",
      "${var.gore_aviatrix_vpn}"
    ]
    description = "Grafana Login"
  }

  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    Name = "MONITORING-SG"
  }
}

resource "aws_security_group" "SG_AVIATRIX_RDP" {
  name = "AVIATRIX-RDP-SG"

  ingress {
    from_port   = 3389
    to_port     = 3389
    protocol    = "tcp"
    cidr_blocks = [
      "${var.gore_aviatrix_vpn}"
    ]
    description = "RDP"
  }

  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    Name = "AVIATRIX-RDP-SG"
  }
}

resource "aws_security_group" "SG_AVIATRIX_SSH" {
  name = "AVIATRIX-SSH-SG"

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = [
      "${var.gore_aviatrix_vpn}"
    ]
    description = "SSH"
  }

  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    Name = "AVIATRIX-SSH-SG"
  }
}

resource "aws_security_group" "SG_JOBSCHEDULER_SSH" {
  name = "JOBSCHEDULER-SSH-SG"
 
 ingress {
    from_port       = 22
    to_port         = 22
    protocol        = "tcp"
    security_groups = [
      "${aws_security_group.SG_JOB_SCHEDULER.id}"
    ]
  }

  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    Name = "JOBSCHEDULER-SSH-SG"
  }
}

resource "aws_security_group" "SG_JENKINS_CLIENT" {
  name = "JENKINS-CLIENT-SG"

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = [
      "10.0.0.0/8"
    ]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    Name = "JENKINS-CLIENT-SG"
  }
}
resource "aws_security_group" "SG_LAMBDA" {
  name = "LAMBDA-FUNCTION-SG"

  ingress {
    from_port   = 9002
    to_port     = 9002
    protocol    = "tcp"
	cidr_blocks = [
      "${var.gore_dev_cidr}"
    ]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    Name = "MICROSERVICES-DEV-LAMBDA-SG"
  }
}

resource "aws_security_group" "SG_JENKINS_CLIENT_SSH" {
  name = "JENKINS-CLIENT-SSH-SG"
 
  ingress {
    from_port       = 22
    to_port         = 22
    protocol        = "tcp"
    security_groups = [
      "${aws_security_group.SG_JENKINS_CLIENT.id}"
    ]
    description = "Jenkins Client SSH"
  }

  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    Name = "JENKINS-CLIENT-SSH-SG"
  }
}

resource "aws_security_group" "SG_OPENTEXT" {
  name = "OPENTEXT_SG"

  ingress {
    from_port   = 28700
    to_port     = 28702
    protocol    = "tcp"
    cidr_blocks = [
      "${var.gore_dev_cidr}",
      "${var.gore_hub_cidr}"
    ]
  }

  ingress {
    from_port   = 2701
    to_port     = 2702
    protocol    = "tcp"
    cidr_blocks = [
      "${var.gore_dev_cidr}",
      "${var.gore_hub_cidr}"
    ]
  }

  ingress {
    from_port   = 8080
    to_port     = 8080
    protocol    = "tcp"
    cidr_blocks = [
      "${var.gore_dev_cidr}",
      "${var.gore_hub_cidr}"
    ]
  }

  ingress {
    from_port   = 8443
    to_port     = 8443
    protocol    = "tcp"
    cidr_blocks = [
      "${var.gore_dev_cidr}",
      "${var.gore_hub_cidr}"
    ]
  }

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = [
      "${var.gore_dev_cidr}",
      "${var.gore_hub_cidr}"
    ]
  }

  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = [
      "${var.gore_dev_cidr}",
      "${var.gore_hub_cidr}"
    ]
  }

  ingress {
    from_port   = 5858
    to_port     = 5858
    protocol    = "tcp"
    cidr_blocks = [
      "${var.gore_dev_cidr}",
      "${var.gore_hub_cidr}"
    ]
  }

  ingress {
    from_port   = 3099
    to_port     = 3099
    protocol    = "tcp"
    cidr_blocks = [
      "${var.gore_dev_cidr}",
      "${var.gore_hub_cidr}"
    ]
  }

  ingress {
    from_port   = 2099
    to_port     = 2099
    protocol    = "tcp"
    cidr_blocks = [
      "${var.gore_dev_cidr}",
      "${var.gore_hub_cidr}"
    ]
  }

  ingress {
    from_port   = 389
    to_port     = 389
    protocol    = "tcp"
    cidr_blocks = [
      "${var.gore_dev_cidr}",
      "${var.gore_hub_cidr}"
    ]
  }

  ingress {
    from_port   = 1433
    to_port     = 1433
    protocol    = "tcp"
    cidr_blocks = [
      "${var.gore_dev_cidr}",
      "${var.gore_hub_cidr}"
    ]
  }

  ingress {
    from_port   = 8002
    to_port     = 8002
    protocol    = "tcp"
    cidr_blocks = [
      "${var.gore_dev_cidr}",
      "${var.gore_hub_cidr}"
    ]
  }

  ingress {
    from_port   = 4440
    to_port     = 4440
    protocol    = "tcp"
    cidr_blocks = [
      "${var.gore_dev_cidr}",
      "${var.gore_hub_cidr}"
    ]
  }

  ingress {
    from_port   = 8009
    to_port     = 8009
    protocol    = "tcp"
    cidr_blocks = [
      "${var.gore_dev_cidr}",
      "${var.gore_hub_cidr}"
    ]
  }

  ingress {
    from_port   = 4040
    to_port     = 4040
    protocol    = "tcp"
    cidr_blocks = [
      "${var.gore_dev_cidr}",
      "${var.gore_hub_cidr}"
    ]
  }

  ingress {
    from_port   = 8989
    to_port     = 8989
    protocol    = "tcp"
    cidr_blocks = [
      "${var.gore_dev_cidr}",
      "${var.gore_hub_cidr}"
    ]
  }

  ingress {
    from_port   = 41616
    to_port     = 41616
    protocol    = "tcp"
    cidr_blocks = [
      "${var.gore_dev_cidr}",
      "${var.gore_hub_cidr}"
    ]
  }

  ingress {
    from_port   = 5868
    to_port     = 5869
    protocol    = "tcp"
    cidr_blocks = [
      "${var.gore_dev_cidr}",
      "${var.gore_hub_cidr}"
    ]
  }

  ingress {
    from_port   = 8100
    to_port     = 8100
    protocol    = "tcp"
    cidr_blocks = [
      "${var.gore_dev_cidr}",
      "${var.gore_hub_cidr}"
    ]
  }

  ingress {
    from_port   = 8300
    to_port     = 8300
    protocol    = "tcp"
    cidr_blocks = [
      "${var.gore_dev_cidr}",
      "${var.gore_hub_cidr}"
    ]
  }

  ingress {
    from_port   = 8500
    to_port     = 8500
    protocol    = "tcp"
    cidr_blocks = [
      "${var.gore_dev_cidr}",
      "${var.gore_hub_cidr}"
    ]
  }

  ingress {
    from_port   = 8600
    to_port     = 8603
    protocol    = "tcp"
    cidr_blocks = [
      "${var.gore_dev_cidr}",
      "${var.gore_hub_cidr}"
    ]
  }

  ingress {
    from_port   = 8512
    to_port     = 8515
    protocol    = "tcp"
    cidr_blocks = [
      "${var.gore_dev_cidr}",
      "${var.gore_hub_cidr}"
    ]
  }

  ingress {
    from_port   = 2719
    to_port     = 2719
    protocol    = "tcp"
    cidr_blocks = [
      "${var.gore_dev_cidr}",
      "${var.gore_hub_cidr}"
    ]
  }

  ingress {
    from_port   = 135
    to_port     = 139
    protocol    = "tcp"
    cidr_blocks = [
      "${var.gore_dev_cidr}",
      "${var.gore_hub_cidr}"
    ]
  }

  ingress {
    from_port   = 135
    to_port     = 139
    protocol    = "udp"
    cidr_blocks = [
      "${var.gore_dev_cidr}",
      "${var.gore_hub_cidr}"
    ]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    Name = "EXSTREAM-SG"
  }
}

### Route53 Resolver - IC HUB DNS ###
resource "aws_security_group" "SG_IC_HUB_DNS_RESOLVER_ENDPOINT" {
  name = "IC-HUB-DNS-RESOLVER-ENDPOINT-SG"

  ingress {
    from_port   = 53
    to_port     = 53
    protocol    = "tcp"
    cidr_blocks = [
      "${var.vpc_cidr}"
    ]
    description = "DNS"
  }

  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    Name = "IC-HUB-DNS-RESOLVER-ENDPOINT-SG"
  }
}
