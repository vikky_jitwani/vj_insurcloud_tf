####### GW VOLUMES #############
resource "aws_volume_attachment" "ICPORTAGE_PROD_GUIDEWIRE_ONLINE_1_DOCKER_EBS_ATTACHMENT" {
  device_name = "/dev/sdh"
  volume_id   = "${aws_ebs_volume.ICPORTAGE_PROD_GUIDEWIRE_ONLINE_1_DOCKER_EBS.id}"
  instance_id = "${aws_instance.ICPORTAGE_PROD_GUIDEWIRE_ONLINE_1.id}"
}

resource "aws_ebs_volume" "ICPORTAGE_PROD_GUIDEWIRE_ONLINE_1_DOCKER_EBS" {
  availability_zone = "ca-central-1a"
  size              = 150
  type              = "gp2"
  encrypted         = true
  kms_key_id        = "arn:aws:kms:ca-central-1:625725589452:key/f98a8019-6c9e-4ba8-8173-a14465501bed"
  tags = {
    Name = "ICPORTAGE_PROD_GUIDEWIRE_ONLINE_1_DOCKER_EBS"
    Docker = "True"
  }
}

resource "aws_volume_attachment" "ICPORTAGE_PROD_GUIDEWIRE_ONLINE_1_LOGS_EBS_ATTACHMENT" {
  device_name = "/dev/sdi"
  volume_id   = "${aws_ebs_volume.ICPORTAGE_PROD_GUIDEWIRE_ONLINE_1_LOGS_EBS.id}"
  instance_id = "${aws_instance.ICPORTAGE_PROD_GUIDEWIRE_ONLINE_1.id}"
}

resource "aws_ebs_volume" "ICPORTAGE_PROD_GUIDEWIRE_ONLINE_1_LOGS_EBS" {
  availability_zone = "ca-central-1a"
  size              = 150
  type              = "gp2"
  encrypted         = true
  kms_key_id        = "arn:aws:kms:ca-central-1:625725589452:key/f98a8019-6c9e-4ba8-8173-a14465501bed"
  tags = {
    Name = "ICPORTAGE_PROD_GUIDEWIRE_ONLINE_1_LOGS_EBS"
    Docker = "True"
  }
}

resource "aws_volume_attachment" "ICPORTAGE_PROD_GUIDEWIRE_ONLINE_2_DOCKER_EBS_ATTACHMENT" {
  device_name = "/dev/sdh"
  volume_id   = "${aws_ebs_volume.ICPORTAGE_PROD_GUIDEWIRE_ONLINE_2_DOCKER_EBS.id}"
  instance_id = "${aws_instance.ICPORTAGE_PROD_GUIDEWIRE_ONLINE_2.id}"
}

resource "aws_ebs_volume" "ICPORTAGE_PROD_GUIDEWIRE_ONLINE_2_DOCKER_EBS" {
  availability_zone = "ca-central-1b"
  size              = 150
  type              = "gp2"
  encrypted         = true
  kms_key_id        = "arn:aws:kms:ca-central-1:625725589452:key/f98a8019-6c9e-4ba8-8173-a14465501bed"
  tags = {
    Name = "ICPORTAGE_PROD_GUIDEWIRE_ONLINE_2_DOCKER_EBS"
    Docker = "True"
  }
}

resource "aws_volume_attachment" "ICPORTAGE_PROD_GUIDEWIRE_ONLINE_2_LOGS_EBS_ATTACHMENT" {
  device_name = "/dev/sdi"
  volume_id   = "${aws_ebs_volume.ICPORTAGE_PROD_GUIDEWIRE_ONLINE_2_LOGS_EBS.id}"
  instance_id = "${aws_instance.ICPORTAGE_PROD_GUIDEWIRE_ONLINE_2.id}"
}

resource "aws_ebs_volume" "ICPORTAGE_PROD_GUIDEWIRE_ONLINE_2_LOGS_EBS" {
  availability_zone = "ca-central-1b"
  size              = 150
  type              = "gp2"
  encrypted         = true
  kms_key_id        = "arn:aws:kms:ca-central-1:625725589452:key/f98a8019-6c9e-4ba8-8173-a14465501bed"
  tags = {
    Name = "ICPORTAGE_PROD_GUIDEWIRE_ONLINE_2_LOGS_EBS"
    Docker = "True"
  }
}

resource "aws_volume_attachment" "ICPORTAGE_PROD_GUIDEWIRE_BATCH_1_DOCKER_EBS_ATTACHMENT" {
  device_name = "/dev/sdh"
  volume_id   = "${aws_ebs_volume.ICPORTAGE_PROD_GUIDEWIRE_BATCH_1_DOCKER_EBS.id}"
  instance_id = "${aws_instance.ICPORTAGE_PROD_GUIDEWIRE_BATCH_1.id}"
}

resource "aws_ebs_volume" "ICPORTAGE_PROD_GUIDEWIRE_BATCH_1_DOCKER_EBS" {
  availability_zone = "ca-central-1a"
  size              = 150
  type              = "gp2"
  encrypted         = true
  kms_key_id        = "arn:aws:kms:ca-central-1:625725589452:key/f98a8019-6c9e-4ba8-8173-a14465501bed"
  tags = {
    Name = "ICPORTAGE_PROD_GUIDEWIRE_BATCH_1_DOCKER_EBS"
    Docker = "True"
  }
}

resource "aws_volume_attachment" "ICPORTAGE_PROD_GUIDEWIRE_BATCH_1_LOGS_EBS_ATTACHMENT" {
  device_name = "/dev/sdi"
  volume_id   = "${aws_ebs_volume.ICPORTAGE_PROD_GUIDEWIRE_BATCH_1_LOGS_EBS.id}"
  instance_id = "${aws_instance.ICPORTAGE_PROD_GUIDEWIRE_BATCH_1.id}"
}

resource "aws_ebs_volume" "ICPORTAGE_PROD_GUIDEWIRE_BATCH_1_LOGS_EBS" {
  availability_zone = "ca-central-1a"
  size              = 150
  type              = "gp2"
  encrypted         = true
  kms_key_id        = "arn:aws:kms:ca-central-1:625725589452:key/f98a8019-6c9e-4ba8-8173-a14465501bed"
  tags = {
    Name = "ICPORTAGE_PROD_GUIDEWIRE_BATCH_1_LOGS_EBS"
    Docker = "True"
  }
}

resource "aws_volume_attachment" "ICPORTAGE_PROD_GUIDEWIRE_BATCH_2_DOCKER_EBS_ATTACHMENT" {
  device_name = "/dev/sdh"
  volume_id   = "${aws_ebs_volume.ICPORTAGE_PROD_GUIDEWIRE_BATCH_2_DOCKER_EBS.id}"
  instance_id = "${aws_instance.ICPORTAGE_PROD_GUIDEWIRE_BATCH_2.id}"
}

resource "aws_ebs_volume" "ICPORTAGE_PROD_GUIDEWIRE_BATCH_2_DOCKER_EBS" {
  availability_zone = "ca-central-1b"
  size              = 150
  type              = "gp2"
  encrypted         = true
  kms_key_id        = "arn:aws:kms:ca-central-1:625725589452:key/f98a8019-6c9e-4ba8-8173-a14465501bed"
  tags = {
    Name = "ICPORTAGE_PROD_GUIDEWIRE_BATCH_2_DOCKER_EBS"
    Docker = "True"
  }
}

resource "aws_volume_attachment" "ICPORTAGE_PROD_GUIDEWIRE_BATCH_2_LOGS_EBS_ATTACHMENT" {
  device_name = "/dev/sdi"
  volume_id   = "${aws_ebs_volume.ICPORTAGE_PROD_GUIDEWIRE_BATCH_2_LOGS_EBS.id}"
  instance_id = "${aws_instance.ICPORTAGE_PROD_GUIDEWIRE_BATCH_2.id}"
}

resource "aws_ebs_volume" "ICPORTAGE_PROD_GUIDEWIRE_BATCH_2_LOGS_EBS" {
  availability_zone = "ca-central-1b"
  size              = 150
  type              = "gp2"
  encrypted         = true
  kms_key_id        = "arn:aws:kms:ca-central-1:625725589452:key/f98a8019-6c9e-4ba8-8173-a14465501bed"
  tags = {
    Name = "ICPORTAGE_PROD_GUIDEWIRE_BATCH_2_LOGS_EBS"
    Docker = "True"
  }
}

############ External HAProxy Volumes ##################
resource "aws_volume_attachment" "ICPORTAGE_PROD_EXTERNAL_HAPROXY_1_DOCKER_EBS_ATTACHMENT" {
  device_name = "/dev/sdh"
  volume_id   = "${aws_ebs_volume.ICPORTAGE_PROD_EXTERNAL_HAPROXY_1_DOCKER_EBS.id}"
  instance_id = "${aws_instance.ICPORTAGE_PROD_EXTERNAL_HAPROXY_1.id}"
}

resource "aws_ebs_volume" "ICPORTAGE_PROD_EXTERNAL_HAPROXY_1_DOCKER_EBS" {
  availability_zone = "ca-central-1a"
  size              = 20
  encrypted         = true
  kms_key_id        = "arn:aws:kms:ca-central-1:625725589452:key/f98a8019-6c9e-4ba8-8173-a14465501bed"
  tags = {
    Name = "ICPORTAGE_PROD_EXTERNAL_HAPROXY_1_DOCKER_EBS"
    Docker = "True"
  }
}

resource "aws_volume_attachment" "ICPORTAGE_PROD_EXTERNAL_HAPROXY_2_DOCKER_EBS_ATTACHMENT" {
  device_name = "/dev/sdh"
  volume_id   = "${aws_ebs_volume.ICPORTAGE_PROD_EXTERNAL_HAPROXY_2_DOCKER_EBS.id}"
  instance_id = "${aws_instance.ICPORTAGE_PROD_EXTERNAL_HAPROXY_2.id}"
}

resource "aws_ebs_volume" "ICPORTAGE_PROD_EXTERNAL_HAPROXY_2_DOCKER_EBS" {
  availability_zone = "ca-central-1b"
  size              = 20
  encrypted         = true
  kms_key_id        = "arn:aws:kms:ca-central-1:625725589452:key/f98a8019-6c9e-4ba8-8173-a14465501bed"
  tags = {
    Name = "ICPORTAGE_PROD_EXTERNAL_HAPROXY_2_DOCKER_EBS"
    Docker = "True"
  }
}

############ Internal HAProxy Volumes ##################
resource "aws_volume_attachment" "ICPORTAGE_PROD_INTERNAL_HAPROXY_1_DOCKER_EBS_ATTACHMENT" {
  device_name = "/dev/sdh"
  volume_id   = "${aws_ebs_volume.ICPORTAGE_PROD_INTERNAL_HAPROXY_1_DOCKER_EBS.id}"
  instance_id = "${aws_instance.ICPORTAGE_PROD_INTERNAL_HAPROXY_1.id}"
}

resource "aws_ebs_volume" "ICPORTAGE_PROD_INTERNAL_HAPROXY_1_DOCKER_EBS" {
  availability_zone = "ca-central-1a"
  size              = 20
  encrypted         = true
  kms_key_id        = "arn:aws:kms:ca-central-1:625725589452:key/f98a8019-6c9e-4ba8-8173-a14465501bed"
  tags = {
    Name = "ICPORTAGE_PROD_INTERNAL_HAPROXY_1_DOCKER_EBS"
    Docker = "True"
  }
}

resource "aws_volume_attachment" "ICPORTAGE_PROD_INTERNAL_HAPROXY_2_DOCKER_EBS_ATTACHMENT" {
  device_name = "/dev/sdh"
  volume_id   = "${aws_ebs_volume.ICPORTAGE_PROD_INTERNAL_HAPROXY_2_DOCKER_EBS.id}"
  instance_id = "${aws_instance.ICPORTAGE_PROD_INTERNAL_HAPROXY_2.id}"
}

resource "aws_ebs_volume" "ICPORTAGE_PROD_INTERNAL_HAPROXY_2_DOCKER_EBS" {
  availability_zone = "ca-central-1b"
  size              = 20
  encrypted         = true
  kms_key_id        = "arn:aws:kms:ca-central-1:625725589452:key/f98a8019-6c9e-4ba8-8173-a14465501bed"
  tags = {
    Name = "ICPORTAGE_PROD_INTERNAL_HAPROXY_2_DOCKER_EBS"
    Docker = "True"
  }
}

############ Microservices Volumes ##################

resource "aws_volume_attachment" "ICPORTAGE_PROD_MICROSERVICES_MANAGER_1_DOCKER_EBS_ATTACHMENT" {
  device_name = "/dev/sdh"
  volume_id   = "${aws_ebs_volume.ICPORTAGE_PROD_MICROSERVICES_MANAGER_1_DOCKER_EBS.id}"
  instance_id = "${aws_instance.ICPORTAGE_PROD_MICROSERVICES_MANAGER_1.id}"
}

resource "aws_ebs_volume" "ICPORTAGE_PROD_MICROSERVICES_MANAGER_1_DOCKER_EBS" {
  availability_zone = "ca-central-1a"
  size              = 50
  encrypted         = true
  kms_key_id        = "arn:aws:kms:ca-central-1:625725589452:key/f98a8019-6c9e-4ba8-8173-a14465501bed"
  tags = {
    Name = "ICPORTAGE_PROD_MICROSERVICES_MANAGER_1_DOCKER_EBS"
    Docker = "True"
  }
}

resource "aws_volume_attachment" "ICPORTAGE_PROD_MICROSERVICES_MANAGER_2_DOCKER_EBS_ATTACHMENT" {
  device_name = "/dev/sdh"
  volume_id   = "${aws_ebs_volume.ICPORTAGE_PROD_MICROSERVICES_MANAGER_2_DOCKER_EBS.id}"
  instance_id = "${aws_instance.ICPORTAGE_PROD_MICROSERVICES_MANAGER_2.id}"
}

resource "aws_ebs_volume" "ICPORTAGE_PROD_MICROSERVICES_MANAGER_2_DOCKER_EBS" {
  availability_zone = "ca-central-1b"
  size              = 50
  encrypted         = true
  kms_key_id        = "arn:aws:kms:ca-central-1:625725589452:key/f98a8019-6c9e-4ba8-8173-a14465501bed"
  tags = {
    Name = "ICPORTAGE_PROD_MICROSERVICES_MANAGER_2_DOCKER_EBS"
    Docker = "True"
  }
}

resource "aws_volume_attachment" "ICPORTAGE_PROD_MICROSERVICES_MANAGER_3_DOCKER_EBS_ATTACHMENT" {
  device_name = "/dev/sdh"
  volume_id   = "${aws_ebs_volume.ICPORTAGE_PROD_MICROSERVICES_MANAGER_3_DOCKER_EBS.id}"
  instance_id = "${aws_instance.ICPORTAGE_PROD_MICROSERVICES_MANAGER_3.id}"
}

resource "aws_ebs_volume" "ICPORTAGE_PROD_MICROSERVICES_MANAGER_3_DOCKER_EBS" {
  availability_zone = "ca-central-1a"
  size              = 50
  encrypted         = true
  kms_key_id        = "arn:aws:kms:ca-central-1:625725589452:key/f98a8019-6c9e-4ba8-8173-a14465501bed"
  tags = {
    Name = "ICPORTAGE_PROD_MICROSERVICES_MANAGER_3_DOCKER_EBS"
    Docker = "True"
  }
}

resource "aws_volume_attachment" "ICPORTAGE_PROD_MICROSERVICES_WORKER_1_DOCKER_EBS_ATTACHMENT" {
  device_name = "/dev/sdh"
  volume_id   = "${aws_ebs_volume.ICPORTAGE_PROD_MICROSERVICES_APPLICATION_1_DOCKER_EBS.id}"
  instance_id = "${aws_instance.ICPORTAGE_PROD_MICROSERVICES_APPLICATION_1.id}"
}

resource "aws_ebs_volume" "ICPORTAGE_PROD_MICROSERVICES_APPLICATION_1_DOCKER_EBS" {
  availability_zone = "ca-central-1a"
  size              = 150
  encrypted         = true
  kms_key_id        = "arn:aws:kms:ca-central-1:625725589452:key/f98a8019-6c9e-4ba8-8173-a14465501bed"
  tags = {
    Name = "ICPORTAGE_PROD_MICROSERVICES_APPLICATION_1_DOCKER_EBS"
    Docker = "True"
  }
}

resource "aws_volume_attachment" "ICPORTAGE_PROD_MICROSERVICES_WORKER_2_DOCKER_EBS_ATTACHMENT" {
  device_name = "/dev/sdh"
  volume_id   = "${aws_ebs_volume.ICPORTAGE_PROD_MICROSERVICES_APPLICATION_2_DOCKER_EBS.id}"
  instance_id = "${aws_instance.ICPORTAGE_PROD_MICROSERVICES_APPLICATION_2.id}"
}

resource "aws_ebs_volume" "ICPORTAGE_PROD_MICROSERVICES_APPLICATION_2_DOCKER_EBS" {
  availability_zone = "ca-central-1b"
  size              = 150
  encrypted         = true
  kms_key_id        = "arn:aws:kms:ca-central-1:625725589452:key/f98a8019-6c9e-4ba8-8173-a14465501bed"
  tags = {
    Name = "ICPORTAGE_PROD_MICROSERVICES_APPLICATION_2_DOCKER_EBS"
    Docker = "True"
  }
}

############ DB Volumes ##################

resource "aws_volume_attachment" "ICPORTAGE_PROD_GUIDEWIRE_DB_WIN_EC2_EBS_ATTACHMENT" {
  device_name = "/dev/sdh"
  volume_id   = "${aws_ebs_volume.ICPORTAGE_PROD_GUIDEWIRE_DB_WIN_EC2_EBS.id}"
  instance_id = "${aws_instance.ICPORTAGE_PROD_GUIDEWIRE_MSSQL_DATABASE_1.id}"
}

resource "aws_ebs_volume" "ICPORTAGE_PROD_GUIDEWIRE_DB_WIN_EC2_EBS" {
  availability_zone = "ca-central-1a"
  size              = 500
  encrypted         = true
  kms_key_id        = "arn:aws:kms:ca-central-1:625725589452:key/f98a8019-6c9e-4ba8-8173-a14465501bed"
  tags = {
    Name = "ICPORTAGE_PROD_GUIDEWIRE_DB_WIN_EC2_EBS"
  }
}

resource "aws_volume_attachment" "ICPORTAGE_PROD_SAP_DB_WIN_EC2_EBS_ATTACHMENT" {
  device_name = "/dev/sdh"
  volume_id   = "${aws_ebs_volume.ICPORTAGE_PROD_SAP_DB_WIN_EC2_EBS.id}"
  instance_id = "${aws_instance.ICPORTAGE_PROD_SAP_SSIS_MSSQL_DATABASE_1.id}"
}

resource "aws_ebs_volume" "ICPORTAGE_PROD_SAP_DB_WIN_EC2_EBS" {
  availability_zone = "ca-central-1a"
  size              = 1500
  encrypted         = true
  kms_key_id        = "arn:aws:kms:ca-central-1:625725589452:key/f98a8019-6c9e-4ba8-8173-a14465501bed"
  tags = {
    Name = "ICPORTAGE_PROD_SAP_DB_WIN_EC2_EBS"
  }
}

resource "aws_volume_attachment" "ICPORTAGE_PROD_OT_DB_WIN_EC2_EBS_ATTACHMENT" {
  device_name = "/dev/sdh"
  volume_id   = "${aws_ebs_volume.ICPORTAGE_PROD_OT_DB_WIN_EC2_EBS.id}"
  instance_id = "${aws_instance.ICPORTAGE_PROD_OT_MSSQL_DATABASE_1.id}"
}

resource "aws_ebs_volume" "ICPORTAGE_PROD_OT_DB_WIN_EC2_EBS" {
  availability_zone = "ca-central-1a"
  size              = 1000
  encrypted         = true
  kms_key_id        = "arn:aws:kms:ca-central-1:625725589452:key/f98a8019-6c9e-4ba8-8173-a14465501bed"
  tags = {
    Name = "ICPORTAGE_PROD_OT_DB_WIN_EC2_EBS"
  }
}

############ Jenkins Client ##################
resource "aws_volume_attachment" "ICPORTAGE_PROD_JENKINS_CLIENT_DOCKER_EBS_ATTACHMENT" {
  device_name = "/dev/sdh"
  volume_id   = "${aws_ebs_volume.ICPORTAGE_PROD_JENKINS_CLIENT_DOCKER_EBS.id}"
  instance_id = "${aws_instance.ICPORTAGE_PROD_JENKINS_CLIENT.id}"
}

resource "aws_ebs_volume" "ICPORTAGE_PROD_JENKINS_CLIENT_DOCKER_EBS" {
  availability_zone = "${aws_instance.ICPORTAGE_PROD_JENKINS_CLIENT.availability_zone}"
  size              = 50
  type              = "gp2"
  encrypted         = true
  kms_key_id        = "arn:aws:kms:ca-central-1:625725589452:key/f98a8019-6c9e-4ba8-8173-a14465501bed"

  tags = {
    Name          = "ICPORTAGE_PROD_JENKINS_CLIENT_DOCKER_EBS"
    Docker        = "True"
  }
}
