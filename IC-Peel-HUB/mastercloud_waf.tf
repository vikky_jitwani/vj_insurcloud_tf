resource "aws_wafregional_sql_injection_match_set" "test_sql_injection_match_set" {
  name = "test_sql_injection_match_set"

  sql_injection_match_tuple {
    text_transformation = "URL_DECODE"

    field_to_match {
      type = "QUERY_STRING"
    }
  }
}

resource "aws_wafregional_rule" "blocksqlrule" {
  name        = "BlockSQL"
  metric_name = "BlockSQL"

  predicate {
    data_id = "${aws_wafregional_sql_injection_match_set.test_sql_injection_match_set.id}"
    negated = false
    type    = "SqlInjectionMatch"
  }
}

resource "aws_wafregional_web_acl" "blocksqlrule" {
  name        = "BlockSqlWebACL"
  metric_name = "BlockSqlWebACL"

  default_action {
    type = "ALLOW"
  }

  rule {
    action {
      type = "BLOCK"
    }

    priority = 1
    rule_id  = "${aws_wafregional_rule.blocksqlrule.id}"
    type     = "REGULAR"
  }
}

#to-do the association doesnt work 
# resource "aws_wafregional_web_acl_association" "GLCheque" {
#   resource_arn = "arn:aws:execute-api:${var.region}:${var.accountId}:${aws_api_gateway_rest_api.GLCheque.id}/*/stages/*"
#   web_acl_id   = "${aws_wafregional_web_acl.blocksqlrul.id}"
# }
