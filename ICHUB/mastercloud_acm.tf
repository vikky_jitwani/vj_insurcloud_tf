resource "aws_acm_certificate" "INTERNAL_ALB_LISTENER_PRIVATE_CERTIFICATE_CICD" {
  domain_name = "*.insurcloud.ca"
  certificate_authority_arn = "arn:aws:acm-pca:ca-central-1:020266858005:certificate-authority/49bebe34-3c02-4afb-9569-42e45da555c2"

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_acm_certificate" "INTERNAL_ALB_LISTENER_PRIVATE_CERTIFICATE_CMIG_CICD" {
  domain_name = "*.cmig.insurcloud.ca"
  certificate_authority_arn = "arn:aws:acm-pca:ca-central-1:020266858005:certificate-authority/49bebe34-3c02-4afb-9569-42e45da555c2"

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_acm_certificate" "INTERNAL_ALB_LISTENER_PRIVATE_CERTIFICATE_ECO_CICD" {
  domain_name = "*.economical.insurcloud.ca"
  certificate_authority_arn = "arn:aws:acm-pca:ca-central-1:020266858005:certificate-authority/49bebe34-3c02-4afb-9569-42e45da555c2"

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_acm_certificate" "INTERNAL_ALB_LISTENER_PRIVATE_CERTIFICATE_GORE_CICD" {
  domain_name = "*.gore.insurcloud.ca"
  certificate_authority_arn = "arn:aws:acm-pca:ca-central-1:020266858005:certificate-authority/49bebe34-3c02-4afb-9569-42e45da555c2"

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_acm_certificate" "INTERNAL_ALB_LISTENER_PRIVATE_CERTIFICATE_PORTAGE_CICD" {
  domain_name = "*.portage.insurcloud.ca"
  certificate_authority_arn = "arn:aws:acm-pca:ca-central-1:020266858005:certificate-authority/49bebe34-3c02-4afb-9569-42e45da555c2"

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_acm_certificate" "INTERNAL_ALB_LISTENER_PRIVATE_CERTIFICATE_BCAA_CICD" {
  domain_name = "*.bcaa.insurcloud.ca"
  certificate_authority_arn = "arn:aws:acm-pca:ca-central-1:020266858005:certificate-authority/49bebe34-3c02-4afb-9569-42e45da555c2"

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_lb_listener_certificate" "INTERNAL_ALB_LISTENER_CERTIFICATE_CMIG_CICD" {
  listener_arn    = "${aws_lb_listener.INTERNAL_ALB_LISTENER_CICD.arn}"
  certificate_arn = "${aws_acm_certificate.INTERNAL_ALB_LISTENER_PRIVATE_CERTIFICATE_CMIG_CICD.arn}"
}

resource "aws_lb_listener_certificate" "INTERNAL_ALB_LISTENER_CERTIFICATE_PEEL_CICD" {
  listener_arn    = "${aws_lb_listener.INTERNAL_ALB_LISTENER_CICD.arn}"
  # ARN of imported wildcard cert *.peel.insurcloud.ca
  certificate_arn = "arn:aws:acm:ca-central-1:020266858005:certificate/3709f9dd-23c1-447e-992f-618af91a9e1e"
}

resource "aws_lb_listener_certificate" "INTERNAL_ALB_LISTENER_CERTIFICATE_ECO_CICD" {
  listener_arn    = "${aws_lb_listener.INTERNAL_ALB_LISTENER_CICD.arn}"
  certificate_arn = "${aws_acm_certificate.INTERNAL_ALB_LISTENER_PRIVATE_CERTIFICATE_ECO_CICD.arn}"
}

resource "aws_lb_listener_certificate" "INTERNAL_ALB_LISTENER_CERTIFICATE_GORE_CICD" {
  listener_arn    = "${aws_lb_listener.INTERNAL_ALB_LISTENER_CICD.arn}"
  certificate_arn = "${aws_acm_certificate.INTERNAL_ALB_LISTENER_PRIVATE_CERTIFICATE_GORE_CICD.arn}"
}

resource "aws_lb_listener_certificate" "INTERNAL_ALB_LISTENER_CERTIFICATE_PORTAGE_CICD" {
  listener_arn    = "${aws_lb_listener.INTERNAL_ALB_LISTENER_CICD.arn}"
  certificate_arn = "${aws_acm_certificate.INTERNAL_ALB_LISTENER_PRIVATE_CERTIFICATE_PORTAGE_CICD.arn}"
}

resource "aws_lb_listener_certificate" "INTERNAL_ALB_LISTENER_CERTIFICATE_BCAA_CICD" {
  listener_arn    = "${aws_lb_listener.INTERNAL_ALB_LISTENER_CICD.arn}"
  certificate_arn = "${aws_acm_certificate.INTERNAL_ALB_LISTENER_PRIVATE_CERTIFICATE_BCAA_CICD.arn}"
}