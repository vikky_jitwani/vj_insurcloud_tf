provider "aws" {
  region     = "ca-central-1"
    version    = "~> 2.63.0" #Temporary until there is a fix for https://github.com/terraform-providers/terraform-provider-aws/issues/13549
}

terraform {
 backend "s3" {
    bucket     = "a-c-mc-p-a004-commonwell-tfstate"
    key        = "IC-Commonwell-HUB/STATE/current.tf"
    region     = "ca-central-1"
  }
}

data "terraform_remote_state" "dev_remote_state" {
  backend = "s3"
  config = {
    bucket     = "a-c-mc-p-a004-commonwell-tfstate"
    key        = "IC-Commonwell-DEV/STATE/current.tf"
    region     = "ca-central-1"
  }
}

data "terraform_remote_state" "prod_remote_state" {
  backend = "s3"
  config = {
    bucket     = "a-c-mc-p-a004-commonwell-tfstate"
    key        = "IC-Commonwell-PROD/STATE/current.tf"
    region     = "ca-central-1"
  }
}