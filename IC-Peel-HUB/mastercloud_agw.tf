#GLCheque API Gateways
resource "aws_api_gateway_rest_api" "GLCheque" {
  name        = "GLCheque"
  description = "This is the API gateway for Peel GLCheque API Gateway Collection."
  endpoint_configuration {
    types = ["REGIONAL"]
  }
}

#Token Auth DEV
resource "aws_api_gateway_authorizer" "Auth0" {
  name                   = "Auth0"
  rest_api_id            = "${aws_api_gateway_rest_api.GLCheque.id}"
  authorizer_uri         = "${aws_lambda_function.peel-APIGatewayAuthenrizer_Hub.invoke_arn}"
  authorizer_credentials = "arn:aws:iam::${var.peel-hub-account}:role/Auth0_invocation_role"
}

#Token Auth Prod 
resource "aws_api_gateway_authorizer" "Auth0_prod" {
  name                   = "Auth0_prod"
  rest_api_id            = "${aws_api_gateway_rest_api.GLCheque.id}"
  authorizer_uri         = "${aws_lambda_function.peelprod-APIGatewayAuthenrizer_Hub.invoke_arn}"
  authorizer_credentials = "arn:aws:iam::${var.peel-hub-account}:role/Auth0_invocation_role"
}

# #VPC Link peel-insurcloud
resource "aws_api_gateway_vpc_link" "peel-insurcloud-prod" {
  name        = "peel-insurcloud-prod"
  #target_arns = ["arn:aws:elasticloadbalancing:${var.region}:${var.peel-hub-account}:loadbalancer/net/peel-insurcloud/a5af8ea64200ff9e"]
  target_arns = ["${aws_lb.ICPEEL_PROD_APIGATEWAY_NLB.arn}"]
}

# #VPC Link peel-insurcloud-dev01
resource "aws_api_gateway_vpc_link" "peel-insurcloud-dev01" {
  name        = "peel-insurcloud-dev01"
  #target_arns = ["arn:aws:elasticloadbalancing:${var.region}:${var.peel-hub-account}:loadbalancer/net/peel-insurcloud-dev01/ac3d5916817d1ad2"]
  target_arns = ["${aws_lb.ICPEEL_DEV01_APIGATEWAY_NLB.arn}"]
}

# #VPC Link peel-insurcloud-pps
resource "aws_api_gateway_vpc_link" "peel-insurcloud-pps" {
  name        = "peel-insurcloud-pps"
  #target_arns = ["arn:aws:elasticloadbalancing:${var.region}:${var.peel-hub-account}:loadbalancer/net/peel-insurcloud-pps/d5263379262f668f"]
  target_arns = ["${aws_lb.ICPEEL_PPS_APIGATEWAY_NLB.arn}"]
}

#DEV01 Resource Group 
resource "aws_api_gateway_resource" "DEV01" {
  rest_api_id = "${aws_api_gateway_rest_api.GLCheque.id}"
  parent_id   = "${aws_api_gateway_rest_api.GLCheque.root_resource_id}"
  path_part   = "DEV01"
}

# Calls Peel Retrieve Cheque Entry MS in DEV
resource "aws_api_gateway_resource" "retrieveChequeEntries" {
  rest_api_id = "${aws_api_gateway_rest_api.GLCheque.id}"
  parent_id   = "${aws_api_gateway_resource.DEV01.id}"
  path_part   = "retrieveChequeEntries"
}

resource "aws_api_gateway_method" "retrieveChequeEntries" {
  rest_api_id   = "${aws_api_gateway_rest_api.GLCheque.id}"
  resource_id   = "${aws_api_gateway_resource.retrieveChequeEntries.id}"
  http_method   = "POST"
  authorization = "CUSTOM"
  authorizer_id = "${aws_api_gateway_authorizer.Auth0.id}"
}

resource "aws_api_gateway_integration" "retrieveChequeEntries" {
  rest_api_id             = "${aws_api_gateway_rest_api.GLCheque.id}"
  resource_id             = "${aws_api_gateway_resource.retrieveChequeEntries.id}"
  http_method             = "${aws_api_gateway_method.retrieveChequeEntries.http_method}"
  integration_http_method = "POST"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.peel-insurcloud-dev01.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.dev01.peel.insurcloud.ca/cheque-service/v1/cheque/retrieveChequeEntries"
}

resource "aws_api_gateway_method_response" "retrieveChequeEntries" {
  rest_api_id = "${aws_api_gateway_rest_api.GLCheque.id}"
  resource_id = "${aws_api_gateway_resource.retrieveChequeEntries.id}"
  http_method = "${aws_api_gateway_method.retrieveChequeEntries.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}

# Calls Peel Update Cheque Number MS in DEV
resource "aws_api_gateway_resource" "updateChequeNumbers" {
  rest_api_id = "${aws_api_gateway_rest_api.GLCheque.id}"
  parent_id   = "${aws_api_gateway_resource.DEV01.id}"
  path_part   = "updateChequeNumbers"
}

resource "aws_api_gateway_method" "updateChequeNumbers" {
  rest_api_id   = "${aws_api_gateway_rest_api.GLCheque.id}"
  resource_id   = "${aws_api_gateway_resource.updateChequeNumbers.id}"
  http_method   = "POST"
  authorization = "CUSTOM"
  authorizer_id = "${aws_api_gateway_authorizer.Auth0.id}"
}

resource "aws_api_gateway_integration" "updateChequeNumbers" {
  rest_api_id             = "${aws_api_gateway_rest_api.GLCheque.id}"
  resource_id             = "${aws_api_gateway_resource.updateChequeNumbers.id}"
  http_method             = "${aws_api_gateway_method.updateChequeNumbers.http_method}"
  integration_http_method = "POST"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.peel-insurcloud-dev01.id}" 
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.dev01.peel.insurcloud.ca/cheque-service/v1/cheque/updateChequeNumbers"
}

resource "aws_api_gateway_method_response" "updateChequeNumbers" {
  rest_api_id = "${aws_api_gateway_rest_api.GLCheque.id}"
  resource_id = "${aws_api_gateway_resource.updateChequeNumbers.id}"
  http_method = "${aws_api_gateway_method.updateChequeNumbers.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}

##PPS Resource Group 
resource "aws_api_gateway_resource" "PPS" {
  rest_api_id = "${aws_api_gateway_rest_api.GLCheque.id}"
  parent_id   = "${aws_api_gateway_rest_api.GLCheque.root_resource_id}"
  path_part   = "PPS"
}

# Calls Peel GL Feed Lambda Function 
resource "aws_api_gateway_resource" "PeelPPSGLFeed" {
  rest_api_id = "${aws_api_gateway_rest_api.GLCheque.id}"
  parent_id   = "${aws_api_gateway_resource.PPS.id}"
  path_part   = "GLFeed"
}

resource "aws_api_gateway_method" "PeelPPSGLFeed" {
  rest_api_id   = "${aws_api_gateway_rest_api.GLCheque.id}"
  resource_id   = "${aws_api_gateway_resource.PeelPPSGLFeed.id}"
  http_method   = "POST"
  authorization = "CUSTOM"
  authorizer_id = "${aws_api_gateway_authorizer.Auth0.id}"
}

resource "aws_api_gateway_integration" "PeelPPSGLFeed" {
  rest_api_id             = "${aws_api_gateway_rest_api.GLCheque.id}"
  resource_id             = "${aws_api_gateway_resource.PeelPPSGLFeed.id}"
  http_method             = "${aws_api_gateway_method.PeelPPSGLFeed.http_method}"
  integration_http_method = "POST"
  type                    = "AWS"
  uri                     = "arn:aws:apigateway:${var.region}:lambda:path/2015-03-31/functions/arn:aws:lambda:${var.region}:${var.peel-prod-account}:function:peel-PPSGLFeed/invocations"
}

resource "aws_api_gateway_method_response" "PeelPPSGLFeedresponse_200" {
  rest_api_id = "${aws_api_gateway_rest_api.GLCheque.id}"
  resource_id = "${aws_api_gateway_resource.PeelPPSGLFeed.id}"
  http_method = "${aws_api_gateway_method.PeelPPSGLFeed.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}


resource "aws_api_gateway_integration_response" "PeelPPSGLFeedIntegrationResponse" {
  rest_api_id = "${aws_api_gateway_rest_api.GLCheque.id}" 
  resource_id = "${aws_api_gateway_resource.PeelPPSGLFeed.id}"
  http_method = "${aws_api_gateway_method.PeelPPSGLFeed.http_method}"
  status_code = "${aws_api_gateway_method_response.PeelPPSGLFeedresponse_200.status_code}"
}

# Calls Peel GL Update Lambda Function 
resource "aws_api_gateway_resource" "PeelPPSGLUpdate" {
  rest_api_id = "${aws_api_gateway_rest_api.GLCheque.id}"
  parent_id   = "${aws_api_gateway_resource.PPS.id}"
  path_part   = "GLUpdate"
}

resource "aws_api_gateway_method" "PeelPPSGLUpdate" {
  rest_api_id   = "${aws_api_gateway_rest_api.GLCheque.id}"
  resource_id   = "${aws_api_gateway_resource.PeelPPSGLUpdate.id}"
  http_method   = "POST"
  authorization = "CUSTOM"
  authorizer_id = "${aws_api_gateway_authorizer.Auth0.id}"
}

resource "aws_api_gateway_integration" "PeelPPSGLUpdate" {
  rest_api_id             = "${aws_api_gateway_rest_api.GLCheque.id}"
  resource_id             = "${aws_api_gateway_resource.PeelPPSGLUpdate.id}"
  http_method             = "${aws_api_gateway_method.PeelPPSGLUpdate.http_method}"
  integration_http_method = "POST"
  type                    = "AWS"
  uri                     = "arn:aws:apigateway:${var.region}:lambda:path/2015-03-31/functions/arn:aws:lambda:${var.region}:${var.peel-prod-account}:function:peel-PPSGLUpdate/invocations"
}

resource "aws_api_gateway_method_response" "PeelPPSGLUpdateresponse_200" {
  rest_api_id = "${aws_api_gateway_rest_api.GLCheque.id}"
  resource_id = "${aws_api_gateway_resource.PeelPPSGLUpdate.id}"
  http_method = "${aws_api_gateway_method.PeelPPSGLUpdate.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}

resource "aws_api_gateway_integration_response" "PeelPPSGLUpdateIntegrationResponse" {
  rest_api_id = "${aws_api_gateway_rest_api.GLCheque.id}"
  resource_id = "${aws_api_gateway_resource.PeelPPSGLUpdate.id}"
  http_method = "${aws_api_gateway_method.PeelPPSGLUpdate.http_method}"
  status_code = "${aws_api_gateway_method_response.PeelPPSGLUpdateresponse_200.status_code}"
}

# Calls Peel Retrieve Cheque Entry MS in PPS
resource "aws_api_gateway_resource" "PPSretrieveChequeEntries" {
  rest_api_id = "${aws_api_gateway_rest_api.GLCheque.id}"
  parent_id   = "${aws_api_gateway_resource.PPS.id}"
  path_part   = "retrieveChequeEntries"
}

resource "aws_api_gateway_method" "PPSretrieveChequeEntries" {
  rest_api_id   = "${aws_api_gateway_rest_api.GLCheque.id}"
  resource_id   = "${aws_api_gateway_resource.PPSretrieveChequeEntries.id}"
  http_method   = "POST"
  authorization = "CUSTOM"
  authorizer_id = "${aws_api_gateway_authorizer.Auth0.id}"
}

resource "aws_api_gateway_integration" "PPSretrieveChequeEntries" {
  rest_api_id             = "${aws_api_gateway_rest_api.GLCheque.id}"
  resource_id             = "${aws_api_gateway_resource.PPSretrieveChequeEntries.id}"
  http_method             = "${aws_api_gateway_method.PPSretrieveChequeEntries.http_method}"
  integration_http_method = "POST"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.peel-insurcloud-pps.id}"
  type                    = "HTTP_PROXY"
  uri                     = "https://ms-pps.nonprod.peel.insurcloud.ca/cheque-service/v1/cheque/retrieveChequeEntries"
}

resource "aws_api_gateway_method_response" "PPSretrieveChequeEntries" {
  rest_api_id = "${aws_api_gateway_rest_api.GLCheque.id}"
  resource_id = "${aws_api_gateway_resource.PPSretrieveChequeEntries.id}"
  http_method = "${aws_api_gateway_method.PPSretrieveChequeEntries.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}

# Calls Peel Update Cheque Number MS in PPS
resource "aws_api_gateway_resource" "PPSupdateChequeNumbers" {
  rest_api_id = "${aws_api_gateway_rest_api.GLCheque.id}"
  parent_id   = "${aws_api_gateway_resource.PPS.id}"
  path_part   = "updateChequeNumbers"
}

resource "aws_api_gateway_method" "PPSupdateChequeNumbers" {
  rest_api_id   = "${aws_api_gateway_rest_api.GLCheque.id}"
  resource_id   = "${aws_api_gateway_resource.PPSupdateChequeNumbers.id}"
  http_method   = "POST"
  authorization = "CUSTOM"
  authorizer_id = "${aws_api_gateway_authorizer.Auth0.id}"
}

resource "aws_api_gateway_integration" "PPSupdateChequeNumbers" {
  rest_api_id             = "${aws_api_gateway_rest_api.GLCheque.id}"
  resource_id             = "${aws_api_gateway_resource.PPSupdateChequeNumbers.id}"
  http_method             = "${aws_api_gateway_method.PPSupdateChequeNumbers.http_method}"
  integration_http_method = "POST"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.peel-insurcloud-pps.id}" 
  type                    = "HTTP_PROXY"
  uri                     = "https://ms-pps.nonprod.peel.insurcloud.ca/cheque-service/v1/cheque/updateChequeNumbers"
}

resource "aws_api_gateway_method_response" "PPSupdateChequeNumbers" {
  rest_api_id = "${aws_api_gateway_rest_api.GLCheque.id}"
  resource_id = "${aws_api_gateway_resource.PPSupdateChequeNumbers.id}"
  http_method = "${aws_api_gateway_method.PPSupdateChequeNumbers.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}

#Prod Resource Group 
resource "aws_api_gateway_resource" "PROD" {
  rest_api_id = "${aws_api_gateway_rest_api.GLCheque.id}"
  parent_id   = "${aws_api_gateway_rest_api.GLCheque.root_resource_id}"
  path_part   = "PROD"
}

resource "aws_api_gateway_resource" "PeelPRODGLFeed" {
  rest_api_id = "${aws_api_gateway_rest_api.GLCheque.id}"
  parent_id   = "${aws_api_gateway_resource.PROD.id}"
  path_part   = "GLFeed"
}

resource "aws_api_gateway_method" "PeelPRODGLFeed" {
  rest_api_id   = "${aws_api_gateway_rest_api.GLCheque.id}"
  resource_id   = "${aws_api_gateway_resource.PeelPRODGLFeed.id}"
  http_method   = "POST"
  authorization = "CUSTOM"
  authorizer_id = "${aws_api_gateway_authorizer.Auth0_prod.id}"
}

resource "aws_api_gateway_integration" "PeelPRODGLFeed" {
  rest_api_id             = "${aws_api_gateway_rest_api.GLCheque.id}"
  resource_id             = "${aws_api_gateway_resource.PeelPRODGLFeed.id}"
  http_method             = "${aws_api_gateway_method.PeelPRODGLFeed.http_method}"
  integration_http_method = "POST"
  type                    = "AWS"
  uri                     = "arn:aws:apigateway:${var.region}:lambda:path/2015-03-31/functions/arn:aws:lambda:${var.region}:${var.peel-prod-account}:function:peel-ProdGLFeed/invocations"
}

resource "aws_api_gateway_method_response" "PeelPRODGLFeedresponse_200" {
  rest_api_id = "${aws_api_gateway_rest_api.GLCheque.id}"
  resource_id = "${aws_api_gateway_resource.PeelPRODGLFeed.id}"
  http_method = "${aws_api_gateway_method.PeelPRODGLFeed.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}

resource "aws_api_gateway_integration_response" "PeelPRODGLFeedIntegrationResponse" {
  rest_api_id = "${aws_api_gateway_rest_api.GLCheque.id}"
  resource_id = "${aws_api_gateway_resource.PeelPRODGLFeed.id}"
  http_method = "${aws_api_gateway_method.PeelPRODGLFeed.http_method}"
  status_code = "${aws_api_gateway_method_response.PeelPRODGLFeedresponse_200.status_code}"
}

resource "aws_api_gateway_resource" "PeelPRODGLUpdate" {
  rest_api_id = "${aws_api_gateway_rest_api.GLCheque.id}"
  parent_id   = "${aws_api_gateway_resource.PROD.id}"
  path_part   = "GLUpdate"
}

resource "aws_api_gateway_method" "PeelPRODGLUpdate" {
  rest_api_id   = "${aws_api_gateway_rest_api.GLCheque.id}"
  resource_id   = "${aws_api_gateway_resource.PeelPRODGLUpdate.id}"
  http_method   = "POST"
  authorization = "CUSTOM"
  authorizer_id = "${aws_api_gateway_authorizer.Auth0_prod.id}"
}

resource "aws_api_gateway_integration" "PeelPRODGLUpdate" {
  rest_api_id             = "${aws_api_gateway_rest_api.GLCheque.id}"
  resource_id             = "${aws_api_gateway_resource.PeelPRODGLUpdate.id}"
  http_method             = "${aws_api_gateway_method.PeelPRODGLUpdate.http_method}"
  integration_http_method = "POST"
  type                    = "AWS"
  uri                     = "arn:aws:apigateway:${var.region}:lambda:path/2015-03-31/functions/arn:aws:lambda:${var.region}:${var.peel-prod-account}:function:peel-ProdGLUpdate/invocations"
}

resource "aws_api_gateway_method_response" "PeelPRODGLUpdateresponse_200" {
  rest_api_id = "${aws_api_gateway_rest_api.GLCheque.id}"
  resource_id = "${aws_api_gateway_resource.PeelPRODGLUpdate.id}"
  http_method = "${aws_api_gateway_method.PeelPRODGLUpdate.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}

resource "aws_api_gateway_integration_response" "PeelPRODGLUpdateIntegrationResponse" {
  rest_api_id = "${aws_api_gateway_rest_api.GLCheque.id}"
  resource_id = "${aws_api_gateway_resource.PeelPRODGLUpdate.id}"
  http_method = "${aws_api_gateway_method.PeelPRODGLUpdate.http_method}"
  status_code = "${aws_api_gateway_method_response.PeelPRODGLUpdateresponse_200.status_code}"
}

# Calls Peel Retrieve Cheque Entry MS in PROD Env 
resource "aws_api_gateway_resource" "PRODretrieveChequeEntries" {
  rest_api_id = "${aws_api_gateway_rest_api.GLCheque.id}"
  parent_id   = "${aws_api_gateway_resource.PROD.id}"
  path_part   = "retrieveChequeEntries"
}

resource "aws_api_gateway_method" "PRODretrieveChequeEntries" {
  rest_api_id   = "${aws_api_gateway_rest_api.GLCheque.id}"
  resource_id   = "${aws_api_gateway_resource.PRODretrieveChequeEntries.id}"
  http_method   = "POST"
  authorization = "CUSTOM"
  authorizer_id = "${aws_api_gateway_authorizer.Auth0_prod.id}"
}

resource "aws_api_gateway_integration" "PRODretrieveChequeEntries" {
  rest_api_id             = "${aws_api_gateway_rest_api.GLCheque.id}"
  resource_id             = "${aws_api_gateway_resource.PRODretrieveChequeEntries.id}"
  http_method             = "${aws_api_gateway_method.PRODretrieveChequeEntries.http_method}"
  integration_http_method = "POST"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.peel-insurcloud-prod.id}" 
  type                    = "HTTP_PROXY"
  uri                     = "https://ms.peel.insurcloud.ca/cheque-service/v1/cheque/retrieveChequeEntries"
}

resource "aws_api_gateway_method_response" "PRODretrieveChequeEntries" {
  rest_api_id = "${aws_api_gateway_rest_api.GLCheque.id}"
  resource_id = "${aws_api_gateway_resource.PRODretrieveChequeEntries.id}"
  http_method = "${aws_api_gateway_method.PRODretrieveChequeEntries.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}

# Calls Peel Update Cheque Number MS in PROD
resource "aws_api_gateway_resource" "PRODupdateChequeNumbers" {
  rest_api_id = "${aws_api_gateway_rest_api.GLCheque.id}"
  parent_id   = "${aws_api_gateway_resource.PROD.id}"
  path_part   = "updateChequeNumbers"
}

resource "aws_api_gateway_method" "PRODupdateChequeNumbers" {
  rest_api_id   = "${aws_api_gateway_rest_api.GLCheque.id}"
  resource_id   = "${aws_api_gateway_resource.PRODupdateChequeNumbers.id}"
  http_method   = "POST"
  authorization = "CUSTOM"
  authorizer_id = "${aws_api_gateway_authorizer.Auth0_prod.id}"
}

resource "aws_api_gateway_integration" "PRODupdateChequeNumbers" {
  rest_api_id             = "${aws_api_gateway_rest_api.GLCheque.id}"
  resource_id             = "${aws_api_gateway_resource.PRODupdateChequeNumbers.id}"
  http_method             = "${aws_api_gateway_method.PRODupdateChequeNumbers.http_method}"
  integration_http_method = "POST"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.peel-insurcloud-prod.id}" 
  type                    = "HTTP_PROXY"
  uri                     = "https://ms.peel.insurcloud.ca/cheque-service/v1/cheque/updateChequeNumbers"
}

resource "aws_api_gateway_method_response" "PRODupdateChequeNumbers" {
  rest_api_id = "${aws_api_gateway_rest_api.GLCheque.id}"
  resource_id = "${aws_api_gateway_resource.PRODupdateChequeNumbers.id}"
  http_method = "${aws_api_gateway_method.PRODupdateChequeNumbers.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}

# Deployment stages in DEV01, PPS and PROD
# resource "aws_api_gateway_deployment" "PEEL" {
#   depends_on = ["aws_api_gateway_integration.retrieveChequeEntries"]
#   rest_api_id = "${aws_api_gateway_rest_api.GLCheque.id}"
#   stage_name  = "PEEL"
# }

#waf and logs needs to be set at stage level afterwards 