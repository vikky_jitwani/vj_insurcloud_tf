#Envrionment specific variables
client_name      = "IC-Portage"
environment_name = "DEV"
environment_type = "NP"
client_number    = "A005"
#ad-dc1 = "10.10.4.10"
#ad-dc2 = "10.10.6.10"
volume_key                                   = "arn:aws:kms:ca-central-1:091072499599:key/9fc4bcc6-13f4-4779-8eb6-2291b5c4a586"
ami_id_windows2012sql14enterprise            = "ami-0b7e5050eee245b99"
ami_id_windows2019container                  = "ami-013453540e1aa0d48"
ami_id_windows2019base                       = "ami-06b8f69a907cf6bb2"
ami_id_windows2016base20200806               = "ami-0388b8d3cb35753f8"
ami_id_windows2016base                       = "ami-0bce38ba7ac166aa9"
ami_id_windows2012R2base                     = "ami-08f7dedaab95e4621"
ami_id_windows2008base                       = "ami-0446e08d2d99fc8c9"
ami_id_redhatlinux75                         = "ami-05c33d286020a47f9"
ami_id_ubuntu1804baseimage                   = "ami-032172cc5f34b32fc"
ami_id_ubuntu1604baseimage                   = "ami-04f40cca01b3186ea"
ami_id_amazonlinux201712base                 = "ami-0a777bbf8ef3f43bf"
ami_id_amazonlinux201709base                 = "ami-058dba934995c5468"
ami_id_amazonlinux2base                      = "ami-071594d49d11fcb38"
ami_id_amazonlinux201803base                 = "ami-06829694f407e15c1"
ami_id_amazonlinuxbase20200806               = "ami-04978aa25eaba28b7"
ami_id_windowsdhicsap                        = "ami-059279d4e15aac67c"
ami_id_windowsdhicdb                         = "ami-0115ce0dc99c883bf"
ami_id_windows2016base20201216_win_ec2       = "ami-03023a3299686fb77" # take it from A-MC-CAC-D-A005-PORTAGE-DEV01-DHIC-SERVER-WIN-EC2
ami_id_windows2016base20201216_mssql_win_ec2 = "ami-030001eb0f89d33e7" # take it from A-MC-CAC-D-A005-PORTAGE-DEV01-DHIC-DATABASE-MSSQL-WIN-EC2
ami_id_windows_base                          = "ami-03023a3299686fb77"
ami_id_windows_mssql_base                    = "ami-030001eb0f89d33e7"

#VPC Specific variables
vpc_id       = "vpc-0d721640c30de46fa"
vpc_cidr     = "10.171.8.0/22"
account_cidr = "10.171.8.0/22"
#sg_cidr = "XXX"

portage-dev-database-1a-subnet = "subnet-0706c472312e67bab"
portage-dev-database-1b-subnet = "subnet-0038d1ea85da4d00a"
portage-dev-private-1a-subnet  = "subnet-091578c4583f10acf"
portage-dev-private-1b-subnet  = "subnet-0ff16bf736a69fc76"

#RDS
mssql-rds-password    = "Toronto12345"
mysql-rds-password    = "Toronto12345"
postgres-rds-password = "Toronto12345"

#Accounts
portage-dev-account  = "091072499599"
portage-prod-account = "625725589452"
portage-hub-account  = "342339668068"
