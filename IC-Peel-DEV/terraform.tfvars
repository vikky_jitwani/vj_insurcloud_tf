#Envrionment specific variables
client_name = "IC-Peel"
environment_name = "DEV"
environment_type = "NP"
client_number = "A003"
#ad-dc1 = "10.10.4.10"
#ad-dc2 = "10.10.6.10"
volume_key = "arn:aws:kms:ca-central-1:005810662120:key/8f33a54d-e8c3-44fc-b083-69e56ffc28a2"
ami_id_windows2012sql14enterprise = "ami-07a67e65265ba59dc"
ami_id_windows2019container = "ami-013453540e1aa0d48"
ami_id_windows2019base = "ami-06b8f69a907cf6bb2"
ami_id_windows2016base = "ami-0bce38ba7ac166aa9"
ami_id_windows2012R2base = "ami-09b707ce10f2e9cee"
ami_id_windows2008base = "ami-0446e08d2d99fc8c9"
ami_id_redhatlinux75 = "ami-05c33d286020a47f9"
ami_id_ubuntu1804baseimage = "ami-032172cc5f34b32fc"
ami_id_ubuntu1604baseimage = "ami-04f40cca01b3186ea"
ami_id_amazonlinux201712base = "ami-0a777bbf8ef3f43bf"
ami_id_amazonlinux201709base = "ami-058dba934995c5468"
ami_id_amazonlinux2base = "ami-0ad9abdd25c431def"
ami_id_amazonlinux201803base = "ami-06829694f407e15c1"


#VPC Specific variables
vpc_id = "vpc-0339588969d277249"
vpc_cidr = "10.11.0.0/22"
account_cidr = "10.11.0.0/22"
#sg_cidr = "XXX"

peel-dev-database-1a-subnet = "subnet-037940ce3114f81bc"
peel-dev-database-1b-subnet = "subnet-005c545fffdc279a1"
peel-dev-private-1a-subnet = "subnet-0a89b4d62a3d76b56"
peel-dev-private-1b-subnet = "subnet-0d9732ba6b5a72e0d"

#RDS
mssql-rds-password = "Toronto12345"
mysql-rds-password = "Toronto12345"
postgres-rds-password = "Toronto12345"

#Accounts
peel-dev-account = "005810662120"
peel-prod-account = "413329031558"
peel-hub-account = "186239701968"
