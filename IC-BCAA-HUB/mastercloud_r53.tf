// #=======================================================================
// # HUB 
// #=======================================================================

resource "aws_route53_record" "ICBCAA_NEXUS_REG_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "nexus.bcaa.insurcloud.ca"
    type    = "A"

    alias {
        name                   = "internal-cicd-internal-alb-2023433918.ca-central-1.elb.amazonaws.com"
        zone_id                = "ZQSVJUPU6J1EY"
        evaluate_target_health = false
    }
}

# resource "aws_route53_record" "ICBCAA_NEXUS_UI_RECORD" {
#     zone_id = "${var.zone_id}"
#     name    = "nexus-ui.bcaa.insurcloud.ca"
#     type    = "A"

#     alias {
#         name                   = "internal-cicd-internal-alb-2023433918.ca-central-1.elb.amazonaws.com"
#         zone_id                = "ZQSVJUPU6J1EY"
#         evaluate_target_health = false
#     }
# }

resource "aws_route53_record" "ICBCAA_SONARQUBE_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "sonarqube.bcaa.insurcloud.ca"
    type    = "A"

    alias {
        name                   = "internal-cicd-internal-alb-2023433918.ca-central-1.elb.amazonaws.com"
        zone_id                = "ZQSVJUPU6J1EY"
        evaluate_target_health = false
    }
}

resource "aws_route53_record" "ICBCAA_CONSUL_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "consul.bcaa.insurcloud.ca"
    type    = "A"

    alias {
        name                   = "internal-cicd-internal-alb-2023433918.ca-central-1.elb.amazonaws.com"
        zone_id                = "ZQSVJUPU6J1EY"
        evaluate_target_health = false
    }
}

resource "aws_route53_record" "ICBCAA_JENKINS_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "jenkins.bcaa.insurcloud.ca"
    type    = "A"

    alias {
        name                   = "internal-cicd-internal-alb-2023433918.ca-central-1.elb.amazonaws.com"
        zone_id                = "ZQSVJUPU6J1EY"
        evaluate_target_health = false
    }
}

# #=======================================================================
# # DEV01
# #=======================================================================

######### GUIDEWIRE ##############
resource "aws_route53_record" "ICBCAA_DEV01_GUIDEWIRE_SUITE_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "gw1.dev01.bcaa.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICBCAA_DEV01_GUIDEWIRE_SUITE_PRIVATEIP}"]
}

resource "aws_route53_record" "ICBCAA_DEV01_GUIDEWIRE_DB_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "gwdb.dev01.bcaa.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICBCAA_DEV01_GUIDEWIRE_SUITE_PRIVATEIP}"]
}

resource "aws_route53_record" "ICBCAA_DEV01_INTERNAL_NLB_GUIDEWIRE_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "gw.dev01.bcaa.insurcloud.ca"
    type    = "A"
    alias {
        name                   = "${data.terraform_remote_state.dev_remote_state.ICBCAA_DEV01_INTERNAL_NLB_DNS_NAME}"
        zone_id                = "${data.terraform_remote_state.dev_remote_state.ICBCAA_DEV01_INTERNAL_NLB_ZONE_ID}"
        evaluate_target_health = false
    }
}

resource "aws_route53_record" "ICBCAA_DEV01_GUIDEWIRE_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "gw-dev01.nonprod.bcaa.insurcloud.ca"
    type    = "A"
    alias {
        name                   = "${data.terraform_remote_state.dev_remote_state.ICBCAA_DEV01_INTERNAL_NLB_DNS_NAME}"
        zone_id                = "${data.terraform_remote_state.dev_remote_state.ICBCAA_DEV01_INTERNAL_NLB_ZONE_ID}"
        evaluate_target_health = false
    }
}

######### MICROSERVICES ###########
resource "aws_route53_record" "ICBCAA_DEV01_MICROSERVICES_APPLICATION_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "ms1.dev01.bcaa.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICBCAA_DEV01_MICROSERVICES_PRIVATEIP}"]
}

resource "aws_route53_record" "ICBCAA_DEV01_MICROSERVICES_DB_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "msdb.dev01.bcaa.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICBCAA_DEV01_MICROSERVICES_PRIVATEIP}"]
}

resource "aws_route53_record" "ICBCAA_DEV01_INTERNAL_NLB_MICROSERVICES_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "ms.dev01.bcaa.insurcloud.ca"
    type    = "A"
    alias {
        name                   = "${data.terraform_remote_state.dev_remote_state.ICBCAA_DEV01_INTERNAL_NLB_DNS_NAME}"
        zone_id                = "${data.terraform_remote_state.dev_remote_state.ICBCAA_DEV01_INTERNAL_NLB_ZONE_ID}"
        evaluate_target_health = false
    }
}

resource "aws_route53_record" "ICBCAA_DEV01_MICROSERVICES_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "ms-dev01.nonprod.bcaa.insurcloud.ca"
    type    = "A"
    alias {
        name                   = "${data.terraform_remote_state.dev_remote_state.ICBCAA_DEV01_INTERNAL_NLB_DNS_NAME}"
        zone_id                = "${data.terraform_remote_state.dev_remote_state.ICBCAA_DEV01_INTERNAL_NLB_ZONE_ID}"
        evaluate_target_health = false
    }
}


######### HAPROXY ##############
resource "aws_route53_record" "ICBCAA_DEV01_INTERNAL_HAPROXY_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "inthap.dev01.bcaa.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICBCAA_DEV01_INTERNAL_HAPROXY_PRIVATEIP}"]
}
# End of DEV01


# #=======================================================================
# # DEV02
# #=======================================================================

######### GUIDEWIRE ##############
resource "aws_route53_record" "ICBCAA_DEV02_GUIDEWIRE_SUITE_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "gw1.dev02.bcaa.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICBCAA_DEV02_GUIDEWIRE_SUITE_PRIVATEIP}"]
}

resource "aws_route53_record" "ICBCAA_DEV02_GUIDEWIRE_DB_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "gwdb.dev02.bcaa.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICBCAA_DEV02_GUIDEWIRE_SUITE_PRIVATEIP}"]
}

resource "aws_route53_record" "ICBCAA_DEV02_INTERNAL_NLB_GUIDEWIRE_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "gw.dev02.bcaa.insurcloud.ca"
    type    = "A"
    alias {
        name                   = "${data.terraform_remote_state.dev_remote_state.ICBCAA_DEV01_INTERNAL_NLB_DNS_NAME}"
        zone_id                = "${data.terraform_remote_state.dev_remote_state.ICBCAA_DEV01_INTERNAL_NLB_ZONE_ID}"
        evaluate_target_health = false
    }
}

resource "aws_route53_record" "ICBCAA_DEV02_GUIDEWIRE_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "gw-dev02.nonprod.bcaa.insurcloud.ca"
    type    = "A"
    alias {
        name                   = "${data.terraform_remote_state.dev_remote_state.ICBCAA_DEV01_INTERNAL_NLB_DNS_NAME}"
        zone_id                = "${data.terraform_remote_state.dev_remote_state.ICBCAA_DEV01_INTERNAL_NLB_ZONE_ID}"
        evaluate_target_health = false
    }
}

######### MICROSERVICES ###########
resource "aws_route53_record" "ICBCAA_DEV02_MICROSERVICES_APPLICATION_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "ms1.dev02.bcaa.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICBCAA_DEV02_MICROSERVICES_PRIVATEIP}"]
}

resource "aws_route53_record" "ICBCAA_DEV02_MICROSERVICES_DB_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "msdb.dev02.bcaa.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICBCAA_DEV02_MICROSERVICES_PRIVATEIP}"]
}

resource "aws_route53_record" "ICBCAA_DEV02_INTERNAL_NLB_MICROSERVICES_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "ms.dev02.bcaa.insurcloud.ca"
    type    = "A"
    alias {
        name                   = "${data.terraform_remote_state.dev_remote_state.ICBCAA_DEV01_INTERNAL_NLB_DNS_NAME}"
        zone_id                = "${data.terraform_remote_state.dev_remote_state.ICBCAA_DEV01_INTERNAL_NLB_ZONE_ID}"
        evaluate_target_health = false
    }
}

resource "aws_route53_record" "ICBCAA_DEV02_MICROSERVICES_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "ms-dev02.nonprod.bcaa.insurcloud.ca"
    type    = "A"
    alias {
        name                   = "${data.terraform_remote_state.dev_remote_state.ICBCAA_DEV01_INTERNAL_NLB_DNS_NAME}"
        zone_id                = "${data.terraform_remote_state.dev_remote_state.ICBCAA_DEV01_INTERNAL_NLB_ZONE_ID}"
        evaluate_target_health = false
    }
}


######### HAPROXY ##############
# Dev02 will use the one haproxy, record points to dev01 haproxy
resource "aws_route53_record" "ICBCAA_DEV02_INTERNAL_HAPROXY_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "inthap.dev02.bcaa.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICBCAA_DEV01_INTERNAL_HAPROXY_PRIVATEIP}"]
}
# End of DEV02




# #=======================================================================
# # PPS
# #=======================================================================
######### GUIDEWIRE ##############

######### MICROSERVICES ###########

######### HAPROXY ##############

######### DATA ##############


# #=======================================================================
# # PPS/DEV/QA
# #=======================================================================
######### OPENTEXT ##############

######### JOBSCHEDULER ##############

############# JENKINS CLIENT #################

resource "aws_route53_record" "ICBCAA_DEV_JENKINS_CLIENT_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "jenkins-client.dev.bcaa.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICBCAA_DEV_JENKINS_CLIENT_PRIVATEIP}"]
}
