##### ALBs #####
########## Internal ###########
resource "aws_lb" "INTERNAL_ALB_CICD" {
    name = "CICD-INTERNAL-ALB"
    internal           = true
    load_balancer_type = "application"
    security_groups    = ["${aws_security_group.SG_HTTPS.id}"]
    subnets            = [
        "${var.ichub-private-1a-subnet}",
        "${var.ichub-private-1b-subnet}"
    ]

    tags {
        Name = "CICD-INTERNAL-ALB"
    }
}

resource "aws_lb_target_group" "INTERNAL_ALB_TARGET_GROUP_JENKINS" {
    name     = "JENKINS-INTERNAL-ALB"
    target_type = "instance"
    port     = 8080
    protocol = "HTTP"
    vpc_id   = "${var.vpc_id}"

    tags {
        Name = "JENKINS-INTERNAL-ALB-TARGET-GROUP"
    }
}

resource "aws_lb_target_group" "INTERNAL_ALB_TARGET_GROUP_CONSUL" {
    name     = "CONSUL-INTERNAL-ALB"
    target_type = "instance"
    port     = 8500
    protocol = "HTTP"
    vpc_id   = "${var.vpc_id}"

    tags {
        Name = "CONSUL-INTERNAL-ALB-TARGET-GROUP"
    }
}

resource "aws_lb_target_group" "INTERNAL_ALB_TARGET_GROUP_NEXUS_CMIG_REG" {
    name     = "NEXUS-CMIG-REG-INTERNAL-ALB"
    target_type = "instance"
    port     = 9443
    protocol = "HTTP"
    vpc_id   = "${var.vpc_id}"

    tags {
        Name = "NEXUS-CMIG-REG-INTERNAL-ALB-TARGET-GROUP"
    }
}

resource "aws_lb_target_group" "INTERNAL_ALB_TARGET_GROUP_NEXUS_CMIG_PROD_REG" {
    name     = "NEXUS-CMIG-PROD-REG-INTERNAL-ALB"
    target_type = "instance"
    port     = 9444
    protocol = "HTTP"
    vpc_id   = "${var.vpc_id}"

    tags {
        Name = "NEXUS-CMIG-PROD-REG-INTERNAL-ALB-TARGET-GROUP"
    }
}

resource "aws_lb_target_group" "INTERNAL_ALB_TARGET_GROUP_NEXUS_PEEL_REG" {
    name     = "NEXUS-PEEL-REG-INTERNAL-ALB"
    target_type = "instance"
    port     = 9445
    protocol = "HTTP"
    vpc_id   = "${var.vpc_id}"

    tags {
        Name = "NEXUS-PEEL-REG-INTERNAL-ALB-TARGET-GROUP"
    }
}

resource "aws_lb_target_group" "INTERNAL_ALB_TARGET_GROUP_NEXUS_PEEL_PROD_REG" {
    name     = "NEXUS-PEEL-PROD-REG-INTERNAL-ALB"
    target_type = "instance"
    port     = 9446
    protocol = "HTTP"
    vpc_id   = "${var.vpc_id}"

    tags {
        Name = "NEXUS-PEEL-PROD-REG-INTERNAL-ALB-TARGET-GROUP"
    }
}

resource "aws_lb_target_group" "INTERNAL_ALB_TARGET_GROUP_NEXUS_ECO_REG" {
    name     = "NEXUS-ECO-REG-INTERNAL-ALB"
    target_type = "instance"
    port     = 9447
    protocol = "HTTP"
    vpc_id   = "${var.vpc_id}"

    tags {
        Name = "NEXUS-ECONOMICAL-REG-INTERNAL-ALB-TARGET-GROUP"
    }
}

resource "aws_lb_target_group" "INTERNAL_ALB_TARGET_GROUP_NEXUS_ECO_PROD_REG" {
    name     = "NEXUS-ECO-PROD-REG-INTERNAL-ALB"
    target_type = "instance"
    port     = 9448
    protocol = "HTTP"
    vpc_id   = "${var.vpc_id}"

    tags {
        Name = "NEXUS-ECONOMICAL-PROD-REG-INTERNAL-ALB-TARGET-GROUP"
    }
}

resource "aws_lb_target_group" "INTERNAL_ALB_TARGET_GROUP_NEXUS_GORE_REG" {
    name     = "NEXUS-GORE-REG-INTERNAL-ALB"
    target_type = "instance"
    port     = 9449
    protocol = "HTTP"
    vpc_id   = "${var.vpc_id}"

    tags {
        Name = "NEXUS-GOREMUTUAL-REG-INTERNAL-ALB-TARGET-GROUP"
    }
}

resource "aws_lb_target_group" "INTERNAL_ALB_TARGET_GROUP_NEXUS_GORE_PROD_REG" {
    name     = "NEXUS-GORE-PROD-REG-INTERNAL-ALB"
    target_type = "instance"
    port     = 9450
    protocol = "HTTP"
    vpc_id   = "${var.vpc_id}"

    tags {
        Name = "NEXUS-GOREMUTUAL-PROD-REG-INTERNAL-ALB-TARGET-GROUP"
    }
}

resource "aws_lb_target_group" "INTERNAL_ALB_TARGET_GROUP_NEXUS_PORTAGE_REG" {
    name     = "NEXUS-PORTAGE-REG-INTERNAL-ALB"
    target_type = "instance"
    port     = 9451
    protocol = "HTTP"
    vpc_id   = "${var.vpc_id}"

    tags {
        Name = "NEXUS-PORTAGEMUTUAL-REG-INTERNAL-ALB-TARGET-GROUP"
    }
}

resource "aws_lb_target_group" "INTERNAL_ALB_TARGET_GROUP_NEXUS_PORTAGE_PROD_REG" {
    name     = "NEXUS-PORTAGE-PROD-REG-INT-ALB"
    target_type = "instance"
    port     = 9452
    protocol = "HTTP"
    vpc_id   = "${var.vpc_id}"

    tags {
        Name = "NEXUS-PORTAGEMUTUAL-PROD-REG-INTERNAL-ALB-TARGET-GROUP"
    }
}

resource "aws_lb_target_group" "INTERNAL_ALB_TARGET_GROUP_NEXUS_BCAA_REG" {
    name     = "NEXUS-BCAA-REG-INTERNAL-ALB"
    target_type = "instance"
    port     = 9453
    protocol = "HTTP"
    vpc_id   = "${var.vpc_id}"

    tags {
        Name = "NEXUS-BCAA-REG-INTERNAL-ALB-TARGET-GROUP"
    }
}

resource "aws_lb_target_group" "INTERNAL_ALB_TARGET_GROUP_NEXUS_BCAA_PROD_REG" {
    name     = "NEXUS-BCAA-PROD-REG-INT-ALB"
    target_type = "instance"
    port     = 9454
    protocol = "HTTP"
    vpc_id   = "${var.vpc_id}"

    tags {
        Name = "NEXUS-BCAA-PROD-REG-INTERNAL-ALB-TARGET-GROUP"
    }
}

resource "aws_lb_target_group" "INTERNAL_ALB_TARGET_GROUP_NEXUS_BASE_REG" {
    name     = "NEXUS-BASE-REG-INTERNAL-ALB"
    target_type = "instance"
    port     = 9455
    protocol = "HTTP"
    vpc_id   = "${var.vpc_id}"

    tags {
        Name = "NEXUS-BASE-REG-INTERNAL-ALB-TARGET-GROUP"
    }
}

resource "aws_lb_target_group" "INTERNAL_ALB_TARGET_GROUP_NEXUS_BASE_PROD_REG" {
    name     = "NEXUS-BASE-PROD-REG-INT-ALB"
    target_type = "instance"
    port     = 9456
    protocol = "HTTP"
    vpc_id   = "${var.vpc_id}"

    tags {
        Name = "NEXUS-BASE-PROD-REG-INTERNAL-ALB-TARGET-GROUP"
    }
}

resource "aws_lb_target_group" "INTERNAL_ALB_TARGET_GROUP_NEXUS_UI" {
    name     = "NEXUS-UI-INTERNAL-ALB"
    target_type = "instance"
    port     = 8081
    protocol = "HTTP"
    vpc_id   = "${var.vpc_id}"

    tags {
        Name = "NEXUS-UI-INTERNAL-ALB-TARGET-GROUP"
    }
}

resource "aws_lb_target_group" "INTERNAL_ALB_TARGET_GROUP_SONARQUBE" {
    name     = "SONARQUBE-INTERNAL-ALB"
    target_type = "instance"
    port     = 9000
    protocol = "HTTP"
    vpc_id   = "${var.vpc_id}"

    tags {
        Name = "SONARQUBE-INTERNAL-ALB-TARGET-GROUP"
    }
}

resource "aws_lb_target_group_attachment" "INTERNAL_ALB_TARGET_GROUP_ATTACHMENT_JENKINS" {
    target_group_arn = "${aws_lb_target_group.INTERNAL_ALB_TARGET_GROUP_JENKINS.arn}"
    target_id        = "${aws_instance.ICHUB_JENKINS_MASTER.id}"
    port             = 8080
}

resource "aws_lb_target_group_attachment" "INTERNAL_ALB_TARGET_GROUP_ATTACHMENT_CONSUL" {
    target_group_arn = "${aws_lb_target_group.INTERNAL_ALB_TARGET_GROUP_CONSUL.arn}"
    target_id        = "${aws_instance.ICHUB_NEXUS_SONARQUBE_CONSUL.id}"
    port             = 8500
}

resource "aws_lb_target_group_attachment" "INTERNAL_ALB_TARGET_GROUP_ATTACHMENT_NEXUS_CMIG_REG" {
    target_group_arn = "${aws_lb_target_group.INTERNAL_ALB_TARGET_GROUP_NEXUS_CMIG_REG.arn}"
    target_id        = "${aws_instance.ICHUB_NEXUS_SONARQUBE_CONSUL.id}"
    port             = 9443
}

resource "aws_lb_target_group_attachment" "INTERNAL_ALB_TARGET_GROUP_ATTACHMENT_NEXUS_CMIG_PROD_REG" {
    target_group_arn = "${aws_lb_target_group.INTERNAL_ALB_TARGET_GROUP_NEXUS_CMIG_PROD_REG.arn}"
    target_id        = "${aws_instance.ICHUB_NEXUS_SONARQUBE_CONSUL.id}"
    port             = 9444
}

resource "aws_lb_target_group_attachment" "INTERNAL_ALB_TARGET_GROUP_ATTACHMENT_NEXUS_PEEL_REG" {
    target_group_arn = "${aws_lb_target_group.INTERNAL_ALB_TARGET_GROUP_NEXUS_PEEL_REG.arn}"
    target_id        = "${aws_instance.ICHUB_NEXUS_SONARQUBE_CONSUL.id}"
    port             = 9445
}

resource "aws_lb_target_group_attachment" "INTERNAL_ALB_TARGET_GROUP_ATTACHMENT_NEXUS_PEEL_PROD_REG" {
    target_group_arn = "${aws_lb_target_group.INTERNAL_ALB_TARGET_GROUP_NEXUS_PEEL_PROD_REG.arn}"
    target_id        = "${aws_instance.ICHUB_NEXUS_SONARQUBE_CONSUL.id}"
    port             = 9446
}

resource "aws_lb_target_group_attachment" "INTERNAL_ALB_TARGET_GROUP_ATTACHMENT_NEXUS_ECO_REG" {
    target_group_arn = "${aws_lb_target_group.INTERNAL_ALB_TARGET_GROUP_NEXUS_ECO_REG.arn}"
    target_id        = "${aws_instance.ICHUB_NEXUS_SONARQUBE_CONSUL.id}"
    port             = 9447
}

resource "aws_lb_target_group_attachment" "INTERNAL_ALB_TARGET_GROUP_ATTACHMENT_NEXUS_ECO_PROD_REG" {
    target_group_arn = "${aws_lb_target_group.INTERNAL_ALB_TARGET_GROUP_NEXUS_ECO_PROD_REG.arn}"
    target_id        = "${aws_instance.ICHUB_NEXUS_SONARQUBE_CONSUL.id}"
    port             = 9448
}

resource "aws_lb_target_group_attachment" "INTERNAL_ALB_TARGET_GROUP_ATTACHMENT_NEXUS_GORE_REG" {
    target_group_arn = "${aws_lb_target_group.INTERNAL_ALB_TARGET_GROUP_NEXUS_GORE_REG.arn}"
    target_id        = "${aws_instance.ICHUB_NEXUS_SONARQUBE_CONSUL.id}"
    port             = 9449
}

resource "aws_lb_target_group_attachment" "INTERNAL_ALB_TARGET_GROUP_ATTACHMENT_NEXUS_GORE_PROD_REG" {
    target_group_arn = "${aws_lb_target_group.INTERNAL_ALB_TARGET_GROUP_NEXUS_GORE_PROD_REG.arn}"
    target_id        = "${aws_instance.ICHUB_NEXUS_SONARQUBE_CONSUL.id}"
    port             = 9450
}

resource "aws_lb_target_group_attachment" "INTERNAL_ALB_TARGET_GROUP_ATTACHMENT_NEXUS_PORTAGE_REG" {
    target_group_arn = "${aws_lb_target_group.INTERNAL_ALB_TARGET_GROUP_NEXUS_PORTAGE_REG.arn}"
    target_id        = "${aws_instance.ICHUB_NEXUS_SONARQUBE_CONSUL.id}"
    port             = 9451
}

resource "aws_lb_target_group_attachment" "INTERNAL_ALB_TARGET_GROUP_ATTACHMENT_NEXUS_PORTAGE_PROD_REG" {
    target_group_arn = "${aws_lb_target_group.INTERNAL_ALB_TARGET_GROUP_NEXUS_PORTAGE_PROD_REG.arn}"
    target_id        = "${aws_instance.ICHUB_NEXUS_SONARQUBE_CONSUL.id}"
    port             = 9452
}

resource "aws_lb_target_group_attachment" "INTERNAL_ALB_TARGET_GROUP_ATTACHMENT_NEXUS_BCAA_REG" {
    target_group_arn = "${aws_lb_target_group.INTERNAL_ALB_TARGET_GROUP_NEXUS_BCAA_REG.arn}"
    target_id        = "${aws_instance.ICHUB_NEXUS_SONARQUBE_CONSUL.id}"
    port             = 9453
}

resource "aws_lb_target_group_attachment" "INTERNAL_ALB_TARGET_GROUP_ATTACHMENT_NEXUS_BCAA_PROD_REG" {
    target_group_arn = "${aws_lb_target_group.INTERNAL_ALB_TARGET_GROUP_NEXUS_BCAA_PROD_REG.arn}"
    target_id        = "${aws_instance.ICHUB_NEXUS_SONARQUBE_CONSUL.id}"
    port             = 9454
}

resource "aws_lb_target_group_attachment" "INTERNAL_ALB_TARGET_GROUP_ATTACHMENT_NEXUS_BASE_REG" {
    target_group_arn = "${aws_lb_target_group.INTERNAL_ALB_TARGET_GROUP_NEXUS_BASE_REG.arn}"
    target_id        = "${aws_instance.ICHUB_NEXUS_SONARQUBE_CONSUL.id}"
    port             = 9455
}

resource "aws_lb_target_group_attachment" "INTERNAL_ALB_TARGET_GROUP_ATTACHMENT_NEXUS_BASE_PROD_REG" {
    target_group_arn = "${aws_lb_target_group.INTERNAL_ALB_TARGET_GROUP_NEXUS_BASE_PROD_REG.arn}"
    target_id        = "${aws_instance.ICHUB_NEXUS_SONARQUBE_CONSUL.id}"
    port             = 9456
}

resource "aws_lb_target_group_attachment" "INTERNAL_ALB_TARGET_GROUP_ATTACHMENT_NEXUS_UI" {
    target_group_arn = "${aws_lb_target_group.INTERNAL_ALB_TARGET_GROUP_NEXUS_UI.arn}"
    target_id        = "${aws_instance.ICHUB_NEXUS_SONARQUBE_CONSUL.id}"
    port             = 8081
}

resource "aws_lb_target_group_attachment" "INTERNAL_ALB_TARGET_GROUP_ATTACHMENT_SONARQUBE" {
    target_group_arn = "${aws_lb_target_group.INTERNAL_ALB_TARGET_GROUP_SONARQUBE.arn}"
    target_id        = "${aws_instance.ICHUB_NEXUS_SONARQUBE_CONSUL.id}"
    port             = 9000
}

resource "aws_lb_listener" "INTERNAL_ALB_LISTENER_CICD" {
    load_balancer_arn = "${aws_lb.INTERNAL_ALB_CICD.arn}"
    port              = "443"
    protocol          = "HTTPS"
    ssl_policy        = "ELBSecurityPolicy-2016-08"
    certificate_arn   = "${aws_acm_certificate.INTERNAL_ALB_LISTENER_PRIVATE_CERTIFICATE_CICD.arn}"

    default_action {
        type = "fixed-response"

        fixed_response {
            content_type = "text/plain"
            status_code  = "418"
        }
    }
}

resource "aws_lb_listener_rule" "INTERNAL_ALB_LISTENER_RULE_JENKINS" {
    listener_arn = "${aws_lb_listener.INTERNAL_ALB_LISTENER_CICD.arn}"

    action {
        type             = "forward"
        target_group_arn = "${aws_lb_target_group.INTERNAL_ALB_TARGET_GROUP_JENKINS.arn}"
    }

    condition {
        host_header {
            values = ["${aws_route53_record.JENKINS_MASTER_RECORD.name}"]
        }
    }
}

resource "aws_lb_listener_rule" "INTERNAL_ALB_LISTENER_RULE_ICCW_JENKINS" {
    listener_arn = "${aws_lb_listener.INTERNAL_ALB_LISTENER_CICD.arn}"

    action {
        type             = "forward"
        target_group_arn = "${aws_lb_target_group.INTERNAL_ALB_TARGET_GROUP_JENKINS.arn}"
    }

    condition {
        host_header {
            values = ["${aws_route53_record.ICCW_JENKINS_MASTER_RECORD.name}"]
        }
    }
}

resource "aws_lb_listener_rule" "INTERNAL_ALB_LISTENER_RULE_ICPEEL_JENKINS" {
    listener_arn = "${aws_lb_listener.INTERNAL_ALB_LISTENER_CICD.arn}"

    action {
        type             = "forward"
        target_group_arn = "${aws_lb_target_group.INTERNAL_ALB_TARGET_GROUP_JENKINS.arn}"
    }

    condition {
        host_header {
            values = ["${aws_route53_record.ICPEEL_JENKINS_MASTER_RECORD.name}"]
        }
    }
}

resource "aws_lb_listener_rule" "INTERNAL_ALB_LISTENER_RULE_ICECO_JENKINS" {
    listener_arn = "${aws_lb_listener.INTERNAL_ALB_LISTENER_CICD.arn}"

    action {
        type             = "forward"
        target_group_arn = "${aws_lb_target_group.INTERNAL_ALB_TARGET_GROUP_JENKINS.arn}"
    }

    condition {
        host_header {
            values = ["${aws_route53_record.ICECO_JENKINS_MASTER_RECORD.name}"]
        }
    }
}

resource "aws_lb_listener_rule" "INTERNAL_ALB_LISTENER_RULE_ICGORE_JENKINS" {
    listener_arn = "${aws_lb_listener.INTERNAL_ALB_LISTENER_CICD.arn}"

    action {
        type             = "forward"
        target_group_arn = "${aws_lb_target_group.INTERNAL_ALB_TARGET_GROUP_JENKINS.arn}"
    }

    condition {
        host_header {
            values = ["${aws_route53_record.ICGORE_JENKINS_MASTER_RECORD.name}"]
        }
    }
}

resource "aws_lb_listener_rule" "INTERNAL_ALB_LISTENER_RULE_ICPORTAGE_JENKINS" {
    listener_arn = "${aws_lb_listener.INTERNAL_ALB_LISTENER_CICD.arn}"

    action {
        type             = "forward"
        target_group_arn = "${aws_lb_target_group.INTERNAL_ALB_TARGET_GROUP_JENKINS.arn}"
    }

    condition {
        host_header {
            values = ["${aws_route53_record.ICPORTAGE_JENKINS_MASTER_RECORD.name}"]
        }
    }
}

resource "aws_lb_listener_rule" "INTERNAL_ALB_LISTENER_RULE_ICBCAA_JENKINS" {
    listener_arn = "${aws_lb_listener.INTERNAL_ALB_LISTENER_CICD.arn}"

    action {
        type             = "forward"
        target_group_arn = "${aws_lb_target_group.INTERNAL_ALB_TARGET_GROUP_JENKINS.arn}"
    }

    condition {
        host_header {
            values = ["${aws_route53_record.ICBCAA_JENKINS_MASTER_RECORD.name}"]
        }
    }
}

resource "aws_lb_listener_rule" "INTERNAL_ALB_LISTENER_RULE_CONSUL" {
    listener_arn = "${aws_lb_listener.INTERNAL_ALB_LISTENER_CICD.arn}"

    action {
        type             = "forward"
        target_group_arn = "${aws_lb_target_group.INTERNAL_ALB_TARGET_GROUP_CONSUL.arn}"
    }

    condition {
        host_header {
            values = ["${aws_route53_record.CONSUL_RECORD.name}"]
        }
    }
}

resource "aws_lb_listener_rule" "INTERNAL_ALB_LISTENER_RULE_ICCW_CONSUL" {
    listener_arn = "${aws_lb_listener.INTERNAL_ALB_LISTENER_CICD.arn}"

    action {
        type             = "forward"
        target_group_arn = "${aws_lb_target_group.INTERNAL_ALB_TARGET_GROUP_CONSUL.arn}"
    }

    condition {
        host_header {
            values = ["${aws_route53_record.ICCW_CONSUL_RECORD.name}"]
        }
    }
}

resource "aws_lb_listener_rule" "INTERNAL_ALB_LISTENER_RULE_ICPEEL_CONSUL" {
    listener_arn = "${aws_lb_listener.INTERNAL_ALB_LISTENER_CICD.arn}"

    action {
        type             = "forward"
        target_group_arn = "${aws_lb_target_group.INTERNAL_ALB_TARGET_GROUP_CONSUL.arn}"
    }

    condition {
        host_header {
            values = ["${aws_route53_record.ICPEEL_CONSUL_RECORD.name}"]
        }
    }
}

resource "aws_lb_listener_rule" "INTERNAL_ALB_LISTENER_RULE_ICECO_CONSUL" {
    listener_arn = "${aws_lb_listener.INTERNAL_ALB_LISTENER_CICD.arn}"

    action {
        type             = "forward"
        target_group_arn = "${aws_lb_target_group.INTERNAL_ALB_TARGET_GROUP_CONSUL.arn}"
    }

    condition {
        host_header {
            values = ["${aws_route53_record.ICECO_CONSUL_RECORD.name}"]
        }
    }
}

resource "aws_lb_listener_rule" "INTERNAL_ALB_LISTENER_RULE_ICGORE_CONSUL" {
    listener_arn = "${aws_lb_listener.INTERNAL_ALB_LISTENER_CICD.arn}"

    action {
        type             = "forward"
        target_group_arn = "${aws_lb_target_group.INTERNAL_ALB_TARGET_GROUP_CONSUL.arn}"
    }

    condition {
        host_header {
            values = ["${aws_route53_record.ICGORE_CONSUL_RECORD.name}"]
        }
    }
}

resource "aws_lb_listener_rule" "INTERNAL_ALB_LISTENER_RULE_ICPORTAGE_CONSUL" {
    listener_arn = "${aws_lb_listener.INTERNAL_ALB_LISTENER_CICD.arn}"

    action {
        type             = "forward"
        target_group_arn = "${aws_lb_target_group.INTERNAL_ALB_TARGET_GROUP_CONSUL.arn}"
    }

    condition {
        host_header {
            values = ["${aws_route53_record.ICPORTAGE_CONSUL_RECORD.name}"]
        }
    }
}

resource "aws_lb_listener_rule" "INTERNAL_ALB_LISTENER_RULE_ICBCAA_CONSUL" {
    listener_arn = "${aws_lb_listener.INTERNAL_ALB_LISTENER_CICD.arn}"

    action {
        type             = "forward"
        target_group_arn = "${aws_lb_target_group.INTERNAL_ALB_TARGET_GROUP_CONSUL.arn}"
    }

    condition {
        host_header {
            values = ["${aws_route53_record.ICBCAA_CONSUL_RECORD.name}"]
        }
    }
}

resource "aws_lb_listener_rule" "INTERNAL_ALB_LISTENER_RULE_NEXUS_CMIG_REG" {
    listener_arn = "${aws_lb_listener.INTERNAL_ALB_LISTENER_CICD.arn}"

    action {
        type             = "forward"
        target_group_arn = "${aws_lb_target_group.INTERNAL_ALB_TARGET_GROUP_NEXUS_CMIG_REG.arn}"
    }

    condition {
        host_header {
            values = ["${aws_route53_record.NEXUS_CMIG_REG_RECORD.name}"]
        }
    }
}

resource "aws_lb_listener_rule" "INTERNAL_ALB_LISTENER_RULE_NEXUS_CMIG_PROD_REG" {
    listener_arn = "${aws_lb_listener.INTERNAL_ALB_LISTENER_CICD.arn}"

    action {
        type             = "forward"
        target_group_arn = "${aws_lb_target_group.INTERNAL_ALB_TARGET_GROUP_NEXUS_CMIG_PROD_REG.arn}"
    }

    condition {
        host_header {
            values = ["${aws_route53_record.NEXUS_CMIG_PROD_REG_RECORD.name}"]
        }
    }
}

resource "aws_lb_listener_rule" "INTERNAL_ALB_LISTENER_RULE_NEXUS_PEEL_REG" {
    listener_arn = "${aws_lb_listener.INTERNAL_ALB_LISTENER_CICD.arn}"

    action {
        type             = "forward"
        target_group_arn = "${aws_lb_target_group.INTERNAL_ALB_TARGET_GROUP_NEXUS_PEEL_REG.arn}"
    }

    condition {
        host_header {
            values = ["${aws_route53_record.NEXUS_PEEL_REG_RECORD.name}"]
        }
    }
}

resource "aws_lb_listener_rule" "INTERNAL_ALB_LISTENER_RULE_NEXUS_PEEL_PROD_REG" {
    listener_arn = "${aws_lb_listener.INTERNAL_ALB_LISTENER_CICD.arn}"

    action {
        type             = "forward"
        target_group_arn = "${aws_lb_target_group.INTERNAL_ALB_TARGET_GROUP_NEXUS_PEEL_PROD_REG.arn}"
    }

    condition {
        host_header {
            values = ["${aws_route53_record.NEXUS_PEEL_PROD_REG_RECORD.name}"]
        }
    }
}

resource "aws_lb_listener_rule" "INTERNAL_ALB_LISTENER_RULE_NEXUS_ECO_REG" {
    listener_arn = "${aws_lb_listener.INTERNAL_ALB_LISTENER_CICD.arn}"

    action {
        type             = "forward"
        target_group_arn = "${aws_lb_target_group.INTERNAL_ALB_TARGET_GROUP_NEXUS_ECO_REG.arn}"
    }

    condition {
        host_header {
            values = ["${aws_route53_record.NEXUS_ECO_REG_RECORD.name}"]
        }
    }
}

resource "aws_lb_listener_rule" "INTERNAL_ALB_LISTENER_RULE_NEXUS_ECO_PROD_REG" {
    listener_arn = "${aws_lb_listener.INTERNAL_ALB_LISTENER_CICD.arn}"

    action {
        type             = "forward"
        target_group_arn = "${aws_lb_target_group.INTERNAL_ALB_TARGET_GROUP_NEXUS_ECO_PROD_REG.arn}"
    }

    condition {
        host_header {
            values = ["${aws_route53_record.NEXUS_ECO_PROD_REG_RECORD.name}"]
        }
    }
}

resource "aws_lb_listener_rule" "INTERNAL_ALB_LISTENER_RULE_NEXUS_GORE_REG" {
    listener_arn = "${aws_lb_listener.INTERNAL_ALB_LISTENER_CICD.arn}"

    action {
        type             = "forward"
        target_group_arn = "${aws_lb_target_group.INTERNAL_ALB_TARGET_GROUP_NEXUS_GORE_REG.arn}"
    }

    condition {
        host_header {
            values = ["${aws_route53_record.NEXUS_GORE_REG_RECORD.name}"]
        }
    }
}

resource "aws_lb_listener_rule" "INTERNAL_ALB_LISTENER_RULE_NEXUS_GORE_PROD_REG" {
    listener_arn = "${aws_lb_listener.INTERNAL_ALB_LISTENER_CICD.arn}"

    action {
        type             = "forward"
        target_group_arn = "${aws_lb_target_group.INTERNAL_ALB_TARGET_GROUP_NEXUS_GORE_PROD_REG.arn}"
    }

    condition {
        host_header {
            values = ["${aws_route53_record.NEXUS_GORE_PROD_REG_RECORD.name}"]
        }
    }
}

resource "aws_lb_listener_rule" "INTERNAL_ALB_LISTENER_RULE_NEXUS_PORTAGE_REG" {
    listener_arn = "${aws_lb_listener.INTERNAL_ALB_LISTENER_CICD.arn}"

    action {
        type             = "forward"
        target_group_arn = "${aws_lb_target_group.INTERNAL_ALB_TARGET_GROUP_NEXUS_PORTAGE_REG.arn}"
    }

    condition {
        host_header {
            values = ["${aws_route53_record.NEXUS_PORTAGE_REG_RECORD.name}"]
        }
    }
}

resource "aws_lb_listener_rule" "INTERNAL_ALB_LISTENER_RULE_NEXUS_PORTAGE_PROD_REG" {
    listener_arn = "${aws_lb_listener.INTERNAL_ALB_LISTENER_CICD.arn}"

    action {
        type             = "forward"
        target_group_arn = "${aws_lb_target_group.INTERNAL_ALB_TARGET_GROUP_NEXUS_PORTAGE_PROD_REG.arn}"
    }

    condition {
        host_header {
            values = ["${aws_route53_record.NEXUS_PORTAGE_PROD_REG_RECORD.name}"]
        }
    }
}

resource "aws_lb_listener_rule" "INTERNAL_ALB_LISTENER_RULE_NEXUS_BCAA_REG" {
    listener_arn = "${aws_lb_listener.INTERNAL_ALB_LISTENER_CICD.arn}"

    action {
        type             = "forward"
        target_group_arn = "${aws_lb_target_group.INTERNAL_ALB_TARGET_GROUP_NEXUS_BCAA_REG.arn}"
    }

    condition {
        host_header {
            values = ["${aws_route53_record.NEXUS_BCAA_REG_RECORD.name}"]
        }
    }
}

resource "aws_lb_listener_rule" "INTERNAL_ALB_LISTENER_RULE_NEXUS_BCAA_PROD_REG" {
    listener_arn = "${aws_lb_listener.INTERNAL_ALB_LISTENER_CICD.arn}"

    action {
        type             = "forward"
        target_group_arn = "${aws_lb_target_group.INTERNAL_ALB_TARGET_GROUP_NEXUS_BCAA_PROD_REG.arn}"
    }

    condition {
        host_header {
            values = ["${aws_route53_record.NEXUS_BCAA_PROD_REG_RECORD.name}"]
        }
    }
}

resource "aws_lb_listener_rule" "INTERNAL_ALB_LISTENER_RULE_NEXUS_BASE_REG" {
    listener_arn = "${aws_lb_listener.INTERNAL_ALB_LISTENER_CICD.arn}"

    action {
        type             = "forward"
        target_group_arn = "${aws_lb_target_group.INTERNAL_ALB_TARGET_GROUP_NEXUS_BASE_REG.arn}"
    }

    condition {
        host_header {
            values = ["${aws_route53_record.NEXUS_BASE_REG_RECORD.name}"]
        }
    }
}

resource "aws_lb_listener_rule" "INTERNAL_ALB_LISTENER_RULE_NEXUS_BASE_PROD_REG" {
    listener_arn = "${aws_lb_listener.INTERNAL_ALB_LISTENER_CICD.arn}"

    action {
        type             = "forward"
        target_group_arn = "${aws_lb_target_group.INTERNAL_ALB_TARGET_GROUP_NEXUS_BASE_PROD_REG.arn}"
    }

    condition {
        host_header {
            values = ["${aws_route53_record.NEXUS_BASE_PROD_REG_RECORD.name}"]
        }
    }
}

resource "aws_lb_listener_rule" "INTERNAL_ALB_LISTENER_RULE_NEXUS_UI" {
    listener_arn = "${aws_lb_listener.INTERNAL_ALB_LISTENER_CICD.arn}"

    action {
        type             = "forward"
        target_group_arn = "${aws_lb_target_group.INTERNAL_ALB_TARGET_GROUP_NEXUS_UI.arn}"
    }

    condition {
        host_header {
            values = ["${aws_route53_record.NEXUS_UI_RECORD.name}"]
        }
    }
}

resource "aws_lb_listener_rule" "INTERNAL_ALB_LISTENER_RULE_ICCW_NEXUS_UI" {
    listener_arn = "${aws_lb_listener.INTERNAL_ALB_LISTENER_CICD.arn}"

    action {
        type             = "forward"
        target_group_arn = "${aws_lb_target_group.INTERNAL_ALB_TARGET_GROUP_NEXUS_UI.arn}"
    }

    condition {
        host_header {
            values = ["${aws_route53_record.ICCW_NEXUS_UI_RECORD.name}"]
        }
    }
}

resource "aws_lb_listener_rule" "INTERNAL_ALB_LISTENER_RULE_ICPEEL_NEXUS_UI" {
    listener_arn = "${aws_lb_listener.INTERNAL_ALB_LISTENER_CICD.arn}"

    action {
        type             = "forward"
        target_group_arn = "${aws_lb_target_group.INTERNAL_ALB_TARGET_GROUP_NEXUS_UI.arn}"
    }

    condition {
        host_header {
            values = ["${aws_route53_record.ICPEEL_NEXUS_UI_RECORD.name}"]
        }
    }
}

resource "aws_lb_listener_rule" "INTERNAL_ALB_LISTENER_RULE_ICECO_NEXUS_UI" {
    listener_arn = "${aws_lb_listener.INTERNAL_ALB_LISTENER_CICD.arn}"

    action {
        type             = "forward"
        target_group_arn = "${aws_lb_target_group.INTERNAL_ALB_TARGET_GROUP_NEXUS_UI.arn}"
    }

    condition {
        host_header {
            values = ["${aws_route53_record.ICECO_NEXUS_UI_RECORD.name}"]
        }
    }
}

resource "aws_lb_listener_rule" "INTERNAL_ALB_LISTENER_RULE_ICGORE_NEXUS_UI" {
    listener_arn = "${aws_lb_listener.INTERNAL_ALB_LISTENER_CICD.arn}"

    action {
        type             = "forward"
        target_group_arn = "${aws_lb_target_group.INTERNAL_ALB_TARGET_GROUP_NEXUS_UI.arn}"
    }

    condition {
        host_header {
            values = ["${aws_route53_record.ICGORE_NEXUS_UI_RECORD.name}"]
        }
    }
}

resource "aws_lb_listener_rule" "INTERNAL_ALB_LISTENER_RULE_ICPORTAGE_NEXUS_UI" {
    listener_arn = "${aws_lb_listener.INTERNAL_ALB_LISTENER_CICD.arn}"

    action {
        type             = "forward"
        target_group_arn = "${aws_lb_target_group.INTERNAL_ALB_TARGET_GROUP_NEXUS_UI.arn}"
    }

    condition {
        host_header {
            values = ["${aws_route53_record.ICPORTAGE_NEXUS_UI_RECORD.name}"]
        }
    }
}

resource "aws_lb_listener_rule" "INTERNAL_ALB_LISTENER_RULE_ICBCAA_NEXUS_UI" {
    listener_arn = "${aws_lb_listener.INTERNAL_ALB_LISTENER_CICD.arn}"

    action {
        type             = "forward"
        target_group_arn = "${aws_lb_target_group.INTERNAL_ALB_TARGET_GROUP_NEXUS_UI.arn}"
    }

    condition {
        host_header {
            values = ["${aws_route53_record.ICBCAA_NEXUS_UI_RECORD.name}"]
        }
    }
}

resource "aws_lb_listener_rule" "INTERNAL_ALB_LISTENER_RULE_SONARQUBE" {
    listener_arn = "${aws_lb_listener.INTERNAL_ALB_LISTENER_CICD.arn}"

    action {
        type             = "forward"
        target_group_arn = "${aws_lb_target_group.INTERNAL_ALB_TARGET_GROUP_SONARQUBE.arn}"
    }

    condition {
        host_header {
            values = ["${aws_route53_record.SONARQUBE_RECORD.name}"]
        }
    }
}

resource "aws_lb_listener_rule" "INTERNAL_ALB_LISTENER_RULE_ICCW_SONARQUBE" {
    listener_arn = "${aws_lb_listener.INTERNAL_ALB_LISTENER_CICD.arn}"

    action {
        type             = "forward"
        target_group_arn = "${aws_lb_target_group.INTERNAL_ALB_TARGET_GROUP_SONARQUBE.arn}"
    }

    condition {
        host_header {
            values = ["${aws_route53_record.ICCW_SONARQUBE_RECORD.name}"]
        }
    }
}

resource "aws_lb_listener_rule" "INTERNAL_ALB_LISTENER_RULE_ICPEEL_SONARQUBE" {
    listener_arn = "${aws_lb_listener.INTERNAL_ALB_LISTENER_CICD.arn}"

    action {
        type             = "forward"
        target_group_arn = "${aws_lb_target_group.INTERNAL_ALB_TARGET_GROUP_SONARQUBE.arn}"
    }

    condition {
        host_header {
            values = ["${aws_route53_record.ICPEEL_SONARQUBE_RECORD.name}"]
        }
    }
}

resource "aws_lb_listener_rule" "INTERNAL_ALB_LISTENER_RULE_ICECO_SONARQUBE" {
    listener_arn = "${aws_lb_listener.INTERNAL_ALB_LISTENER_CICD.arn}"

    action {
        type             = "forward"
        target_group_arn = "${aws_lb_target_group.INTERNAL_ALB_TARGET_GROUP_SONARQUBE.arn}"
    }

    condition {
        host_header {
            values = ["${aws_route53_record.ICECO_SONARQUBE_RECORD.name}"]
        }
    }
}

resource "aws_lb_listener_rule" "INTERNAL_ALB_LISTENER_RULE_ICGORE_SONARQUBE" {
    listener_arn = "${aws_lb_listener.INTERNAL_ALB_LISTENER_CICD.arn}"

    action {
        type             = "forward"
        target_group_arn = "${aws_lb_target_group.INTERNAL_ALB_TARGET_GROUP_SONARQUBE.arn}"
    }

    condition {
        host_header {
            values = ["${aws_route53_record.ICGORE_SONARQUBE_RECORD.name}"]
        }
    }
}

resource "aws_lb_listener_rule" "INTERNAL_ALB_LISTENER_RULE_ICPORTAGE_SONARQUBE" {
    listener_arn = "${aws_lb_listener.INTERNAL_ALB_LISTENER_CICD.arn}"

    action {
        type             = "forward"
        target_group_arn = "${aws_lb_target_group.INTERNAL_ALB_TARGET_GROUP_SONARQUBE.arn}"
    }

    condition {
        host_header {
            values = ["${aws_route53_record.ICPORTAGE_SONARQUBE_RECORD.name}"]
        }
    }
}

resource "aws_lb_listener_rule" "INTERNAL_ALB_LISTENER_RULE_ICBCAA_SONARQUBE" {
    listener_arn = "${aws_lb_listener.INTERNAL_ALB_LISTENER_CICD.arn}"

    action {
        type             = "forward"
        target_group_arn = "${aws_lb_target_group.INTERNAL_ALB_TARGET_GROUP_SONARQUBE.arn}"
    }

    condition {
        host_header {
            values = ["${aws_route53_record.ICBCAA_SONARQUBE_RECORD.name}"]
        }
    }
}
