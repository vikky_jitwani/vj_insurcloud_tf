
#### CONDUIT DB #####

resource "aws_db_subnet_group" "POSTGRES_DB_SUBNET" {
    name        = "postgres-db-subnet"
    description = "RDS subnet group"
    subnet_ids  = ["${var.ichub-private-1a-subnet}", "${var.ichub-private-1b-subnet}"]
}

resource "aws_db_parameter_group" "conduit" {
  name   = "conduitdb"
  family = "postgres10"


  parameter {
    name  = "timezone"
    value = "America/Toronto"
  }
}

resource "aws_db_instance" "conduitdb" {
  engine              = "postgres"
  engine_version      = "10.13"
  instance_class      = "db.t3.medium"
  allocated_storage   = 20
  storage_encrypted   = true
  identifier          = "conduitdb"
  username            = "postgres"
  password            = "${var.conduit-rds-password}" # password
  port                = 5432
  maintenance_window  = "Mon:00:00-Mon:03:00"
  backup_window       = "03:00-06:00"
  deletion_protection = true

  db_subnet_group_name  = "${aws_db_subnet_group.POSTGRES_DB_SUBNET.name}"
  parameter_group_name  = "${aws_db_parameter_group.conduit.id}"
  multi_az              = false # set to true to have high availability: 2 instances synchronized with each other
  vpc_security_group_ids  = ["${aws_security_group.SG_CONDUIT_DB.id}"]
  storage_type            = "gp2"
  backup_retention_period = 30 # how long youre going to keep your backups
  kms_key_id = "arn:aws:kms:ca-central-1:020266858005:key/1eebfe7c-18a8-4431-9791-2ad47fe35f44"
}
