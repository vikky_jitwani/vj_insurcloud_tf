resource "aws_instance" "ICHUB_JENKINS_MASTER" {
    ami = "${var.ami_id_amazonlinux2base}"
    vpc_security_group_ids = ["${aws_security_group.SG_JENKINS_MASTER.id}"]
    subnet_id = "${var.ichub-private-1a-subnet}"
    instance_type = "t3a.large"
    key_name    = "ICHUB-CICD"
    associate_public_ip_address = false
	iam_instance_profile = "CMS-SSMInstanceRole"
	ebs_optimized = "true"

	lifecycle {
       prevent_destroy = false
	}

	root_block_device {
       volume_size = "80"
	   delete_on_termination = "false"
	}

    tags {
		Owner = "robhatnagar@deloitte.ca"
        ManagedBy = "CMS-TF"
		SolutionName = "Insurcloud "
		Environment = "Hub"
		ApplicationName = "jenkins, docker"
		DataClassification = "PII, PHI"
		DLM-Backup = "Daily-7"
		DLM-Retention = "90"
		PatchGroup = "AmazonLinux2-Group1"
		VendorManaged = "False"
		ResourceType = "Client"
		ResourceSeverity = "Normal"
        Name = "A-MC-CAC-D-A003-INSURCLOUD-HUB-JENKINS-MASTER-EC2"
		AccountID = "${data.aws_caller_identity.current.account_id}"
		Role = "Container"
		Account = "IC-hub"
		Client = "ic-hub"
		KeyName = "ICHUB-CICD"
    }

}

resource "aws_instance" "ICHUB_NEXUS_SONARQUBE_CONSUL" {
    ami = "${var.ami_id_amazonlinux2base}"
    vpc_security_group_ids = [
		"${aws_security_group.SG_NEXUS.id}",
		"${aws_security_group.SG_SONARQUBE.id}",
		"${aws_security_group.SG_CONSUL.id}",
	]
	subnet_id = "${var.ichub-private-1a-subnet}"
    instance_type = "r5a.large"
    key_name    = "ICHUB-CICD"
    associate_public_ip_address = false
	iam_instance_profile = "CMS-SSMInstanceRole"
	ebs_optimized = "true"

	root_block_device {
       volume_size = "1500"
	   delete_on_termination = "false"
	}

	tags {
		Owner = "robhatnagar@deloitte.ca"
		ManagedBy = "CMS-TF"
		SolutionName = "Insurcloud "
		Environment = "Hub"
		ApplicationName = "nexus, sonarqube, consul, docker"
		DataClassification = "NA"
		DLM-Backup = "Daily-7"
		DLM-Retention = "90"
		PatchGroup = "AmazonLinux2-Group1"
		VendorManaged = "False"
		ResourceType = "Client"
		ResourceSeverity = "High"
		Name = "A-MC-CAC-D-A003-INSURCLOUD-HUB-NEXUS-SONARQUBE-CONSUL-EC2"
		AccountID = "${data.aws_caller_identity.current.account_id}"
		Role = "Container"
		Account = "IC-hub"
		Client = "ic-hub"
		KeyName = "ICHUB-CICD"
	}
}

#=======================================================================
# Conduit
#=======================================================================
resource "aws_instance" "ICHUB_CONDUIT_WORKER" {
    ami = "${var.ami_id_windows2016base}"
    count = 1
    vpc_security_group_ids = [
		"${aws_security_group.SG_CONDUIT_WORKER.id}",
		"${aws_security_group.SG_AVIATRIX_RDP.id}"
		]
    subnet_id = "${var.ichub-private-1a-subnet}"
    instance_type = "t3a.large"
    key_name    = "ICHUB-CICD"
    associate_public_ip_address = false
    iam_instance_profile = "CMS-SSMInstanceRole"

	root_block_device {
		volume_size = "60"
	}

	tags {
		Owner = "robhatnagar@deloitte.ca"
		ManagedBy = "CMS-TF"
		SolutionName = "Insurcloud"
		Environment = "Hub"
		ApplicationName = "conduit worker"
		DataClassification = "NA"
		DLM-Backup = "Daily-7"
		DLM-Retention = "90"
		PatchGroup = "Windows"
		VendorManaged = "False"
		ResourceType = "Client"
		ResourceSeverity = "Normal"
		Name = "A-MC-CAC-D-A003-INSURCLOUD-HUB-WIN-CONDUIT-WORKER-EC2-${count.index + 1}"
		AccountID = "${data.aws_caller_identity.current.account_id}"
		Role = "Container"
		NexusUse = "False"
		BackupOption = "Patching"
		BILLINGCODE = "FPE01903-CO-TE-TO-1000"
		BILLINGCONTACT = "marrojas@deloitte.ca"
		CLIENT = "CMS"
		COUNTRY = "CA"
		CSCLASS = "Confidential"
		CSQUAL = "CI/PI Data"
		CSTYPE = "External"
		ENVIRONMENT = "PRD"
		FUNCTION = "CON"
		GROUPCONTACT = "marrojas@deloitte.ca"
		MEMBERFIRM = "CA"
		PRIMARYCONTACT = "1600274034767856414"
		SECONDARYCONTACT = "marrojas@deloitte.ca"
		Account = "IC-hub"
		Client = "ic-hub"
		KeyName = "ICHUB-CICD"
	}	
} 

resource "aws_instance" "ICHUB_CONDUIT_SERVER" {
    ami = "${var.ami_id_amazonlinux2base}"
    vpc_security_group_ids = [
		"${aws_security_group.SG_CONDUIT_SERVER.id}",
		"${aws_security_group.SG_AVIATRIX_SSH.id}"
		]
    subnet_id = "${var.ichub-private-1a-subnet}"
    instance_type = "t3a.2xlarge"
    key_name    = "ICHUB-CICD"
    associate_public_ip_address = false
    iam_instance_profile = "CMS-SSMInstanceRole"

	root_block_device {
		volume_size = "100"
	}
	
	tags {
		Owner = "robhatnagar@deloitte.ca"
		ManagedBy = "CMS-TF"
		SolutionName = "Insurcloud"
		Environment = "Hub"
		ApplicationName = "conduit server"
		DataClassification = "NA"
		DLM-Backup = "Daily-7"
		DLM-Retention = "90"
		PatchGroup = "AmazonLinux2-Group1"
		VendorManaged = "False"
		ResourceType = "Client"
		ResourceSeverity = "Normal"
		Name = "A-MC-CAC-D-A003-INSURCLOUD-HUB-LNX-CONDUIT-SERVER-EC2-${count.index + 1}"
		AccountID = "${data.aws_caller_identity.current.account_id}"
		Role = "Container"
		NexusUse = "False"
		BackupOption = "Patching"
		BILLINGCODE = "FPE01903-CO-TE-TO-1000"
		BILLINGCONTACT = "marrojas@deloitte.ca"
		CLIENT = "CMS"
		COUNTRY = "CA"
		CSCLASS = "Confidential"
		CSQUAL = "CI/PI Data"
		CSTYPE = "External"
		ENVIRONMENT = "PRD"
		FUNCTION = "CON"
		GROUPCONTACT = "marrojas@deloitte.ca"
		MEMBERFIRM = "CA"
		PRIMARYCONTACT = "1600274034767856414"
		SECONDARYCONTACT = "marrojas@deloitte.ca"
		Account = "IC-hub"
		Client = "ic-hub"
		KeyName = "ICHUB-CICD"
	}	
}