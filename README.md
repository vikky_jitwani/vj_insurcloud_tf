# Jenkins Master deployment for IC-Commonwell

To mount volumes refer : https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/ebs-using-volumes.html

Linux servers running docker must be provisioned using the steps below:
Create additional volume and mount it to /var/lib/docker
```
sudo mkfs -t xfs /dev/xvdf
sudo mount /dev/xvdf /var/lib/docker/
sudo yum install docker -y
sudo service docker start
```

```
JENKINS DOCKER RUN COMMAND
docker run -dt --name jenkins -p 8080:8080 -p 50000:50000 -p 7999:7999 -e JAVA_OPTS="-Duser.timezone=America/Toronto" -v /var/jenkins_home:/var/jenkins_home jenkins/jenkins:2.176

NEXUS DOCKER RUN COMMAND
docker run -d -p 8081:8081 -p 9443:9443 --name nexus -v nexus-data:/nexus-data sonatype/nexus3:3.16.2
```

To test the CIS benchmark:
```
git clone https://github.com/docker/docker-bench-security
cd docker-bench-security
sudo sh docker-bench-security.sh
```