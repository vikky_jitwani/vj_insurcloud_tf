
# #=======================================================================
# # QA02
# #=======================================================================

######### WINDOWS ##############
resource "aws_route53_record" "ICPORTAGE_QA02_DHIC_RECORD" {
  zone_id = "${var.zone_id}"
  name    = "dhic.QA02.portage.insurcloud.ca"
  ttl     = "300"
  type    = "A"
  records = ["${data.terraform_remote_state.dev_remote_state.ICPORTAGE_QA02_DHIC_SERVER_PRIVATEIP}"]
}

resource "aws_route53_record" "ICPORTAGE_QA02_DHIC_DB_RECORD" {
  zone_id = "${var.zone_id}"
  name    = "dhicdb.QA02.portage.insurcloud.ca"
  ttl     = "300"
  type    = "A"
  records = ["${data.terraform_remote_state.dev_remote_state.ICPORTAGE_QA02_DHIC_MSSQL_DATABASE_PRIVATEIP}"]
}

resource "aws_route53_record" "ICPORTAGE_QA02_GW_SSIS_MSSQL_DATABASE_RECORD" {
  zone_id = "${var.zone_id}"
  name    = "ssisdb.QA02.portage.insurcloud.ca"
  ttl     = "300"
  type    = "A"
  records = ["${data.terraform_remote_state.dev_remote_state.ICPORTAGE_QA02_DHIC_MSSQL_DATABASE_PRIVATEIP}"]
}

######### GUIDEWIRE ##############
resource "aws_route53_record" "ICPORTAGE_QA02_GUIDEWIRE_SUITE_RECORD" {
  zone_id = "${var.zone_id}"
  name    = "gw1.QA02.portage.insurcloud.ca"
  ttl     = "300"
  type    = "A"
  records = ["${data.terraform_remote_state.dev_remote_state.ICPORTAGE_QA02_GUIDEWIRE_SUITE_PRIVATEIP}"]
}

resource "aws_route53_record" "ICPORTAGE_QA02_GUIDEWIRE_DB_RECORD" {
  zone_id = "${var.zone_id}"
  name    = "gwdb.QA02.portage.insurcloud.ca"
  ttl     = "300"
  type    = "A"
  records = ["${data.terraform_remote_state.dev_remote_state.ICPORTAGE_QA02_GUIDEWIRE_SUITE_PRIVATEIP}"]
}

resource "aws_route53_record" "ICPORTAGE_QA02_INTERNAL_NLB_GUIDEWIRE_RECORD" {
  zone_id = "${var.zone_id}"
  name    = "gw.QA02.portage.insurcloud.ca"
  type    = "A"
  alias {
    name                   = "${data.terraform_remote_state.dev_remote_state.ICPORTAGE_DEV01_INTERNAL_NLB_DNS_NAME}"
    zone_id                = "${data.terraform_remote_state.dev_remote_state.ICPORTAGE_DEV01_INTERNAL_NLB_ZONE_ID}"
    evaluate_target_health = false
  }
}

######### MICROSERVICES ###########
resource "aws_route53_record" "ICPORTAGE_QA02_MICROSERVICES_RECORD" {
  zone_id = "${var.zone_id}"
  name    = "ms1.QA02.portage.insurcloud.ca"
  ttl     = "300"
  type    = "A"
  records = ["${data.terraform_remote_state.dev_remote_state.ICPORTAGE_QA02_MICROSERVICES_PRIVATEIP}"]
}

resource "aws_route53_record" "ICPORTAGE_QA02_MICROSERVICES_DB_RECORD" {
  zone_id = "${var.zone_id}"
  name    = "msdb.QA02.portage.insurcloud.ca"
  ttl     = "300"
  type    = "A"
  records = ["${data.terraform_remote_state.dev_remote_state.ICPORTAGE_QA02_MICROSERVICES_PRIVATEIP}"]
}

resource "aws_route53_record" "ICPORTAGE_QA02_INTERNAL_NLB_MICROSERVICES_RECORD" {
  zone_id = "${var.zone_id}"
  name    = "ms.QA02.portage.insurcloud.ca"
  type    = "A"
  alias {
    name                   = "${data.terraform_remote_state.dev_remote_state.ICPORTAGE_DEV01_INTERNAL_NLB_DNS_NAME}"
    zone_id                = "${data.terraform_remote_state.dev_remote_state.ICPORTAGE_DEV01_INTERNAL_NLB_ZONE_ID}"
    evaluate_target_health = false
  }
}

resource "aws_route53_record" "ICPORTAGE_QA02_WIREMOCK_RECORD" {
  zone_id = "${var.zone_id}"
  name    = "mockdata.QA02.portage.insurcloud.ca"
  ttl     = "300"
  type    = "A"
  records = ["${data.terraform_remote_state.dev_remote_state.ICPORTAGE_QA02_MICROSERVICES_PRIVATEIP}"]
}