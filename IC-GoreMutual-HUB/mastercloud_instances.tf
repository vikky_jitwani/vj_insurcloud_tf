######################################

# #=======================================================================
# # DYNATRACE
# #=======================================================================

resource "aws_instance" "ICGORE_DYNATRACE" {
    ami = "${var.ami_id_amazonlinux2base}"
    vpc_security_group_ids = [
        "${aws_security_group.SG_AVIATRIX_SSH.id}",
        "${aws_security_group.SG_JENKINS_CLIENT_SSH.id}",
        "${aws_security_group.SG_DYNATRACE_CLUSTER.id}"
    ]

    subnet_id = "${var.gore-hub-private-1a-subnet}"
    instance_type = "r5a.2xlarge"
    key_name    = "GoreOtherHub"
    associate_public_ip_address = false
    iam_instance_profile = "CMS-SSMInstanceRole"
	ebs_optimized = "true"

    root_block_device {
       volume_size = "200"
    }

    tags {
        Client = "GoreMutual"
        Account = "GORE-MUTUAL-Hub"
        Owner = "robhatnagar@deloitte.ca"
        ManagedBy = "CMS-TF"
        SolutionName = "Insurcloud Gore"
        Environment = "Hub"
        ApplicationName = "dynatrace"
        DataClassification = "PII, PHI"
        DLM-Backup = "Yes"
        DLM-Retention = "90"
        PatchGroup = "AmazonLinux2"
        VendorManaged = "False"
        ResourceType = "Client"
        ResourceSeverity = "Normal"
        Name = "A-MC-CAC-D-A003-GORE-DYNATRACE-LNX-EC2"
        AccountID = "${data.aws_caller_identity.current.account_id}"
        Role = "Container",
        BILLINGCODE = "FPE01903-CO-TE-TO-1000",
        BILLINGCONTACT = "marrojas@deloitte.ca",
        COUNTRY = "CA",
        CSCLASS = "Confidential",
        CSQUAL = "CI/PI Data",
        CSTYPE = "External",
        ENVIRONMENT = "PRD",
        FUNCTION = "CON",
        MEMBERFIRM = "CA",
        PRIMARYCONTACT = "marrojas@deloitte.ca",
        SECONDARYCONTACT ="Samir Modh"
    }

    provisioner "local-exec" {
        command = "echo ${aws_instance.ICGORE_DYNATRACE.private_ip} >> dev_hosts.txt"
    }
}

output "ICGORE_DYNATRACE_PRIVATEIP" {
    value = "${aws_instance.ICGORE_DYNATRACE.private_ip}"
}
# ######### JOBSCHEDULER ##############
#Dont know who will host the scheduler yet. 
# resource "aws_instance" "ICGORE_DEV_JOB_SCHEDULER" {
#   ami = "${var.ami_id_amazonlinux2base}"
#   vpc_security_group_ids = [
#     "${aws_security_group.SG_JOB_SCHEDULER.id}",
#     "${aws_security_group.SG_AVIATRIX_SSH.id}",
#     "${aws_security_group.SG_JENKINS_CLIENT_SSH.id}"
#   ]
#   subnet_id = "${var.GORE-hub-private-1a-subnet}"
#   instance_type = "t3.medium"
#   key_name = "GOREOtherHub"
#   associate_public_ip_address = false
#   iam_instance_profile = "CMS-SSMInstanceRole"
#   ebs_optimized = "true"

#   root_block_device {
#     volume_size = "40"
#   }

#   tags {
#     Owner = "robhatnagar@deloitte.ca"
#     ManagedBy = "CMS-TF"
#     SolutionName = "Insurcloud GORE"
#     Environment = "DEV, PPS"
#     ApplicationName = "jobscheduler"
#     DataClassification = "PII, PHI"
#     DLM-Backup = "Yes"
#     DLM-Retention = "90"
#     PatchGroup = "AmazonLinux2"
#     VendorManaged = "False"
#     ResourceType = "Client"
#     ResourceSeverity = "Normal"
#     Name = "A-MC-CAC-D-A003-GORE-DEV-JOB-SCHEDULER-LNX-EC2"
#     AccountID = "${data.aws_caller_identity.current.account_id}"
#     Role = "Container"
#   }
# }
