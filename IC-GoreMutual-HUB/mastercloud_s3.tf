resource "aws_s3_bucket" "ICGORE_HUB_MS_PROPERTIES" {
  bucket = "gore-ms-properties"
  acl = "private"

  lifecycle_rule {
    enabled = true

    transition {
      days = 30
      storage_class = "STANDARD_IA"
    }

  }

  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm     = "AES256"
      }
    }
  }

  tags {
    Name = "gore-ms-properties"
  }
}

resource "aws_s3_bucket_policy" "ICGORE_HUB_MS_PROPERTIES_BUCKET_POLICY" {
  bucket = "${aws_s3_bucket.ICGORE_HUB_MS_PROPERTIES.id}"

  policy = <<POLICY
{
    "Version": "2012-10-17",
    "Id": "ICGORE_HUB_MS_PROPERTIES_BUCKET_POLICY",
    "Statement": [
        {
            "Sid": "DenyPublicReadACL",
            "Effect": "Deny",
            "Principal": {
                "AWS": "*"
            },
            "Action": [
                "s3:PutObject",
                "s3:PutObjectAcl"
            ],
            "Resource": "arn:aws:s3:::${aws_s3_bucket.ICGORE_HUB_MS_PROPERTIES.id}/*",
            "Condition": {
                "StringEquals": {
                    "s3:x-amz-acl": [
                        "public-read",
                        "public-read-write",
                        "authenticated-read"
                    ]
                }
            }
        },
        {
            "Sid": "DenyPublicReadGrant",
            "Effect": "Deny",
            "Principal": {
                "AWS": "*"
            },
            "Action": [
                "s3:PutObject",
                "s3:PutObjectAcl"
            ],
            "Resource": "arn:aws:s3:::${aws_s3_bucket.ICGORE_HUB_MS_PROPERTIES.id}/*",
            "Condition": {
                "StringLike": {
                    "s3:x-amz-grant-read": [
                        "*http://acs.amazonaws.com/groups/global/AllUsers*",
                        "*http://acs.amazonaws.com/groups/global/AuthenticatedUsers*"
                    ]
                }
            }
        },
        {
            "Sid": "DenyPublicListACL",
            "Effect": "Deny",
            "Principal": {
                "AWS": "*"
            },
            "Action": "s3:PutBucketAcl",
            "Resource": "arn:aws:s3:::${aws_s3_bucket.ICGORE_HUB_MS_PROPERTIES.id}",
            "Condition": {
                "StringEquals": {
                    "s3:x-amz-acl": [
                        "public-read",
                        "public-read-write",
                        "authenticated-read"
                    ]
                }
            }
        },
        {
            "Sid": "DenyPublicListGrant",
            "Effect": "Deny",
            "Principal": {
                "AWS": "*"
            },
            "Action": "s3:PutBucketAcl",
            "Resource": "arn:aws:s3:::${aws_s3_bucket.ICGORE_HUB_MS_PROPERTIES.id}",
            "Condition": {
                "StringLike": {
                    "s3:x-amz-grant-read": [
                        "*http://acs.amazonaws.com/groups/global/AllUsers*",
                        "*http://acs.amazonaws.com/groups/global/AuthenticatedUsers*"
                    ]
                }
            }
        },
        {
            "Sid": "DenyPublicReadConsole",
            "Effect": "Deny",
            "Principal": {
                "AWS": "*"
            },
            "Action": [
                "s3:PutObjectAcl",
                "s3:PutBucketAcl"
            ],
            "Resource": [
                "arn:aws:s3:::${aws_s3_bucket.ICGORE_HUB_MS_PROPERTIES.id}/*",
                "arn:aws:s3:::${aws_s3_bucket.ICGORE_HUB_MS_PROPERTIES.id}"
            ],
            "Condition": {
                "Null": {
                    "s3:x-amz-grant-write": "true",
                    "s3:x-amz-grant-read": "true",
                    "s3:x-amz-acl": "true",
                    "s3:x-amz-grant-read-acp": "true",
                    "s3:x-amz-grant-full-control": "true",
                    "s3:x-amz-grant-write-acp": "true"
                }
            }
        },
        {
            "Sid": "DevInstanceListBucketAllow",
            "Effect": "Allow",
            "Principal": {
                "AWS": "arn:aws:iam::${var.gore-dev-account}:role/CMS-SSMRole"
            },
            "Action": "s3:ListBucket",
            "Resource": "arn:aws:s3:::${aws_s3_bucket.ICGORE_HUB_MS_PROPERTIES.id}"
        },
        {
            "Sid": "DevInstanceObjectAllow",
            "Effect": "Allow",
            "Principal": {
                "AWS": "arn:aws:iam::${var.gore-dev-account}:role/CMS-SSMRole"
            },
            "Action": [
                "s3:GetObject",
                "s3:PutObject",
                "s3:DeleteObject"
            ],
            "Resource": [
                "arn:aws:s3:::${aws_s3_bucket.ICGORE_HUB_MS_PROPERTIES.id}/DEV*/*",
                "arn:aws:s3:::${aws_s3_bucket.ICGORE_HUB_MS_PROPERTIES.id}/QA*/*",
                "arn:aws:s3:::${aws_s3_bucket.ICGORE_HUB_MS_PROPERTIES.id}/TBT*/*",
                "arn:aws:s3:::${aws_s3_bucket.ICGORE_HUB_MS_PROPERTIES.id}/BAT*/*",
                "arn:aws:s3:::${aws_s3_bucket.ICGORE_HUB_MS_PROPERTIES.id}/PERF*/*"
            ]
        },
        {
            "Sid": "HubInstanceListBucketAllow",
            "Effect": "Allow",
            "Principal": {
                "AWS": "arn:aws:iam::${var.gore-hub-account}:role/CMS-SSMRole"
            },
            "Action": "s3:ListBucket",
            "Resource": "arn:aws:s3:::${aws_s3_bucket.ICGORE_HUB_MS_PROPERTIES.id}"
        },
        {
            "Sid": "HubInstanceObjectAllow",
            "Effect": "Allow",
            "Principal": {
                "AWS": "arn:aws:iam::${var.gore-hub-account}:role/CMS-SSMRole"
            },
            "Action": [
                "s3:GetObject",
                "s3:PutObject",
                "s3:DeleteObject"
            ],
            "Resource": [
                "arn:aws:s3:::${aws_s3_bucket.ICGORE_HUB_MS_PROPERTIES.id}/DEV*/*",
                "arn:aws:s3:::${aws_s3_bucket.ICGORE_HUB_MS_PROPERTIES.id}/QA*/*",
                "arn:aws:s3:::${aws_s3_bucket.ICGORE_HUB_MS_PROPERTIES.id}/TBT*/*",
                "arn:aws:s3:::${aws_s3_bucket.ICGORE_HUB_MS_PROPERTIES.id}/PPS/*",
                "arn:aws:s3:::${aws_s3_bucket.ICGORE_HUB_MS_PROPERTIES.id}/PROD/*"
            ]
        },
        {
            "Sid": "ProdInstanceListBucketAllow",
            "Effect": "Allow",
            "Principal": {
                "AWS": "arn:aws:iam::${var.gore-prod-account}:role/CMS-SSMRole"
            },
            "Action": "s3:ListBucket",
            "Resource": "arn:aws:s3:::${aws_s3_bucket.ICGORE_HUB_MS_PROPERTIES.id}"
        },
        {
            "Sid": "ProdInstanceObjectAllow",
            "Effect": "Allow",
            "Principal": {
                "AWS": "arn:aws:iam::${var.gore-prod-account}:role/CMS-SSMRole"
            },
            "Action": [
                "s3:GetObject",
                "s3:PutObject",
                "s3:DeleteObject"
            ],
            "Resource": [
                "arn:aws:s3:::${aws_s3_bucket.ICGORE_HUB_MS_PROPERTIES.id}/PROD/*"
            ]
        }
    ]
}
POLICY
}

resource "aws_s3_bucket" "ICGORE_HUB_MS_LIB_PROPERTIES" {
  bucket = "gore-ms-lib-properties"
  acl = "private"

  lifecycle_rule {
    enabled = true

    transition {
      days = 30
      storage_class = "STANDARD_IA"
    }

  }

  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm     = "AES256"
      }
    }
  }

  tags {
    Name = "gore-ms-lib-properties"
  }
}

resource "aws_s3_bucket_policy" "ICGORE_HUB_MS_LIB_PROPERTIES_BUCKET_POLICY" {
  bucket = "${aws_s3_bucket.ICGORE_HUB_MS_LIB_PROPERTIES.id}"

  policy = <<POLICY
{
    "Version": "2012-10-17",
    "Id": "ICGORE_HUB_MS_LIB_PROPERTIES_BUCKET_POLICY",
    "Statement": [
        {
            "Sid": "DenyPublicReadACL",
            "Effect": "Deny",
            "Principal": {
                "AWS": "*"
            },
            "Action": [
                "s3:PutObject",
                "s3:PutObjectAcl"
            ],
            "Resource": "arn:aws:s3:::${aws_s3_bucket.ICGORE_HUB_MS_LIB_PROPERTIES.id}/*",
            "Condition": {
                "StringEquals": {
                    "s3:x-amz-acl": [
                        "public-read",
                        "public-read-write",
                        "authenticated-read"
                    ]
                }
            }
        },
        {
            "Sid": "DenyPublicReadGrant",
            "Effect": "Deny",
            "Principal": {
                "AWS": "*"
            },
            "Action": [
                "s3:PutObject",
                "s3:PutObjectAcl"
            ],
            "Resource": "arn:aws:s3:::${aws_s3_bucket.ICGORE_HUB_MS_LIB_PROPERTIES.id}/*",
            "Condition": {
                "StringLike": {
                    "s3:x-amz-grant-read": [
                        "*http://acs.amazonaws.com/groups/global/AllUsers*",
                        "*http://acs.amazonaws.com/groups/global/AuthenticatedUsers*"
                    ]
                }
            }
        },
        {
            "Sid": "DenyPublicListACL",
            "Effect": "Deny",
            "Principal": {
                "AWS": "*"
            },
            "Action": "s3:PutBucketAcl",
            "Resource": "arn:aws:s3:::${aws_s3_bucket.ICGORE_HUB_MS_LIB_PROPERTIES.id}",
            "Condition": {
                "StringEquals": {
                    "s3:x-amz-acl": [
                        "public-read",
                        "public-read-write",
                        "authenticated-read"
                    ]
                }
            }
        },
        {
            "Sid": "DenyPublicListGrant",
            "Effect": "Deny",
            "Principal": {
                "AWS": "*"
            },
            "Action": "s3:PutBucketAcl",
            "Resource": "arn:aws:s3:::${aws_s3_bucket.ICGORE_HUB_MS_LIB_PROPERTIES.id}",
            "Condition": {
                "StringLike": {
                    "s3:x-amz-grant-read": [
                        "*http://acs.amazonaws.com/groups/global/AllUsers*",
                        "*http://acs.amazonaws.com/groups/global/AuthenticatedUsers*"
                    ]
                }
            }
        },
        {
            "Sid": "DenyPublicReadConsole",
            "Effect": "Deny",
            "Principal": {
                "AWS": "*"
            },
            "Action": [
                "s3:PutObjectAcl",
                "s3:PutBucketAcl"
            ],
            "Resource": [
                "arn:aws:s3:::${aws_s3_bucket.ICGORE_HUB_MS_LIB_PROPERTIES.id}/*",
                "arn:aws:s3:::${aws_s3_bucket.ICGORE_HUB_MS_LIB_PROPERTIES.id}"
            ],
            "Condition": {
                "Null": {
                    "s3:x-amz-grant-write": "true",
                    "s3:x-amz-grant-read": "true",
                    "s3:x-amz-acl": "true",
                    "s3:x-amz-grant-read-acp": "true",
                    "s3:x-amz-grant-full-control": "true",
                    "s3:x-amz-grant-write-acp": "true"
                }
            }
        },
        {
            "Sid": "DevInstanceListBucketAllow",
            "Effect": "Allow",
            "Principal": {
                "AWS": "arn:aws:iam::${var.gore-dev-account}:role/CMS-SSMRole"
            },
            "Action": "s3:ListBucket",
            "Resource": "arn:aws:s3:::${aws_s3_bucket.ICGORE_HUB_MS_LIB_PROPERTIES.id}"
        },
        {
            "Sid": "DevInstanceObjectAllow",
            "Effect": "Allow",
            "Principal": {
                "AWS": "arn:aws:iam::${var.gore-dev-account}:role/CMS-SSMRole"
            },
            "Action": [
                "s3:GetObject",
                "s3:PutObject",
                "s3:DeleteObject"
            ],
            "Resource": [
                "arn:aws:s3:::${aws_s3_bucket.ICGORE_HUB_MS_LIB_PROPERTIES.id}/DEV*/*",
                "arn:aws:s3:::${aws_s3_bucket.ICGORE_HUB_MS_LIB_PROPERTIES.id}/QA*/*",
                "arn:aws:s3:::${aws_s3_bucket.ICGORE_HUB_MS_LIB_PROPERTIES.id}/TBT*/*",
                "arn:aws:s3:::${aws_s3_bucket.ICGORE_HUB_MS_LIB_PROPERTIES.id}/PPS*/*",
                "arn:aws:s3:::${aws_s3_bucket.ICGORE_HUB_MS_LIB_PROPERTIES.id}/UAT*/*"
            ]
        },
        {
            "Sid": "HubInstanceListBucketAllow",
            "Effect": "Allow",
            "Principal": {
                "AWS": "arn:aws:iam::${var.gore-hub-account}:role/CMS-SSMRole"
            },
            "Action": "s3:ListBucket",
            "Resource": "arn:aws:s3:::${aws_s3_bucket.ICGORE_HUB_MS_LIB_PROPERTIES.id}"
        },
        {
            "Sid": "HubInstanceObjectAllow",
            "Effect": "Allow",
            "Principal": {
                "AWS": "arn:aws:iam::${var.gore-hub-account}:role/CMS-SSMRole"
            },
            "Action": [
                "s3:GetObject",
                "s3:PutObject",
                "s3:DeleteObject"
            ],
            "Resource": [
                "arn:aws:s3:::${aws_s3_bucket.ICGORE_HUB_MS_LIB_PROPERTIES.id}/DEV*/*",
                "arn:aws:s3:::${aws_s3_bucket.ICGORE_HUB_MS_LIB_PROPERTIES.id}/QA*/*",
                "arn:aws:s3:::${aws_s3_bucket.ICGORE_HUB_MS_LIB_PROPERTIES.id}/TBT*/*",
                "arn:aws:s3:::${aws_s3_bucket.ICGORE_HUB_MS_LIB_PROPERTIES.id}/UAT*/*",
                "arn:aws:s3:::${aws_s3_bucket.ICGORE_HUB_MS_LIB_PROPERTIES.id}/PROD/*"
            ]
        },
        {
            "Sid": "ProdInstanceListBucketAllow",
            "Effect": "Allow",
            "Principal": {
                "AWS": "arn:aws:iam::${var.gore-prod-account}:role/CMS-SSMRole"
            },
            "Action": "s3:ListBucket",
            "Resource": "arn:aws:s3:::${aws_s3_bucket.ICGORE_HUB_MS_LIB_PROPERTIES.id}"
        },
        {
            "Sid": "ProdInstanceObjectAllow",
            "Effect": "Allow",
            "Principal": {
                "AWS": "arn:aws:iam::${var.gore-prod-account}:role/CMS-SSMRole"
            },
            "Action": [
                "s3:GetObject",
                "s3:PutObject",
                "s3:DeleteObject"
            ],
            "Resource": [
                "arn:aws:s3:::${aws_s3_bucket.ICGORE_HUB_MS_LIB_PROPERTIES.id}/PROD/*"
            ]
        }
    ]
}
POLICY
}

resource "aws_s3_bucket" "ICGORE_HUB_HAPROXY" {
  bucket = "gore-haproxy-deployment"
  acl = "private"

  lifecycle_rule {
    enabled = true

    transition {
      days = 30
      storage_class = "STANDARD_IA"
    }

  }

  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm     = "AES256"
      }
    }
  }
  
  tags {
    Name = "gore-haproxy-deployment"
  }
}

resource "aws_s3_bucket_policy" "ICGORE_HUB_HAPROXY_BUCKET_POLICY" {
  bucket = "${aws_s3_bucket.ICGORE_HUB_HAPROXY.id}"

  policy = <<POLICY
{
    "Version": "2012-10-17",
    "Id": "ICGORE_HUB_HAPROXY_BUCKET_POLICY",
    "Statement": [
        {
            "Sid": "DenyPublicReadACL",
            "Effect": "Deny",
            "Principal": {
                "AWS": "*"
            },
            "Action": [
                "s3:PutObject",
                "s3:PutObjectAcl"
            ],
            "Resource": "arn:aws:s3:::${aws_s3_bucket.ICGORE_HUB_HAPROXY.id}/*",
            "Condition": {
                "StringEquals": {
                    "s3:x-amz-acl": [
                        "public-read",
                        "public-read-write",
                        "authenticated-read"
                    ]
                }
            }
        },
        {
            "Sid": "DenyPublicReadGrant",
            "Effect": "Deny",
            "Principal": {
                "AWS": "*"
            },
            "Action": [
                "s3:PutObject",
                "s3:PutObjectAcl"
            ],
            "Resource": "arn:aws:s3:::${aws_s3_bucket.ICGORE_HUB_HAPROXY.id}/*",
            "Condition": {
                "StringLike": {
                    "s3:x-amz-grant-read": [
                        "*http://acs.amazonaws.com/groups/global/AllUsers*",
                        "*http://acs.amazonaws.com/groups/global/AuthenticatedUsers*"
                    ]
                }
            }
        },
        {
            "Sid": "DenyPublicListACL",
            "Effect": "Deny",
            "Principal": {
                "AWS": "*"
            },
            "Action": "s3:PutBucketAcl",
            "Resource": "arn:aws:s3:::${aws_s3_bucket.ICGORE_HUB_HAPROXY.id}",
            "Condition": {
                "StringEquals": {
                    "s3:x-amz-acl": [
                        "public-read",
                        "public-read-write",
                        "authenticated-read"
                    ]
                }
            }
        },
        {
            "Sid": "DenyPublicListGrant",
            "Effect": "Deny",
            "Principal": {
                "AWS": "*"
            },
            "Action": "s3:PutBucketAcl",
            "Resource": "arn:aws:s3:::${aws_s3_bucket.ICGORE_HUB_HAPROXY.id}",
            "Condition": {
                "StringLike": {
                    "s3:x-amz-grant-read": [
                        "*http://acs.amazonaws.com/groups/global/AllUsers*",
                        "*http://acs.amazonaws.com/groups/global/AuthenticatedUsers*"
                    ]
                }
            }
        },
        {
            "Sid": "DenyPublicReadConsole",
            "Effect": "Deny",
            "Principal": {
                "AWS": "*"
            },
            "Action": [
                "s3:PutObjectAcl",
                "s3:PutBucketAcl"
            ],
            "Resource": [
                "arn:aws:s3:::${aws_s3_bucket.ICGORE_HUB_HAPROXY.id}/*",
                "arn:aws:s3:::${aws_s3_bucket.ICGORE_HUB_HAPROXY.id}"
            ],
            "Condition": {
                "Null": {
                    "s3:x-amz-grant-write": "true",
                    "s3:x-amz-grant-read": "true",
                    "s3:x-amz-acl": "true",
                    "s3:x-amz-grant-read-acp": "true",
                    "s3:x-amz-grant-full-control": "true",
                    "s3:x-amz-grant-write-acp": "true"
                }
            }
        },
        {
            "Sid": "DevInstanceListBucketAllow",
            "Effect": "Allow",
            "Principal": {
                "AWS": "arn:aws:iam::${var.gore-dev-account}:role/CMS-SSMRole"
            },
            "Action": "s3:ListBucket",
            "Resource": "arn:aws:s3:::${aws_s3_bucket.ICGORE_HUB_HAPROXY.id}"
        },
        {
            "Sid": "DevInstanceObjectAllow",
            "Effect": "Allow",
            "Principal": {
                "AWS": "arn:aws:iam::${var.gore-dev-account}:role/CMS-SSMRole"
            },
            "Action": [
                "s3:GetObject",
                "s3:PutObject",
                "s3:DeleteObject"
            ],
            "Resource": [
                "arn:aws:s3:::${aws_s3_bucket.ICGORE_HUB_HAPROXY.id}/DEV*/*",
                "arn:aws:s3:::${aws_s3_bucket.ICGORE_HUB_HAPROXY.id}/QA*/*",
                "arn:aws:s3:::${aws_s3_bucket.ICGORE_HUB_HAPROXY.id}/TBT*/*",
                "arn:aws:s3:::${aws_s3_bucket.ICGORE_HUB_HAPROXY.id}/UAT*/*"
            ]
        },
        {
            "Sid": "HubInstanceListBucketAllow",
            "Effect": "Allow",
            "Principal": {
                "AWS": "arn:aws:iam::${var.gore-hub-account}:role/CMS-SSMRole"
            },
            "Action": "s3:ListBucket",
            "Resource": "arn:aws:s3:::${aws_s3_bucket.ICGORE_HUB_HAPROXY.id}"
        },
        {
            "Sid": "HubInstanceObjectAllow",
            "Effect": "Allow",
            "Principal": {
                "AWS": "arn:aws:iam::${var.gore-hub-account}:role/CMS-SSMRole"
            },
            "Action": [
                "s3:GetObject",
                "s3:PutObject",
                "s3:DeleteObject"
            ],
            "Resource": [
                "arn:aws:s3:::${aws_s3_bucket.ICGORE_HUB_HAPROXY.id}/DEV*/*",
                "arn:aws:s3:::${aws_s3_bucket.ICGORE_HUB_HAPROXY.id}/QA*/*",
                "arn:aws:s3:::${aws_s3_bucket.ICGORE_HUB_HAPROXY.id}/UAT*/*",
                "arn:aws:s3:::${aws_s3_bucket.ICGORE_HUB_HAPROXY.id}/PROD/*"
            ]
        },
        {
            "Sid": "ProdInstanceListBucketAllow",
            "Effect": "Allow",
            "Principal": {
                "AWS": "arn:aws:iam::${var.gore-prod-account}:role/CMS-SSMRole"
            },
            "Action": "s3:ListBucket",
            "Resource": "arn:aws:s3:::${aws_s3_bucket.ICGORE_HUB_HAPROXY.id}"
        },
        {
            "Sid": "ProdInstanceObjectAllow",
            "Effect": "Allow",
            "Principal": {
                "AWS": "arn:aws:iam::${var.gore-prod-account}:role/CMS-SSMRole"
            },
            "Action": [
                "s3:GetObject",
                "s3:PutObject",
                "s3:DeleteObject"
            ],
            "Resource": [
                "arn:aws:s3:::${aws_s3_bucket.ICGORE_HUB_HAPROXY.id}/PROD/*"
            ]
        }
    ]
}
POLICY
}


resource "aws_s3_bucket" "ICGORE_HUB_NORTH_LB_ACCESS_LOG" {
  bucket = "gore-north-lb-accesslog"
  acl = "private"

  lifecycle_rule {
    enabled = true

    transition {
      days = 30
      storage_class = "STANDARD_IA"
    }

  }

  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm     = "AES256"
      }
    }
  }

  tags {
    Name = "gore-north-lb-accesslog"
  }
}

resource "aws_s3_bucket_policy" "ICGORE_HUB_NORTH_LB_ACCESS_LOG_POLICY" {
  bucket = "${aws_s3_bucket.ICGORE_HUB_NORTH_LB_ACCESS_LOG.id}"

  policy = <<POLICY
{
    "Version": "2012-10-17",
    "Id": "ICGORE_HUB_NORTH_LB_ACCESS_LOG_POLICY",
    "Statement": [
        {
            "Sid": "DenyPublicReadACL",
            "Effect": "Deny",
            "Principal": {
                "AWS": "*"
            },
            "Action": [
                "s3:PutObject",
                "s3:PutObjectAcl"
            ],
            "Resource": "arn:aws:s3:::${aws_s3_bucket.ICGORE_HUB_NORTH_LB_ACCESS_LOG.id}/*",
            "Condition": {
                "StringEquals": {
                    "s3:x-amz-acl": [
                        "public-read",
                        "public-read-write",
                        "authenticated-read"
                    ]
                }
            }
        },
        {
            "Sid": "DenyPublicReadGrant",
            "Effect": "Deny",
            "Principal": {
                "AWS": "*"
            },
            "Action": [
                "s3:PutObject",
                "s3:PutObjectAcl"
            ],
            "Resource": "arn:aws:s3:::${aws_s3_bucket.ICGORE_HUB_NORTH_LB_ACCESS_LOG.id}/*",
            "Condition": {
                "StringLike": {
                    "s3:x-amz-grant-read": [
                        "*http://acs.amazonaws.com/groups/global/AllUsers*",
                        "*http://acs.amazonaws.com/groups/global/AuthenticatedUsers*"
                    ]
                }
            }
        },
        {
            "Sid": "DenyPublicListACL",
            "Effect": "Deny",
            "Principal": {
                "AWS": "*"
            },
            "Action": "s3:PutBucketAcl",
            "Resource": "arn:aws:s3:::${aws_s3_bucket.ICGORE_HUB_NORTH_LB_ACCESS_LOG.id}",
            "Condition": {
                "StringEquals": {
                    "s3:x-amz-acl": [
                        "public-read",
                        "public-read-write",
                        "authenticated-read"
                    ]
                }
            }
        },
        {
            "Sid": "DenyPublicListGrant",
            "Effect": "Deny",
            "Principal": {
                "AWS": "*"
            },
            "Action": "s3:PutBucketAcl",
            "Resource": "arn:aws:s3:::${aws_s3_bucket.ICGORE_HUB_NORTH_LB_ACCESS_LOG.id}",
            "Condition": {
                "StringLike": {
                    "s3:x-amz-grant-read": [
                        "*http://acs.amazonaws.com/groups/global/AllUsers*",
                        "*http://acs.amazonaws.com/groups/global/AuthenticatedUsers*"
                    ]
                }
            }
        },
        {
            "Sid": "DenyPublicReadConsole",
            "Effect": "Deny",
            "Principal": {
                "AWS": "*"
            },
            "Action": [
                "s3:PutObjectAcl",
                "s3:PutBucketAcl"
            ],
            "Resource": [
                "arn:aws:s3:::${aws_s3_bucket.ICGORE_HUB_NORTH_LB_ACCESS_LOG.id}/*",
                "arn:aws:s3:::${aws_s3_bucket.ICGORE_HUB_NORTH_LB_ACCESS_LOG.id}"
            ],
            "Condition": {
                "Null": {
                    "s3:x-amz-grant-write": "true",
                    "s3:x-amz-grant-read": "true",
                    "s3:x-amz-acl": "true",
                    "s3:x-amz-grant-read-acp": "true",
                    "s3:x-amz-grant-full-control": "true",
                    "s3:x-amz-grant-write-acp": "true"
                }
            }
        },
        {
            "Sid": "HubLBBucketAllow",
            "Effect": "Allow",
            "Principal": {
                "AWS": "arn:aws:iam::${var.gore-hub-account}:role/CMS-SSMRole"
            },
            "Action": "s3:ListBucket",
            "Resource": "arn:aws:s3:::${aws_s3_bucket.ICGORE_HUB_NORTH_LB_ACCESS_LOG.id}"
        },
        {
            "Sid": "HubLBObjectAllow",
            "Effect": "Allow",
            "Principal": {
                "AWS": "arn:aws:iam::985666609251:root"
            },
            "Action": [
                "s3:GetObject",
                "s3:PutObject",
                "s3:DeleteObject"
            ],
            "Resource": [
                "arn:aws:s3:::${aws_s3_bucket.ICGORE_HUB_NORTH_LB_ACCESS_LOG.id}/DEV*/*",
                "arn:aws:s3:::${aws_s3_bucket.ICGORE_HUB_NORTH_LB_ACCESS_LOG.id}/QA*/*",
                "arn:aws:s3:::${aws_s3_bucket.ICGORE_HUB_NORTH_LB_ACCESS_LOG.id}/TBT*/*",
                "arn:aws:s3:::${aws_s3_bucket.ICGORE_HUB_NORTH_LB_ACCESS_LOG.id}/BAT*/*",
                "arn:aws:s3:::${aws_s3_bucket.ICGORE_HUB_NORTH_LB_ACCESS_LOG.id}/PERF/*",
                "arn:aws:s3:::${aws_s3_bucket.ICGORE_HUB_NORTH_LB_ACCESS_LOG.id}/PPS/*",
                "arn:aws:s3:::${aws_s3_bucket.ICGORE_HUB_NORTH_LB_ACCESS_LOG.id}/PROD/*"
            ]
        },
        {
            "Sid": "HubLBServiceAllow",
            "Effect": "Allow",
            "Principal": {
                "Service": "delivery.logs.amazonaws.com"
            },
            "Action": "s3:PutObject",
            "Resource": [
                "arn:aws:s3:::${aws_s3_bucket.ICGORE_HUB_NORTH_LB_ACCESS_LOG.id}/DEV*/*",
                "arn:aws:s3:::${aws_s3_bucket.ICGORE_HUB_NORTH_LB_ACCESS_LOG.id}/QA*/*",
                "arn:aws:s3:::${aws_s3_bucket.ICGORE_HUB_NORTH_LB_ACCESS_LOG.id}/TBT*/*",
                "arn:aws:s3:::${aws_s3_bucket.ICGORE_HUB_NORTH_LB_ACCESS_LOG.id}/BAT*/*",
                "arn:aws:s3:::${aws_s3_bucket.ICGORE_HUB_NORTH_LB_ACCESS_LOG.id}/PERF/*",
                "arn:aws:s3:::${aws_s3_bucket.ICGORE_HUB_NORTH_LB_ACCESS_LOG.id}/PPS/*",
                "arn:aws:s3:::${aws_s3_bucket.ICGORE_HUB_NORTH_LB_ACCESS_LOG.id}/PROD/*"
                ],
            "Condition": {
                "StringEquals": {
                "s3:x-amz-acl": "bucket-owner-full-control"
                }
            }
        },
        {
            "Sid": "HubLBAclAllow",
            "Effect": "Allow",
            "Principal": {
                "Service": "delivery.logs.amazonaws.com"
            },
            "Action": "s3:GetBucketAcl",
            "Resource": "arn:aws:s3:::${aws_s3_bucket.ICGORE_HUB_NORTH_LB_ACCESS_LOG.id}"
            }
    ]
}
POLICY
}

# 985666609251 is the elb-account-id for canada central 