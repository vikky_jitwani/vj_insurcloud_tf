#=======================================================================
# EBS format and mount instructions
#=======================================================================
# Ensure fstab entry present:
#
# /dev/sdh /var/lib/docker xfs defaults,nofail 0 2
#
# mkfs.xfs /dev/sdh
# systemctl stop docker
# mount -a
# systemctl start docker
#

#=======================================================================
# JENKINS CLIENT (DEV)
#=======================================================================
resource "aws_ebs_volume" "ICCW_DEV_JENKINS_CLIENT_EBS_DOCKER_VOL" {
  availability_zone = "${aws_instance.ICCW_DEV_JENKINS_CLIENT.availability_zone}"
  size              = 25
  type              = "gp3"
  encrypted         = true
  kms_key_id        = "arn:aws:kms:ca-central-1:532382648425:key/36d3cec7-2957-4ebf-9b2c-4d15d7128ce9"

  tags = {
    Name          = "ICCW_DEV_JENKINS_CLIENT_EBS_DOCKER_VOL"
    Docker        = "True"
  }
}

resource "aws_volume_attachment" "ICCW_DEV_JENKINS_CLIENT_EBS_DOCKER_VOL_ATTACHMENT" {
  device_name = "/dev/sdh"
  volume_id   = "${aws_ebs_volume.ICCW_DEV_JENKINS_CLIENT_EBS_DOCKER_VOL.id}"
  instance_id = "${aws_instance.ICCW_DEV_JENKINS_CLIENT.id}"
}

#=======================================================================
# JENKINS CLIENT (PROD)
#=======================================================================
resource "aws_ebs_volume" "ICCW_PROD_JENKINS_CLIENT_EBS_DOCKER_VOL" {
  availability_zone = "${aws_instance.ICCW_PROD_JENKINS_CLIENT.availability_zone}"
  size              = 25
  type              = "gp3"
  encrypted         = true
  kms_key_id        = "arn:aws:kms:ca-central-1:532382648425:key/36d3cec7-2957-4ebf-9b2c-4d15d7128ce9"

  tags = {
    Name          = "ICCW_PROD_JENKINS_CLIENT_EBS_DOCKER_VOL"
    Docker        = "True"
  }
}

resource "aws_volume_attachment" "ICCW_PROD_JENKINS_CLIENT_EBS_DOCKER_VOL_ATTACHMENT" {
  device_name = "/dev/sdh"
  volume_id   = "${aws_ebs_volume.ICCW_PROD_JENKINS_CLIENT_EBS_DOCKER_VOL.id}"
  instance_id = "${aws_instance.ICCW_PROD_JENKINS_CLIENT.id}"
}
