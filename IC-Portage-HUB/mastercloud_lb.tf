/*
##### NLBs ####################
########## External Load Balancers: North of firewall ###########
#=======================================================================
# DEV01
#=======================================================================
resource "aws_lb" "ICPORTAGE_DEV01_APIGATEWAY_NLB" {
    name               = "portage-insurcloud-apigw-dev01"
    internal           = true
    load_balancer_type = "network"
    enable_cross_zone_load_balancing = false
    subnets            = [
        "${var.portage-hub-private-1a-subnet}",
        "${var.portage-hub-private-1b-subnet}"
    ] # Changing this value will force a recreation of the resource
    
    enable_deletion_protection = false

    # access_logs {
    #     bucket  = "${aws_s3_bucket.ICPORTAGE_DEV01_APIGATEWAY_NLB.bucket}"
    #     enabled = true
    # }

    tags = {
        Name = "PORTAGE-DEV01-APIGATEWAY-NLB"
    }
}

resource "aws_lb_target_group" "ICPORTAGE_DEV01_APIGATEWAY_NLB_TARGET_GROUP" {
    name        = "PORTAGE-DEV01-APIGATEWAY-NLB"
    port        = 80
    protocol    = "TCP"
    target_type = "ip"
    vpc_id      = "${var.vpc_id}"

    health_check {
        protocol = "TCP"
        port = 8444
    }

    tags = {
        Name = "PORTAGE-DEV01-APIGATEWAY-NLB-TARGET-GROUP"
    }
}

resource "aws_lb_listener" "ICPORTAGE_DEV01_APIGATEWAY_NLB_LISTENER" {
    load_balancer_arn = "${aws_lb.ICPORTAGE_DEV01_APIGATEWAY_NLB.arn}"
    port              = "80"
    protocol          = "TCP"

    default_action {
        type             = "forward"
        target_group_arn = "${aws_lb_target_group.ICPORTAGE_DEV01_APIGATEWAY_NLB_TARGET_GROUP.arn}"
    }
}

*/