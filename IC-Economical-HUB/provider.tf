provider "aws" {
  region     = "ca-central-1"
  assume_role = {
    role_arn = "arn:aws:iam::348868695504:role/admin_ops_auto"
  }
}

terraform {
 backend "s3" {
   bucket     = "a-c-mc-p-a004-economical-tfstate"
   key        = "IC-Economical-HUB/STATE/current.tf"
   region     = "ca-central-1"
//    bucket     = "aws-cms-terraformstate"
//    key        = "ICEconomical/hub.tfstate"
//    region     = "ca-central-1"
//    role_arn   = "arn:aws:iam::393766723611:role/admin_ops_auto"
      #encrypt    = true
      #kms_key_id = "arn:aws:kms:ca-central-1::key/"
  }
}

data "terraform_remote_state" "dev_remote_state" {
  backend = "s3"
  config = {
    bucket     = "a-c-mc-p-a004-economical-tfstate"
    key        = "IC-Economical-DEV/STATE/current.tf"
    region     = "ca-central-1"
//    role_arn   = "arn:aws:iam::393766723611:role/admin_ops_auto"
//    bucket     = "aws-cms-terraformstate"
//    key        = "ICEconomical/dev.tfstate"
//    region     = "ca-central-1"
  }
}

data "terraform_remote_state" "prod_remote_state" {
  backend = "s3"
  config = {
    bucket     = "a-c-mc-p-a004-economical-tfstate"
    key        = "IC-Economical-PROD/STATE/current.tf"
    region     = "ca-central-1"
//    role_arn   = "arn:aws:iam::393766723611:role/admin_ops_auto"
//    bucket     = "aws-cms-terraformstate"
//    key        = "ICEconomical/prod.tfstate"
//    region     = "ca-central-1"
  }
}
