#Envrionment specific variables
client_name = "IC-Economical"
environment_name = "HUB"
environment_type = "NP"
client_number = "A004"
#ad-dc1 = "10.10.4.10"
#ad-dc2 = "10.10.6.10"
volume_key = "arn:aws:kms:ca-central-1:348868695504:key/5cf14549-e451-4614-8d33-927f883fda2d"
ami_id_windows2012sql14enterprise = "ami-07a67e65265ba59dc"
ami_id_windows2019container = "ami-013453540e1aa0d48"
ami_id_windows2019base = "ami-06b8f69a907cf6bb2"
ami_id_windows2016base = "ami-0bce38ba7ac166aa9"
ami_id_windows2012R2base = "ami-09b707ce10f2e9cee"
ami_id_windows2008base = "ami-0446e08d2d99fc8c9"
ami_id_redhatlinux75 = "ami-05c33d286020a47f9"
ami_id_ubuntu1804baseimage = "ami-032172cc5f34b32fc"
ami_id_ubuntu1604baseimage = "ami-04f40cca01b3186ea"
ami_id_amazonlinux201712base = "ami-0a777bbf8ef3f43bf"
ami_id_amazonlinux201709base = "ami-058dba934995c5468"
ami_id_amazonlinux2base = "ami-0ad9abdd25c431def"
ami_id_amazonlinux201803base = "ami-06829694f407e15c1"
ami_id_amazonlinuxbase20200506 = "ami-071594d49d11fcb38"


#VPC Specific variables
vpc_id = "vpc-0cf5ca59ac23bcbc8"
vpc_cidr = "10.227.0.0/22"
#sg_cidr = "XXX"

economical-hub-database-1a-subnet = "subnet-043a888a82fb33c89"
economical-hub-database-1b-subnet = "subnet-09e04d83d761479e3"
economical-hub-private-1a-subnet = "subnet-0dcb05b61eed26c57"
economical-hub-private-1b-subnet = "subnet-0c9af8ff9e1b5059d"
economical-hub-public-1a-subnet = "subnet-04e52f35432e30bba"
economical-hub-public-1b-subnet = "subnet-0776abc9a1f1bacad"

#R53 
zone_id = "Z09556903OW0XZGLU9GLL"

#Accounts
economical-dev-account = "632109065501"
economical-prod-account = "852868701745"
economical-hub-account = "1348868695504"
