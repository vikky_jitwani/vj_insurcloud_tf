#Envrionment specific variables
client_name = "IC-Bcaa"
environment_name = "HUB"
environment_type = "NP"
client_number = "A010"
#ad-dc1 = "10.10.4.10"
#ad-dc2 = "10.10.6.10"
volume_key = "arn:aws:kms:ca-central-1:186239701968:key/962ddbb1-d9c4-4c84-ad32-8df4e4252360"
ami_id_windows2012sql14enterprise_2020_12_11 	= "ami-0a98a578d8c99a362"
ami_id_windows2019container_2020_12_11  		= "ami-02f7108e0d1e573d9"
ami_id_windows2019base_2020_12_11       		= "ami-07df0407a8c4fe342"
ami_id_windows2016base_2020_12_11 				= "ami-02d51b2c3cf987496"
#ami_id_redhatlinux75_2020_12_11             	= ""
ami_id_amazonlinux201712base_2020_12_11     	= "ami-06c9db22258a28dd8"
ami_id_amazonlinux201709base_2020_12_11     	= "ami-04c1a0b420a81f05a"
ami_id_amazonlinux2base_2020_12_11           	= "ami-02496f7e580ca22e3"
ami_id_amazonlinux201803base_2020_12_11      	= "ami-04f3673aac82a6c59"
ami_id_windows2016base_server_dhicdev01			= "ami-02c6af2c8292710be"
ami_id_windows2016base_database_dhicdev01		= "ami-008e55415d7aab0cd"


#VPC Specific variables
vpc_id = "vpc-05142afdf432c6ad5"
vpc_cidr = "10.41.0.0/22"
#sg_cidr = "XXX"

bcaa-hub-database-1a-subnet = "subnet-0d25d6d8d40ac180f"
bcaa-hub-database-1b-subnet = "subnet-07401e29268a738d6"
bcaa-hub-private-1a-subnet = "subnet-0866e2bed8cb63994"
bcaa-hub-private-1b-subnet = "subnet-0f47c52518aae03f8"
bcaa-hub-public-1a-subnet = "subnet-082b7ebdcec84556c"
bcaa-hub-public-1b-subnet = "subnet-071fcfd10c541289c"

#R53 
zone_id = "Z003803910O3URB8M6RP2"

#Accounts
bcaa-dev-account = "775629271970"
bcaa-prod-account = "338020371242"
bcaa-hub-account = "057006516829"
