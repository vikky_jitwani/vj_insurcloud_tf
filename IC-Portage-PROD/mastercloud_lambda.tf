## Resources##
resource "aws_lambda_layer_version" "ODBCPy_layer" {
  s3_bucket = "portage-lambda-dependencies"
  s3_key = "ODBCPythonDependencies_1.zip"
  layer_name = "ODBCPythonDependencies_1"
  compatible_runtimes = ["python3.7"]
}

## PPS Lambdas ##
resource "aws_lambda_permission" "apigw_lambda_ppsglfeed" {
  statement_id  = "AllowExecutionFromAPIGateway"
  action        = "lambda:InvokeFunction"
  function_name = "${aws_lambda_function.portage-PPSGLFeed.function_name}"
  principal     = "apigateway.amazonaws.com"

  # More: http://docs.aws.amazon.com/apigateway/latest/developerguide/api-gateway-control-access-using-iam-policies-to-invoke-api.html
  # They are in the hub 
  source_arn = "arn:aws:execute-api:${var.region}:${var.portage-hub-account}:nbfett4mhd/*/*/*"
}

resource "aws_lambda_function" "portage-PPSGLFeed" {

  function_name = "portage-PPSGLFeed"
  role          = "arn:aws:iam::625725589452:role/LambdaAssume"
  handler       = "PortageGLIntegration_get.readFromTable"
  runtime       = "python3.7"
  timeout       = 60
  layers        = ["${aws_lambda_layer_version.ODBCPy_layer.arn}"]
  s3_bucket     = "portage-lambda-dependencies"
  s3_key        = "PortageGLIntegration_get.zip"

  vpc_config    ={
    subnet_ids = ["${var.portage-prod-private-1a-subnet}", "${var.portage-prod-private-1b-subnet}"]
    security_group_ids = ["${aws_security_group.SG_LAMBDA.id}"]
  }

  environment {
    variables = {
      REGION_NAME = "ca-central-1"
      SECRET_NAME = "Portage-PPS-ICUser"
    }
  }
}

#Lambda function for PPS Portage GL Update
resource "aws_lambda_permission" "apigw_lambda_ppsglupdate" {
  statement_id  = "AllowExecutionFromAPIGateway"
  action        = "lambda:InvokeFunction"
  function_name = "${aws_lambda_function.portage-PPSGLUpdate.function_name}"
  principal     = "apigateway.amazonaws.com"

  # More: http://docs.aws.amazon.com/apigateway/latest/developerguide/api-gateway-control-access-using-iam-policies-to-invoke-api.html
  # They are in the hub 
  #source_arn = "arn:aws:execute-api:${var.region}:${var.accountId}:${aws_api_gateway_rest_api.GLCheque.id}/*/${aws_api_gateway_method.PortageGLUpdate.http_method}${aws_api_gateway_resource.PortageGLUpdate.path}"
  source_arn = "arn:aws:execute-api:${var.region}:${var.portage-hub-account}:nbfett4mhd/*/*/*"
}

resource "aws_lambda_function" "portage-PPSGLUpdate" {
  function_name = "portage-PPSGLUpdate"
  role          = "arn:aws:iam::625725589452:role/LambdaAssume"
  handler       = "PortageGLIntegration_post.putInTable"
  runtime       = "python3.7"
  timeout       = 60
  layers        = ["${aws_lambda_layer_version.ODBCPy_layer.arn}"]
  s3_bucket     = "portage-lambda-dependencies"
  s3_key        = "PortageGLIntegration_post.zip"

  vpc_config    ={
    subnet_ids = ["${var.portage-prod-private-1a-subnet}", "${var.portage-prod-private-1b-subnet}"]
    security_group_ids = ["${aws_security_group.SG_LAMBDA.id}"]
  }

  environment {
    variables = {
      REGION_NAME = "ca-central-1"
      SECRET_NAME = "Portage-PPS-ICUser"
    }
  }
}

##PROD LAMBDAs##
resource "aws_lambda_permission" "apigw_lambda_prodglfeed" {
  statement_id  = "AllowExecutionFromAPIGateway"
  action        = "lambda:InvokeFunction"
  function_name = "${aws_lambda_function.portage-ProdGLFeed.function_name}"
  principal     = "apigateway.amazonaws.com"

  # More: http://docs.aws.amazon.com/apigateway/latest/developerguide/api-gateway-control-access-using-iam-policies-to-invoke-api.html
  # They are in the hub 
  # source_arn = "arn:aws:execute-api:${var.region}:${var.accountId}:${aws_api_gateway_rest_api.GLCheque.id}/*/${aws_api_gateway_method.PortageGLFeed.http_method}${aws_api_gateway_resource.PortageGLFeed.path}"
  source_arn = "arn:aws:execute-api:${var.region}:${var.portage-hub-account}:nbfett4mhd/*/*/*"
}

resource "aws_lambda_function" "portage-ProdGLFeed" {
  function_name = "portage-ProdGLFeed"
  role          = "arn:aws:iam::625725589452:role/LambdaAssume"
  handler       = "PortageGLIntegration_get.readFromTable"
  runtime       = "python3.7"
  timeout       = 60
  layers        = ["${aws_lambda_layer_version.ODBCPy_layer.arn}"]
  s3_bucket     = "portage-lambda-dependencies"
  s3_key        = "PortageGLIntegration_get.zip"

  vpc_config    ={
    subnet_ids = ["${var.portage-prod-private-1a-subnet}", "${var.portage-prod-private-1b-subnet}"]
    security_group_ids = ["${aws_security_group.SG_LAMBDA.id}"]
  }

  environment {
    variables = {
      REGION_NAME = "ca-central-1"
      SECRET_NAME = "PORTAGE_PROD_InfoCenter_DBOwner"
    }
  }
}

#Lambda function for PPS Portage GL Update
resource "aws_lambda_permission" "apigw_lambda_prodglupdate" {
  statement_id  = "AllowExecutionFromAPIGateway"
  action        = "lambda:InvokeFunction"
  function_name = "${aws_lambda_function.portage-ProdGLUpdate.function_name}"
  principal     = "apigateway.amazonaws.com"

  # More: http://docs.aws.amazon.com/apigateway/latest/developerguide/api-gateway-control-access-using-iam-policies-to-invoke-api.html
  # They are in the hub 
  #source_arn = "arn:aws:execute-api:${var.region}:${var.accountId}:${aws_api_gateway_rest_api.GLCheque.id}/*/${aws_api_gateway_method.PortageGLUpdate.http_method}${aws_api_gateway_resource.PortageGLUpdate.path}"
   source_arn = "arn:aws:execute-api:${var.region}:${var.portage-hub-account}:nbfett4mhd/*/*/*"
}

resource "aws_lambda_function" "portage-ProdGLUpdate" {
  function_name = "portage-ProdGLUpdate"
  role          = "arn:aws:iam::625725589452:role/LambdaAssume"
  handler       = "PortageGLIntegration_post.putInTable"
  runtime       = "python3.7"
  timeout       = 60
  layers        = ["${aws_lambda_layer_version.ODBCPy_layer.arn}"]
  s3_bucket     = "portage-lambda-dependencies"
  s3_key        = "PortageGLIntegration_post.zip"

  vpc_config    ={
    subnet_ids = ["${var.portage-prod-private-1a-subnet}", "${var.portage-prod-private-1b-subnet}"]
    security_group_ids = ["${aws_security_group.SG_LAMBDA.id}"]
  }

  environment {
    variables = {
      REGION_NAME = "ca-central-1"
      SECRET_NAME = "PORTAGE_PROD_InfoCenter_DBOwner"
    }
  }
}