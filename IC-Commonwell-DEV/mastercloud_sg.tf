resource "aws_security_group" "SG_JOB_SCHEDULER" {
  name = "COMMONWELL-JOB-SCHEDULER-SG"

  ingress {
    from_port       = 4444
    to_port         = 4444
    protocol        = "tcp"
    cidr_blocks = [
      "${var.deloitte_cidr}",
      "${var.hub_cidr}",
      "${var.commonwell_aws_cidr}",
      "${var.commonwell_lindsay_cidr}",
      "${var.commonwell_whitby_cidr}"
    ]
  }

  ingress {
    from_port       = 4444
    to_port         = 4444
    protocol        = "udp"
    cidr_blocks = [
      "${var.deloitte_cidr}",
      "${var.hub_cidr}",
      "${var.commonwell_aws_cidr}",
      "${var.commonwell_lindsay_cidr}",
      "${var.commonwell_whitby_cidr}"
    ]
  }

  ingress {
    from_port       = 4446
    to_port         = 4446
    protocol        = "tcp"
    cidr_blocks = [
      "${var.deloitte_cidr}",
      "${var.hub_cidr}",
      "${var.commonwell_aws_cidr}",
      "${var.commonwell_lindsay_cidr}",
      "${var.commonwell_whitby_cidr}"
    ]
  }

  ingress {
    from_port       = 4446
    to_port         = 4446
    protocol        = "udp"
    cidr_blocks = [
      "${var.deloitte_cidr}",
      "${var.hub_cidr}",
      "${var.commonwell_aws_cidr}",
      "${var.commonwell_lindsay_cidr}",
      "${var.commonwell_whitby_cidr}"
    ]
  }

  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    Name = "COMMONWELL-JOB-SCHEDULER-SG"
  }
}

resource "aws_security_group" "SG_COMMONWELL_MICROSERVICES" {
  name = "COMMONWELL-MICROSERVICES-SG"

  ingress {
    from_port   = 8443
    to_port     = 8443
    protocol    = "tcp"
    security_groups = [
      "${aws_security_group.SG_EXTERNAL_HAPROXY.id}"
    ]
    description = "Orchestrator"
  }

  ingress {
    from_port   = 15672
    to_port     = 15672
    protocol    = "tcp"
    cidr_blocks = [
      "${var.hub_cidr}"
    ]
    security_groups = [
      "${aws_security_group.SG_INTERNAL_HAPROXY.id}"
    ]
    description = "RabbitMQ Management UI"
  }

  ingress {
    from_port   = 9002
    to_port     = 9002
    protocol    = "tcp"
    security_groups = [
      "${aws_security_group.SG_INTERNAL_HAPROXY.id}"
    ]
    description = "Edge"
  }

  ingress {
    from_port   = 9222
    to_port     = 9222
    protocol    = "tcp"
    security_groups = [
      "${aws_security_group.SG_EXTERNAL_HAPROXY.id}",
      "${aws_security_group.SG_INTERNAL_HAPROXY.id}"
    ]
    description = "Audatex"
  }

  ingress {
    from_port   = 2377
    to_port     = 2377
    protocol    = "tcp"
    cidr_blocks = [
      "${var.deloitte_cidr}"
    ]
    description = "Cluster Management Communications"
  }

  ingress {
    from_port   = 7946
    to_port     = 7946
    protocol    = "udp"
    cidr_blocks = [
      "${var.deloitte_cidr}"
    ]
    description = "Communication Among Nodes"
  }

  ingress {
    from_port   = 7946
    to_port     = 7946
    protocol    = "tcp"
    cidr_blocks = [
      "${var.deloitte_cidr}"
    ]
    description = "Communication Among Nodes"
  }

  ingress {
    from_port   = 4789
    to_port     = 4789
    protocol    = "udp"
    cidr_blocks = [
      "${var.deloitte_cidr}"
    ]
    description = "Overlay Network Traffic"
  }

  ingress {
    from_port   = 8500
    to_port     = 8500
    protocol    = "tcp"
    cidr_blocks = [
      "${var.hub_cidr}"
    ]
    description = "Consul UI"
  }

  ingress {
    from_port       = 443
    to_port         = 443
    protocol        = "tcp"
    cidr_blocks = [
      "${var.deloitte_cidr}",
      "${var.hub_cidr}"
    ]
  }

  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    Name = "COMMONWELL-MICROSERVICES-SG"
  }
}

resource "aws_security_group" "SG_COMMONWELL_EXSTREAM" {
  name = "COMMONWELL-EXSTREAM-SG"

  ingress {
    from_port   = 28700
    to_port     = 28702
    protocol    = "tcp"
    cidr_blocks = ["${var.default_cidr}"]
  }

  ingress {
    from_port   = 2701
    to_port     = 2702
    protocol    = "tcp"
    cidr_blocks = ["${var.default_cidr}"]
  }

  ingress {
    from_port   = 8080
    to_port     = 8080
    protocol    = "tcp"
    cidr_blocks = ["${var.default_cidr}"]
  }

  ingress {
    from_port   = 8443
    to_port     = 8443
    protocol    = "tcp"
    cidr_blocks = ["${var.default_cidr}"]
  }

  ingress {
    from_port   = 1433
    to_port     = 1433
    protocol    = "tcp"
    cidr_blocks = ["${var.default_cidr}"]
  }

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["${var.default_cidr}"]
    security_groups = ["${aws_security_group.SG_GUIDEWIRE.id}"]
  }

  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["${var.default_cidr}"]
  }

  ingress {
    from_port   = 5858
    to_port     = 5858
    protocol    = "tcp"
    cidr_blocks = ["${var.default_cidr}"]
  }

  ingress {
    from_port   = 3099
    to_port     = 3099
    protocol    = "tcp"
    cidr_blocks = ["${var.default_cidr}"]
  }

  ingress {
    from_port   = 2099
    to_port     = 2099
    protocol    = "tcp"
    cidr_blocks = ["${var.default_cidr}"]
  }

  ingress {
    from_port   = 389
    to_port     = 389
    protocol    = "tcp"
    cidr_blocks = ["${var.default_cidr}"]
  }

  ingress {
    from_port   = 4440
    to_port     = 4440
    protocol    = "tcp"
    cidr_blocks = ["${var.default_cidr}"]
  }

  ingress {
    from_port   = 8009
    to_port     = 8009
    protocol    = "tcp"
    cidr_blocks = ["${var.default_cidr}"]
  }

  ingress {
    from_port   = 4040
    to_port     = 4040
    protocol    = "tcp"
    cidr_blocks = ["${var.default_cidr}"]
  }

  ingress {
    from_port   = 8989
    to_port     = 8989
    protocol    = "tcp"
    cidr_blocks = ["${var.default_cidr}"]
  }

  ingress {
    from_port   = 41616
    to_port     = 41616
    protocol    = "tcp"
    cidr_blocks = ["${var.default_cidr}"]
  }

  ingress {
    from_port   = 5868
    to_port     = 5869
    protocol    = "tcp"
    cidr_blocks = ["${var.default_cidr}"]
  }

  ingress {
    from_port   = 8100
    to_port     = 8100
    protocol    = "tcp"
    cidr_blocks = ["${var.default_cidr}"]
  }

  ingress {
    from_port   = 8300
    to_port     = 8300
    protocol    = "tcp"
    cidr_blocks = ["${var.default_cidr}"]
  }

  ingress {
    from_port   = 8500
    to_port     = 8500
    protocol    = "tcp"
    cidr_blocks = ["${var.default_cidr}"]
  }

  ingress {
    from_port   = 8600
    to_port     = 8603
    protocol    = "tcp"
    cidr_blocks = ["${var.default_cidr}"]
  }

  ingress {
    from_port   = 8512
    to_port     = 8515
    protocol    = "tcp"
    cidr_blocks = ["${var.default_cidr}"]
  }

  ingress {
    from_port   = 2719
    to_port     = 2719
    protocol    = "tcp"
    cidr_blocks = ["${var.default_cidr}"]
  }

  ingress {
    from_port   = 135
    to_port     = 139
    protocol    = "tcp"
    cidr_blocks = ["${var.deloitte_cidr}"]
  }

  ingress {
    from_port   = 135
    to_port     = 139
    protocol    = "udp"
    cidr_blocks = ["${var.deloitte_cidr}"]
  }

  ingress {
    from_port = 8888
    to_port = 8888
    protocol = "tcp"
    cidr_blocks = ["10.0.0.0/8"]
    description = "System Center"
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    Name = "COMMONWELL-EXSTREAM-SG"
  }
}

resource "aws_security_group" "SG_EXTERNAL_HAPROXY" {
  name = "COMMONWELL-EXTERNAL-HAPROXY-SG"

  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = [
      "${var.deloitte_cidr}",
      "${var.hub_cidr}",
      "${var.commonwell_aws_cidr}",
      "${var.commonwell_lindsay_cidr}",
      "${var.commonwell_whitby_cidr}"
    ]
    description = "HTTPS"
  }

  ingress {
    from_port   = 8444
    to_port     = 8444
    protocol    = "tcp"
    cidr_blocks = [
      "${var.deloitte_cidr}"
    ]
    description = "Health"
  }

  ingress {
    from_port   = 8404
    to_port     = 8404
    protocol    = "tcp"
    cidr_blocks = [
      "${var.deloitte_cidr}",
      "${var.hub_cidr}"
    ]
    description = "Stats"
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    Name = "COMMONWELL-EXTERNAL-HAPROXY-SG"
  }
}

resource "aws_security_group" "SG_INTERNAL_HAPROXY" {
  name = "COMMONWELL-INTERNAL-HAPROXY-SG"

  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = [
      "${var.deloitte_cidr}",
      "${var.hub_cidr}",
      "${var.commonwell_aws_cidr}",
      "${var.commonwell_lindsay_cidr}",
      "${var.commonwell_whitby_cidr}",
      "${var.ichub_cidr}"
    ]
    description = "HTTPS"
  }

  ingress {
    from_port   = 8444
    to_port     = 8444
    protocol    = "tcp"
    cidr_blocks = [
      "${var.deloitte_cidr}"
    ]
    description = "Health"
  }

  ingress {
    from_port   = 8404
    to_port     = 8404
    protocol    = "tcp"
    cidr_blocks = [
      "${var.deloitte_cidr}",
      "${var.hub_cidr}"
    ]
    description = "Stats"
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    Name = "COMMONWELL-INTERNAL-HAPROXY-SG"
  }
}

resource "aws_security_group" "SG_HAPROXY_INTERNAL_PEERING" {
  name = "COMMONWELL-HAPROXY-INTERNAL-PEERING-SG"

  ingress {
    from_port       = 1024
    to_port         = 1024
    protocol        = "tcp"
    security_groups = ["${aws_security_group.SG_INTERNAL_HAPROXY.id}"]
    description     = "HAProxy Peering"
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    Name = "COMMONWELL-HAPROXY-INTERNAL-PEERING-SG"
  }
}

resource "aws_security_group" "SG_HAPROXY_EXTERNAL_PEERING" {
  name = "COMMONWELL-HAPROXY-EXTERNAL-PEERING-SG"

  ingress {
    from_port       = 1024
    to_port         = 1024
    protocol        = "tcp"
    security_groups = ["${aws_security_group.SG_EXTERNAL_HAPROXY.id}"]
    description     = "HAProxy Peering"
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    Name = "COMMONWELL-HAPROXY-EXTERNAL-PEERING-SG"
  }
}

resource "aws_security_group" "SG_GUIDEWIRE" {
  name = "COMMONWELL-GUIDEWIRE-SG"

  ingress {
    from_port   = 8080
    to_port     = 8083
    protocol    = "tcp"
    # security_groups = [
    #   "${aws_security_group.SG_INTERNAL_HAPROXY.id}",
    #   "${aws_security_group.SG_EXTERNAL_HAPROXY.id}"
    # ]
    cidr_blocks = [
      "${var.deloitte_cidr}",
      "${var.hub_cidr}",
      "${var.commonwell_aws_cidr}",
      "${var.commonwell_lindsay_cidr}",
      "${var.commonwell_whitby_cidr}"
    ] # For now. Switch to HAProxy instances only after confirming GW is working fine in CMS
    description = "HTTP"
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    Name = "COMMONWELL-GUIDEWIRE-SG"
  }
}

resource "aws_security_group" "SG_GUIDEWIRE_CLUSTER_NBIO_EVENT_QUEUE" {
  name = "COMMONWELL-GUIDEWIRE-CLUSTER-NBIO-EVENT-QUEUE-SG"

  ingress {
    from_port   = 1024
    to_port     = 65535
    protocol    = "tcp"
    security_groups = ["${aws_security_group.SG_GUIDEWIRE.id}"]
    description = "Cluster NBIO event queue"
  }

  ingress {
    from_port   = 7946
    to_port     = 7946
    protocol    = "tcp"
    security_groups = ["${aws_security_group.SG_GUIDEWIRE.id}"]
    description = "TCP"
  }

  ingress {
    from_port   = 2377
    to_port     = 2377
    protocol    = "tcp"
    security_groups = ["${aws_security_group.SG_GUIDEWIRE.id}"]
    description = "Cluster Management Communications"
  }

  ingress {
    from_port   = 6783
    to_port     = 6783
    protocol    = "udp"
    security_groups = ["${aws_security_group.SG_GUIDEWIRE.id}"]
    description = "UDP"
  }

  ingress {
    from_port   = 6784
    to_port     = 6784
    protocol    = "udp"
    security_groups = ["${aws_security_group.SG_GUIDEWIRE.id}"]
    description = "UDP"
  }

  ingress {
    from_port   = 7946
    to_port     = 7946
    protocol    = "udp"
    security_groups = ["${aws_security_group.SG_GUIDEWIRE.id}"]
    description = "UDP"
  }

  ingress {
    from_port   = 4789
    to_port     = 4789
    protocol    = "udp"
    security_groups = ["${aws_security_group.SG_GUIDEWIRE.id}"]
    description = "overlay network traffic"
  }


  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    Name = "COMMONWELL-GUIDEWIRE-CLUSTER-NBIO-EVENT-QUEUE-SG"
  }
}

resource "aws_security_group" "SG_JENKINS_GUIDEWIRE_HEALTH_CHECK" {
  name = "COMMONWELL-JENKINS-GUIDEWIRE-HEALTH-CHECK-SG"

  ingress {
    from_port   = 8080
    to_port     = 8083
    protocol    = "tcp"
    cidr_blocks = ["10.10.4.75/32"]
    description = "Dev Jenkins Client Health Check"
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    Name = "COMMONWELL-JENKINS-GUIDEWIRE-HEALTH-CHECK-SG"
  }
}

resource "aws_security_group" "SG_MSSQL_DB" {
  name = "COMMONWELL-MSSQL-DB-SG"

  ingress {
    from_port   = 1433
    to_port     = 1433
    protocol    = "tcp"
    cidr_blocks = [
      "${var.deloitte_cidr}",
      "${var.hub_cidr}",
      "${var.commonwell_aws_cidr}",
      "${var.commonwell_lindsay_cidr}",
      "${var.commonwell_whitby_cidr}"
    ]
  }

  ingress {
    from_port   = 1433
    to_port     = 1433
    protocol    = "tcp"
    cidr_blocks = [
      "10.10.64.0/20",
      "10.10.4.0/22",
      "172.31.0.0/16",
      "10.19.0.0/16",
      "10.20.0.0/16",
      "10.10.0.0/16"
    ]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    Name = "COMMONWELL-MSSQL-DB-SG"
  }
}

resource "aws_security_group" "SG_OT_MSSQL_DB" {
  name = "COMMONWELL-OT-MSSQL-DB-SG"

  ingress {
    from_port   = 1433
    to_port     = 1433
    protocol    = "tcp"
    cidr_blocks = [
      "${var.deloitte_cidr}",
      "${var.hub_cidr}",
      "${var.commonwell_aws_cidr}",
      "${var.commonwell_lindsay_cidr}",
      "${var.commonwell_whitby_cidr}"
    ]
  }

  ingress {
    from_port   = 1434
    to_port     = 1434
    protocol    = "udp"
    cidr_blocks = [
      "${var.deloitte_cidr}",
      "${var.hub_cidr}",
      "${var.commonwell_aws_cidr}",
      "${var.commonwell_lindsay_cidr}",
      "${var.commonwell_whitby_cidr}"
    ]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    Name = "COMMONWELL-OT-MSSQL-DB-SG"
  }
}

resource "aws_security_group" "SG_POSTGRES_DB" {
  name = "COMMONWELL-POSTGRES-DB-SG"

  ingress {
    from_port   = 5432
    to_port     = 5432
    protocol    = "tcp"
    cidr_blocks = [
      "${var.deloitte_cidr}",
      "${var.hub_cidr}",
      "${var.commonwell_aws_cidr}",
      "${var.commonwell_lindsay_cidr}",
      "${var.commonwell_whitby_cidr}"
    ]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    Name = "COMMONWELL-POSTGRES-DB-SG"
  }
}

resource "aws_security_group" "SG_MYSQL_DB" {
  name = "COMMONWELL-MYSQL-DB-SG"

  ingress {
    from_port   = 3306
    to_port     = 3306
    protocol    = "tcp"
    cidr_blocks = [
      "${var.deloitte_cidr}",
      "${var.hub_cidr}",
      "${var.commonwell_aws_cidr}",
      "${var.commonwell_lindsay_cidr}",
      "${var.commonwell_whitby_cidr}"
    ]
  }

  ingress {
    from_port   = 3301
    to_port     = 3301
    protocol    = "tcp"
    cidr_blocks = [
      "${var.deloitte_cidr}",
      "${var.hub_cidr}",
      "${var.commonwell_aws_cidr}",
      "${var.commonwell_lindsay_cidr}",
      "${var.commonwell_whitby_cidr}"
    ]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    Name = "COMMONWELL-MYSQL-DB-SG"
  }
}

resource "aws_security_group" "SG_PING_TEST" {
  name = "COMMONWELL-PING-TEST-SG"

  ingress {
    from_port       = -1
    to_port         = -1
    protocol        = "icmp"
    cidr_blocks = [
      "${var.deloitte_cidr}",
      "${var.hub_cidr}",
      "${var.commonwell_aws_cidr}",
      "${var.commonwell_lindsay_cidr}",
      "${var.commonwell_whitby_cidr}"
    ]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    Name = "COMMONWELL-PING-TEST-SG"
  }
}

resource "aws_security_group" "SG_DATAHUB_INFOCENTER" {
  name = "COMMONWELL-DATAHUB-INFOCENTER-SG"

  ingress {
    from_port       = 4444
    to_port         = 4444
    protocol        = "tcp"
    cidr_blocks = [
      "${var.deloitte_cidr}",
      "${var.hub_cidr}",
      "${var.commonwell_aws_cidr}",
      "${var.commonwell_lindsay_cidr}",
      "${var.commonwell_whitby_cidr}"
    ]
  }

  ingress {
    from_port       = 4444
    to_port         = 4444
    protocol        = "udp"
    cidr_blocks = [
      "${var.deloitte_cidr}",
      "${var.hub_cidr}",
      "${var.commonwell_aws_cidr}",
      "${var.commonwell_lindsay_cidr}",
      "${var.commonwell_whitby_cidr}"
    ]
  }

  ingress {
    from_port       = 4446
    to_port         = 4446
    protocol        = "tcp"
    cidr_blocks = [
      "${var.deloitte_cidr}",
      "${var.hub_cidr}",
      "${var.commonwell_aws_cidr}",
      "${var.commonwell_lindsay_cidr}",
      "${var.commonwell_whitby_cidr}"
    ]
  }

  ingress {
    from_port       = 4446
    to_port         = 4446
    protocol        = "udp"
    cidr_blocks = [
      "${var.deloitte_cidr}",
      "${var.hub_cidr}",
      "${var.commonwell_aws_cidr}",
      "${var.commonwell_lindsay_cidr}",
      "${var.commonwell_whitby_cidr}"
    ]
  }

  ingress {
    from_port       = 8080
    to_port         = 8080
    protocol        = "tcp"
    cidr_blocks = [
      "${var.deloitte_cidr}",
      "${var.hub_cidr}",
      "${var.commonwell_aws_cidr}",
      "${var.commonwell_lindsay_cidr}",
      "${var.commonwell_whitby_cidr}"
    ]
  }

  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    "Name" = "COMMONWELL-DATAHUB-INFOCENTER-SG"
  }
}

resource "aws_security_group" "SG_HUBIO" {
  name = "COMMONWELL-HUBIO-SG"

  ingress {
    from_port   = 8080
    to_port     = 8080
    protocol    = "tcp"
    cidr_blocks = [
      "${var.deloitte_cidr}",
      "${var.hub_cidr}",
      "${var.commonwell_aws_cidr}",
      "${var.commonwell_lindsay_cidr}",
      "${var.commonwell_whitby_cidr}"
    ]
  }

  ingress {
    from_port   = 9000
    to_port     = 9010
    protocol    = "tcp"
    cidr_blocks = [
      "${var.deloitte_cidr}",
      "${var.hub_cidr}",
      "${var.commonwell_aws_cidr}",
      "${var.commonwell_lindsay_cidr}",
      "${var.commonwell_whitby_cidr}"
    ]
  }

  ingress {
    from_port   = 4200
    to_port     = 4200
    protocol    = "tcp"
    cidr_blocks = "${var.default_cidr}"
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    Name = "COMMONWELL-HUBIO-SG"
  }
}

resource "aws_security_group" "SG_QLIKSENSE" {
  name = "COMMONWELL-QLIKSENSE-SG"

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = [
      "${var.deloitte_cidr}",
      "${var.hub_cidr}",
      "${var.commonwell_aws_cidr}",
      "${var.commonwell_lindsay_cidr}",
      "${var.commonwell_whitby_cidr}"
    ]
  }

  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = [
      "${var.deloitte_cidr}",
      "${var.hub_cidr}",
      "${var.commonwell_aws_cidr}",
      "${var.commonwell_lindsay_cidr}",
      "${var.commonwell_whitby_cidr}"
    ]
  }

  ingress {
    from_port   = 4244
    to_port     = 4244
    protocol    = "tcp"
    cidr_blocks = [
      "${var.deloitte_cidr}",
      "${var.hub_cidr}",
      "${var.commonwell_aws_cidr}",
      "${var.commonwell_lindsay_cidr}",
      "${var.commonwell_whitby_cidr}"
    ]
  }

  ingress {
    from_port   = 4248
    to_port     = 4248
    protocol    = "tcp"
    cidr_blocks = [
      "${var.deloitte_cidr}",
      "${var.hub_cidr}",
      "${var.commonwell_aws_cidr}",
      "${var.commonwell_lindsay_cidr}",
      "${var.commonwell_whitby_cidr}"
    ]
  }

  ingress {
    from_port   = 4747
    to_port     = 4747
    protocol    = "tcp"
    cidr_blocks = [
      "${var.deloitte_cidr}"
    ]
  }

  ingress {
    from_port   = 4997
    to_port     = 4997
    protocol    = "tcp"
    cidr_blocks = [
      "${var.deloitte_cidr}"
    ]
  }

  ingress {
    from_port   = 4242
    to_port     = 4243
    protocol    = "tcp"
    cidr_blocks = [
      "${var.deloitte_cidr}"
    ]
  }

  ingress {
    from_port   = 4343
    to_port     = 4343
    protocol    = "tcp"
    cidr_blocks = [
      "${var.deloitte_cidr}"
    ]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    Name = "COMMONWELL-QLIKSENSE-SG"
  }
}

resource "aws_security_group" "SG_COMMONWELL_PROMETHEUS_GRAFANA" {
  name = "COMMONWELL-PROMETHEUS-GRAFANA-SG"

  ingress {
    from_port   = 10000
    to_port     = 10001
    protocol    = "tcp"
    cidr_blocks = [
      "${var.commonwell_aviatrix_vpn_1a}",
      "${var.commonwell_aviatrix_vpn_1b}",
      "${var.deloitte_cidr}"
    ]
    description = "Prometheus and Alert Manager"
  }

  ingress {
    from_port   = 10000
    to_port     = 10001
    protocol    = "tcp"
    cidr_blocks = [
        "10.10.5.104/32", 
        "10.10.7.78/32", 
        "10.10.64.0/20"
    ]
    description = "Prometheus and Alert Manager"
  }

  ingress {
    from_port   = 3000
    to_port     = 3000
    protocol    = "tcp"
    cidr_blocks = [
      "${var.commonwell_aviatrix_vpn_1a}",
      "${var.commonwell_aviatrix_vpn_1b}",
      "${var.deloitte_cidr}"
    ]
    description = "Grafana"
  }

  ingress {
    from_port   = 19191
    to_port     = 19191
    protocol    = "tcp"
    cidr_blocks = [
      "10.0.0.0/8",
    ]
    description = "Thanos UI"
  }

  ingress {
    from_port   = 3000
    to_port     = 3000
    protocol    = "tcp"
    cidr_blocks = [
      "10.10.7.78/32", 
      "10.10.64.0/20", 
      "10.10.5.104/32"
    ]
    description = "Grafana"
  }

  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    Name = "COMMONWELL-PROMETHEUS-GRAFANA-SG"
  }
}

resource "aws_security_group" "SG_AVIATRIX_RDP" {
  name = "COMMONWELL-AVIATRIX-RDP-SG"

  ingress {
    from_port   = 3389
    to_port     = 3389
    protocol    = "tcp"
    cidr_blocks = [
      "${var.commonwell_aviatrix_vpn_1a}",
      "${var.commonwell_aviatrix_vpn_1b}"
    ]
    description = "RDP"
  }

  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    Name = "COMMONWELL-AVIATRIX-RDP-SG"
  }
}

resource "aws_security_group" "SG_FORTICLIENT_RDP" {
  name = "COMMONWELL-FORTICLIENT-RDP-SG"

  ingress {
    from_port   = 3389
    to_port     = 3389
    protocol    = "tcp"
    cidr_blocks = [
      "${var.commonwell_aws_cidr}"
    ]
    description = "RDP"
  }

  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    Name = "COMMONWELL-FORTICLIENT-RDP-SG"
  }
}

resource "aws_security_group" "SG_AVIATRIX_SSH" {
  name = "COMMONWELL-AVIATRIX-SSH-SG"

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = [
      "${var.commonwell_aviatrix_vpn_1a}",
      "${var.commonwell_aviatrix_vpn_1b}"
    ]
    description = "SSH"
  }

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = [
      "10.10.5.104/32", 
      "10.10.7.78/32"
    ]
    description = "SSH"
  }

  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    Name = "COMMONWELL-AVIATRIX-SSH-SG"
  }
}

resource "aws_security_group" "SG_COMMONWELL_JOBSCHEDULER_SSH" {
  name = "COMMONWELL-JOBSCHEDULER-SSH-SG"
 
 ingress {
    from_port       = 22
    to_port         = 22
    protocol        = "tcp"
    security_groups = ["${aws_security_group.SG_JOB_SCHEDULER.id}"]
  }

  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    Name = "COMMONWELL-JOBSCHEDULER-SSH-SG"
  }
}

resource "aws_security_group" "SG_COMMONWELL_JENKINS_CLIENT_SSH" {
  name = "COMMONWELL-JENKINS-CLIENT-SSH-SG"
 
  ingress {
    from_port       = 22
    to_port         = 22
    protocol        = "tcp"
    cidr_blocks = ["10.10.4.75/32"]
    description = "Dev Jenkins Client SSH"
  }

  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    Name = "COMMONWELL-JENKINS-CLIENT-SSH-SG"
  }
}


#######################DEV/QA############################

resource "aws_security_group" "SG_EXTERNAL_HAPROXY_DEV" {
  name = "COMMONWELL-EXTERNAL-HAPROXY-DEV-SG"

  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = [
      "${var.deloitte_cidr}",
      "${var.hub_cidr}",
      "${var.commonwell_aws_cidr}",
      "${var.commonwell_lindsay_cidr}",
      "${var.commonwell_whitby_cidr}"
    ]
    description = "HTTPS"
  }

  ingress {
    from_port   = 8444
    to_port     = 8444
    protocol    = "tcp"
    cidr_blocks = [
      "${var.deloitte_cidr}"
    ]
    description = "Health"
  }

  ingress {
    from_port   = 8404
    to_port     = 8404
    protocol    = "tcp"
    cidr_blocks = [
      "${var.deloitte_cidr}",
      "${var.hub_cidr}"
    ]
    description = "Stats"
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    Name = "COMMONWELL-EXTERNAL-HAPROXY-DEV-SG"
  }
}

resource "aws_security_group" "SG_INTERNAL_HAPROXY_DEV" {
  name = "COMMONWELL-INTERNAL-HAPROXY-DEV-SG"

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = [
      "${var.deloitte_cidr}",
      "${var.hub_cidr}",
      "${var.commonwell_aws_cidr}",
      "${var.commonwell_lindsay_cidr}",
      "${var.commonwell_whitby_cidr}",
      "${var.ichub_cidr}"
    ]
    description = "HTTP"
  }

  ingress {
    from_port   = 8444
    to_port     = 8444
    protocol    = "tcp"
    cidr_blocks = [
      "${var.deloitte_cidr}"
    ]
    description = "Health"
  }

  ingress {
    from_port   = 8404
    to_port     = 8404
    protocol    = "tcp"
    cidr_blocks = [
      "${var.deloitte_cidr}",
      "${var.hub_cidr}"
    ]
    description = "Stats"
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    Name = "COMMONWELL-INTERNAL-HAPROXY-DEV-SG"
  }
}



resource "aws_security_group" "COMMONWELL-GW-DEV" {
  name = "COMMONWELL-GW-DEV-SG"

  ingress {
    from_port   = 8080
    to_port     = 8083
    protocol    = "tcp"
    security_groups = [
      "${aws_security_group.SG_INTERNAL_HAPROXY_DEV.id}",
      "${aws_security_group.SG_EXTERNAL_HAPROXY_DEV.id}"
    ]
    description = "HTTP"
  }

  ingress {
    from_port   = 8080
    to_port     = 8083
    protocol    = "tcp"
    cidr_blocks = [
      "10.10.4.0/22",
      "10.20.0.0/16",
      "10.10.64.0/20",
      "10.19.0.0/16",
      "10.10.5.104/32",
      "10.10.7.78/32"
    ]
    description = "HTTP"
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    Name = "COMMONWELL-GW-DEV-SG", 
    PRIMARYCONTACT = "rsaksena@cmsmastercloud.ca",
    CSTYPE = "External",
    CSCLASS = "Confidential",
    CLIENT = "CMS",
    SECONDARYCONTACT = "robhatnagar@deloitte.ca",
    COUNTRY =	"CA",
    GROUPCONTACT = "robhatnagar@deloitte.ca",
    MEMBERFIRM = "CA",
    BILLINGCODE = "DEL02890-01-01-01-NB10",
    CSQUAL = "No CI/PI data"
  }
}

resource "aws_security_group" "SG_COMMONWELL_MICROSERVICES_DEV" {
  name = "COMMONWELL-MICROSERVICES-DEV-SG"

  ingress {
    from_port   = 8443
    to_port     = 8443
    protocol    = "tcp"
    security_groups = [
      "${aws_security_group.SG_EXTERNAL_HAPROXY_DEV.id}"
    ]
    description = "Orchestrator"
  } 

  ingress {
    from_port   = 8443
    to_port     = 8443
    protocol    = "tcp"
    cidr_blocks = [
      "10.0.0.0/8"
    ]
    description = "Orchestrator"
  }

  ingress {
    from_port   = 15672
    to_port     = 15672
    protocol    = "tcp"
    cidr_blocks = [
      "${var.hub_cidr}"
    ]
    security_groups = [
      "${aws_security_group.SG_INTERNAL_HAPROXY_DEV.id}"
    ]
    description = "RabbitMQ Management UI"
  }

  ingress {
    from_port   = 15672
    to_port     = 15672
    protocol    = "tcp"
    cidr_blocks = [
      "10.10.4.0/22"
    ]
    security_groups = [
      "${aws_security_group.SG_INTERNAL_HAPROXY_DEV.id}"
    ]
    description = "RabbitMQ Management UI"
  }

  ingress {
    from_port   = 9002
    to_port     = 9002
    protocol    = "tcp"
    security_groups = [
      "${aws_security_group.SG_INTERNAL_HAPROXY_DEV.id}"
    ]
    description = "Edge"
  }

  ingress {
    from_port   = 9222
    to_port     = 9222
    protocol    = "tcp"
    security_groups = [
      "${aws_security_group.SG_EXTERNAL_HAPROXY_DEV.id}",
      "${aws_security_group.SG_INTERNAL_HAPROXY_DEV.id}"
    ]
    description = "Audatex"
  }

  ingress {
    from_port   = 9222
    to_port     = 9222
    protocol    = "tcp"
    cidr_blocks = [
      "10.0.0.0/8"
    ]
    description = "Audatex"
  }

  ingress {
    from_port   = 4789
    to_port     = 4789
    protocol    = "udp"
    cidr_blocks = [
      "${var.deloitte_cidr}"
    ]
    description = "Overlay Network Traffic"
  }

  ingress {
    from_port   = 4789
    to_port     = 4789
    protocol    = "udp"
    cidr_blocks = [
      "10.10.4.0/22"
    ]
    description = "Overlay Network Traffic"
  }

  ingress {
    from_port   = 8500
    to_port     = 8500
    protocol    = "tcp"
    cidr_blocks = [
      "${var.hub_cidr}"
    ]
    description = "Consul UI"
  }

  ingress {
    from_port   = 8500
    to_port     = 8500
    protocol    = "tcp"
    cidr_blocks = [
      "10.10.4.0/22"
    ]
    description = "Consul UI"
  }


  ingress {
    from_port   = 9002
    to_port     = 9002
    protocol    = "tcp"
    cidr_blocks = [
      "10.0.0.0/8"
    ]
    description = "Edge"
  }

  ingress {
    from_port   = 9002
    to_port     = 9002
    protocol    = "tcp"
    cidr_blocks = [
      "10.10.64.0/20",
      "10.10.4.0/22"
    ]
    description = "Edge"
  }



  ingress {
    from_port       = 443
    to_port         = 443
    protocol        = "tcp"
    cidr_blocks = [
      "${var.deloitte_cidr}",
      "${var.hub_cidr}"
    ]
  }

  ingress {
    from_port       = 443
    to_port         = 443
    protocol        = "tcp"
    cidr_blocks = [
      "10.0.0.0/8"
    ]
  }

  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    Name = "COMMONWELL-MICROSERVICES-DEV-SG"
  }
}

resource "aws_security_group" "SG_IC_HUB_DNS_RESOLVER_ENDPOINT" {
  name = "COMMONWELL-IC-HUB-DNS-RESOLVER-ENDPOINT-SG"

  ingress {
    from_port   = 53
    to_port     = 53
    protocol    = "tcp"
    cidr_blocks = [
      "${var.vpc_cidr}"
    ]
    description = "DNS"
  }

  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    Name = "COMMONWELL-IC-HUB-DNS-RESOLVER-ENDPOINT-SG"
  }
}