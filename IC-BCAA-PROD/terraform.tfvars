#Envrionment specific variables
client_name = "IC-bcaa"
enviornment_name = "PROD"
enviornment_type = "P"
client_number = "A003"
#ad-dc1 = "10.10.4.10"
#ad-dc2 = "10.10.6.10"
volume_key = "arn:aws:kms:ca-central-1:413329031558:key/5cdd3e56-2101-47df-bfe7-c4d1732f684f"
ami_id_windows2012sql14enterprise_2020_12_11 	= "ami-0a98a578d8c99a362"
ami_id_windows2019container_2020_12_11  		= "ami-02f7108e0d1e573d9"
ami_id_windows2019base_2020_12_11       		= "ami-07df0407a8c4fe342"
ami_id_windows2016base_2020_12_11 				= "ami-02d51b2c3cf987496"
ami_id_windows2012R2base_2020_12_11 			= "ami-0b1d8cd85cb4b0db6"
#ami_id_redhatlinux75_2020_12_11             	= ""
ami_id_ubuntu1804baseimage_2020_12_11        	= "ami-0d3ec07a0e2338d75"
ami_id_ubuntu1604baseimage_2020_12_11        	= "ami-0e236826c343be5e5"
ami_id_amazonlinux201712base_2020_12_11     	= "ami-06c9db22258a28dd8"
ami_id_amazonlinux201709base_2020_12_11     	= "ami-04c1a0b420a81f05a"
ami_id_amazonlinux2base_2020_12_11           	= "ami-02496f7e580ca22e3"
ami_id_amazonlinux201803base_2020_12_11      	= "ami-04f3673aac82a6c59"
ami_id_windows2016base_server_dhicdev01			= "ami-02c6af2c8292710be"
ami_id_windows2016base_database_dhicdev01		= "ami-008e55415d7aab0cd"

#VPC Specific variables
vpc_id = "vpc-087fe19b5b60819a4"
vpc_cidr = "10.12.0.0/22"


bcaa-prod-database-1a-subnet = "subnet-04c99d9796ba614c8"
bcaa-prod-database-1b-subnet = "subnet-0408645dead4aee3b"
bcaa-prod-private-1a-subnet = "subnet-0993e8854d704beaf"
bcaa-prod-private-1b-subnet = "subnet-0102f99fde6adeda1"


#RDS
mssql-rds-password = "Toronto12345"
mysql-rds-password = "Toronto12345"
postgres-rds-password = "Toronto12345"

#Accounts
bcaa-dev-account = "005810662120"
bcaa-prod-account = "413329031558"
bcaa-hub-account = "186239701968"
