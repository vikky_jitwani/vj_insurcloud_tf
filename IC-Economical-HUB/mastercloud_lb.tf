##### NLBs ####################
########## External Load Balancers: North of firewall ###########
#=======================================================================
# PROD
#=======================================================================
resource "aws_lb" "ICECO_DEV_DASHBOARD_NLB" {
  name = "ECO-DEV-DASHBOARD-NLB"
  internal = false
  load_balancer_type = "network"
  enable_cross_zone_load_balancing = false
  subnets = [
    "${var.economical-hub-public-1a-subnet}",
    "${var.economical-hub-public-1b-subnet}"
  ]

  enable_deletion_protection = false

  tags = {
    Contract = "MANAGED_SERVICES"
    Name = "ECO-DEV-DASHBOARD-NLB"
  }
}

resource "aws_lb_target_group" "ICECO_DEV_DASHBOARD_NLB_TARGET_GROUP" {
  name = "ECO-DEV-DASHBOARD-NLB-TG"
  port = 443
  protocol = "TCP"
  target_type = "ip"
  vpc_id = "${var.vpc_id}"

  health_check {
    protocol = "TCP"
    port = 8444
  }

  tags = {
    Contract = "MANAGED_SERVICES"
    Name = "ECO-DEV-DASHBOARD-NLB-TARGET-GROUP"
  }
}

resource "aws_lb_listener" "ICECO_DEV_DASHBOARD_NLB_LISTENER" {
  load_balancer_arn = "${aws_lb.ICECO_DEV_DASHBOARD_NLB.arn}"
  port = "443"
  protocol = "TCP"

  default_action {
    type = "forward"
    target_group_arn = "${aws_lb_target_group.ICECO_DEV_DASHBOARD_NLB_TARGET_GROUP.arn}"
  }
}

########## Internal Load Balancers ###########
resource "aws_lb" "ICECO_INTERNAL_ALB_ICHUB" {
  name = "ICECO-INTALB-ICHUB"
  internal = true
  load_balancer_type = "application"
  security_groups = [
    "${aws_security_group.SG_HTTPS.id}"]
  subnets = [
    "${var.economical-hub-private-1a-subnet}",
    "${var.economical-hub-private-1b-subnet}"
  ]

  tags = {
    Contract = "MANAGED_SERVICES"
    Name = "ICECO-INTALB-ICHUB"
  }
}

resource "aws_lb_target_group" "ICECO_INTERNAL_ALB_ICHUB_DEV_VAULT_TG" {
  name = "ICECO-INTALB-ICHUB-DEV-VAULT-TG"
  port = 443
  protocol = "HTTPS"
  target_type = "ip"
  vpc_id = "${var.vpc_id}"

  health_check {
    enabled = true
    protocol = "HTTPS"
    path = "/v1/sys/health?standbyok=true"
    port = "traffic-port"
    matcher = "200"
  }

  tags = {
    Contract = "MANAGED_SERVICES"
    Name = "ICECO-INTALB-ICHUB-DEV-VAULT-TG"
  }
}
resource "aws_lb_target_group_attachment" "ICECO_INTERNAL_ALB_ICHUB_DEV_VAULT_TG_ATT1" {
  target_group_arn = "${aws_lb_target_group.ICECO_INTERNAL_ALB_ICHUB_DEV_VAULT_TG.arn}"
  target_id = "10.15.16.121"
  port = 443
  availability_zone = "all"
}
resource "aws_lb_target_group_attachment" "ICECO_INTERNAL_ALB_ICHUB_DEV_VAULT_TG_ATT2" {
  target_group_arn = "${aws_lb_target_group.ICECO_INTERNAL_ALB_ICHUB_DEV_VAULT_TG.arn}"
  target_id = "10.15.17.238"
  port = 443
  availability_zone = "all"
}

resource "aws_lb_target_group" "ICECO_INTERNAL_ALB_ICHUB_PROD_VAULT_TG" {
  name = "ICECO-INTALB-ICHUB-PROD-VAULT-TG"
  port = 443
  protocol = "HTTPS"
  target_type = "ip"
  vpc_id = "${var.vpc_id}"

  health_check {
    enabled = true
    protocol = "HTTPS"
    path = "/v1/sys/health?standbyok=true"
    port = "traffic-port"
    matcher = "200"
  }

  tags = {
    Contract = "MANAGED_SERVICES"
    Name = "ICECO-INTALB-ICHUB-PROD-VAULT-TG"
  }
}
resource "aws_lb_target_group_attachment" "ICECO_INTERNAL_ALB_ICHUB_PROD_VAULT_TG_ATT1" {
  target_group_arn = "${aws_lb_target_group.ICECO_INTERNAL_ALB_ICHUB_PROD_VAULT_TG.arn}"
  target_id = "10.15.16.170"
  port = 443
  availability_zone = "all"
}
resource "aws_lb_target_group_attachment" "ICECO_INTERNAL_ALB_ICHUB_PROD_VAULT_TG_ATT2" {
  target_group_arn = "${aws_lb_target_group.ICECO_INTERNAL_ALB_ICHUB_PROD_VAULT_TG.arn}"
  target_id = "10.15.17.12"
  port = 443
  availability_zone = "all"
}

resource "aws_lb_target_group" "ICECO_INTERNAL_ALB_ICHUB_JENKINS_TG" {
  name = "ICECO-INTALB-ICHUB-JENKINS-TG"
  port = 8080
  protocol = "HTTP"
  target_type = "ip"
  vpc_id = "${var.vpc_id}"

  health_check {
    enabled = true
    protocol = "HTTP"
    path = "/login"
    port = "traffic-port"
    matcher = "200"
  }

  tags = {
    Contract = "MANAGED_SERVICES"
    Name = "ICECO-INTALB-ICHUB-JENKINS-TG"
  }
}
resource "aws_lb_target_group_attachment" "ICECO_INTERNAL_ALB_ICHUB_JENKINS_TG_ATT1" {
  target_group_arn = "${aws_lb_target_group.ICECO_INTERNAL_ALB_ICHUB_JENKINS_TG.arn}"
  target_id = "10.15.16.222"
  port = 8080
  availability_zone = "all"
}

resource "aws_lb_listener" "ICECO_INTERNAL_ALB_ICHUB_LIST" {
  load_balancer_arn = "${aws_lb.ICECO_INTERNAL_ALB_ICHUB.arn}"
  port = "443"
  protocol = "HTTPS"
  ssl_policy = "ELBSecurityPolicy-TLS-1-2-Ext-2018-06"
  certificate_arn = "arn:aws:acm:ca-central-1:348868695504:certificate/79653118-75bb-495b-a1eb-dcc871736a7f"

  default_action {
    type = "fixed-response"

    fixed_response {
      content_type = "text/plain"
      message_body = "hello there"
      status_code = "418"
    }
  }
}

resource "aws_lb_listener_rule" "ICECO_INTERNAL_ALB_ICHUB_DEV_VAULT_LR" {
  listener_arn = "${aws_lb_listener.ICECO_INTERNAL_ALB_ICHUB_LIST.arn}"

  action {
    type = "forward"
    target_group_arn = "${aws_lb_target_group.ICECO_INTERNAL_ALB_ICHUB_DEV_VAULT_TG.arn}"
  }

  condition {
    host_header {
      values = [
        "${aws_route53_record.ICECONOMICAL_VAULT_DEV_RECORD.name}"]
    }
  }
}

resource "aws_lb_listener_rule" "ICECO_INTERNAL_ALB_ICHUB_PROD_VAULT_LR" {
  listener_arn = "${aws_lb_listener.ICECO_INTERNAL_ALB_ICHUB_LIST.arn}"

  action {
    type = "forward"
    target_group_arn = "${aws_lb_target_group.ICECO_INTERNAL_ALB_ICHUB_PROD_VAULT_TG.arn}"
  }

  condition {
    host_header {
      values = [
        "${aws_route53_record.ICECONOMICAL_VAULT_PROD_RECORD.name}",
        "${aws_route53_record.ICECONOMICAL_VAULT_PROD_RECORD2.name}"
      ]
    }
  }
}

resource "aws_lb_listener_rule" "ICECO_INTERNAL_ALB_ICHUB_JENKINS_LR" {
  listener_arn = "${aws_lb_listener.ICECO_INTERNAL_ALB_ICHUB_LIST.arn}"

  action {
    type = "forward"
    target_group_arn = "${aws_lb_target_group.ICECO_INTERNAL_ALB_ICHUB_JENKINS_TG.arn}"
  }

  condition {
    host_header {
      values = [
        "${aws_route53_record.ICECONOMICAL_JENKINS_RECORD.name}"]
    }
  }
}
