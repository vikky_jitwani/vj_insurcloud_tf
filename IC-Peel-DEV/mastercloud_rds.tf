#### JOB SCHEDULER DB #####
resource "aws_db_subnet_group" "MYSQL_DB_SUBNET" {
    name        = "mysql-db-subnet"
    description = "RDS subnet group"
    subnet_ids  = ["${var.peel-dev-database-1a-subnet}", "${var.peel-dev-database-1b-subnet}"]
}

# Microservices Database
resource "aws_db_parameter_group" "microservices" {
  name   = "peelmicroservices"
  family = "mysql8.0"

  parameter {
    name  = "time_zone"
    value = "US/Eastern" # MySQL has no America/Toronto time zone
  }

  parameter {
    name = "log_output"
    value = "FILE"
  }

  parameter {
    name = "general_log"
    value = "1"
  }

  parameter {
    name = "slow_query_log"
    value = "1"
  }

  parameter {
    name = "long_query_time"
    value = "2"
  }
}

resource "aws_db_instance" "microservicesmysqldb" {

    engine            = "mysql"
    engine_version    = "8.0.17"
    instance_class    = "db.t3.medium"
    allocated_storage = 100
    storage_encrypted = true
    identifier        = "peelppsmicroservices"
    name              = "peelppsmicroservices"
    username          = "sa"
    password          = "${var.mysql-rds-password}" # password
    port              = 3306

    maintenance_window = "Mon:00:00-Mon:03:00"
    backup_window      = "03:00-06:01"
    deletion_protection = true
    enabled_cloudwatch_logs_exports = ["error", "general", "slowquery"]
    apply_immediately  = true

    db_subnet_group_name  = "${aws_db_subnet_group.MYSQL_DB_SUBNET.name}"
    parameter_group_name  = "${aws_db_parameter_group.microservices.id}"
    multi_az              = false # set to true to have high availability: 2 instances synchronized with each other
    vpc_security_group_ids  = ["${aws_security_group.SG_MYSQL_DB.id}"]
    storage_type            = "gp2"
    backup_retention_period = 30 # how long you’re going to keep your backups
    kms_key_id = "arn:aws:kms:ca-central-1:005810662120:key/620c939a-f373-466c-83d3-f9b32bfa2b1a"

    tags = {
        Name = "peel-pps-microservices"
        Environment = "PPS"
    }
}

# JobScheduler Database
resource "aws_db_parameter_group" "jobscheduler" {
  name   = "peeljobscheduler"
  family = "mysql8.0"

  parameter {
    name  = "time_zone"
    value = "US/Eastern" # MySQL has no America/Toronto time zone
  }

  parameter {
    name = "log_output"
    value = "FILE"
  }

  parameter {
    name = "general_log"
    value = "1"
  }

  parameter {
    name = "slow_query_log"
    value = "1"
  }

  parameter {
    name = "long_query_time"
    value = "2"
  }
}

resource "aws_db_instance" "mysqldb" {

    engine            = "mysql"
    engine_version    = "8.0.17"
    instance_class    = "db.t3.small"
    allocated_storage = 100
    storage_encrypted = true
    identifier        = "peelppsjobscheduler"
    name              = "peelppsjobscheduler"
    username          = "sa"
    password          = "${var.mysql-rds-password}" # password
    port              = 3306

    maintenance_window = "Mon:00:00-Mon:03:00"
    backup_window      = "03:00-06:00"
    deletion_protection = true
    enabled_cloudwatch_logs_exports = ["error", "general", "slowquery"]
    apply_immediately   = true

    db_subnet_group_name  = "${aws_db_subnet_group.MYSQL_DB_SUBNET.name}"
    parameter_group_name  = "${aws_db_parameter_group.jobscheduler.id}"
    multi_az              = false # set to true to have high availability: 2 instances synchronized with each other
    vpc_security_group_ids  = ["${aws_security_group.SG_MYSQL_DB.id}"]
    storage_type            = "gp2"
    backup_retention_period = 30 # how long you’re going to keep your backups
    kms_key_id = "arn:aws:kms:ca-central-1:005810662120:key/620c939a-f373-466c-83d3-f9b32bfa2b1a"

    tags = {
        Name = "peel-pps-jobscheduler"
        Environment = "PPS"
    }
}

