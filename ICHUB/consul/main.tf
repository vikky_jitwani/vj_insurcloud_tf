# ---------------------------------------------------------------------------------------------------------------------
# DEPLOY A CONSUL CLUSTER IN AWS
# These templates show an example of how to use the consul-cluster module to deploy Consul in AWS. We deploy two Auto
# Scaling Groups (ASGs): one with a small number of Consul server nodes and one with a larger number of Consul client
# nodes. Note that these templates assume that the AMI you provide via the ami_id input variable is built from
# the examples/consul-ami/consul.json Packer template.
# ---------------------------------------------------------------------------------------------------------------------

# Terraform 0.9.5 suffered from https://github.com/hashicorp/terraform/issues/14399, which causes this template the
# conditionals in this template to fail.
terraform {
  required_version = ">= 0.9.3, != 0.9.5"
}

locals {
  cluster_name = "${var.cluster_name_prefix}-${upper(var.environment)}-CONSUL"
  consul_domain_name = "consul.${var.environment}.${var.hosted_zone_domain_name}"
  api_protocol = "${upper(var.api_protocol)}"
  consul_module_version = "v0.6.1"
}

# ---------------------------------------------------------------------------------------------------------------------
# LOOK UP THE LATEST PRE-BUILT AMI
# ---------------------------------------------------------------------------------------------------------------------
data "aws_ami" "consul" {
  most_recent = true
  owners = ["self"]

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  filter {
    name   = "is-public"
    values = ["false"]
  }

  filter {
    name   = "name"
    values = ["ic_consul-${var.environment}-*"]
  }
}

# ---------------------------------------------------------------------------------------------------------------------
# DEPLOY THE CONSUL SERVER NODES
# ---------------------------------------------------------------------------------------------------------------------
module "consul_servers" {
  # When using these modules in your own templates, you will need to use a Git URL with a ref attribute that pins you
  # to a specific version of the modules, such as the following example:
  source = "github.com/hashicorp/terraform-aws-consul.git//modules/consul-cluster?ref=v0.6.1"
  # source = "./modules/consul-cluster"

  cluster_name  = "${local.cluster_name}"
  cluster_size  = "${var.num_servers}"
  instance_type = "${var.instance_type}"
  spot_price    = "${var.spot_price}"

  # The EC2 Instances will use these tags to automatically discover each other and form a cluster
  cluster_tag_key   = "${var.cluster_tag_key}"
  cluster_tag_value = "${local.cluster_name}"
  
  ami_id    = "${data.aws_ami.consul.image_id}"
  user_data = "${data.template_file.user_data_server.rendered}"

  vpc_id     = "${data.aws_vpc.default.id}"
  subnet_ids = "${data.aws_subnet_ids.default.ids}"

  # To make testing easier, we allow Consul and SSH requests from any IP address here but in a production
  # deployment, we strongly recommend you limit this to the IP address ranges of known, trusted servers inside your VPC.
  allowed_ssh_cidr_blocks     = ["${var.allowed_ssh_cidr_blocks}"]
  allowed_inbound_cidr_blocks = ["${var.allowed_inbound_cidr_blocks}"]
  ssh_key_name                = "${var.ssh_key_name}"

  root_volume_size = "${var.root_volume_size}"
  root_volume_type = "${var.root_volume_type}"

  # Tags
  tags = [
    {
      key                 = "Environment"
      value               = "${var.environment}"
      propagate_at_launch = "true"
    },
  ]
}

module "security_group_rules" {
  source = "./modules/consul-security-https-group-rules"

  security_group_id                    = "${module.consul_servers.security_group_id}"
  allowed_inbound_cidr_blocks          = ["${var.allowed_inbound_cidr_blocks}"]
}

# ---------------------------------------------------------------------------------------------------------------------
# THE USER DATA SCRIPT THAT WILL RUN ON EACH CONSUL SERVER EC2 INSTANCE WHEN IT'S BOOTING
# This script will configure and start Consul
# ---------------------------------------------------------------------------------------------------------------------

data "template_file" "user_data_server" {
  template = "${file("${path.module}/run-consul.sh")}"

  vars {
    cluster_tag_key          = "${var.cluster_tag_key}"
    cluster_tag_value        = "${local.cluster_name}"
    enable_gossip_encryption = "${var.enable_gossip_encryption}"
    gossip_encryption_key    = "${var.gossip_encryption_key}"
    enable_rpc_encryption    = "${var.enable_rpc_encryption}"
    ca_path                  = "${var.ca_path}"
    cert_file_path           = "${var.cert_file_path}"
    key_file_path            = "${var.key_file_path}"
    enable_https             = "${local.api_protocol == "HTTPS"}"
    enable_acl               = "${var.enable_acl}"
  }
}

# ---------------------------------------------------------------------------------------------------------------------
# DEPLOY THE NLB
# ---------------------------------------------------------------------------------------------------------------------
module "consul_nlb" {
  # When using these modules in your own templates, you will need to use a Git URL with a ref attribute that pins you
  # to a specific version of the modules, such as the following example:
  # source = "github.com/hashicorp/terraform-aws-consul//modules/consul-elb?ref=v0.0.1"
  source = "../modules/nlb"

  name = "${local.cluster_name}"

  vpc_id     = "${data.aws_vpc.default.id}"
  subnet_ids = "${data.aws_subnet_ids.default.ids}"
  internal   = "true"

  # Associate the ELB with the instances created by the consul Autoscaling group
  asg_name = "${module.consul_servers.asg_name}"
  api_protocol = "${var.api_protocol}"
  api_port     = "${var.api_port}"
  lb_port      = "${var.lb_port}"

  # In order to access consul over HTTPS, we need a domain name that matches the TLS cert
  create_dns_entry = "${var.create_dns_entry}"

  # Terraform conditionals are not short-circuiting, so we use join as a workaround to avoid errors when the
  # aws_route53_zone data source isn't actually set: https://github.com/hashicorp/hil/issues/50
  hosted_zone_id = "${var.create_dns_entry ? join("", data.aws_route53_zone.selected.*.zone_id) : ""}"

  domain_name = "${local.consul_domain_name}"
}

# Look up the Route 53 Hosted Zone by domain name
data "aws_route53_zone" "selected" {
  count = "${var.create_dns_entry}"
  name  = "${var.hosted_zone_domain_name}."

  vpc_id = "${data.aws_vpc.default.id}"
}

# ---------------------------------------------------------------------------------------------------------------------
# LOOK UP THE LATEST PRE-BUILT CONSUL VPC AND SUBNETS
# ---------------------------------------------------------------------------------------------------------------------
data "aws_vpc" "default" {
  default = "false"

  filter {
    name   = "tag:Name"
    values = "${var.vpc_name_filter}"
  }
}

data "aws_subnet_ids" "default" {
  vpc_id = "${data.aws_vpc.default.id}"

  filter {
    name   = "tag:Name"
    values = "${var.subnet_name_filter}"
  }
}

data "aws_region" "current" {}
