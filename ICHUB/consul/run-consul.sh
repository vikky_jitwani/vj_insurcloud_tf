#!/bin/bash
# This script is meant to be run in the User Data of each EC2 Instance while it's booting. The script uses the
# run-consul script to configure and start Consul in server mode. Note that this script assumes it's running in an AMI
# built from the Packer template in examples/consul-ami/consul.json.

set -e

# Send the log output from this script to user-data.log, syslog, and the console
# From: https://alestic.com/2010/12/ec2-user-data-output/
exec > >(tee /var/log/user-data.log|logger -t user-data -s 2>/dev/console) 2>&1

config_path="/opt/consul/config/default.json"
format_config_file(){
  jq . | sudo tee $config_path > /dev/null 
}

# These variables are passed in via Terraform template interplation
if [[ "${enable_gossip_encryption}" == "true" && ! -z "${gossip_encryption_key}" ]]; then
  # Note that setting the encryption key in plain text here means that it will be readable from the Terraform state file
  # and/or the EC2 API/console. We're doing this for simplicity, but in a real production environment you should pass an
  # encrypted key to Terraform and decrypt it before passing it to run-consul with something like KMS.
  gossip_encryption_configuration="--enable-gossip-encryption --gossip-encryption-key ${gossip_encryption_key}"
fi

if [[ "${enable_rpc_encryption}" == "true" && ! -z "${ca_path}" && ! -z "${cert_file_path}" && ! -z "${key_file_path}" ]]; then
  rpc_encryption_configuration="--enable-rpc-encryption --ca-path ${ca_path} --cert-file-path ${cert_file_path} --key-file-path ${key_file_path}"
fi

/opt/consul/bin/run-consul --server --cluster-tag-key "${cluster_tag_key}" --cluster-tag-value "${cluster_tag_value}" $gossip_encryption_configuration $rpc_encryption_configuration

if [[ "${enable_https}" == "true" ]]; then
  sed -r 's/("client_addr": "0.0.0.0",)/\1 "ports": { "http": -1, "https": 8501 },/g' $config_path | format_config_file
fi

if [[ "${enable_rpc_encryption}" == "true" ]]; then
  sed -i 's/verify_incoming/verify_incoming_rpc/g' $config_path
fi

if [[ "${enable_acl}" == "true" ]]; then
  sed -r 's/("server": true,)/\1 "acl": { "enabled": true, "default_policy": "allow", "enable_token_persistence": true },/g' $config_path | format_config_file
fi
