# #=======================================================================
# # DEV01
# #=======================================================================
##### ALBs #####

##### NLBs ####################
########## Internal ###########
resource "aws_lb" "ICPEEL_DEV01_INTERNAL_NLB" {
    name = "ICPEEL-DEV01-INTERNAL-NLB"
    internal           = true
    load_balancer_type = "network"
    enable_cross_zone_load_balancing = false
    subnets            = [
        "${var.peel-dev-private-1a-subnet}",
        "${var.peel-dev-private-1b-subnet}"
    ]

    tags = {
        Name = "ICPEEL-DEV01-INTERNAL-NLB"
    }
}

resource "aws_lb_target_group" "ICPEEL_DEV01_INTERNAL_NLB_HAPROXY_TARGET_GROUP" {
    name     = "ICPEEL-DEV01-HAPROXY-INT-NLB"
    port     = 80
    protocol = "TCP"
    vpc_id   = "${var.vpc_id}"

    health_check {
        protocol = "TCP"
        port = 8444
    }

    tags = {
        Name = "ICPEEL-DEV01-HAPROXY-INTERNAL-NLB-TARGET-GROUP"
    }
}

resource "aws_lb_listener" "ICPEEL_DEV01_INTERNAL_NLB_HAPROXY_LISTENER" {
    load_balancer_arn = "${aws_lb.ICPEEL_DEV01_INTERNAL_NLB.arn}"
    port              = "80"
    protocol          = "TCP"

    default_action {
        type             = "forward"
        target_group_arn = "${aws_lb_target_group.ICPEEL_DEV01_INTERNAL_NLB_HAPROXY_TARGET_GROUP.arn}"
    }
}


resource "aws_lb_target_group_attachment" "ICPEEL_DEV01_INTERNAL_NLB_TARGET_GROUP_ATTACHMENT_HAPROXY" {
    target_group_arn = "${aws_lb_target_group.ICPEEL_DEV01_INTERNAL_NLB_HAPROXY_TARGET_GROUP.arn}"
    target_id        = "${aws_instance.ICPEEL_DEV01_INTERNAL_HAPROXY.id}"
    port             = 80
}

output "ICPEEL_DEV01_INTERNAL_NLB_DNS_NAME" {
    value = "${aws_lb.ICPEEL_DEV01_INTERNAL_NLB.dns_name}"
}

output "ICPEEL_DEV01_INTERNAL_NLB_ZONE_ID" {
    value = "${aws_lb.ICPEEL_DEV01_INTERNAL_NLB.zone_id}"
}

# Not being used in Dev01 yet
########## External ###########
# resource "aws_lb" "ICPEEL_DEV01_EXTERNAL_NLB" {
#     name               = "ICPEEL-DEV01-EXTERNAL-NLB"
#     internal           = true
#     load_balancer_type = "network"
#     enable_cross_zone_load_balancing = false
#     subnets            = [
#         "${var.peel-dev-private-1a-subnet}",
#         "${var.peel-dev-private-1b-subnet}"
#     ] # Changing this value will force a recreation of the resource

#     tags = {
#         Name = "ICPEEL-DEV01-EXTERNAL-NLB"
#     }
# }

# output "ICPEEL_DEV01_EXTERNAL_NLB_DNS_NAME" {
#   value = "${aws_lb.ICPEEL_DEV01_EXTERNAL_NLB.dns_name}"
# }

# output "ICPEEL_DEV01_EXTERNAL_NLB_ZONE_ID" {
#   value = "${aws_lb.ICPEEL_DEV01_EXTERNAL_NLB.zone_id}"
# }

# resource "aws_lb_target_group" "ICPEEL_DEV01_EXTERNAL_NLB_HAPROXY_TARGET_GROUP" {
#     name     = "ICPEEL-DEV01-HAPROXY-EXT-NLB"
#     port     = 80
#     protocol = "TCP"
#     vpc_id   = "${var.vpc_id}"

#     health_check {
#         protocol = "TCP"
#         port = 8444
#     }

#     tags = {
#         Name = "ICPEEL-DEV01-EXTERNAL-NLB-HAPROXY-TARGET-GROUP"
#     }
# }

# resource "aws_lb_listener" "ICPEEL_DEV01_EXTERNAL_NLB_HAPROXY_LISTENER" {
#     load_balancer_arn = "${aws_lb.ICPEEL_DEV01_EXTERNAL_NLB.arn}"
#     port              = "80"
#     protocol          = "TCP"

#     default_action {
#         type             = "forward"
#         target_group_arn = "${aws_lb_target_group.ICPEEL_DEV01_EXTERNAL_NLB_HAPROXY_TARGET_GROUP.arn}"
#     }
# }

# resource "aws_lb_target_group_attachment" "ICPEEL_DEV01_EXTERNAL_NLB_TARGET_GROUP_ATTACHMENT_HAPROXY" {
#     target_group_arn = "${aws_lb_target_group.ICPEEL_DEV01_EXTERNAL_NLB_HAPROXY_TARGET_GROUP.arn}"
#     target_id        = "${aws_instance.ICPEEL_DEV01_EXTERNAL_HAPROXY.id}"
#     port             = 80
# }

# #=======================================================================
# # PPS
# #=======================================================================
##### ALBs #####

##### NLBs ####################
########## Internal ###########
resource "aws_lb" "ICPEEL_PPS_INTERNAL_NLB" {
    name = "ICPEEL-PPS-INTERNAL-NLB"
    internal           = true
    load_balancer_type = "network"
    enable_cross_zone_load_balancing = false
    subnets            = [
        "${var.peel-dev-private-1a-subnet}",
        "${var.peel-dev-private-1b-subnet}"
    ]

    tags = {
        Name = "ICPEEL-PPS-INTERNAL-NLB"
    }
}

resource "aws_lb_target_group" "ICPEEL_PPS_INTERNAL_NLB_HAPROXY_TARGET_GROUP" {
    name     = "ICPEEL-PPS-HAPROXY-INT-NLB"
    port     = 443
    protocol = "TCP"
    vpc_id   = "${var.vpc_id}"

    health_check {
        protocol = "TCP"
        port = 8444
    }

    tags = {
        Name = "ICPEEL-PPS-HAPROXY-INTERNAL-NLB-TARGET-GROUP"
    }
}

resource "aws_lb_listener" "ICPEEL_PPS_INTERNAL_NLB_HAPROXY_LISTENER" {
    load_balancer_arn = "${aws_lb.ICPEEL_PPS_INTERNAL_NLB.arn}"
    port              = "443"
    protocol          = "TCP"

    default_action {
        type             = "forward"
        target_group_arn = "${aws_lb_target_group.ICPEEL_PPS_INTERNAL_NLB_HAPROXY_TARGET_GROUP.arn}"
    }
}


resource "aws_lb_target_group_attachment" "ICPEEL_PPS_INTERNAL_NLB_TARGET_GROUP_ATTACHMENT_HAPROXY_1" {
    target_group_arn = "${aws_lb_target_group.ICPEEL_PPS_INTERNAL_NLB_HAPROXY_TARGET_GROUP.arn}"
    target_id        = "${aws_instance.ICPEEL_PPS_INTERNAL_HAPROXY_1.id}"
    port             = 443
}

resource "aws_lb_target_group_attachment" "ICPEEL_PPS_INTERNAL_NLB_TARGET_GROUP_ATTACHMENT_HAPROXY_2" {
    target_group_arn = "${aws_lb_target_group.ICPEEL_PPS_INTERNAL_NLB_HAPROXY_TARGET_GROUP.arn}"
    target_id        = "${aws_instance.ICPEEL_PPS_INTERNAL_HAPROXY_2.id}"
    port             = 443
}

output "ICPEEL_PPS_INTERNAL_NLB_DNS_NAME" {
    value = "${aws_lb.ICPEEL_PPS_INTERNAL_NLB.dns_name}"
}

output "ICPEEL_PPS_INTERNAL_NLB_ZONE_ID" {
    value = "${aws_lb.ICPEEL_PPS_INTERNAL_NLB.zone_id}"
}

########## External ###########
resource "aws_lb" "ICPEEL_PPS_EXTERNAL_NLB" {
    name               = "ICPEEL-PPS-EXTERNAL-NLB"
    internal           = true
    load_balancer_type = "network"
    enable_cross_zone_load_balancing = false
    subnets            = [
        "${var.peel-dev-private-1a-subnet}",
        "${var.peel-dev-private-1b-subnet}"
    ] # Changing this value will force a recreation of the resource

    tags = {
        Name = "ICPEEL-PPS-EXTERNAL-NLB"
    }
}

output "ICPEEL_PPS_EXTERNAL_NLB_DNS_NAME" {
  value = "${aws_lb.ICPEEL_PPS_EXTERNAL_NLB.dns_name}"
}

output "ICPEEL_PPS_EXTERNAL_NLB_ZONE_ID" {
  value = "${aws_lb.ICPEEL_PPS_EXTERNAL_NLB.zone_id}"
}

resource "aws_lb_target_group" "ICPEEL_PPS_EXTERNAL_NLB_HAPROXY_TARGET_GROUP" {
    name     = "ICPEEL-PPS-HAPROXY-EXTERNAL-NLB"
    port     = 443
    protocol = "TCP"
    vpc_id   = "${var.vpc_id}"

    health_check {
        protocol = "TCP"
        port = 8444
    }

    tags = {
        Name = "ICPEEL-PPS-EXTERNAL-NLB-HAPROXY-TARGET-GROUP"
    }
}

resource "aws_lb_listener" "ICPEEL_PPS_EXTERNAL_NLB_HAPROXY_LISTENER" {
    load_balancer_arn = "${aws_lb.ICPEEL_PPS_EXTERNAL_NLB.arn}"
    port              = "443"
    protocol          = "TCP"

    default_action {
        type             = "forward"
        target_group_arn = "${aws_lb_target_group.ICPEEL_PPS_EXTERNAL_NLB_HAPROXY_TARGET_GROUP.arn}"
    }
}

resource "aws_lb_target_group_attachment" "ICPEEL_PPS_EXTERNAL_NLB_TARGET_GROUP_ATTACHMENT_HAPROXY_1" {
    target_group_arn = "${aws_lb_target_group.ICPEEL_PPS_EXTERNAL_NLB_HAPROXY_TARGET_GROUP.arn}"
    target_id        = "${aws_instance.ICPEEL_PPS_EXTERNAL_HAPROXY_1.id}"
    port             = 443
}

resource "aws_lb_target_group_attachment" "ICPEEL_PPS_EXTERNAL_NLB_TARGET_GROUP_ATTACHMENT_HAPROXY_2" {
    target_group_arn = "${aws_lb_target_group.ICPEEL_PPS_EXTERNAL_NLB_HAPROXY_TARGET_GROUP.arn}"
    target_id        = "${aws_instance.ICPEEL_PPS_EXTERNAL_HAPROXY_2.id}"
    port             = 443
}
