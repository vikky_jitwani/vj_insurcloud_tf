
resource "aws_db_subnet_group" "MSSQL_DB_SUBNET" {
    name        = "mssql-db-subnet"
    description = "RDS subnet group"
    subnet_ids  = ["${var.peel-prod-database-1a-subnet}", "${var.peel-prod-database-1b-subnet}"]
}

resource "aws_db_subnet_group" "MYSQL_DB_SUBNET" {
    name        = "mysql-db-subnet"
    description = "RDS subnet group"
    subnet_ids  = ["${var.peel-prod-database-1a-subnet}", "${var.peel-prod-database-1b-subnet}"]
}

resource "aws_db_subnet_group" "POSTGRES_DB_SUBNET" {
    name        = "postgres-db-subnet"
    description = "RDS subnet group"
    subnet_ids  = ["${var.peel-prod-database-1a-subnet}", "${var.peel-prod-database-1b-subnet}"]
}

# Microservices Database
resource "aws_db_parameter_group" "microservices" {
  name   = "peelmicroservices"
  family = "mysql8.0"

  parameter {
    name  = "time_zone"
    value = "US/Eastern" # MySQL has no America/Toronto time zone
  }

  parameter {
    name = "log_output"
    value = "FILE"
  }

  parameter {
    name = "general_log"
    value = "1"
  }

  parameter {
    name = "slow_query_log"
    value = "1"
  }

  parameter {
    name = "long_query_time"
    value = "2"
  }
}

resource "aws_db_instance" "microservicesmysqldb" {

    engine            = "mysql"
    engine_version    = "8.0.17"
    instance_class    = "db.t3.medium"
    allocated_storage = 500
    storage_encrypted = true
    identifier        = "peelmicroservices"
    name              = "peelmicroservices"
    username          = "sa"
    password          = "${var.mysql-rds-password}" # password
    port              = 3306

    maintenance_window = "Mon:00:00-Mon:03:00"
    backup_window      = "03:00-06:01"
    deletion_protection = true
    enabled_cloudwatch_logs_exports = ["error", "general", "slowquery"]
    apply_immediately  = true

    db_subnet_group_name  = "${aws_db_subnet_group.MYSQL_DB_SUBNET.name}"
    parameter_group_name  = "${aws_db_parameter_group.microservices.id}"
    multi_az              = true # set to true to have high availability: 2 instances synchronized with each other
    vpc_security_group_ids  = ["${aws_security_group.SG_MYSQL_DB.id}"]
    storage_type            = "gp2"
    backup_retention_period = 30 # how long you’re going to keep your backups
    kms_key_id = "arn:aws:kms:ca-central-1:413329031558:key/6f5d1b3c-2901-4ce2-8f72-91414de86f20"

    tags = {
        Name = "peel-microservices"
        Environment = "PROD"
    }
}

# JobScheduler Database
resource "aws_db_parameter_group" "jobscheduler" {
  name   = "peeljobscheduler"
  family = "mysql8.0"

  parameter {
    name  = "time_zone"
    value = "US/Eastern" # MySQL has no America/Toronto time zone
  }

  parameter {
    name = "log_output"
    value = "FILE"
  }

  parameter {
    name = "general_log"
    value = "1"
  }

  parameter {
    name = "slow_query_log"
    value = "1"
  }

  parameter {
    name = "long_query_time"
    value = "2"
  }
}

resource "aws_db_instance" "mysqldb" {

    engine            = "mysql"
    engine_version    = "8.0.17"
    instance_class    = "db.t3.small"
    allocated_storage = 100
    storage_encrypted = true
    identifier        = "peeljobscheduler"
    name              = "peeljobscheduler"
    username          = "sa"
    password          = "${var.mysql-rds-password}" # password
    port              = 3306

    maintenance_window = "Mon:00:00-Mon:03:00"
    backup_window      = "03:00-06:01"
    deletion_protection = true
    enabled_cloudwatch_logs_exports = ["error", "general", "slowquery"]
    apply_immediately  = true

    db_subnet_group_name  = "${aws_db_subnet_group.MYSQL_DB_SUBNET.name}"
    parameter_group_name  = "${aws_db_parameter_group.jobscheduler.id}"
    multi_az              = true # set to true to have high availability: 2 instances synchronized with each other
    vpc_security_group_ids  = ["${aws_security_group.SG_MYSQL_DB.id}"]
    storage_type            = "gp2"
    backup_retention_period = 30 # how long you’re going to keep your backups
    kms_key_id = "arn:aws:kms:ca-central-1:413329031558:key/6f5d1b3c-2901-4ce2-8f72-91414de86f20"

    tags = {
        Name = "peel-jobscheduler"
        Environment = "PROD"
    }
}

# Prod Hubio Database
resource "aws_db_parameter_group" "hubio" {
  name   = "peelprod01hubio"
  family = "postgres10"

  parameter {
    name  = "timezone"
    value = "America/Toronto"
  }
}

resource "aws_db_instance" "postgresdb" {

    engine            = "postgres"
    engine_version    = "10.13"
    instance_class    = "db.t3.medium"
    allocated_storage = 50
    storage_encrypted = true
    identifier        = "peelprod01hubio"
    name              = "peelprod01hubio"
    username          = "sa"
    password          = "${var.postgres-rds-password}" # password
    port              = 5432

    maintenance_window = "Mon:00:00-Mon:03:00"
    backup_window      = "03:00-06:00"
    deletion_protection = true
    enabled_cloudwatch_logs_exports = ["postgresql", "upgrade"]

    db_subnet_group_name  = "${aws_db_subnet_group.POSTGRES_DB_SUBNET.name}"
    parameter_group_name  = "${aws_db_parameter_group.hubio.id}"
    multi_az              = true # set to true to have high availability: 2 instances synchronized with each other
    vpc_security_group_ids  = ["${aws_security_group.SG_POSTGRES_DB.id}"]
    storage_type            = "gp2"
    backup_retention_period = 30 # how long you’re going to keep your backups
    kms_key_id = "arn:aws:kms:ca-central-1:413329031558:key/6f5d1b3c-2901-4ce2-8f72-91414de86f20"

    tags = {
        Name = "peel-hubio"
        Environment = "PROD"
    }
}

# PPS Hubio Database
resource "aws_db_parameter_group" "hubiopps" {
  name   = "peel-pps-hubio"
  family = "postgres10"

  parameter {
    name  = "timezone"
    value = "America/Toronto"
  }
}

resource "aws_db_instance" "postgresdbpps" {

    engine            = "postgres"
    engine_version    = "10.13"
    instance_class    = "db.t3.small"
    allocated_storage = 100
    storage_encrypted = true
    identifier        = "peel-pps-hubio"
    name              = "peelppshubio"
    username          = "sa"
    password          = "${var.postgres-rds-password}" # password
    port              = 5432

    maintenance_window = "Mon:00:00-Mon:03:00"
    backup_window      = "03:00-06:00"
    deletion_protection = true
    enabled_cloudwatch_logs_exports = ["postgresql", "upgrade"]

    db_subnet_group_name  = "${aws_db_subnet_group.POSTGRES_DB_SUBNET.name}"
    parameter_group_name  = "${aws_db_parameter_group.hubio.id}"
    multi_az              = false # set to true to have high availability: 2 instances synchronized with each other
    vpc_security_group_ids  = ["${aws_security_group.SG_POSTGRES_DB.id}"]
    storage_type            = "gp2"
    backup_retention_period = 30 # how long you’re going to keep your backups
    kms_key_id = "arn:aws:kms:ca-central-1:413329031558:key/6f5d1b3c-2901-4ce2-8f72-91414de86f20"

    tags = {
        Name = "peel-pps-hubio"
        Environment = "PPS"
    }
}

