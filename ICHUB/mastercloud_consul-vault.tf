locals {
  vpc_name_filter    = "*HUB-*"
  subnet_name_filter = "*HUB-PRIVATE-*"

  pps_env_tag  = "pps"
  pps_cross_account_role = [
    "arn:aws:iam::005810662120:role/PeelConsulVaultRole",
    "arn:aws:iam::632109065501:role/CMS-SSMRole",
    "arn:aws:iam::348868695504:role/CMS-SSMRole",
    "arn:aws:iam::852868701745:role/CMS-SSMRole",
    "arn:aws:iam::623885847088:role/CmigServiceRole",
    "arn:aws:iam::532382648425:role/CmigServiceRole"
  ]
  prod_env_tag = "prod"
  prod_cross_account_role = [
    "arn:aws:iam::413329031558:role/PeelConsulVaultRole",
    "arn:aws:iam::632109065501:role/CMS-SSMRole",
    "arn:aws:iam::348868695504:role/CMS-SSMRole",
    "arn:aws:iam::852868701745:role/CMS-SSMRole",
    "arn:aws:iam::942574907772:role/CmigServiceRole",
    "arn:aws:iam::532382648425:role/CmigServiceRole"
  ]

  pps_default_ssh_key = "Insurcloud-Default_PPS"
  prod_consul_ssh_key = "Insurcloud-Default_Prod"
  prod_vault_ssh_key  = "Insurcloud-Vault_Prod"

  cluster_name_prefix = "A-MC-CAC-D-A003-IC"
  patching_policy_arn = "arn:aws:iam::020266858005:policy/CMS-SSMPolicy"
  permissions_boundary = "arn:aws:iam::020266858005:policy/DeloittePermissionsBoundaryPolicy"
}


# ---------------------------------------------------------------------------------------------------------------------
# PPS Clusters
# ---------------------------------------------------------------------------------------------------------------------

module "consul-pps" {
  source = "./consul"  

  environment         = "${local.pps_env_tag}"
  cluster_name_prefix = "${local.cluster_name_prefix}"
  
  vpc_name_filter    = ["${local.vpc_name_filter}"]
  subnet_name_filter = ["${local.subnet_name_filter}"]

  allowed_inbound_cidr_blocks = ["${var.ichub_cidr}", "${var.peeldev_cidr}"]
  allowed_ssh_cidr_blocks     = ["${var.ichub_cidr}"]
  ssh_key_name                = "${local.pps_default_ssh_key}" 
}

module "vault-pps" {
  source = "./vault"
  
  environment              = "${local.pps_env_tag}"
  cluster_name_prefix      = "${local.cluster_name_prefix}"
  consul_cluster_tag_key   = "${module.consul-pps.consul_servers_cluster_tag_key}"
  consul_cluster_tag_value = "${module.consul-pps.consul_servers_cluster_tag_value}"
    
  vpc_name_filter    = ["${local.vpc_name_filter}"]
  subnet_name_filter = ["${local.subnet_name_filter}"]

  allowed_inbound_cidr_blocks = ["${var.ichub_cidr}", "${var.peeldev_cidr}"]
  allowed_ssh_cidr_blocks     = ["${var.ichub_cidr}"]
  ssh_key_name                = "${local.pps_default_ssh_key}" 

  cross_account_assumed_role = "${local.pps_cross_account_role}"
  patching_policy_arn        = "${local.patching_policy_arn}"
  permissions_boundary       = "${local.permissions_boundary}"
}

# ---------------------------------------------------------------------------------------------------------------------
# PROD Clusters
# ---------------------------------------------------------------------------------------------------------------------

module "consul-prod" {
  source = "./consul"

  environment         = "${local.prod_env_tag}"
  cluster_name_prefix = "${local.cluster_name_prefix}"
  
  vpc_name_filter    = ["${local.vpc_name_filter}"]
  subnet_name_filter = ["${local.subnet_name_filter}"]

  allowed_inbound_cidr_blocks = ["${var.ichub_cidr}"]
  allowed_ssh_cidr_blocks     = ["${var.ichub_cidr}"]
  ssh_key_name                = "${local.prod_consul_ssh_key}" 
}

module "vault-prod" {
  source = "./vault"
 
  environment              = "${local.prod_env_tag}" 
  cluster_name_prefix      = "${local.cluster_name_prefix}"
  consul_cluster_tag_key   = "${module.consul-prod.consul_servers_cluster_tag_key}"
  consul_cluster_tag_value = "${module.consul-prod.consul_servers_cluster_tag_value}"
  
  vpc_name_filter    = ["${local.vpc_name_filter}"]
  subnet_name_filter = ["${local.subnet_name_filter}"]

  allowed_inbound_cidr_blocks = ["${var.ichub_cidr}", "${var.peelprod_cidr}"]
  allowed_ssh_cidr_blocks     = ["${var.ichub_cidr}"]
  ssh_key_name                = "${local.prod_vault_ssh_key}" 
  
  cross_account_assumed_role = "${local.prod_cross_account_role}"
  patching_policy_arn        = "${local.patching_policy_arn}"
  permissions_boundary       = "${local.permissions_boundary}"
}
