#=======================================================================
# EBS format and mount instructions
#=======================================================================
# Ensure fstab entry present:
#
# /dev/sdh /var/lib/docker xfs defaults,nofail 0 2
#
# mkfs.xfs /dev/sdh
# systemctl stop docker
# mount -a
# systemctl start docker
#

#=======================================================================
# JENKINS MASTER (HUB)
#=======================================================================
resource "aws_ebs_volume" "ICHUB_JENKINS_MASTER_DOCKER_EBS" {
  availability_zone = "ca-central-1a"
  size              = 25
  type              = "gp3"
  encrypted         = true
  kms_key_id        = "arn:aws:kms:ca-central-1:020266858005:key/6344f41d-b112-48df-882d-24dbcc75a7a5"
  snapshot_id       = "snap-0023a0dd9edbd7e2b"

  tags = {
    Name          = "ICHUB_JENKINS_MASTER_DOCKER_EBS"
    Docker        = "True"
  }
}

resource "aws_volume_attachment" "ICHUB_JENKINS_MASTER_DOCKER_EBS_ATTACHMENT" {
  device_name = "/dev/sdh"
  volume_id   = "${aws_ebs_volume.ICHUB_JENKINS_MASTER_DOCKER_EBS.id}"
  instance_id = "${aws_instance.ICHUB_JENKINS_MASTER.id}"
}

#=======================================================================
# NEXUS SONARQUBE CONSUL
#=======================================================================
resource "aws_ebs_volume" "ICHUB_NEXUS_SONARQUBE_CONSUL_DOCKER_EBS" {
  availability_zone = "ca-central-1a"
  size              = 25
  type              = "gp3"
  encrypted         = true
  kms_key_id        = "arn:aws:kms:ca-central-1:020266858005:key/6344f41d-b112-48df-882d-24dbcc75a7a5"
  snapshot_id       = "snap-0fc49da50ebd262c6"

  tags = {
    Name          = "ICHUB_NEXUS_SONARQUBE_CONSUL_DOCKER_EBS"
    Docker        = "True"
  }
}

resource "aws_volume_attachment" "ICHUB_NEXUS_SONARQUBE_CONSUL_DOCKER_EBS_ATTACHMENT" {
  device_name = "/dev/sdh"
  volume_id   = "${aws_ebs_volume.ICHUB_NEXUS_SONARQUBE_CONSUL_DOCKER_EBS.id}"
  instance_id = "${aws_instance.ICHUB_NEXUS_SONARQUBE_CONSUL.id}"
} 
