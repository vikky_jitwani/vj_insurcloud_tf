variable "client_name" {
  description = "AWS client name for resources (Ex: MASTERCLOUD)"
  default = ""
}

variable "enviornment_name" {
  description = "AWS enviornment name for resources (Ex: NON-PROD)"
  default = ""
}

variable "enviornment_type" {
  description = "AWS enviornment type for resources (Ex: P=PROD/HUB, D=NON-PROD)"
  default = ""
}

variable "client_number" {
  description = "AWS client number for tagging (Ex: A001)"
  default = "A010"
}

variable "region" {
  description = "AWS region for hosting our your network"
  default = "ca-central-1"
}

variable "volume_key" {
  description = "KMS volume key prod account"
  default = ""
}

variable "timezone" {
  description = "(Optional) Time zone of the DB instance. timezone is currently only supported by Microsoft SQL Server. The timezone can only be set on creation. See MSSQL User Guide for more information."
  default     = "Eastern"
}

variable "ami_id_amazonlinux201709base" {
  description = "Amazon Linux Encypted Root Volume AMI"
  default = ""
}

variable "ami_id_redhatlinux75" {
  description = "Redhat Linux Encypted Root Volume AMI"
  default = ""
}

variable "ami_id_amazonlinux201803base" {
  description = "Amazon Linux 2018 03 Encypted Root Volume AMI"
  default = ""
}

variable "ami_id_amazonlinux2base" {
  description = "Amazon Linux 2 Base Encypted Root Volume AMI"
  default = ""
}

variable "ami_id_windows2008base" {
  description = "Windows 2008 Base Encypted Root Volume AMI"
  default = ""
}

variable "ami_id_amazonlinux201712base" {
  description = "Amazon Linux 2017 12 Base Encypted Root Volume AMI"
  default = ""
}

variable "ami_id_windows2012R2base" {
  description = "Windows 2012 R2 Base Encypted Root Volume AMI"
  default = ""
}

variable "ami_id_windows2012R2sql14standard" {
  description = "Windows 2012 R2 Base Encypted Root Volume AMI with SQL 2014 Enterprise from Marketplace (https://aws.amazon.com/marketplace/pp/B00YQAUW4E)"
  default = ""
}

variable "ami_id_windows2012R2sql14enterprise" {
  description = "Windows 2012 R2 Base Encypted Root Volume AMI with SQL 2014 Enterprise from Marketplace (https://aws.amazon.com/marketplace/pp/B00KQP3L78)"
  default = ""
}

variable "ami_id_windows2016base" {
  description = "Windows 2016 Base Encypted Root Volume AMI"
  default = ""
}

variable "ami_id_windows2019base" {
  description = "Windows 2019 Base Encypted Root Volume AMI"
  default = ""
}
variable "ami_id_windows2019container" {
  description = "Windows 2019 Cont Encypted Root Volume AMI"
  default = ""
}

variable "ami_id_ubuntu1604baseimage" {
  description = "Ubuntu 16 04 Base Encypted Root Volume AMI"
  default = ""
}

variable "ami_id_ubuntu1804baseimage" {
  description = "Ubuntu 18 04 Base Encypted Root Volume AMI"
  default = ""
}

variable "vpc_id" {
  description = "VPC id for PROD account"
  default = "vpc-0db9dff301a2574a9"
}

variable "vpc_cidr" {
  description = "VPC cidr prod account"
  default = ""
}

variable "bcaa-prod-database-1a-subnet" {
  description = "Database subnet one"
  default = ""
}

variable "bcaa-prod-database-1b-subnet" {
  description = "Database subnet two"
  default = ""
}

variable "bcaa-prod-private-1a-subnet" {
  description = "Private Subnet one"
  default = ""
}

variable "bcaa-prod-private-1b-subnet" {
  description = "Private Subnet Two"
  default = ""

}

variable "default_cidr" {
  description = "default cidr used"
  default = ["10.0.0.0/8"]
}

variable "zone_id" {
  description = "default zone id"
  default = ""
}
variable "bcaa_dev_cidr" {
 description = "default cidr used"
 type = "list"
 default = ["10.41.8.0/22"]
}
 
variable "bcaa_prod_cidr" {
 description = "default cidr used"
 type = "list"
 default = ["10.41.4.0/22"]
}
 
variable "bcaa_hub_cidr" {
 description = "default cidr used"
 type = "list"
 default = ["10.41.0.0/22"]
}

variable "bcaa-hub_private_cidr" {
  description = "default cidr used"
  type = "list"
  default = ["10.41.0.0/24", "10.41.1.0/24"]
}

variable "bcaa_aviatrix_vpn" {
    default = "10.41.2.108/32"
}

variable "bcaa_dev_jobscheduler" {
    default = "10.13.0.63/32"
}
variable "bcaa_cidr" {
    default = "10.10.10.0/23"
}

variable "bcaa_watchguard_cidr" {
    default = "192.168.113.0/24"
}

variable "bcaa_dmz_cidr" {
    default = "192.168.100.0/24"
}

variable "bcaa_wireless_cidr" {
    default = "192.168.101.0/24"
}

variable "ad_dns1" {
    default = "10.41.0.91"
}
variable "ad_dns2" {
    default = "10.41.1.111"
}
variable "ad_dns3" {
    default = "10.41.0.2"
}
variable "ou_name" {
    default = "dev"
}
variable "pwordparameter" {
    default = "domain_password"
}
variable "unameparameter" {
    default = "domain_username"
}
variable "ad_domain" {
    default = "internal.bcaa.insurcloud.ca"
}

variable "mssql-rds-password" {
    default = "Toronto12345"
}
variable "mysql-rds-password" {
    default = "Toronto12345"
}

variable "postgres-rds-password" {
    default = "Toronto12345"
}

data "aws_caller_identity" "current" {}

variable "bcaa-dev-account" {
  default = "775629271970"
}

variable "bcaa-prod-account" {
  default = "338020371242"
}

variable "bcaa-hub-account" {
  default = "057006516829"
}