# #=======================================================================
# # DEV01
# #=======================================================================
####### GW VOLUMES #############
resource "aws_volume_attachment" "ICECONOMICAL_DEV01_GUIDEWIRE_SUITE_DOCKER_EBS_ATTACHMENT" {
  device_name = "/dev/sdh"
  volume_id = "${aws_ebs_volume.ICECONOMICAL_DEV01_GUIDEWIRE_SUITE_DOCKER_EBS.id}"
  instance_id = "${aws_instance.ICECONOMICAL_DEV01_GUIDEWIRE_SUITE.id}"
}

resource "aws_ebs_volume" "ICECONOMICAL_DEV01_GUIDEWIRE_SUITE_DOCKER_EBS" {
  availability_zone = "ca-central-1a"
  size = 150
  type = "gp3"
  encrypted = true
  kms_key_id = "arn:aws:kms:ca-central-1:632109065501:key/4bd92492-cc3b-4eed-8e08-3d8272303471"
  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "MANAGED_SERVICES"
    Name = "ICECONOMICAL_DEV01_GUIDEWIRE_SUITE_DOCKER_EBS"
    Docker = "True"
  }
}

resource "aws_volume_attachment" "ICECONOMICAL_DEV01_GUIDEWIRE_SUITE_LOGS_EBS_ATTACHMENT" {
  device_name = "/dev/sdi"
  volume_id = "${aws_ebs_volume.ICECONOMICAL_DEV01_GUIDEWIRE_SUITE_LOGS_EBS.id}"
  instance_id = "${aws_instance.ICECONOMICAL_DEV01_GUIDEWIRE_SUITE.id}"
}

resource "aws_ebs_volume" "ICECONOMICAL_DEV01_GUIDEWIRE_SUITE_LOGS_EBS" {
  availability_zone = "ca-central-1a"
  size = 150
  type = "gp3"
  encrypted = true
  kms_key_id = "arn:aws:kms:ca-central-1:632109065501:key/4bd92492-cc3b-4eed-8e08-3d8272303471"
  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "MANAGED_SERVICES"
    Name = "ICECONOMICAL_DEV01_GUIDEWIRE_SUITE_LOGS_EBS"
    Docker = "True"
  }
}


############ Internal HAProxy Volumes ##################
resource "aws_volume_attachment" "ICECONOMICAL_DEV01_INTERNAL_HAPROXY_DOCKER_EBS_ATTACHMENT" {
  device_name = "/dev/sdh"
  volume_id = "${aws_ebs_volume.ICECONOMICAL_DEV01_INTERNAL_HAPROXY_DOCKER_EBS.id}"
  instance_id = "${aws_instance.ICECONOMICAL_DEV01_INTERNAL_HAPROXY.id}"
}

resource "aws_ebs_volume" "ICECONOMICAL_DEV01_INTERNAL_HAPROXY_DOCKER_EBS" {
  availability_zone = "ca-central-1a"
  size = 20
  type = "gp3"
  encrypted = true
  kms_key_id = "arn:aws:kms:ca-central-1:632109065501:key/4bd92492-cc3b-4eed-8e08-3d8272303471"
  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "MANAGED_SERVICES"
    Name = "ICECONOMICAL_DEV01_INTERNAL_HAPROXY_DOCKER_EBS"
    Docker = "True"
  }
}

############ Microservices Volumes ##################
resource "aws_volume_attachment" "ICECONOMICAL_DEV01_MICROSERVICES_DOCKER_EBS_ATTACHMENT" {
  device_name = "/dev/sdh"
  volume_id = "${aws_ebs_volume.ICECONOMICAL_DEV01_MICROSERVICES_DOCKER_EBS.id}"
  instance_id = "${aws_instance.ICECONOMICAL_DEV01_MICROSERVICES.id}"
}

resource "aws_ebs_volume" "ICECONOMICAL_DEV01_MICROSERVICES_DOCKER_EBS" {
  availability_zone = "ca-central-1a"
  size = 50
  type = "gp3"
  encrypted = true
  kms_key_id = "arn:aws:kms:ca-central-1:632109065501:key/4bd92492-cc3b-4eed-8e08-3d8272303471"
  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "MANAGED_SERVICES"
    Name = "ICECONOMICAL_DEV01_MICROSERVICES_DOCKER_EBS"
    Docker = "True"

  }
}


# #=======================================================================
# # DEV02
# #=======================================================================
####### GW VOLUMES #############
resource "aws_volume_attachment" "ICECONOMICAL_DEV02_GUIDEWIRE_SUITE_DOCKER_EBS_ATTACHMENT" {
  device_name = "/dev/sdh"
  volume_id = "${aws_ebs_volume.ICECONOMICAL_DEV02_GUIDEWIRE_SUITE_DOCKER_EBS.id}"
  instance_id = "${aws_instance.ICECONOMICAL_DEV02_GUIDEWIRE_SUITE.id}"
}

resource "aws_ebs_volume" "ICECONOMICAL_DEV02_GUIDEWIRE_SUITE_DOCKER_EBS" {
  availability_zone = "ca-central-1a"
  size = 150
  type = "gp3"
  encrypted = true
  kms_key_id = "arn:aws:kms:ca-central-1:632109065501:key/4bd92492-cc3b-4eed-8e08-3d8272303471"
  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "PROJECT"
    Name = "ICECONOMICAL_DEV02_GUIDEWIRE_SUITE_DOCKER_EBS"
    Docker = "True"

  }
}

resource "aws_volume_attachment" "ICECONOMICAL_DEV02_GUIDEWIRE_SUITE_LOGS_EBS_ATTACHMENT" {
  device_name = "/dev/sdi"
  volume_id = "${aws_ebs_volume.ICECONOMICAL_DEV02_GUIDEWIRE_SUITE_LOGS_EBS.id}"
  instance_id = "${aws_instance.ICECONOMICAL_DEV02_GUIDEWIRE_SUITE.id}"
}

resource "aws_ebs_volume" "ICECONOMICAL_DEV02_GUIDEWIRE_SUITE_LOGS_EBS" {
  availability_zone = "ca-central-1a"
  size = 150
  type = "gp3"
  encrypted = true
  kms_key_id = "arn:aws:kms:ca-central-1:632109065501:key/4bd92492-cc3b-4eed-8e08-3d8272303471"
  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "PROJECT"
    Name = "ICECONOMICAL_DEV02_GUIDEWIRE_SUITE_LOGS_EBS"
    Docker = "True"

  }
}

############ Microservices Volumes ##################
resource "aws_volume_attachment" "ICECONOMICAL_DEV02_MICROSERVICES_DOCKER_EBS_ATTACHMENT" {
  device_name = "/dev/sdh"
  volume_id = "${aws_ebs_volume.ICECONOMICAL_DEV02_MICROSERVICES_DOCKER_EBS.id}"
  instance_id = "${aws_instance.ICECONOMICAL_DEV02_MICROSERVICES.id}"
}

resource "aws_ebs_volume" "ICECONOMICAL_DEV02_MICROSERVICES_DOCKER_EBS" {
  availability_zone = "ca-central-1a"
  size = 50
  type = "gp3"
  encrypted = true
  kms_key_id = "arn:aws:kms:ca-central-1:632109065501:key/4bd92492-cc3b-4eed-8e08-3d8272303471"
  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "PROJECT"
    Name = "ICECONOMICAL_DEV02_MICROSERVICES_DOCKER_EBS"
    Docker = "True"

  }
}

############ Internal HAProxy Volumes ##################
resource "aws_volume_attachment" "ICECONOMICAL_DEV02_INTERNAL_HAPROXY_DOCKER_EBS_ATTACHMENT" {
  device_name = "/dev/sdh"
  volume_id = "${aws_ebs_volume.ICECONOMICAL_DEV02_INTERNAL_HAPROXY_DOCKER_EBS.id}"
  instance_id = "${aws_instance.ICECONOMICAL_DEV02_INTERNAL_HAPROXY.id}"
}

resource "aws_ebs_volume" "ICECONOMICAL_DEV02_INTERNAL_HAPROXY_DOCKER_EBS" {
  availability_zone = "ca-central-1a"
  size = 20
  type = "gp3"
  encrypted = true
  kms_key_id = "arn:aws:kms:ca-central-1:632109065501:key/4bd92492-cc3b-4eed-8e08-3d8272303471"
  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "PROJECT"
    Name = "ICECONOMICAL_DEV02_INTERNAL_HAPROXY_DOCKER_EBS"
    Docker = "True"

  }
}


# #=======================================================================
# # ST01
# #=======================================================================
####### GW VOLUMES #############
resource "aws_volume_attachment" "ICECONOMICAL_ST01_GUIDEWIRE_SUITE_DOCKER_EBS_ATTACHMENT" {
  device_name = "/dev/sdh"
  volume_id = "${aws_ebs_volume.ICECONOMICAL_ST01_GUIDEWIRE_SUITE_DOCKER_EBS.id}"
  instance_id = "${aws_instance.ICECONOMICAL_ST01_GUIDEWIRE_SUITE.id}"
}

resource "aws_ebs_volume" "ICECONOMICAL_ST01_GUIDEWIRE_SUITE_DOCKER_EBS" {
  availability_zone = "ca-central-1a"
  size = 150
  type = "gp3"
  encrypted = true
  kms_key_id = "arn:aws:kms:ca-central-1:632109065501:key/4bd92492-cc3b-4eed-8e08-3d8272303471"
  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "PROJECT"
    Name = "ICECONOMICAL_ST01_GUIDEWIRE_SUITE_DOCKER_EBS"
    Docker = "True"
  }
}

resource "aws_volume_attachment" "ICECONOMICAL_ST01_GUIDEWIRE_SUITE_LOGS_EBS_ATTACHMENT" {
  device_name = "/dev/sdi"
  volume_id = "${aws_ebs_volume.ICECONOMICAL_ST01_GUIDEWIRE_SUITE_LOGS_EBS.id}"
  instance_id = "${aws_instance.ICECONOMICAL_ST01_GUIDEWIRE_SUITE.id}"
}

resource "aws_ebs_volume" "ICECONOMICAL_ST01_GUIDEWIRE_SUITE_LOGS_EBS" {
  availability_zone = "ca-central-1a"
  size = 150
  type = "gp3"
  encrypted = true
  kms_key_id = "arn:aws:kms:ca-central-1:632109065501:key/4bd92492-cc3b-4eed-8e08-3d8272303471"
  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "PROJECT"
    Name = "ICECONOMICAL_ST01_GUIDEWIRE_SUITE_LOGS_EBS"
    Docker = "True"
  }
}

############ Microservices Volumes ##################
resource "aws_volume_attachment" "ICECONOMICAL_ST01_MICROSERVICES_DOCKER_EBS_ATTACHMENT" {
  device_name = "/dev/sdh"
  volume_id = "${aws_ebs_volume.ICECONOMICAL_ST01_MICROSERVICES_DOCKER_EBS.id}"
  instance_id = "${aws_instance.ICECONOMICAL_ST01_MICROSERVICES.id}"
}

resource "aws_ebs_volume" "ICECONOMICAL_ST01_MICROSERVICES_DOCKER_EBS" {
  availability_zone = "ca-central-1a"
  size = 50
  type = "gp3"
  encrypted = true
  kms_key_id = "arn:aws:kms:ca-central-1:632109065501:key/4bd92492-cc3b-4eed-8e08-3d8272303471"
  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "PROJECT"
    Name = "ICECONOMICAL_ST01_MICROSERVICES_DOCKER_EBS"
    Docker = "True"
  }
}

############ Internal HAProxy Volumes ##################
resource "aws_volume_attachment" "ICECONOMICAL_ST01_INTERNAL_HAPROXY_DOCKER_EBS_ATTACHMENT" {
  device_name = "/dev/sdh"
  volume_id = "${aws_ebs_volume.ICECONOMICAL_ST01_INTERNAL_HAPROXY_DOCKER_EBS.id}"
  instance_id = "${aws_instance.ICECONOMICAL_ST01_INTERNAL_HAPROXY.id}"
}

resource "aws_ebs_volume" "ICECONOMICAL_ST01_INTERNAL_HAPROXY_DOCKER_EBS" {
  availability_zone = "ca-central-1a"
  size = 20
  type = "gp3"
  encrypted = true
  kms_key_id = "arn:aws:kms:ca-central-1:632109065501:key/4bd92492-cc3b-4eed-8e08-3d8272303471"
  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "PROJECT"
    Name = "ICECONOMICAL_ST01_INTERNAL_HAPROXY_DOCKER_EBS"
    Docker = "True"
  }
}


# #=======================================================================
# # UAT01
# #=======================================================================
####### GW VOLUMES #############
resource "aws_volume_attachment" "ICECONOMICAL_UAT01_GUIDEWIRE_SUITE_DOCKER_EBS_ATTACHMENT" {
  device_name = "/dev/sdh"
  volume_id = "${aws_ebs_volume.ICECONOMICAL_UAT01_GUIDEWIRE_SUITE_DOCKER_EBS.id}"
  instance_id = "${aws_instance.ICECONOMICAL_UAT01_GUIDEWIRE_SUITE.id}"
}

resource "aws_ebs_volume" "ICECONOMICAL_UAT01_GUIDEWIRE_SUITE_DOCKER_EBS" {
  availability_zone = "ca-central-1a"
  size = 150
  type = "gp3"
  encrypted = true
  kms_key_id = "arn:aws:kms:ca-central-1:632109065501:key/4bd92492-cc3b-4eed-8e08-3d8272303471"
  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "PROJECT"
    Name = "ICECONOMICAL_UAT01_GUIDEWIRE_SUITE_DOCKER_EBS"
    Docker = "True"
  }
}

resource "aws_volume_attachment" "ICECONOMICAL_UAT01_GUIDEWIRE_SUITE_LOGS_EBS_ATTACHMENT" {
  device_name = "/dev/sdi"
  volume_id = "${aws_ebs_volume.ICECONOMICAL_UAT01_GUIDEWIRE_SUITE_LOGS_EBS.id}"
  instance_id = "${aws_instance.ICECONOMICAL_UAT01_GUIDEWIRE_SUITE.id}"
}

resource "aws_ebs_volume" "ICECONOMICAL_UAT01_GUIDEWIRE_SUITE_LOGS_EBS" {
  availability_zone = "ca-central-1a"
  size = 150
  type = "gp3"
  encrypted = true
  kms_key_id = "arn:aws:kms:ca-central-1:632109065501:key/4bd92492-cc3b-4eed-8e08-3d8272303471"
  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "PROJECT"
    Name = "ICECONOMICAL_UAT01_GUIDEWIRE_SUITE_LOGS_EBS"
    Docker = "True"
  }
}

############ Microservices Volumes ##################
resource "aws_volume_attachment" "ICECONOMICAL_UAT01_MICROSERVICES_DOCKER_EBS_ATTACHMENT" {
  device_name = "/dev/sdh"
  volume_id = "${aws_ebs_volume.ICECONOMICAL_UAT01_MICROSERVICES_DOCKER_EBS.id}"
  instance_id = "${aws_instance.ICECONOMICAL_UAT01_MICROSERVICES.id}"
}

resource "aws_ebs_volume" "ICECONOMICAL_UAT01_MICROSERVICES_DOCKER_EBS" {
  availability_zone = "ca-central-1a"
  size = 50
  type = "gp3"
  encrypted = true
  kms_key_id = "arn:aws:kms:ca-central-1:632109065501:key/4bd92492-cc3b-4eed-8e08-3d8272303471"
  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "PROJECT"
    Name = "ICECONOMICAL_UAT01_MICROSERVICES_DOCKER_EBS"
    Docker = "True"
  }
}

############ Internal HAProxy Volumes ##################
resource "aws_volume_attachment" "ICECONOMICAL_UAT01_INTERNAL_HAPROXY_DOCKER_EBS_ATTACHMENT" {
  device_name = "/dev/sdh"
  volume_id = "${aws_ebs_volume.ICECONOMICAL_UAT01_INTERNAL_HAPROXY_DOCKER_EBS.id}"
  instance_id = "${aws_instance.ICECONOMICAL_UAT01_INTERNAL_HAPROXY.id}"
}

resource "aws_ebs_volume" "ICECONOMICAL_UAT01_INTERNAL_HAPROXY_DOCKER_EBS" {
  availability_zone = "ca-central-1a"
  size = 20
  type = "gp3"
  encrypted = true
  kms_key_id = "arn:aws:kms:ca-central-1:632109065501:key/4bd92492-cc3b-4eed-8e08-3d8272303471"
  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "PROJECT"
    Name = "ICECONOMICAL_UAT01_INTERNAL_HAPROXY_DOCKER_EBS"
    Docker = "True"
  }
}


# #=======================================================================
# # PRF01
# #=======================================================================

####### GW VOLUMES #############
resource "aws_volume_attachment" "ICECONOMICAL_PRF01_GUIDEWIRE01_SUITE_DOCKER_EBS_ATTACHMENT" {
  device_name = "/dev/sdh"
  volume_id = "${aws_ebs_volume.ICECONOMICAL_PRF01_GUIDEWIRE01_SUITE_DOCKER_EBS.id}"
  instance_id = "${aws_instance.ICECONOMICAL_PRF01_GUIDEWIRE01_SUITE.id}"
}

resource "aws_ebs_volume" "ICECONOMICAL_PRF01_GUIDEWIRE01_SUITE_DOCKER_EBS" {
  availability_zone = "ca-central-1a"
  size = 150
  type = "gp3"
  encrypted = true
  kms_key_id = "arn:aws:kms:ca-central-1:632109065501:key/4bd92492-cc3b-4eed-8e08-3d8272303471"
  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "PROJECT"
    Name = "ICECONOMICAL_PRF01_GUIDEWIRE01_SUITE_DOCKER_EBS"
    Docker = "True"
  }
}

resource "aws_volume_attachment" "ICECONOMICAL_PRF01_GUIDEWIRE01_SUITE_LOGS_EBS_ATTACHMENT" {
  device_name = "/dev/sdi"
  volume_id = "${aws_ebs_volume.ICECONOMICAL_PRF01_GUIDEWIRE01_SUITE_LOGS_EBS.id}"
  instance_id = "${aws_instance.ICECONOMICAL_PRF01_GUIDEWIRE01_SUITE.id}"
}

resource "aws_ebs_volume" "ICECONOMICAL_PRF01_GUIDEWIRE01_SUITE_LOGS_EBS" {
  availability_zone = "ca-central-1a"
  size = 150
  type = "gp3"
  encrypted = true
  kms_key_id = "arn:aws:kms:ca-central-1:632109065501:key/4bd92492-cc3b-4eed-8e08-3d8272303471"
  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "PROJECT"
    Name = "ICECONOMICAL_PRF01_GUIDEWIRE01_SUITE_LOGS_EBS"
    Docker = "True"
  }
}

resource "aws_volume_attachment" "ICECONOMICAL_PRF01_GUIDEWIRE02_SUITE_DOCKER_EBS_ATTACHMENT" {
  device_name = "/dev/sdh"
  volume_id = "${aws_ebs_volume.ICECONOMICAL_PRF01_GUIDEWIRE02_SUITE_DOCKER_EBS.id}"
  instance_id = "${aws_instance.ICECONOMICAL_PRF01_GUIDEWIRE02_SUITE.id}"
}

resource "aws_ebs_volume" "ICECONOMICAL_PRF01_GUIDEWIRE02_SUITE_DOCKER_EBS" {
  availability_zone = "ca-central-1b"
  size = 150
  type = "gp3"
  encrypted = true
  kms_key_id = "arn:aws:kms:ca-central-1:632109065501:key/4bd92492-cc3b-4eed-8e08-3d8272303471"
  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "PROJECT"
    Name = "ICECONOMICAL_PRF01_GUIDEWIRE02_SUITE_DOCKER_EBS"
    Docker = "True"
  }
}

resource "aws_volume_attachment" "ICECONOMICAL_PRF01_GUIDEWIRE02_SUITE_LOGS_EBS_ATTACHMENT" {
  device_name = "/dev/sdi"
  volume_id = "${aws_ebs_volume.ICECONOMICAL_PRF01_GUIDEWIRE02_SUITE_LOGS_EBS.id}"
  instance_id = "${aws_instance.ICECONOMICAL_PRF01_GUIDEWIRE02_SUITE.id}"
}

resource "aws_ebs_volume" "ICECONOMICAL_PRF01_GUIDEWIRE02_SUITE_LOGS_EBS" {
  availability_zone = "ca-central-1b"
  size = 150
  type = "gp3"
  encrypted = true
  kms_key_id = "arn:aws:kms:ca-central-1:632109065501:key/4bd92492-cc3b-4eed-8e08-3d8272303471"
  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "PROJECT"
    Name = "ICECONOMICAL_PRF01_GUIDEWIRE02_SUITE_LOGS_EBS"
    Docker = "True"
  }
}

resource "aws_volume_attachment" "ICECONOMICAL_PRF01_GUIDEWIRE03_SUITE_DOCKER_EBS_ATTACHMENT" {
  device_name = "/dev/sdh"
  volume_id = "${aws_ebs_volume.ICECONOMICAL_PRF01_GUIDEWIRE03_SUITE_DOCKER_EBS.id}"
  instance_id = "${aws_instance.ICECONOMICAL_PRF01_GUIDEWIRE03_SUITE.id}"
}

resource "aws_ebs_volume" "ICECONOMICAL_PRF01_GUIDEWIRE03_SUITE_DOCKER_EBS" {
  availability_zone = "ca-central-1a"
  size = 150
  type = "gp3"
  encrypted = true
  kms_key_id = "arn:aws:kms:ca-central-1:632109065501:key/4bd92492-cc3b-4eed-8e08-3d8272303471"
  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "PROJECT"
    Name = "ICECONOMICAL_PRF01_GUIDEWIRE03_SUITE_DOCKER_EBS"
    Docker = "True"
  }
}

resource "aws_volume_attachment" "ICECONOMICAL_PRF01_GUIDEWIRE03_SUITE_LOGS_EBS_ATTACHMENT" {
  device_name = "/dev/sdi"
  volume_id = "${aws_ebs_volume.ICECONOMICAL_PRF01_GUIDEWIRE03_SUITE_LOGS_EBS.id}"
  instance_id = "${aws_instance.ICECONOMICAL_PRF01_GUIDEWIRE03_SUITE.id}"
}

resource "aws_ebs_volume" "ICECONOMICAL_PRF01_GUIDEWIRE03_SUITE_LOGS_EBS" {
  availability_zone = "ca-central-1a"
  size = 150
  type = "gp3"
  encrypted = true
  kms_key_id = "arn:aws:kms:ca-central-1:632109065501:key/4bd92492-cc3b-4eed-8e08-3d8272303471"
  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "PROJECT"
    Name = "ICECONOMICAL_PRF01_GUIDEWIRE03_SUITE_LOGS_EBS"
    Docker = "True"
  }
}

resource "aws_volume_attachment" "ICECONOMICAL_PRF01_GUIDEWIRE04_SUITE_DOCKER_EBS_ATTACHMENT" {
  device_name = "/dev/sdh"
  volume_id = "${aws_ebs_volume.ICECONOMICAL_PRF01_GUIDEWIRE04_SUITE_DOCKER_EBS.id}"
  instance_id = "${aws_instance.ICECONOMICAL_PRF01_GUIDEWIRE04_SUITE.id}"
}

resource "aws_ebs_volume" "ICECONOMICAL_PRF01_GUIDEWIRE04_SUITE_DOCKER_EBS" {
  availability_zone = "ca-central-1b"
  size = 150
  type = "gp3"
  encrypted = true
  kms_key_id = "arn:aws:kms:ca-central-1:632109065501:key/4bd92492-cc3b-4eed-8e08-3d8272303471"
  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "PROJECT"
    Name = "ICECONOMICAL_PRF01_GUIDEWIRE04_SUITE_DOCKER_EBS"
    Docker = "True"
  }
}

resource "aws_volume_attachment" "ICECONOMICAL_PRF01_GUIDEWIRE04_SUITE_LOGS_EBS_ATTACHMENT" {
  device_name = "/dev/sdi"
  volume_id = "${aws_ebs_volume.ICECONOMICAL_PRF01_GUIDEWIRE04_SUITE_LOGS_EBS.id}"
  instance_id = "${aws_instance.ICECONOMICAL_PRF01_GUIDEWIRE04_SUITE.id}"
}

resource "aws_ebs_volume" "ICECONOMICAL_PRF01_GUIDEWIRE04_SUITE_LOGS_EBS" {
  availability_zone = "ca-central-1b"
  size = 150
  type = "gp3"
  encrypted = true
  kms_key_id = "arn:aws:kms:ca-central-1:632109065501:key/4bd92492-cc3b-4eed-8e08-3d8272303471"
  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "PROJECT"
    Name = "ICECONOMICAL_PRF01_GUIDEWIRE04_SUITE_LOGS_EBS"
    Docker = "True"
  }
}

############ Microservices Volumes ##################
resource "aws_volume_attachment" "ICECONOMICAL_PRF01_MICROSERVICES01_DOCKER_EBS_ATTACHMENT" {
  device_name = "/dev/sdh"
  volume_id = "${aws_ebs_volume.ICECONOMICAL_PRF01_MICROSERVICES01_DOCKER_EBS.id}"
  instance_id = "${aws_instance.ICECONOMICAL_PRF01_MICROSERVICES_APPLICATION01.id}"
}

resource "aws_ebs_volume" "ICECONOMICAL_PRF01_MICROSERVICES01_DOCKER_EBS" {
  availability_zone = "ca-central-1a"
  size = 50
  type = "gp3"
  encrypted = true
  kms_key_id = "arn:aws:kms:ca-central-1:632109065501:key/4bd92492-cc3b-4eed-8e08-3d8272303471"
  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "PROJECT"
    Name = "ICECONOMICAL_PRF01_MICROSERVICES01_DOCKER_EBS"
    Docker = "True"
  }
}

resource "aws_volume_attachment" "ICECONOMICAL_PRF01_MICROSERVICES02_DOCKER_EBS_ATTACHMENT" {
  device_name = "/dev/sdh"
  volume_id = "${aws_ebs_volume.ICECONOMICAL_PRF01_MICROSERVICES02_DOCKER_EBS.id}"
  instance_id = "${aws_instance.ICECONOMICAL_PRF01_MICROSERVICES_APPLICATION02.id}"
}

resource "aws_ebs_volume" "ICECONOMICAL_PRF01_MICROSERVICES02_DOCKER_EBS" {
  availability_zone = "ca-central-1b"
  size = 50
  type = "gp3"
  encrypted = true
  kms_key_id = "arn:aws:kms:ca-central-1:632109065501:key/4bd92492-cc3b-4eed-8e08-3d8272303471"
  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "PROJECT"
    Name = "ICECONOMICAL_PRF01_MICROSERVICES02_DOCKER_EBS"
    Docker = "True"
  }
}

############ Internal HAProxy Volumes ##################
resource "aws_volume_attachment" "ICECONOMICAL_PRF01_INTERNAL_HAPROXY_DOCKER_EBS_ATTACHMENT01" {
  device_name = "/dev/sdh"
  volume_id = "${aws_ebs_volume.ICECONOMICAL_PRF01_INTERNAL_HAPROXY_DOCKER_EBS01.id}"
  instance_id = "${aws_instance.ICECONOMICAL_PRF01_INTERNAL_HAPROXY01.id}"
}

resource "aws_ebs_volume" "ICECONOMICAL_PRF01_INTERNAL_HAPROXY_DOCKER_EBS01" {
  availability_zone = "ca-central-1a"
  size = 20
  type = "gp3"
  encrypted = true
  kms_key_id = "arn:aws:kms:ca-central-1:632109065501:key/4bd92492-cc3b-4eed-8e08-3d8272303471"
  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "PROJECT"
    Name = "ICECONOMICAL_PRF01_INTERNAL_HAPROXY_DOCKER_EBS01"
    Docker = "True"
  }
}

resource "aws_volume_attachment" "ICECONOMICAL_PRF01_INTERNAL_HAPROXY_DOCKER_EBS_ATTACHMENT02" {
  device_name = "/dev/sdh"
  volume_id = "${aws_ebs_volume.ICECONOMICAL_PRF01_INTERNAL_HAPROXY_DOCKER_EBS02.id}"
  instance_id = "${aws_instance.ICECONOMICAL_PRF01_INTERNAL_HAPROXY02.id}"
}

resource "aws_ebs_volume" "ICECONOMICAL_PRF01_INTERNAL_HAPROXY_DOCKER_EBS02" {
  availability_zone = "ca-central-1b"
  size = 20
  type = "gp3"
  encrypted = true
  kms_key_id = "arn:aws:kms:ca-central-1:632109065501:key/4bd92492-cc3b-4eed-8e08-3d8272303471"
  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "PROJECT"
    Name = "ICECONOMICAL_PRF01_INTERNAL_HAPROXY_DOCKER_EBS02"
    Docker = "True"
  }
}


# #=======================================================================
# # SIT01
# #=======================================================================

####### GW VOLUMES #############
resource "aws_volume_attachment" "ICECONOMICAL_SIT01_GUIDEWIRE01_SUITE_DOCKER_EBS_ATTACHMENT" {
  device_name = "/dev/sdh"
  volume_id = "${aws_ebs_volume.ICECONOMICAL_SIT01_GUIDEWIRE01_SUITE_DOCKER_EBS.id}"
  instance_id = "${aws_instance.ICECONOMICAL_SIT01_GUIDEWIRE01_SUITE.id}"
}

resource "aws_ebs_volume" "ICECONOMICAL_SIT01_GUIDEWIRE01_SUITE_DOCKER_EBS" {
  availability_zone = "ca-central-1a"
  size = 150
  type = "gp3"
  encrypted = true
  kms_key_id = "arn:aws:kms:ca-central-1:632109065501:key/4bd92492-cc3b-4eed-8e08-3d8272303471"
  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "PROJECT"
    Name = "ICECONOMICAL_SIT01_GUIDEWIRE01_SUITE_DOCKER_EBS"
    Docker = "True"
  }
}

resource "aws_volume_attachment" "ICECONOMICAL_SIT01_GUIDEWIRE01_SUITE_LOGS_EBS_ATTACHMENT" {
  device_name = "/dev/sdi"
  volume_id = "${aws_ebs_volume.ICECONOMICAL_SIT01_GUIDEWIRE01_SUITE_LOGS_EBS.id}"
  instance_id = "${aws_instance.ICECONOMICAL_SIT01_GUIDEWIRE01_SUITE.id}"
}

resource "aws_ebs_volume" "ICECONOMICAL_SIT01_GUIDEWIRE01_SUITE_LOGS_EBS" {
  availability_zone = "ca-central-1a"
  size = 150
  type = "gp3"
  encrypted = true
  kms_key_id = "arn:aws:kms:ca-central-1:632109065501:key/4bd92492-cc3b-4eed-8e08-3d8272303471"
  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "PROJECT"
    Name = "ICECONOMICAL_SIT01_GUIDEWIRE01_SUITE_LOGS_EBS"
    Docker = "True"
  }
}

resource "aws_volume_attachment" "ICECONOMICAL_SIT01_GUIDEWIRE02_SUITE_DOCKER_EBS_ATTACHMENT" {
  device_name = "/dev/sdh"
  volume_id = "${aws_ebs_volume.ICECONOMICAL_SIT01_GUIDEWIRE02_SUITE_DOCKER_EBS.id}"
  instance_id = "${aws_instance.ICECONOMICAL_SIT01_GUIDEWIRE02_SUITE.id}"
}

resource "aws_ebs_volume" "ICECONOMICAL_SIT01_GUIDEWIRE02_SUITE_DOCKER_EBS" {
  availability_zone = "ca-central-1b"
  size = 150
  type = "gp3"
  encrypted = true
  kms_key_id = "arn:aws:kms:ca-central-1:632109065501:key/4bd92492-cc3b-4eed-8e08-3d8272303471"
  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "PROJECT"
    Name = "ICECONOMICAL_SIT01_GUIDEWIRE02_SUITE_DOCKER_EBS"
    Docker = "True"
  }
}

resource "aws_volume_attachment" "ICECONOMICAL_SIT01_GUIDEWIRE02_SUITE_LOGS_EBS_ATTACHMENT" {
  device_name = "/dev/sdi"
  volume_id = "${aws_ebs_volume.ICECONOMICAL_SIT01_GUIDEWIRE02_SUITE_LOGS_EBS.id}"
  instance_id = "${aws_instance.ICECONOMICAL_SIT01_GUIDEWIRE02_SUITE.id}"
}

resource "aws_ebs_volume" "ICECONOMICAL_SIT01_GUIDEWIRE02_SUITE_LOGS_EBS" {
  availability_zone = "ca-central-1b"
  size = 150
  type = "gp3"
  encrypted = true
  kms_key_id = "arn:aws:kms:ca-central-1:632109065501:key/4bd92492-cc3b-4eed-8e08-3d8272303471"
  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "PROJECT"
    Name = "ICECONOMICAL_SIT01_GUIDEWIRE02_SUITE_LOGS_EBS"
    Docker = "True"
  }
}

resource "aws_volume_attachment" "ICECONOMICAL_SIT01_GUIDEWIRE03_SUITE_DOCKER_EBS_ATTACHMENT" {
  device_name = "/dev/sdh"
  volume_id = "${aws_ebs_volume.ICECONOMICAL_SIT01_GUIDEWIRE03_SUITE_DOCKER_EBS.id}"
  instance_id = "${aws_instance.ICECONOMICAL_SIT01_GUIDEWIRE03_SUITE.id}"
}

resource "aws_ebs_volume" "ICECONOMICAL_SIT01_GUIDEWIRE03_SUITE_DOCKER_EBS" {
  availability_zone = "ca-central-1a"
  size = 150
  type = "gp3"
  encrypted = true
  kms_key_id = "arn:aws:kms:ca-central-1:632109065501:key/4bd92492-cc3b-4eed-8e08-3d8272303471"
  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "PROJECT"
    Name = "ICECONOMICAL_SIT01_GUIDEWIRE03_SUITE_DOCKER_EBS"
    Docker = "True"
  }
}

resource "aws_volume_attachment" "ICECONOMICAL_SIT01_GUIDEWIRE03_SUITE_LOGS_EBS_ATTACHMENT" {
  device_name = "/dev/sdi"
  volume_id = "${aws_ebs_volume.ICECONOMICAL_SIT01_GUIDEWIRE03_SUITE_LOGS_EBS.id}"
  instance_id = "${aws_instance.ICECONOMICAL_SIT01_GUIDEWIRE03_SUITE.id}"
}

resource "aws_ebs_volume" "ICECONOMICAL_SIT01_GUIDEWIRE03_SUITE_LOGS_EBS" {
  availability_zone = "ca-central-1a"
  size = 150
  type = "gp3"
  encrypted = true
  kms_key_id = "arn:aws:kms:ca-central-1:632109065501:key/4bd92492-cc3b-4eed-8e08-3d8272303471"
  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "PROJECT"
    Name = "ICECONOMICAL_SIT01_GUIDEWIRE03_SUITE_LOGS_EBS"
    Docker = "True"
  }
}

resource "aws_volume_attachment" "ICECONOMICAL_SIT01_GUIDEWIRE04_SUITE_DOCKER_EBS_ATTACHMENT" {
  device_name = "/dev/sdh"
  volume_id = "${aws_ebs_volume.ICECONOMICAL_SIT01_GUIDEWIRE04_SUITE_DOCKER_EBS.id}"
  instance_id = "${aws_instance.ICECONOMICAL_SIT01_GUIDEWIRE04_SUITE.id}"
}

resource "aws_ebs_volume" "ICECONOMICAL_SIT01_GUIDEWIRE04_SUITE_DOCKER_EBS" {
  availability_zone = "ca-central-1b"
  size = 150
  type = "gp3"
  encrypted = true
  kms_key_id = "arn:aws:kms:ca-central-1:632109065501:key/4bd92492-cc3b-4eed-8e08-3d8272303471"
  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "PROJECT"
    Name = "ICECONOMICAL_SIT01_GUIDEWIRE04_SUITE_DOCKER_EBS"
    Docker = "True"
  }
}

resource "aws_volume_attachment" "ICECONOMICAL_SIT01_GUIDEWIRE04_SUITE_LOGS_EBS_ATTACHMENT" {
  device_name = "/dev/sdi"
  volume_id = "${aws_ebs_volume.ICECONOMICAL_SIT01_GUIDEWIRE04_SUITE_LOGS_EBS.id}"
  instance_id = "${aws_instance.ICECONOMICAL_SIT01_GUIDEWIRE04_SUITE.id}"
}

resource "aws_ebs_volume" "ICECONOMICAL_SIT01_GUIDEWIRE04_SUITE_LOGS_EBS" {
  availability_zone = "ca-central-1b"
  size = 150
  type = "gp3"
  encrypted = true
  kms_key_id = "arn:aws:kms:ca-central-1:632109065501:key/4bd92492-cc3b-4eed-8e08-3d8272303471"
  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "PROJECT"
    Name = "ICECONOMICAL_SIT01_GUIDEWIRE04_SUITE_LOGS_EBS"
    Docker = "True"
  }
}

############ Microservices Volumes ##################
resource "aws_volume_attachment" "ICECONOMICAL_SIT01_MICROSERVICES01_DOCKER_EBS_ATTACHMENT" {
  device_name = "/dev/sdh"
  volume_id = "${aws_ebs_volume.ICECONOMICAL_SIT01_MICROSERVICES01_DOCKER_EBS.id}"
  instance_id = "${aws_instance.ICECONOMICAL_SIT01_MICROSERVICES_APPLICATION01.id}"
}

resource "aws_ebs_volume" "ICECONOMICAL_SIT01_MICROSERVICES01_DOCKER_EBS" {
  availability_zone = "ca-central-1a"
  size = 50
  type = "gp3"
  encrypted = true
  kms_key_id = "arn:aws:kms:ca-central-1:632109065501:key/4bd92492-cc3b-4eed-8e08-3d8272303471"
  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "PROJECT"
    Name = "ICECONOMICAL_SIT01_MICROSERVICES01_DOCKER_EBS"
    Docker = "True"
  }
}

resource "aws_volume_attachment" "ICECONOMICAL_SIT01_MICROSERVICES02_DOCKER_EBS_ATTACHMENT" {
  device_name = "/dev/sdh"
  volume_id = "${aws_ebs_volume.ICECONOMICAL_SIT01_MICROSERVICES02_DOCKER_EBS.id}"
  instance_id = "${aws_instance.ICECONOMICAL_SIT01_MICROSERVICES_APPLICATION02.id}"
}

resource "aws_ebs_volume" "ICECONOMICAL_SIT01_MICROSERVICES02_DOCKER_EBS" {
  availability_zone = "ca-central-1b"
  size = 50
  type = "gp3"
  encrypted = true
  kms_key_id = "arn:aws:kms:ca-central-1:632109065501:key/4bd92492-cc3b-4eed-8e08-3d8272303471"
  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "PROJECT"
    Name = "ICECONOMICAL_SIT01_MICROSERVICES02_DOCKER_EBS"
    Docker = "True"
  }
}

############ Internal HAProxy Volumes ##################
resource "aws_volume_attachment" "ICECONOMICAL_SIT01_INTERNAL_HAPROXY_DOCKER_EBS_ATTACHMENT01" {
  device_name = "/dev/sdh"
  volume_id = "${aws_ebs_volume.ICECONOMICAL_SIT01_INTERNAL_HAPROXY_DOCKER_EBS01.id}"
  instance_id = "${aws_instance.ICECONOMICAL_SIT01_INTERNAL_HAPROXY01.id}"
}

resource "aws_ebs_volume" "ICECONOMICAL_SIT01_INTERNAL_HAPROXY_DOCKER_EBS01" {
  availability_zone = "ca-central-1a"
  size = 20
  type = "gp3"
  encrypted = true
  kms_key_id = "arn:aws:kms:ca-central-1:632109065501:key/4bd92492-cc3b-4eed-8e08-3d8272303471"
  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "PROJECT"
    Name = "ICECONOMICAL_SIT01_INTERNAL_HAPROXY_DOCKER_EBS01"
    Docker = "True"
  }
}

resource "aws_volume_attachment" "ICECONOMICAL_SIT01_INTERNAL_HAPROXY_DOCKER_EBS_ATTACHMENT02" {
  device_name = "/dev/sdh"
  volume_id = "${aws_ebs_volume.ICECONOMICAL_SIT01_INTERNAL_HAPROXY_DOCKER_EBS02.id}"
  instance_id = "${aws_instance.ICECONOMICAL_SIT01_INTERNAL_HAPROXY02.id}"
}

resource "aws_ebs_volume" "ICECONOMICAL_SIT01_INTERNAL_HAPROXY_DOCKER_EBS02" {
  availability_zone = "ca-central-1b"
  size = 20
  type = "gp3"
  encrypted = true
  kms_key_id = "arn:aws:kms:ca-central-1:632109065501:key/4bd92492-cc3b-4eed-8e08-3d8272303471"
  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "PROJECT"
    Name = "ICECONOMICAL_SIT01_INTERNAL_HAPROXY_DOCKER_EBS02"
    Docker = "True"
  }
}


# #=======================================================================
# # PPS01
# #=======================================================================

####### GW VOLUMES #############
resource "aws_volume_attachment" "ICECONOMICAL_PPS01_GUIDEWIRE01_SUITE_DOCKER_EBS_ATTACHMENT" {
  device_name = "/dev/sdh"
  volume_id = "${aws_ebs_volume.ICECONOMICAL_PPS01_GUIDEWIRE01_SUITE_DOCKER_EBS.id}"
  instance_id = "${aws_instance.ICECONOMICAL_PPS01_GUIDEWIRE01_SUITE.id}"
}

resource "aws_ebs_volume" "ICECONOMICAL_PPS01_GUIDEWIRE01_SUITE_DOCKER_EBS" {
  availability_zone = "ca-central-1a"
  size = 150
  type = "gp3"
  encrypted = true
  kms_key_id = "arn:aws:kms:ca-central-1:632109065501:key/4bd92492-cc3b-4eed-8e08-3d8272303471"
  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "MANAGED_SERVICES"
    Name = "ICECONOMICAL_PPS01_GUIDEWIRE01_SUITE_DOCKER_EBS"
    Docker = "True"
  }
}

resource "aws_volume_attachment" "ICECONOMICAL_PPS01_GUIDEWIRE01_SUITE_LOGS_EBS_ATTACHMENT" {
  device_name = "/dev/sdi"
  volume_id = "${aws_ebs_volume.ICECONOMICAL_PPS01_GUIDEWIRE01_SUITE_LOGS_EBS.id}"
  instance_id = "${aws_instance.ICECONOMICAL_PPS01_GUIDEWIRE01_SUITE.id}"
}

resource "aws_ebs_volume" "ICECONOMICAL_PPS01_GUIDEWIRE01_SUITE_LOGS_EBS" {
  availability_zone = "ca-central-1a"
  size = 150
  type = "gp3"
  encrypted = true
  kms_key_id = "arn:aws:kms:ca-central-1:632109065501:key/4bd92492-cc3b-4eed-8e08-3d8272303471"
  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "MANAGED_SERVICES"
    Name = "ICECONOMICAL_PPS01_GUIDEWIRE01_SUITE_LOGS_EBS"
    Docker = "True"
  }
}

resource "aws_volume_attachment" "ICECONOMICAL_PPS01_GUIDEWIRE02_SUITE_DOCKER_EBS_ATTACHMENT" {
  device_name = "/dev/sdh"
  volume_id = "${aws_ebs_volume.ICECONOMICAL_PPS01_GUIDEWIRE02_SUITE_DOCKER_EBS.id}"
  instance_id = "${aws_instance.ICECONOMICAL_PPS01_GUIDEWIRE02_SUITE.id}"
}

resource "aws_ebs_volume" "ICECONOMICAL_PPS01_GUIDEWIRE02_SUITE_DOCKER_EBS" {
  availability_zone = "ca-central-1b"
  size = 150
  type = "gp3"
  encrypted = true
  kms_key_id = "arn:aws:kms:ca-central-1:632109065501:key/4bd92492-cc3b-4eed-8e08-3d8272303471"
  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "MANAGED_SERVICES"
    Name = "ICECONOMICAL_PPS01_GUIDEWIRE02_SUITE_DOCKER_EBS"
    Docker = "True"
  }
}

resource "aws_volume_attachment" "ICECONOMICAL_PPS01_GUIDEWIRE02_SUITE_LOGS_EBS_ATTACHMENT" {
  device_name = "/dev/sdi"
  volume_id = "${aws_ebs_volume.ICECONOMICAL_PPS01_GUIDEWIRE02_SUITE_LOGS_EBS.id}"
  instance_id = "${aws_instance.ICECONOMICAL_PPS01_GUIDEWIRE02_SUITE.id}"
}

resource "aws_ebs_volume" "ICECONOMICAL_PPS01_GUIDEWIRE02_SUITE_LOGS_EBS" {
  availability_zone = "ca-central-1b"
  size = 150
  type = "gp3"
  encrypted = true
  kms_key_id = "arn:aws:kms:ca-central-1:632109065501:key/4bd92492-cc3b-4eed-8e08-3d8272303471"
  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "MANAGED_SERVICES"
    Name = "ICECONOMICAL_PPS01_GUIDEWIRE02_SUITE_LOGS_EBS"
    Docker = "True"
  }
}

resource "aws_volume_attachment" "ICECONOMICAL_PPS01_GUIDEWIRE03_SUITE_DOCKER_EBS_ATTACHMENT" {
  device_name = "/dev/sdh"
  volume_id = "${aws_ebs_volume.ICECONOMICAL_PPS01_GUIDEWIRE03_SUITE_DOCKER_EBS.id}"
  instance_id = "${aws_instance.ICECONOMICAL_PPS01_GUIDEWIRE03_SUITE.id}"
}

resource "aws_ebs_volume" "ICECONOMICAL_PPS01_GUIDEWIRE03_SUITE_DOCKER_EBS" {
  availability_zone = "ca-central-1a"
  size = 150
  type = "gp3"
  encrypted = true
  kms_key_id = "arn:aws:kms:ca-central-1:632109065501:key/4bd92492-cc3b-4eed-8e08-3d8272303471"
  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "MANAGED_SERVICES"
    Name = "ICECONOMICAL_PPS01_GUIDEWIRE03_SUITE_DOCKER_EBS"
    Docker = "True"
  }
}

resource "aws_volume_attachment" "ICECONOMICAL_PPS01_GUIDEWIRE03_SUITE_LOGS_EBS_ATTACHMENT" {
  device_name = "/dev/sdi"
  volume_id = "${aws_ebs_volume.ICECONOMICAL_PPS01_GUIDEWIRE03_SUITE_LOGS_EBS.id}"
  instance_id = "${aws_instance.ICECONOMICAL_PPS01_GUIDEWIRE03_SUITE.id}"
}

resource "aws_ebs_volume" "ICECONOMICAL_PPS01_GUIDEWIRE03_SUITE_LOGS_EBS" {
  availability_zone = "ca-central-1a"
  size = 150
  type = "gp3"
  encrypted = true
  kms_key_id = "arn:aws:kms:ca-central-1:632109065501:key/4bd92492-cc3b-4eed-8e08-3d8272303471"
  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "MANAGED_SERVICES"
    Name = "ICECONOMICAL_PPS01_GUIDEWIRE03_SUITE_LOGS_EBS"
    Docker = "True"
  }
}

resource "aws_volume_attachment" "ICECONOMICAL_PPS01_GUIDEWIRE04_SUITE_DOCKER_EBS_ATTACHMENT" {
  device_name = "/dev/sdh"
  volume_id = "${aws_ebs_volume.ICECONOMICAL_PPS01_GUIDEWIRE04_SUITE_DOCKER_EBS.id}"
  instance_id = "${aws_instance.ICECONOMICAL_PPS01_GUIDEWIRE04_SUITE.id}"
}

resource "aws_ebs_volume" "ICECONOMICAL_PPS01_GUIDEWIRE04_SUITE_DOCKER_EBS" {
  availability_zone = "ca-central-1b"
  size = 150
  type = "gp3"
  encrypted = true
  kms_key_id = "arn:aws:kms:ca-central-1:632109065501:key/4bd92492-cc3b-4eed-8e08-3d8272303471"
  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "MANAGED_SERVICES"
    Name = "ICECONOMICAL_PPS01_GUIDEWIRE04_SUITE_DOCKER_EBS"
    Docker = "True"
  }
}

resource "aws_volume_attachment" "ICECONOMICAL_PPS01_GUIDEWIRE04_SUITE_LOGS_EBS_ATTACHMENT" {
  device_name = "/dev/sdi"
  volume_id = "${aws_ebs_volume.ICECONOMICAL_PPS01_GUIDEWIRE04_SUITE_LOGS_EBS.id}"
  instance_id = "${aws_instance.ICECONOMICAL_PPS01_GUIDEWIRE04_SUITE.id}"
}

resource "aws_ebs_volume" "ICECONOMICAL_PPS01_GUIDEWIRE04_SUITE_LOGS_EBS" {
  availability_zone = "ca-central-1b"
  size = 150
  type = "gp3"
  encrypted = true
  kms_key_id = "arn:aws:kms:ca-central-1:632109065501:key/4bd92492-cc3b-4eed-8e08-3d8272303471"
  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "MANAGED_SERVICES"
    Name = "ICECONOMICAL_PPS01_GUIDEWIRE04_SUITE_LOGS_EBS"
    Docker = "True"
  }
}

############ Microservices Volumes ##################
resource "aws_volume_attachment" "ICECONOMICAL_PPS01_MICROSERVICES01_DOCKER_EBS_ATTACHMENT" {
  device_name = "/dev/sdh"
  volume_id = "${aws_ebs_volume.ICECONOMICAL_PPS01_MICROSERVICES01_DOCKER_EBS.id}"
  instance_id = "${aws_instance.ICECONOMICAL_PPS01_MICROSERVICES_APPLICATION01.id}"
}

resource "aws_ebs_volume" "ICECONOMICAL_PPS01_MICROSERVICES01_DOCKER_EBS" {
  availability_zone = "ca-central-1a"
  size = 50
  type = "gp3"
  encrypted = true
  kms_key_id = "arn:aws:kms:ca-central-1:632109065501:key/4bd92492-cc3b-4eed-8e08-3d8272303471"
  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "MANAGED_SERVICES"
    Name = "ICECONOMICAL_PPS01_MICROSERVICES01_DOCKER_EBS"
    Docker = "True"
  }
}

resource "aws_volume_attachment" "ICECONOMICAL_PPS01_MICROSERVICES02_DOCKER_EBS_ATTACHMENT" {
  device_name = "/dev/sdh"
  volume_id = "${aws_ebs_volume.ICECONOMICAL_PPS01_MICROSERVICES02_DOCKER_EBS.id}"
  instance_id = "${aws_instance.ICECONOMICAL_PPS01_MICROSERVICES_APPLICATION02.id}"
}

resource "aws_ebs_volume" "ICECONOMICAL_PPS01_MICROSERVICES02_DOCKER_EBS" {
  availability_zone = "ca-central-1b"
  size = 50
  type = "gp3"
  encrypted = true
  kms_key_id = "arn:aws:kms:ca-central-1:632109065501:key/4bd92492-cc3b-4eed-8e08-3d8272303471"
  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "MANAGED_SERVICES"
    Name = "ICECONOMICAL_PPS01_MICROSERVICES02_DOCKER_EBS"
    Docker = "True"
  }
}

############ Internal HAProxy Volumes ##################
resource "aws_volume_attachment" "ICECONOMICAL_PPS01_INTERNAL_HAPROXY_DOCKER_EBS_ATTACHMENT01" {
  device_name = "/dev/sdh"
  volume_id = "${aws_ebs_volume.ICECONOMICAL_PPS01_INTERNAL_HAPROXY_DOCKER_EBS01.id}"
  instance_id = "${aws_instance.ICECONOMICAL_PPS01_INTERNAL_HAPROXY01.id}"
}

resource "aws_ebs_volume" "ICECONOMICAL_PPS01_INTERNAL_HAPROXY_DOCKER_EBS01" {
  availability_zone = "ca-central-1a"
  size = 20
  type = "gp3"
  encrypted = true
  kms_key_id = "arn:aws:kms:ca-central-1:632109065501:key/4bd92492-cc3b-4eed-8e08-3d8272303471"
  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "MANAGED_SERVICES"
    Name = "ICECONOMICAL_PPS01_INTERNAL_HAPROXY_DOCKER_EBS01"
    Docker = "True"
  }
}

resource "aws_volume_attachment" "ICECONOMICAL_PPS01_INTERNAL_HAPROXY_DOCKER_EBS_ATTACHMENT02" {
  device_name = "/dev/sdh"
  volume_id = "${aws_ebs_volume.ICECONOMICAL_PPS01_INTERNAL_HAPROXY_DOCKER_EBS02.id}"
  instance_id = "${aws_instance.ICECONOMICAL_PPS01_INTERNAL_HAPROXY02.id}"
}

resource "aws_ebs_volume" "ICECONOMICAL_PPS01_INTERNAL_HAPROXY_DOCKER_EBS02" {
  availability_zone = "ca-central-1b"
  size = 20
  type = "gp3"
  encrypted = true
  kms_key_id = "arn:aws:kms:ca-central-1:632109065501:key/4bd92492-cc3b-4eed-8e08-3d8272303471"
  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "MANAGED_SERVICES"
    Name = "ICECONOMICAL_PPS01_INTERNAL_HAPROXY_DOCKER_EBS02"
    Docker = "True"
  }
}


# #=======================================================================
# # DEV
# #=======================================================================
############ Jenkins Client ##################
resource "aws_volume_attachment" "ICECONOMICAL_DEV_JENKINS_CLIENT_DOCKER_EBS_ATTACHMENT" {
  device_name = "/dev/sdh"
  volume_id = "${aws_ebs_volume.ICECONOMICAL_DEV_JENKINS_CLIENT_DOCKER_EBS.id}"
  instance_id = "${aws_instance.ICECONOMICAL_DEV_JENKINS_CLIENT.id}"
}

resource "aws_ebs_volume" "ICECONOMICAL_DEV_JENKINS_CLIENT_DOCKER_EBS" {
  availability_zone = "${aws_instance.ICECONOMICAL_DEV_JENKINS_CLIENT.availability_zone}"
  size = 50
  type = "gp3"
  encrypted = true
  kms_key_id = "arn:aws:kms:ca-central-1:632109065501:key/4bd92492-cc3b-4eed-8e08-3d8272303471"

  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "MANAGED_SERVICES"
    Name = "ICECONOMICAL_DEV_JENKINS_CLIENT_DOCKER_EBS"
    Docker = "True"
  }
}
