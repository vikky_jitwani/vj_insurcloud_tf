# #=======================================================================
# # DEV01
# #=======================================================================

######### GUIDEWIRE ##############
resource "aws_route53_record" "ICECONOMICAL_DEV01_GUIDEWIRE_SUITE_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "gw1.dev01.economical.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICECONOMICAL_DEV01_GUIDEWIRE_SUITE_PRIVATEIP}"]
}

resource "aws_route53_record" "ICECONOMICAL_DEV01_INTERNAL_NLB_GUIDEWIRE_RECORD_1" {
    zone_id = "${var.zone_id}"
    name    = "gw.dev01.economical.insurcloud.ca"
    type    = "A"
    alias {
        name                   = "${data.terraform_remote_state.dev_remote_state.ICECONOMICAL_DEV01_INTERNAL_NLB_DNS_NAME}"
        zone_id                = "${data.terraform_remote_state.dev_remote_state.ICECONOMICAL_DEV01_INTERNAL_NLB_ZONE_ID}"
        evaluate_target_health = false
    }
}

resource "aws_route53_record" "ICECONOMICAL_DEV01_INTERNAL_NLB_GUIDEWIRE_RECORD_2" {
    zone_id = "${var.zone_id}"
    name    = "gw-dev01.economical.insurcloud.ca"
    type    = "A"
    alias {
        name                   = "${data.terraform_remote_state.dev_remote_state.ICECONOMICAL_DEV01_INTERNAL_NLB_DNS_NAME}"
        zone_id                = "${data.terraform_remote_state.dev_remote_state.ICECONOMICAL_DEV01_INTERNAL_NLB_ZONE_ID}"
        evaluate_target_health = false
    }
}

######### MICROSERVICES ###########
resource "aws_route53_record" "ICECONOMICAL_DEV01_MICROSERVICES_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "ms1.dev01.economical.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICECONOMICAL_DEV01_MICROSERVICES_PRIVATEIP}"]
}

resource "aws_route53_record" "ICECONOMICAL_DEV01_INTERNAL_NLB_MICROSERVICES_RECORD_1" {
    zone_id = "${var.zone_id}"
    name    = "ms.dev01.economical.insurcloud.ca"
    type    = "A"
    alias {
        name                   = "${data.terraform_remote_state.dev_remote_state.ICECONOMICAL_DEV01_INTERNAL_NLB_DNS_NAME}"
        zone_id                = "${data.terraform_remote_state.dev_remote_state.ICECONOMICAL_DEV01_INTERNAL_NLB_ZONE_ID}"
        evaluate_target_health = false
    }
}

resource "aws_route53_record" "ICECONOMICAL_DEV01_INTERNAL_NLB_MICROSERVICES_RECORD_2" {
    zone_id = "${var.zone_id}"
    name    = "ms-dev01.economical.insurcloud.ca"
    type    = "A"
    alias {
        name                   = "${data.terraform_remote_state.dev_remote_state.ICECONOMICAL_DEV01_INTERNAL_NLB_DNS_NAME}"
        zone_id                = "${data.terraform_remote_state.dev_remote_state.ICECONOMICAL_DEV01_INTERNAL_NLB_ZONE_ID}"
        evaluate_target_health = false
    }
}

######### RPA ##############
resource "aws_route53_record" "ICECONOMICAL_DEV01_RPA_APP_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "rpa-app.dev01.economical.insurcloud.ca"
    ttl     = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICECONOMICAL_DEV01_RPA_APP_PRIVATEIP}"]
}

resource "aws_route53_record" "ICECONOMICAL_DEV01_RPA_DB_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "rpa-db.dev01.economical.insurcloud.ca"
    ttl     = "300"
    type    = "CNAME"
    records = ["${data.terraform_remote_state.dev_remote_state.ICECONOMICAL_DEV01_RPA_DB_PRIVATEIP}"]
}

resource "aws_route53_record" "ICECONOMICAL_DEV01_RPA_CLIENT01_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "rpa-client01.dev01.economical.insurcloud.ca"
    ttl     = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICECONOMICAL_DEV01_RPA_CLIENT01_PRIVATEIP}"]
}

resource "aws_route53_record" "ICECONOMICAL_DEV01_RPA_CLIENT02_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "rpa-client02.dev01.economical.insurcloud.ca"
    ttl     = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICECONOMICAL_DEV01_RPA_CLIENT02_PRIVATEIP}"]
}

######### HAPROXY ##############
resource "aws_route53_record" "ICECONOMICAL_DEV01_INTERNAL_HAPROXY_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "inthap.dev01.economical.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICECONOMICAL_DEV01_INTERNAL_HAPROXY_PRIVATEIP}"]
}

resource "aws_route53_record" "ICECONOMICAL_DEV01_EXTERNAL_HAPROXY_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "exthap.dev01.economical.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICECONOMICAL_DEV01_EXTERNAL_HAPROXY_PRIVATEIP}"]
}


# #=======================================================================
# # DEV02
# #=======================================================================

######### GUIDEWIRE ##############
resource "aws_route53_record" "ICECONOMICAL_DEV02_GUIDEWIRE_SUITE_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "gw1.dev02.economical.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICECONOMICAL_DEV02_GUIDEWIRE_SUITE_PRIVATEIP}"]
}

resource "aws_route53_record" "ICECONOMICAL_DEV02_INTERNAL_NLB_GUIDEWIRE_RECORD_1" {
    zone_id = "${var.zone_id}"
    name    = "gw.dev02.economical.insurcloud.ca"
    type    = "A"
    alias {
        name                   = "${data.terraform_remote_state.dev_remote_state.ICECONOMICAL_DEV02_INTERNAL_NLB_DNS_NAME}"
        zone_id                = "${data.terraform_remote_state.dev_remote_state.ICECONOMICAL_DEV02_INTERNAL_NLB_ZONE_ID}"
        evaluate_target_health = false
    }
}

resource "aws_route53_record" "ICECONOMICAL_DEV02_INTERNAL_NLB_GUIDEWIRE_RECORD_2" {
    zone_id = "${var.zone_id}"
    name    = "gw-dev02.economical.insurcloud.ca"
    type    = "A"
    alias {
        name                   = "${data.terraform_remote_state.dev_remote_state.ICECONOMICAL_DEV02_INTERNAL_NLB_DNS_NAME}"
        zone_id                = "${data.terraform_remote_state.dev_remote_state.ICECONOMICAL_DEV02_INTERNAL_NLB_ZONE_ID}"
        evaluate_target_health = false
    }
}

######### MICROSERVICES ###########
resource "aws_route53_record" "ICECONOMICAL_DEV02_MICROSERVICES_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "ms1.dev02.economical.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICECONOMICAL_DEV02_MICROSERVICES_PRIVATEIP}"]
}

resource "aws_route53_record" "ICECONOMICAL_DEV02_INTERNAL_NLB_MICROSERVICES_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "ms.dev02.economical.insurcloud.ca"
    type    = "A"
    alias {
        name                   = "${data.terraform_remote_state.dev_remote_state.ICECONOMICAL_DEV02_INTERNAL_NLB_DNS_NAME}"
        zone_id                = "${data.terraform_remote_state.dev_remote_state.ICECONOMICAL_DEV02_INTERNAL_NLB_ZONE_ID}"
        evaluate_target_health = false
    }
}

resource "aws_route53_record" "ICECONOMICAL_DEV02_INTERNAL_NLB_MICROSERVICES_RECORD02" {
    zone_id = "${var.zone_id}"
    name    = "ms-dev02.economical.insurcloud.ca"
    type    = "A"
    alias {
        name                   = "${data.terraform_remote_state.dev_remote_state.ICECONOMICAL_DEV02_INTERNAL_NLB_DNS_NAME}"
        zone_id                = "${data.terraform_remote_state.dev_remote_state.ICECONOMICAL_DEV02_INTERNAL_NLB_ZONE_ID}"
        evaluate_target_health = false
    }
}

######### RPA ##############
resource "aws_route53_record" "ICECONOMICAL_DEV02_RPA_APP_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "rpa-app.dev02.economical.insurcloud.ca"
    ttl     = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICECONOMICAL_DEV02_RPA_APP_PRIVATEIP}"]
}

resource "aws_route53_record" "ICECONOMICAL_DEV02_RPA_DB_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "rpa-db.dev02.economical.insurcloud.ca"
    ttl     = "300"
    type    = "CNAME"
    records = ["${data.terraform_remote_state.dev_remote_state.ICECONOMICAL_DEV02_RPA_DB_PRIVATEIP}"]
}

resource "aws_route53_record" "ICECONOMICAL_DEV02_RPA_CLIENT01_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "rpa-client01.dev02.economical.insurcloud.ca"
    ttl     = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICECONOMICAL_DEV02_RPA_CLIENT01_PRIVATEIP}"]
}

resource "aws_route53_record" "ICECONOMICAL_DEV02_RPA_CLIENT02_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "rpa-client02.dev02.economical.insurcloud.ca"
    ttl     = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICECONOMICAL_DEV02_RPA_CLIENT02_PRIVATEIP}"]
}

######### HAPROXY ##############
resource "aws_route53_record" "ICECONOMICAL_DEV02_INTERNAL_HAPROXY_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "inthap.dev02.economical.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICECONOMICAL_DEV02_INTERNAL_HAPROXY_PRIVATEIP}"]
}


# #=======================================================================
# # ST01
# #=======================================================================

######### GUIDEWIRE ##############
resource "aws_route53_record" "ICECONOMICAL_ST01_GUIDEWIRE_SUITE_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "gw1.st01.economical.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICECONOMICAL_ST01_GUIDEWIRE_SUITE_PRIVATEIP}"]
}

resource "aws_route53_record" "ICECONOMICAL_ST01_INTERNAL_NLB_GUIDEWIRE_RECORD_1" {
    zone_id = "${var.zone_id}"
    name    = "gw.st01.economical.insurcloud.ca"
    type    = "A"
    alias {
        name                   = "${data.terraform_remote_state.dev_remote_state.ICECONOMICAL_ST01_INTERNAL_NLB_DNS_NAME}"
        zone_id                = "${data.terraform_remote_state.dev_remote_state.ICECONOMICAL_ST01_INTERNAL_NLB_ZONE_ID}"
        evaluate_target_health = false
    }
}

resource "aws_route53_record" "ICECONOMICAL_ST01_INTERNAL_NLB_GUIDEWIRE_RECORD_2" {
    zone_id = "${var.zone_id}"
    name    = "gw-st01.economical.insurcloud.ca"
    type    = "A"
    alias {
        name                   = "${data.terraform_remote_state.dev_remote_state.ICECONOMICAL_ST01_INTERNAL_NLB_DNS_NAME}"
        zone_id                = "${data.terraform_remote_state.dev_remote_state.ICECONOMICAL_ST01_INTERNAL_NLB_ZONE_ID}"
        evaluate_target_health = false
    }
}

######### MICROSERVICES ###########
resource "aws_route53_record" "ICECONOMICAL_ST01_MICROSERVICES_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "ms1.st01.economical.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICECONOMICAL_ST01_MICROSERVICES_PRIVATEIP}"]
}

resource "aws_route53_record" "ICECONOMICAL_ST01_INTERNAL_NLB_MICROSERVICES_RECORD_1" {
    zone_id = "${var.zone_id}"
    name    = "ms.st01.economical.insurcloud.ca"
    type    = "A"
    alias {
        name                   = "${data.terraform_remote_state.dev_remote_state.ICECONOMICAL_ST01_INTERNAL_NLB_DNS_NAME}"
        zone_id                = "${data.terraform_remote_state.dev_remote_state.ICECONOMICAL_ST01_INTERNAL_NLB_ZONE_ID}"
        evaluate_target_health = false
    }
}

resource "aws_route53_record" "ICECONOMICAL_ST01_INTERNAL_NLB_MICROSERVICES_RECORD_2" {
    zone_id = "${var.zone_id}"
    name    = "ms-st01.economical.insurcloud.ca"
    type    = "A"
    alias {
        name                   = "${data.terraform_remote_state.dev_remote_state.ICECONOMICAL_ST01_INTERNAL_NLB_DNS_NAME}"
        zone_id                = "${data.terraform_remote_state.dev_remote_state.ICECONOMICAL_ST01_INTERNAL_NLB_ZONE_ID}"
        evaluate_target_health = false
    }
}

######### RPA ##############
resource "aws_route53_record" "ICECONOMICAL_ST01_RPA_APP_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "rpa-app.st01.economical.insurcloud.ca"
    ttl     = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICECONOMICAL_ST01_RPA_APP_PRIVATEIP}"]
}

resource "aws_route53_record" "ICECONOMICAL_ST01_RPA_DB_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "rpa-db.st01.economical.insurcloud.ca"
    ttl     = "300"
    type    = "CNAME"
    records = ["${data.terraform_remote_state.dev_remote_state.ICECONOMICAL_ST01_RPA_DB_PRIVATEIP}"]
}

resource "aws_route53_record" "ICECONOMICAL_ST01_RPA_CLIENT01_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "rpa-client01.st01.economical.insurcloud.ca"
    ttl     = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICECONOMICAL_ST01_RPA_CLIENT01_PRIVATEIP}"]
}

resource "aws_route53_record" "ICECONOMICAL_ST01_RPA_CLIENT02_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "rpa-client02.st01.economical.insurcloud.ca"
    ttl     = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICECONOMICAL_ST01_RPA_CLIENT02_PRIVATEIP}"]
}

######### HAPROXY ##############
resource "aws_route53_record" "ICECONOMICAL_ST01_INTERNAL_HAPROXY_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "inthap.st01.economical.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICECONOMICAL_ST01_INTERNAL_HAPROXY_PRIVATEIP}"]
}


# #=======================================================================
# # ST02 (PROXY OF DEV02)
# #=======================================================================

######### GUIDEWIRE ##############
resource "aws_route53_record" "ICECONOMICAL_ST02_GUIDEWIRE_SUITE_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "gw1.st02.economical.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICECONOMICAL_ST01_GUIDEWIRE_SUITE_PRIVATEIP}"]
}

resource "aws_route53_record" "ICECONOMICAL_ST02_INTERNAL_NLB_GUIDEWIRE_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "gw-st02.economical.insurcloud.ca"
    type    = "A"
    alias {
        name                   = "${data.terraform_remote_state.dev_remote_state.ICECONOMICAL_ST01_INTERNAL_NLB_DNS_NAME}"
        zone_id                = "${data.terraform_remote_state.dev_remote_state.ICECONOMICAL_ST01_INTERNAL_NLB_ZONE_ID}"
        evaluate_target_health = false
    }
}


# #=======================================================================
# # UAT01
# #=======================================================================

######### GUIDEWIRE ##############
resource "aws_route53_record" "ICECONOMICAL_UAT01_GUIDEWIRE_SUITE_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "gw1.uat01.economical.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICECONOMICAL_UAT01_GUIDEWIRE_SUITE_PRIVATEIP}"]
}

resource "aws_route53_record" "ICECONOMICAL_UAT01_INTERNAL_NLB_GUIDEWIRE_RECORD_1" {
    zone_id = "${var.zone_id}"
    name    = "gw.uat01.economical.insurcloud.ca"
    type    = "A"
    alias {
        name                   = "${data.terraform_remote_state.dev_remote_state.ICECONOMICAL_UAT01_INTERNAL_NLB_DNS_NAME}"
        zone_id                = "${data.terraform_remote_state.dev_remote_state.ICECONOMICAL_UAT01_INTERNAL_NLB_ZONE_ID}"
        evaluate_target_health = false
    }
}

resource "aws_route53_record" "ICECONOMICAL_UAT01_INTERNAL_NLB_GUIDEWIRE_RECORD_2" {
    zone_id = "${var.zone_id}"
    name    = "gw-uat01.economical.insurcloud.ca"
    type    = "A"
    alias {
        name                   = "${data.terraform_remote_state.dev_remote_state.ICECONOMICAL_UAT01_INTERNAL_NLB_DNS_NAME}"
        zone_id                = "${data.terraform_remote_state.dev_remote_state.ICECONOMICAL_UAT01_INTERNAL_NLB_ZONE_ID}"
        evaluate_target_health = false
    }
}

######### MICROSERVICES ###########
resource "aws_route53_record" "ICECONOMICAL_UAT01_MICROSERVICES_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "ms1.uat01.economical.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICECONOMICAL_UAT01_MICROSERVICES_PRIVATEIP}"]
}

resource "aws_route53_record" "ICECONOMICAL_UAT01_INTERNAL_NLB_MICROSERVICES_RECORD_1" {
    zone_id = "${var.zone_id}"
    name    = "ms.uat01.economical.insurcloud.ca"
    type    = "A"
    alias {
        name                   = "${data.terraform_remote_state.dev_remote_state.ICECONOMICAL_UAT01_INTERNAL_NLB_DNS_NAME}"
        zone_id                = "${data.terraform_remote_state.dev_remote_state.ICECONOMICAL_UAT01_INTERNAL_NLB_ZONE_ID}"
        evaluate_target_health = false
    }
}

resource "aws_route53_record" "ICECONOMICAL_UAT01_INTERNAL_NLB_MICROSERVICES_RECORD_2" {
    zone_id = "${var.zone_id}"
    name    = "ms-uat01.economical.insurcloud.ca"
    type    = "A"
    alias {
        name                   = "${data.terraform_remote_state.dev_remote_state.ICECONOMICAL_UAT01_INTERNAL_NLB_DNS_NAME}"
        zone_id                = "${data.terraform_remote_state.dev_remote_state.ICECONOMICAL_UAT01_INTERNAL_NLB_ZONE_ID}"
        evaluate_target_health = false
    }
}

######### RPA ##############
resource "aws_route53_record" "ICECONOMICAL_UAT01_RPA_WS_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "rpa-uat01.economical.insurcloud.ca"
    type    = "A"
    alias {
        name                   = "${data.terraform_remote_state.dev_remote_state.ICECONOMICAL_UAT01_RPA_NLB_DNS_NAME}"
        zone_id                = "${data.terraform_remote_state.dev_remote_state.ICECONOMICAL_UAT01_RPA_NLB_ZONE_ID}"
        evaluate_target_health = false
    }
}

resource "aws_route53_record" "ICECONOMICAL_UAT01_RPA_APP_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "rpa-app.uat01.economical.insurcloud.ca"
    ttl     = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICECONOMICAL_UAT01_RPA_APP_PRIVATEIP}"]
}

resource "aws_route53_record" "ICECONOMICAL_UAT01_RPA_DB_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "rpa-db.uat01.economical.insurcloud.ca"
    ttl     = "300"
    type    = "CNAME"
    records = ["${data.terraform_remote_state.dev_remote_state.ICECONOMICAL_UAT01_RPA_DB_PRIVATEIP}"]
}

resource "aws_route53_record" "ICECONOMICAL_UAT01_RPA_CLIENT01_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "rpa-client01.uat01.economical.insurcloud.ca"
    ttl     = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICECONOMICAL_UAT01_RPA_CLIENT01_PRIVATEIP}"]
}

resource "aws_route53_record" "ICECONOMICAL_UAT01_RPA_CLIENT02_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "rpa-client02.uat01.economical.insurcloud.ca"
    ttl     = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICECONOMICAL_UAT01_RPA_CLIENT02_PRIVATEIP}"]
}

resource "aws_route53_record" "ICECONOMICAL_UAT01_RPA_CLIENT03_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "rpa-client03.uat01.economical.insurcloud.ca"
    ttl     = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICECONOMICAL_UAT01_RPA_CLIENT03_PRIVATEIP}"]
}

resource "aws_route53_record" "ICECONOMICAL_UAT01_RPA_CLIENT04_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "rpa-client04.uat01.economical.insurcloud.ca"
    ttl     = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICECONOMICAL_UAT01_RPA_CLIENT04_PRIVATEIP}"]
}

resource "aws_route53_record" "ICECONOMICAL_UAT01_RPA_CLIENT05_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "rpa-client05.uat01.economical.insurcloud.ca"
    ttl     = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICECONOMICAL_UAT01_RPA_CLIENT05_PRIVATEIP}"]
}

resource "aws_route53_record" "ICECONOMICAL_UAT01_RPA_CLIENT06_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "rpa-client06.uat01.economical.insurcloud.ca"
    ttl     = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICECONOMICAL_UAT01_RPA_CLIENT06_PRIVATEIP}"]
}

######### HAPROXY ##############
resource "aws_route53_record" "ICECONOMICAL_UAT01_INTERNAL_HAPROXY_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "inthap.uat01.economical.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICECONOMICAL_UAT01_INTERNAL_HAPROXY_PRIVATEIP}"]
}

resource "aws_route53_record" "ICECONOMICAL_UAT01_INTERNAL_HAPROXY01_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "inthap01.uat01.economical.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICECONOMICAL_UAT01_INTERNAL_HAPROXY_PRIVATEIP}"]
}


# #=======================================================================
# # UAT02
# #=======================================================================

######### GUIDEWIRE ##############
resource "aws_route53_record" "ICECONOMICAL_UAT02_GUIDEWIRE_SUITE_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "gw1.uat02.economical.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICECONOMICAL_UAT01_GUIDEWIRE_SUITE_PRIVATEIP}"]
}

resource "aws_route53_record" "ICECONOMICAL_UAT02_INTERNAL_NLB_GUIDEWIRE_RECORD_1" {
    zone_id = "${var.zone_id}"
    name    = "gw.uat02.economical.insurcloud.ca"
    type    = "A"
    alias {
        name                   = "${data.terraform_remote_state.dev_remote_state.ICECONOMICAL_UAT01_INTERNAL_NLB_DNS_NAME}"
        zone_id                = "${data.terraform_remote_state.dev_remote_state.ICECONOMICAL_UAT01_INTERNAL_NLB_ZONE_ID}"
        evaluate_target_health = false
    }
}

resource "aws_route53_record" "ICECONOMICAL_UAT02_INTERNAL_NLB_GUIDEWIRE_RECORD_2" {
    zone_id = "${var.zone_id}"
    name    = "gw-uat02.economical.insurcloud.ca"
    type    = "A"
    alias {
        name                   = "${data.terraform_remote_state.dev_remote_state.ICECONOMICAL_UAT01_INTERNAL_NLB_DNS_NAME}"
        zone_id                = "${data.terraform_remote_state.dev_remote_state.ICECONOMICAL_UAT01_INTERNAL_NLB_ZONE_ID}"
        evaluate_target_health = false
    }
}


# #=======================================================================
# # PRF01
# #=======================================================================

######### GUIDEWIRE ##############
resource "aws_route53_record" "ICECONOMICAL_PRF01_GUIDEWIRE01_SUITE_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "gw1.prf01.economical.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICECONOMICAL_PRF01_GUIDEWIRE01_SUITE_PRIVATEIP}"]
}

resource "aws_route53_record" "ICECONOMICAL_PRF01_GUIDEWIRE02_SUITE_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "gw2.prf01.economical.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICECONOMICAL_PRF01_GUIDEWIRE02_SUITE_PRIVATEIP}"]
}

resource "aws_route53_record" "ICECONOMICAL_PRF01_GUIDEWIRE03_SUITE_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "gw3.prf01.economical.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICECONOMICAL_PRF01_GUIDEWIRE03_SUITE_PRIVATEIP}"]
}

resource "aws_route53_record" "ICECONOMICAL_PRF01_GUIDEWIRE04_SUITE_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "gw4.prf01.economical.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICECONOMICAL_PRF01_GUIDEWIRE04_SUITE_PRIVATEIP}"]
}

resource "aws_route53_record" "ICECONOMICAL_PRF01_GUIDEWIRE_DB_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "gwdb.prf01.economical.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICECONOMICAL_PRF01_GUIDEWIRE_DB_PRIVATEIP}"]
}

resource "aws_route53_record" "ICECONOMICAL_PRF01_INTERNAL_NLB_GUIDEWIRE_RECORD_1" {
    zone_id = "${var.zone_id}"
    name    = "gw.prf01.economical.insurcloud.ca"
    type    = "A"
    alias {
        name                   = "${data.terraform_remote_state.dev_remote_state.ICECONOMICAL_PRF01_INTERNAL_NLB_DNS_NAME}"
        zone_id                = "${data.terraform_remote_state.dev_remote_state.ICECONOMICAL_PRF01_INTERNAL_NLB_ZONE_ID}"
        evaluate_target_health = false
    }
}

resource "aws_route53_record" "ICECONOMICAL_PRF01_INTERNAL_NLB_GUIDEWIRE_RECORD_2" {
    zone_id = "${var.zone_id}"
    name    = "gw-prf01.economical.insurcloud.ca"
    type    = "A"
    alias {
        name                   = "${data.terraform_remote_state.dev_remote_state.ICECONOMICAL_PRF01_INTERNAL_NLB_DNS_NAME}"
        zone_id                = "${data.terraform_remote_state.dev_remote_state.ICECONOMICAL_PRF01_INTERNAL_NLB_ZONE_ID}"
        evaluate_target_health = false
    }
}

######### MICROSERVICES ###########
resource "aws_route53_record" "ICECONOMICAL_PRF01_MICROSERVICES_MANAGER01_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "msman1.prf01.economical.insurcloud.ca"
    ttl     = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICECONOMICAL_PRF01_MICROSERVICES_MANAGER01_PRIVATEIP}"]
}

resource "aws_route53_record" "ICECONOMICAL_PRF01_MICROSERVICES_MANAGER02_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "msman2.prf01.economical.insurcloud.ca"
    ttl     = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICECONOMICAL_PRF01_MICROSERVICES_MANAGER02_PRIVATEIP}"]
}

resource "aws_route53_record" "ICECONOMICAL_PRF01_MICROSERVICES_MANAGER03_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "msman3.prf01.economical.insurcloud.ca"
    ttl     = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICECONOMICAL_PRF01_MICROSERVICES_MANAGER03_PRIVATEIP}"]
}

resource "aws_route53_record" "ICECONOMICAL_PRF01_MICROSERVICES_APPLICATION01_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "ms1.prf01.economical.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICECONOMICAL_PRF01_MICROSERVICES_APPLICATION01_PRIVATEIP}"]
}

resource "aws_route53_record" "ICECONOMICAL_PRF01_MICROSERVICES_APPLICATION02_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "ms2.prf01.economical.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICECONOMICAL_PRF01_MICROSERVICES_APPLICATION02_PRIVATEIP}"]
}

resource "aws_route53_record" "ICECONOMICAL_PRF01_INTERNAL_NLB_MICROSERVICES_RECORD_1" {
    zone_id = "${var.zone_id}"
    name    = "ms.prf01.economical.insurcloud.ca"
    type    = "A"
    alias {
        name                   = "${data.terraform_remote_state.dev_remote_state.ICECONOMICAL_PRF01_INTERNAL_NLB_DNS_NAME}"
        zone_id                = "${data.terraform_remote_state.dev_remote_state.ICECONOMICAL_PRF01_INTERNAL_NLB_ZONE_ID}"
        evaluate_target_health = false
    }
}

resource "aws_route53_record" "ICECONOMICAL_PRF01_INTERNAL_NLB_MICROSERVICES_RECORD_2" {
    zone_id = "${var.zone_id}"
    name    = "ms-prf01.economical.insurcloud.ca"
    type    = "A"
    alias {
        name                   = "${data.terraform_remote_state.dev_remote_state.ICECONOMICAL_PRF01_INTERNAL_NLB_DNS_NAME}"
        zone_id                = "${data.terraform_remote_state.dev_remote_state.ICECONOMICAL_PRF01_INTERNAL_NLB_ZONE_ID}"
        evaluate_target_health = false
    }
}

######### RPA ##############
resource "aws_route53_record" "ICECONOMICAL_PRF01_RPA_WS_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "rpa-prf01.economical.insurcloud.ca"
    type    = "A"
    alias {
        name                   = "${data.terraform_remote_state.dev_remote_state.ICECONOMICAL_PRF01_RPA_NLB_DNS_NAME}"
        zone_id                = "${data.terraform_remote_state.dev_remote_state.ICECONOMICAL_PRF01_RPA_NLB_ZONE_ID}"
        evaluate_target_health = false
    }
}

resource "aws_route53_record" "ICECONOMICAL_PRF01_RPA_APP_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "rpa-app.prf01.economical.insurcloud.ca"
    ttl     = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICECONOMICAL_PRF01_RPA_APP_PRIVATEIP}"]
}

resource "aws_route53_record" "ICECONOMICAL_PRF01_RPA_INT_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "rpa-int.prf01.economical.insurcloud.ca"
    ttl     = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICECONOMICAL_PRF01_RPA_INT01_PRIVATEIP}"]
}

resource "aws_route53_record" "ICECONOMICAL_PRF01_RPA_DB_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "rpa-db.prf01.economical.insurcloud.ca"
    ttl     = "300"
    type    = "CNAME"
    records = ["${data.terraform_remote_state.dev_remote_state.ICECONOMICAL_PRF01_RPA_DB_PRIVATEIP}"]
}

resource "aws_route53_record" "ICECONOMICAL_PRF01_RPA_CLIENT01_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "rpa-client01.prf01.economical.insurcloud.ca"
    ttl     = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICECONOMICAL_PRF01_RPA_CLIENT01_PRIVATEIP}"]
}

resource "aws_route53_record" "ICECONOMICAL_PRF01_RPA_CLIENT02_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "rpa-client02.prf01.economical.insurcloud.ca"
    ttl     = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICECONOMICAL_PRF01_RPA_CLIENT02_PRIVATEIP}"]
}

resource "aws_route53_record" "ICECONOMICAL_PRF01_RPA_CLIENT03_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "rpa-client03.prf01.economical.insurcloud.ca"
    ttl     = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICECONOMICAL_PRF01_RPA_CLIENT03_PRIVATEIP}"]
}

resource "aws_route53_record" "ICECONOMICAL_PRF01_RPA_CLIENT04_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "rpa-client04.prf01.economical.insurcloud.ca"
    ttl     = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICECONOMICAL_PRF01_RPA_CLIENT04_PRIVATEIP}"]
}

resource "aws_route53_record" "ICECONOMICAL_PRF01_RPA_CLIENT05_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "rpa-client05.prf01.economical.insurcloud.ca"
    ttl     = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICECONOMICAL_PRF01_RPA_CLIENT05_PRIVATEIP}"]
}

resource "aws_route53_record" "ICECONOMICAL_PRF01_RPA_CLIENT06_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "rpa-client06.prf01.economical.insurcloud.ca"
    ttl     = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICECONOMICAL_PRF01_RPA_CLIENT06_PRIVATEIP}"]
}

######### HAPROXY ##############
resource "aws_route53_record" "ICECONOMICAL_PRF01_INTERNAL_HAPROXY_RECORD" { /* TODO: REMOVE THIS LATER */
    zone_id = "${var.zone_id}"
    name    = "inthap.prf01.economical.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICECONOMICAL_PRF01_INTERNAL_HAPROXY_PRIVATEIP01}"]
}

resource "aws_route53_record" "ICECONOMICAL_PRF01_INTERNAL_HAPROXY_RECORD01" {
    zone_id = "${var.zone_id}"
    name    = "inthap01.prf01.economical.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICECONOMICAL_PRF01_INTERNAL_HAPROXY_PRIVATEIP01}"]
}

resource "aws_route53_record" "ICECONOMICAL_PRF01_INTERNAL_HAPROXY_RECORD02" {
    zone_id = "${var.zone_id}"
    name    = "inthap02.prf01.economical.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICECONOMICAL_PRF01_INTERNAL_HAPROXY_PRIVATEIP02}"]
}

# #=======================================================================
# # SIT01
# #=======================================================================

######### GUIDEWIRE ##############
resource "aws_route53_record" "ICECONOMICAL_SIT01_GUIDEWIRE01_SUITE_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "gw1.sit01.economical.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICECONOMICAL_SIT01_GUIDEWIRE01_SUITE_PRIVATEIP}"]
}

resource "aws_route53_record" "ICECONOMICAL_SIT01_GUIDEWIRE02_SUITE_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "gw2.sit01.economical.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICECONOMICAL_SIT01_GUIDEWIRE02_SUITE_PRIVATEIP}"]
}

resource "aws_route53_record" "ICECONOMICAL_SIT01_GUIDEWIRE03_SUITE_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "gw3.sit01.economical.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICECONOMICAL_SIT01_GUIDEWIRE03_SUITE_PRIVATEIP}"]
}

resource "aws_route53_record" "ICECONOMICAL_SIT01_GUIDEWIRE04_SUITE_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "gw4.sit01.economical.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICECONOMICAL_SIT01_GUIDEWIRE04_SUITE_PRIVATEIP}"]
}

resource "aws_route53_record" "ICECONOMICAL_SIT01_GUIDEWIRE_DB_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "gwdb.sit01.economical.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICECONOMICAL_SIT01_GUIDEWIRE_DB_PRIVATEIP}"]
}

resource "aws_route53_record" "ICECONOMICAL_SIT01_INTERNAL_NLB_GUIDEWIRE_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "gw-sit01.economical.insurcloud.ca"
    type    = "A"
    alias {
        name                   = "${data.terraform_remote_state.dev_remote_state.ICECONOMICAL_SIT01_INTERNAL_NLB_DNS_NAME}"
        zone_id                = "${data.terraform_remote_state.dev_remote_state.ICECONOMICAL_SIT01_INTERNAL_NLB_ZONE_ID}"
        evaluate_target_health = false
    }
}

######### MICROSERVICES ###########
resource "aws_route53_record" "ICECONOMICAL_SIT01_MICROSERVICES_MANAGER01_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "msman1.sit01.economical.insurcloud.ca"
    ttl     = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICECONOMICAL_SIT01_MICROSERVICES_MANAGER01_PRIVATEIP}"]
}

resource "aws_route53_record" "ICECONOMICAL_SIT01_MICROSERVICES_MANAGER02_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "msman2.sit01.economical.insurcloud.ca"
    ttl     = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICECONOMICAL_SIT01_MICROSERVICES_MANAGER02_PRIVATEIP}"]
}

resource "aws_route53_record" "ICECONOMICAL_SIT01_MICROSERVICES_MANAGER03_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "msman3.sit01.economical.insurcloud.ca"
    ttl     = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICECONOMICAL_SIT01_MICROSERVICES_MANAGER03_PRIVATEIP}"]
}

resource "aws_route53_record" "ICECONOMICAL_SIT01_MICROSERVICES_APPLICATION01_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "ms1.sit01.economical.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICECONOMICAL_SIT01_MICROSERVICES_APPLICATION01_PRIVATEIP}"]
}

resource "aws_route53_record" "ICECONOMICAL_SIT01_MICROSERVICES_APPLICATION02_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "ms2.sit01.economical.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICECONOMICAL_SIT01_MICROSERVICES_APPLICATION02_PRIVATEIP}"]
}

resource "aws_route53_record" "ICECONOMICAL_SIT01_INTERNAL_NLB_MICROSERVICES_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "ms-sit01.economical.insurcloud.ca"
    type    = "A"
    alias {
        name                   = "${data.terraform_remote_state.dev_remote_state.ICECONOMICAL_SIT01_INTERNAL_NLB_DNS_NAME}"
        zone_id                = "${data.terraform_remote_state.dev_remote_state.ICECONOMICAL_SIT01_INTERNAL_NLB_ZONE_ID}"
        evaluate_target_health = false
    }
}

######### RPA ##############
resource "aws_route53_record" "ICECONOMICAL_SIT01_RPA_WS_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "rpa-sit01.economical.insurcloud.ca"
    type    = "A"
    alias {
        name                   = "${data.terraform_remote_state.dev_remote_state.ICECONOMICAL_SIT01_RPA_NLB_DNS_NAME}"
        zone_id                = "${data.terraform_remote_state.dev_remote_state.ICECONOMICAL_SIT01_RPA_NLB_ZONE_ID}"
        evaluate_target_health = false
    }
}

resource "aws_route53_record" "ICECONOMICAL_SIT01_RPA_APP01_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "rpa-app01.sit01.economical.insurcloud.ca"
    ttl     = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICECONOMICAL_SIT01_RPA_APP01_PRIVATEIP}"]
}

resource "aws_route53_record" "ICECONOMICAL_SIT01_RPA_APP02_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "rpa-app02.sit01.economical.insurcloud.ca"
    ttl     = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICECONOMICAL_SIT01_RPA_APP02_PRIVATEIP}"]
}

resource "aws_route53_record" "ICECONOMICAL_SIT01_RPA_DB_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "rpa-db.sit01.economical.insurcloud.ca"
    ttl     = "300"
    type    = "CNAME"
    records = ["${data.terraform_remote_state.dev_remote_state.ICECONOMICAL_SIT01_RPA_DB_PRIVATEIP}"]
}

resource "aws_route53_record" "ICECONOMICAL_SIT01_RPA_INT01_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "rpa-int01.sit01.economical.insurcloud.ca"
    ttl     = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICECONOMICAL_SIT01_RPA_INT01_PRIVATEIP}"]
}

resource "aws_route53_record" "ICECONOMICAL_SIT01_RPA_CLIENT01_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "rpa-client01.sit01.economical.insurcloud.ca"
    ttl     = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICECONOMICAL_SIT01_RPA_CLIENT01_PRIVATEIP}"]
}

resource "aws_route53_record" "ICECONOMICAL_SIT01_RPA_CLIENT02_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "rpa-client02.sit01.economical.insurcloud.ca"
    ttl     = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICECONOMICAL_SIT01_RPA_CLIENT02_PRIVATEIP}"]
}

resource "aws_route53_record" "ICECONOMICAL_SIT01_RPA_CLIENT03_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "rpa-client03.sit01.economical.insurcloud.ca"
    ttl     = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICECONOMICAL_SIT01_RPA_CLIENT03_PRIVATEIP}"]
}

resource "aws_route53_record" "ICECONOMICAL_SIT01_RPA_CLIENT04_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "rpa-client04.sit01.economical.insurcloud.ca"
    ttl     = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICECONOMICAL_SIT01_RPA_CLIENT04_PRIVATEIP}"]
}

resource "aws_route53_record" "ICECONOMICAL_SIT01_RPA_CLIENT05_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "rpa-client05.sit01.economical.insurcloud.ca"
    ttl     = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICECONOMICAL_SIT01_RPA_CLIENT05_PRIVATEIP}"]
}

resource "aws_route53_record" "ICECONOMICAL_SIT01_RPA_CLIENT06_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "rpa-client06.sit01.economical.insurcloud.ca"
    ttl     = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICECONOMICAL_SIT01_RPA_CLIENT06_PRIVATEIP}"]
}

######### HAPROXY ##############
resource "aws_route53_record" "ICECONOMICAL_SIT01_INTERNAL_HAPROXY_RECORD" { /* TODO: REMOVE THIS LATER */
    zone_id = "${var.zone_id}"
    name    = "inthap.sit01.economical.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICECONOMICAL_SIT01_INTERNAL_HAPROXY_PRIVATEIP01}"]
}

resource "aws_route53_record" "ICECONOMICAL_SIT01_INTERNAL_HAPROXY_RECORD01" {
    zone_id = "${var.zone_id}"
    name    = "inthap01.sit01.economical.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICECONOMICAL_SIT01_INTERNAL_HAPROXY_PRIVATEIP01}"]
}

resource "aws_route53_record" "ICECONOMICAL_SIT01_INTERNAL_HAPROXY_RECORD02" {
    zone_id = "${var.zone_id}"
    name    = "inthap02.sit01.economical.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICECONOMICAL_SIT01_INTERNAL_HAPROXY_PRIVATEIP02}"]
}


# #=======================================================================
# # EF01
# #=======================================================================

######### GUIDEWIRE ##############
resource "aws_route53_record" "ICECONOMICAL_EF01_GUIDEWIRE01_SUITE_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "gw1.ef01.economical.insurcloud.ca"
    ttl     = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICECONOMICAL_SIT01_GUIDEWIRE01_SUITE_PRIVATEIP}"]
}

resource "aws_route53_record" "ICECONOMICAL_EF01_GUIDEWIRE02_SUITE_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "gw2.ef01.economical.insurcloud.ca"
    ttl     = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICECONOMICAL_SIT01_GUIDEWIRE02_SUITE_PRIVATEIP}"]
}

resource "aws_route53_record" "ICECONOMICAL_EF01_GUIDEWIRE03_SUITE_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "gw3.ef01.economical.insurcloud.ca"
    ttl     = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICECONOMICAL_SIT01_GUIDEWIRE03_SUITE_PRIVATEIP}"]
}

resource "aws_route53_record" "ICECONOMICAL_EF01_GUIDEWIRE04_SUITE_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "gw4.ef01.economical.insurcloud.ca"
    ttl     = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICECONOMICAL_SIT01_GUIDEWIRE04_SUITE_PRIVATEIP}"]
}

resource "aws_route53_record" "ICECONOMICAL_EF01_GUIDEWIRE_DB_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "gwdb.ef01.economical.insurcloud.ca"
    ttl     = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICECONOMICAL_SIT01_GUIDEWIRE_DB_PRIVATEIP}"]
}

resource "aws_route53_record" "ICECONOMICAL_EF01_INTERNAL_NLB_GUIDEWIRE_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "gw-ef01.economical.insurcloud.ca"
    type    = "A"
    alias {
        name                   = "${data.terraform_remote_state.dev_remote_state.ICECONOMICAL_SIT01_INTERNAL_NLB_DNS_NAME}"
        zone_id                = "${data.terraform_remote_state.dev_remote_state.ICECONOMICAL_SIT01_INTERNAL_NLB_ZONE_ID}"
        evaluate_target_health = false
    }
}


# #=======================================================================
# # PPS01
# #=======================================================================

######### GUIDEWIRE ##############
resource "aws_route53_record" "ICECONOMICAL_PPS01_GUIDEWIRE01_SUITE_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "gw1.pps01.economical.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICECONOMICAL_PPS01_GUIDEWIRE01_SUITE_PRIVATEIP}"]
}

resource "aws_route53_record" "ICECONOMICAL_PPS01_GUIDEWIRE02_SUITE_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "gw2.pps01.economical.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICECONOMICAL_PPS01_GUIDEWIRE02_SUITE_PRIVATEIP}"]
}

resource "aws_route53_record" "ICECONOMICAL_PPS01_GUIDEWIRE03_SUITE_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "gw3.pps01.economical.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICECONOMICAL_PPS01_GUIDEWIRE03_SUITE_PRIVATEIP}"]
}

resource "aws_route53_record" "ICECONOMICAL_PPS01_GUIDEWIRE04_SUITE_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "gw4.pps01.economical.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICECONOMICAL_PPS01_GUIDEWIRE04_SUITE_PRIVATEIP}"]
}

resource "aws_route53_record" "ICECONOMICAL_PPS01_GUIDEWIRE_DB_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "gwdb.pps01.economical.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICECONOMICAL_PPS01_GUIDEWIRE_DB_PRIVATEIP}"]
}

resource "aws_route53_record" "ICECONOMICAL_PPS01_INTERNAL_NLB_GUIDEWIRE_RECORD_1" {
    zone_id = "${var.zone_id}"
    name    = "gw.pps01.economical.insurcloud.ca"
    type    = "A"
    alias {
        name                   = "${data.terraform_remote_state.dev_remote_state.ICECONOMICAL_PPS01_INTERNAL_NLB_DNS_NAME}"
        zone_id                = "${data.terraform_remote_state.dev_remote_state.ICECONOMICAL_PPS01_INTERNAL_NLB_ZONE_ID}"
        evaluate_target_health = false
    }
}

resource "aws_route53_record" "ICECONOMICAL_PPS01_INTERNAL_NLB_GUIDEWIRE_RECORD_2" {
    zone_id = "${var.zone_id}"
    name    = "gw-pps01.economical.insurcloud.ca"
    type    = "A"
    alias {
        name                   = "${data.terraform_remote_state.dev_remote_state.ICECONOMICAL_PPS01_INTERNAL_NLB_DNS_NAME}"
        zone_id                = "${data.terraform_remote_state.dev_remote_state.ICECONOMICAL_PPS01_INTERNAL_NLB_ZONE_ID}"
        evaluate_target_health = false
    }
}

######### MICROSERVICES ###########
resource "aws_route53_record" "ICECONOMICAL_PPS01_MICROSERVICES_MANAGER01_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "msman1.pps01.economical.insurcloud.ca"
    ttl     = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICECONOMICAL_PPS01_MICROSERVICES_MANAGER01_PRIVATEIP}"]
}

resource "aws_route53_record" "ICECONOMICAL_PPS01_MICROSERVICES_MANAGER02_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "msman2.pps01.economical.insurcloud.ca"
    ttl     = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICECONOMICAL_PPS01_MICROSERVICES_MANAGER02_PRIVATEIP}"]
}

resource "aws_route53_record" "ICECONOMICAL_PPS01_MICROSERVICES_MANAGER03_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "msman3.pps01.economical.insurcloud.ca"
    ttl     = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICECONOMICAL_PPS01_MICROSERVICES_MANAGER03_PRIVATEIP}"]
}

resource "aws_route53_record" "ICECONOMICAL_PPS01_MICROSERVICES_APPLICATION01_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "ms1.pps01.economical.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICECONOMICAL_PPS01_MICROSERVICES_APPLICATION01_PRIVATEIP}"]
}

resource "aws_route53_record" "ICECONOMICAL_PPS01_MICROSERVICES_APPLICATION02_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "ms2.pps01.economical.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICECONOMICAL_PPS01_MICROSERVICES_APPLICATION02_PRIVATEIP}"]
}

resource "aws_route53_record" "ICECONOMICAL_PPS01_INTERNAL_NLB_MICROSERVICES_RECORD_1" {
    zone_id = "${var.zone_id}"
    name    = "ms.pps01.economical.insurcloud.ca"
    type    = "A"
    alias {
        name                   = "${data.terraform_remote_state.dev_remote_state.ICECONOMICAL_PPS01_INTERNAL_NLB_DNS_NAME}"
        zone_id                = "${data.terraform_remote_state.dev_remote_state.ICECONOMICAL_PPS01_INTERNAL_NLB_ZONE_ID}"
        evaluate_target_health = false
    }
}

resource "aws_route53_record" "ICECONOMICAL_PPS01_INTERNAL_NLB_MICROSERVICES_RECORD_2" {
    zone_id = "${var.zone_id}"
    name    = "ms-pps01.economical.insurcloud.ca"
    type    = "A"
    alias {
        name                   = "${data.terraform_remote_state.dev_remote_state.ICECONOMICAL_PPS01_INTERNAL_NLB_DNS_NAME}"
        zone_id                = "${data.terraform_remote_state.dev_remote_state.ICECONOMICAL_PPS01_INTERNAL_NLB_ZONE_ID}"
        evaluate_target_health = false
    }
}

######### RPA ##############
resource "aws_route53_record" "ICECONOMICAL_PPS01_RPA_WS_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "rpa-pps01.economical.insurcloud.ca"
    type    = "A"
    alias {
        name                   = "${data.terraform_remote_state.dev_remote_state.ICECONOMICAL_PPS01_RPA_NLB_DNS_NAME}"
        zone_id                = "${data.terraform_remote_state.dev_remote_state.ICECONOMICAL_PPS01_RPA_NLB_ZONE_ID}"
        evaluate_target_health = false
    }
}

resource "aws_route53_record" "ICECONOMICAL_PPS01_RPA_APP01_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "rpa-app01.pps01.economical.insurcloud.ca"
    ttl     = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICECONOMICAL_PPS01_RPA_APP01_PRIVATEIP}"]
}

resource "aws_route53_record" "ICECONOMICAL_PPS01_RPA_APP02_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "rpa-app02.pps01.economical.insurcloud.ca"
    ttl     = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICECONOMICAL_PPS01_RPA_APP02_PRIVATEIP}"]
}

resource "aws_route53_record" "ICECONOMICAL_PPS01_RPA_DB_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "rpa-db.pps01.economical.insurcloud.ca"
    ttl     = "300"
    type    = "CNAME"
    records = ["${data.terraform_remote_state.dev_remote_state.ICECONOMICAL_PPS01_RPA_DB_PRIVATEIP}"]
}

resource "aws_route53_record" "ICECONOMICAL_PPS01_RPA_INT01_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "rpa-int01.pps01.economical.insurcloud.ca"
    ttl     = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICECONOMICAL_PPS01_RPA_INT01_PRIVATEIP}"]
}

resource "aws_route53_record" "ICECONOMICAL_PPS01_RPA_CLIENT01_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "rpa-client01.pps01.economical.insurcloud.ca"
    ttl     = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICECONOMICAL_PPS01_RPA_CLIENT01_PRIVATEIP}"]
}

resource "aws_route53_record" "ICECONOMICAL_PPS01_RPA_CLIENT02_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "rpa-client02.pps01.economical.insurcloud.ca"
    ttl     = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICECONOMICAL_PPS01_RPA_CLIENT02_PRIVATEIP}"]
}

resource "aws_route53_record" "ICECONOMICAL_PPS01_RPA_CLIENT03_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "rpa-client03.pps01.economical.insurcloud.ca"
    ttl     = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICECONOMICAL_PPS01_RPA_CLIENT03_PRIVATEIP}"]
}

resource "aws_route53_record" "ICECONOMICAL_PPS01_RPA_CLIENT04_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "rpa-client04.pps01.economical.insurcloud.ca"
    ttl     = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICECONOMICAL_PPS01_RPA_CLIENT04_PRIVATEIP}"]
}

resource "aws_route53_record" "ICECONOMICAL_PPS01_RPA_CLIENT05_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "rpa-client05.pps01.economical.insurcloud.ca"
    ttl     = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICECONOMICAL_PPS01_RPA_CLIENT05_PRIVATEIP}"]
}

resource "aws_route53_record" "ICECONOMICAL_PPS01_RPA_CLIENT06_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "rpa-client06.pps01.economical.insurcloud.ca"
    ttl     = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICECONOMICAL_PPS01_RPA_CLIENT06_PRIVATEIP}"]
}

resource "aws_route53_record" "ICECONOMICAL_PPS01_RPA_CLIENT07_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "rpa-client07.pps01.economical.insurcloud.ca"
    ttl     = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICECONOMICAL_PPS01_RPA_CLIENT07_PRIVATEIP}"]
}

resource "aws_route53_record" "ICECONOMICAL_PPS01_RPA_CLIENT08_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "rpa-client08.pps01.economical.insurcloud.ca"
    ttl     = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICECONOMICAL_PPS01_RPA_CLIENT08_PRIVATEIP}"]
}

######### HAPROXY ##############
resource "aws_route53_record" "ICECONOMICAL_PPS01_INTERNAL_HAPROXY_RECORD" { /* TODO: REMOVE THIS LATER */
    zone_id = "${var.zone_id}"
    name    = "inthap.pps01.economical.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICECONOMICAL_PPS01_INTERNAL_HAPROXY_PRIVATEIP01}"]
}

resource "aws_route53_record" "ICECONOMICAL_PPS01_INTERNAL_HAPROXY_RECORD01" {
    zone_id = "${var.zone_id}"
    name    = "inthap01.pps01.economical.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICECONOMICAL_PPS01_INTERNAL_HAPROXY_PRIVATEIP01}"]
}

resource "aws_route53_record" "ICECONOMICAL_PPS01_INTERNAL_HAPROXY_RECORD02" {
    zone_id = "${var.zone_id}"
    name    = "inthap02.pps01.economical.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICECONOMICAL_PPS01_INTERNAL_HAPROXY_PRIVATEIP02}"]
}


#=======================================================================
# MISC
#=======================================================================

############# JENKINS CLIENT #################
resource "aws_route53_record" "ICECONOMICAL_DEV_JENKINS_CLIENT_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "jenkins-client.dev.economical.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.dev_remote_state.ICECONOMICAL_DEV_JENKINS_CLIENT_PRIVATEIP}"]
}

resource "aws_route53_record" "ICECONOMICAL_PROD_JENKINS_CLIENT_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "jenkins-client.economical.insurcloud.ca"
    ttl = "300"
    type    = "A"
    records = ["${data.terraform_remote_state.prod_remote_state.ICECONOMICAL_PROD_JENKINS_CLIENT_PRIVATEIP}"]
}
