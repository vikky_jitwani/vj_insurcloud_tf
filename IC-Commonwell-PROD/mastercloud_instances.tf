# #=======================================================================
# # GUIDEWIRE
# #=======================================================================
######### PROD SHARED NODES ##############
resource "aws_instance" "ICCW_PROD_GUIDEWIRE_ONLINE_1" {
    ami = "${var.ami_id_amazonlinux2base20200506}"
    vpc_security_group_ids = [
		"${aws_security_group.SG_GUIDEWIRE.id}",
		"${aws_security_group.SG_GUIDEWIRE_CLUSTER_NBIO_EVENT_QUEUE.id}",
		"${aws_security_group.SG_JENKINS_GUIDEWIRE_HEALTH_CHECK.id}",
		"${aws_security_group.SG_AVIATRIX_SSH.id}",
		"${aws_security_group.SG_JENKINS_CLIENT_SSH.id}",
	]
    subnet_id = "${var.commonwell-prod-guidewire-containers-1a-subnet}"
    instance_type = "r5a.2xlarge"
    key_name    = "CommonwellProdGuidewire"
    associate_public_ip_address = false
    iam_instance_profile = "CmigServiceRole"
	ebs_optimized = "true"

    root_block_device {
       volume_size = "100"
    }

    tags {
        Owner = "robhatnagar@deloitte.ca"
        ManagedBy = "CMS-TF"
        SolutionName = "Insurcloud Commonwell"
        Environment = "Prod"
        ApplicationName = "guidewire, docker"
        DataClassification = "PII, PHI"
        DLM-Backup = "Daily-7"
        DLM-Retention = "90"
        PatchGroup = "AmazonLinux2"
        VendorManaged = "False"
        ResourceType = "Client"
        ResourceSeverity = "Critical"
        Name = "A-MC-CAC-D-A003-COMMONWELL-PROD-GW-ONLINE-1-LNX-EC2"
        AccountID = "${data.aws_caller_identity.current.account_id}"
        Role = "Container"
        NexusUse = "True"
		Account = "COMMONWELL-prod"
		Client = "cmig"
		KeyName = "CommonwellProdGuidewire"
    }    

    provisioner "local-exec" {
        command = "echo ${aws_instance.ICCW_PROD_GUIDEWIRE_ONLINE_1.private_ip} >> prod_hosts.txt"
    }
}

output "ICCW_PROD_GUIDEWIRE_ONLINE_1_PRIVATEIP" {
    value = "${aws_instance.ICCW_PROD_GUIDEWIRE_ONLINE_1.private_ip}"
}

resource "aws_instance" "ICCW_PROD_GUIDEWIRE_ONLINE_2" {
    ami = "${var.ami_id_amazonlinux2base20200506}"
    vpc_security_group_ids = [
		"${aws_security_group.SG_GUIDEWIRE.id}",
		"${aws_security_group.SG_GUIDEWIRE_CLUSTER_NBIO_EVENT_QUEUE.id}",
		"${aws_security_group.SG_JENKINS_GUIDEWIRE_HEALTH_CHECK.id}",
		"${aws_security_group.SG_AVIATRIX_SSH.id}",
		"${aws_security_group.SG_JENKINS_CLIENT_SSH.id}",
	]
    subnet_id = "${var.commonwell-prod-guidewire-containers-1b-subnet}"
    instance_type = "r5a.2xlarge"
    key_name    = "CommonwellProdGuidewire"
    associate_public_ip_address = false
    iam_instance_profile = "CmigServiceRole"
	ebs_optimized = "true"

    root_block_device {
       volume_size = "100"
    }

    tags {
        Owner = "robhatnagar@deloitte.ca"
        ManagedBy = "CMS-TF"
        SolutionName = "Insurcloud Commonwell"
        Environment = "Prod"
        ApplicationName = "guidewire, docker"
        DataClassification = "PII, PHI"
        DLM-Backup = "Daily-7"
        DLM-Retention = "90"
        PatchGroup = "AmazonLinux2"
        VendorManaged = "False"
        ResourceType = "Client"
        ResourceSeverity = "Critical"
        Name = "A-MC-CAC-D-A003-COMMONWELL-PROD-GW-ONLINE-2-LNX-EC2"
        AccountID = "${data.aws_caller_identity.current.account_id}"
        Role = "Container"
        NexusUse = "True"
		Account = "COMMONWELL-prod"
		Client = "cmig"
		KeyName = "CommonwellProdGuidewire"
    }    

    provisioner "local-exec" {
        command = "echo ${aws_instance.ICCW_PROD_GUIDEWIRE_ONLINE_2.private_ip} >> prod_hosts.txt"
    }
}

output "ICCW_PROD_GUIDEWIRE_ONLINE_2_PRIVATEIP" {
    value = "${aws_instance.ICCW_PROD_GUIDEWIRE_ONLINE_2.private_ip}"
}

resource "aws_instance" "ICCW_PROD_GUIDEWIRE_BATCH_1" {
    ami = "${var.ami_id_amazonlinux2base20200506}"
    vpc_security_group_ids = [
        "${aws_security_group.SG_GUIDEWIRE.id}",
        "${aws_security_group.SG_GUIDEWIRE_CLUSTER_NBIO_EVENT_QUEUE.id}",
        "${aws_security_group.SG_JENKINS_GUIDEWIRE_HEALTH_CHECK.id}",
        "${aws_security_group.SG_AVIATRIX_SSH.id}",
        "${aws_security_group.SG_JENKINS_CLIENT_SSH.id}",
    ]
    subnet_id = "${var.commonwell-prod-private-1a-subnet}"
    instance_type = "r5a.2xlarge"
    key_name    = "CommonwellProdGuidewire"
    associate_public_ip_address = false
    iam_instance_profile = "CmigServiceRole"
	ebs_optimized = "true"

    root_block_device {
       volume_size = "100"
    }

    tags {
        Owner = "robhatnagar@deloitte.ca"
        ManagedBy = "CMS-TF"
        SolutionName = "Insurcloud Commonwell"
        Environment = "Prod"
        ApplicationName = "guidewire, docker"
        DataClassification = "PII, PHI"
        DLM-Backup = "Daily-7"
        DLM-Retention = "90"
        PatchGroup = "AmazonLinux2"
        VendorManaged = "False"
        ResourceType = "Client"
        ResourceSeverity = "Normal"
        Name = "A-MC-CAC-D-A003-COMMONWELL-PROD-GW-BATCH-1-LNX-EC2"
        AccountID = "${data.aws_caller_identity.current.account_id}"
        Role = "Container"
		Account = "COMMONWELL-prod"
		Client = "cmig"
		KeyName = "CommonwellProdGuidewire"
    }    

    provisioner "local-exec" {
        command = "echo ${aws_instance.ICCW_PROD_GUIDEWIRE_BATCH_1.private_ip} >> prod_hosts.txt"
    }
}

output "ICCW_PROD_GUIDEWIRE_BATCH_1_PRIVATEIP" {
    value = "${aws_instance.ICCW_PROD_GUIDEWIRE_BATCH_1.private_ip}"
}

resource "aws_instance" "ICCW_PROD_GUIDEWIRE_BATCH_2" {
    ami = "${var.ami_id_amazonlinux2base20200506}"
    vpc_security_group_ids = [
        "${aws_security_group.SG_GUIDEWIRE.id}",
        "${aws_security_group.SG_GUIDEWIRE_CLUSTER_NBIO_EVENT_QUEUE.id}",
        "${aws_security_group.SG_JENKINS_GUIDEWIRE_HEALTH_CHECK.id}",
        "${aws_security_group.SG_AVIATRIX_SSH.id}",
        "${aws_security_group.SG_JENKINS_CLIENT_SSH.id}",
    ]
    subnet_id = "${var.commonwell-prod-private-1b-subnet}"
    instance_type = "r5a.2xlarge"
    key_name    = "CommonwellProdGuidewire"
    associate_public_ip_address = false
    iam_instance_profile = "CmigServiceRole"
	ebs_optimized = "true"

    root_block_device {
       volume_size = "100"
    }

    tags {
        Owner = "robhatnagar@deloitte.ca"
        ManagedBy = "CMS-TF"
        SolutionName = "Insurcloud Commonwell"
        Environment = "Prod"
        ApplicationName = "guidewire, docker"
        DataClassification = "PII, PHI"
        DLM-Backup = "Daily-7"
        DLM-Retention = "90"
        PatchGroup = "AmazonLinux2"
        VendorManaged = "False"
        ResourceType = "Client"
        ResourceSeverity = "Normal"
        Name = "A-MC-CAC-D-A003-COMMONWELL-PROD-GW-BATCH-2-LNX-EC2"
        AccountID = "${data.aws_caller_identity.current.account_id}"
        Role = "Container"
		Account = "COMMONWELL-prod"
		Client = "cmig"
		KeyName = "CommonwellProdGuidewire"
    }    

    provisioner "local-exec" {
        command = "echo ${aws_instance.ICCW_PROD_GUIDEWIRE_BATCH_2.private_ip} >> prod_hosts.txt"
    }
}

output "ICCW_PROD_GUIDEWIRE_BATCH_2_PRIVATEIP" {
    value = "${aws_instance.ICCW_PROD_GUIDEWIRE_BATCH_2.private_ip}"
}

# #=======================================================================
# # Microservices
# #=======================================================================
resource "aws_instance" "ICCW_PROD_MICROSERVICES_MANAGER_1" {
    ami = "${var.ami_id_amazonlinux2base}"
    vpc_security_group_ids = ["${aws_security_group.SG_COMMONWELL_MICROSERVICES.id}"]
	subnet_id = "${var.commonwell-prod-spring-microservices-1a-subnet}"
    instance_type = "t3a.small"
    key_name    = "CommonwellProdMicroservices"
    associate_public_ip_address = false
	iam_instance_profile = "CmigServiceRole"
	ebs_optimized = "true"

	root_block_device {
       volume_size = "16"
	}

	tags {
		Owner = "robhatnagar@deloitte.ca"
		ManagedBy = "CMS-TF"
		SolutionName = "Insurcloud Commonwell"
		Environment = "Prod"
		ApplicationName = "docker, microservices"
		DataClassification = "PII, PHI"
		DLM-Backup = "Daily-7"
		DLM-Retention = "90"
		PatchGroup = "AmazonLinux2-Microsvc1"
		VendorManaged = "False"
		ResourceType = "Client"
		ResourceSeverity = "Critical"
		Name = "A-MC-CAC-D-A003-COMMONWELL-PROD-MICROSERVICES-MANAGER-1-LNX-EC2"
		AccountID = "${data.aws_caller_identity.current.account_id}"
		Role = "Container"
		NexusUse = "True"
		Account = "COMMONWELL-prod"
		Client = "cmig"
		KeyName = "CommonwellProdMicroservices"
	}

	provisioner "local-exec" {
    	command = "echo ${aws_instance.ICCW_PROD_MICROSERVICES_MANAGER_1.private_ip} >> prod_hosts.txt"
  	}
}

output "ICCW_PROD_MICROSERVICES_MANAGER_1_PRIVATEIP" {
  value = "${aws_instance.ICCW_PROD_MICROSERVICES_MANAGER_1.private_ip}"
}

resource "aws_instance" "ICCW_PROD_MICROSERVICES_MANAGER_2" {
    ami = "${var.ami_id_amazonlinux2base}"
    vpc_security_group_ids = ["${aws_security_group.SG_COMMONWELL_MICROSERVICES.id}"]
	subnet_id = "${var.commonwell-prod-spring-microservices-1b-subnet}"
    instance_type = "t3a.small"
    key_name    = "CommonwellProdMicroservices"
    associate_public_ip_address = false
	iam_instance_profile = "CmigServiceRole"
	ebs_optimized = "true"

	root_block_device {
       volume_size = "16"
	}

	tags {
		Owner = "robhatnagar@deloitte.ca"
		ManagedBy = "CMS-TF"
		SolutionName = "Insurcloud Commonwell"
		Environment = "Prod"
		ApplicationName = "docker, microservices"
		DataClassification = "PII, PHI"
		DLM-Backup = "Daily-7"
		DLM-Retention = "90"
		PatchGroup = "AmazonLinux2-Microsvc2"
		VendorManaged = "False"
		ResourceType = "Client"
		ResourceSeverity = "Critical"
		Name = "A-MC-CAC-D-A003-COMMONWELL-PROD-MICROSERVICES-MANAGER-2-LNX-EC2"
		AccountID = "${data.aws_caller_identity.current.account_id}"
		Role = "Container"
		NexusUse = "True"
		Account = "COMMONWELL-prod"
		Client = "cmig"
		KeyName = "CommonwellProdMicroservices"
	}

	provisioner "local-exec" {
    	command = "echo ${aws_instance.ICCW_PROD_MICROSERVICES_MANAGER_2.private_ip} >> prod_hosts.txt"
  	}
}

output "ICCW_PROD_MICROSERVICES_MANAGER_2_PRIVATEIP" {
  value = "${aws_instance.ICCW_PROD_MICROSERVICES_MANAGER_2.private_ip}"
}

resource "aws_instance" "ICCW_PROD_MICROSERVICES_MANAGER_3" {
    ami = "${var.ami_id_amazonlinux2base}"
    vpc_security_group_ids = ["${aws_security_group.SG_COMMONWELL_MICROSERVICES.id}"]
	subnet_id = "${var.commonwell-prod-spring-microservices-1a-subnet}"
    instance_type = "t3a.small"
    key_name    = "CommonwellProdMicroservices"
    associate_public_ip_address = false
	iam_instance_profile = "CmigServiceRole"
	ebs_optimized = "true"

	root_block_device {
       volume_size = "16"
	}

	tags {
		Owner = "robhatnagar@deloitte.ca"
		ManagedBy = "CMS-TF"
		SolutionName = "Insurcloud Commonwell"
		Environment = "Prod"
		ApplicationName = "docker, microservices"
		DataClassification = "PII, PHI"
		DLM-Backup = "Daily-7"
		DLM-Retention = "90"
		PatchGroup = "AmazonLinux2-Microsvc3"
		VendorManaged = "False"
		ResourceType = "Client"
		ResourceSeverity = "Critical"
		Name = "A-MC-CAC-D-A003-COMMONWELL-PROD-MICROSERVICES-MANAGER-3-LNX-EC2"
		AccountID = "${data.aws_caller_identity.current.account_id}"
		Role = "Container"
		NexusUse = "True"
		Account = "COMMONWELL-prod"
		Client = "cmig"
		KeyName = "CommonwellProdMicroservices"
	}

	provisioner "local-exec" {
    	command = "echo ${aws_instance.ICCW_PROD_MICROSERVICES_MANAGER_3.private_ip} >> prod_hosts.txt"
  	}
}

output "ICCW_PROD_MICROSERVICES_MANAGER_3_PRIVATEIP" {
  value = "${aws_instance.ICCW_PROD_MICROSERVICES_MANAGER_3.private_ip}"
}

resource "aws_instance" "ICCW_PROD_MICROSERVICES_APPLICATION_1" {
    ami = "${var.ami_id_amazonlinux2base}"
    vpc_security_group_ids = ["${aws_security_group.SG_COMMONWELL_MICROSERVICES.id}","${aws_security_group.SG_COMMONWELL-PROMETHEUS-GRAFANA.id}"]
	subnet_id = "${var.commonwell-prod-spring-microservices-1a-subnet}"
    instance_type = "r5a.2xlarge"
    key_name    = "CommonwellProdMicroservices"
    associate_public_ip_address = false
	iam_instance_profile = "CmigServiceRole"
	ebs_optimized = true

	root_block_device {
       volume_size = "16"
	}

	tags {
		Owner = "robhatnagar@deloitte.ca"
		ManagedBy = "CMS-TF"
		SolutionName = "Insurcloud Commonwell"
		Environment = "Prod"
		ApplicationName = "docker, microservices"
		DataClassification = "PII, PHI"
		DLM-Backup = "Daily-7"
		DLM-Retention = "90"
		PatchGroup = "AmazonLinux2-Microsvc4"
		VendorManaged = "False"
		ResourceType = "Client"
		ResourceSeverity = "Critical"
		Name = "A-MC-CAC-D-A003-COMMONWELL-PROD-MICROSERVICES-APP-1-LNX-EC2"
		AccountID = "${data.aws_caller_identity.current.account_id}"
		Role = "Container"
		NexusUse = "True"
		Account = "COMMONWELL-prod"
		Client = "cmig"
		KeyName = "CommonwellProdMicroservices"
	}

	provisioner "local-exec" {
    	command = "echo ${aws_instance.ICCW_PROD_MICROSERVICES_APPLICATION_1.private_ip} >> prod_hosts.txt"
  	}
}

output "ICCW_PROD_MICROSERVICES_APPLICATION_1_PRIVATEIP" {
  value = "${aws_instance.ICCW_PROD_MICROSERVICES_APPLICATION_1.private_ip}"
}

resource "aws_instance" "ICCW_PROD_MICROSERVICES_APPLICATION_2" {
    ami = "${var.ami_id_amazonlinux2base}"
    vpc_security_group_ids = ["${aws_security_group.SG_COMMONWELL_MICROSERVICES.id}","${aws_security_group.SG_COMMONWELL-PROMETHEUS-GRAFANA.id}"]
	subnet_id = "${var.commonwell-prod-spring-microservices-1b-subnet}"
    instance_type = "r5a.2xlarge"
    key_name    = "CommonwellProdMicroservices"
    associate_public_ip_address = false
	iam_instance_profile = "CmigServiceRole"
	ebs_optimized = true

	root_block_device {
       volume_size = "16"
	}

	tags {
		Owner = "robhatnagar@deloitte.ca"
		ManagedBy = "CMS-TF"
		SolutionName = "Insurcloud Commonwell"
		Environment = "Prod"
		ApplicationName = "docker, microservices"
		DataClassification = "PII, PHI"
		DLM-Backup = "Daily-7"
		DLM-Retention = "90"
		PatchGroup = "AmazonLinux2-Microsvc5"
		VendorManaged = "False"
		ResourceType = "Client"
		ResourceSeverity = "Critical"
		Name = "A-MC-CAC-D-A003-COMMONWELL-PROD-MICROSERVICES-APP-2-LNX-EC2"
		AccountID = "${data.aws_caller_identity.current.account_id}"
		Role = "Container"
		NexusUse = "True"
		Account = "COMMONWELL-prod"
		Client = "cmig"
		KeyName = "CommonwellProdMicroservices"
	}

	provisioner "local-exec" {
    	command = "echo ${aws_instance.ICCW_PROD_MICROSERVICES_APPLICATION_2.private_ip} >> prod_hosts.txt"
  	}
}

output "ICCW_PROD_MICROSERVICES_APPLICATION_2_PRIVATEIP" {
  value = "${aws_instance.ICCW_PROD_MICROSERVICES_APPLICATION_2.private_ip}"
}

# #=======================================================================
# # HAProxies
# #=======================================================================
resource "aws_instance" "ICCW_PROD_EXTERNAL_HA_PROXY_N1" {
    ami = "${var.ami_id_amazonlinux2base}"
    vpc_security_group_ids = ["${aws_security_group.SG_EXTERNAL_HAPROXY.id}","${aws_security_group.SG_HAPROXY_EXTERNAL_PEERING.id}"]
	subnet_id = "${var.commonwell-prod-private-1a-subnet}"
    instance_type = "t3a.small"
    key_name    = "CommonwellProdHAProxy"
    associate_public_ip_address = false
	iam_instance_profile = "CmigServiceRole"

	root_block_device {
       volume_size = "10"
	}

	tags {
		Owner = "robhatnagar@deloitte.ca"
		ManagedBy = "CMS-TF"
		SolutionName = "Insurcloud Commonwell"
		Environment = "Prod"
		ApplicationName = "docker, haproxy"
		DataClassification = "PII, PHI"
		DLM-Backup = "Daily-7"
		DLM-Retention = "90"
		PatchGroup = "AmazonLinux2"
		VendorManaged = "False"
		ResourceType = "Client"
		ResourceSeverity = "Critical"
		Name = "A-MC-CAC-D-A003-COMMONWELL-PROD-EXTERNAL-HA-PROXY-N1-EC2"
		AccountID = "${data.aws_caller_identity.current.account_id}"
		Role = "Container"
		NexusUse = "True"
		Account = "COMMONWELL-prod"
		Client = "cmig"
		KeyName = "CommonwellProdHAProxy"
	}

	provisioner "local-exec" {
    	command = "echo ${aws_instance.ICCW_PROD_EXTERNAL_HA_PROXY_N1.private_ip} >> prod_hosts.txt"
  	}
}

output "ICCW_PROD_EXTERNAL_HA_PROXY_N1_PRIVATEIP" {
  value = "${aws_instance.ICCW_PROD_EXTERNAL_HA_PROXY_N1.private_ip}"
}

resource "aws_instance" "ICCW_PROD_EXTERNAL_HA_PROXY_N2" {
    ami = "${var.ami_id_amazonlinux2base}"
    vpc_security_group_ids = ["${aws_security_group.SG_EXTERNAL_HAPROXY.id}","${aws_security_group.SG_HAPROXY_EXTERNAL_PEERING.id}"]
	subnet_id = "${var.commonwell-prod-private-1b-subnet}"
    instance_type = "t3a.small"
    key_name    = "CommonwellProdHAProxy"
    associate_public_ip_address = false
	iam_instance_profile = "CmigServiceRole"

	root_block_device {
       volume_size = "10"
	}

	tags {
		Owner = "robhatnagar@deloitte.ca"
		ManagedBy = "CMS-TF"
		SolutionName = "Insurcloud Commonwell"
		Environment = "Prod"
		ApplicationName = "docker, haproxy"
		DataClassification = "NA"
		DLM-Backup = "Daily-7"
		DLM-Retention = "90"
		PatchGroup = "AmazonLinux2"
		VendorManaged = "False"
		ResourceType = "Client"
		ResourceSeverity = "Critical"
		Name = "A-MC-CAC-D-A003-COMMONWELL-PROD-EXTERNAL-HA-PROXY-N2-EC2"
		AccountID = "${data.aws_caller_identity.current.account_id}"
		Role = "Container"
		NexusUse = "True"
		Account = "COMMONWELL-prod"
		Client = "cmig"
		KeyName = "CommonwellProdHAProxy"
	}

	provisioner "local-exec" {
    	command = "echo ${aws_instance.ICCW_PROD_EXTERNAL_HA_PROXY_N2.private_ip} >> prod_hosts.txt"
  	}
}

output "ICCW_PROD_EXTERNAL_HA_PROXY_N2_PRIVATEIP" {
  value = "${aws_instance.ICCW_PROD_EXTERNAL_HA_PROXY_N2.private_ip}"
}

resource "aws_instance" "ICCW_PROD_INTERNAL_HA_PROXY_N1" {
    ami = "${var.ami_id_amazonlinux2base}"
    vpc_security_group_ids = ["${aws_security_group.SG_INTERNAL_HAPROXY.id}","${aws_security_group.SG_HAPROXY_INTERNAL_PEERING.id}"]
	subnet_id = "${var.commonwell-prod-private-1a-subnet}"
    instance_type = "t3a.small"
    key_name    = "CommonwellProdHAProxy"
    associate_public_ip_address = false
	iam_instance_profile = "CmigServiceRole"

	root_block_device {
       volume_size = "10"
	}

	tags {
		Owner = "robhatnagar@deloitte.ca"
		ManagedBy = "CMS-TF"
		SolutionName = "Insurcloud Commonwell"
		Environment = "Prod"
		ApplicationName = "docker, haproxy"
		DataClassification = "NA"
		DLM-Backup = "Daily-7"
		DLM-Retention = "90"
		PatchGroup = "AmazonLinux2"
		VendorManaged = "False"
		ResourceType = "Client"
		ResourceSeverity = "Critical"
		Name = "A-MC-CAC-D-A003-COMMONWELL-PROD-INTERNAL-HA-PROXY-N1-EC2"
		AccountID = "${data.aws_caller_identity.current.account_id}"
		Role = "Container"
		NexusUse = "True"
		Account = "COMMONWELL-prod"
		Client = "cmig"
		KeyName = "CommonwellProdHAProxy"
	}

	provisioner "local-exec" {
    	command = "echo ${aws_instance.ICCW_PROD_INTERNAL_HA_PROXY_N1.private_ip} >> prod_hosts.txt"
  	}
}

output "ICCW_PROD_INTERNAL_HA_PROXY_N1_PRIVATEIP" {
  value = "${aws_instance.ICCW_PROD_INTERNAL_HA_PROXY_N1.private_ip}"
}

resource "aws_instance" "ICCW_PROD_INTERNAL_HA_PROXY_N2" {
    ami = "${var.ami_id_amazonlinux2base}"
    vpc_security_group_ids = ["${aws_security_group.SG_INTERNAL_HAPROXY.id}","${aws_security_group.SG_HAPROXY_INTERNAL_PEERING.id}"]
	subnet_id = "${var.commonwell-prod-private-1b-subnet}"
    instance_type = "t3a.small"
    key_name    = "CommonwellProdHAProxy"
    associate_public_ip_address = false
	iam_instance_profile = "CmigServiceRole"

	root_block_device {
       volume_size = "10"
	}

	tags {
		Owner = "robhatnagar@deloitte.ca"
		ManagedBy = "CMS-TF"
		SolutionName = "Insurcloud Commonwell"
		Environment = "Prod"
		ApplicationName = "docker, haproxy"
		DataClassification = "NA"
		DLM-Backup = "Daily-7"
		DLM-Retention = "90"
		PatchGroup = "AmazonLinux2"
		VendorManaged = "False"
		ResourceType = "Client"
		ResourceSeverity = "Critical"
		Name = "A-MC-CAC-D-A003-COMMONWELL-PROD-INTERNAL-HA-PROXY-N2-EC2"
		AccountID = "${data.aws_caller_identity.current.account_id}"
		Role = "Container"
		NexusUse = "True"
		Account = "COMMONWELL-prod"
		Client = "cmig"
		KeyName = "CommonwellProdHAProxy"
	}

	provisioner "local-exec" {
    	command = "echo ${aws_instance.ICCW_PROD_INTERNAL_HA_PROXY_N2.private_ip} >> prod_hosts.txt"
  	}
}

output "ICCW_PROD_INTERNAL_HA_PROXY_N2_PRIVATEIP" {
  value = "${aws_instance.ICCW_PROD_INTERNAL_HA_PROXY_N2.private_ip}"
}

# #=======================================================================
# # OT
# #=======================================================================

resource "aws_instance" "ICCW_EXSTREAM_COM_SERVER" {
    ami = "ami-01b912743b28aabf9"
    vpc_security_group_ids = ["${aws_security_group.SG_COMMONWELL_EXSTREAM.id}"]
	subnet_id = "${var.commonwell-prod-private-1a-subnet}"
    instance_type = "r5a.xlarge"
    key_name    = "CommonwellProdExstream"
    associate_public_ip_address = false
	iam_instance_profile = "CmigServiceRole"

    lifecycle {
      prevent_destroy = true
  	}

	root_block_device {
       volume_size = "1122"
	   delete_on_termination = false
	}

	tags {
		Owner = "robhatnagar@deloitte.ca"
		ManagedBy = "CMS-TF"
		SolutionName = "Insurcloud Commonwell"
		Environment = "Prod"
		ApplicationName = "exstream"
		DataClassification = "PII, PHI"
		DLM-Backup = "Daily-7"
		DLM-Retention = "90"
		PatchGroup = "Windows"
		VendorManaged = "False"
		ResourceType = "Client"
		ResourceSeverity = "Critical"
		Name = "A-MC-CAC-D-A003-COMMONWELL-PROD-EXSTREAM-COMMUNICATIONS-WIN-EC2"
		AccountID = "${data.aws_caller_identity.current.account_id}"
		Role = "Container"
		NexusUse = "False"
		RestartWindow = "restart"
		Account = "COMMONWELL-prod"
		Client = "cmig"
		KeyName = "CommonwellProdExstream"
	}

}

output "ICCW_EXSTREAM_COM_SERVER_PRIVATEIP" {
	value = "${aws_instance.ICCW_EXSTREAM_COM_SERVER.private_ip}"
}

resource "aws_instance" "ICCW_EXSTREAM_CONTENT_BRAVA_SERVER" {
    ami = "ami-01b912743b28aabf9"
    vpc_security_group_ids = ["${aws_security_group.SG_COMMONWELL_EXSTREAM.id}"]
	subnet_id = "${var.commonwell-prod-private-1a-subnet}"
    instance_type = "r5a.large"
    key_name    = "CommonwellProdExstream"
    associate_public_ip_address = false
	iam_instance_profile = "CmigServiceRole"

    lifecycle {
    prevent_destroy = true
  	}

	root_block_device {
       volume_size = "560"
	}

	tags {
		Owner = "robhatnagar@deloitte.ca"
		ManagedBy = "CMS-TF"
		SolutionName = "Insurcloud Commonwell"
		Environment = "Prod"
		ApplicationName = "otcs"
		DataClassification = "PII, PHI"
		DLM-Backup = "Daily-7"
		DLM-Retention = "90"
		PatchGroup = "Windows"
		VendorManaged = "False"
		ResourceType = "Client"
		ResourceSeverity = "Critical"
		Name = "A-MC-CAC-D-A003-COMMONWELL-PROD-OT-CONTENT-SUITE-BRAVA-WIN-EC2"
		AccountID = "${data.aws_caller_identity.current.account_id}"
		Role = "Container"
		NexusUse = "False"
		Account = "COMMONWELL-prod"
		Client = "cmig"
		KeyName = "CommonwellProdExstream"
	}

}

output "ICCW_OT_CONTENT_SUITE_BRAVA_SERVER_PRIVATEIP" {
	value = "${aws_instance.ICCW_EXSTREAM_CONTENT_BRAVA_SERVER.private_ip}"
}

resource "aws_instance" "ICCW_EXSTREAM_CONTENT_INDEX_SERVER" {
    ami = "ami-01b912743b28aabf9"
    vpc_security_group_ids = ["${aws_security_group.SG_COMMONWELL_EXSTREAM.id}"]
	subnet_id = "${var.commonwell-prod-private-1a-subnet}"
    instance_type = "i3.2xlarge"
    key_name    = "CommonwellProdExstream"
    associate_public_ip_address = false
	iam_instance_profile = "CmigServiceRole"

    lifecycle {
    prevent_destroy = true
  	}

	root_block_device {
       volume_size = "560"
	}

	tags {
		Owner = "robhatnagar@deloitte.ca"
		ManagedBy = "CMS-TF"
		SolutionName = "Insurcloud Commonwell"
		Environment = "Prod"
		ApplicationName = "otcs"
		DataClassification = "PII, PHI"
		DLM-Backup = "Daily-7"
		DLM-Retention = "90"
		PatchGroup = "Windows"
		VendorManaged = "False"
		ResourceType = "Client"
		ResourceSeverity = "Critical"
		Name = "A-MC-CAC-D-A003-COMMONWELL-PROD-OT-CONTENT-SUITE-INDEX-WIN-EC2"
		AccountID = "${data.aws_caller_identity.current.account_id}"
		Role = "Container"
		NexusUse = "False"
		Account = "COMMONWELL-prod"
		Client = "cmig"
		KeyName = "CommonwellProdExstream"
	}
}

output "ICCW_OT_CONTENT_SUITE_INDEX_SERVER_PRIVATEIP" {
	value = "${aws_instance.ICCW_EXSTREAM_CONTENT_INDEX_SERVER.private_ip}"
}

# #=======================================================================
# # Jobscheduler
# #=======================================================================
resource "aws_instance" "ICCW_PROD_JOB_SCHEDULER" {
    ami = "${var.ami_id_amazonlinux2base}"
    vpc_security_group_ids = ["${aws_security_group.SG_JOB_SCHEDULER.id}"]
    subnet_id = "${var.commonwell-prod-private-1b-subnet}"
    instance_type = "t3a.large"
    key_name    = "CommonwellProdSAP"
    associate_public_ip_address = false
    iam_instance_profile = "CmigServiceRole"

	lifecycle {
        prevent_destroy = true
  	}

    root_block_device {
       volume_size = "40"
    }

	tags {
		Owner = "robhatnagar@deloitte.ca"
		ManagedBy = "CMS-TF"
		SolutionName = "Insurcloud Commonwell"
		Environment = "Prod"
		ApplicationName = "jobscheduler"
		DataClassification = "PII, PHI"
		DLM-Backup = "Daily-7"
		DLM-Retention = "90"
		PatchGroup = "AmazonLinux2"
		VendorManaged = "False"
		ResourceType = "Client"
		ResourceSeverity = "Critical"
		Name = "A-MC-CAC-D-A003-COMMONWELL-PROD-JOB-SCHEDULER-LNX-EC2"
		AccountID = "${data.aws_caller_identity.current.account_id}"
		Role = "Container"
		NexusUse = "True"
		Account = "COMMONWELL-prod"
		Client = "cmig"
		KeyName = "CommonwellProdSAP"
	}

}

output "ICCW_PROD_JOB_SCHEDULER_PRIVATEIP" {
	value = "${aws_instance.ICCW_PROD_JOB_SCHEDULER.private_ip}"
}

# #=======================================================================
# # Hubio
# #=======================================================================
resource "aws_instance" "ICCW_PROD_HUBIO_SERVER" {
    ami = "${var.ami_id_windows2019cont}"
    vpc_security_group_ids = ["${aws_security_group.SG_HUBIO.id}","${aws_security_group.SG_COMMONWELL_JOBSCHEDULER_SSH.id}"]
	subnet_id = "${var.commonwell-prod-private-1b-subnet}"
    instance_type = "t3a.large"
    key_name    = "CommonwellProdApplications"
    associate_public_ip_address = false
	iam_instance_profile = "CmigServiceRole"

	lifecycle {
        prevent_destroy = true
  	}

	root_block_device {
       volume_size = "100"
	}

	tags {
		Owner = "robhatnagar@deloitte.ca"
		ManagedBy = "CMS-TF"
		SolutionName = "Insurcloud Commonwell"
		Environment = "Prod"
		ApplicationName = "hubio"
		DataClassification = "PII, PHI"
		DLM-Backup = "Daily-7"
		DLM-Retention = "90"
		PatchGroup = "Windows"
		VendorManaged = "False"
		ResourceType = "Client"
		ResourceSeverity = "Critical"
		Name = "A-MC-CAC-D-A003-COMMONWELL-PROD-HUBIO-WIN-EC2"
		AccountID = "${data.aws_caller_identity.current.account_id}"
		Role = "Container"
		NexusUse = "False"
		Account = "COMMONWELL-prod"
		Client = "cmig"
		KeyName = "CommonwellProdApplications"
	}

}

output "ICCW_PROD_HUBIO_SERVER_PRIVATEIP" {
	value = "${aws_instance.ICCW_PROD_HUBIO_SERVER.private_ip}"
}

# #=======================================================================
# # Qlik
# #=======================================================================
resource "aws_instance" "ICCW_QLIK_SENSE" {
    ami = "${var.ami_id_windows2012R2base}"
    vpc_security_group_ids = ["${aws_security_group.SG_QLIKSENSE.id}"]
    subnet_id = "${var.commonwell-prod-private-1a-subnet}"
    instance_type = "r5a.4xlarge"
    key_name    = "CommonwellProdApplications"
    associate_public_ip_address = false
    iam_instance_profile = "CmigServiceRole"

    lifecycle {
        prevent_destroy = true
  	}

    root_block_device {
        volume_size = "4000"
    }

	tags {
		Owner = "robhatnagar@deloitte.ca"
		ManagedBy = "CMS-TF"
		SolutionName = "Insurcloud Commonwell"
		Environment = "Prod"
		ApplicationName = "qliksense"
		DataClassification = "PII, PHI"
		DLM-Backup = "Daily-7"
		DLM-Retention = "90"
		PatchGroup = "Windows"
		VendorManaged = "False"
		ResourceType = "Client"
		ResourceSeverity = "Critical"
		Name = "A-MC-CAC-D-A003-COMMONWELL-PROD-QLIK-SENSE-WIN-EC2"
		AccountID = "${data.aws_caller_identity.current.account_id}"
		Role = "Container"
		NexusUse = "False"
		Account = "COMMONWELL-prod"
		Client = "cmig"
		KeyName = "CommonwellProdApplications"
	}

}

output "ICCW_QLIK_SENSE_PRIVATEIP" {
	value = "${aws_instance.ICCW_QLIK_SENSE.private_ip}"
}

resource "aws_instance" "ICCW_NPRINTING" {
    ami = "${var.ami_id_windows2016base}"
    vpc_security_group_ids = ["${aws_security_group.SG_NPRINTING.id}"]
    subnet_id = "${var.commonwell-prod-private-1a-subnet}"
    instance_type = "t3a.large"
    key_name    = "CommonwellProdApplications"
    associate_public_ip_address = false
    iam_instance_profile = "CmigServiceRole"
	ebs_optimized = "true"

    lifecycle {
        prevent_destroy = true
  	}

    root_block_device {
        volume_size = "128"
    }

	tags {
		Owner = "robhatnagar@deloitte.ca"
		ManagedBy = "CMS-TF"
		SolutionName = "Insurcloud Commonwell"
		Environment = "Prod"
		ApplicationName = "nPrinting"
		DataClassification = "PII, PHI"
		DLM-Backup = "Daily-7"
		DLM-Retention = "90"
		PatchGroup = "Windows"
		VendorManaged = "False"
		ResourceType = "Client"
		ResourceSeverity = "Critical"
		Name = "A-MC-CAC-D-A003-COMMONWELL-PROD-NPRINTING-WIN-EC2"
		AccountID = "${data.aws_caller_identity.current.account_id}"
		Role = "Container"
		NexusUse = "False"
		BILLINGCODE = "DEL02890-01-01-01-NB10"
		BILLINGCONTACT = "smodh@deloitte.ca"
		CLIENT = "CMS"
		COUNTRY = "CA"
		CSCLASS = "Confidential"
		CSQUAL = "No CI/PI data"
		CSTYPE = "External"
		ENVIRONMENT = "PRD"
		FUNCTION = "CON"
		GROUPCONTACT = "smodh@deloitte.ca"
		MEMBERFIRM = "CA" 
		PRIMARYCONTACT = "session1"
		SECONDARYCONTACT = "smodh@deloitte.ca"
		Account = "COMMONWELL-prod"
		Client = "cmig"
		KeyName = "CommonwellProdApplications"
	}

}

output "ICCW_NPRINTING_PRIVATEIP" {
	value = "${aws_instance.ICCW_NPRINTING.private_ip}"
}

# #=======================================================================
# # DH/IC
# #=======================================================================
resource "aws_instance" "ICCW_INFOCENTER_DATAHUB_SAP_SERVER" {
	ami                         = "${var.ami_id_windows2016base}"
	vpc_security_group_ids      = ["${aws_security_group.SG_DATAHUB_INFOCENTER.id}", "${aws_security_group.SG_COMMONWELL_JOBSCHEDULER_SSH.id}"]
	subnet_id                   = "${var.commonwell-prod-private-1a-subnet}"
	instance_type               = "t3a.2xlarge"
	key_name                    = "CommonwellProdSAP"
	associate_public_ip_address = false
	iam_instance_profile        = "CmigServiceRole"
	private_ip     			  = "10.10.54.26"
	ebs_optimized				  = "true"
	
	lifecycle {
		prevent_destroy = true
	}

	root_block_device {
		volume_size = "200"
	}

	tags {
		Owner = "robhatnagar@deloitte.ca"
		ManagedBy = "CMS-TF"
		SolutionName = "Insurcloud Commonwell"
		Environment = "Prod"
		ApplicationName = "datahub"
		DataClassification = "PII, PHI"
		DLM-Backup = "Daily-7"
		DLM-Retention = "90"
		PatchGroup = "Windows"
		VendorManaged = "False"
		ResourceType = "Client"
		ResourceSeverity = "Critical"
		Name = "A-MC-CAC-D-A003-COMMONWELL-PROD-INFOCENTER-DATAHUB-SAP-WIN-EC2"
		AccountID = "${data.aws_caller_identity.current.account_id}"
		Role = "Container"
		NexusUse = "False"
		Account = "COMMONWELL-prod"
		Client = "cmig"
		KeyName = "CommonwellProdSAP"
	}

	provisioner "local-exec" {
		command = "echo ${aws_instance.ICCW_INFOCENTER_DATAHUB_SAP_SERVER.private_ip} >> prod_hosts.txt"
	}
}

output "ICCW_INFOCENTER_DATAHUB_SAP_SERVER_PRIVATEIP" {
  value = "${aws_instance.ICCW_INFOCENTER_DATAHUB_SAP_SERVER.private_ip}"
}

resource "aws_instance" "ICCW_DATAHUB_INFOCENTER_DATABASE_R2_MSSQL_2014_DEV_EDITION" {
    ami = "ami-03ca5b004e3e2ca01"
    vpc_security_group_ids      = [
		"${aws_security_group.SG_MSSQL_DB.id}",
		"${aws_security_group.SG_AVIATRIX_RDP.id}",
		"${aws_security_group.SG_FORTICLIENT_RDP.id}",
		"${aws_security_group.SG_COMMONWELL_JOBSCHEDULER_SSH.id}",
		"${aws_security_group.SG_MSSQL_OMNIA_DB.id}"
	]
	subnet_id = "${var.commonwell-prod-database-1a-subnet}"
    instance_type = "r5a.2xlarge"
    key_name    = "CommonwellProdDatabase"
    associate_public_ip_address = false
	iam_instance_profile = "CMS-SSMInstanceRole"
	ebs_optimized = "true"

	lifecycle {
       prevent_destroy = true
  	}

	root_block_device {
       volume_size = "1500"
	}

	tags {
		Owner = "robhatnagar@deloitte.ca"
		ManagedBy = "CMS-TF"
		SolutionName = "Insurcloud Commonwell"
		Environment = "Prod"
		ApplicationName = "MSSQL"
		DataClassification = "PII, PHI"
		DLM-Backup = "Daily-7"
		DLM-Retention = "90"
		PatchGroup = "Windows"
		VendorManaged = "False"
		ResourceType = "Client"
		ResourceSeverity = "Normal"
		Name = "A-MC-CAC-D-A003-COMMONWELL-QC-DATAHUB-INFOCENTER-DATABASE-R2-MSSQL-WIN-EC2-DEV-EDITION"
		AccountID = "${data.aws_caller_identity.current.account_id}"
		Role = "Container"
		NexusUse = "False"
		Account = "COMMONWELL-prod"
		Client = "cmig"
		KeyName = "CommonwellProdDatabase"
	}
}

output "ICCW_DATAHUB_INFOCENTER_DATABASE_R2_MSSQL_2014_DEV_EDITION_PRIVATEIP" {
  value = "${aws_instance.ICCW_DATAHUB_INFOCENTER_DATABASE_R2_MSSQL_2014_DEV_EDITION.private_ip}"
}

resource "aws_instance" "ICCW_INFOCENTER_DATAHUB_SAP_SERVER_TEST_EC2" {
	ami                         = "ami-0882ac19ae61771e4"
	vpc_security_group_ids      = ["${aws_security_group.SG_DATAHUB_INFOCENTER.id}", "${aws_security_group.SG_COMMONWELL_JOBSCHEDULER_SSH.id}"]
	subnet_id                   = "${var.commonwell-prod-private-1a-subnet}"
	instance_type               = "t3.2xlarge"
	key_name                    = "CommonwellProdSAP"
	associate_public_ip_address = false
	iam_instance_profile        = "CmigServiceRole"
	ebs_optimized				  = "true"

	lifecycle {
		prevent_destroy = true
	}

	root_block_device {
		volume_size = "200"
	}

	tags {
		Owner = "robhatnagar@deloitte.ca"
		ManagedBy = "CMS-TF"
		SolutionName = "Insurcloud Commonwell"
		Environment = "Prod"
		ApplicationName = "datahub"
		DataClassification = "PII, PHI"
		DLM-Backup = "Daily-7"
		DLM-Retention = "90"
		PatchGroup = "Windows"
		VendorManaged = "False"
		ResourceType = "Client"
		ResourceSeverity = "Critical"
		Name = "A-MC-CAC-D-A003-COMMONWELL-QC-INFOCENTER-DATAHUB-SAP-WIN-EC2"
		AccountID = "${data.aws_caller_identity.current.account_id}"
		Role = "Container"
		NexusUse = "False"
		Account = "COMMONWELL-prod"
		Client = "cmig"
		KeyName = "CommonwellProdSAP"
	}

	provisioner "local-exec" {
		command = "echo ${aws_instance.ICCW_INFOCENTER_DATAHUB_SAP_SERVER_TEST_EC2.private_ip} >> prod_hosts.txt"
	}
} 

output "ICCW_INFOCENTER_DATAHUB_SAP_SERVER_TEST_EC2_PRIVATEIP" {
  value = "${aws_instance.ICCW_INFOCENTER_DATAHUB_SAP_SERVER_TEST_EC2.private_ip}"
}
