resource "aws_security_group" "SG_CLIENT_PING_TEST" {
  name = "CLIENT-PING-TEST"

  ingress {
    from_port       = -1
    to_port         = -1
    protocol        = "icmp"
    cidr_blocks     = ["${var.gore_cidr}"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    Name = "CLIENT-PING-TEST"
  }
}

resource "aws_security_group" "SG_EXSTREAM_LICENSE" {
  name = "EXSTREAM-LICENSE-SG"

  ingress {
    from_port   = 28700
    to_port     = 28700
    protocol    = "tcp"
    cidr_blocks = ["${var.default_cidr}"]
  }

  ingress {
    from_port   = 27000
    to_port     = 27000
    protocol    = "tcp"
    cidr_blocks = ["${var.default_cidr}"]
  }

  ingress {
    from_port   = 2701
    to_port     = 2702
    protocol    = "tcp"
    cidr_blocks = ["${var.default_cidr}"]
  }

  ingress {
    from_port   = 8443
    to_port     = 8443
    protocol    = "tcp"
    cidr_blocks = ["${var.default_cidr}"]
  }

  ingress {
    from_port   = 1433
    to_port     = 1433
    protocol    = "tcp"
    cidr_blocks = ["${var.default_cidr}"]
  }

  ingress {
    from_port   = 8080
    to_port     = 8080
    protocol    = "tcp"
    cidr_blocks = ["${var.default_cidr}"]
  }

  ingress {
    from_port   = 28701
    to_port     = 28702
    protocol    = "tcp"
    cidr_blocks = ["${var.default_cidr}"]
  }

  ingress {
    from_port   = 2718
    to_port     = 2719
    protocol    = "tcp"
    cidr_blocks = ["${var.default_cidr}"]
  }

  ingress {
    from_port   = 54415
    to_port     = 54415
    protocol    = "tcp"
    cidr_blocks = ["${var.default_cidr}"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    Name = "EXSTREAM-LICENSE-SG"
  }
}

resource "aws_security_group" "SG_AVIATRIX_RDP" {
  name = "AVIATRIX-RDP-SG"

  ingress {
    from_port   = 3389
    to_port     = 3389
    protocol    = "tcp"
    cidr_blocks = ["${var.gore_aviatrix_vpn}"]
  
    description = "RDP"
  }

  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    Name = "AVIATRIX-RDP-SG"
  }
}

resource "aws_security_group" "SG_AVIATRIX_SSH" {
  name = "AVIATRIX-SSH-SG"

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = [
      "${var.gore_aviatrix_vpn}"
    ]
    description = "SSH"
  }

  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    Name = "AVIATRIX-SSH-SG"
  }
}

resource "aws_security_group" "SG_JOB_SCHEDULER" {
  name = "JOB-SCHEDULER-SG"

  ingress {
    from_port       = 4444
    to_port         = 4444
    protocol        = "tcp"
    cidr_blocks = [
      "${var.gore_dev_cidr}",
      "${var.gore_aviatrix_vpn}",
      "${var.gore_cidr}"
    ]
  }

  ingress {
    from_port       = 4444
    to_port         = 4444
    protocol        = "udp"
    cidr_blocks = [
      "${var.gore_dev_cidr}",
      "${var.gore_aviatrix_vpn}",
      "${var.gore_cidr}"
    ]
  }

  ingress {
    from_port       = 4446
    to_port         = 4446
    protocol        = "tcp"
    cidr_blocks = [
      "${var.gore_dev_cidr}",
      "${var.gore_aviatrix_vpn}",
      "${var.gore_cidr}"
    ]
  }

  ingress {
    from_port       = 4446
    to_port         = 4446
    protocol        = "udp"
    cidr_blocks = [
      "${var.gore_dev_cidr}",
      "${var.gore_aviatrix_vpn}",
      "${var.gore_cidr}"
    ]
  }

  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    Name = "JOB-SCHEDULER-SG"
  }
}

resource "aws_security_group" "SG_JENKINS_CLIENT_SSH" {
  name = "JENKINS-CLIENT-SSH-SG"
 
  ingress {
    from_port       = 22
    to_port         = 22
    protocol        = "tcp"
    cidr_blocks = [
      "10.11.0.173/32" 
    ]
    description = "Jenkins Client SSH"
  }

  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    Name = "JENKINS-CLIENT-SSH-SG"
  }
}


resource "aws_security_group" "SG_ELB_PREPROD" {
  name = "PREPROD-ELB-SG"
 
  ingress {
    from_port       = 22
    to_port         = 22
    protocol        = "tcp"
    cidr_blocks = [
      "10.11.0.173/32" 
    ]
    description = "Jenkins Client SSH"
  }

  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    Name = "JENKINS-CLIENT-SSH-SG"
  }
}

resource "aws_security_group" "SG_DYNATRACE_CLUSTER" {
  name = "DYNATRACE-CLUSTER-SG"
 
  ingress {
    from_port       = 443
    to_port         = 443
    protocol        = "tcp"
    cidr_blocks = [
      "${var.gore_dev_cidr}",
      "${var.gore_aviatrix_vpn}",
      "${var.gore_cidr}"
    ]
    description = "DYNATRACE UI HTTPS"
  }

  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
  }

  vpc_id = "${var.vpc_id}"

  tags = {
    Name = "DYNATRACE-CLUSTER-SG"
  }
}