################### Exsteam ########################
data "template_file" "ICPEEL_EXSTREAM_LIC_1" {
  template = "${file("IC-Peel-HUB/windowsdata.tpl")}"

  vars {
    computer_name = "EXSTREAM-LIC-1"
    region              = "${var.region}"
    domain              = "${var.ad_domain}"
    dns1                = "${var.ad_dns1}"
    dns2                = "${var.ad_dns2}"
    dns3                = "${var.ad_dns3}"
    ouselection         = "${var.ou_name}"
    pwordparameter      = "${var.pwordparameter}"
    unameparameter      = "${var.unameparameter}"
  }
}
resource "aws_instance" "ICPEEL_EXSTREAM_LIC_1" {
    ami = "${var.ami_id_windows2012R2base}"
    vpc_security_group_ids = ["${aws_security_group.SG_EXSTREAM_LICENSE.id}","${aws_security_group.SG_AVIATRIX_RDP.id}"]
    subnet_id = "${var.peel-hub-private-1a-subnet}"
    instance_type = "t3a.small"
    key_name    = "PeelExstreamHub"
    associate_public_ip_address = false
    iam_instance_profile = "CMS-SSMInstanceRole"
	ebs_optimized = "true"

    lifecycle {
       prevent_destroy = false
    }

    root_block_device {
       volume_size = "80"
    }

    user_data = "${data.template_file.ICPEEL_EXSTREAM_LIC_1.rendered}"
    
    tags {
        Owner = "robhatnagar@deloitte.ca"
        ManagedBy = "CMS-TF"
        SolutionName = "Insurcloud Peel"
        Environment = "Hub"
        ApplicationName = "exstream"
        DataClassification = "NA"
        DLM-Backup = "Daily-1"
        DLM-Retention = "90"
        PatchGroup = "Windows"
        VendorManaged = "False"
        ResourceType = "Client"
        ResourceSeverity = "Normal"
        Name = "A-MC-CAC-D-A003-PEEL-HUB-WIN-EXSTREAM-LIC-1-EC2"
        AccountID = "${data.aws_caller_identity.current.account_id}"
        Role = "Container"
        CLIENT = "InsurCloud PEEL"
        NexusUse = "False"
		Account = "PEEL-hub"
		Client = "Peel"
		KeyName = "PeelExstreamHub"
    }

}

data "template_file" "ICPEEL_EXSTREAM_LIC_2" {
  template = "${file("IC-Peel-HUB/windowsdata.tpl")}"

    vars {
        computer_name = "EXSTREAM-LIC-2"
        region              = "${var.region}"
        domain              = "${var.ad_domain}"
        dns1                = "${var.ad_dns1}"
        dns2                = "${var.ad_dns2}"
        dns3                = "${var.ad_dns3}"
        ouselection         = "${var.ou_name}"
        pwordparameter      = "${var.pwordparameter}"
        unameparameter      = "${var.unameparameter}"
    }
}
resource "aws_instance" "ICPEEL_EXSTREAM_LIC_2" {
    ami = "${var.ami_id_windows2012R2base}"
    vpc_security_group_ids = ["${aws_security_group.SG_EXSTREAM_LICENSE.id}","${aws_security_group.SG_AVIATRIX_RDP.id}"]
    subnet_id = "${var.peel-hub-private-1a-subnet}"
    instance_type = "t3a.small"
    key_name    = "PeelExstreamHub"
    associate_public_ip_address = false
    iam_instance_profile = "CMS-SSMInstanceRole"
	ebs_optimized = "true"

    lifecycle {
       prevent_destroy = false
    }

    root_block_device {
       volume_size = "80"
    }

    user_data = "${data.template_file.ICPEEL_EXSTREAM_LIC_2.rendered}"

    tags {
        Owner = "robhatnagar@deloitte.ca"
        ManagedBy = "CMS-TF"
        SolutionName = "Insurcloud Peel"
        Environment = "Hub"
        ApplicationName = "exstream"
        DataClassification = "NA"
        DLM-Backup = "Daily-1"
        DLM-Retention = "90"
        PatchGroup = "Windows"
        VendorManaged = "False"
        ResourceType = "Client"
        ResourceSeverity = "Normal"
        Name = "A-MC-CAC-D-A003-PEEL-HUB-WIN-EXSTREAM-LIC-2-EC2"
        AccountID = "${data.aws_caller_identity.current.account_id}"
        Role = "Container"
        CLIENT = "InsurCloud PEEL"
        NexusUse = "False"
		Account = "PEEL-hub"
		Client = "Peel"
		KeyName = "PeelExstreamHub"
    }
}

data "template_file" "ICPEEL_EXSTREAM_LIC_3" {
      template = "${file("IC-Peel-HUB/windowsdata.tpl")}"

    vars {
        computer_name = "EXSTREAM-LIC-3"
        region              = "${var.region}"
        domain              = "${var.ad_domain}"
        dns1                = "${var.ad_dns1}"
        dns2                = "${var.ad_dns2}"
        dns3                = "${var.ad_dns3}"
        ouselection         = "${var.ou_name}"
        pwordparameter      = "${var.pwordparameter}"
        unameparameter      = "${var.unameparameter}"
    }
}
resource "aws_instance" "ICPEEL_EXSTREAM_LIC_3" {
    ami = "${var.ami_id_windows2012R2base}"
    vpc_security_group_ids = ["${aws_security_group.SG_EXSTREAM_LICENSE.id}","${aws_security_group.SG_AVIATRIX_RDP.id}"]
    subnet_id = "${var.peel-hub-private-1b-subnet}"
    instance_type = "t3a.small"
    key_name    = "PeelExstreamHub"
    associate_public_ip_address = false
    iam_instance_profile = "CMS-SSMInstanceRole"
	ebs_optimized = "true"

    lifecycle {
       prevent_destroy = false
    }

    root_block_device {
       volume_size = "80"
    }

    user_data = "${data.template_file.ICPEEL_EXSTREAM_LIC_3.rendered}"

    tags {
        Owner = "robhatnagar@deloitte.ca"
        ManagedBy = "CMS-TF"
        SolutionName = "Insurcloud Peel"
        Environment = "Hub"
        ApplicationName = "exstream"
        DataClassification = "NA"
        DLM-Backup = "Daily-1"
        DLM-Retention = "90"
        PatchGroup = "Windows"
        VendorManaged = "False"
        ResourceType = "Client"
        ResourceSeverity = "Normal"
        Name = "A-MC-CAC-D-A003-PEEL-HUB-WIN-EXSTREAM-LIC-3-EC2"
        AccountID = "${data.aws_caller_identity.current.account_id}"
        Role = "Container"
        CLIENT = "InsurCloud PEEL"
        NexusUse = "False"
		Account = "PEEL-hub"
		Client = "Peel"
		KeyName = "PeelExstreamHub"
    }    
}

######### JOBSCHEDULER ##############
resource "aws_instance" "ICPEEL_DEV_JOB_SCHEDULER" {
    ami = "${var.ami_id_amazonlinux2base}"
    vpc_security_group_ids      = [
        "${aws_security_group.SG_JOB_SCHEDULER.id}",
        "${aws_security_group.SG_AVIATRIX_SSH.id}",
        "${aws_security_group.SG_JENKINS_CLIENT_SSH.id}"
    ]
    subnet_id = "${var.peel-hub-private-1a-subnet}"
    instance_type = "t3a.medium"
    key_name    = "PeelOtherHub"
    associate_public_ip_address = false
    iam_instance_profile = "CMS-SSMInstanceRole"
	ebs_optimized = "true"

    root_block_device {
       volume_size = "40"
       delete_on_termination = "false"
    }

    tags {
        Owner = "robhatnagar@deloitte.ca"
        ManagedBy = "CMS-TF"
        SolutionName = "Insurcloud Peel"
        Environment = "Hub"
        ApplicationName = "jobscheduler"
        DataClassification = "PII, PHI"
        DLM-Backup = "Daily-1"
        DLM-Retention = "90"
        PatchGroup = "AmazonLinux2"
        VendorManaged = "False"
        ResourceType = "Client"
        ResourceSeverity = "Normal"
        Name = "A-MC-CAC-D-A003-PEEL-DEV-JOB-SCHEDULER-LNX-EC2"
        AccountID = "${data.aws_caller_identity.current.account_id}"
        Role = "Container"
        CLIENT = "InsurCloud PEEL"
		Account = "PEEL-hub"
		Client = "Peel"
		KeyName = "PeelOtherHub"
    }
}
