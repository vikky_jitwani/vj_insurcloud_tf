#=======================================================================
# Jenkins
#=======================================================================
resource "aws_instance" "ICCW_DEV_JENKINS_CLIENT" {
    ami = "${var.ami_id_amazonlinux2base}"
    vpc_security_group_ids = ["${aws_security_group.SG_JENKINS_CLIENT.id}"]
	subnet_id = "${var.commonwell-hub-private-1a-subnet}"
    instance_type = "r5a.xlarge"
    key_name    = "CommonwellCICD"
    associate_public_ip_address = false
	private_ip = "10.10.4.75"
	iam_instance_profile = "CmigServiceRole"

	lifecycle {
       prevent_destroy = true
	}

	root_block_device {
       volume_size = "250"
	}

	tags {
		Owner = "robhatnagar@deloitte.ca"
		ManagedBy = "CMS-TF"
		SolutionName = "Insurcloud Commonwell"
		Environment = "Hub"
		ApplicationName = "jenkins, docker"
		DataClassification = "NA"
		DLM-Backup = "Daily-1"
		DLM-Retention = "90"
		PatchGroup = "AmazonLinux2"
		VendorManaged = "False"
		ResourceType = "Client"
		ResourceSeverity = "Normal"
		Name = "A-MC-CAC-D-A003-INSURCLOUD-DEV-JENKINS-CLIENT-EC2"
		AccountID = "${data.aws_caller_identity.current.account_id}"
		Role = "Container"
		NexusUse = "True"
		BackupOption = "Patching"
		Account = "COMMONWELL-hub"
		Client = "cmig"
		KeyName = "CommonwellCICD"
	}
}

resource "aws_instance" "ICCW_PROD_JENKINS_CLIENT" {
    ami = "${var.ami_id_amazonlinux2base}"
    vpc_security_group_ids = ["${aws_security_group.SG_JENKINS_CLIENT.id}"]
	subnet_id = "${var.commonwell-hub-private-1a-subnet}"
    instance_type = "t3a.small"
    key_name    = "CommonwellCICD"
    associate_public_ip_address = false
	iam_instance_profile = "CmigServiceRole"
	ebs_optimized = "true"

	lifecycle {
       prevent_destroy = true
	}
	root_block_device {
       volume_size = "250"
	}

	tags {
		Owner = "robhatnagar@deloitte.ca"
		ManagedBy = "CMS-TF"
		SolutionName = "Insurcloud Commonwell"
		Environment = "Hub"
		ApplicationName = "jenkins, docker"
		DataClassification = "NA"
		DLM-Backup = "Daily-1"
		DLM-Retention = "90"
		PatchGroup = "AmazonLinux2"
		VendorManaged = "False"
		ResourceType = "Client"
		ResourceSeverity = "Normal"
		Name = "A-MC-CAC-D-A003-INSURCLOUD-PROD-JENKINS-CLIENT-EC2"
		AccountID = "${data.aws_caller_identity.current.account_id}"
		Role = "Container"
		NexusUse = "True"
		BackupOption = "Patching"
		Account = "COMMONWELL-hub"
		Client = "cmig"
		KeyName = "CommonwellCICD"
	}
	
}

#=======================================================================
# Exstream
#=======================================================================
resource "aws_instance" "ICCW_EXSTEAM_LIC_1" {
    ami = "${var.ami_id_windows2012R2base}"
    vpc_security_group_ids = ["${aws_security_group.SG_COMMONWELL_EXSTREAM.id}"]
	subnet_id = "${var.commonwell-hub-private-1a-subnet}"
    instance_type = "t3a.small"
    key_name    = "CommonwellExstreamHub"
    associate_public_ip_address = false
	iam_instance_profile = "CmigServiceRole"
	ebs_optimized = "true"

	lifecycle {
       prevent_destroy = true
	}

	root_block_device {
       volume_size = "80"
		delete_on_termination = false
	}

	tags {
		Owner = "robhatnagar@deloitte.ca"
		ManagedBy = "CMS-TF"
		SolutionName = "Insurcloud Commonwell"
		Environment = "Hub"
		ApplicationName = "exstream"
		DataClassification = "NA"
		DLM-Backup = "Daily-1"
		DLM-Retention = "90"
		PatchGroup = "Windows"
		VendorManaged = "False"
		ResourceType = "Client"
		ResourceSeverity = "Normal"
		Name = "A-MC-CAC-D-A003-INSURCLOUD-HUB-WIN-EXSTREAM-LIC-1-EC2"
		AccountID = "${data.aws_caller_identity.current.account_id}"
		Role = "Container"
		NexusUse = "False"
		BackupOption = "Patching"
		Account = "COMMONWELL-hub"
		Client = "cmig"
		KeyName = "CommonwellExstreamHub"
	}

}

resource "aws_instance" "ICCW_EXSTEAM_LIC_2" {
    ami = "${var.ami_id_windows2012R2base}"
    vpc_security_group_ids = ["${aws_security_group.SG_COMMONWELL_EXSTREAM.id}"]
	subnet_id = "${var.commonwell-hub-private-1a-subnet}"
    instance_type = "t3a.small"
    key_name    = "CommonwellExstreamHub"
    associate_public_ip_address = false
	iam_instance_profile = "CmigServiceRole"
	ebs_optimized = "true"

	lifecycle {
       prevent_destroy = true
	}

	root_block_device {
       volume_size = "80"
		delete_on_termination = false
	}

	tags {
		Owner = "robhatnagar@deloitte.ca"
		ManagedBy = "CMS-TF"
		SolutionName = "Insurcloud Commonwell"
		Environment = "Hub"
		ApplicationName = "exstream"
		DataClassification = "NA"
		DLM-Backup = "Daily-1"
		DLM-Retention = "90"
		PatchGroup = "Windows"
		VendorManaged = "False"
		ResourceType = "Client"
		ResourceSeverity = "Normal"
		Name = "A-MC-CAC-D-A003-INSURCLOUD-HUB-WIN-EXSTREAM-LIC-2-EC2"
		AccountID = "${data.aws_caller_identity.current.account_id}"
		Role = "Container"
		NexusUse = "False"
		BackupOption = "Patching"
		Account = "COMMONWELL-hub"
		Client = "cmig"
		KeyName = "CommonwellExstreamHub"
	}
}

resource "aws_instance" "ICCW_EXSTEAM_LIC_3" {
    ami = "${var.ami_id_windows2012R2base}"
    vpc_security_group_ids = ["${aws_security_group.SG_COMMONWELL_EXSTREAM.id}"]
	subnet_id = "${var.commonwell-hub-private-1b-subnet}"
    instance_type = "t3a.small"
    key_name    = "CommonwellExstreamHub"
    associate_public_ip_address = false
	iam_instance_profile = "CmigServiceRole"
	ebs_optimized = "true"

	lifecycle {
       prevent_destroy = true
	}

	root_block_device {
       volume_size = "80"
		delete_on_termination = false
	}

	tags {
		Owner = "robhatnagar@deloitte.ca"
		ManagedBy = "CMS-TF"
		SolutionName = "Insurcloud Commonwell"
		Environment = "Hub"
		ApplicationName = "exstream"
		DataClassification = "NA"
		DLM-Backup = "Daily-1"
		DLM-Retention = "90"
		PatchGroup = "Windows"
		VendorManaged = "False"
		ResourceType = "Client"
		ResourceSeverity = "Normal"
		Name = "A-MC-CAC-D-A003-INSURCLOUD-HUB-WIN-EXSTREAM-LIC-3-EC2"
		AccountID = "${data.aws_caller_identity.current.account_id}"
		Role = "Container"
		NexusUse = "False"
		BackupOption = "Patching"
		Account = "COMMONWELL-hub"
		Client = "cmig"
		KeyName = "CommonwellExstreamHub"
	}	
}

#=======================================================================
# Conduit
#=======================================================================
resource "aws_instance" "ICCW_CONDUIT_WORKER" {
    ami = "${var.ami_id_windows2012R2base}"
    count = 3
    vpc_security_group_ids = ["${aws_security_group.SG_CONDUIT_WORKER.id}"]
    subnet_id = "${var.commonwell-hub-private-1b-subnet}"
    instance_type = "t3a.large"
    key_name    = "CommonwellConduit"
    associate_public_ip_address = false
    iam_instance_profile = "CmigServiceRole"

	root_block_device {
		volume_size = "60"
		delete_on_termination = false
	}

	tags {
		Owner = "robhatnagar@deloitte.ca"
		ManagedBy = "CMS-TF"
		SolutionName = "Insurcloud Commonwell"
		Environment = "Hub"
		ApplicationName = "conduit worker"
		DataClassification = "NA"
		DLM-Backup = "Daily-1"
		DLM-Retention = "90"
		PatchGroup = "Windows"
		VendorManaged = "False"
		ResourceType = "Client"
		ResourceSeverity = "Normal"
		Name = "A-MC-CAC-D-A003-INSURCLOUD-HUB-WIN-CONDUIT-WORKER-EC2-${count.index + 1}"
		AccountID = "${data.aws_caller_identity.current.account_id}"
		Role = "Container"
		NexusUse = "False"
		BackupOption = "Patching"
		Account = "COMMONWELL-hub"
		Client = "cmig"
		KeyName = "CommonwellConduit"
	}	
} 

resource "aws_instance" "ICCW_CONDUIT_SERVER" {
    ami = "${var.ami_id_amazonlinux2base}"
    vpc_security_group_ids = ["${aws_security_group.SG_CONDUIT_SERVER.id}"]
    subnet_id = "${var.commonwell-hub-private-1b-subnet}"
    instance_type = "t3a.2xlarge"
    key_name    = "CommonwellConduit"
    associate_public_ip_address = false
    iam_instance_profile = "CmigServiceRole"

	root_block_device {
		volume_size = "500"
		delete_on_termination = false
	}
	
	tags {
		Owner = "robhatnagar@deloitte.ca"
		ManagedBy = "CMS-TF"
		SolutionName = "Insurcloud Commonwell"
		Environment = "Hub"
		ApplicationName = "conduit server"
		DataClassification = "NA"
		DLM-Backup = "Daily-1"
		DLM-Retention = "90"
		PatchGroup = "AmazonLinux2"
		VendorManaged = "False"
		ResourceType = "Client"
		ResourceSeverity = "Normal"
		Name = "A-MC-CAC-D-A003-INSURCLOUD-HUB-LNX-CONDUIT-SERVER-EC2-${count.index + 1}"
		AccountID = "${data.aws_caller_identity.current.account_id}"
		Role = "Container"
		NexusUse = "False"
		BackupOption = "Patching"
		Account = "COMMONWELL-hub"
		Client = "cmig"
		KeyName = "CommonwellConduit"
	}	
}
