
####### GW SHARED NODES VOLUMES #############
resource "aws_volume_attachment" "ICCW_PROD_GUIDEWIRE_ONLINE_1_DOCKER_EBS_ATTACHMENT" {
  device_name = "/dev/sdh"
  volume_id   = "${aws_ebs_volume.ICCW_PROD_GUIDEWIRE_ONLINE_1_DOCKER_EBS.id}"
  instance_id = "${aws_instance.ICCW_PROD_GUIDEWIRE_ONLINE_1.id}"
}

resource "aws_ebs_volume" "ICCW_PROD_GUIDEWIRE_ONLINE_1_DOCKER_EBS" {
  availability_zone = "ca-central-1a"
  size              = 25
  type              = "gp3"
  encrypted         = true
  kms_key_id        = "arn:aws:kms:ca-central-1:942574907772:key/cada27fc-e5f9-4f1c-b85a-3527eb3e75bb"
  tags = {
    Name = "ICCW_PROD_GUIDEWIRE_ONLINE_1_DOCKER_EBS"
    Docker = "True"
  }
}

resource "aws_volume_attachment" "ICCW_PROD_GUIDEWIRE_ONLINE_2_DOCKER_EBS_ATTACHMENT" {
  device_name = "/dev/sdh"
  volume_id   = "${aws_ebs_volume.ICCW_PROD_GUIDEWIRE_ONLINE_2_DOCKER_EBS.id}"
  instance_id = "${aws_instance.ICCW_PROD_GUIDEWIRE_ONLINE_2.id}"
}

resource "aws_ebs_volume" "ICCW_PROD_GUIDEWIRE_ONLINE_2_DOCKER_EBS" {
  availability_zone = "ca-central-1b"
  size              = 25
  type              = "gp3"
  encrypted         = true
  kms_key_id        = "arn:aws:kms:ca-central-1:942574907772:key/cada27fc-e5f9-4f1c-b85a-3527eb3e75bb"
  tags = {
    Name = "ICCW_PROD_GUIDEWIRE_ONLINE_2_DOCKER_EBS"
    Docker = "True"
  }
}

resource "aws_volume_attachment" "ICCW_PROD_GUIDEWIRE_BATCH_1_DOCKER_EBS_ATTACHMENT" {
  device_name = "/dev/sdh"
  volume_id   = "${aws_ebs_volume.ICCW_PROD_GUIDEWIRE_BATCH_1_DOCKER_EBS.id}"
  instance_id = "${aws_instance.ICCW_PROD_GUIDEWIRE_BATCH_1.id}"
}

resource "aws_ebs_volume" "ICCW_PROD_GUIDEWIRE_BATCH_1_DOCKER_EBS" {
  availability_zone = "ca-central-1a"
  size              = 25
  type              = "gp3"
  encrypted         = true
  kms_key_id        = "arn:aws:kms:ca-central-1:942574907772:key/cada27fc-e5f9-4f1c-b85a-3527eb3e75bb"
  tags = {
    Name = "ICCW_PROD_GUIDEWIRE_BATCH_1_DOCKER_EBS"
    Docker = "True"
  }
}

resource "aws_volume_attachment" "ICCW_PROD_GUIDEWIRE_BATCH_2_DOCKER_EBS_ATTACHMENT" {
  device_name = "/dev/sdh"
  volume_id   = "${aws_ebs_volume.ICCW_PROD_GUIDEWIRE_BATCH_2_DOCKER_EBS.id}"
  instance_id = "${aws_instance.ICCW_PROD_GUIDEWIRE_BATCH_2.id}"
}

resource "aws_ebs_volume" "ICCW_PROD_GUIDEWIRE_BATCH_2_DOCKER_EBS" {
  availability_zone = "ca-central-1b"
  size              = 25
  type              = "gp3"
  encrypted         = true
  kms_key_id        = "arn:aws:kms:ca-central-1:942574907772:key/cada27fc-e5f9-4f1c-b85a-3527eb3e75bb"
  tags = {
    Name = "ICCW_PROD_GUIDEWIRE_BATCH_2_DOCKER_EBS"
    Docker = "True"
  }
}

############ External HAProxy Volumes ##################

resource "aws_volume_attachment" "EXT_HA_1_EBS_ATTACHMENT" {
  device_name = "/dev/sdh"
  volume_id   = "${aws_ebs_volume.EXT_HA_1_EBS.id}"
  instance_id = "${aws_instance.ICCW_PROD_EXTERNAL_HA_PROXY_N1.id}"
}

resource "aws_ebs_volume" "EXT_HA_1_EBS" {
  availability_zone = "ca-central-1a"
  size              = 10
  encrypted         = true
  kms_key_id        = "arn:aws:kms:ca-central-1:942574907772:key/cada27fc-e5f9-4f1c-b85a-3527eb3e75bb"
 tags = {
    Name = "ICCW_PROD_EXTERNAL_HA_PROXY_N1_DOCKER_VOL"
    Docker = "True"
  }
}

resource "aws_volume_attachment" "EXT_HA_2_EBS_ATTACHMENT" {
  device_name = "/dev/sdh"
  volume_id   = "${aws_ebs_volume.EXT_HA_2_EBS.id}"
  instance_id = "${aws_instance.ICCW_PROD_EXTERNAL_HA_PROXY_N2.id}"
}

resource "aws_ebs_volume" "EXT_HA_2_EBS" {
  availability_zone = "ca-central-1b"
  size              = 10
  encrypted         = true
  kms_key_id        = "arn:aws:kms:ca-central-1:942574907772:key/cada27fc-e5f9-4f1c-b85a-3527eb3e75bb"
 tags = {
    Name = "ICCW_PROD_EXTERNAL_HA_PROXY_N2_DOCKER_VOL"
    Docker = "True"
  }
}

############ Internal HAProxy Volumes ##################

resource "aws_volume_attachment" "INT_HA_1_EBS_ATTACHMENT" {
  device_name = "/dev/sdh"
  volume_id   = "${aws_ebs_volume.INT_HA_1_EBS.id}"
  instance_id = "${aws_instance.ICCW_PROD_INTERNAL_HA_PROXY_N1.id}"
}

resource "aws_ebs_volume" "INT_HA_1_EBS" {
  availability_zone = "ca-central-1a"
  size              = 10
  encrypted         = true
  kms_key_id        = "arn:aws:kms:ca-central-1:942574907772:key/cada27fc-e5f9-4f1c-b85a-3527eb3e75bb"
 tags = {
    Name = "ICCW_PROD_EXTERNAL_HA_PROXY_N1_DOCKER_VOL"
    Docker = "True"
  }
}

resource "aws_volume_attachment" "INT_HA_2_EBS_ATTACHMENT" {
  device_name = "/dev/sdh"
  volume_id   = "${aws_ebs_volume.INT_HA_2_EBS.id}"
  instance_id = "${aws_instance.ICCW_PROD_INTERNAL_HA_PROXY_N2.id}"
}

resource "aws_ebs_volume" "INT_HA_2_EBS" {
  availability_zone = "ca-central-1b"
  size              = 10
  encrypted         = true
  kms_key_id        = "arn:aws:kms:ca-central-1:942574907772:key/cada27fc-e5f9-4f1c-b85a-3527eb3e75bb"
 tags = {
    Name = "ICCW_PROD_INTERNAL_HA_PROXY_N2_DOCKER_VOL"
    Docker = "True"
  }
}

############ Microservices Volumes ##################

resource "aws_volume_attachment" "MICROSERVICES_MANAGER_1_EBS_ATTACHMENT" {
  device_name = "/dev/sdh"
  volume_id   = "${aws_ebs_volume.MICROSERVICES_MANAGER_1_EBS.id}"
  instance_id = "${aws_instance.ICCW_PROD_MICROSERVICES_MANAGER_1.id}"
}

resource "aws_ebs_volume" "MICROSERVICES_MANAGER_1_EBS" {
  availability_zone = "ca-central-1a"
  size              = 50
  encrypted         = true
  kms_key_id        = "arn:aws:kms:ca-central-1:942574907772:key/cada27fc-e5f9-4f1c-b85a-3527eb3e75bb"
 tags = {
    Name = "ICCW_PROD_MICROSERVICES_MANAGER_1_DOCKER_VOL"
    Docker = "True"
  }
}

resource "aws_volume_attachment" "MICROSERVICES_MANAGER_2_EBS_ATTACHMENT" {
  device_name = "/dev/sdh"
  volume_id   = "${aws_ebs_volume.MICROSERVICES_MANAGER_2_EBS.id}"
  instance_id = "${aws_instance.ICCW_PROD_MICROSERVICES_MANAGER_2.id}"
}

resource "aws_ebs_volume" "MICROSERVICES_MANAGER_2_EBS" {
  availability_zone = "ca-central-1b"
  size              = 50
  encrypted         = true
  kms_key_id        = "arn:aws:kms:ca-central-1:942574907772:key/cada27fc-e5f9-4f1c-b85a-3527eb3e75bb"
 tags = {
    Name = "ICCW_PROD_MICROSERVICES_MANAGER_2_DOCKER_VOL"
    Docker = "True"
  }
}

resource "aws_volume_attachment" "MICROSERVICES_MANAGER_3_EBS_ATTACHMENT" {
  device_name = "/dev/sdh"
  volume_id   = "${aws_ebs_volume.MICROSERVICES_MANAGER_3_EBS.id}"
  instance_id = "${aws_instance.ICCW_PROD_MICROSERVICES_MANAGER_3.id}"
}

resource "aws_ebs_volume" "MICROSERVICES_MANAGER_3_EBS" {
  availability_zone = "ca-central-1a"
  size              = 50
  encrypted         = true
  kms_key_id        = "arn:aws:kms:ca-central-1:942574907772:key/cada27fc-e5f9-4f1c-b85a-3527eb3e75bb"
 tags = {
    Name = "ICCW_PROD_MICROSERVICES_MANAGER_3_DOCKER_VOL"
    Docker = "True"
  }
}

resource "aws_volume_attachment" "MICROSERVICES_WORKER_1_EBS_ATTACHMENT" {
  device_name = "/dev/sdh"
  volume_id   = "${aws_ebs_volume.ICCW_PROD_MICROSERVICES_APPLICATION_1_EBS.id}"
  instance_id = "${aws_instance.ICCW_PROD_MICROSERVICES_APPLICATION_1.id}"
}

resource "aws_ebs_volume" "ICCW_PROD_MICROSERVICES_APPLICATION_1_EBS" {
  availability_zone = "ca-central-1a"
  size              = 150
  encrypted         = true
  kms_key_id        = "arn:aws:kms:ca-central-1:942574907772:key/cada27fc-e5f9-4f1c-b85a-3527eb3e75bb"
 tags = {
    Name = "ICCW_PROD_MICROSERVICES_APPLICATION_1_DOCKER_VOL"
    Docker = "True"
  }
}

resource "aws_volume_attachment" "MICROSERVICES_WORKER_2_EBS_ATTACHMENT" {
  device_name = "/dev/sdh"
  volume_id   = "${aws_ebs_volume.ICCW_PROD_MICROSERVICES_APPLICATION_2_EBS.id}"
  instance_id = "${aws_instance.ICCW_PROD_MICROSERVICES_APPLICATION_2.id}"
}

resource "aws_ebs_volume" "ICCW_PROD_MICROSERVICES_APPLICATION_2_EBS" {
  availability_zone = "ca-central-1b"
  size              = 150
  encrypted         = true
  kms_key_id        = "arn:aws:kms:ca-central-1:942574907772:key/cada27fc-e5f9-4f1c-b85a-3527eb3e75bb"
 tags = {
    Name = "ICCW_PROD_MICROSERVICES_APPLICATION_2_DOCKER_VOL"
    Docker = "True"
  }
}

############ Brava/Index Volumes ##################

resource "aws_volume_attachment" "COMMONWELL_PROD_EXSTREAM_CONTENT_BRAVA_WIN_EC2_EBS_ATTACHMENT" {
  device_name = "/dev/sdh"
  volume_id   = "${aws_ebs_volume.COMMONWELL_PROD_EXSTREAM_CONTENT_BRAVA_WIN_EC2_EBS.id}"
  instance_id = "${aws_instance.ICCW_EXSTREAM_CONTENT_BRAVA_SERVER.id}"
}

resource "aws_ebs_volume" "COMMONWELL_PROD_EXSTREAM_CONTENT_BRAVA_WIN_EC2_EBS" {
  availability_zone = "ca-central-1a"
  size              = 500
  encrypted         = true
  kms_key_id        = "arn:aws:kms:ca-central-1:942574907772:key/cada27fc-e5f9-4f1c-b85a-3527eb3e75bb"
 tags = {
    Name = "COMMONWELL_PROD_EXSTREAM_CONTENT_BRAVA_WIN_EC2_VOL"
  }
}

resource "aws_volume_attachment" "COMMONWELL_PROD_EXSTREAM_CONTENT_INDEX_WIN_EC2_EBS_ATTACHMENT" {
  device_name = "/dev/sdh"
  volume_id   = "${aws_ebs_volume.COMMONWELL_PROD_EXSTREAM_CONTENT_INDEX_WIN_EC2_EBS.id}"
  instance_id = "${aws_instance.ICCW_EXSTREAM_CONTENT_INDEX_SERVER.id}"
}

resource "aws_ebs_volume" "COMMONWELL_PROD_EXSTREAM_CONTENT_INDEX_WIN_EC2_EBS" {
  availability_zone = "ca-central-1a"
  size              = 500
  encrypted         = true
  kms_key_id        = "arn:aws:kms:ca-central-1:942574907772:key/cada27fc-e5f9-4f1c-b85a-3527eb3e75bb"
 tags = {
    Name = "COMMONWELL_PROD_EXSTREAM_CONTENT_INDEX_WIN_EC2_EBS"
  }
}
