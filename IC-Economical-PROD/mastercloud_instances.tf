# #=======================================================================
# # PROD01
# #=======================================================================

######### GUIDEWIRE ##############
resource "aws_instance" "ICECONOMICAL_PROD01_GUIDEWIRE01_SUITE" {
  ami = "${var.ami_id_amazonlinuxbase20200506}"
  vpc_security_group_ids = [
    "${aws_security_group.SG_GUIDEWIRE.id}",
    "${aws_security_group.SG_JENKINS_GUIDEWIRE_HEALTH_CHECK.id}",
    "${aws_security_group.SG_GUIDEWIRE_CLUSTER_NBIO_EVENT_QUEUE.id}",
    "${aws_security_group.SG_AVIATRIX_SSH.id}",
    "${aws_security_group.SG_JENKINS_CLIENT_SSH.id}"
  ]
  subnet_id = "${var.economical-prod-private-1a-subnet}"
  instance_type = "m5a.xlarge"
  key_name = "EconomicalGuidewirePRD"
  associate_public_ip_address = false
  iam_instance_profile = "CMS-SSMInstanceRole"
  ebs_optimized = "true"

  root_block_device {
    volume_size = "100"
  }

  lifecycle {
    prevent_destroy = true
    ignore_changes = [
      "tags.%",
      "tags.BILLINGCODE",
      "tags.BILLINGCONTACT",
      "tags.CLIENT",
      "tags.COUNTRY",
      "tags.CSCLASS",
      "tags.CSQUAL",
      "tags.CSTYPE",
      "tags.ENVIRONMENT",
      "tags.FUNCTION",
      "tags.GROUPCONTACT",
      "tags.MEMBERFIRM",
      "tags.PRIMARYCONTACT",
      "tags.SECONDARYCONTACT",
      "tags.AccountName"]
  }

  tags = {
    Client = "Economical"
    Contract = "MANAGED_SERVICES"
    PatchGroup = "AmazonLinux2-AzA-Group1"
    Owner = "robhatnagar@deloitte.ca"
    ManagedBy = "CMS-TF"
    SolutionName = "InsurCloud Economical"
    Environment = "Prod"
    ApplicationName = "guidewire, docker"
    DataClassification = "PII, PHI"
    DLM-Backup = "Yes"
    DLM-Retention = "90"
    VendorManaged = "False"
    ResourceType = "Client"
    ResourceSeverity = "Critical"
    Name = "A-MC-CAC-D-A003-ECONOMICAL-PROD01-GW01-SUITE-LNX-EC2"
    AccountID = "${data.aws_caller_identity.current.account_id}"
    Role = "Container"
  }

  provisioner "local-exec" {
    command = "echo ${aws_instance.ICECONOMICAL_PROD01_GUIDEWIRE01_SUITE.private_ip} >> dev_hosts.txt"
  }
}

output "ICECONOMICAL_PROD01_GUIDEWIRE01_SUITE_PRIVATEIP" {
  value = "${aws_instance.ICECONOMICAL_PROD01_GUIDEWIRE01_SUITE.private_ip}"
}

resource "aws_instance" "ICECONOMICAL_PROD01_GUIDEWIRE02_SUITE" {
  ami = "${var.ami_id_amazonlinuxbase20200506}"
  vpc_security_group_ids = [
    "${aws_security_group.SG_GUIDEWIRE.id}",
    "${aws_security_group.SG_JENKINS_GUIDEWIRE_HEALTH_CHECK.id}",
    "${aws_security_group.SG_GUIDEWIRE_CLUSTER_NBIO_EVENT_QUEUE.id}",
    "${aws_security_group.SG_AVIATRIX_SSH.id}",
    "${aws_security_group.SG_JENKINS_CLIENT_SSH.id}"
  ]
  subnet_id = "${var.economical-prod-private-1b-subnet}"
  instance_type = "m5a.xlarge"
  key_name = "EconomicalGuidewirePRD"
  associate_public_ip_address = false
  iam_instance_profile = "CMS-SSMInstanceRole"
  ebs_optimized = "true"

  root_block_device {
    volume_size = "100"
  }

  lifecycle {
    prevent_destroy = true
    ignore_changes = [
      "tags.%",
      "tags.BILLINGCODE",
      "tags.BILLINGCONTACT",
      "tags.CLIENT",
      "tags.COUNTRY",
      "tags.CSCLASS",
      "tags.CSQUAL",
      "tags.CSTYPE",
      "tags.ENVIRONMENT",
      "tags.FUNCTION",
      "tags.GROUPCONTACT",
      "tags.MEMBERFIRM",
      "tags.PRIMARYCONTACT",
      "tags.SECONDARYCONTACT",
      "tags.AccountName"]
  }

  tags = {
    Client = "Economical"
    Contract = "MANAGED_SERVICES"
    PatchGroup = "AmazonLinux2-AzB-Group1"
    Owner = "robhatnagar@deloitte.ca"
    ManagedBy = "CMS-TF"
    SolutionName = "InsurCloud Economical"
    Environment = "Prod"
    ApplicationName = "guidewire, docker"
    DataClassification = "PII, PHI"
    DLM-Backup = "Yes"
    DLM-Retention = "90"
    VendorManaged = "False"
    ResourceType = "Client"
    ResourceSeverity = "Critical"
    Name = "A-MC-CAC-D-A003-ECONOMICAL-PROD01-GW02-SUITE-LNX-EC2"
    AccountID = "${data.aws_caller_identity.current.account_id}"
    Role = "Container"
  }

  provisioner "local-exec" {
    command = "echo ${aws_instance.ICECONOMICAL_PROD01_GUIDEWIRE02_SUITE.private_ip} >> dev_hosts.txt"
  }
}

output "ICECONOMICAL_PROD01_GUIDEWIRE02_SUITE_PRIVATEIP" {
  value = "${aws_instance.ICECONOMICAL_PROD01_GUIDEWIRE02_SUITE.private_ip}"
}

resource "aws_instance" "ICECONOMICAL_PROD01_GUIDEWIRE03_SUITE" {
  ami = "${var.ami_id_amazonlinuxbase20200506}"
  vpc_security_group_ids = [
    "${aws_security_group.SG_GUIDEWIRE.id}",
    "${aws_security_group.SG_JENKINS_GUIDEWIRE_HEALTH_CHECK.id}",
    "${aws_security_group.SG_GUIDEWIRE_CLUSTER_NBIO_EVENT_QUEUE.id}",
    "${aws_security_group.SG_AVIATRIX_SSH.id}",
    "${aws_security_group.SG_JENKINS_CLIENT_SSH.id}"
  ]
  subnet_id = "${var.economical-prod-private-1a-subnet}"
  instance_type = "m5a.xlarge"
  key_name = "EconomicalGuidewirePRD"
  associate_public_ip_address = false
  iam_instance_profile = "CMS-SSMInstanceRole"
  ebs_optimized = "true"

  root_block_device {
    volume_size = "100"
  }

  lifecycle {
    prevent_destroy = true
    ignore_changes = [
      "tags.%",
      "tags.BILLINGCODE",
      "tags.BILLINGCONTACT",
      "tags.CLIENT",
      "tags.COUNTRY",
      "tags.CSCLASS",
      "tags.CSQUAL",
      "tags.CSTYPE",
      "tags.ENVIRONMENT",
      "tags.FUNCTION",
      "tags.GROUPCONTACT",
      "tags.MEMBERFIRM",
      "tags.PRIMARYCONTACT",
      "tags.SECONDARYCONTACT",
      "tags.AccountName"]
  }

  tags = {
    Client = "Economical"
    Contract = "MANAGED_SERVICES"
    PatchGroup = "AmazonLinux2-AzA-Group1"
    Owner = "robhatnagar@deloitte.ca"
    ManagedBy = "CMS-TF"
    SolutionName = "InsurCloud Economical"
    Environment = "Prod"
    ApplicationName = "guidewire, docker"
    DataClassification = "PII, PHI"
    DLM-Backup = "Yes"
    DLM-Retention = "90"
    VendorManaged = "False"
    ResourceType = "Client"
    ResourceSeverity = "Critical"
    Name = "A-MC-CAC-D-A003-ECONOMICAL-PROD01-GW03-SUITE-LNX-EC2"
    AccountID = "${data.aws_caller_identity.current.account_id}"
    Role = "Container"
  }

  provisioner "local-exec" {
    command = "echo ${aws_instance.ICECONOMICAL_PROD01_GUIDEWIRE03_SUITE.private_ip} >> dev_hosts.txt"
  }
}

output "ICECONOMICAL_PROD01_GUIDEWIRE03_SUITE_PRIVATEIP" {
  value = "${aws_instance.ICECONOMICAL_PROD01_GUIDEWIRE03_SUITE.private_ip}"
}

resource "aws_instance" "ICECONOMICAL_PROD01_GUIDEWIRE04_SUITE" {
  ami = "${var.ami_id_amazonlinuxbase20200506}"
  vpc_security_group_ids = [
    "${aws_security_group.SG_GUIDEWIRE.id}",
    "${aws_security_group.SG_JENKINS_GUIDEWIRE_HEALTH_CHECK.id}",
    "${aws_security_group.SG_GUIDEWIRE_CLUSTER_NBIO_EVENT_QUEUE.id}",
    "${aws_security_group.SG_AVIATRIX_SSH.id}",
    "${aws_security_group.SG_JENKINS_CLIENT_SSH.id}"
  ]
  subnet_id = "${var.economical-prod-private-1b-subnet}"
  instance_type = "m5a.xlarge"
  key_name = "EconomicalGuidewirePRD"
  associate_public_ip_address = false
  iam_instance_profile = "CMS-SSMInstanceRole"
  ebs_optimized = "true"

  root_block_device {
    volume_size = "100"
  }

  lifecycle {
    prevent_destroy = true
    ignore_changes = [
      "tags.%",
      "tags.BILLINGCODE",
      "tags.BILLINGCONTACT",
      "tags.CLIENT",
      "tags.COUNTRY",
      "tags.CSCLASS",
      "tags.CSQUAL",
      "tags.CSTYPE",
      "tags.ENVIRONMENT",
      "tags.FUNCTION",
      "tags.GROUPCONTACT",
      "tags.MEMBERFIRM",
      "tags.PRIMARYCONTACT",
      "tags.SECONDARYCONTACT",
      "tags.AccountName"]
  }

  tags = {
    Client = "Economical"
    Contract = "MANAGED_SERVICES"
    PatchGroup = "AmazonLinux2-AzB-Group1"
    Owner = "robhatnagar@deloitte.ca"
    ManagedBy = "CMS-TF"
    SolutionName = "InsurCloud Economical"
    Environment = "Prod"
    ApplicationName = "guidewire, docker"
    DataClassification = "PII, PHI"
    DLM-Backup = "Yes"
    DLM-Retention = "90"
    VendorManaged = "False"
    ResourceType = "Client"
    ResourceSeverity = "Critical"
    Name = "A-MC-CAC-D-A003-ECONOMICAL-PROD01-GW04-SUITE-LNX-EC2"
    AccountID = "${data.aws_caller_identity.current.account_id}"
    Role = "Container"
  }

  provisioner "local-exec" {
    command = "echo ${aws_instance.ICECONOMICAL_PROD01_GUIDEWIRE04_SUITE.private_ip} >> dev_hosts.txt"
  }
}

output "ICECONOMICAL_PROD01_GUIDEWIRE04_SUITE_PRIVATEIP" {
  value = "${aws_instance.ICECONOMICAL_PROD01_GUIDEWIRE04_SUITE.private_ip}"
}

data "template_file" "ECO_PROD01_DOMAIN_JOIN_GW_DATABASE01" {
  template = "${file("IC-Economical-PROD/windowsdata.tpl")}"

  vars = {
    computer_name = "PROD01-GW-DB01"
    region = "${var.region}"
    domain = "${var.ad_domain}"
    dns1 = "${var.ad_dns1}"
    dns2 = "${var.ad_dns2}"
    dns3 = "${var.ad_dns3}"
    ouselection = "${var.ou_name}"
    pwordparameter = "${var.pwordparameter}"
    unameparameter = "${var.unameparameter}"
  }
}

resource "aws_instance" "ICECONOMICAL_PROD01_GUIDEWIRE_MSSQL_DATABASE01" {
  ami = "${var.ami_id_windows2019base}"
  vpc_security_group_ids = [
    "${aws_security_group.SG_AVIATRIX_RDP.id}",
    "${aws_security_group.SG_MSSQL_DB.id}",
    "${aws_security_group.SG_WIN_CLUSTER_01.id}",
    "${aws_security_group.SG_WIN_CLUSTER_02.id}"
  ]
  subnet_id = "${var.economical-prod-database-1a-subnet}"
  instance_type = "r5a.large"
  key_name = "EconomicalGuidewirePRD"
  associate_public_ip_address = false
  iam_instance_profile = "CMS-SSMInstanceRole"
  ebs_optimized = "true"

  root_block_device {
    volume_size = "100"
  }

  user_data = "${data.template_file.ECO_PROD01_DOMAIN_JOIN_GW_DATABASE01.rendered}"

  lifecycle {
    prevent_destroy = true
    ignore_changes = [
      "tags.%",
      "tags.BILLINGCODE",
      "tags.BILLINGCONTACT",
      "tags.CLIENT",
      "tags.COUNTRY",
      "tags.CSCLASS",
      "tags.CSQUAL",
      "tags.CSTYPE",
      "tags.ENVIRONMENT",
      "tags.FUNCTION",
      "tags.GROUPCONTACT",
      "tags.MEMBERFIRM",
      "tags.PRIMARYCONTACT",
      "tags.SECONDARYCONTACT",
      "tags.AccountName"]
  }

  tags = {
    Client = "Economical"
    Contract = "MANAGED_SERVICES"
    PatchGroup = "Windows-AzA-Group1"
    Owner = "robhatnagar@deloitte.ca"
    ManagedBy = "CMS-TF"
    SolutionName = "InsurCloud Economical"
    Environment = "Prod"
    ApplicationName = "mssql, guidewire"
    DataClassification = "PII, PHI"
    DLM-Backup = "Yes"
    DLM-Retention = "90"
    VendorManaged = "False"
    ResourceType = "Client"
    ResourceSeverity = "Critical"
    Name = "A-MC-CAC-D-A003-ECONOMICAL-PROD01-GW-MSSQL-DB01-WIN-EC2"
    AccountID = "${data.aws_caller_identity.current.account_id}"
  }
}

output "ICECONOMICAL_PROD01_GUIDEWIRE_DATABASE01_PRIVATEIP" {
  value = "${aws_instance.ICECONOMICAL_PROD01_GUIDEWIRE_MSSQL_DATABASE01.private_ip}"
}

data "template_file" "ECO_PROD01_DOMAIN_JOIN_GW_DATABASE02" {
  template = "${file("IC-Economical-PROD/windowsdata.tpl")}"

  vars = {
    computer_name = "PROD01-GW-DB02"
    region = "${var.region}"
    domain = "${var.ad_domain}"
    dns1 = "${var.ad_dns1}"
    dns2 = "${var.ad_dns2}"
    dns3 = "${var.ad_dns3}"
    ouselection = "${var.ou_name}"
    pwordparameter = "${var.pwordparameter}"
    unameparameter = "${var.unameparameter}"
  }
}

resource "aws_instance" "ICECONOMICAL_PROD01_GUIDEWIRE_MSSQL_DATABASE02" {
  ami = "${var.ami_id_windows2019base}"
  vpc_security_group_ids = [
    "${aws_security_group.SG_AVIATRIX_RDP.id}",
    "${aws_security_group.SG_MSSQL_DB.id}",
    "${aws_security_group.SG_WIN_CLUSTER_01.id}",
    "${aws_security_group.SG_WIN_CLUSTER_02.id}"
  ]
  subnet_id = "${var.economical-prod-database-1b-subnet}"
  instance_type = "r5a.large"
  key_name = "EconomicalGuidewirePRD"
  associate_public_ip_address = false
  iam_instance_profile = "CMS-SSMInstanceRole"
  ebs_optimized = "true"

  root_block_device {
    volume_size = "100"
  }

  user_data = "${data.template_file.ECO_PROD01_DOMAIN_JOIN_GW_DATABASE02.rendered}"

  lifecycle {
    prevent_destroy = true
    ignore_changes = [
      "tags.%",
      "tags.BILLINGCODE",
      "tags.BILLINGCONTACT",
      "tags.CLIENT",
      "tags.COUNTRY",
      "tags.CSCLASS",
      "tags.CSQUAL",
      "tags.CSTYPE",
      "tags.ENVIRONMENT",
      "tags.FUNCTION",
      "tags.GROUPCONTACT",
      "tags.MEMBERFIRM",
      "tags.PRIMARYCONTACT",
      "tags.SECONDARYCONTACT",
      "tags.AccountName"]
  }

  tags = {
    Client = "Economical"
    Contract = "MANAGED_SERVICES"
    PatchGroup = "Windows-AzB-Group1"
    Owner = "robhatnagar@deloitte.ca"
    ManagedBy = "CMS-TF"
    SolutionName = "InsurCloud Economical"
    Environment = "Prod"
    ApplicationName = "mssql, guidewire"
    DataClassification = "PII, PHI"
    DLM-Backup = "Yes"
    DLM-Retention = "90"
    VendorManaged = "False"
    ResourceType = "Client"
    ResourceSeverity = "Critical"
    Name = "A-MC-CAC-D-A003-ECONOMICAL-PROD01-GW-MSSQL-DB02-WIN-EC2"
    AccountID = "${data.aws_caller_identity.current.account_id}"
  }
}

output "ICECONOMICAL_PROD01_GUIDEWIRE_DATABASE02_PRIVATEIP" {
  value = "${aws_instance.ICECONOMICAL_PROD01_GUIDEWIRE_MSSQL_DATABASE02.private_ip}"
}

######### MICROSERVICES ###########
resource "aws_instance" "ICECONOMICAL_PROD01_MICROSERVICES_MANAGER01" {
  ami = "${var.ami_id_amazonlinuxbase20200506}"
  vpc_security_group_ids = [
    "${aws_security_group.SG_MICROSERVICES.id}",
    "${aws_security_group.SG_MONITORING.id}",
    "${aws_security_group.SG_AVIATRIX_SSH.id}",
    "${aws_security_group.SG_JENKINS_CLIENT_SSH.id}"
  ]
  subnet_id = "${var.economical-prod-private-1a-subnet}"
  instance_type = "t3a.small"
  key_name = "EconomicalMicroservicesPRD"
  associate_public_ip_address = false
  iam_instance_profile = "CMS-SSMInstanceRole"
  ebs_optimized = "true"

  root_block_device {
    volume_size = "16"
  }

  lifecycle {
    prevent_destroy = true
    ignore_changes = [
      "tags.%",
      "tags.BILLINGCODE",
      "tags.BILLINGCONTACT",
      "tags.CLIENT",
      "tags.COUNTRY",
      "tags.CSCLASS",
      "tags.CSQUAL",
      "tags.CSTYPE",
      "tags.ENVIRONMENT",
      "tags.FUNCTION",
      "tags.GROUPCONTACT",
      "tags.MEMBERFIRM",
      "tags.PRIMARYCONTACT",
      "tags.SECONDARYCONTACT",
      "tags.AccountName"]
  }

  tags = {
    Client = "Economical"
    Contract = "MANAGED_SERVICES"
    PatchGroup = "AmazonLinux2-AzA-Group2"
    Owner = "robhatnagar@deloitte.ca"
    ManagedBy = "CMS-TF"
    SolutionName = "InsurCloud Economical"
    Environment = "Prod"
    ApplicationName = "docker, microservices"
    DataClassification = "PII, PHI"
    DLM-Backup = "Yes"
    DLM-Retention = "90"
    VendorManaged = "False"
    ResourceType = "Client"
    ResourceSeverity = "Normal"
    Name = "A-MC-CAC-D-A003-ECONOMICAL-PROD01-MS-MANAGER01-LNX-EC2"
    AccountID = "${data.aws_caller_identity.current.account_id}"
    Role = "Container"
  }

  provisioner "local-exec" {
    command = "echo ${aws_instance.ICECONOMICAL_PROD01_MICROSERVICES_MANAGER01.private_ip} >> dev_hosts.txt"
  }
}

output "ICECONOMICAL_PROD01_MICROSERVICES_MANAGER01_PRIVATEIP" {
  value = "${aws_instance.ICECONOMICAL_PROD01_MICROSERVICES_MANAGER01.private_ip}"
}

resource "aws_instance" "ICECONOMICAL_PROD01_MICROSERVICES_MANAGER02" {
  ami = "${var.ami_id_amazonlinuxbase20200506}"
  vpc_security_group_ids = [
    "${aws_security_group.SG_MICROSERVICES.id}",
    "${aws_security_group.SG_MONITORING.id}",
    "${aws_security_group.SG_AVIATRIX_SSH.id}",
    "${aws_security_group.SG_JENKINS_CLIENT_SSH.id}"
  ]
  subnet_id = "${var.economical-prod-private-1b-subnet}"
  instance_type = "t3a.small"
  key_name = "EconomicalMicroservicesPRD"
  associate_public_ip_address = false
  iam_instance_profile = "CMS-SSMInstanceRole"
  ebs_optimized = "true"

  root_block_device {
    volume_size = "16"
  }

  lifecycle {
    prevent_destroy = true
    ignore_changes = [
      "tags.%",
      "tags.BILLINGCODE",
      "tags.BILLINGCONTACT",
      "tags.CLIENT",
      "tags.COUNTRY",
      "tags.CSCLASS",
      "tags.CSQUAL",
      "tags.CSTYPE",
      "tags.ENVIRONMENT",
      "tags.FUNCTION",
      "tags.GROUPCONTACT",
      "tags.MEMBERFIRM",
      "tags.PRIMARYCONTACT",
      "tags.SECONDARYCONTACT",
      "tags.AccountName"]
  }

  tags = {
    Client = "Economical"
    Contract = "MANAGED_SERVICES"
    PatchGroup = "AmazonLinux2-AzB-Group2"
    Owner = "robhatnagar@deloitte.ca"
    ManagedBy = "CMS-TF"
    SolutionName = "InsurCloud Economical"
    Environment = "Prod"
    ApplicationName = "docker, microservices"
    DataClassification = "PII, PHI"
    DLM-Backup = "Yes"
    DLM-Retention = "90"
    VendorManaged = "False"
    ResourceType = "Client"
    ResourceSeverity = "Normal"
    Name = "A-MC-CAC-D-A003-ECONOMICAL-PROD01-MS-MANAGER02-LNX-EC2"
    AccountID = "${data.aws_caller_identity.current.account_id}"
    Role = "Container"
  }

  provisioner "local-exec" {
    command = "echo ${aws_instance.ICECONOMICAL_PROD01_MICROSERVICES_MANAGER02.private_ip} >> dev_hosts.txt"
  }
}

output "ICECONOMICAL_PROD01_MICROSERVICES_MANAGER02_PRIVATEIP" {
  value = "${aws_instance.ICECONOMICAL_PROD01_MICROSERVICES_MANAGER02.private_ip}"
}

resource "aws_instance" "ICECONOMICAL_PROD01_MICROSERVICES_MANAGER03" {
  ami = "${var.ami_id_amazonlinuxbase20200506}"
  vpc_security_group_ids = [
    "${aws_security_group.SG_MICROSERVICES.id}",
    "${aws_security_group.SG_MONITORING.id}",
    "${aws_security_group.SG_AVIATRIX_SSH.id}",
    "${aws_security_group.SG_JENKINS_CLIENT_SSH.id}"
  ]
  subnet_id = "${var.economical-prod-private-1b-subnet}"
  instance_type = "t3a.small"
  key_name = "EconomicalMicroservicesPRD"
  associate_public_ip_address = false
  iam_instance_profile = "CMS-SSMInstanceRole"
  ebs_optimized = "true"

  root_block_device {
    volume_size = "16"
  }

  lifecycle {
    prevent_destroy = true
    ignore_changes = [
      "tags.%",
      "tags.BILLINGCODE",
      "tags.BILLINGCONTACT",
      "tags.CLIENT",
      "tags.COUNTRY",
      "tags.CSCLASS",
      "tags.CSQUAL",
      "tags.CSTYPE",
      "tags.ENVIRONMENT",
      "tags.FUNCTION",
      "tags.GROUPCONTACT",
      "tags.MEMBERFIRM",
      "tags.PRIMARYCONTACT",
      "tags.SECONDARYCONTACT",
      "tags.AccountName"]
  }

  tags = {
    Client = "Economical"
    Contract = "MANAGED_SERVICES"
    PatchGroup = "AmazonLinux2-AzB-Group3"
    Owner = "robhatnagar@deloitte.ca"
    ManagedBy = "CMS-TF"
    SolutionName = "InsurCloud Economical"
    Environment = "Prod"
    ApplicationName = "docker, microservices"
    DataClassification = "PII, PHI"
    DLM-Backup = "Yes"
    DLM-Retention = "90"
    VendorManaged = "False"
    ResourceType = "Client"
    ResourceSeverity = "Normal"
    Name = "A-MC-CAC-D-A003-ECONOMICAL-PROD01-MS-MANAGER03-LNX-EC2"
    AccountID = "${data.aws_caller_identity.current.account_id}"
    Role = "Container"
  }

  provisioner "local-exec" {
    command = "echo ${aws_instance.ICECONOMICAL_PROD01_MICROSERVICES_MANAGER03.private_ip} >> dev_hosts.txt"
  }
}

output "ICECONOMICAL_PROD01_MICROSERVICES_MANAGER03_PRIVATEIP" {
  value = "${aws_instance.ICECONOMICAL_PROD01_MICROSERVICES_MANAGER03.private_ip}"
}

resource "aws_instance" "ICECONOMICAL_PROD01_MICROSERVICES_APPLICATION01" {
  ami = "${var.ami_id_amazonlinuxbase20200506}"
  vpc_security_group_ids = [
    "${aws_security_group.SG_MICROSERVICES.id}",
    "${aws_security_group.SG_MONITORING.id}",
    "${aws_security_group.SG_AVIATRIX_SSH.id}",
    "${aws_security_group.SG_JENKINS_CLIENT_SSH.id}"
  ]
  subnet_id = "${var.economical-prod-private-1a-subnet}"
  instance_type = "t3a.2xlarge"
  key_name = "EconomicalMicroservicesPRD"
  associate_public_ip_address = false
  iam_instance_profile = "CMS-SSMInstanceRole"
  ebs_optimized = "true"

  root_block_device {
    volume_size = "100"
  }

  lifecycle {
    prevent_destroy = true
    ignore_changes = [
      "tags.%",
      "tags.BILLINGCODE",
      "tags.BILLINGCONTACT",
      "tags.CLIENT",
      "tags.COUNTRY",
      "tags.CSCLASS",
      "tags.CSQUAL",
      "tags.CSTYPE",
      "tags.ENVIRONMENT",
      "tags.FUNCTION",
      "tags.GROUPCONTACT",
      "tags.MEMBERFIRM",
      "tags.PRIMARYCONTACT",
      "tags.SECONDARYCONTACT",
      "tags.AccountName"]
  }

  tags = {
    Client = "Economical"
    Contract = "MANAGED_SERVICES"
    PatchGroup = "AmazonLinux2-AzA-Group1"
    Owner = "robhatnagar@deloitte.ca"
    ManagedBy = "CMS-TF"
    SolutionName = "InsurCloud Economical"
    Environment = "Prod"
    ApplicationName = "docker, microservices"
    DataClassification = "PII, PHI"
    DLM-Backup = "Yes"
    DLM-Retention = "90"
    VendorManaged = "False"
    ResourceType = "Client"
    ResourceSeverity = "Critical"
    Name = "A-MC-CAC-D-A003-ECONOMICAL-PROD01-MS-APP01-LNX-EC2"
    AccountID = "${data.aws_caller_identity.current.account_id}"
    Role = "Container"
  }

  provisioner "local-exec" {
    command = "echo ${aws_instance.ICECONOMICAL_PROD01_MICROSERVICES_APPLICATION01.private_ip} >> dev_hosts.txt"
  }
}

output "ICECONOMICAL_PROD01_MICROSERVICES_APPLICATION01_PRIVATEIP" {
  value = "${aws_instance.ICECONOMICAL_PROD01_MICROSERVICES_APPLICATION01.private_ip}"
}

resource "aws_instance" "ICECONOMICAL_PROD01_MICROSERVICES_APPLICATION02" {
  ami = "${var.ami_id_amazonlinuxbase20200506}"
  vpc_security_group_ids = [
    "${aws_security_group.SG_MICROSERVICES.id}",
    "${aws_security_group.SG_MONITORING.id}",
    "${aws_security_group.SG_AVIATRIX_SSH.id}",
    "${aws_security_group.SG_JENKINS_CLIENT_SSH.id}"
  ]
  subnet_id = "${var.economical-prod-private-1b-subnet}"
  instance_type = "t3a.2xlarge"
  key_name = "EconomicalMicroservicesPRD"
  associate_public_ip_address = false
  iam_instance_profile = "CMS-SSMInstanceRole"
  ebs_optimized = "true"

  root_block_device {
    volume_size = "100"
  }

  lifecycle {
    prevent_destroy = true
    ignore_changes = [
      "tags.%",
      "tags.BILLINGCODE",
      "tags.BILLINGCONTACT",
      "tags.CLIENT",
      "tags.COUNTRY",
      "tags.CSCLASS",
      "tags.CSQUAL",
      "tags.CSTYPE",
      "tags.ENVIRONMENT",
      "tags.FUNCTION",
      "tags.GROUPCONTACT",
      "tags.MEMBERFIRM",
      "tags.PRIMARYCONTACT",
      "tags.SECONDARYCONTACT",
      "tags.AccountName"]
  }

  tags = {
    Client = "Economical"
    Contract = "MANAGED_SERVICES"
    PatchGroup = "AmazonLinux2-AzB-Group1"
    Owner = "robhatnagar@deloitte.ca"
    ManagedBy = "CMS-TF"
    SolutionName = "InsurCloud Economical"
    Environment = "Prod"
    ApplicationName = "docker, microservices"
    DataClassification = "PII, PHI"
    DLM-Backup = "Yes"
    DLM-Retention = "90"
    VendorManaged = "False"
    ResourceType = "Client"
    ResourceSeverity = "Critical"
    Name = "A-MC-CAC-D-A003-ECONOMICAL-PROD01-MS-APP02-LNX-EC2"
    AccountID = "${data.aws_caller_identity.current.account_id}"
    Role = "Container"
  }

  provisioner "local-exec" {
    command = "echo ${aws_instance.ICECONOMICAL_PROD01_MICROSERVICES_APPLICATION02.private_ip} >> dev_hosts.txt"
  }
}

output "ICECONOMICAL_PROD01_MICROSERVICES_APPLICATION02_PRIVATEIP" {
  value = "${aws_instance.ICECONOMICAL_PROD01_MICROSERVICES_APPLICATION02.private_ip}"
}

######### RPA ###########
data "template_file" "ECO_PROD01_DOMAIN_JOIN_RPA_APP01" {
  template = "${file("IC-Economical-PROD/windowsdata.tpl")}"

  vars = {
    computer_name = "PROD01-RPA-A01"
    region = "${var.region}"
    domain = "${var.ad_domain}"
    dns1 = "${var.ad_dns1}"
    dns2 = "${var.ad_dns2}"
    dns3 = "${var.ad_dns3}"
    ouselection = "${var.ou_name}"
    pwordparameter = "${var.pwordparameter}"
    unameparameter = "${var.unameparameter}"
  }
}

resource "aws_instance" "ICECONOMICAL_PROD01_RPA_APP01" {
  ami = "${var.ami_id_windows2016base}"
  vpc_security_group_ids = [
    "${aws_security_group.SG_AVIATRIX_RDP.id}",
    "${aws_security_group.SG_RPA.id}",
    "${aws_security_group.SG_RPA_APP.id}"
  ]
  subnet_id = "${var.economical-prod-private-1a-subnet}"
  instance_type = "t3a.medium"
  key_name = "EconomicalRPAPRD"
  associate_public_ip_address = false
  iam_instance_profile = "CMS-SSMInstanceRole"
  ebs_optimized = "true"

  root_block_device {
    volume_size = "50"
  }

  user_data = "${data.template_file.ECO_PROD01_DOMAIN_JOIN_RPA_APP01.rendered}"

  lifecycle {
    prevent_destroy = true
    ignore_changes = [
      "tags.%",
      "tags.BILLINGCODE",
      "tags.BILLINGCONTACT",
      "tags.CLIENT",
      "tags.COUNTRY",
      "tags.CSCLASS",
      "tags.CSQUAL",
      "tags.CSTYPE",
      "tags.ENVIRONMENT",
      "tags.FUNCTION",
      "tags.GROUPCONTACT",
      "tags.MEMBERFIRM",
      "tags.PRIMARYCONTACT",
      "tags.SECONDARYCONTACT",
      "tags.AccountName"]
  }

  tags = {
    Client = "Economical"
    Contract = "MANAGED_SERVICES"
    PatchGroup = "Windows-AzA-Group1"
    Owner = "robhatnagar@deloitte.ca"
    ManagedBy = "CMS-TF"
    SolutionName = "InsurCloud Economical"
    Environment = "Prod"
    ApplicationName = "rpa, blueprism"
    DataClassification = "PII, PHI"
    DLM-Backup = "Yes"
    DLM-Retention = "90"
    VendorManaged = "False"
    ResourceType = "Client"
    ResourceSeverity = "Critical"
    Name = "A-MC-CAC-D-A003-ECONOMICAL-PROD01-RPA-APP01-WIN-EC2"
    AccountID = "${data.aws_caller_identity.current.account_id}"
  }
}

output "ICECONOMICAL_PROD01_RPA_APP01_PRIVATEIP" {
  value = "${aws_instance.ICECONOMICAL_PROD01_RPA_APP01.private_ip}"
}

data "template_file" "ECO_PROD01_DOMAIN_JOIN_RPA_APP02" {
  template = "${file("IC-Economical-PROD/windowsdata.tpl")}"

  vars = {
    computer_name = "PROD01-RPA-A02"
    region = "${var.region}"
    domain = "${var.ad_domain}"
    dns1 = "${var.ad_dns1}"
    dns2 = "${var.ad_dns2}"
    dns3 = "${var.ad_dns3}"
    ouselection = "${var.ou_name}"
    pwordparameter = "${var.pwordparameter}"
    unameparameter = "${var.unameparameter}"
  }
}

resource "aws_instance" "ICECONOMICAL_PROD01_RPA_APP02" {
  ami = "${var.ami_id_windows2016base}"
  vpc_security_group_ids = [
    "${aws_security_group.SG_AVIATRIX_RDP.id}",
    "${aws_security_group.SG_RPA.id}",
    "${aws_security_group.SG_RPA_APP.id}"
  ]
  subnet_id = "${var.economical-prod-private-1b-subnet}"
  instance_type = "t3a.medium"
  key_name = "EconomicalRPAPRD"
  associate_public_ip_address = false
  iam_instance_profile = "CMS-SSMInstanceRole"
  ebs_optimized = "true"

  root_block_device {
    volume_size = "50"
  }

  user_data = "${data.template_file.ECO_PROD01_DOMAIN_JOIN_RPA_APP02.rendered}"

  lifecycle {
    prevent_destroy = true
    ignore_changes = [
      "tags.%",
      "tags.BILLINGCODE",
      "tags.BILLINGCONTACT",
      "tags.CLIENT",
      "tags.COUNTRY",
      "tags.CSCLASS",
      "tags.CSQUAL",
      "tags.CSTYPE",
      "tags.ENVIRONMENT",
      "tags.FUNCTION",
      "tags.GROUPCONTACT",
      "tags.MEMBERFIRM",
      "tags.PRIMARYCONTACT",
      "tags.SECONDARYCONTACT",
      "tags.AccountName"]
  }

  tags = {
    Client = "Economical"
    Contract = "MANAGED_SERVICES"
    PatchGroup = "Windows-AzB-Group1"
    Owner = "robhatnagar@deloitte.ca"
    ManagedBy = "CMS-TF"
    SolutionName = "InsurCloud Economical"
    Environment = "Prod"
    ApplicationName = "rpa, blueprism"
    DataClassification = "PII, PHI"
    DLM-Backup = "Yes"
    DLM-Retention = "90"
    VendorManaged = "False"
    ResourceType = "Client"
    ResourceSeverity = "Critical"
    Name = "A-MC-CAC-D-A003-ECONOMICAL-PROD01-RPA-APP02-WIN-EC2"
    AccountID = "${data.aws_caller_identity.current.account_id}"
  }
}

output "ICECONOMICAL_PROD01_RPA_APP02_PRIVATEIP" {
  value = "${aws_instance.ICECONOMICAL_PROD01_RPA_APP02.private_ip}"
}

data "template_file" "ECO_PROD01_DOMAIN_JOIN_INT01" {
  template = "${file("IC-Economical-PROD/windowsdata.tpl")}"

  vars = {
    computer_name = "PROD01-RPA-I01"
    region = "${var.region}"
    domain = "${var.ad_domain}"
    dns1 = "${var.ad_dns1}"
    dns2 = "${var.ad_dns2}"
    dns3 = "${var.ad_dns3}"
    ouselection = "${var.ou_name}"
    pwordparameter = "${var.pwordparameter}"
    unameparameter = "${var.unameparameter}"
  }
}

resource "aws_instance" "ICECONOMICAL_PROD01_RPA_INT01" {
  ami = "${var.ami_id_windows2016base}"
  vpc_security_group_ids = [
    "${aws_security_group.SG_AVIATRIX_RDP.id}",
    "${aws_security_group.SG_RPA.id}",
    "${aws_security_group.SG_RPA_APP.id}"
  ]
  subnet_id = "${var.economical-prod-private-1a-subnet}"
  instance_type = "t3a.medium"
  key_name = "EconomicalRPAPRD"
  associate_public_ip_address = false
  iam_instance_profile = "CMS-SSMInstanceRole"
  ebs_optimized = "true"

  root_block_device {
    volume_size = "50"
  }

  user_data = "${data.template_file.ECO_PROD01_DOMAIN_JOIN_INT01.rendered}"

  lifecycle {
    prevent_destroy = true
    ignore_changes = [
      "tags.%",
      "tags.BILLINGCODE",
      "tags.BILLINGCONTACT",
      "tags.CLIENT",
      "tags.COUNTRY",
      "tags.CSCLASS",
      "tags.CSQUAL",
      "tags.CSTYPE",
      "tags.ENVIRONMENT",
      "tags.FUNCTION",
      "tags.GROUPCONTACT",
      "tags.MEMBERFIRM",
      "tags.PRIMARYCONTACT",
      "tags.SECONDARYCONTACT",
      "tags.AccountName"]
  }

  tags = {
    Client = "Economical"
    Contract = "MANAGED_SERVICES"
    PatchGroup = "Windows-AzA-Group1"
    Owner = "robhatnagar@deloitte.ca"
    ManagedBy = "CMS-TF"
    SolutionName = "InsurCloud Economical"
    Environment = "Prod"
    ApplicationName = "rpa, blueprism"
    DataClassification = "PII, PHI"
    DLM-Backup = "Yes"
    DLM-Retention = "90"
    VendorManaged = "False"
    ResourceType = "Client"
    ResourceSeverity = "Normal"
    Name = "A-MC-CAC-D-A003-ECONOMICAL-PROD01-RPA-INT01-WIN-EC2"
    AccountID = "${data.aws_caller_identity.current.account_id}"
  }
}

output "ICECONOMICAL_PROD01_RPA_INT01_PRIVATEIP" {
  value = "${aws_instance.ICECONOMICAL_PROD01_RPA_INT01.private_ip}"
}

data "template_file" "ECO_PROD01_DOMAIN_JOIN_RPA_C01" {
  template = "${file("IC-Economical-PROD/windowsdata.tpl")}"

  vars = {
    computer_name = "PROD01-RPA-C01"
    region = "${var.region}"
    domain = "${var.ad_domain}"
    dns1 = "${var.ad_dns1}"
    dns2 = "${var.ad_dns2}"
    dns3 = "${var.ad_dns3}"
    ouselection = "${var.ou_name}"
    pwordparameter = "${var.pwordparameter}"
    unameparameter = "${var.unameparameter}"
  }
}

resource "aws_instance" "ICECONOMICAL_PROD01_RPA_CLIENT01" {
  ami = "${var.ami_id_windows2016base}"
  vpc_security_group_ids = [
    "${aws_security_group.SG_AVIATRIX_RDP.id}",
    "${aws_security_group.SG_RPA.id}",
    "${aws_security_group.SG_RPA_CLIENT.id}"
  ]
  subnet_id = "${var.economical-prod-private-1a-subnet}"
  instance_type = "t3a.medium"
  key_name = "EconomicalRPAPRD"
  associate_public_ip_address = false
  iam_instance_profile = "CMS-SSMInstanceRole"
  ebs_optimized = "true"

  root_block_device {
    volume_size = "50"
  }

  user_data = "${data.template_file.ECO_PROD01_DOMAIN_JOIN_RPA_C01.rendered}"

  lifecycle {
    prevent_destroy = true
    ignore_changes = [
      "tags.%",
      "tags.BILLINGCODE",
      "tags.BILLINGCONTACT",
      "tags.CLIENT",
      "tags.COUNTRY",
      "tags.CSCLASS",
      "tags.CSQUAL",
      "tags.CSTYPE",
      "tags.ENVIRONMENT",
      "tags.FUNCTION",
      "tags.GROUPCONTACT",
      "tags.MEMBERFIRM",
      "tags.PRIMARYCONTACT",
      "tags.SECONDARYCONTACT",
      "tags.AccountName"]
  }

  tags = {
    Client = "Economical"
    Contract = "MANAGED_SERVICES"
    PatchGroup = "Windows-AzA-Group1"
    Owner = "robhatnagar@deloitte.ca"
    ManagedBy = "CMS-TF"
    SolutionName = "InsurCloud Economical"
    Environment = "Prod"
    ApplicationName = "rpa, blueprism"
    DataClassification = "PII, PHI"
    DLM-Backup = "Yes"
    DLM-Retention = "90"
    VendorManaged = "False"
    ResourceType = "Client"
    ResourceSeverity = "Critical"
    Name = "A-MC-CAC-D-A003-ECONOMICAL-PROD01-RPA-CLIENT01-WIN-EC2"
    AccountID = "${data.aws_caller_identity.current.account_id}"
  }
}

output "ICECONOMICAL_PROD01_RPA_CLIENT01_PRIVATEIP" {
  value = "${aws_instance.ICECONOMICAL_PROD01_RPA_CLIENT01.private_ip}"
}

data "template_file" "ECO_PROD01_DOMAIN_JOIN_RPA_C02" {
  template = "${file("IC-Economical-PROD/windowsdata.tpl")}"

  vars = {
    computer_name = "PROD01-RPA-C02"
    region = "${var.region}"
    domain = "${var.ad_domain}"
    dns1 = "${var.ad_dns1}"
    dns2 = "${var.ad_dns2}"
    dns3 = "${var.ad_dns3}"
    ouselection = "${var.ou_name}"
    pwordparameter = "${var.pwordparameter}"
    unameparameter = "${var.unameparameter}"
  }
}

resource "aws_instance" "ICECONOMICAL_PROD01_RPA_CLIENT02" {
  ami = "${var.ami_id_windows2016base}"
  vpc_security_group_ids = [
    "${aws_security_group.SG_AVIATRIX_RDP.id}",
    "${aws_security_group.SG_RPA.id}",
    "${aws_security_group.SG_RPA_CLIENT.id}"
  ]
  subnet_id = "${var.economical-prod-private-1b-subnet}"
  instance_type = "t3a.medium"
  key_name = "EconomicalRPAPRD"
  associate_public_ip_address = false
  iam_instance_profile = "CMS-SSMInstanceRole"
  ebs_optimized = "true"

  root_block_device {
    volume_size = "50"
  }

  user_data = "${data.template_file.ECO_PROD01_DOMAIN_JOIN_RPA_C02.rendered}"

  lifecycle {
    prevent_destroy = true
    ignore_changes = [
      "tags.%",
      "tags.BILLINGCODE",
      "tags.BILLINGCONTACT",
      "tags.CLIENT",
      "tags.COUNTRY",
      "tags.CSCLASS",
      "tags.CSQUAL",
      "tags.CSTYPE",
      "tags.ENVIRONMENT",
      "tags.FUNCTION",
      "tags.GROUPCONTACT",
      "tags.MEMBERFIRM",
      "tags.PRIMARYCONTACT",
      "tags.SECONDARYCONTACT",
      "tags.AccountName"]
  }

  tags = {
    Client = "Economical"
    Contract = "MANAGED_SERVICES"
    PatchGroup = "Windows-AzB-Group1"
    Owner = "robhatnagar@deloitte.ca"
    ManagedBy = "CMS-TF"
    SolutionName = "InsurCloud Economical"
    Environment = "Prod"
    ApplicationName = "rpa, blueprism"
    DataClassification = "PII, PHI"
    DLM-Backup = "Yes"
    DLM-Retention = "90"
    VendorManaged = "False"
    ResourceType = "Client"
    ResourceSeverity = "Critical"
    Name = "A-MC-CAC-D-A003-ECONOMICAL-PROD01-RPA-CLIENT02-WIN-EC2"
    AccountID = "${data.aws_caller_identity.current.account_id}"
  }
}

output "ICECONOMICAL_PROD01_RPA_CLIENT02_PRIVATEIP" {
  value = "${aws_instance.ICECONOMICAL_PROD01_RPA_CLIENT02.private_ip}"
}

data "template_file" "ECO_PROD01_DOMAIN_JOIN_RPA_C03" {
  template = "${file("IC-Economical-PROD/windowsdata.tpl")}"

  vars = {
    computer_name = "PROD01-RPA-C03"
    region = "${var.region}"
    domain = "${var.ad_domain}"
    dns1 = "${var.ad_dns1}"
    dns2 = "${var.ad_dns2}"
    dns3 = "${var.ad_dns3}"
    ouselection = "${var.ou_name}"
    pwordparameter = "${var.pwordparameter}"
    unameparameter = "${var.unameparameter}"
  }
}

resource "aws_instance" "ICECONOMICAL_PROD01_RPA_CLIENT03" {
  ami = "${var.ami_id_windows2016base}"
  vpc_security_group_ids = [
    "${aws_security_group.SG_AVIATRIX_RDP.id}",
    "${aws_security_group.SG_RPA.id}",
    "${aws_security_group.SG_RPA_CLIENT.id}"
  ]
  subnet_id = "${var.economical-prod-private-1a-subnet}"
  instance_type = "t3a.medium"
  key_name = "EconomicalRPAPRD"
  associate_public_ip_address = false
  iam_instance_profile = "CMS-SSMInstanceRole"
  ebs_optimized = "true"

  root_block_device {
    volume_size = "50"
  }

  user_data = "${data.template_file.ECO_PROD01_DOMAIN_JOIN_RPA_C03.rendered}"

  lifecycle {
    prevent_destroy = true
    ignore_changes = [
      "tags.%",
      "tags.BILLINGCODE",
      "tags.BILLINGCONTACT",
      "tags.CLIENT",
      "tags.COUNTRY",
      "tags.CSCLASS",
      "tags.CSQUAL",
      "tags.CSTYPE",
      "tags.ENVIRONMENT",
      "tags.FUNCTION",
      "tags.GROUPCONTACT",
      "tags.MEMBERFIRM",
      "tags.PRIMARYCONTACT",
      "tags.SECONDARYCONTACT",
      "tags.AccountName"]
  }

  tags = {
    Client = "Economical"
    Contract = "MANAGED_SERVICES"
    PatchGroup = "Windows-AzA-Group1"
    Owner = "robhatnagar@deloitte.ca"
    ManagedBy = "CMS-TF"
    SolutionName = "InsurCloud Economical"
    Environment = "Prod"
    ApplicationName = "rpa, blueprism"
    DataClassification = "PII, PHI"
    DLM-Backup = "Yes"
    DLM-Retention = "90"
    VendorManaged = "False"
    ResourceType = "Client"
    ResourceSeverity = "Normal"
    Name = "A-MC-CAC-D-A003-ECONOMICAL-PROD01-RPA-CLIENT03-WIN-EC2"
    AccountID = "${data.aws_caller_identity.current.account_id}"
  }
}

output "ICECONOMICAL_PROD01_RPA_CLIENT03_PRIVATEIP" {
  value = "${aws_instance.ICECONOMICAL_PROD01_RPA_CLIENT03.private_ip}"
}

data "template_file" "ECO_PROD01_DOMAIN_JOIN_RPA_C04" {
  template = "${file("IC-Economical-PROD/windowsdata.tpl")}"

  vars = {
    computer_name = "PROD01-RPA-C04"
    region = "${var.region}"
    domain = "${var.ad_domain}"
    dns1 = "${var.ad_dns1}"
    dns2 = "${var.ad_dns2}"
    dns3 = "${var.ad_dns3}"
    ouselection = "${var.ou_name}"
    pwordparameter = "${var.pwordparameter}"
    unameparameter = "${var.unameparameter}"
  }
}

resource "aws_instance" "ICECONOMICAL_PROD01_RPA_CLIENT04" {
  ami = "${var.ami_id_windows2016base}"
  vpc_security_group_ids = [
    "${aws_security_group.SG_AVIATRIX_RDP.id}",
    "${aws_security_group.SG_RPA.id}",
    "${aws_security_group.SG_RPA_CLIENT.id}"
  ]
  subnet_id = "${var.economical-prod-private-1b-subnet}"
  instance_type = "t3a.medium"
  key_name = "EconomicalRPAPRD"
  associate_public_ip_address = false
  iam_instance_profile = "CMS-SSMInstanceRole"
  ebs_optimized = "true"

  root_block_device {
    volume_size = "50"
  }

  user_data = "${data.template_file.ECO_PROD01_DOMAIN_JOIN_RPA_C04.rendered}"

  lifecycle {
    prevent_destroy = true
    ignore_changes = [
      "tags.%",
      "tags.BILLINGCODE",
      "tags.BILLINGCONTACT",
      "tags.CLIENT",
      "tags.COUNTRY",
      "tags.CSCLASS",
      "tags.CSQUAL",
      "tags.CSTYPE",
      "tags.ENVIRONMENT",
      "tags.FUNCTION",
      "tags.GROUPCONTACT",
      "tags.MEMBERFIRM",
      "tags.PRIMARYCONTACT",
      "tags.SECONDARYCONTACT",
      "tags.AccountName"]
  }

  tags = {
    Client = "Economical"
    Contract = "MANAGED_SERVICES"
    PatchGroup = "Windows-AzB-Group1"
    Owner = "robhatnagar@deloitte.ca"
    ManagedBy = "CMS-TF"
    SolutionName = "InsurCloud Economical"
    Environment = "Prod"
    ApplicationName = "rpa, blueprism"
    DataClassification = "PII, PHI"
    DLM-Backup = "Yes"
    DLM-Retention = "90"
    VendorManaged = "False"
    ResourceType = "Client"
    ResourceSeverity = "Normal"
    Name = "A-MC-CAC-D-A003-ECONOMICAL-PROD01-RPA-CLIENT04-WIN-EC2"
    AccountID = "${data.aws_caller_identity.current.account_id}"
  }
}

output "ICECONOMICAL_PROD01_RPA_CLIENT04_PRIVATEIP" {
  value = "${aws_instance.ICECONOMICAL_PROD01_RPA_CLIENT04.private_ip}"
}

data "template_file" "ECO_PROD01_DOMAIN_JOIN_RPA_C05" {
  template = "${file("IC-Economical-PROD/windowsdata.tpl")}"

  vars = {
    computer_name = "PROD01-RPA-C05"
    region = "${var.region}"
    domain = "${var.ad_domain}"
    dns1 = "${var.ad_dns1}"
    dns2 = "${var.ad_dns2}"
    dns3 = "${var.ad_dns3}"
    ouselection = "${var.ou_name}"
    pwordparameter = "${var.pwordparameter}"
    unameparameter = "${var.unameparameter}"
  }
}

resource "aws_instance" "ICECONOMICAL_PROD01_RPA_CLIENT05" {
  ami = "${var.ami_id_windows2016base}"
  vpc_security_group_ids = [
    "${aws_security_group.SG_AVIATRIX_RDP.id}",
    "${aws_security_group.SG_RPA.id}",
    "${aws_security_group.SG_RPA_CLIENT.id}"
  ]
  subnet_id = "${var.economical-prod-private-1a-subnet}"
  instance_type = "t3a.medium"
  key_name = "EconomicalRPAPRD"
  associate_public_ip_address = false
  iam_instance_profile = "CMS-SSMInstanceRole"
  ebs_optimized = "true"

  root_block_device {
    volume_size = "50"
  }

  user_data = "${data.template_file.ECO_PROD01_DOMAIN_JOIN_RPA_C05.rendered}"

  lifecycle {
    prevent_destroy = true
    ignore_changes = [
      "tags.%",
      "tags.BILLINGCODE",
      "tags.BILLINGCONTACT",
      "tags.CLIENT",
      "tags.COUNTRY",
      "tags.CSCLASS",
      "tags.CSQUAL",
      "tags.CSTYPE",
      "tags.ENVIRONMENT",
      "tags.FUNCTION",
      "tags.GROUPCONTACT",
      "tags.MEMBERFIRM",
      "tags.PRIMARYCONTACT",
      "tags.SECONDARYCONTACT",
      "tags.AccountName"]
  }

  tags = {
    Client = "Economical"
    Contract = "MANAGED_SERVICES"
    PatchGroup = "Windows-AzA-Group1"
    Owner = "robhatnagar@deloitte.ca"
    ManagedBy = "CMS-TF"
    SolutionName = "InsurCloud Economical"
    Environment = "Prod"
    ApplicationName = "rpa, blueprism"
    DataClassification = "PII, PHI"
    DLM-Backup = "Yes"
    DLM-Retention = "90"
    VendorManaged = "False"
    ResourceType = "Client"
    ResourceSeverity = "Normal"
    Name = "A-MC-CAC-D-A003-ECONOMICAL-PROD01-RPA-CLIENT05-WIN-EC2"
    AccountID = "${data.aws_caller_identity.current.account_id}"
  }
}

output "ICECONOMICAL_PROD01_RPA_CLIENT05_PRIVATEIP" {
  value = "${aws_instance.ICECONOMICAL_PROD01_RPA_CLIENT05.private_ip}"
}

data "template_file" "ECO_PROD01_DOMAIN_JOIN_RPA_C06" {
  template = "${file("IC-Economical-PROD/windowsdata.tpl")}"

  vars = {
    computer_name = "PROD01-RPA-C06"
    region = "${var.region}"
    domain = "${var.ad_domain}"
    dns1 = "${var.ad_dns1}"
    dns2 = "${var.ad_dns2}"
    dns3 = "${var.ad_dns3}"
    ouselection = "${var.ou_name}"
    pwordparameter = "${var.pwordparameter}"
    unameparameter = "${var.unameparameter}"
  }
}

resource "aws_instance" "ICECONOMICAL_PROD01_RPA_CLIENT06" {
  ami = "${var.ami_id_windows2016base}"
  vpc_security_group_ids = [
    "${aws_security_group.SG_AVIATRIX_RDP.id}",
    "${aws_security_group.SG_RPA.id}",
    "${aws_security_group.SG_RPA_CLIENT.id}"
  ]
  subnet_id = "${var.economical-prod-private-1b-subnet}"
  instance_type = "t3a.medium"
  key_name = "EconomicalRPAPRD"
  associate_public_ip_address = false
  iam_instance_profile = "CMS-SSMInstanceRole"
  ebs_optimized = "true"

  root_block_device {
    volume_size = "50"
  }

  user_data = "${data.template_file.ECO_PROD01_DOMAIN_JOIN_RPA_C06.rendered}"

  lifecycle {
    prevent_destroy = true
    ignore_changes = [
      "tags.%",
      "tags.BILLINGCODE",
      "tags.BILLINGCONTACT",
      "tags.CLIENT",
      "tags.COUNTRY",
      "tags.CSCLASS",
      "tags.CSQUAL",
      "tags.CSTYPE",
      "tags.ENVIRONMENT",
      "tags.FUNCTION",
      "tags.GROUPCONTACT",
      "tags.MEMBERFIRM",
      "tags.PRIMARYCONTACT",
      "tags.SECONDARYCONTACT",
      "tags.AccountName"]
  }

  tags = {
    Client = "Economical"
    Contract = "MANAGED_SERVICES"
    PatchGroup = "Windows-AzB-Group1"
    Owner = "robhatnagar@deloitte.ca"
    ManagedBy = "CMS-TF"
    SolutionName = "InsurCloud Economical"
    Environment = "Prod"
    ApplicationName = "rpa, blueprism"
    DataClassification = "PII, PHI"
    DLM-Backup = "Yes"
    DLM-Retention = "90"
    VendorManaged = "False"
    ResourceType = "Client"
    ResourceSeverity = "Normal"
    Name = "A-MC-CAC-D-A003-ECONOMICAL-PROD01-RPA-CLIENT06-WIN-EC2"
    AccountID = "${data.aws_caller_identity.current.account_id}"
  }
}

output "ICECONOMICAL_PROD01_RPA_CLIENT06_PRIVATEIP" {
  value = "${aws_instance.ICECONOMICAL_PROD01_RPA_CLIENT06.private_ip}"
}

data "template_file" "ECO_PROD01_DOMAIN_JOIN_RPA_C07" {
  template = "${file("IC-Economical-PROD/windowsdata.tpl")}"

  vars = {
    computer_name = "PROD01-RPA-C07"
    region = "${var.region}"
    domain = "${var.ad_domain}"
    dns1 = "${var.ad_dns1}"
    dns2 = "${var.ad_dns2}"
    dns3 = "${var.ad_dns3}"
    ouselection = "${var.ou_name}"
    pwordparameter = "${var.pwordparameter}"
    unameparameter = "${var.unameparameter}"
  }
}

resource "aws_instance" "ICECONOMICAL_PROD01_RPA_CLIENT07" {
  ami = "${var.ami_id_windows2016base}"
  vpc_security_group_ids = [
    "${aws_security_group.SG_AVIATRIX_RDP.id}",
    "${aws_security_group.SG_RPA.id}",
    "${aws_security_group.SG_RPA_CLIENT.id}"
  ]
  subnet_id = "${var.economical-prod-private-1a-subnet}"
  instance_type = "t3a.medium"
  key_name = "EconomicalRPAPRD"
  associate_public_ip_address = false
  iam_instance_profile = "CMS-SSMInstanceRole"
  ebs_optimized = "true"

  root_block_device {
    volume_size = "50"
  }

  user_data = "${data.template_file.ECO_PROD01_DOMAIN_JOIN_RPA_C07.rendered}"

  lifecycle {
    prevent_destroy = true
    ignore_changes = [
      "tags.%",
      "tags.BILLINGCODE",
      "tags.BILLINGCONTACT",
      "tags.CLIENT",
      "tags.COUNTRY",
      "tags.CSCLASS",
      "tags.CSQUAL",
      "tags.CSTYPE",
      "tags.ENVIRONMENT",
      "tags.FUNCTION",
      "tags.GROUPCONTACT",
      "tags.MEMBERFIRM",
      "tags.PRIMARYCONTACT",
      "tags.SECONDARYCONTACT",
      "tags.AccountName"]
  }

  tags = {
    Client = "Economical"
    Contract = "MANAGED_SERVICES"
    PatchGroup = "Windows-AzA-Group1"
    Owner = "robhatnagar@deloitte.ca"
    ManagedBy = "CMS-TF"
    SolutionName = "InsurCloud Economical"
    Environment = "Prod"
    ApplicationName = "rpa, blueprism"
    DataClassification = "PII, PHI"
    DLM-Backup = "Yes"
    DLM-Retention = "90"
    VendorManaged = "False"
    ResourceType = "Client"
    ResourceSeverity = "Normal"
    Name = "A-MC-CAC-D-A003-ECONOMICAL-PROD01-RPA-CLIENT07-WIN-EC2"
    AccountID = "${data.aws_caller_identity.current.account_id}"
  }
}

output "ICECONOMICAL_PROD01_RPA_CLIENT07_PRIVATEIP" {
  value = "${aws_instance.ICECONOMICAL_PROD01_RPA_CLIENT07.private_ip}"
}

data "template_file" "ECO_PROD01_DOMAIN_JOIN_RPA_C08" {
  template = "${file("IC-Economical-PROD/windowsdata.tpl")}"

  vars = {
    computer_name = "PROD01-RPA-C08"
    region = "${var.region}"
    domain = "${var.ad_domain}"
    dns1 = "${var.ad_dns1}"
    dns2 = "${var.ad_dns2}"
    dns3 = "${var.ad_dns3}"
    ouselection = "${var.ou_name}"
    pwordparameter = "${var.pwordparameter}"
    unameparameter = "${var.unameparameter}"
  }
}

resource "aws_instance" "ICECONOMICAL_PROD01_RPA_CLIENT08" {
  ami = "${var.ami_id_windows2016base}"
  vpc_security_group_ids = [
    "${aws_security_group.SG_AVIATRIX_RDP.id}",
    "${aws_security_group.SG_RPA.id}",
    "${aws_security_group.SG_RPA_CLIENT.id}"
  ]
  subnet_id = "${var.economical-prod-private-1b-subnet}"
  instance_type = "t3a.medium"
  key_name = "EconomicalRPAPRD"
  associate_public_ip_address = false
  iam_instance_profile = "CMS-SSMInstanceRole"
  ebs_optimized = "true"

  root_block_device {
    volume_size = "50"
  }

  user_data = "${data.template_file.ECO_PROD01_DOMAIN_JOIN_RPA_C08.rendered}"

  lifecycle {
    prevent_destroy = true
    ignore_changes = [
      "tags.%",
      "tags.BILLINGCODE",
      "tags.BILLINGCONTACT",
      "tags.CLIENT",
      "tags.COUNTRY",
      "tags.CSCLASS",
      "tags.CSQUAL",
      "tags.CSTYPE",
      "tags.ENVIRONMENT",
      "tags.FUNCTION",
      "tags.GROUPCONTACT",
      "tags.MEMBERFIRM",
      "tags.PRIMARYCONTACT",
      "tags.SECONDARYCONTACT",
      "tags.AccountName"]
  }

  tags = {
    Client = "Economical"
    Contract = "MANAGED_SERVICES"
    PatchGroup = "Windows-AzB-Group1"
    Owner = "robhatnagar@deloitte.ca"
    ManagedBy = "CMS-TF"
    SolutionName = "InsurCloud Economical"
    Environment = "Prod"
    ApplicationName = "rpa, blueprism"
    DataClassification = "PII, PHI"
    DLM-Backup = "Yes"
    DLM-Retention = "90"
    VendorManaged = "False"
    ResourceType = "Client"
    ResourceSeverity = "Normal"
    Name = "A-MC-CAC-D-A003-ECONOMICAL-PROD01-RPA-CLIENT08-WIN-EC2"
    AccountID = "${data.aws_caller_identity.current.account_id}"
  }
}

output "ICECONOMICAL_PROD01_RPA_CLIENT08_PRIVATEIP" {
  value = "${aws_instance.ICECONOMICAL_PROD01_RPA_CLIENT08.private_ip}"
}

data "template_file" "ECO_PROD01_DOMAIN_JOIN_RPA_C09" {
  template = "${file("IC-Economical-PROD/windowsdata.tpl")}"

  vars = {
    computer_name = "PROD01-RPA-C09"
    region = "${var.region}"
    domain = "${var.ad_domain}"
    dns1 = "${var.ad_dns1}"
    dns2 = "${var.ad_dns2}"
    dns3 = "${var.ad_dns3}"
    ouselection = "${var.ou_name}"
    pwordparameter = "${var.pwordparameter}"
    unameparameter = "${var.unameparameter}"
  }
}

resource "aws_instance" "ICECONOMICAL_PROD01_RPA_CLIENT09" {
  ami = "${var.ami_id_windows2016base}"
  vpc_security_group_ids = [
    "${aws_security_group.SG_AVIATRIX_RDP.id}",
    "${aws_security_group.SG_RPA.id}",
    "${aws_security_group.SG_RPA_CLIENT.id}"
  ]
  subnet_id = "${var.economical-prod-private-1a-subnet}"
  instance_type = "t3a.medium"
  key_name = "EconomicalRPAPRD"
  associate_public_ip_address = false
  iam_instance_profile = "CMS-SSMInstanceRole"
  ebs_optimized = "true"

  root_block_device {
    volume_size = "50"
  }

  user_data = "${data.template_file.ECO_PROD01_DOMAIN_JOIN_RPA_C09.rendered}"

  lifecycle {
    prevent_destroy = true
    ignore_changes = [
      "tags.%",
      "tags.BILLINGCODE",
      "tags.BILLINGCONTACT",
      "tags.CLIENT",
      "tags.COUNTRY",
      "tags.CSCLASS",
      "tags.CSQUAL",
      "tags.CSTYPE",
      "tags.ENVIRONMENT",
      "tags.FUNCTION",
      "tags.GROUPCONTACT",
      "tags.MEMBERFIRM",
      "tags.PRIMARYCONTACT",
      "tags.SECONDARYCONTACT",
      "tags.AccountName"]
  }

  tags = {
    Client = "Economical"
    Contract = "MANAGED_SERVICES"
    PatchGroup = "Windows-AzA-Group1"
    Owner = "robhatnagar@deloitte.ca"
    ManagedBy = "CMS-TF"
    SolutionName = "InsurCloud Economical"
    Environment = "Prod"
    ApplicationName = "rpa, blueprism"
    DataClassification = "PII, PHI"
    DLM-Backup = "Yes"
    DLM-Retention = "90"
    VendorManaged = "False"
    ResourceType = "Client"
    ResourceSeverity = "Normal"
    Name = "A-MC-CAC-D-A003-ECONOMICAL-PROD01-RPA-CLIENT09-WIN-EC2"
    AccountID = "${data.aws_caller_identity.current.account_id}"
  }
}

output "ICECONOMICAL_PROD01_RPA_CLIENT09_PRIVATEIP" {
  value = "${aws_instance.ICECONOMICAL_PROD01_RPA_CLIENT09.private_ip}"
}

data "template_file" "ECO_PROD01_DOMAIN_JOIN_RPA_C10" {
  template = "${file("IC-Economical-PROD/windowsdata.tpl")}"

  vars = {
    computer_name = "PROD01-RPA-C10"
    region = "${var.region}"
    domain = "${var.ad_domain}"
    dns1 = "${var.ad_dns1}"
    dns2 = "${var.ad_dns2}"
    dns3 = "${var.ad_dns3}"
    ouselection = "${var.ou_name}"
    pwordparameter = "${var.pwordparameter}"
    unameparameter = "${var.unameparameter}"
  }
}

resource "aws_instance" "ICECONOMICAL_PROD01_RPA_CLIENT10" {
  ami = "${var.ami_id_windows2016base}"
  vpc_security_group_ids = [
    "${aws_security_group.SG_AVIATRIX_RDP.id}",
    "${aws_security_group.SG_RPA.id}",
    "${aws_security_group.SG_RPA_CLIENT.id}"
  ]
  subnet_id = "${var.economical-prod-private-1b-subnet}"
  instance_type = "t3a.medium"
  key_name = "EconomicalRPAPRD"
  associate_public_ip_address = false
  iam_instance_profile = "CMS-SSMInstanceRole"
  ebs_optimized = "true"

  root_block_device {
    volume_size = "50"
  }

  user_data = "${data.template_file.ECO_PROD01_DOMAIN_JOIN_RPA_C10.rendered}"

  lifecycle {
    prevent_destroy = true
    ignore_changes = [
      "tags.%",
      "tags.BILLINGCODE",
      "tags.BILLINGCONTACT",
      "tags.CLIENT",
      "tags.COUNTRY",
      "tags.CSCLASS",
      "tags.CSQUAL",
      "tags.CSTYPE",
      "tags.ENVIRONMENT",
      "tags.FUNCTION",
      "tags.GROUPCONTACT",
      "tags.MEMBERFIRM",
      "tags.PRIMARYCONTACT",
      "tags.SECONDARYCONTACT",
      "tags.AccountName"]
  }

  tags = {
    Client = "Economical"
    Contract = "MANAGED_SERVICES"
    PatchGroup = "Windows-AzB-Group1"
    Owner = "robhatnagar@deloitte.ca"
    ManagedBy = "CMS-TF"
    SolutionName = "InsurCloud Economical"
    Environment = "Prod"
    ApplicationName = "rpa, blueprism"
    DataClassification = "PII, PHI"
    DLM-Backup = "Yes"
    DLM-Retention = "90"
    VendorManaged = "False"
    ResourceType = "Client"
    ResourceSeverity = "Normal"
    Name = "A-MC-CAC-D-A003-ECONOMICAL-PROD01-RPA-CLIENT10-WIN-EC2"
    AccountID = "${data.aws_caller_identity.current.account_id}"
  }
}

output "ICECONOMICAL_PROD01_RPA_CLIENT10_PRIVATEIP" {
  value = "${aws_instance.ICECONOMICAL_PROD01_RPA_CLIENT10.private_ip}"
}

data "template_file" "ECO_PROD01_DOMAIN_JOIN_RPA_C11" {
  template = "${file("IC-Economical-PROD/windowsdata.tpl")}"

  vars = {
    computer_name = "PROD01-RPA-C11"
    region = "${var.region}"
    domain = "${var.ad_domain}"
    dns1 = "${var.ad_dns1}"
    dns2 = "${var.ad_dns2}"
    dns3 = "${var.ad_dns3}"
    ouselection = "${var.ou_name}"
    pwordparameter = "${var.pwordparameter}"
    unameparameter = "${var.unameparameter}"
  }
}

resource "aws_instance" "ICECONOMICAL_PROD01_RPA_CLIENT11" {
  ami = "${var.ami_id_windows2016base}"
  vpc_security_group_ids = [
    "${aws_security_group.SG_AVIATRIX_RDP.id}",
    "${aws_security_group.SG_RPA.id}",
    "${aws_security_group.SG_RPA_CLIENT.id}"
  ]
  subnet_id = "${var.economical-prod-private-1a-subnet}"
  instance_type = "t3a.medium"
  key_name = "EconomicalRPAPRD"
  associate_public_ip_address = false
  iam_instance_profile = "CMS-SSMInstanceRole"
  ebs_optimized = "true"

  root_block_device {
    volume_size = "50"
  }

  user_data = "${data.template_file.ECO_PROD01_DOMAIN_JOIN_RPA_C11.rendered}"

  lifecycle {
    prevent_destroy = true
    ignore_changes = [
      "tags.%",
      "tags.BILLINGCODE",
      "tags.BILLINGCONTACT",
      "tags.CLIENT",
      "tags.COUNTRY",
      "tags.CSCLASS",
      "tags.CSQUAL",
      "tags.CSTYPE",
      "tags.ENVIRONMENT",
      "tags.FUNCTION",
      "tags.GROUPCONTACT",
      "tags.MEMBERFIRM",
      "tags.PRIMARYCONTACT",
      "tags.SECONDARYCONTACT",
      "tags.AccountName"]
  }

  tags = {
    Client = "Economical"
    Contract = "MANAGED_SERVICES"
    PatchGroup = "Windows-AzA-Group1"
    Owner = "robhatnagar@deloitte.ca"
    ManagedBy = "CMS-TF"
    SolutionName = "InsurCloud Economical"
    Environment = "Prod"
    ApplicationName = "rpa, blueprism"
    DataClassification = "PII, PHI"
    DLM-Backup = "Yes"
    DLM-Retention = "90"
    VendorManaged = "False"
    ResourceType = "Client"
    ResourceSeverity = "Normal"
    Name = "A-MC-CAC-D-A003-ECONOMICAL-PROD01-RPA-CLIENT11-WIN-EC2"
    AccountID = "${data.aws_caller_identity.current.account_id}"
  }
}

output "ICECONOMICAL_PROD01_RPA_CLIENT11_PRIVATEIP" {
  value = "${aws_instance.ICECONOMICAL_PROD01_RPA_CLIENT11.private_ip}"
}

data "template_file" "ECO_PROD01_DOMAIN_JOIN_RPA_C12" {
  template = "${file("IC-Economical-PROD/windowsdata.tpl")}"

  vars = {
    computer_name = "PROD01-RPA-C12"
    region = "${var.region}"
    domain = "${var.ad_domain}"
    dns1 = "${var.ad_dns1}"
    dns2 = "${var.ad_dns2}"
    dns3 = "${var.ad_dns3}"
    ouselection = "${var.ou_name}"
    pwordparameter = "${var.pwordparameter}"
    unameparameter = "${var.unameparameter}"
  }
}

resource "aws_instance" "ICECONOMICAL_PROD01_RPA_CLIENT12" {
  ami = "${var.ami_id_windows2016base}"
  vpc_security_group_ids = [
    "${aws_security_group.SG_AVIATRIX_RDP.id}",
    "${aws_security_group.SG_RPA.id}",
    "${aws_security_group.SG_RPA_CLIENT.id}"
  ]
  subnet_id = "${var.economical-prod-private-1b-subnet}"
  instance_type = "t3a.medium"
  key_name = "EconomicalRPAPRD"
  associate_public_ip_address = false
  iam_instance_profile = "CMS-SSMInstanceRole"
  ebs_optimized = "true"

  root_block_device {
    volume_size = "50"
  }

  user_data = "${data.template_file.ECO_PROD01_DOMAIN_JOIN_RPA_C12.rendered}"

  lifecycle {
    prevent_destroy = true
    ignore_changes = [
      "tags.%",
      "tags.BILLINGCODE",
      "tags.BILLINGCONTACT",
      "tags.CLIENT",
      "tags.COUNTRY",
      "tags.CSCLASS",
      "tags.CSQUAL",
      "tags.CSTYPE",
      "tags.ENVIRONMENT",
      "tags.FUNCTION",
      "tags.GROUPCONTACT",
      "tags.MEMBERFIRM",
      "tags.PRIMARYCONTACT",
      "tags.SECONDARYCONTACT",
      "tags.AccountName"]
  }

  tags = {
    Client = "Economical"
    Contract = "MANAGED_SERVICES"
    PatchGroup = "Windows-AzB-Group1"
    Owner = "robhatnagar@deloitte.ca"
    ManagedBy = "CMS-TF"
    SolutionName = "InsurCloud Economical"
    Environment = "Prod"
    ApplicationName = "rpa, blueprism"
    DataClassification = "PII, PHI"
    DLM-Backup = "Yes"
    DLM-Retention = "90"
    VendorManaged = "False"
    ResourceType = "Client"
    ResourceSeverity = "Normal"
    Name = "A-MC-CAC-D-A003-ECONOMICAL-PROD01-RPA-CLIENT12-WIN-EC2"
    AccountID = "${data.aws_caller_identity.current.account_id}"
  }
}

output "ICECONOMICAL_PROD01_RPA_CLIENT12_PRIVATEIP" {
  value = "${aws_instance.ICECONOMICAL_PROD01_RPA_CLIENT12.private_ip}"
}

data "template_file" "ECO_PROD01_DOMAIN_JOIN_RPA_C13" {
  template = "${file("IC-Economical-PROD/windowsdata.tpl")}"

  vars = {
    computer_name = "PROD01-RPA-C13"
    region = "${var.region}"
    domain = "${var.ad_domain}"
    dns1 = "${var.ad_dns1}"
    dns2 = "${var.ad_dns2}"
    dns3 = "${var.ad_dns3}"
    ouselection = "${var.ou_name}"
    pwordparameter = "${var.pwordparameter}"
    unameparameter = "${var.unameparameter}"
  }
}

resource "aws_instance" "ICECONOMICAL_PROD01_RPA_CLIENT13" {
  ami = "${var.ami_id_windows2016base}"
  vpc_security_group_ids = [
    "${aws_security_group.SG_AVIATRIX_RDP.id}",
    "${aws_security_group.SG_RPA.id}",
    "${aws_security_group.SG_RPA_CLIENT.id}"
  ]
  subnet_id = "${var.economical-prod-private-1a-subnet}"
  instance_type = "t3a.medium"
  key_name = "EconomicalRPAPRD"
  associate_public_ip_address = false
  iam_instance_profile = "CMS-SSMInstanceRole"
  ebs_optimized = "true"

  root_block_device {
    volume_size = "50"
  }

  user_data = "${data.template_file.ECO_PROD01_DOMAIN_JOIN_RPA_C13.rendered}"

  lifecycle {
    prevent_destroy = true
    ignore_changes = [
      "tags.%",
      "tags.BILLINGCODE",
      "tags.BILLINGCONTACT",
      "tags.CLIENT",
      "tags.COUNTRY",
      "tags.CSCLASS",
      "tags.CSQUAL",
      "tags.CSTYPE",
      "tags.ENVIRONMENT",
      "tags.FUNCTION",
      "tags.GROUPCONTACT",
      "tags.MEMBERFIRM",
      "tags.PRIMARYCONTACT",
      "tags.SECONDARYCONTACT",
      "tags.AccountName"]
  }

  tags = {
    Client = "Economical"
    Contract = "MANAGED_SERVICES"
    PatchGroup = "Windows-AzA-Group1"
    Owner = "robhatnagar@deloitte.ca"
    ManagedBy = "CMS-TF"
    SolutionName = "InsurCloud Economical"
    Environment = "Prod"
    ApplicationName = "rpa, blueprism"
    DataClassification = "PII, PHI"
    DLM-Backup = "Yes"
    DLM-Retention = "90"
    VendorManaged = "False"
    ResourceType = "Client"
    ResourceSeverity = "Normal"
    Name = "A-MC-CAC-D-A003-ECONOMICAL-PROD01-RPA-CLIENT13-WIN-EC2"
    AccountID = "${data.aws_caller_identity.current.account_id}"
  }
}

output "ICECONOMICAL_PROD01_RPA_CLIENT13_PRIVATEIP" {
  value = "${aws_instance.ICECONOMICAL_PROD01_RPA_CLIENT13.private_ip}"
}

data "template_file" "ECO_PROD01_DOMAIN_JOIN_RPA_C14" {
  template = "${file("IC-Economical-PROD/windowsdata.tpl")}"

  vars = {
    computer_name = "PROD01-RPA-C14"
    region = "${var.region}"
    domain = "${var.ad_domain}"
    dns1 = "${var.ad_dns1}"
    dns2 = "${var.ad_dns2}"
    dns3 = "${var.ad_dns3}"
    ouselection = "${var.ou_name}"
    pwordparameter = "${var.pwordparameter}"
    unameparameter = "${var.unameparameter}"
  }
}

resource "aws_instance" "ICECONOMICAL_PROD01_RPA_CLIENT14" {
  ami = "${var.ami_id_windows2016base}"
  vpc_security_group_ids = [
    "${aws_security_group.SG_AVIATRIX_RDP.id}",
    "${aws_security_group.SG_RPA.id}",
    "${aws_security_group.SG_RPA_CLIENT.id}"
  ]
  subnet_id = "${var.economical-prod-private-1b-subnet}"
  instance_type = "t3a.medium"
  key_name = "EconomicalRPAPRD"
  associate_public_ip_address = false
  iam_instance_profile = "CMS-SSMInstanceRole"
  ebs_optimized = "true"

  root_block_device {
    volume_size = "50"
  }

  user_data = "${data.template_file.ECO_PROD01_DOMAIN_JOIN_RPA_C14.rendered}"

  lifecycle {
    prevent_destroy = true
    ignore_changes = [
      "tags.%",
      "tags.BILLINGCODE",
      "tags.BILLINGCONTACT",
      "tags.CLIENT",
      "tags.COUNTRY",
      "tags.CSCLASS",
      "tags.CSQUAL",
      "tags.CSTYPE",
      "tags.ENVIRONMENT",
      "tags.FUNCTION",
      "tags.GROUPCONTACT",
      "tags.MEMBERFIRM",
      "tags.PRIMARYCONTACT",
      "tags.SECONDARYCONTACT",
      "tags.AccountName"]
  }

  tags = {
    Client = "Economical"
    Contract = "MANAGED_SERVICES"
    PatchGroup = "Windows-AzB-Group1"
    Owner = "robhatnagar@deloitte.ca"
    ManagedBy = "CMS-TF"
    SolutionName = "InsurCloud Economical"
    Environment = "Prod"
    ApplicationName = "rpa, blueprism"
    DataClassification = "PII, PHI"
    DLM-Backup = "Yes"
    DLM-Retention = "90"
    VendorManaged = "False"
    ResourceType = "Client"
    ResourceSeverity = "Normal"
    Name = "A-MC-CAC-D-A003-ECONOMICAL-PROD01-RPA-CLIENT14-WIN-EC2"
    AccountID = "${data.aws_caller_identity.current.account_id}"
  }
}

output "ICECONOMICAL_PROD01_RPA_CLIENT14_PRIVATEIP" {
  value = "${aws_instance.ICECONOMICAL_PROD01_RPA_CLIENT14.private_ip}"
}

data "template_file" "ECO_PROD01_DOMAIN_JOIN_RPA_C15" {
  template = "${file("IC-Economical-PROD/windowsdata.tpl")}"

  vars = {
    computer_name = "PROD01-RPA-C15"
    region = "${var.region}"
    domain = "${var.ad_domain}"
    dns1 = "${var.ad_dns1}"
    dns2 = "${var.ad_dns2}"
    dns3 = "${var.ad_dns3}"
    ouselection = "${var.ou_name}"
    pwordparameter = "${var.pwordparameter}"
    unameparameter = "${var.unameparameter}"
  }
}

resource "aws_instance" "ICECONOMICAL_PROD01_RPA_CLIENT15" {
  ami = "${var.ami_id_windows2016base}"
  vpc_security_group_ids = [
    "${aws_security_group.SG_AVIATRIX_RDP.id}",
    "${aws_security_group.SG_RPA.id}",
    "${aws_security_group.SG_RPA_CLIENT.id}"
  ]
  subnet_id = "${var.economical-prod-private-1a-subnet}"
  instance_type = "t3a.medium"
  key_name = "EconomicalRPAPRD"
  associate_public_ip_address = false
  iam_instance_profile = "CMS-SSMInstanceRole"
  ebs_optimized = "true"

  root_block_device {
    volume_size = "50"
  }

  user_data = "${data.template_file.ECO_PROD01_DOMAIN_JOIN_RPA_C15.rendered}"

  lifecycle {
    prevent_destroy = true
    ignore_changes = [
      "tags.%",
      "tags.BILLINGCODE",
      "tags.BILLINGCONTACT",
      "tags.CLIENT",
      "tags.COUNTRY",
      "tags.CSCLASS",
      "tags.CSQUAL",
      "tags.CSTYPE",
      "tags.ENVIRONMENT",
      "tags.FUNCTION",
      "tags.GROUPCONTACT",
      "tags.MEMBERFIRM",
      "tags.PRIMARYCONTACT",
      "tags.SECONDARYCONTACT",
      "tags.AccountName"]
  }

  tags = {
    Client = "Economical"
    Contract = "MANAGED_SERVICES"
    PatchGroup = "Windows-AzA-Group1"
    Owner = "robhatnagar@deloitte.ca"
    ManagedBy = "CMS-TF"
    SolutionName = "InsurCloud Economical"
    Environment = "Prod"
    ApplicationName = "rpa, blueprism"
    DataClassification = "PII, PHI"
    DLM-Backup = "Yes"
    DLM-Retention = "90"
    VendorManaged = "False"
    ResourceType = "Client"
    ResourceSeverity = "Normal"
    Name = "A-MC-CAC-D-A003-ECONOMICAL-PROD01-RPA-CLIENT15-WIN-EC2"
    AccountID = "${data.aws_caller_identity.current.account_id}"
  }
}

output "ICECONOMICAL_PROD01_RPA_CLIENT15_PRIVATEIP" {
  value = "${aws_instance.ICECONOMICAL_PROD01_RPA_CLIENT15.private_ip}"
}

data "template_file" "ECO_PROD01_DOMAIN_JOIN_RPA_C16" {
  template = "${file("IC-Economical-PROD/windowsdata.tpl")}"

  vars = {
    computer_name = "PROD01-RPA-C16"
    region = "${var.region}"
    domain = "${var.ad_domain}"
    dns1 = "${var.ad_dns1}"
    dns2 = "${var.ad_dns2}"
    dns3 = "${var.ad_dns3}"
    ouselection = "${var.ou_name}"
    pwordparameter = "${var.pwordparameter}"
    unameparameter = "${var.unameparameter}"
  }
}

resource "aws_instance" "ICECONOMICAL_PROD01_RPA_CLIENT16" {
  ami = "${var.ami_id_windows2016base_patch2020nov}"
  vpc_security_group_ids = [
    "${aws_security_group.SG_AVIATRIX_RDP.id}",
    "${aws_security_group.SG_RPA.id}",
    "${aws_security_group.SG_RPA_CLIENT.id}"
  ]
  subnet_id = "${var.economical-prod-private-1b-subnet}"
  instance_type = "t3a.medium"
  key_name = "EconomicalRPAPRD"
  associate_public_ip_address = false
  iam_instance_profile = "CMS-SSMInstanceRole"
  ebs_optimized = "true"

  root_block_device {
    volume_size = "50"
  }

  user_data = "${data.template_file.ECO_PROD01_DOMAIN_JOIN_RPA_C16.rendered}"

  lifecycle {
    prevent_destroy = true
    ignore_changes = [
      "tags.%",
      "tags.BILLINGCODE",
      "tags.BILLINGCONTACT",
      "tags.CLIENT",
      "tags.COUNTRY",
      "tags.CSCLASS",
      "tags.CSQUAL",
      "tags.CSTYPE",
      "tags.ENVIRONMENT",
      "tags.FUNCTION",
      "tags.GROUPCONTACT",
      "tags.MEMBERFIRM",
      "tags.PRIMARYCONTACT",
      "tags.SECONDARYCONTACT",
      "tags.AccountName"]
  }

  tags = {
    Client = "Economical"
    Contract = "MANAGED_SERVICES"
    PatchGroup = "Windows-AzB-Group1"
    Owner = "robhatnagar@deloitte.ca"
    ManagedBy = "CMS-TF"
    SolutionName = "InsurCloud Economical"
    Environment = "Prod"
    ApplicationName = "rpa, blueprism"
    DataClassification = "PII, PHI"
    DLM-Backup = "Yes"
    DLM-Retention = "90"
    VendorManaged = "False"
    ResourceType = "Client"
    ResourceSeverity = "Normal"
    Name = "A-MC-CAC-D-A003-ECONOMICAL-PROD01-RPA-CLIENT16-WIN-EC2"
    AccountID = "${data.aws_caller_identity.current.account_id}"
  }
}

output "ICECONOMICAL_PROD01_RPA_CLIENT16_PRIVATEIP" {
  value = "${aws_instance.ICECONOMICAL_PROD01_RPA_CLIENT16.private_ip}"
}

data "template_file" "ECO_PROD01_DOMAIN_JOIN_RPA_C17" {
  template = "${file("IC-Economical-PROD/windowsdata.tpl")}"

  vars = {
    computer_name = "PROD01-RPA-C17"
    region = "${var.region}"
    domain = "${var.ad_domain}"
    dns1 = "${var.ad_dns1}"
    dns2 = "${var.ad_dns2}"
    dns3 = "${var.ad_dns3}"
    ouselection = "${var.ou_name}"
    pwordparameter = "${var.pwordparameter}"
    unameparameter = "${var.unameparameter}"
  }
}

resource "aws_instance" "ICECONOMICAL_PROD01_RPA_CLIENT17" {
  ami = "${var.ami_id_windows2016base_patch2020nov}"
  vpc_security_group_ids = [
    "${aws_security_group.SG_AVIATRIX_RDP.id}",
    "${aws_security_group.SG_RPA.id}",
    "${aws_security_group.SG_RPA_CLIENT.id}"
  ]
  subnet_id = "${var.economical-prod-private-1a-subnet}"
  instance_type = "t3a.medium"
  key_name = "EconomicalRPAPRD"
  associate_public_ip_address = false
  iam_instance_profile = "CMS-SSMInstanceRole"
  ebs_optimized = "true"

  root_block_device {
    volume_size = "50"
  }

  user_data = "${data.template_file.ECO_PROD01_DOMAIN_JOIN_RPA_C17.rendered}"

  lifecycle {
    prevent_destroy = true
    ignore_changes = [
      "tags.%",
      "tags.BILLINGCODE",
      "tags.BILLINGCONTACT",
      "tags.CLIENT",
      "tags.COUNTRY",
      "tags.CSCLASS",
      "tags.CSQUAL",
      "tags.CSTYPE",
      "tags.ENVIRONMENT",
      "tags.FUNCTION",
      "tags.GROUPCONTACT",
      "tags.MEMBERFIRM",
      "tags.PRIMARYCONTACT",
      "tags.SECONDARYCONTACT",
      "tags.AccountName"]
  }

  tags = {
    Client = "Economical"
    Contract = "MANAGED_SERVICES"
    PatchGroup = "Windows-AzA-Group1"
    Owner = "robhatnagar@deloitte.ca"
    ManagedBy = "CMS-TF"
    SolutionName = "InsurCloud Economical"
    Environment = "Prod"
    ApplicationName = "rpa, blueprism"
    DataClassification = "PII, PHI"
    DLM-Backup = "Yes"
    DLM-Retention = "90"
    VendorManaged = "False"
    ResourceType = "Client"
    ResourceSeverity = "Normal"
    Name = "A-MC-CAC-D-A003-ECONOMICAL-PROD01-RPA-CLIENT17-WIN-EC2"
    AccountID = "${data.aws_caller_identity.current.account_id}"
  }
}

output "ICECONOMICAL_PROD01_RPA_CLIENT17_PRIVATEIP" {
  value = "${aws_instance.ICECONOMICAL_PROD01_RPA_CLIENT17.private_ip}"
}

data "template_file" "ECO_PROD01_DOMAIN_JOIN_RPA_C18" {
  template = "${file("IC-Economical-PROD/windowsdata.tpl")}"

  vars = {
    computer_name = "PROD01-RPA-C18"
    region = "${var.region}"
    domain = "${var.ad_domain}"
    dns1 = "${var.ad_dns1}"
    dns2 = "${var.ad_dns2}"
    dns3 = "${var.ad_dns3}"
    ouselection = "${var.ou_name}"
    pwordparameter = "${var.pwordparameter}"
    unameparameter = "${var.unameparameter}"
  }
}

resource "aws_instance" "ICECONOMICAL_PROD01_RPA_CLIENT18" {
  ami = "${var.ami_id_windows2016base_patch2020nov}"
  vpc_security_group_ids = [
    "${aws_security_group.SG_AVIATRIX_RDP.id}",
    "${aws_security_group.SG_RPA.id}",
    "${aws_security_group.SG_RPA_CLIENT.id}"
  ]
  subnet_id = "${var.economical-prod-private-1b-subnet}"
  instance_type = "t3a.medium"
  key_name = "EconomicalRPAPRD"
  associate_public_ip_address = false
  iam_instance_profile = "CMS-SSMInstanceRole"
  ebs_optimized = "true"

  root_block_device {
    volume_size = "50"
  }

  user_data = "${data.template_file.ECO_PROD01_DOMAIN_JOIN_RPA_C18.rendered}"

  lifecycle {
    prevent_destroy = true
    ignore_changes = [
      "tags.%",
      "tags.BILLINGCODE",
      "tags.BILLINGCONTACT",
      "tags.CLIENT",
      "tags.COUNTRY",
      "tags.CSCLASS",
      "tags.CSQUAL",
      "tags.CSTYPE",
      "tags.ENVIRONMENT",
      "tags.FUNCTION",
      "tags.GROUPCONTACT",
      "tags.MEMBERFIRM",
      "tags.PRIMARYCONTACT",
      "tags.SECONDARYCONTACT",
      "tags.AccountName"]
  }

  tags = {
    Client = "Economical"
    Contract = "MANAGED_SERVICES"
    PatchGroup = "Windows-AzB-Group1"
    Owner = "robhatnagar@deloitte.ca"
    ManagedBy = "CMS-TF"
    SolutionName = "InsurCloud Economical"
    Environment = "Prod"
    ApplicationName = "rpa, blueprism"
    DataClassification = "PII, PHI"
    DLM-Backup = "Yes"
    DLM-Retention = "90"
    VendorManaged = "False"
    ResourceType = "Client"
    ResourceSeverity = "Normal"
    Name = "A-MC-CAC-D-A003-ECONOMICAL-PROD01-RPA-CLIENT18-WIN-EC2"
    AccountID = "${data.aws_caller_identity.current.account_id}"
  }
}

output "ICECONOMICAL_PROD01_RPA_CLIENT18_PRIVATEIP" {
  value = "${aws_instance.ICECONOMICAL_PROD01_RPA_CLIENT18.private_ip}"
}

data "template_file" "ECO_PROD01_DOMAIN_JOIN_RPA_C19" {
  template = "${file("IC-Economical-PROD/windowsdata.tpl")}"

  vars = {
    computer_name = "PROD01-RPA-C19"
    region = "${var.region}"
    domain = "${var.ad_domain}"
    dns1 = "${var.ad_dns1}"
    dns2 = "${var.ad_dns2}"
    dns3 = "${var.ad_dns3}"
    ouselection = "${var.ou_name}"
    pwordparameter = "${var.pwordparameter}"
    unameparameter = "${var.unameparameter}"
  }
}

resource "aws_instance" "ICECONOMICAL_PROD01_RPA_CLIENT19" {
  ami = "${var.ami_id_windows2016base_patch2020nov}"
  vpc_security_group_ids = [
    "${aws_security_group.SG_AVIATRIX_RDP.id}",
    "${aws_security_group.SG_RPA.id}",
    "${aws_security_group.SG_RPA_CLIENT.id}"
  ]
  subnet_id = "${var.economical-prod-private-1a-subnet}"
  instance_type = "t3a.medium"
  key_name = "EconomicalRPAPRD"
  associate_public_ip_address = false
  iam_instance_profile = "CMS-SSMInstanceRole"
  ebs_optimized = "true"

  root_block_device {
    volume_size = "50"
  }

  user_data = "${data.template_file.ECO_PROD01_DOMAIN_JOIN_RPA_C19.rendered}"

  lifecycle {
    prevent_destroy = true
    ignore_changes = [
      "tags.%",
      "tags.BILLINGCODE",
      "tags.BILLINGCONTACT",
      "tags.CLIENT",
      "tags.COUNTRY",
      "tags.CSCLASS",
      "tags.CSQUAL",
      "tags.CSTYPE",
      "tags.ENVIRONMENT",
      "tags.FUNCTION",
      "tags.GROUPCONTACT",
      "tags.MEMBERFIRM",
      "tags.PRIMARYCONTACT",
      "tags.SECONDARYCONTACT",
      "tags.AccountName"]
  }

  tags = {
    Client = "Economical"
    Contract = "MANAGED_SERVICES"
    PatchGroup = "Windows-AzA-Group1"
    Owner = "robhatnagar@deloitte.ca"
    ManagedBy = "CMS-TF"
    SolutionName = "InsurCloud Economical"
    Environment = "Prod"
    ApplicationName = "rpa, blueprism"
    DataClassification = "PII, PHI"
    DLM-Backup = "Yes"
    DLM-Retention = "90"
    VendorManaged = "False"
    ResourceType = "Client"
    ResourceSeverity = "Normal"
    Name = "A-MC-CAC-D-A003-ECONOMICAL-PROD01-RPA-CLIENT19-WIN-EC2"
    AccountID = "${data.aws_caller_identity.current.account_id}"
  }
}

output "ICECONOMICAL_PROD01_RPA_CLIENT19_PRIVATEIP" {
  value = "${aws_instance.ICECONOMICAL_PROD01_RPA_CLIENT19.private_ip}"
}

data "template_file" "ECO_PROD01_DOMAIN_JOIN_RPA_C20" {
  template = "${file("IC-Economical-PROD/windowsdata.tpl")}"

  vars = {
    computer_name = "PROD01-RPA-C20"
    region = "${var.region}"
    domain = "${var.ad_domain}"
    dns1 = "${var.ad_dns1}"
    dns2 = "${var.ad_dns2}"
    dns3 = "${var.ad_dns3}"
    ouselection = "${var.ou_name}"
    pwordparameter = "${var.pwordparameter}"
    unameparameter = "${var.unameparameter}"
  }
}

resource "aws_instance" "ICECONOMICAL_PROD01_RPA_CLIENT20" {
  ami = "${var.ami_id_windows2016base_patch2020nov}"
  vpc_security_group_ids = [
    "${aws_security_group.SG_AVIATRIX_RDP.id}",
    "${aws_security_group.SG_RPA.id}",
    "${aws_security_group.SG_RPA_CLIENT.id}"
  ]
  subnet_id = "${var.economical-prod-private-1b-subnet}"
  instance_type = "t3a.medium"
  key_name = "EconomicalRPAPRD"
  associate_public_ip_address = false
  iam_instance_profile = "CMS-SSMInstanceRole"
  ebs_optimized = "true"

  root_block_device {
    volume_size = "50"
  }

  user_data = "${data.template_file.ECO_PROD01_DOMAIN_JOIN_RPA_C20.rendered}"

  lifecycle {
    prevent_destroy = true
    ignore_changes = [
      "tags.%",
      "tags.BILLINGCODE",
      "tags.BILLINGCONTACT",
      "tags.CLIENT",
      "tags.COUNTRY",
      "tags.CSCLASS",
      "tags.CSQUAL",
      "tags.CSTYPE",
      "tags.ENVIRONMENT",
      "tags.FUNCTION",
      "tags.GROUPCONTACT",
      "tags.MEMBERFIRM",
      "tags.PRIMARYCONTACT",
      "tags.SECONDARYCONTACT",
      "tags.AccountName"]
  }

  tags = {
    Client = "Economical"
    Contract = "MANAGED_SERVICES"
    PatchGroup = "Windows-AzB-Group1"
    Owner = "robhatnagar@deloitte.ca"
    ManagedBy = "CMS-TF"
    SolutionName = "InsurCloud Economical"
    Environment = "Prod"
    ApplicationName = "rpa, blueprism"
    DataClassification = "PII, PHI"
    DLM-Backup = "Yes"
    DLM-Retention = "90"
    VendorManaged = "False"
    ResourceType = "Client"
    ResourceSeverity = "Normal"
    Name = "A-MC-CAC-D-A003-ECONOMICAL-PROD01-RPA-CLIENT20-WIN-EC2"
    AccountID = "${data.aws_caller_identity.current.account_id}"
  }
}

output "ICECONOMICAL_PROD01_RPA_CLIENT20_PRIVATEIP" {
  value = "${aws_instance.ICECONOMICAL_PROD01_RPA_CLIENT20.private_ip}"
}

data "template_file" "ECO_PROD01_DOMAIN_JOIN_RPA_C21" {
  template = "${file("IC-Economical-PROD/windowsdata.tpl")}"

  vars = {
    computer_name = "PROD01-RPA-C21"
    region = "${var.region}"
    domain = "${var.ad_domain}"
    dns1 = "${var.ad_dns1}"
    dns2 = "${var.ad_dns2}"
    dns3 = "${var.ad_dns3}"
    ouselection = "${var.ou_name}"
    pwordparameter = "${var.pwordparameter}"
    unameparameter = "${var.unameparameter}"
  }
}

resource "aws_instance" "ICECONOMICAL_PROD01_RPA_CLIENT21" {
  ami = "${var.ami_id_windows2016base_patch2020nov}"
  vpc_security_group_ids = [
    "${aws_security_group.SG_AVIATRIX_RDP.id}",
    "${aws_security_group.SG_RPA.id}",
    "${aws_security_group.SG_RPA_CLIENT.id}"
  ]
  subnet_id = "${var.economical-prod-private-1a-subnet}"
  instance_type = "t3a.medium"
  key_name = "EconomicalRPAPRD"
  associate_public_ip_address = false
  iam_instance_profile = "CMS-SSMInstanceRole"
  ebs_optimized = "true"

  root_block_device {
    volume_size = "50"
  }

  user_data = "${data.template_file.ECO_PROD01_DOMAIN_JOIN_RPA_C21.rendered}"

  lifecycle {
    prevent_destroy = true
    ignore_changes = [
      "tags.%",
      "tags.BILLINGCODE",
      "tags.BILLINGCONTACT",
      "tags.CLIENT",
      "tags.COUNTRY",
      "tags.CSCLASS",
      "tags.CSQUAL",
      "tags.CSTYPE",
      "tags.ENVIRONMENT",
      "tags.FUNCTION",
      "tags.GROUPCONTACT",
      "tags.MEMBERFIRM",
      "tags.PRIMARYCONTACT",
      "tags.SECONDARYCONTACT",
      "tags.AccountName"]
  }

  tags = {
    Client = "Economical"
    Contract = "MANAGED_SERVICES"
    PatchGroup = "Windows-AzA-Group1"
    Owner = "robhatnagar@deloitte.ca"
    ManagedBy = "CMS-TF"
    SolutionName = "InsurCloud Economical"
    Environment = "Prod"
    ApplicationName = "rpa, blueprism"
    DataClassification = "PII, PHI"
    DLM-Backup = "Yes"
    DLM-Retention = "90"
    VendorManaged = "False"
    ResourceType = "Client"
    ResourceSeverity = "Normal"
    Name = "A-MC-CAC-D-A003-ECONOMICAL-PROD01-RPA-CLIENT21-WIN-EC2"
    AccountID = "${data.aws_caller_identity.current.account_id}"
  }
}

output "ICECONOMICAL_PROD01_RPA_CLIENT21_PRIVATEIP" {
  value = "${aws_instance.ICECONOMICAL_PROD01_RPA_CLIENT21.private_ip}"
}

data "template_file" "ECO_PROD01_DOMAIN_JOIN_RPA_C22" {
  template = "${file("IC-Economical-PROD/windowsdata.tpl")}"

  vars = {
    computer_name = "PROD01-RPA-C22"
    region = "${var.region}"
    domain = "${var.ad_domain}"
    dns1 = "${var.ad_dns1}"
    dns2 = "${var.ad_dns2}"
    dns3 = "${var.ad_dns3}"
    ouselection = "${var.ou_name}"
    pwordparameter = "${var.pwordparameter}"
    unameparameter = "${var.unameparameter}"
  }
}

resource "aws_instance" "ICECONOMICAL_PROD01_RPA_CLIENT22" {
  ami = "${var.ami_id_windows2016base_patch2020nov}"
  vpc_security_group_ids = [
    "${aws_security_group.SG_AVIATRIX_RDP.id}",
    "${aws_security_group.SG_RPA.id}",
    "${aws_security_group.SG_RPA_CLIENT.id}"
  ]
  subnet_id = "${var.economical-prod-private-1b-subnet}"
  instance_type = "t3a.medium"
  key_name = "EconomicalRPAPRD"
  associate_public_ip_address = false
  iam_instance_profile = "CMS-SSMInstanceRole"
  ebs_optimized = "true"

  root_block_device {
    volume_size = "50"
  }

  user_data = "${data.template_file.ECO_PROD01_DOMAIN_JOIN_RPA_C22.rendered}"

  lifecycle {
    prevent_destroy = true
    ignore_changes = [
      "tags.%",
      "tags.BILLINGCODE",
      "tags.BILLINGCONTACT",
      "tags.CLIENT",
      "tags.COUNTRY",
      "tags.CSCLASS",
      "tags.CSQUAL",
      "tags.CSTYPE",
      "tags.ENVIRONMENT",
      "tags.FUNCTION",
      "tags.GROUPCONTACT",
      "tags.MEMBERFIRM",
      "tags.PRIMARYCONTACT",
      "tags.SECONDARYCONTACT",
      "tags.AccountName"]
  }

  tags = {
    Client = "Economical"
    Contract = "MANAGED_SERVICES"
    PatchGroup = "Windows-AzB-Group1"
    Owner = "robhatnagar@deloitte.ca"
    ManagedBy = "CMS-TF"
    SolutionName = "InsurCloud Economical"
    Environment = "Prod"
    ApplicationName = "rpa, blueprism"
    DataClassification = "PII, PHI"
    DLM-Backup = "Yes"
    DLM-Retention = "90"
    VendorManaged = "False"
    ResourceType = "Client"
    ResourceSeverity = "Normal"
    Name = "A-MC-CAC-D-A003-ECONOMICAL-PROD01-RPA-CLIENT22-WIN-EC2"
    AccountID = "${data.aws_caller_identity.current.account_id}"
  }
}

output "ICECONOMICAL_PROD01_RPA_CLIENT22_PRIVATEIP" {
  value = "${aws_instance.ICECONOMICAL_PROD01_RPA_CLIENT22.private_ip}"
}

data "template_file" "ECO_PROD01_DOMAIN_JOIN_RPA_C23" {
  template = "${file("IC-Economical-PROD/windowsdata.tpl")}"

  vars = {
    computer_name = "PROD01-RPA-C23"
    region = "${var.region}"
    domain = "${var.ad_domain}"
    dns1 = "${var.ad_dns1}"
    dns2 = "${var.ad_dns2}"
    dns3 = "${var.ad_dns3}"
    ouselection = "${var.ou_name}"
    pwordparameter = "${var.pwordparameter}"
    unameparameter = "${var.unameparameter}"
  }
}

resource "aws_instance" "ICECONOMICAL_PROD01_RPA_CLIENT23" {
  ami = "${var.ami_id_windows2016base_patch2020nov}"
  vpc_security_group_ids = [
    "${aws_security_group.SG_AVIATRIX_RDP.id}",
    "${aws_security_group.SG_RPA.id}",
    "${aws_security_group.SG_RPA_CLIENT.id}"
  ]
  subnet_id = "${var.economical-prod-private-1a-subnet}"
  instance_type = "t3a.medium"
  key_name = "EconomicalRPAPRD"
  associate_public_ip_address = false
  iam_instance_profile = "CMS-SSMInstanceRole"
  ebs_optimized = "true"

  root_block_device {
    volume_size = "50"
  }

  user_data = "${data.template_file.ECO_PROD01_DOMAIN_JOIN_RPA_C23.rendered}"

  lifecycle {
    prevent_destroy = true
    ignore_changes = [
      "tags.%",
      "tags.BILLINGCODE",
      "tags.BILLINGCONTACT",
      "tags.CLIENT",
      "tags.COUNTRY",
      "tags.CSCLASS",
      "tags.CSQUAL",
      "tags.CSTYPE",
      "tags.ENVIRONMENT",
      "tags.FUNCTION",
      "tags.GROUPCONTACT",
      "tags.MEMBERFIRM",
      "tags.PRIMARYCONTACT",
      "tags.SECONDARYCONTACT",
      "tags.AccountName"]
  }

  tags = {
    Client = "Economical"
    Contract = "MANAGED_SERVICES"
    PatchGroup = "Windows-AzA-Group1"
    Owner = "robhatnagar@deloitte.ca"
    ManagedBy = "CMS-TF"
    SolutionName = "InsurCloud Economical"
    Environment = "Prod"
    ApplicationName = "rpa, blueprism"
    DataClassification = "PII, PHI"
    DLM-Backup = "Yes"
    DLM-Retention = "90"
    VendorManaged = "False"
    ResourceType = "Client"
    ResourceSeverity = "Normal"
    Name = "A-MC-CAC-D-A003-ECONOMICAL-PROD01-RPA-CLIENT23-WIN-EC2"
    AccountID = "${data.aws_caller_identity.current.account_id}"
  }
}

output "ICECONOMICAL_PROD01_RPA_CLIENT23_PRIVATEIP" {
  value = "${aws_instance.ICECONOMICAL_PROD01_RPA_CLIENT23.private_ip}"
}

data "template_file" "ECO_PROD01_DOMAIN_JOIN_RPA_C24" {
  template = "${file("IC-Economical-PROD/windowsdata.tpl")}"

  vars = {
    computer_name = "PROD01-RPA-C24"
    region = "${var.region}"
    domain = "${var.ad_domain}"
    dns1 = "${var.ad_dns1}"
    dns2 = "${var.ad_dns2}"
    dns3 = "${var.ad_dns3}"
    ouselection = "${var.ou_name}"
    pwordparameter = "${var.pwordparameter}"
    unameparameter = "${var.unameparameter}"
  }
}

resource "aws_instance" "ICECONOMICAL_PROD01_RPA_CLIENT24" {
  ami = "${var.ami_id_windows2016base_patch2020nov}"
  vpc_security_group_ids = [
    "${aws_security_group.SG_AVIATRIX_RDP.id}",
    "${aws_security_group.SG_RPA.id}",
    "${aws_security_group.SG_RPA_CLIENT.id}"
  ]
  subnet_id = "${var.economical-prod-private-1b-subnet}"
  instance_type = "t3a.medium"
  key_name = "EconomicalRPAPRD"
  associate_public_ip_address = false
  iam_instance_profile = "CMS-SSMInstanceRole"
  ebs_optimized = "true"

  root_block_device {
    volume_size = "50"
  }

  user_data = "${data.template_file.ECO_PROD01_DOMAIN_JOIN_RPA_C24.rendered}"

  lifecycle {
    prevent_destroy = true
    ignore_changes = [
      "tags.%",
      "tags.BILLINGCODE",
      "tags.BILLINGCONTACT",
      "tags.CLIENT",
      "tags.COUNTRY",
      "tags.CSCLASS",
      "tags.CSQUAL",
      "tags.CSTYPE",
      "tags.ENVIRONMENT",
      "tags.FUNCTION",
      "tags.GROUPCONTACT",
      "tags.MEMBERFIRM",
      "tags.PRIMARYCONTACT",
      "tags.SECONDARYCONTACT",
      "tags.AccountName"]
  }

  tags = {
    Client = "Economical"
    Contract = "MANAGED_SERVICES"
    PatchGroup = "Windows-AzB-Group1"
    Owner = "robhatnagar@deloitte.ca"
    ManagedBy = "CMS-TF"
    SolutionName = "InsurCloud Economical"
    Environment = "Prod"
    ApplicationName = "rpa, blueprism"
    DataClassification = "PII, PHI"
    DLM-Backup = "Yes"
    DLM-Retention = "90"
    VendorManaged = "False"
    ResourceType = "Client"
    ResourceSeverity = "Normal"
    Name = "A-MC-CAC-D-A003-ECONOMICAL-PROD01-RPA-CLIENT24-WIN-EC2"
    AccountID = "${data.aws_caller_identity.current.account_id}"
  }
}

output "ICECONOMICAL_PROD01_RPA_CLIENT24_PRIVATEIP" {
  value = "${aws_instance.ICECONOMICAL_PROD01_RPA_CLIENT24.private_ip}"
}

data "template_file" "ECO_PROD01_DOMAIN_JOIN_RPA_C25" {
  template = "${file("IC-Economical-PROD/windowsdata.tpl")}"

  vars = {
    computer_name = "PROD01-RPA-C25"
    region = "${var.region}"
    domain = "${var.ad_domain}"
    dns1 = "${var.ad_dns1}"
    dns2 = "${var.ad_dns2}"
    dns3 = "${var.ad_dns3}"
    ouselection = "${var.ou_name}"
    pwordparameter = "${var.pwordparameter}"
    unameparameter = "${var.unameparameter}"
  }
}

resource "aws_instance" "ICECONOMICAL_PROD01_RPA_CLIENT25" {
  ami = "${var.ami_id_windows2016base_patch2020nov}"
  vpc_security_group_ids = [
    "${aws_security_group.SG_AVIATRIX_RDP.id}",
    "${aws_security_group.SG_RPA.id}",
    "${aws_security_group.SG_RPA_CLIENT.id}"
  ]
  subnet_id = "${var.economical-prod-private-1a-subnet}"
  instance_type = "t3a.medium"
  key_name = "EconomicalRPAPRD"
  associate_public_ip_address = false
  iam_instance_profile = "CMS-SSMInstanceRole"
  ebs_optimized = "true"

  root_block_device {
    volume_size = "50"
  }

  user_data = "${data.template_file.ECO_PROD01_DOMAIN_JOIN_RPA_C25.rendered}"

  lifecycle {
    prevent_destroy = true
    ignore_changes = [
      "tags.%",
      "tags.BILLINGCODE",
      "tags.BILLINGCONTACT",
      "tags.CLIENT",
      "tags.COUNTRY",
      "tags.CSCLASS",
      "tags.CSQUAL",
      "tags.CSTYPE",
      "tags.ENVIRONMENT",
      "tags.FUNCTION",
      "tags.GROUPCONTACT",
      "tags.MEMBERFIRM",
      "tags.PRIMARYCONTACT",
      "tags.SECONDARYCONTACT",
      "tags.AccountName"]
  }

  tags = {
    Client = "Economical"
    Contract = "MANAGED_SERVICES"
    PatchGroup = "Windows-AzA-Group1"
    Owner = "robhatnagar@deloitte.ca"
    ManagedBy = "CMS-TF"
    SolutionName = "InsurCloud Economical"
    Environment = "Prod"
    ApplicationName = "rpa, blueprism"
    DataClassification = "PII, PHI"
    DLM-Backup = "Yes"
    DLM-Retention = "90"
    VendorManaged = "False"
    ResourceType = "Client"
    ResourceSeverity = "Normal"
    Name = "A-MC-CAC-D-A003-ECONOMICAL-PROD01-RPA-CLIENT25-WIN-EC2"
    AccountID = "${data.aws_caller_identity.current.account_id}"
  }
}

output "ICECONOMICAL_PROD01_RPA_CLIENT25_PRIVATEIP" {
  value = "${aws_instance.ICECONOMICAL_PROD01_RPA_CLIENT25.private_ip}"
}

data "template_file" "ECO_PROD01_DOMAIN_JOIN_RPA_C26" {
  template = "${file("IC-Economical-PROD/windowsdata.tpl")}"

  vars = {
    computer_name = "PROD01-RPA-C26"
    region = "${var.region}"
    domain = "${var.ad_domain}"
    dns1 = "${var.ad_dns1}"
    dns2 = "${var.ad_dns2}"
    dns3 = "${var.ad_dns3}"
    ouselection = "${var.ou_name}"
    pwordparameter = "${var.pwordparameter}"
    unameparameter = "${var.unameparameter}"
  }
}

resource "aws_instance" "ICECONOMICAL_PROD01_RPA_CLIENT26" {
  ami = "${var.ami_id_windows2016base_patch2020nov}"
  vpc_security_group_ids = [
    "${aws_security_group.SG_AVIATRIX_RDP.id}",
    "${aws_security_group.SG_RPA.id}",
    "${aws_security_group.SG_RPA_CLIENT.id}"
  ]
  subnet_id = "${var.economical-prod-private-1b-subnet}"
  instance_type = "t3a.medium"
  key_name = "EconomicalRPAPRD"
  associate_public_ip_address = false
  iam_instance_profile = "CMS-SSMInstanceRole"
  ebs_optimized = "true"

  root_block_device {
    volume_size = "50"
  }

  user_data = "${data.template_file.ECO_PROD01_DOMAIN_JOIN_RPA_C26.rendered}"

  lifecycle {
    prevent_destroy = true
    ignore_changes = [
      "tags.%",
      "tags.BILLINGCODE",
      "tags.BILLINGCONTACT",
      "tags.CLIENT",
      "tags.COUNTRY",
      "tags.CSCLASS",
      "tags.CSQUAL",
      "tags.CSTYPE",
      "tags.ENVIRONMENT",
      "tags.FUNCTION",
      "tags.GROUPCONTACT",
      "tags.MEMBERFIRM",
      "tags.PRIMARYCONTACT",
      "tags.SECONDARYCONTACT",
      "tags.AccountName"]
  }

  tags = {
    Client = "Economical"
    Contract = "MANAGED_SERVICES"
    PatchGroup = "Windows-AzB-Group1"
    Owner = "robhatnagar@deloitte.ca"
    ManagedBy = "CMS-TF"
    SolutionName = "InsurCloud Economical"
    Environment = "Prod"
    ApplicationName = "rpa, blueprism"
    DataClassification = "PII, PHI"
    DLM-Backup = "Yes"
    DLM-Retention = "90"
    VendorManaged = "False"
    ResourceType = "Client"
    ResourceSeverity = "Normal"
    Name = "A-MC-CAC-D-A003-ECONOMICAL-PROD01-RPA-CLIENT26-WIN-EC2"
    AccountID = "${data.aws_caller_identity.current.account_id}"
  }
}

output "ICECONOMICAL_PROD01_RPA_CLIENT26_PRIVATEIP" {
  value = "${aws_instance.ICECONOMICAL_PROD01_RPA_CLIENT26.private_ip}"
}

data "template_file" "ECO_PROD01_DOMAIN_JOIN_RPA_C27" {
  template = "${file("IC-Economical-PROD/windowsdata.tpl")}"

  vars = {
    computer_name = "PROD01-RPA-C27"
    region = "${var.region}"
    domain = "${var.ad_domain}"
    dns1 = "${var.ad_dns1}"
    dns2 = "${var.ad_dns2}"
    dns3 = "${var.ad_dns3}"
    ouselection = "${var.ou_name}"
    pwordparameter = "${var.pwordparameter}"
    unameparameter = "${var.unameparameter}"
  }
}

resource "aws_instance" "ICECONOMICAL_PROD01_RPA_CLIENT27" {
  ami = "${var.ami_id_windows2016base_patch2020nov}"
  vpc_security_group_ids = [
    "${aws_security_group.SG_AVIATRIX_RDP.id}",
    "${aws_security_group.SG_RPA.id}",
    "${aws_security_group.SG_RPA_CLIENT.id}"
  ]
  subnet_id = "${var.economical-prod-private-1a-subnet}"
  instance_type = "t3a.medium"
  key_name = "EconomicalRPAPRD"
  associate_public_ip_address = false
  iam_instance_profile = "CMS-SSMInstanceRole"
  ebs_optimized = "true"

  root_block_device {
    volume_size = "50"
  }

  user_data = "${data.template_file.ECO_PROD01_DOMAIN_JOIN_RPA_C27.rendered}"

  lifecycle {
    prevent_destroy = true
    ignore_changes = [
      "tags.%",
      "tags.BILLINGCODE",
      "tags.BILLINGCONTACT",
      "tags.CLIENT",
      "tags.COUNTRY",
      "tags.CSCLASS",
      "tags.CSQUAL",
      "tags.CSTYPE",
      "tags.ENVIRONMENT",
      "tags.FUNCTION",
      "tags.GROUPCONTACT",
      "tags.MEMBERFIRM",
      "tags.PRIMARYCONTACT",
      "tags.SECONDARYCONTACT",
      "tags.AccountName"]
  }

  tags = {
    Client = "Economical"
    Contract = "MANAGED_SERVICES"
    PatchGroup = "Windows-AzA-Group1"
    Owner = "robhatnagar@deloitte.ca"
    ManagedBy = "CMS-TF"
    SolutionName = "InsurCloud Economical"
    Environment = "Prod"
    ApplicationName = "rpa, blueprism"
    DataClassification = "PII, PHI"
    DLM-Backup = "Yes"
    DLM-Retention = "90"
    VendorManaged = "False"
    ResourceType = "Client"
    ResourceSeverity = "Normal"
    Name = "A-MC-CAC-D-A003-ECONOMICAL-PROD01-RPA-CLIENT27-WIN-EC2"
    AccountID = "${data.aws_caller_identity.current.account_id}"
  }
}

output "ICECONOMICAL_PROD01_RPA_CLIENT27_PRIVATEIP" {
  value = "${aws_instance.ICECONOMICAL_PROD01_RPA_CLIENT27.private_ip}"
}

data "template_file" "ECO_PROD01_DOMAIN_JOIN_RPA_C28" {
  template = "${file("IC-Economical-PROD/windowsdata.tpl")}"

  vars = {
    computer_name = "PROD01-RPA-C28"
    region = "${var.region}"
    domain = "${var.ad_domain}"
    dns1 = "${var.ad_dns1}"
    dns2 = "${var.ad_dns2}"
    dns3 = "${var.ad_dns3}"
    ouselection = "${var.ou_name}"
    pwordparameter = "${var.pwordparameter}"
    unameparameter = "${var.unameparameter}"
  }
}

resource "aws_instance" "ICECONOMICAL_PROD01_RPA_CLIENT28" {
  ami = "${var.ami_id_windows2016base_patch2020nov}"
  vpc_security_group_ids = [
    "${aws_security_group.SG_AVIATRIX_RDP.id}",
    "${aws_security_group.SG_RPA.id}",
    "${aws_security_group.SG_RPA_CLIENT.id}"
  ]
  subnet_id = "${var.economical-prod-private-1b-subnet}"
  instance_type = "t3a.medium"
  key_name = "EconomicalRPAPRD"
  associate_public_ip_address = false
  iam_instance_profile = "CMS-SSMInstanceRole"
  ebs_optimized = "true"

  root_block_device {
    volume_size = "50"
  }

  user_data = "${data.template_file.ECO_PROD01_DOMAIN_JOIN_RPA_C28.rendered}"

  lifecycle {
    prevent_destroy = true
    ignore_changes = [
      "tags.%",
      "tags.BILLINGCODE",
      "tags.BILLINGCONTACT",
      "tags.CLIENT",
      "tags.COUNTRY",
      "tags.CSCLASS",
      "tags.CSQUAL",
      "tags.CSTYPE",
      "tags.ENVIRONMENT",
      "tags.FUNCTION",
      "tags.GROUPCONTACT",
      "tags.MEMBERFIRM",
      "tags.PRIMARYCONTACT",
      "tags.SECONDARYCONTACT",
      "tags.AccountName"]
  }

  tags = {
    Client = "Economical"
    Contract = "MANAGED_SERVICES"
    PatchGroup = "Windows-AzB-Group1"
    Owner = "robhatnagar@deloitte.ca"
    ManagedBy = "CMS-TF"
    SolutionName = "InsurCloud Economical"
    Environment = "Prod"
    ApplicationName = "rpa, blueprism"
    DataClassification = "PII, PHI"
    DLM-Backup = "Yes"
    DLM-Retention = "90"
    VendorManaged = "False"
    ResourceType = "Client"
    ResourceSeverity = "Normal"
    Name = "A-MC-CAC-D-A003-ECONOMICAL-PROD01-RPA-CLIENT28-WIN-EC2"
    AccountID = "${data.aws_caller_identity.current.account_id}"
  }
}

output "ICECONOMICAL_PROD01_RPA_CLIENT28_PRIVATEIP" {
  value = "${aws_instance.ICECONOMICAL_PROD01_RPA_CLIENT28.private_ip}"
}

data "template_file" "ECO_PROD01_DOMAIN_JOIN_RPA_C29" {
  template = "${file("IC-Economical-PROD/windowsdata.tpl")}"

  vars = {
    computer_name = "PROD01-RPA-C29"
    region = "${var.region}"
    domain = "${var.ad_domain}"
    dns1 = "${var.ad_dns1}"
    dns2 = "${var.ad_dns2}"
    dns3 = "${var.ad_dns3}"
    ouselection = "${var.ou_name}"
    pwordparameter = "${var.pwordparameter}"
    unameparameter = "${var.unameparameter}"
  }
}

resource "aws_instance" "ICECONOMICAL_PROD01_RPA_CLIENT29" {
  ami = "${var.ami_id_windows2016base_patch2020nov}"
  vpc_security_group_ids = [
    "${aws_security_group.SG_AVIATRIX_RDP.id}",
    "${aws_security_group.SG_RPA.id}",
    "${aws_security_group.SG_RPA_CLIENT.id}"
  ]
  subnet_id = "${var.economical-prod-private-1a-subnet}"
  instance_type = "t3a.medium"
  key_name = "EconomicalRPAPRD"
  associate_public_ip_address = false
  iam_instance_profile = "CMS-SSMInstanceRole"
  ebs_optimized = "true"

  root_block_device {
    volume_size = "50"
  }

  user_data = "${data.template_file.ECO_PROD01_DOMAIN_JOIN_RPA_C29.rendered}"

  lifecycle {
    prevent_destroy = true
    ignore_changes = [
      "tags.%",
      "tags.BILLINGCODE",
      "tags.BILLINGCONTACT",
      "tags.CLIENT",
      "tags.COUNTRY",
      "tags.CSCLASS",
      "tags.CSQUAL",
      "tags.CSTYPE",
      "tags.ENVIRONMENT",
      "tags.FUNCTION",
      "tags.GROUPCONTACT",
      "tags.MEMBERFIRM",
      "tags.PRIMARYCONTACT",
      "tags.SECONDARYCONTACT",
      "tags.AccountName"]
  }

  tags = {
    Client = "Economical"
    Contract = "MANAGED_SERVICES"
    PatchGroup = "Windows-AzA-Group1"
    Owner = "robhatnagar@deloitte.ca"
    ManagedBy = "CMS-TF"
    SolutionName = "InsurCloud Economical"
    Environment = "Prod"
    ApplicationName = "rpa, blueprism"
    DataClassification = "PII, PHI"
    DLM-Backup = "Yes"
    DLM-Retention = "90"
    VendorManaged = "False"
    ResourceType = "Client"
    ResourceSeverity = "Normal"
    Name = "A-MC-CAC-D-A003-ECONOMICAL-PROD01-RPA-CLIENT29-WIN-EC2"
    AccountID = "${data.aws_caller_identity.current.account_id}"
  }
}

output "ICECONOMICAL_PROD01_RPA_CLIENT29_PRIVATEIP" {
  value = "${aws_instance.ICECONOMICAL_PROD01_RPA_CLIENT29.private_ip}"
}

data "template_file" "ECO_PROD01_DOMAIN_JOIN_RPA_C30" {
  template = "${file("IC-Economical-PROD/windowsdata.tpl")}"

  vars = {
    computer_name = "PROD01-RPA-C30"
    region = "${var.region}"
    domain = "${var.ad_domain}"
    dns1 = "${var.ad_dns1}"
    dns2 = "${var.ad_dns2}"
    dns3 = "${var.ad_dns3}"
    ouselection = "${var.ou_name}"
    pwordparameter = "${var.pwordparameter}"
    unameparameter = "${var.unameparameter}"
  }
}

resource "aws_instance" "ICECONOMICAL_PROD01_RPA_CLIENT30" {
  ami = "${var.ami_id_windows2016base_patch2020nov}"
  vpc_security_group_ids = [
    "${aws_security_group.SG_AVIATRIX_RDP.id}",
    "${aws_security_group.SG_RPA.id}",
    "${aws_security_group.SG_RPA_CLIENT.id}"
  ]
  subnet_id = "${var.economical-prod-private-1b-subnet}"
  instance_type = "t3a.medium"
  key_name = "EconomicalRPAPRD"
  associate_public_ip_address = false
  iam_instance_profile = "CMS-SSMInstanceRole"
  ebs_optimized = "true"

  root_block_device {
    volume_size = "50"
  }

  user_data = "${data.template_file.ECO_PROD01_DOMAIN_JOIN_RPA_C30.rendered}"

  lifecycle {
    prevent_destroy = true
    ignore_changes = [
      "tags.%",
      "tags.BILLINGCODE",
      "tags.BILLINGCONTACT",
      "tags.CLIENT",
      "tags.COUNTRY",
      "tags.CSCLASS",
      "tags.CSQUAL",
      "tags.CSTYPE",
      "tags.ENVIRONMENT",
      "tags.FUNCTION",
      "tags.GROUPCONTACT",
      "tags.MEMBERFIRM",
      "tags.PRIMARYCONTACT",
      "tags.SECONDARYCONTACT",
      "tags.AccountName"]
  }

  tags = {
    Client = "Economical"
    Contract = "MANAGED_SERVICES"
    PatchGroup = "Windows-AzB-Group1"
    Owner = "robhatnagar@deloitte.ca"
    ManagedBy = "CMS-TF"
    SolutionName = "InsurCloud Economical"
    Environment = "Prod"
    ApplicationName = "rpa, blueprism"
    DataClassification = "PII, PHI"
    DLM-Backup = "Yes"
    DLM-Retention = "90"
    VendorManaged = "False"
    ResourceType = "Client"
    ResourceSeverity = "Normal"
    Name = "A-MC-CAC-D-A003-ECONOMICAL-PROD01-RPA-CLIENT30-WIN-EC2"
    AccountID = "${data.aws_caller_identity.current.account_id}"
  }
}

output "ICECONOMICAL_PROD01_RPA_CLIENT30_PRIVATEIP" {
  value = "${aws_instance.ICECONOMICAL_PROD01_RPA_CLIENT30.private_ip}"
}

data "template_file" "ECO_PROD01_DOMAIN_JOIN_RPA_C31" {
  template = "${file("IC-Economical-PROD/windowsdata.tpl")}"

  vars = {
    computer_name = "PROD01-RPA-C31"
    region = "${var.region}"
    domain = "${var.ad_domain}"
    dns1 = "${var.ad_dns1}"
    dns2 = "${var.ad_dns2}"
    dns3 = "${var.ad_dns3}"
    ouselection = "${var.ou_name}"
    pwordparameter = "${var.pwordparameter}"
    unameparameter = "${var.unameparameter}"
  }
}

resource "aws_instance" "ICECONOMICAL_PROD01_RPA_CLIENT31" {
  ami = "${var.ami_id_windows2016base_patch2020nov}"
  vpc_security_group_ids = [
    "${aws_security_group.SG_AVIATRIX_RDP.id}",
    "${aws_security_group.SG_RPA.id}",
    "${aws_security_group.SG_RPA_CLIENT.id}"
  ]
  subnet_id = "${var.economical-prod-private-1a-subnet}"
  instance_type = "t3a.medium"
  key_name = "EconomicalRPAPRD"
  associate_public_ip_address = false
  iam_instance_profile = "CMS-SSMInstanceRole"
  ebs_optimized = "true"

  root_block_device {
    volume_size = "50"
  }

  user_data = "${data.template_file.ECO_PROD01_DOMAIN_JOIN_RPA_C31.rendered}"

  lifecycle {
    prevent_destroy = true
    ignore_changes = [
      "tags.%",
      "tags.BILLINGCODE",
      "tags.BILLINGCONTACT",
      "tags.CLIENT",
      "tags.COUNTRY",
      "tags.CSCLASS",
      "tags.CSQUAL",
      "tags.CSTYPE",
      "tags.ENVIRONMENT",
      "tags.FUNCTION",
      "tags.GROUPCONTACT",
      "tags.MEMBERFIRM",
      "tags.PRIMARYCONTACT",
      "tags.SECONDARYCONTACT",
      "tags.AccountName"]
  }

  tags = {
    Client = "Economical"
    Contract = "MANAGED_SERVICES"
    PatchGroup = "Windows-AzA-Group1"
    Owner = "robhatnagar@deloitte.ca"
    ManagedBy = "CMS-TF"
    SolutionName = "InsurCloud Economical"
    Environment = "Prod"
    ApplicationName = "rpa, blueprism"
    DataClassification = "PII, PHI"
    DLM-Backup = "Yes"
    DLM-Retention = "90"
    VendorManaged = "False"
    ResourceType = "Client"
    ResourceSeverity = "Normal"
    Name = "A-MC-CAC-D-A003-ECONOMICAL-PROD01-RPA-CLIENT31-WIN-EC2"
    AccountID = "${data.aws_caller_identity.current.account_id}"
  }
}

output "ICECONOMICAL_PROD01_RPA_CLIENT31_PRIVATEIP" {
  value = "${aws_instance.ICECONOMICAL_PROD01_RPA_CLIENT31.private_ip}"
}

data "template_file" "ECO_PROD01_DOMAIN_JOIN_RPA_C32" {
  template = "${file("IC-Economical-PROD/windowsdata.tpl")}"

  vars = {
    computer_name = "PROD01-RPA-C32"
    region = "${var.region}"
    domain = "${var.ad_domain}"
    dns1 = "${var.ad_dns1}"
    dns2 = "${var.ad_dns2}"
    dns3 = "${var.ad_dns3}"
    ouselection = "${var.ou_name}"
    pwordparameter = "${var.pwordparameter}"
    unameparameter = "${var.unameparameter}"
  }
}

resource "aws_instance" "ICECONOMICAL_PROD01_RPA_CLIENT32" {
  ami = "${var.ami_id_windows2016base_patch2020dec}"
  vpc_security_group_ids = [
    "${aws_security_group.SG_AVIATRIX_RDP.id}",
    "${aws_security_group.SG_RPA.id}",
    "${aws_security_group.SG_RPA_CLIENT.id}"
  ]
  subnet_id = "${var.economical-prod-private-1b-subnet}"
  instance_type = "t3a.medium"
  key_name = "EconomicalRPAPRD"
  associate_public_ip_address = false
  iam_instance_profile = "CMS-SSMInstanceRole"
  ebs_optimized = "true"

  root_block_device {
    volume_size = "50"
  }

  user_data = "${data.template_file.ECO_PROD01_DOMAIN_JOIN_RPA_C32.rendered}"

  lifecycle {
    prevent_destroy = true
    ignore_changes = [
      "tags.%",
      "tags.BILLINGCODE",
      "tags.BILLINGCONTACT",
      "tags.CLIENT",
      "tags.COUNTRY",
      "tags.CSCLASS",
      "tags.CSQUAL",
      "tags.CSTYPE",
      "tags.ENVIRONMENT",
      "tags.FUNCTION",
      "tags.GROUPCONTACT",
      "tags.MEMBERFIRM",
      "tags.PRIMARYCONTACT",
      "tags.SECONDARYCONTACT",
      "tags.AccountName"]
  }

  tags = {
    Client = "Economical"
    Contract = "MANAGED_SERVICES"
    PatchGroup = "Windows-AzB-Group1"
    Owner = "robhatnagar@deloitte.ca"
    ManagedBy = "CMS-TF"
    SolutionName = "InsurCloud Economical"
    Environment = "Prod"
    ApplicationName = "rpa, blueprism"
    DataClassification = "PII, PHI"
    DLM-Backup = "Yes"
    DLM-Retention = "90"
    VendorManaged = "False"
    ResourceType = "Client"
    ResourceSeverity = "Normal"
    Name = "A-MC-CAC-D-A003-ECONOMICAL-PROD01-RPA-CLIENT32-WIN-EC2"
    AccountID = "${data.aws_caller_identity.current.account_id}"
  }
}

output "ICECONOMICAL_PROD01_RPA_CLIENT32_PRIVATEIP" {
  value = "${aws_instance.ICECONOMICAL_PROD01_RPA_CLIENT32.private_ip}"
}

data "template_file" "ECO_PROD01_DOMAIN_JOIN_RPA_C33" {
  template = "${file("IC-Economical-PROD/windowsdata.tpl")}"

  vars = {
    computer_name = "PROD01-RPA-C33"
    region = "${var.region}"
    domain = "${var.ad_domain}"
    dns1 = "${var.ad_dns1}"
    dns2 = "${var.ad_dns2}"
    dns3 = "${var.ad_dns3}"
    ouselection = "${var.ou_name}"
    pwordparameter = "${var.pwordparameter}"
    unameparameter = "${var.unameparameter}"
  }
}

resource "aws_instance" "ICECONOMICAL_PROD01_RPA_CLIENT33" {
  ami = "${var.ami_id_windows2016base_patch2020dec}"
  vpc_security_group_ids = [
    "${aws_security_group.SG_AVIATRIX_RDP.id}",
    "${aws_security_group.SG_RPA.id}",
    "${aws_security_group.SG_RPA_CLIENT.id}"
  ]
  subnet_id = "${var.economical-prod-private-1a-subnet}"
  instance_type = "t3a.medium"
  key_name = "EconomicalRPAPRD"
  associate_public_ip_address = false
  iam_instance_profile = "CMS-SSMInstanceRole"
  ebs_optimized = "true"

  root_block_device {
    volume_size = "50"
  }

  user_data = "${data.template_file.ECO_PROD01_DOMAIN_JOIN_RPA_C33.rendered}"

  lifecycle {
    prevent_destroy = true
    ignore_changes = [
      "tags.%",
      "tags.BILLINGCODE",
      "tags.BILLINGCONTACT",
      "tags.CLIENT",
      "tags.COUNTRY",
      "tags.CSCLASS",
      "tags.CSQUAL",
      "tags.CSTYPE",
      "tags.ENVIRONMENT",
      "tags.FUNCTION",
      "tags.GROUPCONTACT",
      "tags.MEMBERFIRM",
      "tags.PRIMARYCONTACT",
      "tags.SECONDARYCONTACT",
      "tags.AccountName"]
  }

  tags = {
    Client = "Economical"
    Contract = "MANAGED_SERVICES"
    PatchGroup = "Windows-AzA-Group1"
    Owner = "robhatnagar@deloitte.ca"
    ManagedBy = "CMS-TF"
    SolutionName = "InsurCloud Economical"
    Environment = "Prod"
    ApplicationName = "rpa, blueprism"
    DataClassification = "PII, PHI"
    DLM-Backup = "Yes"
    DLM-Retention = "90"
    VendorManaged = "False"
    ResourceType = "Client"
    ResourceSeverity = "Normal"
    Name = "A-MC-CAC-D-A003-ECONOMICAL-PROD01-RPA-CLIENT33-WIN-EC2"
    AccountID = "${data.aws_caller_identity.current.account_id}"
  }
}

output "ICECONOMICAL_PROD01_RPA_CLIENT33_PRIVATEIP" {
  value = "${aws_instance.ICECONOMICAL_PROD01_RPA_CLIENT33.private_ip}"
}

data "template_file" "ECO_PROD01_DOMAIN_JOIN_RPA_C34" {
  template = "${file("IC-Economical-PROD/windowsdata.tpl")}"

  vars = {
    computer_name = "PROD01-RPA-C34"
    region = "${var.region}"
    domain = "${var.ad_domain}"
    dns1 = "${var.ad_dns1}"
    dns2 = "${var.ad_dns2}"
    dns3 = "${var.ad_dns3}"
    ouselection = "${var.ou_name}"
    pwordparameter = "${var.pwordparameter}"
    unameparameter = "${var.unameparameter}"
  }
}

resource "aws_instance" "ICECONOMICAL_PROD01_RPA_CLIENT34" {
  ami = "${var.ami_id_windows2016base_patch2020dec}"
  vpc_security_group_ids = [
    "${aws_security_group.SG_AVIATRIX_RDP.id}",
    "${aws_security_group.SG_RPA.id}",
    "${aws_security_group.SG_RPA_CLIENT.id}"
  ]
  subnet_id = "${var.economical-prod-private-1b-subnet}"
  instance_type = "t3a.medium"
  key_name = "EconomicalRPAPRD"
  associate_public_ip_address = false
  iam_instance_profile = "CMS-SSMInstanceRole"
  ebs_optimized = "true"

  root_block_device {
    volume_size = "50"
  }

  user_data = "${data.template_file.ECO_PROD01_DOMAIN_JOIN_RPA_C34.rendered}"

  lifecycle {
    prevent_destroy = true
    ignore_changes = [
      "tags.%",
      "tags.BILLINGCODE",
      "tags.BILLINGCONTACT",
      "tags.CLIENT",
      "tags.COUNTRY",
      "tags.CSCLASS",
      "tags.CSQUAL",
      "tags.CSTYPE",
      "tags.ENVIRONMENT",
      "tags.FUNCTION",
      "tags.GROUPCONTACT",
      "tags.MEMBERFIRM",
      "tags.PRIMARYCONTACT",
      "tags.SECONDARYCONTACT",
      "tags.AccountName"]
  }

  tags = {
    Client = "Economical"
    Contract = "MANAGED_SERVICES"
    PatchGroup = "Windows-AzB-Group1"
    Owner = "robhatnagar@deloitte.ca"
    ManagedBy = "CMS-TF"
    SolutionName = "InsurCloud Economical"
    Environment = "Prod"
    ApplicationName = "rpa, blueprism"
    DataClassification = "PII, PHI"
    DLM-Backup = "Yes"
    DLM-Retention = "90"
    VendorManaged = "False"
    ResourceType = "Client"
    ResourceSeverity = "Normal"
    Name = "A-MC-CAC-D-A003-ECONOMICAL-PROD01-RPA-CLIENT34-WIN-EC2"
    AccountID = "${data.aws_caller_identity.current.account_id}"
  }
}

output "ICECONOMICAL_PROD01_RPA_CLIENT34_PRIVATEIP" {
  value = "${aws_instance.ICECONOMICAL_PROD01_RPA_CLIENT34.private_ip}"
}

data "template_file" "ECO_PROD01_DOMAIN_JOIN_RPA_C35" {
  template = "${file("IC-Economical-PROD/windowsdata.tpl")}"

  vars = {
    computer_name = "PROD01-RPA-C35"
    region = "${var.region}"
    domain = "${var.ad_domain}"
    dns1 = "${var.ad_dns1}"
    dns2 = "${var.ad_dns2}"
    dns3 = "${var.ad_dns3}"
    ouselection = "${var.ou_name}"
    pwordparameter = "${var.pwordparameter}"
    unameparameter = "${var.unameparameter}"
  }
}

resource "aws_instance" "ICECONOMICAL_PROD01_RPA_CLIENT35" {
  ami = "${var.ami_id_windows2016base_patch2020dec}"
  vpc_security_group_ids = [
    "${aws_security_group.SG_AVIATRIX_RDP.id}",
    "${aws_security_group.SG_RPA.id}",
    "${aws_security_group.SG_RPA_CLIENT.id}"
  ]
  subnet_id = "${var.economical-prod-private-1a-subnet}"
  instance_type = "t3a.medium"
  key_name = "EconomicalRPAPRD"
  associate_public_ip_address = false
  iam_instance_profile = "CMS-SSMInstanceRole"
  ebs_optimized = "true"

  root_block_device {
    volume_size = "50"
  }

  user_data = "${data.template_file.ECO_PROD01_DOMAIN_JOIN_RPA_C35.rendered}"

  lifecycle {
    prevent_destroy = true
    ignore_changes = [
      "tags.%",
      "tags.BILLINGCODE",
      "tags.BILLINGCONTACT",
      "tags.CLIENT",
      "tags.COUNTRY",
      "tags.CSCLASS",
      "tags.CSQUAL",
      "tags.CSTYPE",
      "tags.ENVIRONMENT",
      "tags.FUNCTION",
      "tags.GROUPCONTACT",
      "tags.MEMBERFIRM",
      "tags.PRIMARYCONTACT",
      "tags.SECONDARYCONTACT",
      "tags.AccountName"]
  }

  tags = {
    Client = "Economical"
    Contract = "MANAGED_SERVICES"
    PatchGroup = "Windows-AzA-Group1"
    Owner = "robhatnagar@deloitte.ca"
    ManagedBy = "CMS-TF"
    SolutionName = "InsurCloud Economical"
    Environment = "Prod"
    ApplicationName = "rpa, blueprism"
    DataClassification = "PII, PHI"
    DLM-Backup = "Yes"
    DLM-Retention = "90"
    VendorManaged = "False"
    ResourceType = "Client"
    ResourceSeverity = "Normal"
    Name = "A-MC-CAC-D-A003-ECONOMICAL-PROD01-RPA-CLIENT35-WIN-EC2"
    AccountID = "${data.aws_caller_identity.current.account_id}"
  }
}

output "ICECONOMICAL_PROD01_RPA_CLIENT35_PRIVATEIP" {
  value = "${aws_instance.ICECONOMICAL_PROD01_RPA_CLIENT35.private_ip}"
}

data "template_file" "ECO_PROD01_DOMAIN_JOIN_RPA_C36" {
  template = "${file("IC-Economical-PROD/windowsdata.tpl")}"

  vars = {
    computer_name = "PROD01-RPA-C36"
    region = "${var.region}"
    domain = "${var.ad_domain}"
    dns1 = "${var.ad_dns1}"
    dns2 = "${var.ad_dns2}"
    dns3 = "${var.ad_dns3}"
    ouselection = "${var.ou_name}"
    pwordparameter = "${var.pwordparameter}"
    unameparameter = "${var.unameparameter}"
  }
}

resource "aws_instance" "ICECONOMICAL_PROD01_RPA_CLIENT36" {
  ami = "${var.ami_id_windows2016base_patch2020dec}"
  vpc_security_group_ids = [
    "${aws_security_group.SG_AVIATRIX_RDP.id}",
    "${aws_security_group.SG_RPA.id}",
    "${aws_security_group.SG_RPA_CLIENT.id}"
  ]
  subnet_id = "${var.economical-prod-private-1b-subnet}"
  instance_type = "t3a.medium"
  key_name = "EconomicalRPAPRD"
  associate_public_ip_address = false
  iam_instance_profile = "CMS-SSMInstanceRole"
  ebs_optimized = "true"

  root_block_device {
    volume_size = "50"
  }

  user_data = "${data.template_file.ECO_PROD01_DOMAIN_JOIN_RPA_C36.rendered}"

  lifecycle {
    prevent_destroy = true
    ignore_changes = [
      "tags.%",
      "tags.BILLINGCODE",
      "tags.BILLINGCONTACT",
      "tags.CLIENT",
      "tags.COUNTRY",
      "tags.CSCLASS",
      "tags.CSQUAL",
      "tags.CSTYPE",
      "tags.ENVIRONMENT",
      "tags.FUNCTION",
      "tags.GROUPCONTACT",
      "tags.MEMBERFIRM",
      "tags.PRIMARYCONTACT",
      "tags.SECONDARYCONTACT",
      "tags.AccountName"]
  }

  tags = {
    Client = "Economical"
    Contract = "MANAGED_SERVICES"
    PatchGroup = "Windows-AzB-Group1"
    Owner = "robhatnagar@deloitte.ca"
    ManagedBy = "CMS-TF"
    SolutionName = "InsurCloud Economical"
    Environment = "Prod"
    ApplicationName = "rpa, blueprism"
    DataClassification = "PII, PHI"
    DLM-Backup = "Yes"
    DLM-Retention = "90"
    VendorManaged = "False"
    ResourceType = "Client"
    ResourceSeverity = "Normal"
    Name = "A-MC-CAC-D-A003-ECONOMICAL-PROD01-RPA-CLIENT36-WIN-EC2"
    AccountID = "${data.aws_caller_identity.current.account_id}"
  }
}

output "ICECONOMICAL_PROD01_RPA_CLIENT36_PRIVATEIP" {
  value = "${aws_instance.ICECONOMICAL_PROD01_RPA_CLIENT36.private_ip}"
}

data "template_file" "ECO_PROD01_DOMAIN_JOIN_RPA_C37" {
  template = "${file("IC-Economical-PROD/windowsdata.tpl")}"

  vars = {
    computer_name = "PROD01-RPA-C37"
    region = "${var.region}"
    domain = "${var.ad_domain}"
    dns1 = "${var.ad_dns1}"
    dns2 = "${var.ad_dns2}"
    dns3 = "${var.ad_dns3}"
    ouselection = "${var.ou_name}"
    pwordparameter = "${var.pwordparameter}"
    unameparameter = "${var.unameparameter}"
  }
}

resource "aws_instance" "ICECONOMICAL_PROD01_RPA_CLIENT37" {
  ami = "${var.ami_id_windows2016base_patch2020dec}"
  vpc_security_group_ids = [
    "${aws_security_group.SG_AVIATRIX_RDP.id}",
    "${aws_security_group.SG_RPA.id}",
    "${aws_security_group.SG_RPA_CLIENT.id}"
  ]
  subnet_id = "${var.economical-prod-private-1a-subnet}"
  instance_type = "t3a.medium"
  key_name = "EconomicalRPAPRD"
  associate_public_ip_address = false
  iam_instance_profile = "CMS-SSMInstanceRole"
  ebs_optimized = "true"

  root_block_device {
    volume_size = "50"
  }

  user_data = "${data.template_file.ECO_PROD01_DOMAIN_JOIN_RPA_C37.rendered}"

  lifecycle {
    prevent_destroy = true
    ignore_changes = [
      "tags.%",
      "tags.BILLINGCODE",
      "tags.BILLINGCONTACT",
      "tags.CLIENT",
      "tags.COUNTRY",
      "tags.CSCLASS",
      "tags.CSQUAL",
      "tags.CSTYPE",
      "tags.ENVIRONMENT",
      "tags.FUNCTION",
      "tags.GROUPCONTACT",
      "tags.MEMBERFIRM",
      "tags.PRIMARYCONTACT",
      "tags.SECONDARYCONTACT",
      "tags.AccountName"]
  }

  tags = {
    Client = "Economical"
    Contract = "MANAGED_SERVICES"
    PatchGroup = "Windows-AzA-Group1"
    Owner = "robhatnagar@deloitte.ca"
    ManagedBy = "CMS-TF"
    SolutionName = "InsurCloud Economical"
    Environment = "Prod"
    ApplicationName = "rpa, blueprism"
    DataClassification = "PII, PHI"
    DLM-Backup = "Yes"
    DLM-Retention = "90"
    VendorManaged = "False"
    ResourceType = "Client"
    ResourceSeverity = "Normal"
    Name = "A-MC-CAC-D-A003-ECONOMICAL-PROD01-RPA-CLIENT37-WIN-EC2"
    AccountID = "${data.aws_caller_identity.current.account_id}"
  }
}

output "ICECONOMICAL_PROD01_RPA_CLIENT37_PRIVATEIP" {
  value = "${aws_instance.ICECONOMICAL_PROD01_RPA_CLIENT37.private_ip}"
}

data "template_file" "ECO_PROD01_DOMAIN_JOIN_RPA_C38" {
  template = "${file("IC-Economical-PROD/windowsdata.tpl")}"

  vars = {
    computer_name = "PROD01-RPA-C38"
    region = "${var.region}"
    domain = "${var.ad_domain}"
    dns1 = "${var.ad_dns1}"
    dns2 = "${var.ad_dns2}"
    dns3 = "${var.ad_dns3}"
    ouselection = "${var.ou_name}"
    pwordparameter = "${var.pwordparameter}"
    unameparameter = "${var.unameparameter}"
  }
}

resource "aws_instance" "ICECONOMICAL_PROD01_RPA_CLIENT38" {
  ami = "${var.ami_id_windows2016base_patch2020dec}"
  vpc_security_group_ids = [
    "${aws_security_group.SG_AVIATRIX_RDP.id}",
    "${aws_security_group.SG_RPA.id}",
    "${aws_security_group.SG_RPA_CLIENT.id}"
  ]
  subnet_id = "${var.economical-prod-private-1b-subnet}"
  instance_type = "t3a.medium"
  key_name = "EconomicalRPAPRD"
  associate_public_ip_address = false
  iam_instance_profile = "CMS-SSMInstanceRole"
  ebs_optimized = "true"

  root_block_device {
    volume_size = "50"
  }

  user_data = "${data.template_file.ECO_PROD01_DOMAIN_JOIN_RPA_C38.rendered}"

  lifecycle {
    prevent_destroy = true
    ignore_changes = [
      "tags.%",
      "tags.BILLINGCODE",
      "tags.BILLINGCONTACT",
      "tags.CLIENT",
      "tags.COUNTRY",
      "tags.CSCLASS",
      "tags.CSQUAL",
      "tags.CSTYPE",
      "tags.ENVIRONMENT",
      "tags.FUNCTION",
      "tags.GROUPCONTACT",
      "tags.MEMBERFIRM",
      "tags.PRIMARYCONTACT",
      "tags.SECONDARYCONTACT",
      "tags.AccountName"]
  }

  tags = {
    Client = "Economical"
    Contract = "MANAGED_SERVICES"
    PatchGroup = "Windows-AzB-Group1"
    Owner = "robhatnagar@deloitte.ca"
    ManagedBy = "CMS-TF"
    SolutionName = "InsurCloud Economical"
    Environment = "Prod"
    ApplicationName = "rpa, blueprism"
    DataClassification = "PII, PHI"
    DLM-Backup = "Yes"
    DLM-Retention = "90"
    VendorManaged = "False"
    ResourceType = "Client"
    ResourceSeverity = "Normal"
    Name = "A-MC-CAC-D-A003-ECONOMICAL-PROD01-RPA-CLIENT38-WIN-EC2"
    AccountID = "${data.aws_caller_identity.current.account_id}"
  }
}

output "ICECONOMICAL_PROD01_RPA_CLIENT38_PRIVATEIP" {
  value = "${aws_instance.ICECONOMICAL_PROD01_RPA_CLIENT38.private_ip}"
}

data "template_file" "ECO_PROD01_DOMAIN_JOIN_RPA_C39" {
  template = "${file("IC-Economical-PROD/windowsdata.tpl")}"

  vars = {
    computer_name = "PROD01-RPA-C39"
    region = "${var.region}"
    domain = "${var.ad_domain}"
    dns1 = "${var.ad_dns1}"
    dns2 = "${var.ad_dns2}"
    dns3 = "${var.ad_dns3}"
    ouselection = "${var.ou_name}"
    pwordparameter = "${var.pwordparameter}"
    unameparameter = "${var.unameparameter}"
  }
}

resource "aws_instance" "ICECONOMICAL_PROD01_RPA_CLIENT39" {
  ami = "${var.ami_id_windows2016base_patch2020dec}"
  vpc_security_group_ids = [
    "${aws_security_group.SG_AVIATRIX_RDP.id}",
    "${aws_security_group.SG_RPA.id}",
    "${aws_security_group.SG_RPA_CLIENT.id}"
  ]
  subnet_id = "${var.economical-prod-private-1a-subnet}"
  instance_type = "t3a.medium"
  key_name = "EconomicalRPAPRD"
  associate_public_ip_address = false
  iam_instance_profile = "CMS-SSMInstanceRole"
  ebs_optimized = "true"

  root_block_device {
    volume_size = "50"
  }

  user_data = "${data.template_file.ECO_PROD01_DOMAIN_JOIN_RPA_C39.rendered}"

  lifecycle {
    prevent_destroy = true
    ignore_changes = [
      "tags.%",
      "tags.BILLINGCODE",
      "tags.BILLINGCONTACT",
      "tags.CLIENT",
      "tags.COUNTRY",
      "tags.CSCLASS",
      "tags.CSQUAL",
      "tags.CSTYPE",
      "tags.ENVIRONMENT",
      "tags.FUNCTION",
      "tags.GROUPCONTACT",
      "tags.MEMBERFIRM",
      "tags.PRIMARYCONTACT",
      "tags.SECONDARYCONTACT",
      "tags.AccountName"]
  }

  tags = {
    Client = "Economical"
    Contract = "MANAGED_SERVICES"
    PatchGroup = "Windows-AzA-Group1"
    Owner = "robhatnagar@deloitte.ca"
    ManagedBy = "CMS-TF"
    SolutionName = "InsurCloud Economical"
    Environment = "Prod"
    ApplicationName = "rpa, blueprism"
    DataClassification = "PII, PHI"
    DLM-Backup = "Yes"
    DLM-Retention = "90"
    VendorManaged = "False"
    ResourceType = "Client"
    ResourceSeverity = "Normal"
    Name = "A-MC-CAC-D-A003-ECONOMICAL-PROD01-RPA-CLIENT39-WIN-EC2"
    AccountID = "${data.aws_caller_identity.current.account_id}"
  }
}

output "ICECONOMICAL_PROD01_RPA_CLIENT39_PRIVATEIP" {
  value = "${aws_instance.ICECONOMICAL_PROD01_RPA_CLIENT39.private_ip}"
}

data "template_file" "ECO_PROD01_DOMAIN_JOIN_RPA_C40" {
  template = "${file("IC-Economical-PROD/windowsdata.tpl")}"

  vars = {
    computer_name = "PROD01-RPA-C40"
    region = "${var.region}"
    domain = "${var.ad_domain}"
    dns1 = "${var.ad_dns1}"
    dns2 = "${var.ad_dns2}"
    dns3 = "${var.ad_dns3}"
    ouselection = "${var.ou_name}"
    pwordparameter = "${var.pwordparameter}"
    unameparameter = "${var.unameparameter}"
  }
}

resource "aws_instance" "ICECONOMICAL_PROD01_RPA_CLIENT40" {
  ami = "${var.ami_id_windows2016base_patch2020dec}"
  vpc_security_group_ids = [
    "${aws_security_group.SG_AVIATRIX_RDP.id}",
    "${aws_security_group.SG_RPA.id}",
    "${aws_security_group.SG_RPA_CLIENT.id}"
  ]
  subnet_id = "${var.economical-prod-private-1b-subnet}"
  instance_type = "t3a.medium"
  key_name = "EconomicalRPAPRD"
  associate_public_ip_address = false
  iam_instance_profile = "CMS-SSMInstanceRole"
  ebs_optimized = "true"

  root_block_device {
    volume_size = "50"
  }

  user_data = "${data.template_file.ECO_PROD01_DOMAIN_JOIN_RPA_C40.rendered}"

  lifecycle {
    prevent_destroy = true
    ignore_changes = [
      "tags.%",
      "tags.BILLINGCODE",
      "tags.BILLINGCONTACT",
      "tags.CLIENT",
      "tags.COUNTRY",
      "tags.CSCLASS",
      "tags.CSQUAL",
      "tags.CSTYPE",
      "tags.ENVIRONMENT",
      "tags.FUNCTION",
      "tags.GROUPCONTACT",
      "tags.MEMBERFIRM",
      "tags.PRIMARYCONTACT",
      "tags.SECONDARYCONTACT",
      "tags.AccountName"]
  }

  tags = {
    Client = "Economical"
    Contract = "MANAGED_SERVICES"
    PatchGroup = "Windows-AzB-Group1"
    Owner = "robhatnagar@deloitte.ca"
    ManagedBy = "CMS-TF"
    SolutionName = "InsurCloud Economical"
    Environment = "Prod"
    ApplicationName = "rpa, blueprism"
    DataClassification = "PII, PHI"
    DLM-Backup = "Yes"
    DLM-Retention = "90"
    VendorManaged = "False"
    ResourceType = "Client"
    ResourceSeverity = "Normal"
    Name = "A-MC-CAC-D-A003-ECONOMICAL-PROD01-RPA-CLIENT40-WIN-EC2"
    AccountID = "${data.aws_caller_identity.current.account_id}"
  }
}

output "ICECONOMICAL_PROD01_RPA_CLIENT40_PRIVATEIP" {
  value = "${aws_instance.ICECONOMICAL_PROD01_RPA_CLIENT40.private_ip}"
}

data "template_file" "ECO_PROD01_DOMAIN_JOIN_RPA_C41" {
  template = "${file("IC-Economical-PROD/windowsdata.tpl")}"

  vars = {
    computer_name = "PROD01-RPA-C41"
    region = "${var.region}"
    domain = "${var.ad_domain}"
    dns1 = "${var.ad_dns1}"
    dns2 = "${var.ad_dns2}"
    dns3 = "${var.ad_dns3}"
    ouselection = "${var.ou_name}"
    pwordparameter = "${var.pwordparameter}"
    unameparameter = "${var.unameparameter}"
  }
}

resource "aws_instance" "ICECONOMICAL_PROD01_RPA_CLIENT41" {
  ami = "${var.ami_id_windows2016base_patch2020dec}"
  vpc_security_group_ids = [
    "${aws_security_group.SG_AVIATRIX_RDP.id}",
    "${aws_security_group.SG_RPA.id}",
    "${aws_security_group.SG_RPA_CLIENT.id}"
  ]
  subnet_id = "${var.economical-prod-private-1a-subnet}"
  instance_type = "t3a.medium"
  key_name = "EconomicalRPAPRD"
  associate_public_ip_address = false
  iam_instance_profile = "CMS-SSMInstanceRole"
  ebs_optimized = "true"

  root_block_device {
    volume_size = "50"
  }

  user_data = "${data.template_file.ECO_PROD01_DOMAIN_JOIN_RPA_C41.rendered}"

  lifecycle {
    prevent_destroy = true
    ignore_changes = [
      "tags.%",
      "tags.BILLINGCODE",
      "tags.BILLINGCONTACT",
      "tags.CLIENT",
      "tags.COUNTRY",
      "tags.CSCLASS",
      "tags.CSQUAL",
      "tags.CSTYPE",
      "tags.ENVIRONMENT",
      "tags.FUNCTION",
      "tags.GROUPCONTACT",
      "tags.MEMBERFIRM",
      "tags.PRIMARYCONTACT",
      "tags.SECONDARYCONTACT",
      "tags.AccountName"]
  }

  tags = {
    Client = "Economical"
    Contract = "MANAGED_SERVICES"
    PatchGroup = "Windows-AzA-Group1"
    Owner = "robhatnagar@deloitte.ca"
    ManagedBy = "CMS-TF"
    SolutionName = "InsurCloud Economical"
    Environment = "Prod"
    ApplicationName = "rpa, blueprism"
    DataClassification = "PII, PHI"
    DLM-Backup = "Yes"
    DLM-Retention = "90"
    VendorManaged = "False"
    ResourceType = "Client"
    ResourceSeverity = "Normal"
    Name = "A-MC-CAC-D-A003-ECONOMICAL-PROD01-RPA-CLIENT41-WIN-EC2"
    AccountID = "${data.aws_caller_identity.current.account_id}"
  }
}

output "ICECONOMICAL_PROD01_RPA_CLIENT41_PRIVATEIP" {
  value = "${aws_instance.ICECONOMICAL_PROD01_RPA_CLIENT41.private_ip}"
}

data "template_file" "ECO_PROD01_DOMAIN_JOIN_RPA_C42" {
  template = "${file("IC-Economical-PROD/windowsdata.tpl")}"

  vars = {
    computer_name = "PROD01-RPA-C42"
    region = "${var.region}"
    domain = "${var.ad_domain}"
    dns1 = "${var.ad_dns1}"
    dns2 = "${var.ad_dns2}"
    dns3 = "${var.ad_dns3}"
    ouselection = "${var.ou_name}"
    pwordparameter = "${var.pwordparameter}"
    unameparameter = "${var.unameparameter}"
  }
}

resource "aws_instance" "ICECONOMICAL_PROD01_RPA_CLIENT42" {
  ami = "${var.ami_id_windows2016base_patch2020dec}"
  vpc_security_group_ids = [
    "${aws_security_group.SG_AVIATRIX_RDP.id}",
    "${aws_security_group.SG_RPA.id}",
    "${aws_security_group.SG_RPA_CLIENT.id}"
  ]
  subnet_id = "${var.economical-prod-private-1b-subnet}"
  instance_type = "t3a.medium"
  key_name = "EconomicalRPAPRD"
  associate_public_ip_address = false
  iam_instance_profile = "CMS-SSMInstanceRole"
  ebs_optimized = "true"

  root_block_device {
    volume_size = "50"
  }

  user_data = "${data.template_file.ECO_PROD01_DOMAIN_JOIN_RPA_C42.rendered}"

  lifecycle {
    prevent_destroy = true
    ignore_changes = [
      "tags.%",
      "tags.BILLINGCODE",
      "tags.BILLINGCONTACT",
      "tags.CLIENT",
      "tags.COUNTRY",
      "tags.CSCLASS",
      "tags.CSQUAL",
      "tags.CSTYPE",
      "tags.ENVIRONMENT",
      "tags.FUNCTION",
      "tags.GROUPCONTACT",
      "tags.MEMBERFIRM",
      "tags.PRIMARYCONTACT",
      "tags.SECONDARYCONTACT",
      "tags.AccountName"]
  }

  tags = {
    Client = "Economical"
    Contract = "MANAGED_SERVICES"
    PatchGroup = "Windows-AzB-Group1"
    Owner = "robhatnagar@deloitte.ca"
    ManagedBy = "CMS-TF"
    SolutionName = "InsurCloud Economical"
    Environment = "Prod"
    ApplicationName = "rpa, blueprism"
    DataClassification = "PII, PHI"
    DLM-Backup = "Yes"
    DLM-Retention = "90"
    VendorManaged = "False"
    ResourceType = "Client"
    ResourceSeverity = "Normal"
    Name = "A-MC-CAC-D-A003-ECONOMICAL-PROD01-RPA-CLIENT42-WIN-EC2"
    AccountID = "${data.aws_caller_identity.current.account_id}"
  }
}

output "ICECONOMICAL_PROD01_RPA_CLIENT42_PRIVATEIP" {
  value = "${aws_instance.ICECONOMICAL_PROD01_RPA_CLIENT42.private_ip}"
}

data "template_file" "ECO_PROD01_DOMAIN_JOIN_RPA_C43" {
  template = "${file("IC-Economical-PROD/windowsdata.tpl")}"

  vars = {
    computer_name = "PROD01-RPA-C43"
    region = "${var.region}"
    domain = "${var.ad_domain}"
    dns1 = "${var.ad_dns1}"
    dns2 = "${var.ad_dns2}"
    dns3 = "${var.ad_dns3}"
    ouselection = "${var.ou_name}"
    pwordparameter = "${var.pwordparameter}"
    unameparameter = "${var.unameparameter}"
  }
}

resource "aws_instance" "ICECONOMICAL_PROD01_RPA_CLIENT43" {
  ami = "${var.ami_id_windows2016base_patch2020dec}"
  vpc_security_group_ids = [
    "${aws_security_group.SG_AVIATRIX_RDP.id}",
    "${aws_security_group.SG_RPA.id}",
    "${aws_security_group.SG_RPA_CLIENT.id}"
  ]
  subnet_id = "${var.economical-prod-private-1a-subnet}"
  instance_type = "t3a.medium"
  key_name = "EconomicalRPAPRD"
  associate_public_ip_address = false
  iam_instance_profile = "CMS-SSMInstanceRole"
  ebs_optimized = "true"

  root_block_device {
    volume_size = "50"
  }

  user_data = "${data.template_file.ECO_PROD01_DOMAIN_JOIN_RPA_C43.rendered}"

  lifecycle {
    prevent_destroy = true
    ignore_changes = [
      "tags.%",
      "tags.BILLINGCODE",
      "tags.BILLINGCONTACT",
      "tags.CLIENT",
      "tags.COUNTRY",
      "tags.CSCLASS",
      "tags.CSQUAL",
      "tags.CSTYPE",
      "tags.ENVIRONMENT",
      "tags.FUNCTION",
      "tags.GROUPCONTACT",
      "tags.MEMBERFIRM",
      "tags.PRIMARYCONTACT",
      "tags.SECONDARYCONTACT",
      "tags.AccountName"]
  }

  tags = {
    Client = "Economical"
    Contract = "MANAGED_SERVICES"
    PatchGroup = "Windows-AzA-Group1"
    Owner = "robhatnagar@deloitte.ca"
    ManagedBy = "CMS-TF"
    SolutionName = "InsurCloud Economical"
    Environment = "Prod"
    ApplicationName = "rpa, blueprism"
    DataClassification = "PII, PHI"
    DLM-Backup = "Yes"
    DLM-Retention = "90"
    VendorManaged = "False"
    ResourceType = "Client"
    ResourceSeverity = "Normal"
    Name = "A-MC-CAC-D-A003-ECONOMICAL-PROD01-RPA-CLIENT43-WIN-EC2"
    AccountID = "${data.aws_caller_identity.current.account_id}"
  }
}

output "ICECONOMICAL_PROD01_RPA_CLIENT43_PRIVATEIP" {
  value = "${aws_instance.ICECONOMICAL_PROD01_RPA_CLIENT43.private_ip}"
}

data "template_file" "ECO_PROD01_DOMAIN_JOIN_RPA_C44" {
  template = "${file("IC-Economical-PROD/windowsdata.tpl")}"

  vars = {
    computer_name = "PROD01-RPA-C44"
    region = "${var.region}"
    domain = "${var.ad_domain}"
    dns1 = "${var.ad_dns1}"
    dns2 = "${var.ad_dns2}"
    dns3 = "${var.ad_dns3}"
    ouselection = "${var.ou_name}"
    pwordparameter = "${var.pwordparameter}"
    unameparameter = "${var.unameparameter}"
  }
}

resource "aws_instance" "ICECONOMICAL_PROD01_RPA_CLIENT44" {
  ami = "${var.ami_id_windows2016base_patch2020dec}"
  vpc_security_group_ids = [
    "${aws_security_group.SG_AVIATRIX_RDP.id}",
    "${aws_security_group.SG_RPA.id}",
    "${aws_security_group.SG_RPA_CLIENT.id}"
  ]
  subnet_id = "${var.economical-prod-private-1b-subnet}"
  instance_type = "t3a.medium"
  key_name = "EconomicalRPAPRD"
  associate_public_ip_address = false
  iam_instance_profile = "CMS-SSMInstanceRole"
  ebs_optimized = "true"

  root_block_device {
    volume_size = "50"
  }

  user_data = "${data.template_file.ECO_PROD01_DOMAIN_JOIN_RPA_C44.rendered}"

  lifecycle {
    prevent_destroy = true
    ignore_changes = [
      "tags.%",
      "tags.BILLINGCODE",
      "tags.BILLINGCONTACT",
      "tags.CLIENT",
      "tags.COUNTRY",
      "tags.CSCLASS",
      "tags.CSQUAL",
      "tags.CSTYPE",
      "tags.ENVIRONMENT",
      "tags.FUNCTION",
      "tags.GROUPCONTACT",
      "tags.MEMBERFIRM",
      "tags.PRIMARYCONTACT",
      "tags.SECONDARYCONTACT",
      "tags.AccountName"]
  }

  tags = {
    Client = "Economical"
    Contract = "MANAGED_SERVICES"
    PatchGroup = "Windows-AzB-Group1"
    Owner = "robhatnagar@deloitte.ca"
    ManagedBy = "CMS-TF"
    SolutionName = "InsurCloud Economical"
    Environment = "Prod"
    ApplicationName = "rpa, blueprism"
    DataClassification = "PII, PHI"
    DLM-Backup = "Yes"
    DLM-Retention = "90"
    VendorManaged = "False"
    ResourceType = "Client"
    ResourceSeverity = "Normal"
    Name = "A-MC-CAC-D-A003-ECONOMICAL-PROD01-RPA-CLIENT44-WIN-EC2"
    AccountID = "${data.aws_caller_identity.current.account_id}"
  }
}

output "ICECONOMICAL_PROD01_RPA_CLIENT44_PRIVATEIP" {
  value = "${aws_instance.ICECONOMICAL_PROD01_RPA_CLIENT44.private_ip}"
}

data "template_file" "ECO_PROD01_DOMAIN_JOIN_RPA_C45" {
  template = "${file("IC-Economical-PROD/windowsdata.tpl")}"

  vars = {
    computer_name = "PROD01-RPA-C45"
    region = "${var.region}"
    domain = "${var.ad_domain}"
    dns1 = "${var.ad_dns1}"
    dns2 = "${var.ad_dns2}"
    dns3 = "${var.ad_dns3}"
    ouselection = "${var.ou_name}"
    pwordparameter = "${var.pwordparameter}"
    unameparameter = "${var.unameparameter}"
  }
}

resource "aws_instance" "ICECONOMICAL_PROD01_RPA_CLIENT45" {
  ami = "${var.ami_id_windows2016base_patch2020dec}"
  vpc_security_group_ids = [
    "${aws_security_group.SG_AVIATRIX_RDP.id}",
    "${aws_security_group.SG_RPA.id}",
    "${aws_security_group.SG_RPA_CLIENT.id}"
  ]
  subnet_id = "${var.economical-prod-private-1a-subnet}"
  instance_type = "t3a.medium"
  key_name = "EconomicalRPAPRD"
  associate_public_ip_address = false
  iam_instance_profile = "CMS-SSMInstanceRole"
  ebs_optimized = "true"

  root_block_device {
    volume_size = "50"
  }

  user_data = "${data.template_file.ECO_PROD01_DOMAIN_JOIN_RPA_C45.rendered}"

  lifecycle {
    prevent_destroy = true
    ignore_changes = [
      "tags.%",
      "tags.BILLINGCODE",
      "tags.BILLINGCONTACT",
      "tags.CLIENT",
      "tags.COUNTRY",
      "tags.CSCLASS",
      "tags.CSQUAL",
      "tags.CSTYPE",
      "tags.ENVIRONMENT",
      "tags.FUNCTION",
      "tags.GROUPCONTACT",
      "tags.MEMBERFIRM",
      "tags.PRIMARYCONTACT",
      "tags.SECONDARYCONTACT",
      "tags.AccountName"]
  }

  tags = {
    Client = "Economical"
    Contract = "MANAGED_SERVICES"
    PatchGroup = "Windows-AzA-Group1"
    Owner = "robhatnagar@deloitte.ca"
    ManagedBy = "CMS-TF"
    SolutionName = "InsurCloud Economical"
    Environment = "Prod"
    ApplicationName = "rpa, blueprism"
    DataClassification = "PII, PHI"
    DLM-Backup = "Yes"
    DLM-Retention = "90"
    VendorManaged = "False"
    ResourceType = "Client"
    ResourceSeverity = "Normal"
    Name = "A-MC-CAC-D-A003-ECONOMICAL-PROD01-RPA-CLIENT45-WIN-EC2"
    AccountID = "${data.aws_caller_identity.current.account_id}"
  }
}

output "ICECONOMICAL_PROD01_RPA_CLIENT45_PRIVATEIP" {
  value = "${aws_instance.ICECONOMICAL_PROD01_RPA_CLIENT45.private_ip}"
}

data "template_file" "ECO_PROD01_DOMAIN_JOIN_RPA_C46" {
  template = "${file("IC-Economical-PROD/windowsdata.tpl")}"

  vars = {
    computer_name = "PROD01-RPA-C46"
    region = "${var.region}"
    domain = "${var.ad_domain}"
    dns1 = "${var.ad_dns1}"
    dns2 = "${var.ad_dns2}"
    dns3 = "${var.ad_dns3}"
    ouselection = "${var.ou_name}"
    pwordparameter = "${var.pwordparameter}"
    unameparameter = "${var.unameparameter}"
  }
}

resource "aws_instance" "ICECONOMICAL_PROD01_RPA_CLIENT46" {
  ami = "${var.ami_id_windows2016base_patch2020dec}"
  vpc_security_group_ids = [
    "${aws_security_group.SG_AVIATRIX_RDP.id}",
    "${aws_security_group.SG_RPA.id}",
    "${aws_security_group.SG_RPA_CLIENT.id}"
  ]
  subnet_id = "${var.economical-prod-private-1b-subnet}"
  instance_type = "t3a.medium"
  key_name = "EconomicalRPAPRD"
  associate_public_ip_address = false
  iam_instance_profile = "CMS-SSMInstanceRole"
  ebs_optimized = "true"

  root_block_device {
    volume_size = "50"
  }

  user_data = "${data.template_file.ECO_PROD01_DOMAIN_JOIN_RPA_C46.rendered}"

  lifecycle {
    prevent_destroy = true
    ignore_changes = [
      "tags.%",
      "tags.BILLINGCODE",
      "tags.BILLINGCONTACT",
      "tags.CLIENT",
      "tags.COUNTRY",
      "tags.CSCLASS",
      "tags.CSQUAL",
      "tags.CSTYPE",
      "tags.ENVIRONMENT",
      "tags.FUNCTION",
      "tags.GROUPCONTACT",
      "tags.MEMBERFIRM",
      "tags.PRIMARYCONTACT",
      "tags.SECONDARYCONTACT",
      "tags.AccountName"]
  }

  tags = {
    Client = "Economical"
    Contract = "MANAGED_SERVICES"
    PatchGroup = "Windows-AzB-Group1"
    Owner = "robhatnagar@deloitte.ca"
    ManagedBy = "CMS-TF"
    SolutionName = "InsurCloud Economical"
    Environment = "Prod"
    ApplicationName = "rpa, blueprism"
    DataClassification = "PII, PHI"
    DLM-Backup = "Yes"
    DLM-Retention = "90"
    VendorManaged = "False"
    ResourceType = "Client"
    ResourceSeverity = "Normal"
    Name = "A-MC-CAC-D-A003-ECONOMICAL-PROD01-RPA-CLIENT46-WIN-EC2"
    AccountID = "${data.aws_caller_identity.current.account_id}"
  }
}

output "ICECONOMICAL_PROD01_RPA_CLIENT46_PRIVATEIP" {
  value = "${aws_instance.ICECONOMICAL_PROD01_RPA_CLIENT46.private_ip}"
}

######### HAPROXY ##############
resource "aws_instance" "ICECONOMICAL_PROD01_INTERNAL_HAPROXY01" {
  ami = "${var.ami_id_amazonlinuxbase20200506}"
  vpc_security_group_ids = [
    "${aws_security_group.SG_INTERNAL_HAPROXY.id}",
    "${aws_security_group.SG_AVIATRIX_SSH.id}",
    "${aws_security_group.SG_JENKINS_CLIENT_SSH.id}"
  ]
  subnet_id = "${var.economical-prod-private-1a-subnet}"
  instance_type = "t3a.small"
  key_name = "EconomicalHAProxyPRD"
  associate_public_ip_address = false
  iam_instance_profile = "CMS-SSMInstanceRole"
  ebs_optimized = "true"

  root_block_device {
    volume_size = "40"
  }

  lifecycle {
    prevent_destroy = true
    ignore_changes = [
      "tags.%",
      "tags.BILLINGCODE",
      "tags.BILLINGCONTACT",
      "tags.CLIENT",
      "tags.COUNTRY",
      "tags.CSCLASS",
      "tags.CSQUAL",
      "tags.CSTYPE",
      "tags.ENVIRONMENT",
      "tags.FUNCTION",
      "tags.GROUPCONTACT",
      "tags.MEMBERFIRM",
      "tags.PRIMARYCONTACT",
      "tags.SECONDARYCONTACT",
      "tags.AccountName"]
  }

  tags = {
    Client = "Economical"
    Contract = "MANAGED_SERVICES"
    PatchGroup = "AmazonLinux2-AzA-Group1"
    Owner = "robhatnagar@deloitte.ca"
    ManagedBy = "CMS-TF"
    SolutionName = "InsurCloud Economical"
    Environment = "Prod"
    ApplicationName = "docker, haproxy"
    DataClassification = "PII, PHI"
    DLM-Backup = "Yes"
    DLM-Retention = "90"
    VendorManaged = "False"
    ResourceType = "Client"
    ResourceSeverity = "Normal"
    Name = "A-MC-CAC-D-A003-ECONOMICAL-PROD01-INTERNAL-HAPROXY01-EC2"
    AccountID = "${data.aws_caller_identity.current.account_id}"
    Role = "Container"
  }

  provisioner "local-exec" {
    command = "echo ${aws_instance.ICECONOMICAL_PROD01_INTERNAL_HAPROXY01.private_ip} >> dev_hosts.txt"
  }
}

output "ICECONOMICAL_PROD01_INTERNAL_HAPROXY_PRIVATEIP01" {
  value = "${aws_instance.ICECONOMICAL_PROD01_INTERNAL_HAPROXY01.private_ip}"
}

resource "aws_instance" "ICECONOMICAL_PROD01_INTERNAL_HAPROXY02" {
  ami = "${var.ami_id_amazonlinuxbase20200506}"
  vpc_security_group_ids = [
    "${aws_security_group.SG_INTERNAL_HAPROXY.id}",
    "${aws_security_group.SG_AVIATRIX_SSH.id}",
    "${aws_security_group.SG_JENKINS_CLIENT_SSH.id}"
  ]
  subnet_id = "${var.economical-prod-private-1b-subnet}"
  instance_type = "t3a.small"
  key_name = "EconomicalHAProxyPRD"
  associate_public_ip_address = false
  iam_instance_profile = "CMS-SSMInstanceRole"
  ebs_optimized = "true"

  root_block_device {
    volume_size = "40"
  }

  lifecycle {
    prevent_destroy = true
    ignore_changes = [
      "tags.%",
      "tags.BILLINGCODE",
      "tags.BILLINGCONTACT",
      "tags.CLIENT",
      "tags.COUNTRY",
      "tags.CSCLASS",
      "tags.CSQUAL",
      "tags.CSTYPE",
      "tags.ENVIRONMENT",
      "tags.FUNCTION",
      "tags.GROUPCONTACT",
      "tags.MEMBERFIRM",
      "tags.PRIMARYCONTACT",
      "tags.SECONDARYCONTACT",
      "tags.AccountName"]
  }

  tags = {
    Client = "Economical"
    Contract = "MANAGED_SERVICES"
    PatchGroup = "AmazonLinux2-AzB-Group1"
    Owner = "robhatnagar@deloitte.ca"
    ManagedBy = "CMS-TF"
    SolutionName = "InsurCloud Economical"
    Environment = "Prod"
    ApplicationName = "docker, haproxy"
    DataClassification = "PII, PHI"
    DLM-Backup = "Yes"
    DLM-Retention = "90"
    VendorManaged = "False"
    ResourceType = "Client"
    ResourceSeverity = "Normal"
    Name = "A-MC-CAC-D-A003-ECONOMICAL-PROD01-INTERNAL-HAPROXY02-EC2"
    AccountID = "${data.aws_caller_identity.current.account_id}"
    Role = "Container"
  }

  provisioner "local-exec" {
    command = "echo ${aws_instance.ICECONOMICAL_PROD01_INTERNAL_HAPROXY02.private_ip} >> dev_hosts.txt"
  }
}

output "ICECONOMICAL_PROD01_INTERNAL_HAPROXY_PRIVATEIP02" {
  value = "${aws_instance.ICECONOMICAL_PROD01_INTERNAL_HAPROXY02.private_ip}"
}


# #=======================================================================
# # PROD
# #=======================================================================
############# JENKINS CLIENT #################
resource "aws_instance" "ICECONOMICAL_PROD_JENKINS_CLIENT" {
  ami = "${var.ami_id_amazonlinuxbase20200506}"
  vpc_security_group_ids = [
    "${aws_security_group.SG_JENKINS_CLIENT.id}"]
  subnet_id = "${var.economical-prod-private-1a-subnet}"
  instance_type = "t3a.small"
  key_name = "EconomicalJenkinsPRD"
  associate_public_ip_address = false
  iam_instance_profile = "CMS-SSMInstanceRole"
  ebs_optimized = "true"

  root_block_device {
    volume_size = "250"
  }

  lifecycle {
    prevent_destroy = true
    ignore_changes = [
      "tags.%",
      "tags.BILLINGCODE",
      "tags.BILLINGCONTACT",
      "tags.CLIENT",
      "tags.COUNTRY",
      "tags.CSCLASS",
      "tags.CSQUAL",
      "tags.CSTYPE",
      "tags.ENVIRONMENT",
      "tags.FUNCTION",
      "tags.GROUPCONTACT",
      "tags.MEMBERFIRM",
      "tags.PRIMARYCONTACT",
      "tags.SECONDARYCONTACT",
      "tags.AccountName"]
  }

  tags = {
    Client = "Economical"
    Contract = "MANAGED_SERVICES"
    PatchGroup = "AmazonLinux2-AzA-Group1"
    Owner = "robhatnagar@deloitte.ca"
    ManagedBy = "CMS-TF"
    SolutionName = "InsurCloud Economical"
    Environment = "Prod"
    ApplicationName = "jenkins, docker"
    DataClassification = "NA"
    DLM-Backup = "Yes"
    DLM-Retention = "90"
    VendorManaged = "False"
    ResourceType = "Client"
    ResourceSeverity = "Normal"
    Name = "A-MC-CAC-D-A003-ECONOMICAL-PROD-JENKINS-CLIENT"
    AccountID = "${data.aws_caller_identity.current.account_id}"
    Role = "Container"
  }
}

output "ICECONOMICAL_PROD_JENKINS_CLIENT_PRIVATEIP" {
  value = "${aws_instance.ICECONOMICAL_PROD_JENKINS_CLIENT.private_ip}"
}

######################################
