#Envrionment specific variables
client_name = "IC-Economical"
enviornment_name = "PROD"
enviornment_type = "P"
client_number = "A004"
#ad-dc1 = "10.10.4.10"
#ad-dc2 = "10.10.6.10"
volume_key = "arn:aws:kms:ca-central-1:852868701745:key/21cbfbdb-fa63-43fd-8f85-440e73a52e56"
ami_id_windows2012R2sql14enterprise = "ami-07ebb1b4727f89cda"
ami_id_windows2012R2sql14standard = "ami-0913a2afb4fa5fc5b"
ami_id_windows2019container = "ami-013453540e1aa0d48"
ami_id_windows2019base = "ami-06b8f69a907cf6bb2"
ami_id_windows2016base = "ami-0bce38ba7ac166aa9"
ami_id_windows2016base_patch2020nov = "ami-0388b8d3cb35753f8"
ami_id_windows2016base_patch2020dec = "ami-02d51b2c3cf987496"
ami_id_windows2012R2base = "ami-09b707ce10f2e9cee"
ami_id_windows2008base = "ami-0446e08d2d99fc8c9"
ami_id_redhatlinux75 = "ami-05c33d286020a47f9"
ami_id_ubuntu1804baseimage = "ami-032172cc5f34b32fc"
ami_id_ubuntu1604baseimage = "ami-04f40cca01b3186ea"
ami_id_amazonlinux201712base = "ami-0a777bbf8ef3f43bf"
ami_id_amazonlinux201709base = "ami-058dba934995c5468"
ami_id_amazonlinux2base = "ami-0ad9abdd25c431def"
ami_id_amazonlinux201803base = "ami-06829694f407e15c1"
ami_id_amazonlinuxbase20200506 = "ami-071594d49d11fcb38"

#VPC Specific variables
vpc_id = "vpc-01c0deab7c0164307"
vpc_cidr = "10.227.4.0/22"


economical-prod-database-1a-subnet = "subnet-0f307fc12aa386287"
economical-prod-database-1b-subnet = "subnet-0d3c439e7aa249ab8"
economical-prod-private-1a-subnet = "subnet-0f8c40277ee3b0bad"
economical-prod-private-1b-subnet = "subnet-0211002fbe970842a"


#RDS
mssql-rds-password = "Toronto12345"
mysql-rds-password = "Toronto12345"
postgres-rds-password = "Toronto12345"

#Accounts
economical-dev-account = "632109065501"
economical-prod-account = "852868701745"
economical-hub-account = "348868695504"
