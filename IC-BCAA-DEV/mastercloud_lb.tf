# #=======================================================================
# # DEV01
# #=======================================================================
##### ALBs #####

##### NLBs ####################
########## Internal ###########
resource "aws_lb" "ICBCAA_DEV01_INTERNAL_NLB" {
    name = "ICBCAA-DEV01-INTERNAL-NLB"
    internal           = true
    load_balancer_type = "network"
    enable_cross_zone_load_balancing = false
    subnets            = [
        "${var.bcaa-dev-private-1a-subnet}",
        "${var.bcaa-dev-private-1b-subnet}"
    ]

    tags = {
        Name = "ICBCAA-DEV01-INTERNAL-NLB"
    }
}

resource "aws_lb_target_group" "ICBCAA_DEV01_INTERNAL_NLB_HAPROXY_TARGET_GROUP" {
    name     = "ICBCAA-DEV01-HAPROXY-INT-NLB"
    port     = 80
    protocol = "TCP"
    vpc_id   = "${var.vpc_id}"

    health_check {
        protocol = "TCP"
        port = 8444
    }

    tags = {
        Name = "ICBCAA-DEV01-HAPROXY-INTERNAL-NLB-TARGET-GROUP"
    }
}

resource "aws_lb_listener" "ICBCAA_DEV01_INTERNAL_NLB_HAPROXY_LISTENER" {
    load_balancer_arn = "${aws_lb.ICBCAA_DEV01_INTERNAL_NLB.arn}"
    port              = "80"
    protocol          = "TCP"

    default_action {
        type             = "forward"
        target_group_arn = "${aws_lb_target_group.ICBCAA_DEV01_INTERNAL_NLB_HAPROXY_TARGET_GROUP.arn}"
    }
}


resource "aws_lb_target_group_attachment" "ICBCAA_DEV01_INTERNAL_NLB_TARGET_GROUP_ATTACHMENT_HAPROXY" {
    target_group_arn = "${aws_lb_target_group.ICBCAA_DEV01_INTERNAL_NLB_HAPROXY_TARGET_GROUP.arn}"
    target_id        = "${aws_instance.ICBCAA_DEV01_INTERNAL_HAPROXY.id}"
    port             = 80
}

output "ICBCAA_DEV01_INTERNAL_NLB_DNS_NAME" {
    value = "${aws_lb.ICBCAA_DEV01_INTERNAL_NLB.dns_name}"
}

output "ICBCAA_DEV01_INTERNAL_NLB_ZONE_ID" {
    value = "${aws_lb.ICBCAA_DEV01_INTERNAL_NLB.zone_id}"
}