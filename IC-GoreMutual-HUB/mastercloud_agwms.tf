#API Gateway

resource "aws_api_gateway_rest_api" "GORE_AUTH0" {
  name        = "GORE_AUTH0"
  description = "This is the API gateway for AUTH0 Enabled Gateway Collection."
  endpoint_configuration {
    types = ["REGIONAL"]
  }
}

######### AUTHORIZER ##############
#Token Auth DEV

resource "aws_api_gateway_authorizer" "NonProd_Auth0_HCAI" {
  name                   = "NonProd_Auth0_HCAI"
  rest_api_id            = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  authorizer_uri         = "${aws_lambda_function.gore_AGWAuthenrizer_HCAI_Hub.invoke_arn}"
  authorizer_credentials = "arn:aws:iam::${var.gore-hub-account}:role/Auth0_invocation_role"
}
resource "aws_api_gateway_authorizer" "NonProd_Auth0_ChequePrinting" {
  name                   = "NonProd_Auth0_ChequePrinting"
  rest_api_id            = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  authorizer_uri         = "${aws_lambda_function.gore_AGWAuthenrizer_Cheque_Hub.invoke_arn}"
  authorizer_credentials = "arn:aws:iam::${var.gore-hub-account}:role/Auth0_invocation_role"
}
resource "aws_api_gateway_authorizer" "NonProd_Auth0_MVR" {
  name                   = "NonProd_Auth0_mvr"
  rest_api_id            = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  authorizer_uri         = "${aws_lambda_function.gore_AGWAuthenrizer_MVR_Hub.invoke_arn}"
  authorizer_credentials = "arn:aws:iam::${var.gore-hub-account}:role/Auth0_invocation_role"
}
resource "aws_api_gateway_authorizer" "NonProd_Auth0_CRC" {
  name                   = "NonProd_Auth0_CRC"
  rest_api_id            = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  authorizer_uri         = "${aws_lambda_function.gore_AGWAuthenrizer_CRC_Hub.invoke_arn}"
  authorizer_credentials = "arn:aws:iam::${var.gore-hub-account}:role/Auth0_invocation_role"
}
resource "aws_api_gateway_authorizer" "NonProd_Auth0_AddressValidation" {
  name                   = "NonProd_Auth0_AddressValidation"
  rest_api_id            = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  authorizer_uri         = "${aws_lambda_function.gore_AGWAuthenrizer_AddressValidation_Hub.invoke_arn}"
  authorizer_credentials = "arn:aws:iam::${var.gore-hub-account}:role/Auth0_invocation_role"
}
resource "aws_api_gateway_authorizer" "NonProd_Auth0_Audatex" {
  name                   = "NonProd_Auth0_Audatex"
  rest_api_id            = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  authorizer_uri         = "${aws_lambda_function.gore_AGWAuthenrizer_Audatex_Hub.invoke_arn}"
  authorizer_credentials = "arn:aws:iam::${var.gore-hub-account}:role/Auth0_invocation_role"
}
resource "aws_api_gateway_authorizer" "NonProd_Auth0_UnibanService" {
  name                   = "NonProd_Auth0_UnibanService"
  rest_api_id            = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  authorizer_uri         = "${aws_lambda_function.gore_AGWAuthenrizer_UnibanService_Hub.invoke_arn}"
  authorizer_credentials = "arn:aws:iam::${var.gore-hub-account}:role/Auth0_invocation_role"
}
resource "aws_api_gateway_authorizer" "NonProd_Auth0_Vehicle" {
  name                   = "NonProd_Auth0_Vehicle"
  rest_api_id            = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  authorizer_uri         = "${aws_lambda_function.gore_AGWAuthenrizer_Vehicle_Hub.invoke_arn}"
  authorizer_credentials = "arn:aws:iam::${var.gore-hub-account}:role/Auth0_invocation_role"
}

######### VPC LINKS ##############
# #VPC Link gore-insurcloud-dev01
#resource "aws_api_gateway_vpc_link" "gore-insurcloud-dev01" {
#  name        = "gore-insurcloud-dev01"
#  target_arns = ["${aws_lb.ICGORE_DEV01_APW_NLB.arn}"]
#}
# #VPC Link gore-insurcloud-qa01
#resource "aws_api_gateway_vpc_link" "gore-insurcloud-qa01" {
#  name        = "gore-insurcloud-qa01"
#  target_arns = ["${aws_lb.ICGORE_QA01_APW_NLB.arn}"]
#}
# #VPC Link gore-insurcloud-dev02
#resource "aws_api_gateway_vpc_link" "gore-insurcloud-dev02" {
#  name        = "gore-insurcloud-dev02"
#  target_arns = ["${aws_lb.ICGORE_DEV02_APW_NLB.arn}"]
#}
# #VPC Link gore-insurcloud-dev03
resource "aws_api_gateway_vpc_link" "gore-insurcloud-dev03" {
  name        = "gore-insurcloud-dev03"
  target_arns = ["${aws_lb.ICGORE_DEV03_APW_NLB.arn}"]
}
# #VPC Link gore-insurcloud-qa02
resource "aws_api_gateway_vpc_link" "gore-insurcloud-qa02" {
  name        = "gore-insurcloud-qa02"
  target_arns = ["${aws_lb.ICGORE_QA02_APW_NLB.arn}"]
}
resource "aws_api_gateway_vpc_link" "gore-insurcloud-tbt01" {
  name        = "gore-insurcloud-tbt01"
  target_arns = ["${aws_lb.ICGORE_TBT01_APW_NLB.arn}"]
}
resource "aws_api_gateway_vpc_link" "gore-insurcloud-tbt02" {
  name        = "gore-insurcloud-tbt02"
  target_arns = ["${aws_lb.ICGORE_TBT02_APW_NLB.arn}"]
}
resource "aws_api_gateway_vpc_link" "gore-insurcloud-bat" {
  name        = "gore-insurcloud-bat"
  target_arns = ["${aws_lb.ICGORE_BAT_APW_NLB.arn}"]
}
#####AUTH0 ENABLED API GATEWAYS#######
######### AUTH0 ##############

######### RESOURCE GROUPS ##############
#########DEV01#########

resource "aws_api_gateway_resource" "DEV01_AUTH" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_rest_api.GORE_AUTH0.root_resource_id}"
  path_part   = "DEV01_AUTH"
}
#####Address Validation#####
resource "aws_api_gateway_resource" "DEV01_Address" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.DEV01_AUTH.id}"
  path_part   = "DEV01_Address"
}

# Address Outbound GW -API Gateway - ARMS
resource "aws_api_gateway_method" "DEV01_Address" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id   = "${aws_api_gateway_resource.DEV01_Address.id}"
  http_method   = "POST"
  authorization = "CUSTOM"
  authorizer_id = "${aws_api_gateway_authorizer.NonProd_Auth0_AddressValidation.id}"
}

resource "aws_api_gateway_integration" "DEV01_Address" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id             = "${aws_api_gateway_resource.DEV01_Address.id}"
  http_method             = "${aws_api_gateway_method.DEV01_Address.http_method}"
  integration_http_method = "POST"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-dev01.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.dev01.gore.insurcloud.ca/address-validation-service/microservices/addressvalidation/ws"
}

resource "aws_api_gateway_method_response" "DEV01_Address" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id = "${aws_api_gateway_resource.DEV01_Address.id}"
  http_method = "${aws_api_gateway_method.DEV01_Address.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}
###HCAI Outbound###

resource "aws_api_gateway_resource" "DEV01_HCAISecurity" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.DEV01_AUTH.id}"
  path_part   = "DEV01_HCAISecurity"
}

# HCAI Outbound GW -API Gateway - ARMS
resource "aws_api_gateway_method" "DEV01_HCAISecurity" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id   = "${aws_api_gateway_resource.DEV01_HCAISecurity.id}"
  http_method   = "POST"
  authorization = "CUSTOM"
  authorizer_id = "${aws_api_gateway_authorizer.NonProd_Auth0_HCAI.id}"
}

resource "aws_api_gateway_integration" "DEV01_HCAISecurity" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id             = "${aws_api_gateway_resource.DEV01_HCAISecurity.id}"
  http_method             = "${aws_api_gateway_method.DEV01_HCAISecurity.http_method}"
  integration_http_method = "POST"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-dev01.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.dev01.gore.insurcloud.ca/soap-security-service/v1/proxy"
}

resource "aws_api_gateway_method_response" "DEV01_HCAISecurity" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id = "${aws_api_gateway_resource.DEV01_HCAISecurity.id}"
  http_method = "${aws_api_gateway_method.DEV01_HCAISecurity.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}

# ###Cheque ###
resource "aws_api_gateway_resource" "DEV01_ChequePrinting" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.DEV01_AUTH.id}"
  path_part   = "DEV01_ChequePrinting"
} 
# #Cheque Outbound#####
resource "aws_api_gateway_resource" "DEV01_SaveCheque" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.DEV01_ChequePrinting.id}"
  path_part   = "DEV01_SaveCheque"
}

resource "aws_api_gateway_method" "DEV01_SaveCheque" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id   = "${aws_api_gateway_resource.DEV01_SaveCheque.id}"
  http_method   = "POST"
  authorization = "CUSTOM"
  authorizer_id = "${aws_api_gateway_authorizer.NonProd_Auth0_ChequePrinting.id}"
}

resource "aws_api_gateway_integration" "DEV01_SaveCheque" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id             = "${aws_api_gateway_resource.DEV01_SaveCheque.id}"
  http_method             = "${aws_api_gateway_method.DEV01_SaveCheque.http_method}"
  integration_http_method = "POST"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-dev01.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.dev01.gore.insurcloud.ca/cheque-service/v1/cheque/print"
}

resource "aws_api_gateway_method_response" "DEV01_SaveCheque" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id = "${aws_api_gateway_resource.DEV01_SaveCheque.id}"
  http_method = "${aws_api_gateway_method.DEV01_SaveCheque.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}
# #Cheque --> F & 0 --> MS#####
resource "aws_api_gateway_resource" "DEV01_ChequeUpdateNumbers" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.DEV01_ChequePrinting.id}"
  path_part   = "DEV01_ChequeUpdateNumbers"
}

resource "aws_api_gateway_method" "DEV01_ChequeUpdateNumbers" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id   = "${aws_api_gateway_resource.DEV01_ChequeUpdateNumbers.id}"
  http_method   = "POST"
  authorization = "CUSTOM"
  authorizer_id = "${aws_api_gateway_authorizer.NonProd_Auth0_ChequePrinting.id}"
}

resource "aws_api_gateway_integration" "DEV01_ChequeUpdateNumbers" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id             = "${aws_api_gateway_resource.DEV01_ChequeUpdateNumbers.id}"
  http_method             = "${aws_api_gateway_method.DEV01_ChequeUpdateNumbers.http_method}"
  integration_http_method = "POST"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-dev01.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.dev01.gore.insurcloud.ca/cheque-service/v1/cheque/updateChequeNumbers"
}

resource "aws_api_gateway_method_response" "DEV01_ChequeUpdateNumbers" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id = "${aws_api_gateway_resource.DEV01_ChequeUpdateNumbers.id}"
  http_method = "${aws_api_gateway_method.DEV01_ChequeUpdateNumbers.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}

# #MVR Service#####
resource "aws_api_gateway_resource" "DEV01_MVRDriverInfo" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.DEV01_AUTH.id}"
  path_part   = "DEV01_MVRDriverInfo"
}

resource "aws_api_gateway_method" "DEV01_MVRDriverInfo" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id   = "${aws_api_gateway_resource.DEV01_MVRDriverInfo.id}"
  http_method   = "POST"
  authorization = "CUSTOM"
  authorizer_id = "${aws_api_gateway_authorizer.NonProd_Auth0_MVR.id}"
}

resource "aws_api_gateway_integration" "DEV01_MVRDriverInfo" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id             = "${aws_api_gateway_resource.DEV01_MVRDriverInfo.id}"
  http_method             = "${aws_api_gateway_method.DEV01_MVRDriverInfo.http_method}"
  integration_http_method = "POST"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-dev01.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.dev01.gore.insurcloud.ca/mvr-service/v1/driver/info"
}

resource "aws_api_gateway_method_response" "DEV01_MVRDriverInfo" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id = "${aws_api_gateway_resource.DEV01_MVRDriverInfo.id}"
  http_method = "${aws_api_gateway_method.DEV01_MVRDriverInfo.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}
# #Download Service for PC payload#####
resource "aws_api_gateway_resource" "DEV01_BMSDownload" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.DEV01_AUTH.id}"
  path_part   = "DEV01_BMSDownload"
}
resource "aws_api_gateway_resource" "DEV01_BMSDownload_PC" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.DEV01_BMSDownload.id}"
  path_part   = "DEV01_BMSDownload_PC"
}
resource "aws_api_gateway_method" "DEV01_BMSDownload_PC" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id   = "${aws_api_gateway_resource.DEV01_BMSDownload_PC.id}"
  http_method   = "POST"
  authorization = "NONE"
}

resource "aws_api_gateway_integration" "DEV01_BMSDownload_PC" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id             = "${aws_api_gateway_resource.DEV01_BMSDownload_PC.id}"
  http_method             = "${aws_api_gateway_method.DEV01_BMSDownload_PC.http_method}"
  integration_http_method = "POST"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-dev01.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.dev01.gore.insurcloud.ca/bms-download-service/v1/bmsdownload/pcpayload"
}

resource "aws_api_gateway_method_response" "DEV01_BMSDownload_PC" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id = "${aws_api_gateway_resource.DEV01_BMSDownload_PC.id}"
  http_method = "${aws_api_gateway_method.DEV01_BMSDownload_PC.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}

# #Download Service for BC payload#####
resource "aws_api_gateway_resource" "DEV01_BMSDownload_BC" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.DEV01_BMSDownload.id}"
  path_part   = "DEV01_BMSDownload_BC"
}

resource "aws_api_gateway_method" "DEV01_BMSDownload_BC" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id   = "${aws_api_gateway_resource.DEV01_BMSDownload_BC.id}"
  http_method   = "POST"
  authorization = "NONE"
}

resource "aws_api_gateway_integration" "DEV01_BMSDownload_BC" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id             = "${aws_api_gateway_resource.DEV01_BMSDownload_BC.id}"
  http_method             = "${aws_api_gateway_method.DEV01_BMSDownload_BC.http_method}"
  integration_http_method = "POST"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-dev01.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.dev01.gore.insurcloud.ca/bms-download-service/v1/bmsdownload/bcpayload"
}

resource "aws_api_gateway_method_response" "DEV01_BMSDownload_BC" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id = "${aws_api_gateway_resource.DEV01_BMSDownload_BC.id}"
  http_method = "${aws_api_gateway_method.DEV01_BMSDownload_BC.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}

###Document Service###

resource "aws_api_gateway_resource" "DEV01_Document" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.DEV01_AUTH.id}"
  path_part   = "DEV01_Document"
}

# Document Search
resource "aws_api_gateway_resource" "DEV01_Document_Search" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.DEV01_Document.id}"
  path_part   = "DEV01_Document_Search"
}

resource "aws_api_gateway_method" "DEV01_Document_Search" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id   = "${aws_api_gateway_resource.DEV01_Document_Search.id}"
  http_method   = "POST"
  authorization = "CUSTOM"
  authorizer_id = "${aws_api_gateway_authorizer.NonProd_Auth0_HCAI.id}"
}

resource "aws_api_gateway_integration" "DEV01_Document_Search" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id             = "${aws_api_gateway_resource.DEV01_Document_Search.id}"
  http_method             = "${aws_api_gateway_method.DEV01_Document_Search.http_method}"
  integration_http_method = "POST"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-dev01.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.dev01.gore.insurcloud.ca/document-service/v1/document/gore/summaries/search/metadata"
}

resource "aws_api_gateway_method_response" "DEV01_Document_Search" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id = "${aws_api_gateway_resource.DEV01_Document_Search.id}"
  http_method = "${aws_api_gateway_method.DEV01_Document_Search.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}
# Document Retrieve
resource "aws_api_gateway_resource" "DEV01_Document_Retrieve" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.DEV01_Document.id}"
  path_part   = "DEV01_Document_Retrieve"
}

resource "aws_api_gateway_method" "DEV01_Document_Retrieve" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id   = "${aws_api_gateway_resource.DEV01_Document_Retrieve.id}"
  http_method   = "POST"
  authorization = "CUSTOM"
  authorizer_id = "${aws_api_gateway_authorizer.NonProd_Auth0_HCAI.id}"
}

resource "aws_api_gateway_integration" "DEV01_Document_Retrieve" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id             = "${aws_api_gateway_resource.DEV01_Document_Retrieve.id}"
  http_method             = "${aws_api_gateway_method.DEV01_Document_Retrieve.http_method}"
  integration_http_method = "POST"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-dev01.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.dev01.gore.insurcloud.ca/document-service/v1/document/gore/retrieve/key"
}

resource "aws_api_gateway_method_response" "DEV01_Document_Retrieve" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id = "${aws_api_gateway_resource.DEV01_Document_Retrieve.id}"
  http_method = "${aws_api_gateway_method.DEV01_Document_Retrieve.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}

# Document Upload1
resource "aws_api_gateway_resource" "DEV01_Document_Upload1" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.DEV01_Document.id}"
  path_part   = "DEV01_Document_Upload1"
}

resource "aws_api_gateway_method" "DEV01_Document_Upload1" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id   = "${aws_api_gateway_resource.DEV01_Document_Upload1.id}"
  http_method   = "POST"
  authorization = "CUSTOM"
  authorizer_id = "${aws_api_gateway_authorizer.NonProd_Auth0_HCAI.id}"
}

resource "aws_api_gateway_integration" "DEV01_Document_Upload1" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id             = "${aws_api_gateway_resource.DEV01_Document_Upload1.id}"
  http_method             = "${aws_api_gateway_method.DEV01_Document_Upload1.http_method}"
  integration_http_method = "POST"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-dev01.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.dev01.gore.insurcloud.ca/document-service/v1/document/gore/base64/compose"
}

resource "aws_api_gateway_method_response" "DEV01_Document_Upload1" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id = "${aws_api_gateway_resource.DEV01_Document_Upload1.id}"
  http_method = "${aws_api_gateway_method.DEV01_Document_Upload1.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}

# Document Upload2
resource "aws_api_gateway_resource" "DEV01_Document_Upload2" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.DEV01_Document.id}"
  path_part   = "DEV01_Document_Upload2"
}

resource "aws_api_gateway_method" "DEV01_Document_Upload2" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id   = "${aws_api_gateway_resource.DEV01_Document_Upload2.id}"
  http_method   = "POST"
  authorization = "CUSTOM"
  authorizer_id = "${aws_api_gateway_authorizer.NonProd_Auth0_HCAI.id}"
}

resource "aws_api_gateway_integration" "DEV01_Document_Upload2" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id             = "${aws_api_gateway_resource.DEV01_Document_Upload2.id}"
  http_method             = "${aws_api_gateway_method.DEV01_Document_Upload2.http_method}"
  integration_http_method = "POST"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-dev01.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.dev01.gore.insurcloud.ca/document-service/v1/document/gore/save/document/payload"
}

resource "aws_api_gateway_method_response" "DEV01_Document_Upload2" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id = "${aws_api_gateway_resource.DEV01_Document_Upload2.id}"
  http_method = "${aws_api_gateway_method.DEV01_Document_Upload2.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}
###CRC###
resource "aws_api_gateway_resource" "DEV01_CRC" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.DEV01_AUTH.id}"
  path_part   = "DEV01_CRC"
}

resource "aws_api_gateway_resource" "DEV01_CRCCreateFNOL" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.DEV01_CRC.id}"
  path_part   = "DEV01_CRCCreateFNOL"
}

#CRC --> Guidewire
resource "aws_api_gateway_method" "DEV01_CRCCreateFNOL" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id   = "${aws_api_gateway_resource.DEV01_CRCCreateFNOL.id}"
  http_method   = "POST"
  authorization = "CUSTOM"
  authorizer_id = "${aws_api_gateway_authorizer.NonProd_Auth0_CRC.id}"
}

resource "aws_api_gateway_integration" "DEV01_CRCCreateFNOL" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id             = "${aws_api_gateway_resource.DEV01_CRCCreateFNOL.id}"
  http_method             = "${aws_api_gateway_method.DEV01_CRCCreateFNOL.http_method}"
  integration_http_method = "POST"
  connection_type         = "INTERNET"
  type                    = "HTTP_PROXY"
  uri                     = "https://dev.goredev.guidewire.net/cc/ws/ic/integration/crc/CollisionReportingCenterAPI"
}

resource "aws_api_gateway_method_response" "DEV01_CRCCreateFNOL" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id = "${aws_api_gateway_resource.DEV01_CRCCreateFNOL.id}"
  http_method = "${aws_api_gateway_method.DEV01_CRCCreateFNOL.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}

#CRC --> MS 
resource "aws_api_gateway_resource" "DEV01_CRCCollisionDocumentsMS" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.DEV01_CRC.id}"
  path_part   = "DEV01_CRCCollisionDocumentsMS"
}

resource "aws_api_gateway_method" "DEV01_CRCCollisionDocumentsMS" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id   = "${aws_api_gateway_resource.DEV01_CRCCollisionDocumentsMS.id}"
  http_method   = "POST"
  authorization = "CUSTOM"
  authorizer_id = "${aws_api_gateway_authorizer.NonProd_Auth0_CRC.id}"
}

resource "aws_api_gateway_integration" "DEV01_CRCCollisionDocumentsMS" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id             = "${aws_api_gateway_resource.DEV01_CRCCollisionDocumentsMS.id}"
  http_method             = "${aws_api_gateway_method.DEV01_CRCCollisionDocumentsMS.http_method}"
  integration_http_method = "POST"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-dev01.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.dev01.gore.insurcloud.ca/crc-service/microservices/crc/ws/"
}

resource "aws_api_gateway_method_response" "DEV01_CRCCollisionDocumentsMS" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id = "${aws_api_gateway_resource.DEV01_CRCCollisionDocumentsMS.id}"
  http_method = "${aws_api_gateway_method.DEV01_CRCCollisionDocumentsMS.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}

####Audatex###

# Audatex Inbount Acknowledgment - ARMS

resource "aws_api_gateway_resource" "DEV01_AudatexINB" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.DEV01_AUTH.id}"
  path_part   = "DEV01_AudatexINB"
}
resource "aws_api_gateway_resource" "DEV01_AudatexAssignmentAck" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.DEV01_AudatexINB.id}"
  path_part   = "DEV01_AudatexAssignmentAck"
}

resource "aws_api_gateway_method" "DEV01_AudatexAssignmentAck" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id   = "${aws_api_gateway_resource.DEV01_AudatexAssignmentAck.id}"
  http_method   = "POST"
  authorization = "CUSTOM"
  authorizer_id = "${aws_api_gateway_authorizer.NonProd_Auth0_Audatex.id}"
}

resource "aws_api_gateway_integration" "DEV01_AudatexAssignmentAck" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id             = "${aws_api_gateway_resource.DEV01_AudatexAssignmentAck.id}"
  http_method             = "${aws_api_gateway_method.DEV01_AudatexAssignmentAck.http_method}"
  integration_http_method = "POST"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-dev01.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.dev01.gore.insurcloud.ca/audatex-service/v1/assignment/ack"
}

resource "aws_api_gateway_method_response" "DEV01_AudatexAssignmentAck" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id = "${aws_api_gateway_resource.DEV01_AudatexAssignmentAck.id}"
  http_method = "${aws_api_gateway_method.DEV01_AudatexAssignmentAck.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}

##Audatex Inbound estimate
resource "aws_api_gateway_resource" "DEV01_AudatexEstimate" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.DEV01_AudatexINB.id}"
  path_part   = "DEV01_AudatexEstimate"
}

resource "aws_api_gateway_method" "DEV01_AudatexEstimate" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id   = "${aws_api_gateway_resource.DEV01_AudatexEstimate.id}"
  http_method   = "POST"
  authorization = "CUSTOM"
  authorizer_id = "${aws_api_gateway_authorizer.NonProd_Auth0_Audatex.id}"
}


resource "aws_api_gateway_integration" "DEV01_AudatexEstimate" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id             = "${aws_api_gateway_resource.DEV01_AudatexEstimate.id}"
  http_method             = "${aws_api_gateway_method.DEV01_AudatexEstimate.http_method}"
  integration_http_method = "POST"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-dev01.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.dev01.gore.insurcloud.ca/audatex-service/v1/estimate"
}

resource "aws_api_gateway_method_response" "DEV01_AudatexEstimate" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id = "${aws_api_gateway_resource.DEV01_AudatexEstimate.id}"
  http_method = "${aws_api_gateway_method.DEV01_AudatexEstimate.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}
####end of audatex#####
######

# ###Uniban###
resource "aws_api_gateway_resource" "DEV01_Uniban_Get" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.DEV01_AUTH.id}"
  path_part   = "DEV01_Uniban_Get"
}

##Uniban --> API
resource "aws_api_gateway_method" "DEV01_Uniban_Get" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id   = "${aws_api_gateway_resource.DEV01_Uniban_Get.id}"
  http_method   = "GET"
  authorization = "CUSTOM"
  authorizer_id = "${aws_api_gateway_authorizer.NonProd_Auth0_UnibanService.id}"
  request_parameters = {
    "method.request.querystring.policyNumber"= true
    "method.request.querystring.dateOfLoss"= true
    "method.request.querystring.vin"= true
  }
}

resource "aws_api_gateway_integration" "DEV01_Uniban_Get" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id             = "${aws_api_gateway_resource.DEV01_Uniban_Get.id}"
  http_method             = "${aws_api_gateway_method.DEV01_Uniban_Get.http_method}"
  integration_http_method = "GET"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-dev01.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.dev01.gore.insurcloud.ca/uniban-service/v1/retrievePolicy"
}

resource "aws_api_gateway_method_response" "DEV01_Uniban_Get" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id = "${aws_api_gateway_resource.DEV01_Uniban_Get.id}"
  http_method = "${aws_api_gateway_method.DEV01_Uniban_Get.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}
###IBC Vehicle MicroService###

resource "aws_api_gateway_resource" "DEV01_IBC" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.DEV01_AUTH.id}"
  path_part   = "DEV01_IBC"
}

# IBC Vehicle Endpoint 1
resource "aws_api_gateway_resource" "DEV01_vininfo" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.DEV01_IBC.id}"
  path_part   = "DEV01_vininfo"
}

resource "aws_api_gateway_method" "DEV01_vininfo" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id   = "${aws_api_gateway_resource.DEV01_vininfo.id}"
  http_method   = "GET"
  authorization = "CUSTOM"
  authorizer_id = "${aws_api_gateway_authorizer.NonProd_Auth0_Vehicle.id}"
  request_parameters = {
    "method.request.querystring.vin"= true
  }
}

resource "aws_api_gateway_integration" "DEV01_vininfo" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id             = "${aws_api_gateway_resource.DEV01_vininfo.id}"
  http_method             = "${aws_api_gateway_method.DEV01_vininfo.http_method}"
  integration_http_method = "GET"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-dev01.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.dev01.gore.insurcloud.ca/vehicle-info-service/v1/vininfo"
}

resource "aws_api_gateway_method_response" "DEV01_vininfo" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id = "${aws_api_gateway_resource.DEV01_vininfo.id}"
  http_method = "${aws_api_gateway_method.DEV01_vininfo.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}
# IBC Vehicle Endpoint 2####
resource "aws_api_gateway_resource" "DEV01_submit" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.DEV01_IBC.id}"
  path_part   = "DEV01_submit"
}

resource "aws_api_gateway_method" "DEV01_submit" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id   = "${aws_api_gateway_resource.DEV01_submit.id}"
  http_method   = "POST"
  authorization = "CUSTOM"
  authorizer_id = "${aws_api_gateway_authorizer.NonProd_Auth0_Vehicle.id}"
}

resource "aws_api_gateway_integration" "DEV01_submit" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id             = "${aws_api_gateway_resource.DEV01_submit.id}"
  http_method             = "${aws_api_gateway_method.DEV01_submit.http_method}"
  integration_http_method = "POST"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-dev01.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.dev01.gore.insurcloud.ca/vehicle-info-service/v1/product/download/submit"
}

resource "aws_api_gateway_method_response" "DEV01_submit" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id = "${aws_api_gateway_resource.DEV01_submit.id}"
  http_method = "${aws_api_gateway_method.DEV01_submit.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}

# IBC Vehicle ppvinfo
resource "aws_api_gateway_resource" "DEV01_ppvinfo" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.DEV01_IBC.id}"
  path_part   = "DEV01_ppvinfo"
}

resource "aws_api_gateway_method" "DEV01_ppvinfo" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id   = "${aws_api_gateway_resource.DEV01_ppvinfo.id}"
  http_method   = "GET"
  authorization = "CUSTOM"
  authorizer_id = "${aws_api_gateway_authorizer.NonProd_Auth0_Vehicle.id}"
  request_parameters = {
    "method.request.querystring.rateGroupPublicationYear"= true
    "method.request.querystring.carCode"= true
    "method.request.querystring.modelYear"= true
  }
}

resource "aws_api_gateway_integration" "DEV01_ppvinfo" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id             = "${aws_api_gateway_resource.DEV01_ppvinfo.id}"
  http_method             = "${aws_api_gateway_method.DEV01_ppvinfo.http_method}"
  integration_http_method = "GET"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-dev01.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.dev01.gore.insurcloud.ca/vehicle-info-service/v1/ppvinfo"
}

resource "aws_api_gateway_method_response" "DEV01_ppvinfo" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id = "${aws_api_gateway_resource.DEV01_ppvinfo.id}"
  http_method = "${aws_api_gateway_method.DEV01_ppvinfo.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}

# IBC Vehicle recinfo
resource "aws_api_gateway_resource" "DEV01_recinfo" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.DEV01_IBC.id}"
  path_part   = "DEV01_recinfo"
}

resource "aws_api_gateway_method" "DEV01_recinfo" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id   = "${aws_api_gateway_resource.DEV01_recinfo.id}"
  http_method   = "GET"
  authorization = "CUSTOM"
  authorizer_id = "${aws_api_gateway_authorizer.NonProd_Auth0_Vehicle.id}"
  request_parameters = {
    "method.request.querystring.publicationYear"= true
    "method.request.querystring.vehicleType"= true
    "method.request.querystring.vehicleMake"= true
    "method.request.querystring.vehicleModel"= true
    "method.request.querystring.modelYear"= true
  }
}

resource "aws_api_gateway_integration" "DEV01_recinfo" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id             = "${aws_api_gateway_resource.DEV01_recinfo.id}"
  http_method             = "${aws_api_gateway_method.DEV01_recinfo.http_method}"
  integration_http_method = "GET"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-dev01.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.dev01.gore.insurcloud.ca/vehicle-info-service/v1/recinfo"
}

resource "aws_api_gateway_method_response" "DEV01_recinfo" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id = "${aws_api_gateway_resource.DEV01_recinfo.id}"
  http_method = "${aws_api_gateway_method.DEV01_recinfo.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}


######### RESOURCE GROUPS ##############
#########DEV02#########

resource "aws_api_gateway_resource" "DEV02_AUTH" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_rest_api.GORE_AUTH0.root_resource_id}"
  path_part   = "DEV02_AUTH"
}
#####Address Validation#####
resource "aws_api_gateway_resource" "DEV02_Address" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.DEV02_AUTH.id}"
  path_part   = "DEV02_Address"
}

# Address Outbound GW -API Gateway - ARMS
resource "aws_api_gateway_method" "DEV02_Address" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id   = "${aws_api_gateway_resource.DEV02_Address.id}"
  http_method   = "POST"
  authorization = "CUSTOM"
  authorizer_id = "${aws_api_gateway_authorizer.NonProd_Auth0_AddressValidation.id}"
}

resource "aws_api_gateway_integration" "DEV02_Address" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id             = "${aws_api_gateway_resource.DEV02_Address.id}"
  http_method             = "${aws_api_gateway_method.DEV02_Address.http_method}"
  integration_http_method = "POST"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-dev02.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.dev02.gore.insurcloud.ca/address-validation-service/microservices/addressvalidation/ws"
}

resource "aws_api_gateway_method_response" "DEV02_Address" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id = "${aws_api_gateway_resource.DEV02_Address.id}"
  http_method = "${aws_api_gateway_method.DEV02_Address.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}

###HCAI###

resource "aws_api_gateway_resource" "DEV02_HCAISecurity" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.DEV02_AUTH.id}"
  path_part   = "DEV02_HCAISecurity"
}

# HCAI Outbound GW -API Gateway - ARMS
resource "aws_api_gateway_method" "DEV02_HCAISecurity" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id   = "${aws_api_gateway_resource.DEV02_HCAISecurity.id}"
  http_method   = "POST"
  authorization = "CUSTOM"
  authorizer_id = "${aws_api_gateway_authorizer.NonProd_Auth0_HCAI.id}"
}

resource "aws_api_gateway_integration" "DEV02_HCAISecurity" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id             = "${aws_api_gateway_resource.DEV02_HCAISecurity.id}"
  http_method             = "${aws_api_gateway_method.DEV02_HCAISecurity.http_method}"
  integration_http_method = "POST"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-dev02.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.dev02.gore.insurcloud.ca/soap-security-service/v1/proxy"
}

resource "aws_api_gateway_method_response" "DEV02_HCAISecurity" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id = "${aws_api_gateway_resource.DEV02_HCAISecurity.id}"
  http_method = "${aws_api_gateway_method.DEV02_HCAISecurity.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}

# ###Cheque ###
resource "aws_api_gateway_resource" "DEV02_ChequePrinting" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.DEV02_AUTH.id}"
  path_part   = "DEV02_ChequePrinting"
} 
# #Cheque Outbound#####
resource "aws_api_gateway_resource" "DEV02_SaveCheque" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.DEV02_ChequePrinting.id}"
  path_part   = "DEV02_SaveCheque"
}

resource "aws_api_gateway_method" "DEV02_SaveCheque" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id   = "${aws_api_gateway_resource.DEV02_SaveCheque.id}"
  http_method   = "POST"
  authorization = "CUSTOM"
  authorizer_id = "${aws_api_gateway_authorizer.NonProd_Auth0_ChequePrinting.id}"
}

resource "aws_api_gateway_integration" "DEV02_SaveCheque" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id             = "${aws_api_gateway_resource.DEV02_SaveCheque.id}"
  http_method             = "${aws_api_gateway_method.DEV02_SaveCheque.http_method}"
  integration_http_method = "POST"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-dev02.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.dev02.gore.insurcloud.ca/cheque-service/v1/cheque/print"
}

resource "aws_api_gateway_method_response" "DEV02_SaveCheque" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id = "${aws_api_gateway_resource.DEV02_SaveCheque.id}"
  http_method = "${aws_api_gateway_method.DEV02_SaveCheque.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}
# #Cheque --> F & 0 --> MS#####
resource "aws_api_gateway_resource" "DEV02_ChequeUpdateNumbers" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.DEV02_ChequePrinting.id}"
  path_part   = "DEV02_ChequeUpdateNumbers"
}

resource "aws_api_gateway_method" "DEV02_ChequeUpdateNumbers" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id   = "${aws_api_gateway_resource.DEV02_ChequeUpdateNumbers.id}"
  http_method   = "POST"
  authorization = "CUSTOM"
  authorizer_id = "${aws_api_gateway_authorizer.NonProd_Auth0_ChequePrinting.id}"
}

resource "aws_api_gateway_integration" "DEV02_ChequeUpdateNumbers" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id             = "${aws_api_gateway_resource.DEV02_ChequeUpdateNumbers.id}"
  http_method             = "${aws_api_gateway_method.DEV02_ChequeUpdateNumbers.http_method}"
  integration_http_method = "POST"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-dev02.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.dev02.gore.insurcloud.ca/cheque-service/v1/cheque/updateChequeNumbers"
}

resource "aws_api_gateway_method_response" "DEV02_ChequeUpdateNumbers" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id = "${aws_api_gateway_resource.DEV02_ChequeUpdateNumbers.id}"
  http_method = "${aws_api_gateway_method.DEV02_ChequeUpdateNumbers.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}

# #MVR Service#####
resource "aws_api_gateway_resource" "DEV02_MVRDriverInfo" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.DEV02_AUTH.id}"
  path_part   = "DEV02_MVRDriverInfo"
}

resource "aws_api_gateway_method" "DEV02_MVRDriverInfo" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id   = "${aws_api_gateway_resource.DEV02_MVRDriverInfo.id}"
  http_method   = "POST"
  authorization = "CUSTOM"
  authorizer_id = "${aws_api_gateway_authorizer.NonProd_Auth0_MVR.id}"
}

resource "aws_api_gateway_integration" "DEV02_MVRDriverInfo" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id             = "${aws_api_gateway_resource.DEV02_MVRDriverInfo.id}"
  http_method             = "${aws_api_gateway_method.DEV02_MVRDriverInfo.http_method}"
  integration_http_method = "POST"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-dev02.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.dev02.gore.insurcloud.ca/mvr-service/v1/driver/info"
}

resource "aws_api_gateway_method_response" "DEV02_MVRDriverInfo" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id = "${aws_api_gateway_resource.DEV02_MVRDriverInfo.id}"
  http_method = "${aws_api_gateway_method.DEV02_MVRDriverInfo.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}
# #Download Service for PC payload#####
resource "aws_api_gateway_resource" "DEV02_BMSDownload" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.DEV02_AUTH.id}"
  path_part   = "DEV02_BMSDownload"
}
resource "aws_api_gateway_resource" "DEV02_BMSDownload_PC" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.DEV02_BMSDownload.id}"
  path_part   = "DEV02_BMSDownload_PC"
}

resource "aws_api_gateway_method" "DEV02_BMSDownload_PC" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id   = "${aws_api_gateway_resource.DEV02_BMSDownload_PC.id}"
  http_method   = "POST"
  authorization = "CUSTOM"
  authorizer_id = "${aws_api_gateway_authorizer.NonProd_Auth0_MVR.id}"
}

resource "aws_api_gateway_integration" "DEV02_BMSDownload_PC" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id             = "${aws_api_gateway_resource.DEV02_BMSDownload_PC.id}"
  http_method             = "${aws_api_gateway_method.DEV02_BMSDownload_PC.http_method}"
  integration_http_method = "POST"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-qa01.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.dev02.gore.insurcloud.ca/bms-download-service/v1/bmsdownload/pcpayload"
}

resource "aws_api_gateway_method_response" "DEV02_BMSDownload_PC" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id = "${aws_api_gateway_resource.DEV02_BMSDownload_PC.id}"
  http_method = "${aws_api_gateway_method.DEV02_BMSDownload_PC.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}

# #Download Service for BC payload#####

resource "aws_api_gateway_resource" "DEV02_BMSDownload_BC" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.DEV02_BMSDownload.id}"
  path_part   = "DEV02_BMSDownload_BC"
}

resource "aws_api_gateway_method" "DEV02_BMSDownload_BC" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id   = "${aws_api_gateway_resource.DEV02_BMSDownload_BC.id}"
  http_method   = "POST"
  authorization = "CUSTOM"
  authorizer_id = "${aws_api_gateway_authorizer.NonProd_Auth0_MVR.id}"
}

resource "aws_api_gateway_integration" "DEV02_BMSDownload_BC" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id             = "${aws_api_gateway_resource.DEV02_BMSDownload_BC.id}"
  http_method             = "${aws_api_gateway_method.DEV02_BMSDownload_BC.http_method}"
  integration_http_method = "POST"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-dev02.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.dev02.gore.insurcloud.ca/bms-download-service/v1/bmsdownload/bcpayload"
}

resource "aws_api_gateway_method_response" "DEV02_BMSDownload_BC" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id = "${aws_api_gateway_resource.DEV02_BMSDownload_BC.id}"
  http_method = "${aws_api_gateway_method.DEV02_BMSDownload_BC.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}
###Document Service###

resource "aws_api_gateway_resource" "DEV02_Document" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.DEV02_AUTH.id}"
  path_part   = "DEV02_Document"
}

# Document Search
resource "aws_api_gateway_resource" "DEV02_Document_Search" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.DEV02_Document.id}"
  path_part   = "DEV02_Document_Search"
}

resource "aws_api_gateway_method" "DEV02_Document_Search" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id   = "${aws_api_gateway_resource.DEV02_Document_Search.id}"
  http_method   = "POST"
  authorization = "CUSTOM"
  authorizer_id = "${aws_api_gateway_authorizer.NonProd_Auth0_HCAI.id}"
}

resource "aws_api_gateway_integration" "DEV02_Document_Search" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id             = "${aws_api_gateway_resource.DEV02_Document_Search.id}"
  http_method             = "${aws_api_gateway_method.DEV02_Document_Search.http_method}"
  integration_http_method = "POST"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-dev02.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.dev02.gore.insurcloud.ca/document-service/v1/document/gore/summaries/search/metadata"
}

resource "aws_api_gateway_method_response" "DEV02_Document_Search" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id = "${aws_api_gateway_resource.DEV02_Document_Search.id}"
  http_method = "${aws_api_gateway_method.DEV02_Document_Search.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}
# Document Retrieve
resource "aws_api_gateway_resource" "DEV02_Document_Retrieve" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.DEV02_Document.id}"
  path_part   = "DEV02_Document_Retrieve"
}

resource "aws_api_gateway_method" "DEV02_Document_Retrieve" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id   = "${aws_api_gateway_resource.DEV02_Document_Retrieve.id}"
  http_method   = "POST"
  authorization = "CUSTOM"
  authorizer_id = "${aws_api_gateway_authorizer.NonProd_Auth0_HCAI.id}"
}

resource "aws_api_gateway_integration" "DEV02_Document_Retrieve" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id             = "${aws_api_gateway_resource.DEV02_Document_Retrieve.id}"
  http_method             = "${aws_api_gateway_method.DEV02_Document_Retrieve.http_method}"
  integration_http_method = "POST"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-dev02.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.dev02.gore.insurcloud.ca/document-service/v1/document/gore/retrieve/key"
}

resource "aws_api_gateway_method_response" "DEV02_Document_Retrieve" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id = "${aws_api_gateway_resource.DEV02_Document_Retrieve.id}"
  http_method = "${aws_api_gateway_method.DEV02_Document_Retrieve.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}

# Document Upload1
resource "aws_api_gateway_resource" "DEV02_Document_Upload1" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.DEV02_Document.id}"
  path_part   = "DEV02_Document_Upload1"
}

resource "aws_api_gateway_method" "DEV02_Document_Upload1" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id   = "${aws_api_gateway_resource.DEV02_Document_Upload1.id}"
  http_method   = "POST"
  authorization = "CUSTOM"
  authorizer_id = "${aws_api_gateway_authorizer.NonProd_Auth0_HCAI.id}"
}

resource "aws_api_gateway_integration" "DEV02_Document_Upload1" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id             = "${aws_api_gateway_resource.DEV02_Document_Upload1.id}"
  http_method             = "${aws_api_gateway_method.DEV02_Document_Upload1.http_method}"
  integration_http_method = "POST"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-dev02.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.dev02.gore.insurcloud.ca/document-service/v1/document/gore/base64/compose"
}

resource "aws_api_gateway_method_response" "DEV02_Document_Upload1" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id = "${aws_api_gateway_resource.DEV02_Document_Upload1.id}"
  http_method = "${aws_api_gateway_method.DEV02_Document_Upload1.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}

# Document Upload2
resource "aws_api_gateway_resource" "DEV02_Document_Upload2" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.DEV02_Document.id}"
  path_part   = "DEV02_Document_Upload2"
}

resource "aws_api_gateway_method" "DEV02_Document_Upload2" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id   = "${aws_api_gateway_resource.DEV02_Document_Upload2.id}"
  http_method   = "POST"
  authorization = "CUSTOM"
  authorizer_id = "${aws_api_gateway_authorizer.NonProd_Auth0_HCAI.id}"
}

resource "aws_api_gateway_integration" "DEV02_Document_Upload2" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id             = "${aws_api_gateway_resource.DEV02_Document_Upload2.id}"
  http_method             = "${aws_api_gateway_method.DEV02_Document_Upload2.http_method}"
  integration_http_method = "POST"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-dev02.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.dev02.gore.insurcloud.ca/document-service/v1/document/gore/save/document/payload"
}

resource "aws_api_gateway_method_response" "DEV02_Document_Upload2" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id = "${aws_api_gateway_resource.DEV02_Document_Upload2.id}"
  http_method = "${aws_api_gateway_method.DEV02_Document_Upload2.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}
###CRC###
resource "aws_api_gateway_resource" "DEV02_CRC" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.DEV02_AUTH.id}"
  path_part   = "DEV02_CRC"
}

resource "aws_api_gateway_resource" "DEV02_CRCCreateFNOL" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.DEV02_CRC.id}"
  path_part   = "DEV02_CRCCreateFNOL"
}

#CRC --> Guidewire
resource "aws_api_gateway_method" "DEV02_CRCCreateFNOL" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id   = "${aws_api_gateway_resource.DEV02_CRCCreateFNOL.id}"
  http_method   = "POST"
  authorization = "CUSTOM"
  authorizer_id = "${aws_api_gateway_authorizer.NonProd_Auth0_CRC.id}"
}

resource "aws_api_gateway_integration" "DEV02_CRCCreateFNOL" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id             = "${aws_api_gateway_resource.DEV02_CRCCreateFNOL.id}"
  http_method             = "${aws_api_gateway_method.DEV02_CRCCreateFNOL.http_method}"
  integration_http_method = "POST"
  connection_type         = "INTERNET"
  type                    = "HTTP_PROXY"
  uri                     = "https://dev2.goredev.guidewire.net/cc/ws/ic/integration/crc/CollisionReportingCenterAPI"
}

resource "aws_api_gateway_method_response" "DEV02_CRCCreateFNOL" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id = "${aws_api_gateway_resource.DEV02_CRCCreateFNOL.id}"
  http_method = "${aws_api_gateway_method.DEV02_CRCCreateFNOL.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}

#CRC --> MS 

resource "aws_api_gateway_resource" "DEV02_CRCCollisionDocumentsMS" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.DEV02_CRC.id}"
  path_part   = "DEV02_CRCCollisionDocumentsMS"
}

resource "aws_api_gateway_method" "DEV02_CRCCollisionDocumentsMS" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id   = "${aws_api_gateway_resource.DEV02_CRCCollisionDocumentsMS.id}"
  http_method   = "POST"
  authorization = "CUSTOM"
  authorizer_id = "${aws_api_gateway_authorizer.NonProd_Auth0_CRC.id}"
}

resource "aws_api_gateway_integration" "DEV02_CRCCollisionDocumentsMS" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id             = "${aws_api_gateway_resource.DEV02_CRCCollisionDocumentsMS.id}"
  http_method             = "${aws_api_gateway_method.DEV02_CRCCollisionDocumentsMS.http_method}"
  integration_http_method = "POST"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-dev02.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.dev02.gore.insurcloud.ca/crc-service/microservices/crc/ws/"
}

resource "aws_api_gateway_method_response" "DEV02_CRCCollisionDocumentsMS" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id = "${aws_api_gateway_resource.DEV02_CRCCollisionDocumentsMS.id}"
  http_method = "${aws_api_gateway_method.DEV02_CRCCollisionDocumentsMS.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}

####Audatex###
# Audatex Inbount Acknowledgment - ARMS

resource "aws_api_gateway_resource" "DEV02_AudatexINB" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.DEV02_AUTH.id}"
  path_part   = "DEV02_AudatexINB"
}
resource "aws_api_gateway_resource" "DEV02_AudatexAssignmentAck" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.DEV02_AudatexINB.id}"
  path_part   = "DEV02_AudatexAssignmentAck"
}

resource "aws_api_gateway_method" "DEV02_AudatexAssignmentAck" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id   = "${aws_api_gateway_resource.DEV02_AudatexAssignmentAck.id}"
  http_method   = "POST"
  authorization = "CUSTOM"
  authorizer_id = "${aws_api_gateway_authorizer.NonProd_Auth0_Audatex.id}"
}

resource "aws_api_gateway_integration" "DEV02_AudatexAssignmentAck" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id             = "${aws_api_gateway_resource.DEV02_AudatexAssignmentAck.id}"
  http_method             = "${aws_api_gateway_method.DEV02_AudatexAssignmentAck.http_method}"
  integration_http_method = "POST"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-dev02.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.dev02.gore.insurcloud.ca/audatex-service/v1/assignment/ack"
}

resource "aws_api_gateway_method_response" "DEV02_AudatexAssignmentAck" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id = "${aws_api_gateway_resource.DEV02_AudatexAssignmentAck.id}"
  http_method = "${aws_api_gateway_method.DEV02_AudatexAssignmentAck.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}

##Audatex Inbound estimate
resource "aws_api_gateway_resource" "DEV02_AudatexEstimate" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.DEV02_AudatexINB.id}"
  path_part   = "DEV02_AudatexEstimate"
}

resource "aws_api_gateway_method" "DEV02_AudatexEstimate" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id   = "${aws_api_gateway_resource.DEV02_AudatexEstimate.id}"
  http_method   = "POST"
  authorization = "CUSTOM"
  authorizer_id = "${aws_api_gateway_authorizer.NonProd_Auth0_Audatex.id}"
}


resource "aws_api_gateway_integration" "DEV02_AudatexEstimate" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id             = "${aws_api_gateway_resource.DEV02_AudatexEstimate.id}"
  http_method             = "${aws_api_gateway_method.DEV02_AudatexEstimate.http_method}"
  integration_http_method = "POST"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-dev02.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.dev02.gore.insurcloud.ca/audatex-service/v1/estimate"
}

resource "aws_api_gateway_method_response" "DEV02_AudatexEstimate" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id = "${aws_api_gateway_resource.DEV02_AudatexEstimate.id}"
  http_method = "${aws_api_gateway_method.DEV02_AudatexEstimate.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}

# ###Uniban###
resource "aws_api_gateway_resource" "DEV02_Uniban_Get" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.DEV02_AUTH.id}"
  path_part   = "DEV02_Uniban_Get"
}

##Uniban --> API
resource "aws_api_gateway_method" "DEV02_Uniban_Get" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id   = "${aws_api_gateway_resource.DEV02_Uniban_Get.id}"
  http_method   = "GET"
  authorization = "CUSTOM"
  authorizer_id = "${aws_api_gateway_authorizer.NonProd_Auth0_UnibanService.id}"
  request_parameters = {
    "method.request.querystring.policyNumber"= true
    "method.request.querystring.dateOfLoss"= true
    "method.request.querystring.vin"= true
  }
}

resource "aws_api_gateway_integration" "DEV02_Uniban_Get" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id             = "${aws_api_gateway_resource.DEV02_Uniban_Get.id}"
  http_method             = "${aws_api_gateway_method.DEV02_Uniban_Get.http_method}"
  integration_http_method = "GET"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-dev02.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.dev02.gore.insurcloud.ca/uniban-service/v1/retrievePolicy"
}

resource "aws_api_gateway_method_response" "DEV02_Uniban_Get" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id = "${aws_api_gateway_resource.DEV02_Uniban_Get.id}"
  http_method = "${aws_api_gateway_method.DEV02_Uniban_Get.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}
###IBC Vehicle MicroService###

resource "aws_api_gateway_resource" "DEV02_IBC" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.DEV02_AUTH.id}"
  path_part   = "DEV02_IBC"
}

### IBC Vehicle Endpoint 1
resource "aws_api_gateway_resource" "DEV02_vininfo" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.DEV02_IBC.id}"
  path_part   = "DEV02_vininfo"
}

resource "aws_api_gateway_method" "DEV02_vininfo" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id   = "${aws_api_gateway_resource.DEV02_vininfo.id}"
  http_method   = "GET"
  authorization = "CUSTOM"
  authorizer_id = "${aws_api_gateway_authorizer.NonProd_Auth0_Vehicle.id}"
  request_parameters = {
    "method.request.querystring.vin"= true
  }
}

resource "aws_api_gateway_integration" "DEV02_vininfo" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id             = "${aws_api_gateway_resource.DEV02_vininfo.id}"
  http_method             = "${aws_api_gateway_method.DEV02_vininfo.http_method}"
  integration_http_method = "GET"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-dev02.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.dev02.gore.insurcloud.ca/vehicle-info-service/v1/vininfo"
}

resource "aws_api_gateway_method_response" "DEV02_vininfo" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id = "${aws_api_gateway_resource.DEV02_vininfo.id}"
  http_method = "${aws_api_gateway_method.DEV02_vininfo.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}
# IBC Vehicle Endpoint 2####
resource "aws_api_gateway_resource" "DEV02_submit" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.DEV02_IBC.id}"
  path_part   = "DEV02_submit"
}

resource "aws_api_gateway_method" "DEV02_submit" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id   = "${aws_api_gateway_resource.DEV02_submit.id}"
  http_method   = "POST"
  authorization = "CUSTOM"
  authorizer_id = "${aws_api_gateway_authorizer.NonProd_Auth0_Vehicle.id}"
}

resource "aws_api_gateway_integration" "DEV02_submit" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id             = "${aws_api_gateway_resource.DEV02_submit.id}"
  http_method             = "${aws_api_gateway_method.DEV02_submit.http_method}"
  integration_http_method = "POST"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-dev02.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.dev02.gore.insurcloud.ca/vehicle-info-service/v1/product/download/submit"
}

resource "aws_api_gateway_method_response" "DEV02_submit" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id = "${aws_api_gateway_resource.DEV02_submit.id}"
  http_method = "${aws_api_gateway_method.DEV02_submit.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}

# IBC Vehicle ppvinfo
resource "aws_api_gateway_resource" "DEV02_ppvinfo" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.DEV02_IBC.id}"
  path_part   = "DEV02_ppvinfo"
}

resource "aws_api_gateway_method" "DEV02_ppvinfo" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id   = "${aws_api_gateway_resource.DEV02_ppvinfo.id}"
  http_method   = "GET"
  authorization = "CUSTOM"
  authorizer_id = "${aws_api_gateway_authorizer.NonProd_Auth0_Vehicle.id}"
  request_parameters = {
    "method.request.querystring.rateGroupPublicationYear"= true
    "method.request.querystring.carCode"= true
    "method.request.querystring.modelYear"= true
  }
}

resource "aws_api_gateway_integration" "DEV02_ppvinfo" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id             = "${aws_api_gateway_resource.DEV02_ppvinfo.id}"
  http_method             = "${aws_api_gateway_method.DEV02_ppvinfo.http_method}"
  integration_http_method = "GET"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-dev02.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.dev02.gore.insurcloud.ca/vehicle-info-service/v1/ppvinfo"
}

resource "aws_api_gateway_method_response" "DEV02_ppvinfo" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id = "${aws_api_gateway_resource.DEV02_ppvinfo.id}"
  http_method = "${aws_api_gateway_method.DEV02_ppvinfo.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}

# IBC Vehicle recinfo
resource "aws_api_gateway_resource" "DEV02_recinfo" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.DEV02_IBC.id}"
  path_part   = "DEV02_recinfo"
}

resource "aws_api_gateway_method" "DEV02_recinfo" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id   = "${aws_api_gateway_resource.DEV02_recinfo.id}"
  http_method   = "GET"
  authorization = "CUSTOM"
  authorizer_id = "${aws_api_gateway_authorizer.NonProd_Auth0_Vehicle.id}"
  request_parameters = {
    "method.request.querystring.publicationYear"= true
    "method.request.querystring.vehicleType"= true
    "method.request.querystring.vehicleMake"= true
    "method.request.querystring.vehicleModel"= true
    "method.request.querystring.modelYear"= true
  }
}

resource "aws_api_gateway_integration" "DEV02_recinfo" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id             = "${aws_api_gateway_resource.DEV02_recinfo.id}"
  http_method             = "${aws_api_gateway_method.DEV02_recinfo.http_method}"
  integration_http_method = "GET"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-dev02.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.dev02.gore.insurcloud.ca/vehicle-info-service/v1/recinfo"
}

resource "aws_api_gateway_method_response" "DEV02_recinfo" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id = "${aws_api_gateway_resource.DEV02_recinfo.id}"
  http_method = "${aws_api_gateway_method.DEV02_recinfo.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}

######### RESOURCE GROUPS ##############
#########QA01#########
resource "aws_api_gateway_resource" "QA01_AUTH" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_rest_api.GORE_AUTH0.root_resource_id}"
  path_part   = "QA01_AUTH"
}
#####Address Validation#####
resource "aws_api_gateway_resource" "QA01_Address" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.QA01_AUTH.id}"
  path_part   = "QA01_Address"
}

# Address Outbound GW -API Gateway - ARMS
resource "aws_api_gateway_method" "QA01_Address" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id   = "${aws_api_gateway_resource.QA01_Address.id}"
  http_method   = "POST"
  authorization = "CUSTOM"
  authorizer_id = "${aws_api_gateway_authorizer.NonProd_Auth0_AddressValidation.id}"
}

resource "aws_api_gateway_integration" "QA01_Address" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id             = "${aws_api_gateway_resource.QA01_Address.id}"
  http_method             = "${aws_api_gateway_method.QA01_Address.http_method}"
  integration_http_method = "POST"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-qa01.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.qa01.gore.insurcloud.ca/address-validation-service/microservices/addressvalidation/ws"
}

resource "aws_api_gateway_method_response" "QA01_Address" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id = "${aws_api_gateway_resource.QA01_Address.id}"
  http_method = "${aws_api_gateway_method.QA01_Address.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}
###HCAI###

resource "aws_api_gateway_resource" "QA01_HCAISecurity" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.QA01_AUTH.id}"
  path_part   = "QA01_HCAISecurity"
}

# HCAI Outbound GW -API Gateway - ARMS
resource "aws_api_gateway_method" "QA01_HCAISecurity" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id   = "${aws_api_gateway_resource.QA01_HCAISecurity.id}"
  http_method   = "POST"
  authorization = "CUSTOM"
  authorizer_id = "${aws_api_gateway_authorizer.NonProd_Auth0_HCAI.id}"
}

resource "aws_api_gateway_integration" "QA01_HCAISecurity" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id             = "${aws_api_gateway_resource.QA01_HCAISecurity.id}"
  http_method             = "${aws_api_gateway_method.QA01_HCAISecurity.http_method}"
  integration_http_method = "POST"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-qa01.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.qa01.gore.insurcloud.ca/soap-security-service/v1/proxy"
}

resource "aws_api_gateway_method_response" "QA01_HCAISecurity" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id = "${aws_api_gateway_resource.QA01_HCAISecurity.id}"
  http_method = "${aws_api_gateway_method.QA01_HCAISecurity.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}

###Cheque Printing#####
resource "aws_api_gateway_resource" "QA01_ChequePrinting" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.QA01_AUTH.id}"
  path_part   = "QA01_ChequePrinting"
}
# #Cheque Outbound#####
resource "aws_api_gateway_resource" "QA01_SaveCheque" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.QA01_ChequePrinting.id}"
  path_part   = "QA01_SaveCheque"
}

resource "aws_api_gateway_method" "QA01_SaveCheque" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id   = "${aws_api_gateway_resource.QA01_SaveCheque.id}"
  http_method   = "POST"
  authorization = "CUSTOM"
  authorizer_id = "${aws_api_gateway_authorizer.NonProd_Auth0_ChequePrinting.id}"
}

resource "aws_api_gateway_integration" "QA01_SaveCheque" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id             = "${aws_api_gateway_resource.QA01_SaveCheque.id}"
  http_method             = "${aws_api_gateway_method.QA01_SaveCheque.http_method}"
  integration_http_method = "POST"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-qa01.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.qa01.gore.insurcloud.ca/cheque-service/v1/cheque/print"
}

resource "aws_api_gateway_method_response" "QA01_SaveCheque" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id = "${aws_api_gateway_resource.QA01_SaveCheque.id}"
  http_method = "${aws_api_gateway_method.QA01_SaveCheque.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}
# #Cheque --> F & 0 --> MS#####
resource "aws_api_gateway_resource" "QA01_ChequeUpdateNumbers" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.QA01_ChequePrinting.id}"
  path_part   = "QA01_ChequeUpdateNumbers"
}

resource "aws_api_gateway_method" "QA01_ChequeUpdateNumbers" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id   = "${aws_api_gateway_resource.QA01_ChequeUpdateNumbers.id}"
  http_method   = "POST"
  authorization = "CUSTOM"
  authorizer_id = "${aws_api_gateway_authorizer.NonProd_Auth0_ChequePrinting.id}"
}

resource "aws_api_gateway_integration" "QA01_ChequeUpdateNumbers" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id             = "${aws_api_gateway_resource.QA01_ChequeUpdateNumbers.id}"
  http_method             = "${aws_api_gateway_method.QA01_ChequeUpdateNumbers.http_method}"
  integration_http_method = "POST"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-qa01.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.qa01.gore.insurcloud.ca/cheque-service/v1/cheque/updateChequeNumbers"
}

resource "aws_api_gateway_method_response" "QA01_ChequeUpdateNumbers" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id = "${aws_api_gateway_resource.QA01_ChequeUpdateNumbers.id}"
  http_method = "${aws_api_gateway_method.QA01_ChequeUpdateNumbers.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}

# #MVR Service#####
resource "aws_api_gateway_resource" "QA01_MVRDriverInfo" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.QA01_AUTH.id}"
  path_part   = "QA01_MVRDriverInfo"
}

resource "aws_api_gateway_method" "QA01_MVRDriverInfo" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id   = "${aws_api_gateway_resource.QA01_MVRDriverInfo.id}"
  http_method   = "POST"
  authorization = "CUSTOM"
  authorizer_id = "${aws_api_gateway_authorizer.NonProd_Auth0_MVR.id}"
}

resource "aws_api_gateway_integration" "QA01_MVRDriverInfo" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id             = "${aws_api_gateway_resource.QA01_MVRDriverInfo.id}"
  http_method             = "${aws_api_gateway_method.QA01_MVRDriverInfo.http_method}"
  integration_http_method = "POST"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-qa01.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.qa01.gore.insurcloud.ca/mvr-service/v1/driver/info"
}

resource "aws_api_gateway_method_response" "QA01_MVRDriverInfo" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id = "${aws_api_gateway_resource.QA01_MVRDriverInfo.id}"
  http_method = "${aws_api_gateway_method.QA01_MVRDriverInfo.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}

#######Download service#####
resource "aws_api_gateway_resource" "QA01_BMSDownload" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.QA01_AUTH.id}"
  path_part   = "QA01_BMSDownload"
}

# #Download Service for PC payload#####
resource "aws_api_gateway_resource" "QA01_BMSDownload_PC" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.QA01_AUTH.id}"
  path_part   = "QA01_BMSDownload_PC"
}

resource "aws_api_gateway_method" "QA01_BMSDownload_PC" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id   = "${aws_api_gateway_resource.QA01_BMSDownload_PC.id}"
  http_method   = "POST"
  authorization = "CUSTOM"
  authorizer_id = "${aws_api_gateway_authorizer.NonProd_Auth0_MVR.id}"
}

resource "aws_api_gateway_integration" "QA01_BMSDownload_PC" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id             = "${aws_api_gateway_resource.QA01_BMSDownload_PC.id}"
  http_method             = "${aws_api_gateway_method.QA01_BMSDownload_PC.http_method}"
  integration_http_method = "POST"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-qa01.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.qa01.gore.insurcloud.ca/bms-download-service/v1/bmsdownload/pcpayload"
}

resource "aws_api_gateway_method_response" "QA01_BMSDownload_PC" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id = "${aws_api_gateway_resource.QA01_BMSDownload_PC.id}"
  http_method = "${aws_api_gateway_method.QA01_BMSDownload_PC.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}

# #Download Service for BC payload#####
resource "aws_api_gateway_resource" "QA01_BMSDownload_BC" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.QA01_BMSDownload.id}"
  path_part   = "QA01_BMSDownload_BC"
}

resource "aws_api_gateway_method" "QA01_BMSDownload_BC" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id   = "${aws_api_gateway_resource.QA01_BMSDownload_BC.id}"
  http_method   = "POST"
  authorization = "CUSTOM"
  authorizer_id = "${aws_api_gateway_authorizer.NonProd_Auth0_MVR.id}"
}

resource "aws_api_gateway_integration" "QA01_BMSDownload_BC" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id             = "${aws_api_gateway_resource.QA01_BMSDownload_BC.id}"
  http_method             = "${aws_api_gateway_method.QA01_BMSDownload_BC.http_method}"
  integration_http_method = "POST"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-qa01.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.qa01.gore.insurcloud.ca/bms-download-service/v1/bmsdownload/bcpayload"
}

resource "aws_api_gateway_method_response" "QA01_BMSDownload_BC" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id = "${aws_api_gateway_resource.QA01_BMSDownload_BC.id}"
  http_method = "${aws_api_gateway_method.QA01_BMSDownload_BC.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}

###Document Service###

resource "aws_api_gateway_resource" "QA01_Document" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.QA01_AUTH.id}"
  path_part   = "QA01_Document"
}

# Document Search
resource "aws_api_gateway_resource" "QA01_Document_Search" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.QA01_Document.id}"
  path_part   = "QA01_Document_Search"
}

resource "aws_api_gateway_method" "QA01_Document_Search" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id   = "${aws_api_gateway_resource.QA01_Document_Search.id}"
  http_method   = "POST"
  authorization = "CUSTOM"
  authorizer_id = "${aws_api_gateway_authorizer.NonProd_Auth0_HCAI.id}"
}

resource "aws_api_gateway_integration" "QA01_Document_Search" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id             = "${aws_api_gateway_resource.QA01_Document_Search.id}"
  http_method             = "${aws_api_gateway_method.QA01_Document_Search.http_method}"
  integration_http_method = "POST"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-qa01.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.qa01.gore.insurcloud.ca/document-service/v1/document/gore/summaries/search/metadata"
}

resource "aws_api_gateway_method_response" "QA01_Document_Search" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id = "${aws_api_gateway_resource.QA01_Document_Search.id}"
  http_method = "${aws_api_gateway_method.QA01_Document_Search.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}
# Document Retrieve
resource "aws_api_gateway_resource" "QA01_Document_Retrieve" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.QA01_Document.id}"
  path_part   = "QA01_Document_Retrieve"
}

resource "aws_api_gateway_method" "QA01_Document_Retrieve" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id   = "${aws_api_gateway_resource.QA01_Document_Retrieve.id}"
  http_method   = "POST"
  authorization = "CUSTOM"
  authorizer_id = "${aws_api_gateway_authorizer.NonProd_Auth0_HCAI.id}"
}

resource "aws_api_gateway_integration" "QA01_Document_Retrieve" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id             = "${aws_api_gateway_resource.QA01_Document_Retrieve.id}"
  http_method             = "${aws_api_gateway_method.QA01_Document_Retrieve.http_method}"
  integration_http_method = "POST"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-qa01.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.qa01.gore.insurcloud.ca/document-service/v1/document/gore/retrieve/key"
}

resource "aws_api_gateway_method_response" "QA01_Document_Retrieve" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id = "${aws_api_gateway_resource.QA01_Document_Retrieve.id}"
  http_method = "${aws_api_gateway_method.QA01_Document_Retrieve.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}

# Document Upload1
resource "aws_api_gateway_resource" "QA01_Document_Upload1" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.QA01_Document.id}"
  path_part   = "QA01_Document_Upload1"
}

resource "aws_api_gateway_method" "QA01_Document_Upload1" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id   = "${aws_api_gateway_resource.QA01_Document_Upload1.id}"
  http_method   = "POST"
  authorization = "CUSTOM"
  authorizer_id = "${aws_api_gateway_authorizer.NonProd_Auth0_HCAI.id}"
}

resource "aws_api_gateway_integration" "QA01_Document_Upload1" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id             = "${aws_api_gateway_resource.QA01_Document_Upload1.id}"
  http_method             = "${aws_api_gateway_method.QA01_Document_Upload1.http_method}"
  integration_http_method = "POST"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-qa01.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.qa01.gore.insurcloud.ca/document-service/v1/document/gore/base64/compose"
}

resource "aws_api_gateway_method_response" "QA01_Document_Upload1" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id = "${aws_api_gateway_resource.QA01_Document_Upload1.id}"
  http_method = "${aws_api_gateway_method.QA01_Document_Upload1.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}

# Document Upload2
resource "aws_api_gateway_resource" "QA01_Document_Upload2" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.QA01_Document.id}"
  path_part   = "QA01_Document_Upload2"
}

resource "aws_api_gateway_method" "QA01_Document_Upload2" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id   = "${aws_api_gateway_resource.QA01_Document_Upload2.id}"
  http_method   = "POST"
  authorization = "CUSTOM"
  authorizer_id = "${aws_api_gateway_authorizer.NonProd_Auth0_HCAI.id}"
}

resource "aws_api_gateway_integration" "QA01_Document_Upload2" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id             = "${aws_api_gateway_resource.QA01_Document_Upload2.id}"
  http_method             = "${aws_api_gateway_method.QA01_Document_Upload2.http_method}"
  integration_http_method = "POST"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-qa01.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.qa01.gore.insurcloud.ca/document-service/v1/document/gore/save/document/payload"
}

resource "aws_api_gateway_method_response" "QA01_Document_Upload2" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id = "${aws_api_gateway_resource.QA01_Document_Upload2.id}"
  http_method = "${aws_api_gateway_method.QA01_Document_Upload2.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}
###CRC###

resource "aws_api_gateway_resource" "QA01_CRC" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.QA01_AUTH.id}"
  path_part   = "QA01_CRC"
}
resource "aws_api_gateway_resource" "QA01_CRCCreateFNOL" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.QA01_CRC.id}"
  path_part   = "QA01_CRCCreateFNOL"
}


#CRC --> Guidewire
resource "aws_api_gateway_method" "QA01_CRCCreateFNOL" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id   = "${aws_api_gateway_resource.QA01_CRCCreateFNOL.id}"
  http_method   = "POST"
  authorization = "CUSTOM"
  authorizer_id = "${aws_api_gateway_authorizer.NonProd_Auth0_CRC.id}"
}

resource "aws_api_gateway_integration" "QA01_CRCCreateFNOL" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id             = "${aws_api_gateway_resource.QA01_CRCCreateFNOL.id}"
  http_method             = "${aws_api_gateway_method.QA01_CRCCreateFNOL.http_method}"
  integration_http_method = "POST"
  connection_type         = "INTERNET"
  type                    = "HTTP_PROXY"
  uri                     = "https://qa.goredev.guidewire.net/cc/ws/ic/integration/crc/CollisionReportingCenterAPI"
}

resource "aws_api_gateway_method_response" "QA01_CRCCreateFNOL" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id = "${aws_api_gateway_resource.QA01_CRCCreateFNOL.id}"
  http_method = "${aws_api_gateway_method.QA01_CRCCreateFNOL.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}

#CRC --> MS 

resource "aws_api_gateway_resource" "QA01_CRCCollisionDocumentsMS" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.QA01_CRC.id}"
  path_part   = "QA01_CRCCollisionDocumentsMS"
}

resource "aws_api_gateway_method" "QA01_CRCCollisionDocumentsMS" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id   = "${aws_api_gateway_resource.QA01_CRCCollisionDocumentsMS.id}"
  http_method   = "POST"
  authorization = "CUSTOM"
  authorizer_id = "${aws_api_gateway_authorizer.NonProd_Auth0_CRC.id}"
}

resource "aws_api_gateway_integration" "QA01_CRCCollisionDocumentsMS" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id             = "${aws_api_gateway_resource.QA01_CRCCollisionDocumentsMS.id}"
  http_method             = "${aws_api_gateway_method.QA01_CRCCollisionDocumentsMS.http_method}"
  integration_http_method = "POST"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-qa01.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.qa01.gore.insurcloud.ca/crc-service/microservices/crc/ws/"
}

resource "aws_api_gateway_method_response" "QA01_CRCCollisionDocumentsMS" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id = "${aws_api_gateway_resource.QA01_CRCCollisionDocumentsMS.id}"
  http_method = "${aws_api_gateway_method.QA01_CRCCollisionDocumentsMS.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}

####Audatex###

# Audatex Inbount Acknowledgment - ARMS

resource "aws_api_gateway_resource" "QA01_AudatexINB" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.QA01_AUTH.id}"
  path_part   = "QA01_AudatexINB"
}
resource "aws_api_gateway_resource" "QA01_AudatexAssignmentAck" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.QA01_AudatexINB.id}"
  path_part   = "QA01_AudatexAssignmentAck"
}

resource "aws_api_gateway_method" "QA01_AudatexAssignmentAck" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id   = "${aws_api_gateway_resource.QA01_AudatexAssignmentAck.id}"
  http_method   = "POST"
  authorization = "CUSTOM"
  authorizer_id = "${aws_api_gateway_authorizer.NonProd_Auth0_Audatex.id}"
}

resource "aws_api_gateway_integration" "QA01_AudatexAssignmentAck" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id             = "${aws_api_gateway_resource.QA01_AudatexAssignmentAck.id}"
  http_method             = "${aws_api_gateway_method.QA01_AudatexAssignmentAck.http_method}"
  integration_http_method = "POST"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-qa01.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.qa01.gore.insurcloud.ca/audatex-service/v1/assignment/ack"
}

resource "aws_api_gateway_method_response" "QA01_AudatexAssignmentAck" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id = "${aws_api_gateway_resource.QA01_AudatexAssignmentAck.id}"
  http_method = "${aws_api_gateway_method.QA01_AudatexAssignmentAck.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}

##Audatex Inbound estimate
resource "aws_api_gateway_resource" "QA01_AudatexEstimate" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.QA01_AudatexINB.id}"
  path_part   = "QA01_AudatexEstimate"
}

resource "aws_api_gateway_method" "QA01_AudatexEstimate" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id   = "${aws_api_gateway_resource.QA01_AudatexEstimate.id}"
  http_method   = "POST"
  authorization = "CUSTOM"
  authorizer_id = "${aws_api_gateway_authorizer.NonProd_Auth0_Audatex.id}"
}


resource "aws_api_gateway_integration" "QA01_AudatexEstimate" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id             = "${aws_api_gateway_resource.QA01_AudatexEstimate.id}"
  http_method             = "${aws_api_gateway_method.QA01_AudatexEstimate.http_method}"
  integration_http_method = "POST"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-qa01.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.qa01.gore.insurcloud.ca/audatex-service/v1/estimate"
}

resource "aws_api_gateway_method_response" "QA01_AudatexEstimate" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id = "${aws_api_gateway_resource.QA01_AudatexEstimate.id}"
  http_method = "${aws_api_gateway_method.QA01_AudatexEstimate.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}
####end of audatex#####


# ###Uniban###
resource "aws_api_gateway_resource" "QA01_Uniban_Get" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.QA01_AUTH.id}"
  path_part   = "QA01_Uniban_Get"
}

##Uniban --> API
resource "aws_api_gateway_method" "QA01_Uniban_Get" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id   = "${aws_api_gateway_resource.QA01_Uniban_Get.id}"
  http_method   = "GET"
  authorization = "CUSTOM"
  authorizer_id = "${aws_api_gateway_authorizer.NonProd_Auth0_UnibanService.id}"
  request_parameters = {
    "method.request.querystring.policyNumber"= true
    "method.request.querystring.dateOfLoss"= true
    "method.request.querystring.vin"= true
  }
}

resource "aws_api_gateway_integration" "QA01_Uniban_Get" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id             = "${aws_api_gateway_resource.QA01_Uniban_Get.id}"
  http_method             = "${aws_api_gateway_method.QA01_Uniban_Get.http_method}"
  integration_http_method = "GET"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-qa01.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.qa01.gore.insurcloud.ca/uniban-service/v1/retrievePolicy"
}

resource "aws_api_gateway_method_response" "QA01_Uniban_Get" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id = "${aws_api_gateway_resource.QA01_Uniban_Get.id}"
  http_method = "${aws_api_gateway_method.QA01_Uniban_Get.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}

###IBC Vehicle MicroService###

resource "aws_api_gateway_resource" "QA01_IBC" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.QA01_AUTH.id}"
  path_part   = "QA01_IBC"
}

### IBC Vehicle Endpoint 1
resource "aws_api_gateway_resource" "QA01_vininfo" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.QA01_IBC.id}"
  path_part   = "QA01_vininfo"
}

resource "aws_api_gateway_method" "QA01_vininfo" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id   = "${aws_api_gateway_resource.QA01_vininfo.id}"
  http_method   = "GET"
  authorization = "CUSTOM"
  authorizer_id = "${aws_api_gateway_authorizer.NonProd_Auth0_Vehicle.id}"
  request_parameters = {
    "method.request.querystring.vin"= true
  }
}

resource "aws_api_gateway_integration" "QA01_vininfo" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id             = "${aws_api_gateway_resource.QA01_vininfo.id}"
  http_method             = "${aws_api_gateway_method.QA01_vininfo.http_method}"
  integration_http_method = "GET"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-qa01.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.qa01.gore.insurcloud.ca/vehicle-info-service/v1/vininfo"
}

resource "aws_api_gateway_method_response" "QA01_vininfo" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id = "${aws_api_gateway_resource.QA01_vininfo.id}"
  http_method = "${aws_api_gateway_method.QA01_vininfo.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}
# IBC Vehicle Endpoint 2####
resource "aws_api_gateway_resource" "QA01_submit" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.QA01_IBC.id}"
  path_part   = "QA01_submit"
}

resource "aws_api_gateway_method" "QA01_submit" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id   = "${aws_api_gateway_resource.QA01_submit.id}"
  http_method   = "POST"
  authorization = "CUSTOM"
  authorizer_id = "${aws_api_gateway_authorizer.NonProd_Auth0_Vehicle.id}"
}

resource "aws_api_gateway_integration" "QA01_submit" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id             = "${aws_api_gateway_resource.QA01_submit.id}"
  http_method             = "${aws_api_gateway_method.QA01_submit.http_method}"
  integration_http_method = "POST"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-qa01.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.qa01.gore.insurcloud.ca/vehicle-info-service/v1/product/download/submit"
}

resource "aws_api_gateway_method_response" "QA01_submit" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id = "${aws_api_gateway_resource.QA01_submit.id}"
  http_method = "${aws_api_gateway_method.QA01_submit.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}

# IBC Vehicle ppvinfo
resource "aws_api_gateway_resource" "QA01_ppvinfo" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.QA01_IBC.id}"
  path_part   = "QA01_ppvinfo"
}

resource "aws_api_gateway_method" "QA01_ppvinfo" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id   = "${aws_api_gateway_resource.QA01_ppvinfo.id}"
  http_method   = "GET"
  authorization = "CUSTOM"
  authorizer_id = "${aws_api_gateway_authorizer.NonProd_Auth0_Vehicle.id}"
  request_parameters = {
    "method.request.querystring.rateGroupPublicationYear"= true
    "method.request.querystring.carCode"= true
    "method.request.querystring.modelYear"= true
  }
}

resource "aws_api_gateway_integration" "QA01_ppvinfo" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id             = "${aws_api_gateway_resource.QA01_ppvinfo.id}"
  http_method             = "${aws_api_gateway_method.QA01_ppvinfo.http_method}"
  integration_http_method = "GET"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-qa01.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.qa01.gore.insurcloud.ca/vehicle-info-service/v1/ppvinfo"
}

resource "aws_api_gateway_method_response" "QA01_ppvinfo" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id = "${aws_api_gateway_resource.QA01_ppvinfo.id}"
  http_method = "${aws_api_gateway_method.QA01_ppvinfo.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}

# IBC Vehicle recinfo
resource "aws_api_gateway_resource" "QA01_recinfo" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.QA01_IBC.id}"
  path_part   = "QA01_recinfo"
}

resource "aws_api_gateway_method" "QA01_recinfo" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id   = "${aws_api_gateway_resource.QA01_recinfo.id}"
  http_method   = "GET"
  authorization = "CUSTOM"
  authorizer_id = "${aws_api_gateway_authorizer.NonProd_Auth0_Vehicle.id}"
  request_parameters = {
    "method.request.querystring.publicationYear"= true
    "method.request.querystring.vehicleType"= true
    "method.request.querystring.vehicleMake"= true
    "method.request.querystring.vehicleModel"= true
    "method.request.querystring.modelYear"= true
 }
}

resource "aws_api_gateway_integration" "QA01_recinfo" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id             = "${aws_api_gateway_resource.QA01_recinfo.id}"
  http_method             = "${aws_api_gateway_method.QA01_recinfo.http_method}"
  integration_http_method = "GET"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-qa01.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.qa01.gore.insurcloud.ca/vehicle-info-service/v1/recinfo"
}

resource "aws_api_gateway_method_response" "QA01_recinfo" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id = "${aws_api_gateway_resource.QA01_recinfo.id}"
  http_method = "${aws_api_gateway_method.QA01_recinfo.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}

######### RESOURCE GROUPS ##############
#########QA02#########
resource "aws_api_gateway_resource" "QA02_AUTH" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_rest_api.GORE_AUTH0.root_resource_id}"
  path_part   = "QA02_AUTH"
}
#####Address Validation#####
resource "aws_api_gateway_resource" "QA02_Address" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.QA02_AUTH.id}"
  path_part   = "QA02_Address"
}

# Address Outbound GW -API Gateway - ARMS
resource "aws_api_gateway_method" "QA02_Address" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id   = "${aws_api_gateway_resource.QA02_Address.id}"
  http_method   = "POST"
  authorization = "CUSTOM"
  authorizer_id = "${aws_api_gateway_authorizer.NonProd_Auth0_AddressValidation.id}"
}

resource "aws_api_gateway_integration" "QA02_Address" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id             = "${aws_api_gateway_resource.QA02_Address.id}"
  http_method             = "${aws_api_gateway_method.QA02_Address.http_method}"
  integration_http_method = "POST"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-qa02.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.qa02.gore.insurcloud.ca/address-validation-service/microservices/addressvalidation/ws"
}

resource "aws_api_gateway_method_response" "QA02_Address" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id = "${aws_api_gateway_resource.QA02_Address.id}"
  http_method = "${aws_api_gateway_method.QA02_Address.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}
###HCAI###

resource "aws_api_gateway_resource" "QA02_HCAISecurity" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.QA02_AUTH.id}"
  path_part   = "QA02_HCAISecurity"
}

# HCAI Outbound GW -API Gateway - ARMS
resource "aws_api_gateway_method" "QA02_HCAISecurity" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id   = "${aws_api_gateway_resource.QA02_HCAISecurity.id}"
  http_method   = "POST"
  authorization = "CUSTOM"
  authorizer_id = "${aws_api_gateway_authorizer.NonProd_Auth0_HCAI.id}"
}

resource "aws_api_gateway_integration" "QA02_HCAISecurity" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id             = "${aws_api_gateway_resource.QA02_HCAISecurity.id}"
  http_method             = "${aws_api_gateway_method.QA02_HCAISecurity.http_method}"
  integration_http_method = "POST"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-qa02.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.qa02.gore.insurcloud.ca/soap-security-service/v1/proxy"
}

resource "aws_api_gateway_method_response" "QA02_HCAISecurity" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id = "${aws_api_gateway_resource.QA02_HCAISecurity.id}"
  http_method = "${aws_api_gateway_method.QA02_HCAISecurity.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}

###Cheque Printing#####
resource "aws_api_gateway_resource" "QA02_ChequePrinting" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.QA02_AUTH.id}"
  path_part   = "QA02_ChequePrinting"
}
# #Cheque Outbound#####
resource "aws_api_gateway_resource" "QA02_SaveCheque" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.QA02_ChequePrinting.id}"
  path_part   = "QA02_SaveCheque"
}

resource "aws_api_gateway_method" "QA02_SaveCheque" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id   = "${aws_api_gateway_resource.QA02_SaveCheque.id}"
  http_method   = "POST"
  authorization = "CUSTOM"
  authorizer_id = "${aws_api_gateway_authorizer.NonProd_Auth0_ChequePrinting.id}"
}

resource "aws_api_gateway_integration" "QA02_SaveCheque" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id             = "${aws_api_gateway_resource.QA02_SaveCheque.id}"
  http_method             = "${aws_api_gateway_method.QA02_SaveCheque.http_method}"
  integration_http_method = "POST"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-qa02.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.qa02.gore.insurcloud.ca/cheque-service/v1/cheque/print"
}

resource "aws_api_gateway_method_response" "QA02_SaveCheque" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id = "${aws_api_gateway_resource.QA02_SaveCheque.id}"
  http_method = "${aws_api_gateway_method.QA02_SaveCheque.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}
# #Cheque --> F & 0 --> MS#####
resource "aws_api_gateway_resource" "QA02_ChequeUpdateNumbers" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.QA02_ChequePrinting.id}"
  path_part   = "QA02_ChequeUpdateNumbers"
}

resource "aws_api_gateway_method" "QA02_ChequeUpdateNumbers" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id   = "${aws_api_gateway_resource.QA02_ChequeUpdateNumbers.id}"
  http_method   = "POST"
  authorization = "CUSTOM"
  authorizer_id = "${aws_api_gateway_authorizer.NonProd_Auth0_ChequePrinting.id}"
}

resource "aws_api_gateway_integration" "QA02_ChequeUpdateNumbers" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id             = "${aws_api_gateway_resource.QA02_ChequeUpdateNumbers.id}"
  http_method             = "${aws_api_gateway_method.QA02_ChequeUpdateNumbers.http_method}"
  integration_http_method = "POST"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-qa02.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.qa02.gore.insurcloud.ca/cheque-service/v1/cheque/updateChequeNumbers"
}

resource "aws_api_gateway_method_response" "QA02_ChequeUpdateNumbers" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id = "${aws_api_gateway_resource.QA02_ChequeUpdateNumbers.id}"
  http_method = "${aws_api_gateway_method.QA02_ChequeUpdateNumbers.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}
# #MVR Service#####
resource "aws_api_gateway_resource" "QA02_MVRDriverInfo" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.QA02_AUTH.id}"
  path_part   = "QA02_MVRDriverInfo"
}

resource "aws_api_gateway_method" "QA02_MVRDriverInfo" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id   = "${aws_api_gateway_resource.QA02_MVRDriverInfo.id}"
  http_method   = "POST"
  authorization = "CUSTOM"
  authorizer_id = "${aws_api_gateway_authorizer.NonProd_Auth0_MVR.id}"
}

resource "aws_api_gateway_integration" "QA02_MVRDriverInfo" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id             = "${aws_api_gateway_resource.QA02_MVRDriverInfo.id}"
  http_method             = "${aws_api_gateway_method.QA02_MVRDriverInfo.http_method}"
  integration_http_method = "POST"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-qa02.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.qa02.gore.insurcloud.ca/mvr-service/v1/driver/info"
}

resource "aws_api_gateway_method_response" "QA02_MVRDriverInfo" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id = "${aws_api_gateway_resource.QA02_MVRDriverInfo.id}"
  http_method = "${aws_api_gateway_method.QA02_MVRDriverInfo.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}

#######Download service#####
resource "aws_api_gateway_resource" "QA02_BMSDownload" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.QA02_AUTH.id}"
  path_part   = "QA02_BMSDownload"
}

# #Download Service for PC payload#####
resource "aws_api_gateway_resource" "QA02_BMSDownload_PC" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.QA02_AUTH.id}"
  path_part   = "QA02_BMSDownload_PC"
}

resource "aws_api_gateway_method" "QA02_BMSDownload_PC" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id   = "${aws_api_gateway_resource.QA02_BMSDownload_PC.id}"
  http_method   = "POST"
  authorization = "CUSTOM"
  authorizer_id = "${aws_api_gateway_authorizer.NonProd_Auth0_MVR.id}"
}

resource "aws_api_gateway_integration" "QA02_BMSDownload_PC" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id             = "${aws_api_gateway_resource.QA02_BMSDownload_PC.id}"
  http_method             = "${aws_api_gateway_method.QA02_BMSDownload_PC.http_method}"
  integration_http_method = "POST"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-qa02.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.qa02.gore.insurcloud.ca/bms-download-service/v1/bmsdownload/pcpayload"
}

resource "aws_api_gateway_method_response" "QA02_BMSDownload_PC" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id = "${aws_api_gateway_resource.QA02_BMSDownload_PC.id}"
  http_method = "${aws_api_gateway_method.QA02_BMSDownload_PC.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}

# #Download Service for BC payload#####
resource "aws_api_gateway_resource" "QA02_BMSDownload_BC" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.QA02_BMSDownload.id}"
  path_part   = "QA02_BMSDownload_BC"
}

resource "aws_api_gateway_method" "QA02_BMSDownload_BC" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id   = "${aws_api_gateway_resource.QA02_BMSDownload_BC.id}"
  http_method   = "POST"
  authorization = "CUSTOM"
  authorizer_id = "${aws_api_gateway_authorizer.NonProd_Auth0_MVR.id}"
}

resource "aws_api_gateway_integration" "QA02_BMSDownload_BC" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id             = "${aws_api_gateway_resource.QA02_BMSDownload_BC.id}"
  http_method             = "${aws_api_gateway_method.QA02_BMSDownload_BC.http_method}"
  integration_http_method = "POST"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-qa02.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.qa02.gore.insurcloud.ca/bms-download-service/v1/bmsdownload/bcpayload"
}

resource "aws_api_gateway_method_response" "QA02_BMSDownload_BC" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id = "${aws_api_gateway_resource.QA02_BMSDownload_BC.id}"
  http_method = "${aws_api_gateway_method.QA02_BMSDownload_BC.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}

###Document Service###

resource "aws_api_gateway_resource" "QA02_Document" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.QA02_AUTH.id}"
  path_part   = "QA02_Document"
}

# Document Search
resource "aws_api_gateway_resource" "QA02_Document_Search" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.QA02_Document.id}"
  path_part   = "QA02_Document_Search"
}

resource "aws_api_gateway_method" "QA02_Document_Search" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id   = "${aws_api_gateway_resource.QA02_Document_Search.id}"
  http_method   = "POST"
  authorization = "CUSTOM"
  authorizer_id = "${aws_api_gateway_authorizer.NonProd_Auth0_HCAI.id}"
}

resource "aws_api_gateway_integration" "QA02_Document_Search" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id             = "${aws_api_gateway_resource.QA02_Document_Search.id}"
  http_method             = "${aws_api_gateway_method.QA02_Document_Search.http_method}"
  integration_http_method = "POST"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-qa02.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.qa02.gore.insurcloud.ca/document-service/v1/document/gore/summaries/search/metadata"
}

resource "aws_api_gateway_method_response" "QA02_Document_Search" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id = "${aws_api_gateway_resource.QA02_Document_Search.id}"
  http_method = "${aws_api_gateway_method.QA02_Document_Search.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}
# Document Retrieve
resource "aws_api_gateway_resource" "QA02_Document_Retrieve" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.QA02_Document.id}"
  path_part   = "QA02_Document_Retrieve"
}

resource "aws_api_gateway_method" "QA02_Document_Retrieve" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id   = "${aws_api_gateway_resource.QA02_Document_Retrieve.id}"
  http_method   = "POST"
  authorization = "CUSTOM"
  authorizer_id = "${aws_api_gateway_authorizer.NonProd_Auth0_HCAI.id}"
}

resource "aws_api_gateway_integration" "QA02_Document_Retrieve" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id             = "${aws_api_gateway_resource.QA02_Document_Retrieve.id}"
  http_method             = "${aws_api_gateway_method.QA02_Document_Retrieve.http_method}"
  integration_http_method = "POST"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-qa02.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.qa02.gore.insurcloud.ca/document-service/v1/document/gore/retrieve/key"
}

resource "aws_api_gateway_method_response" "QA02_Document_Retrieve" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id = "${aws_api_gateway_resource.QA02_Document_Retrieve.id}"
  http_method = "${aws_api_gateway_method.QA02_Document_Retrieve.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}

# Document Upload1
resource "aws_api_gateway_resource" "QA02_Document_Upload1" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.QA02_Document.id}"
  path_part   = "QA02_Document_Upload1"
}

resource "aws_api_gateway_method" "QA02_Document_Upload1" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id   = "${aws_api_gateway_resource.QA02_Document_Upload1.id}"
  http_method   = "POST"
  authorization = "CUSTOM"
  authorizer_id = "${aws_api_gateway_authorizer.NonProd_Auth0_HCAI.id}"
}

resource "aws_api_gateway_integration" "QA02_Document_Upload1" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id             = "${aws_api_gateway_resource.QA02_Document_Upload1.id}"
  http_method             = "${aws_api_gateway_method.QA02_Document_Upload1.http_method}"
  integration_http_method = "POST"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-qa02.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.qa02.gore.insurcloud.ca/document-service/v1/document/gore/base64/compose"
}

resource "aws_api_gateway_method_response" "QA02_Document_Upload1" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id = "${aws_api_gateway_resource.QA02_Document_Upload1.id}"
  http_method = "${aws_api_gateway_method.QA02_Document_Upload1.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}

# Document Upload2
resource "aws_api_gateway_resource" "QA02_Document_Upload2" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.QA02_Document.id}"
  path_part   = "QA02_Document_Upload2"
}

resource "aws_api_gateway_method" "QA02_Document_Upload2" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id   = "${aws_api_gateway_resource.QA02_Document_Upload2.id}"
  http_method   = "POST"
  authorization = "CUSTOM"
  authorizer_id = "${aws_api_gateway_authorizer.NonProd_Auth0_HCAI.id}"
}

resource "aws_api_gateway_integration" "QA02_Document_Upload2" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id             = "${aws_api_gateway_resource.QA02_Document_Upload2.id}"
  http_method             = "${aws_api_gateway_method.QA02_Document_Upload2.http_method}"
  integration_http_method = "POST"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-qa02.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.qa02.gore.insurcloud.ca/document-service/v1/document/gore/save/document/payload"
}

resource "aws_api_gateway_method_response" "QA02_Document_Upload2" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id = "${aws_api_gateway_resource.QA02_Document_Upload2.id}"
  http_method = "${aws_api_gateway_method.QA02_Document_Upload2.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}
###CRC###

resource "aws_api_gateway_resource" "QA02_CRC" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.QA02_AUTH.id}"
  path_part   = "QA02_CRC"
}
resource "aws_api_gateway_resource" "QA02_CRCCreateFNOL" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.QA02_CRC.id}"
  path_part   = "QA02_CRCCreateFNOL"
}


#CRC --> Guidewire
resource "aws_api_gateway_method" "QA02_CRCCreateFNOL" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id   = "${aws_api_gateway_resource.QA02_CRCCreateFNOL.id}"
  http_method   = "POST"
  authorization = "CUSTOM"
  authorizer_id = "${aws_api_gateway_authorizer.NonProd_Auth0_CRC.id}"
}

resource "aws_api_gateway_integration" "QA02_CRCCreateFNOL" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id             = "${aws_api_gateway_resource.QA02_CRCCreateFNOL.id}"
  http_method             = "${aws_api_gateway_method.QA02_CRCCreateFNOL.http_method}"
  integration_http_method = "POST"
  connection_type         = "INTERNET"
  type                    = "HTTP_PROXY"
  uri                     = "https://qa2.goredev.guidewire.net/cc/ws/ic/integration/crc/CollisionReportingCenterAPI"
}

resource "aws_api_gateway_method_response" "QA02_CRCCreateFNOL" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id = "${aws_api_gateway_resource.QA02_CRCCreateFNOL.id}"
  http_method = "${aws_api_gateway_method.QA02_CRCCreateFNOL.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}

#CRC --> MS 

resource "aws_api_gateway_resource" "QA02_CRCCollisionDocumentsMS" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.QA02_CRC.id}"
  path_part   = "QA02_CRCCollisionDocumentsMS"
}

resource "aws_api_gateway_method" "QA02_CRCCollisionDocumentsMS" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id   = "${aws_api_gateway_resource.QA02_CRCCollisionDocumentsMS.id}"
  http_method   = "POST"
  authorization = "CUSTOM"
  authorizer_id = "${aws_api_gateway_authorizer.NonProd_Auth0_CRC.id}"
}

resource "aws_api_gateway_integration" "QA02_CRCCollisionDocumentsMS" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id             = "${aws_api_gateway_resource.QA02_CRCCollisionDocumentsMS.id}"
  http_method             = "${aws_api_gateway_method.QA02_CRCCollisionDocumentsMS.http_method}"
  integration_http_method = "POST"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-qa02.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.qa02.gore.insurcloud.ca/crc-service/microservices/crc/ws/"
}

resource "aws_api_gateway_method_response" "QA02_CRCCollisionDocumentsMS" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id = "${aws_api_gateway_resource.QA02_CRCCollisionDocumentsMS.id}"
  http_method = "${aws_api_gateway_method.QA02_CRCCollisionDocumentsMS.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}

####Audatex###

# Audatex Inbount Acknowledgment - ARMS

resource "aws_api_gateway_resource" "QA02_AudatexINB" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.QA02_AUTH.id}"
  path_part   = "QA02_AudatexINB"
}
resource "aws_api_gateway_resource" "QA02_AudatexAssignmentAck" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.QA02_AudatexINB.id}"
  path_part   = "QA02_AudatexAssignmentAck"
}

resource "aws_api_gateway_method" "QA02_AudatexAssignmentAck" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id   = "${aws_api_gateway_resource.QA02_AudatexAssignmentAck.id}"
  http_method   = "POST"
  authorization = "CUSTOM"
  authorizer_id = "${aws_api_gateway_authorizer.NonProd_Auth0_Audatex.id}"
}

resource "aws_api_gateway_integration" "QA02_AudatexAssignmentAck" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id             = "${aws_api_gateway_resource.QA02_AudatexAssignmentAck.id}"
  http_method             = "${aws_api_gateway_method.QA02_AudatexAssignmentAck.http_method}"
  integration_http_method = "POST"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-qa02.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.qa02.gore.insurcloud.ca/audatex-service/v1/assignment/ack"
}

resource "aws_api_gateway_method_response" "QA02_AudatexAssignmentAck" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id = "${aws_api_gateway_resource.QA02_AudatexAssignmentAck.id}"
  http_method = "${aws_api_gateway_method.QA02_AudatexAssignmentAck.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}

##Audatex Inbound estimate
resource "aws_api_gateway_resource" "QA02_AudatexEstimate" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.QA02_AudatexINB.id}"
  path_part   = "QA02_AudatexEstimate"
}

resource "aws_api_gateway_method" "QA02_AudatexEstimate" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id   = "${aws_api_gateway_resource.QA02_AudatexEstimate.id}"
  http_method   = "POST"
  authorization = "CUSTOM"
  authorizer_id = "${aws_api_gateway_authorizer.NonProd_Auth0_Audatex.id}"
}


resource "aws_api_gateway_integration" "QA02_AudatexEstimate" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id             = "${aws_api_gateway_resource.QA02_AudatexEstimate.id}"
  http_method             = "${aws_api_gateway_method.QA02_AudatexEstimate.http_method}"
  integration_http_method = "POST"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-qa02.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.qa02.gore.insurcloud.ca/audatex-service/v1/estimate"
}

resource "aws_api_gateway_method_response" "QA02_AudatexEstimate" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id = "${aws_api_gateway_resource.QA02_AudatexEstimate.id}"
  http_method = "${aws_api_gateway_method.QA02_AudatexEstimate.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}
####end of audatex#####


# ###Uniban###
resource "aws_api_gateway_resource" "QA02_Uniban_Get" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.QA02_AUTH.id}"
  path_part   = "QA02_Uniban_Get"
}

##Uniban --> API
resource "aws_api_gateway_method" "QA02_Uniban_Get" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id   = "${aws_api_gateway_resource.QA02_Uniban_Get.id}"
  http_method   = "GET"
  authorization = "CUSTOM"
  authorizer_id = "${aws_api_gateway_authorizer.NonProd_Auth0_UnibanService.id}"
  request_parameters = {
    "method.request.querystring.policyNumber"= true
    "method.request.querystring.dateOfLoss"= true
    "method.request.querystring.vin"= true
  }
}

resource "aws_api_gateway_integration" "QA02_Uniban_Get" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id             = "${aws_api_gateway_resource.QA02_Uniban_Get.id}"
  http_method             = "${aws_api_gateway_method.QA02_Uniban_Get.http_method}"
  integration_http_method = "GET"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-qa02.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.qa02.gore.insurcloud.ca/uniban-service/v1/retrievePolicy"
}

resource "aws_api_gateway_method_response" "QA02_Uniban_Get" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id = "${aws_api_gateway_resource.QA02_Uniban_Get.id}"
  http_method = "${aws_api_gateway_method.QA02_Uniban_Get.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}

###IBC Vehicle MicroService###

resource "aws_api_gateway_resource" "QA02_IBC" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.QA02_AUTH.id}"
  path_part   = "QA02_IBC"
}

### IBC Vehicle Endpoint 1
resource "aws_api_gateway_resource" "QA02_vininfo" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.QA02_IBC.id}"
  path_part   = "QA02_vininfo"
}

resource "aws_api_gateway_method" "QA02_vininfo" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id   = "${aws_api_gateway_resource.QA02_vininfo.id}"
  http_method   = "GET"
  authorization = "CUSTOM"
  authorizer_id = "${aws_api_gateway_authorizer.NonProd_Auth0_Vehicle.id}"
  request_parameters = {
    "method.request.querystring.vin"= true
  }
}

resource "aws_api_gateway_integration" "QA02_vininfo" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id             = "${aws_api_gateway_resource.QA02_vininfo.id}"
  http_method             = "${aws_api_gateway_method.QA02_vininfo.http_method}"
  integration_http_method = "GET"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-qa02.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.qa02.gore.insurcloud.ca/vehicle-info-service/v1/vininfo"
}

resource "aws_api_gateway_method_response" "QA02_vininfo" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id = "${aws_api_gateway_resource.QA02_vininfo.id}"
  http_method = "${aws_api_gateway_method.QA02_vininfo.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}
# IBC Vehicle Endpoint 2####
resource "aws_api_gateway_resource" "QA02_submit" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.QA02_IBC.id}"
  path_part   = "QA02_submit"
}

resource "aws_api_gateway_method" "QA02_submit" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id   = "${aws_api_gateway_resource.QA02_submit.id}"
  http_method   = "POST"
  authorization = "CUSTOM"
  authorizer_id = "${aws_api_gateway_authorizer.NonProd_Auth0_Vehicle.id}"
}

resource "aws_api_gateway_integration" "QA02_submit" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id             = "${aws_api_gateway_resource.QA02_submit.id}"
  http_method             = "${aws_api_gateway_method.QA02_submit.http_method}"
  integration_http_method = "POST"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-qa02.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.qa02.gore.insurcloud.ca/vehicle-info-service/v1/product/download/submit"
}

resource "aws_api_gateway_method_response" "QA02_submit" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id = "${aws_api_gateway_resource.QA02_submit.id}"
  http_method = "${aws_api_gateway_method.QA02_submit.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}

# IBC Vehicle ppvinfo
resource "aws_api_gateway_resource" "QA02_ppvinfo" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.QA02_IBC.id}"
  path_part   = "QA02_ppvinfo"
}

resource "aws_api_gateway_method" "QA02_ppvinfo" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id   = "${aws_api_gateway_resource.QA02_ppvinfo.id}"
  http_method   = "GET"
  authorization = "CUSTOM"
  authorizer_id = "${aws_api_gateway_authorizer.NonProd_Auth0_Vehicle.id}"
  request_parameters = {
    "method.request.querystring.rateGroupPublicationYear"= true
    "method.request.querystring.carCode"= true
    "method.request.querystring.modelYear"= true
  }
}

resource "aws_api_gateway_integration" "QA02_ppvinfo" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id             = "${aws_api_gateway_resource.QA02_ppvinfo.id}"
  http_method             = "${aws_api_gateway_method.QA02_ppvinfo.http_method}"
  integration_http_method = "GET"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-qa02.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.qa02.gore.insurcloud.ca/vehicle-info-service/v1/ppvinfo"
}

resource "aws_api_gateway_method_response" "QA02_ppvinfo" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id = "${aws_api_gateway_resource.QA02_ppvinfo.id}"
  http_method = "${aws_api_gateway_method.QA02_ppvinfo.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}

# IBC Vehicle recinfo
resource "aws_api_gateway_resource" "QA02_recinfo" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.QA02_IBC.id}"
  path_part   = "QA02_recinfo"
}

resource "aws_api_gateway_method" "QA02_recinfo" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id   = "${aws_api_gateway_resource.QA02_recinfo.id}"
  http_method   = "GET"
  authorization = "CUSTOM"
  authorizer_id = "${aws_api_gateway_authorizer.NonProd_Auth0_Vehicle.id}"
  request_parameters = {
    "method.request.querystring.publicationYear"= true
    "method.request.querystring.vehicleType"= true
    "method.request.querystring.vehicleMake"= true
    "method.request.querystring.vehicleModel"= true
    "method.request.querystring.modelYear"= true
 }
}

resource "aws_api_gateway_integration" "QA02_recinfo" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id             = "${aws_api_gateway_resource.QA02_recinfo.id}"
  http_method             = "${aws_api_gateway_method.QA02_recinfo.http_method}"
  integration_http_method = "GET"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-qa02.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.qa02.gore.insurcloud.ca/vehicle-info-service/v1/recinfo"
}

resource "aws_api_gateway_method_response" "QA02_recinfo" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id = "${aws_api_gateway_resource.QA02_recinfo.id}"
  http_method = "${aws_api_gateway_method.QA02_recinfo.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}

######### RESOURCE GROUPS ##############
#########DEV03#########
resource "aws_api_gateway_resource" "DEV03_AUTH" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_rest_api.GORE_AUTH0.root_resource_id}"
  path_part   = "DEV03_AUTH"
}
#####Address Validation#####
resource "aws_api_gateway_resource" "DEV03_Address" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.DEV03_AUTH.id}"
  path_part   = "DEV03_Address"
}

# Address Outbound GW -API Gateway - ARMS
resource "aws_api_gateway_method" "DEV03_Address" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id   = "${aws_api_gateway_resource.DEV03_Address.id}"
  http_method   = "POST"
  authorization = "CUSTOM"
  authorizer_id = "${aws_api_gateway_authorizer.NonProd_Auth0_AddressValidation.id}"
}

resource "aws_api_gateway_integration" "DEV03_Address" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id             = "${aws_api_gateway_resource.DEV03_Address.id}"
  http_method             = "${aws_api_gateway_method.DEV03_Address.http_method}"
  integration_http_method = "POST"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-dev03.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.dev03.gore.insurcloud.ca/address-validation-service/microservices/addressvalidation/ws"
}

resource "aws_api_gateway_method_response" "DEV03_Address" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id = "${aws_api_gateway_resource.DEV03_Address.id}"
  http_method = "${aws_api_gateway_method.DEV03_Address.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}
###HCAI###

resource "aws_api_gateway_resource" "DEV03_HCAISecurity" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.DEV03_AUTH.id}"
  path_part   = "DEV03_HCAISecurity"
}

# HCAI Outbound GW -API Gateway - ARMS
resource "aws_api_gateway_method" "DEV03_HCAISecurity" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id   = "${aws_api_gateway_resource.DEV03_HCAISecurity.id}"
  http_method   = "POST"
  authorization = "CUSTOM"
  authorizer_id = "${aws_api_gateway_authorizer.NonProd_Auth0_HCAI.id}"
}

resource "aws_api_gateway_integration" "DEV03_HCAISecurity" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id             = "${aws_api_gateway_resource.DEV03_HCAISecurity.id}"
  http_method             = "${aws_api_gateway_method.DEV03_HCAISecurity.http_method}"
  integration_http_method = "POST"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-dev03.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.dev03.gore.insurcloud.ca/soap-security-service/v1/proxy"
}

resource "aws_api_gateway_method_response" "DEV03_HCAISecurity" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id = "${aws_api_gateway_resource.DEV03_HCAISecurity.id}"
  http_method = "${aws_api_gateway_method.DEV03_HCAISecurity.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}

###Cheque Printing#####
resource "aws_api_gateway_resource" "DEV03_ChequePrinting" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.DEV03_AUTH.id}"
  path_part   = "DEV03_ChequePrinting"
}
# #Cheque Outbound#####
resource "aws_api_gateway_resource" "DEV03_SaveCheque" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.DEV03_ChequePrinting.id}"
  path_part   = "DEV03_SaveCheque"
}

resource "aws_api_gateway_method" "DEV03_SaveCheque" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id   = "${aws_api_gateway_resource.DEV03_SaveCheque.id}"
  http_method   = "POST"
  authorization = "CUSTOM"
  authorizer_id = "${aws_api_gateway_authorizer.NonProd_Auth0_ChequePrinting.id}"
}

resource "aws_api_gateway_integration" "DEV03_SaveCheque" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id             = "${aws_api_gateway_resource.DEV03_SaveCheque.id}"
  http_method             = "${aws_api_gateway_method.DEV03_SaveCheque.http_method}"
  integration_http_method = "POST"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-dev03.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.dev03.gore.insurcloud.ca/cheque-service/v1/cheque/print"
}

resource "aws_api_gateway_method_response" "DEV03_SaveCheque" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id = "${aws_api_gateway_resource.DEV03_SaveCheque.id}"
  http_method = "${aws_api_gateway_method.DEV03_SaveCheque.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}
# #Cheque --> F & 0 --> MS#####
resource "aws_api_gateway_resource" "DEV03_ChequeUpdateNumbers" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.DEV03_ChequePrinting.id}"
  path_part   = "DEV03_ChequeUpdateNumbers"
}

resource "aws_api_gateway_method" "DEV03_ChequeUpdateNumbers" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id   = "${aws_api_gateway_resource.DEV03_ChequeUpdateNumbers.id}"
  http_method   = "POST"
  authorization = "CUSTOM"
  authorizer_id = "${aws_api_gateway_authorizer.NonProd_Auth0_ChequePrinting.id}"
}

resource "aws_api_gateway_integration" "DEV03_ChequeUpdateNumbers" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id             = "${aws_api_gateway_resource.DEV03_ChequeUpdateNumbers.id}"
  http_method             = "${aws_api_gateway_method.DEV03_ChequeUpdateNumbers.http_method}"
  integration_http_method = "POST"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-dev03.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.dev03.gore.insurcloud.ca/cheque-service/v1/cheque/updateChequeNumbers"
}

resource "aws_api_gateway_method_response" "DEV03_ChequeUpdateNumbers" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id = "${aws_api_gateway_resource.DEV03_ChequeUpdateNumbers.id}"
  http_method = "${aws_api_gateway_method.DEV03_ChequeUpdateNumbers.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}
# #MVR Service#####
resource "aws_api_gateway_resource" "DEV03_MVRDriverInfo" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.DEV03_AUTH.id}"
  path_part   = "DEV03_MVRDriverInfo"
}

resource "aws_api_gateway_method" "DEV03_MVRDriverInfo" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id   = "${aws_api_gateway_resource.DEV03_MVRDriverInfo.id}"
  http_method   = "POST"
  authorization = "CUSTOM"
  authorizer_id = "${aws_api_gateway_authorizer.NonProd_Auth0_MVR.id}"
}

resource "aws_api_gateway_integration" "DEV03_MVRDriverInfo" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id             = "${aws_api_gateway_resource.DEV03_MVRDriverInfo.id}"
  http_method             = "${aws_api_gateway_method.DEV03_MVRDriverInfo.http_method}"
  integration_http_method = "POST"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-dev03.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.dev03.gore.insurcloud.ca/mvr-service/v1/driver/info"
}

resource "aws_api_gateway_method_response" "DEV03_MVRDriverInfo" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id = "${aws_api_gateway_resource.DEV03_MVRDriverInfo.id}"
  http_method = "${aws_api_gateway_method.DEV03_MVRDriverInfo.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}

#######Download service#####
resource "aws_api_gateway_resource" "DEV03_BMSDownload" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.DEV03_AUTH.id}"
  path_part   = "DEV03_BMSDownload"
}

# #Download Service for PC payload#####
resource "aws_api_gateway_resource" "DEV03_BMSDownload_PC" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.DEV03_AUTH.id}"
  path_part   = "DEV03_BMSDownload_PC"
}

resource "aws_api_gateway_method" "DEV03_BMSDownload_PC" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id   = "${aws_api_gateway_resource.DEV03_BMSDownload_PC.id}"
  http_method   = "POST"
  authorization = "CUSTOM"
  authorizer_id = "${aws_api_gateway_authorizer.NonProd_Auth0_MVR.id}"
}

resource "aws_api_gateway_integration" "DEV03_BMSDownload_PC" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id             = "${aws_api_gateway_resource.DEV03_BMSDownload_PC.id}"
  http_method             = "${aws_api_gateway_method.DEV03_BMSDownload_PC.http_method}"
  integration_http_method = "POST"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-dev03.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.dev03.gore.insurcloud.ca/bms-download-service/v1/bmsdownload/pcpayload"
}

resource "aws_api_gateway_method_response" "DEV03_BMSDownload_PC" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id = "${aws_api_gateway_resource.DEV03_BMSDownload_PC.id}"
  http_method = "${aws_api_gateway_method.DEV03_BMSDownload_PC.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}

# #Download Service for BC payload#####
resource "aws_api_gateway_resource" "DEV03_BMSDownload_BC" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.DEV03_BMSDownload.id}"
  path_part   = "DEV03_BMSDownload_BC"
}

resource "aws_api_gateway_method" "DEV03_BMSDownload_BC" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id   = "${aws_api_gateway_resource.DEV03_BMSDownload_BC.id}"
  http_method   = "POST"
  authorization = "CUSTOM"
  authorizer_id = "${aws_api_gateway_authorizer.NonProd_Auth0_MVR.id}"
}

resource "aws_api_gateway_integration" "DEV03_BMSDownload_BC" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id             = "${aws_api_gateway_resource.DEV03_BMSDownload_BC.id}"
  http_method             = "${aws_api_gateway_method.DEV03_BMSDownload_BC.http_method}"
  integration_http_method = "POST"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-dev03.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.dev03.gore.insurcloud.ca/bms-download-service/v1/bmsdownload/bcpayload"
}

resource "aws_api_gateway_method_response" "DEV03_BMSDownload_BC" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id = "${aws_api_gateway_resource.DEV03_BMSDownload_BC.id}"
  http_method = "${aws_api_gateway_method.DEV03_BMSDownload_BC.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}

###Document Service###

resource "aws_api_gateway_resource" "DEV03_Document" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.DEV03_AUTH.id}"
  path_part   = "DEV03_Document"
}

# Document Search
resource "aws_api_gateway_resource" "DEV03_Document_Search" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.DEV03_Document.id}"
  path_part   = "DEV03_Document_Search"
}

resource "aws_api_gateway_method" "DEV03_Document_Search" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id   = "${aws_api_gateway_resource.DEV03_Document_Search.id}"
  http_method   = "POST"
  authorization = "CUSTOM"
  authorizer_id = "${aws_api_gateway_authorizer.NonProd_Auth0_HCAI.id}"
}

resource "aws_api_gateway_integration" "DEV03_Document_Search" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id             = "${aws_api_gateway_resource.DEV03_Document_Search.id}"
  http_method             = "${aws_api_gateway_method.DEV03_Document_Search.http_method}"
  integration_http_method = "POST"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-dev03.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.dev03.gore.insurcloud.ca/document-service/v1/document/gore/summaries/search/metadata"
}

resource "aws_api_gateway_method_response" "DEV03_Document_Search" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id = "${aws_api_gateway_resource.DEV03_Document_Search.id}"
  http_method = "${aws_api_gateway_method.DEV03_Document_Search.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}
# Document Retrieve
resource "aws_api_gateway_resource" "DEV03_Document_Retrieve" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.DEV03_Document.id}"
  path_part   = "DEV03_Document_Retrieve"
}

resource "aws_api_gateway_method" "DEV03_Document_Retrieve" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id   = "${aws_api_gateway_resource.DEV03_Document_Retrieve.id}"
  http_method   = "POST"
  authorization = "CUSTOM"
  authorizer_id = "${aws_api_gateway_authorizer.NonProd_Auth0_HCAI.id}"
}

resource "aws_api_gateway_integration" "DEV03_Document_Retrieve" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id             = "${aws_api_gateway_resource.DEV03_Document_Retrieve.id}"
  http_method             = "${aws_api_gateway_method.DEV03_Document_Retrieve.http_method}"
  integration_http_method = "POST"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-dev03.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.dev03.gore.insurcloud.ca/document-service/v1/document/gore/retrieve/key"
}

resource "aws_api_gateway_method_response" "DEV03_Document_Retrieve" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id = "${aws_api_gateway_resource.DEV03_Document_Retrieve.id}"
  http_method = "${aws_api_gateway_method.DEV03_Document_Retrieve.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}

# Document Upload1
resource "aws_api_gateway_resource" "DEV03_Document_Upload1" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.DEV03_Document.id}"
  path_part   = "DEV03_Document_Upload1"
}

resource "aws_api_gateway_method" "DEV03_Document_Upload1" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id   = "${aws_api_gateway_resource.DEV03_Document_Upload1.id}"
  http_method   = "POST"
  authorization = "CUSTOM"
  authorizer_id = "${aws_api_gateway_authorizer.NonProd_Auth0_HCAI.id}"
}

resource "aws_api_gateway_integration" "DEV03_Document_Upload1" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id             = "${aws_api_gateway_resource.DEV03_Document_Upload1.id}"
  http_method             = "${aws_api_gateway_method.DEV03_Document_Upload1.http_method}"
  integration_http_method = "POST"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-dev03.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.dev03.gore.insurcloud.ca/document-service/v1/document/gore/base64/compose"
}

resource "aws_api_gateway_method_response" "DEV03_Document_Upload1" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id = "${aws_api_gateway_resource.DEV03_Document_Upload1.id}"
  http_method = "${aws_api_gateway_method.DEV03_Document_Upload1.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}

# Document Upload2
resource "aws_api_gateway_resource" "DEV03_Document_Upload2" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.DEV03_Document.id}"
  path_part   = "DEV03_Document_Upload2"
}

resource "aws_api_gateway_method" "DEV03_Document_Upload2" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id   = "${aws_api_gateway_resource.DEV03_Document_Upload2.id}"
  http_method   = "POST"
  authorization = "CUSTOM"
  authorizer_id = "${aws_api_gateway_authorizer.NonProd_Auth0_HCAI.id}"
}

resource "aws_api_gateway_integration" "DEV03_Document_Upload2" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id             = "${aws_api_gateway_resource.DEV03_Document_Upload2.id}"
  http_method             = "${aws_api_gateway_method.DEV03_Document_Upload2.http_method}"
  integration_http_method = "POST"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-dev03.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.dev03.gore.insurcloud.ca/document-service/v1/document/gore/save/document/payload"
}

resource "aws_api_gateway_method_response" "DEV03_Document_Upload2" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id = "${aws_api_gateway_resource.DEV03_Document_Upload2.id}"
  http_method = "${aws_api_gateway_method.DEV03_Document_Upload2.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}
###CRC###

resource "aws_api_gateway_resource" "DEV03_CRC" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.DEV03_AUTH.id}"
  path_part   = "DEV03_CRC"
}
resource "aws_api_gateway_resource" "DEV03_CRCCreateFNOL" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.DEV03_CRC.id}"
  path_part   = "DEV03_CRCCreateFNOL"
}


#CRC --> Guidewire
resource "aws_api_gateway_method" "DEV03_CRCCreateFNOL" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id   = "${aws_api_gateway_resource.DEV03_CRCCreateFNOL.id}"
  http_method   = "POST"
  authorization = "CUSTOM"
  authorizer_id = "${aws_api_gateway_authorizer.NonProd_Auth0_CRC.id}"
}

resource "aws_api_gateway_integration" "DEV03_CRCCreateFNOL" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id             = "${aws_api_gateway_resource.DEV03_CRCCreateFNOL.id}"
  http_method             = "${aws_api_gateway_method.DEV03_CRCCreateFNOL.http_method}"
  integration_http_method = "POST"
  connection_type         = "INTERNET"
  type                    = "HTTP_PROXY"
  uri                     = "https://dev3.goredev.guidewire.net/cc/ws/ic/integration/crc/CollisionReportingCenterAPI"
}

resource "aws_api_gateway_method_response" "DEV03_CRCCreateFNOL" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id = "${aws_api_gateway_resource.DEV03_CRCCreateFNOL.id}"
  http_method = "${aws_api_gateway_method.DEV03_CRCCreateFNOL.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}

#CRC --> MS 

resource "aws_api_gateway_resource" "DEV03_CRCCollisionDocumentsMS" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.DEV03_CRC.id}"
  path_part   = "DEV03_CRCCollisionDocumentsMS"
}

resource "aws_api_gateway_method" "DEV03_CRCCollisionDocumentsMS" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id   = "${aws_api_gateway_resource.DEV03_CRCCollisionDocumentsMS.id}"
  http_method   = "POST"
  authorization = "CUSTOM"
  authorizer_id = "${aws_api_gateway_authorizer.NonProd_Auth0_CRC.id}"
}

resource "aws_api_gateway_integration" "DEV03_CRCCollisionDocumentsMS" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id             = "${aws_api_gateway_resource.DEV03_CRCCollisionDocumentsMS.id}"
  http_method             = "${aws_api_gateway_method.DEV03_CRCCollisionDocumentsMS.http_method}"
  integration_http_method = "POST"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-dev03.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.dev03.gore.insurcloud.ca/crc-service/microservices/crc/ws/"
}

resource "aws_api_gateway_method_response" "DEV03_CRCCollisionDocumentsMS" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id = "${aws_api_gateway_resource.DEV03_CRCCollisionDocumentsMS.id}"
  http_method = "${aws_api_gateway_method.DEV03_CRCCollisionDocumentsMS.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}

####Audatex###

# Audatex Inbount Acknowledgment - ARMS

resource "aws_api_gateway_resource" "DEV03_AudatexINB" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.DEV03_AUTH.id}"
  path_part   = "DEV03_AudatexINB"
}
resource "aws_api_gateway_resource" "DEV03_AudatexAssignmentAck" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.DEV03_AudatexINB.id}"
  path_part   = "DEV03_AudatexAssignmentAck"
}

resource "aws_api_gateway_method" "DEV03_AudatexAssignmentAck" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id   = "${aws_api_gateway_resource.DEV03_AudatexAssignmentAck.id}"
  http_method   = "POST"
  authorization = "CUSTOM"
  authorizer_id = "${aws_api_gateway_authorizer.NonProd_Auth0_Audatex.id}"
}

resource "aws_api_gateway_integration" "DEV03_AudatexAssignmentAck" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id             = "${aws_api_gateway_resource.DEV03_AudatexAssignmentAck.id}"
  http_method             = "${aws_api_gateway_method.DEV03_AudatexAssignmentAck.http_method}"
  integration_http_method = "POST"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-dev03.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.dev03.gore.insurcloud.ca/audatex-service/v1/assignment/ack"
}

resource "aws_api_gateway_method_response" "DEV03_AudatexAssignmentAck" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id = "${aws_api_gateway_resource.DEV03_AudatexAssignmentAck.id}"
  http_method = "${aws_api_gateway_method.DEV03_AudatexAssignmentAck.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}

##Audatex Inbound estimate
resource "aws_api_gateway_resource" "DEV03_AudatexEstimate" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.DEV03_AudatexINB.id}"
  path_part   = "DEV03_AudatexEstimate"
}

resource "aws_api_gateway_method" "DEV03_AudatexEstimate" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id   = "${aws_api_gateway_resource.DEV03_AudatexEstimate.id}"
  http_method   = "POST"
  authorization = "CUSTOM"
  authorizer_id = "${aws_api_gateway_authorizer.NonProd_Auth0_Audatex.id}"
}


resource "aws_api_gateway_integration" "DEV03_AudatexEstimate" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id             = "${aws_api_gateway_resource.DEV03_AudatexEstimate.id}"
  http_method             = "${aws_api_gateway_method.DEV03_AudatexEstimate.http_method}"
  integration_http_method = "POST"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-dev03.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.dev03.gore.insurcloud.ca/audatex-service/v1/estimate"
}

resource "aws_api_gateway_method_response" "DEV03_AudatexEstimate" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id = "${aws_api_gateway_resource.DEV03_AudatexEstimate.id}"
  http_method = "${aws_api_gateway_method.DEV03_AudatexEstimate.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}
####end of audatex#####


# ###Uniban###
resource "aws_api_gateway_resource" "DEV03_Uniban_Get" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.DEV03_AUTH.id}"
  path_part   = "DEV03_Uniban_Get"
}

##Uniban --> API
resource "aws_api_gateway_method" "DEV03_Uniban_Get" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id   = "${aws_api_gateway_resource.DEV03_Uniban_Get.id}"
  http_method   = "GET"
  authorization = "CUSTOM"
  authorizer_id = "${aws_api_gateway_authorizer.NonProd_Auth0_UnibanService.id}"
  request_parameters = {
    "method.request.querystring.policyNumber"= true
    "method.request.querystring.dateOfLoss"= true
    "method.request.querystring.vin"= true
  }
}

resource "aws_api_gateway_integration" "DEV03_Uniban_Get" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id             = "${aws_api_gateway_resource.DEV03_Uniban_Get.id}"
  http_method             = "${aws_api_gateway_method.DEV03_Uniban_Get.http_method}"
  integration_http_method = "GET"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-dev03.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.dev03.gore.insurcloud.ca/uniban-service/v1/retrievePolicy"
}

resource "aws_api_gateway_method_response" "DEV03_Uniban_Get" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id = "${aws_api_gateway_resource.DEV03_Uniban_Get.id}"
  http_method = "${aws_api_gateway_method.DEV03_Uniban_Get.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}

###IBC Vehicle MicroService###

resource "aws_api_gateway_resource" "DEV03_IBC" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.DEV03_AUTH.id}"
  path_part   = "DEV03_IBC"
}

### IBC Vehicle Endpoint 1
resource "aws_api_gateway_resource" "DEV03_vininfo" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.DEV03_IBC.id}"
  path_part   = "DEV03_vininfo"
}

resource "aws_api_gateway_method" "DEV03_vininfo" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id   = "${aws_api_gateway_resource.DEV03_vininfo.id}"
  http_method   = "GET"
  authorization = "CUSTOM"
  authorizer_id = "${aws_api_gateway_authorizer.NonProd_Auth0_Vehicle.id}"
  request_parameters = {
    "method.request.querystring.vin"= true
  }
}

resource "aws_api_gateway_integration" "DEV03_vininfo" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id             = "${aws_api_gateway_resource.DEV03_vininfo.id}"
  http_method             = "${aws_api_gateway_method.DEV03_vininfo.http_method}"
  integration_http_method = "GET"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-dev03.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.dev03.gore.insurcloud.ca/vehicle-info-service/v1/vininfo"
}

resource "aws_api_gateway_method_response" "DEV03_vininfo" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id = "${aws_api_gateway_resource.DEV03_vininfo.id}"
  http_method = "${aws_api_gateway_method.DEV03_vininfo.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}
# IBC Vehicle Endpoint 2####
resource "aws_api_gateway_resource" "DEV03_submit" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.DEV03_IBC.id}"
  path_part   = "DEV03_submit"
}

resource "aws_api_gateway_method" "DEV03_submit" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id   = "${aws_api_gateway_resource.DEV03_submit.id}"
  http_method   = "POST"
  authorization = "CUSTOM"
  authorizer_id = "${aws_api_gateway_authorizer.NonProd_Auth0_Vehicle.id}"
}

resource "aws_api_gateway_integration" "DEV03_submit" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id             = "${aws_api_gateway_resource.DEV03_submit.id}"
  http_method             = "${aws_api_gateway_method.DEV03_submit.http_method}"
  integration_http_method = "POST"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-dev03.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.dev03.gore.insurcloud.ca/vehicle-info-service/v1/product/download/submit"
}

resource "aws_api_gateway_method_response" "DEV03_submit" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id = "${aws_api_gateway_resource.DEV03_submit.id}"
  http_method = "${aws_api_gateway_method.DEV03_submit.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}

# IBC Vehicle ppvinfo
resource "aws_api_gateway_resource" "DEV03_ppvinfo" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.DEV03_IBC.id}"
  path_part   = "DEV03_ppvinfo"
}

resource "aws_api_gateway_method" "DEV03_ppvinfo" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id   = "${aws_api_gateway_resource.DEV03_ppvinfo.id}"
  http_method   = "GET"
  authorization = "CUSTOM"
  authorizer_id = "${aws_api_gateway_authorizer.NonProd_Auth0_Vehicle.id}"
  request_parameters = {
    "method.request.querystring.rateGroupPublicationYear"= true
    "method.request.querystring.carCode"= true
    "method.request.querystring.modelYear"= true
  }
}

resource "aws_api_gateway_integration" "DEV03_ppvinfo" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id             = "${aws_api_gateway_resource.DEV03_ppvinfo.id}"
  http_method             = "${aws_api_gateway_method.DEV03_ppvinfo.http_method}"
  integration_http_method = "GET"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-dev03.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.dev03.gore.insurcloud.ca/vehicle-info-service/v1/ppvinfo"
}

resource "aws_api_gateway_method_response" "DEV03_ppvinfo" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id = "${aws_api_gateway_resource.DEV03_ppvinfo.id}"
  http_method = "${aws_api_gateway_method.DEV03_ppvinfo.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}

# IBC Vehicle recinfo
resource "aws_api_gateway_resource" "DEV03_recinfo" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.DEV03_IBC.id}"
  path_part   = "DEV03_recinfo"
}

resource "aws_api_gateway_method" "DEV03_recinfo" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id   = "${aws_api_gateway_resource.DEV03_recinfo.id}"
  http_method   = "GET"
  authorization = "CUSTOM"
  authorizer_id = "${aws_api_gateway_authorizer.NonProd_Auth0_Vehicle.id}"
  request_parameters = {
    "method.request.querystring.publicationYear"= true
    "method.request.querystring.vehicleType"= true
    "method.request.querystring.vehicleMake"= true
    "method.request.querystring.vehicleModel"= true
    "method.request.querystring.modelYear"= true
 }
}

resource "aws_api_gateway_integration" "DEV03_recinfo" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id             = "${aws_api_gateway_resource.DEV03_recinfo.id}"
  http_method             = "${aws_api_gateway_method.DEV03_recinfo.http_method}"
  integration_http_method = "GET"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-dev03.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.dev03.gore.insurcloud.ca/vehicle-info-service/v1/recinfo"
}

resource "aws_api_gateway_method_response" "DEV03_recinfo" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id = "${aws_api_gateway_resource.DEV03_recinfo.id}"
  http_method = "${aws_api_gateway_method.DEV03_recinfo.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}
######### RESOURCE GROUPS ##############
#########TBT01#########
resource "aws_api_gateway_resource" "TBT01_AUTH" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_rest_api.GORE_AUTH0.root_resource_id}"
  path_part   = "TBT01_AUTH"
}
#####Address Validation#####
resource "aws_api_gateway_resource" "TBT01_Address" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.TBT01_AUTH.id}"
  path_part   = "TBT01_Address"
}

# Address Outbound GW -API Gateway - ARMS
resource "aws_api_gateway_method" "TBT01_Address" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id   = "${aws_api_gateway_resource.TBT01_Address.id}"
  http_method   = "POST"
  authorization = "CUSTOM"
  authorizer_id = "${aws_api_gateway_authorizer.NonProd_Auth0_AddressValidation.id}"
}

resource "aws_api_gateway_integration" "TBT01_Address" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id             = "${aws_api_gateway_resource.TBT01_Address.id}"
  http_method             = "${aws_api_gateway_method.TBT01_Address.http_method}"
  integration_http_method = "POST"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-tbt01.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.tbt01.gore.insurcloud.ca/address-validation-service/microservices/addressvalidation/ws"
}

resource "aws_api_gateway_method_response" "TBT01_Address" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id = "${aws_api_gateway_resource.TBT01_Address.id}"
  http_method = "${aws_api_gateway_method.TBT01_Address.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}
###HCAI###

resource "aws_api_gateway_resource" "TBT01_HCAISecurity" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.TBT01_AUTH.id}"
  path_part   = "TBT01_HCAISecurity"
}

# HCAI Outbound GW -API Gateway - ARMS
resource "aws_api_gateway_method" "TBT01_HCAISecurity" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id   = "${aws_api_gateway_resource.TBT01_HCAISecurity.id}"
  http_method   = "POST"
  authorization = "CUSTOM"
  authorizer_id = "${aws_api_gateway_authorizer.NonProd_Auth0_HCAI.id}"
}

resource "aws_api_gateway_integration" "TBT01_HCAISecurity" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id             = "${aws_api_gateway_resource.TBT01_HCAISecurity.id}"
  http_method             = "${aws_api_gateway_method.TBT01_HCAISecurity.http_method}"
  integration_http_method = "POST"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-tbt01.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.tbt01.gore.insurcloud.ca/soap-security-service/v1/proxy"
}

resource "aws_api_gateway_method_response" "TBT01_HCAISecurity" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id = "${aws_api_gateway_resource.TBT01_HCAISecurity.id}"
  http_method = "${aws_api_gateway_method.TBT01_HCAISecurity.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}

###Cheque Printing#####
resource "aws_api_gateway_resource" "TBT01_ChequePrinting" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.TBT01_AUTH.id}"
  path_part   = "TBT01_ChequePrinting"
}
# #Cheque Outbound#####
resource "aws_api_gateway_resource" "TBT01_SaveCheque" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.TBT01_ChequePrinting.id}"
  path_part   = "TBT01_SaveCheque"
}

resource "aws_api_gateway_method" "TBT01_SaveCheque" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id   = "${aws_api_gateway_resource.TBT01_SaveCheque.id}"
  http_method   = "POST"
  authorization = "CUSTOM"
  authorizer_id = "${aws_api_gateway_authorizer.NonProd_Auth0_ChequePrinting.id}"
}

resource "aws_api_gateway_integration" "TBT01_SaveCheque" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id             = "${aws_api_gateway_resource.TBT01_SaveCheque.id}"
  http_method             = "${aws_api_gateway_method.TBT01_SaveCheque.http_method}"
  integration_http_method = "POST"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-tbt01.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.tbt01.gore.insurcloud.ca/cheque-service/v1/cheque/print"
}

resource "aws_api_gateway_method_response" "TBT01_SaveCheque" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id = "${aws_api_gateway_resource.TBT01_SaveCheque.id}"
  http_method = "${aws_api_gateway_method.TBT01_SaveCheque.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}
# #Cheque --> F & 0 --> MS#####
resource "aws_api_gateway_resource" "TBT01_ChequeUpdateNumbers" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.TBT01_ChequePrinting.id}"
  path_part   = "TBT01_ChequeUpdateNumbers"
}

resource "aws_api_gateway_method" "TBT01_ChequeUpdateNumbers" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id   = "${aws_api_gateway_resource.TBT01_ChequeUpdateNumbers.id}"
  http_method   = "POST"
  authorization = "CUSTOM"
  authorizer_id = "${aws_api_gateway_authorizer.NonProd_Auth0_ChequePrinting.id}"
}

resource "aws_api_gateway_integration" "TBT01_ChequeUpdateNumbers" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id             = "${aws_api_gateway_resource.TBT01_ChequeUpdateNumbers.id}"
  http_method             = "${aws_api_gateway_method.TBT01_ChequeUpdateNumbers.http_method}"
  integration_http_method = "POST"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-tbt01.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.tbt01.gore.insurcloud.ca/cheque-service/v1/cheque/updateChequeNumbers"
}

resource "aws_api_gateway_method_response" "TBT01_ChequeUpdateNumbers" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id = "${aws_api_gateway_resource.TBT01_ChequeUpdateNumbers.id}"
  http_method = "${aws_api_gateway_method.TBT01_ChequeUpdateNumbers.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}
# #MVR Service#####
resource "aws_api_gateway_resource" "TBT01_MVRDriverInfo" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.TBT01_AUTH.id}"
  path_part   = "TBT01_MVRDriverInfo"
}

resource "aws_api_gateway_method" "TBT01_MVRDriverInfo" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id   = "${aws_api_gateway_resource.TBT01_MVRDriverInfo.id}"
  http_method   = "POST"
  authorization = "CUSTOM"
  authorizer_id = "${aws_api_gateway_authorizer.NonProd_Auth0_MVR.id}"
}

resource "aws_api_gateway_integration" "TBT01_MVRDriverInfo" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id             = "${aws_api_gateway_resource.TBT01_MVRDriverInfo.id}"
  http_method             = "${aws_api_gateway_method.TBT01_MVRDriverInfo.http_method}"
  integration_http_method = "POST"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-tbt01.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.tbt01.gore.insurcloud.ca/mvr-service/v1/driver/info"
}

resource "aws_api_gateway_method_response" "TBT01_MVRDriverInfo" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id = "${aws_api_gateway_resource.TBT01_MVRDriverInfo.id}"
  http_method = "${aws_api_gateway_method.TBT01_MVRDriverInfo.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}

#######Download service#####
resource "aws_api_gateway_resource" "TBT01_BMSDownload" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.TBT01_AUTH.id}"
  path_part   = "TBT01_BMSDownload"
}

# #Download Service for PC payload#####
resource "aws_api_gateway_resource" "TBT01_BMSDownload_PC" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.TBT01_AUTH.id}"
  path_part   = "TBT01_BMSDownload_PC"
}

resource "aws_api_gateway_method" "TBT01_BMSDownload_PC" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id   = "${aws_api_gateway_resource.TBT01_BMSDownload_PC.id}"
  http_method   = "POST"
  authorization = "CUSTOM"
  authorizer_id = "${aws_api_gateway_authorizer.NonProd_Auth0_MVR.id}"
}

resource "aws_api_gateway_integration" "TBT01_BMSDownload_PC" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id             = "${aws_api_gateway_resource.TBT01_BMSDownload_PC.id}"
  http_method             = "${aws_api_gateway_method.TBT01_BMSDownload_PC.http_method}"
  integration_http_method = "POST"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-tbt01.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.tbt01.gore.insurcloud.ca/bms-download-service/v1/bmsdownload/pcpayload"
}

resource "aws_api_gateway_method_response" "TBT01_BMSDownload_PC" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id = "${aws_api_gateway_resource.TBT01_BMSDownload_PC.id}"
  http_method = "${aws_api_gateway_method.TBT01_BMSDownload_PC.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}

# #Download Service for BC payload#####
resource "aws_api_gateway_resource" "TBT01_BMSDownload_BC" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.TBT01_BMSDownload.id}"
  path_part   = "TBT01_BMSDownload_BC"
}

resource "aws_api_gateway_method" "TBT01_BMSDownload_BC" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id   = "${aws_api_gateway_resource.TBT01_BMSDownload_BC.id}"
  http_method   = "POST"
  authorization = "CUSTOM"
  authorizer_id = "${aws_api_gateway_authorizer.NonProd_Auth0_MVR.id}"
}

resource "aws_api_gateway_integration" "TBT01_BMSDownload_BC" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id             = "${aws_api_gateway_resource.TBT01_BMSDownload_BC.id}"
  http_method             = "${aws_api_gateway_method.TBT01_BMSDownload_BC.http_method}"
  integration_http_method = "POST"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-tbt01.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.tbt01.gore.insurcloud.ca/bms-download-service/v1/bmsdownload/bcpayload"
}

resource "aws_api_gateway_method_response" "TBT01_BMSDownload_BC" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id = "${aws_api_gateway_resource.TBT01_BMSDownload_BC.id}"
  http_method = "${aws_api_gateway_method.TBT01_BMSDownload_BC.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}

###Document Service###

resource "aws_api_gateway_resource" "TBT01_Document" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.TBT01_AUTH.id}"
  path_part   = "TBT01_Document"
}

# Document Search
resource "aws_api_gateway_resource" "TBT01_Document_Search" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.TBT01_Document.id}"
  path_part   = "TBT01_Document_Search"
}

resource "aws_api_gateway_method" "TBT01_Document_Search" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id   = "${aws_api_gateway_resource.TBT01_Document_Search.id}"
  http_method   = "POST"
  authorization = "CUSTOM"
  authorizer_id = "${aws_api_gateway_authorizer.NonProd_Auth0_HCAI.id}"
}

resource "aws_api_gateway_integration" "TBT01_Document_Search" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id             = "${aws_api_gateway_resource.TBT01_Document_Search.id}"
  http_method             = "${aws_api_gateway_method.TBT01_Document_Search.http_method}"
  integration_http_method = "POST"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-tbt01.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.tbt01.gore.insurcloud.ca/document-service/v1/document/gore/summaries/search/metadata"
}

resource "aws_api_gateway_method_response" "TBT01_Document_Search" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id = "${aws_api_gateway_resource.TBT01_Document_Search.id}"
  http_method = "${aws_api_gateway_method.TBT01_Document_Search.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}
# Document Retrieve
resource "aws_api_gateway_resource" "TBT01_Document_Retrieve" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.TBT01_Document.id}"
  path_part   = "TBT01_Document_Retrieve"
}

resource "aws_api_gateway_method" "TBT01_Document_Retrieve" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id   = "${aws_api_gateway_resource.TBT01_Document_Retrieve.id}"
  http_method   = "POST"
  authorization = "CUSTOM"
  authorizer_id = "${aws_api_gateway_authorizer.NonProd_Auth0_HCAI.id}"
}

resource "aws_api_gateway_integration" "TBT01_Document_Retrieve" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id             = "${aws_api_gateway_resource.TBT01_Document_Retrieve.id}"
  http_method             = "${aws_api_gateway_method.TBT01_Document_Retrieve.http_method}"
  integration_http_method = "POST"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-tbt01.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.tbt01.gore.insurcloud.ca/document-service/v1/document/gore/retrieve/key"
}

resource "aws_api_gateway_method_response" "TBT01_Document_Retrieve" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id = "${aws_api_gateway_resource.TBT01_Document_Retrieve.id}"
  http_method = "${aws_api_gateway_method.TBT01_Document_Retrieve.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}

# Document Upload1
resource "aws_api_gateway_resource" "TBT01_Document_Upload1" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.TBT01_Document.id}"
  path_part   = "TBT01_Document_Upload1"
}

resource "aws_api_gateway_method" "TBT01_Document_Upload1" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id   = "${aws_api_gateway_resource.TBT01_Document_Upload1.id}"
  http_method   = "POST"
  authorization = "CUSTOM"
  authorizer_id = "${aws_api_gateway_authorizer.NonProd_Auth0_HCAI.id}"
}

resource "aws_api_gateway_integration" "TBT01_Document_Upload1" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id             = "${aws_api_gateway_resource.TBT01_Document_Upload1.id}"
  http_method             = "${aws_api_gateway_method.TBT01_Document_Upload1.http_method}"
  integration_http_method = "POST"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-tbt01.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.tbt01.gore.insurcloud.ca/document-service/v1/document/gore/base64/compose"
}

resource "aws_api_gateway_method_response" "TBT01_Document_Upload1" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id = "${aws_api_gateway_resource.TBT01_Document_Upload1.id}"
  http_method = "${aws_api_gateway_method.TBT01_Document_Upload1.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}

# Document Upload2
resource "aws_api_gateway_resource" "TBT01_Document_Upload2" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.TBT01_Document.id}"
  path_part   = "TBT01_Document_Upload2"
}

resource "aws_api_gateway_method" "TBT01_Document_Upload2" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id   = "${aws_api_gateway_resource.TBT01_Document_Upload2.id}"
  http_method   = "POST"
  authorization = "CUSTOM"
  authorizer_id = "${aws_api_gateway_authorizer.NonProd_Auth0_HCAI.id}"
}

resource "aws_api_gateway_integration" "TBT01_Document_Upload2" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id             = "${aws_api_gateway_resource.TBT01_Document_Upload2.id}"
  http_method             = "${aws_api_gateway_method.TBT01_Document_Upload2.http_method}"
  integration_http_method = "POST"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-tbt01.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.tbt01.gore.insurcloud.ca/document-service/v1/document/gore/save/document/payload"
}

resource "aws_api_gateway_method_response" "TBT01_Document_Upload2" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id = "${aws_api_gateway_resource.TBT01_Document_Upload2.id}"
  http_method = "${aws_api_gateway_method.TBT01_Document_Upload2.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}
###CRC###

resource "aws_api_gateway_resource" "TBT01_CRC" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.TBT01_AUTH.id}"
  path_part   = "TBT01_CRC"
}
resource "aws_api_gateway_resource" "TBT01_CRCCreateFNOL" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.TBT01_CRC.id}"
  path_part   = "TBT01_CRCCreateFNOL"
}


#CRC --> Guidewire
resource "aws_api_gateway_method" "TBT01_CRCCreateFNOL" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id   = "${aws_api_gateway_resource.TBT01_CRCCreateFNOL.id}"
  http_method   = "POST"
  authorization = "CUSTOM"
  authorizer_id = "${aws_api_gateway_authorizer.NonProd_Auth0_CRC.id}"
}

resource "aws_api_gateway_integration" "TBT01_CRCCreateFNOL" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id             = "${aws_api_gateway_resource.TBT01_CRCCreateFNOL.id}"
  http_method             = "${aws_api_gateway_method.TBT01_CRCCreateFNOL.http_method}"
  integration_http_method = "POST"
  connection_type         = "INTERNET"
  type                    = "HTTP_PROXY"
  uri                     = "https://tbt1.goredev.guidewire.net/cc/ws/ic/integration/crc/CollisionReportingCenterAPI"
}

resource "aws_api_gateway_method_response" "TBT01_CRCCreateFNOL" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id = "${aws_api_gateway_resource.TBT01_CRCCreateFNOL.id}"
  http_method = "${aws_api_gateway_method.TBT01_CRCCreateFNOL.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}

#CRC --> MS 

resource "aws_api_gateway_resource" "TBT01_CRCCollisionDocumentsMS" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.TBT01_CRC.id}"
  path_part   = "TBT01_CRCCollisionDocumentsMS"
}

resource "aws_api_gateway_method" "TBT01_CRCCollisionDocumentsMS" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id   = "${aws_api_gateway_resource.TBT01_CRCCollisionDocumentsMS.id}"
  http_method   = "POST"
  authorization = "CUSTOM"
  authorizer_id = "${aws_api_gateway_authorizer.NonProd_Auth0_CRC.id}"
}

resource "aws_api_gateway_integration" "TBT01_CRCCollisionDocumentsMS" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id             = "${aws_api_gateway_resource.TBT01_CRCCollisionDocumentsMS.id}"
  http_method             = "${aws_api_gateway_method.TBT01_CRCCollisionDocumentsMS.http_method}"
  integration_http_method = "POST"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-tbt01.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.tbt01.gore.insurcloud.ca/crc-service/microservices/crc/ws/"
}

resource "aws_api_gateway_method_response" "TBT01_CRCCollisionDocumentsMS" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id = "${aws_api_gateway_resource.TBT01_CRCCollisionDocumentsMS.id}"
  http_method = "${aws_api_gateway_method.TBT01_CRCCollisionDocumentsMS.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}

####Audatex###

# Audatex Inbount Acknowledgment - ARMS

resource "aws_api_gateway_resource" "TBT01_AudatexINB" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.TBT01_AUTH.id}"
  path_part   = "TBT01_AudatexINB"
}
resource "aws_api_gateway_resource" "TBT01_AudatexAssignmentAck" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.TBT01_AudatexINB.id}"
  path_part   = "TBT01_AudatexAssignmentAck"
}

resource "aws_api_gateway_method" "TBT01_AudatexAssignmentAck" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id   = "${aws_api_gateway_resource.TBT01_AudatexAssignmentAck.id}"
  http_method   = "POST"
  authorization = "CUSTOM"
  authorizer_id = "${aws_api_gateway_authorizer.NonProd_Auth0_Audatex.id}"
}

resource "aws_api_gateway_integration" "TBT01_AudatexAssignmentAck" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id             = "${aws_api_gateway_resource.TBT01_AudatexAssignmentAck.id}"
  http_method             = "${aws_api_gateway_method.TBT01_AudatexAssignmentAck.http_method}"
  integration_http_method = "POST"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-tbt01.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.tbt01.gore.insurcloud.ca/audatex-service/v1/assignment/ack"
}

resource "aws_api_gateway_method_response" "TBT01_AudatexAssignmentAck" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id = "${aws_api_gateway_resource.TBT01_AudatexAssignmentAck.id}"
  http_method = "${aws_api_gateway_method.TBT01_AudatexAssignmentAck.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}

##Audatex Inbound estimate
resource "aws_api_gateway_resource" "TBT01_AudatexEstimate" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.TBT01_AudatexINB.id}"
  path_part   = "TBT01_AudatexEstimate"
}

resource "aws_api_gateway_method" "TBT01_AudatexEstimate" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id   = "${aws_api_gateway_resource.TBT01_AudatexEstimate.id}"
  http_method   = "POST"
  authorization = "CUSTOM"
  authorizer_id = "${aws_api_gateway_authorizer.NonProd_Auth0_Audatex.id}"
}


resource "aws_api_gateway_integration" "TBT01_AudatexEstimate" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id             = "${aws_api_gateway_resource.TBT01_AudatexEstimate.id}"
  http_method             = "${aws_api_gateway_method.TBT01_AudatexEstimate.http_method}"
  integration_http_method = "POST"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-tbt01.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.tbt01.gore.insurcloud.ca/audatex-service/v1/estimate"
}

resource "aws_api_gateway_method_response" "TBT01_AudatexEstimate" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id = "${aws_api_gateway_resource.TBT01_AudatexEstimate.id}"
  http_method = "${aws_api_gateway_method.TBT01_AudatexEstimate.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}
####end of audatex#####


# ###Uniban###
resource "aws_api_gateway_resource" "TBT01_Uniban_Get" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.TBT01_AUTH.id}"
  path_part   = "TBT01_Uniban_Get"
}

##Uniban --> API
resource "aws_api_gateway_method" "TBT01_Uniban_Get" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id   = "${aws_api_gateway_resource.TBT01_Uniban_Get.id}"
  http_method   = "GET"
  authorization = "CUSTOM"
  authorizer_id = "${aws_api_gateway_authorizer.NonProd_Auth0_UnibanService.id}"
  request_parameters = {
    "method.request.querystring.policyNumber"= true
    "method.request.querystring.dateOfLoss"= true
    "method.request.querystring.vin"= true
  }
}

resource "aws_api_gateway_integration" "TBT01_Uniban_Get" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id             = "${aws_api_gateway_resource.TBT01_Uniban_Get.id}"
  http_method             = "${aws_api_gateway_method.TBT01_Uniban_Get.http_method}"
  integration_http_method = "GET"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-tbt01.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.tbt01.gore.insurcloud.ca/uniban-service/v1/retrievePolicy"
}

resource "aws_api_gateway_method_response" "TBT01_Uniban_Get" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id = "${aws_api_gateway_resource.TBT01_Uniban_Get.id}"
  http_method = "${aws_api_gateway_method.TBT01_Uniban_Get.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}

###IBC Vehicle MicroService###

resource "aws_api_gateway_resource" "TBT01_IBC" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.TBT01_AUTH.id}"
  path_part   = "TBT01_IBC"
}

### IBC Vehicle Endpoint 1
resource "aws_api_gateway_resource" "TBT01_vininfo" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.TBT01_IBC.id}"
  path_part   = "TBT01_vininfo"
}

resource "aws_api_gateway_method" "TBT01_vininfo" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id   = "${aws_api_gateway_resource.TBT01_vininfo.id}"
  http_method   = "GET"
  authorization = "CUSTOM"
  authorizer_id = "${aws_api_gateway_authorizer.NonProd_Auth0_Vehicle.id}"
  request_parameters = {
    "method.request.querystring.vin"= true
  }
}

resource "aws_api_gateway_integration" "TBT01_vininfo" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id             = "${aws_api_gateway_resource.TBT01_vininfo.id}"
  http_method             = "${aws_api_gateway_method.TBT01_vininfo.http_method}"
  integration_http_method = "GET"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-tbt01.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.tbt01.gore.insurcloud.ca/vehicle-info-service/v1/vininfo"
}

resource "aws_api_gateway_method_response" "TBT01_vininfo" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id = "${aws_api_gateway_resource.TBT01_vininfo.id}"
  http_method = "${aws_api_gateway_method.TBT01_vininfo.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}
# IBC Vehicle Endpoint 2####
resource "aws_api_gateway_resource" "TBT01_submit" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.TBT01_IBC.id}"
  path_part   = "TBT01_submit"
}

resource "aws_api_gateway_method" "TBT01_submit" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id   = "${aws_api_gateway_resource.TBT01_submit.id}"
  http_method   = "POST"
  authorization = "CUSTOM"
  authorizer_id = "${aws_api_gateway_authorizer.NonProd_Auth0_Vehicle.id}"
}

resource "aws_api_gateway_integration" "TBT01_submit" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id             = "${aws_api_gateway_resource.TBT01_submit.id}"
  http_method             = "${aws_api_gateway_method.TBT01_submit.http_method}"
  integration_http_method = "POST"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-tbt01.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.tbt01.gore.insurcloud.ca/vehicle-info-service/v1/product/download/submit"
}

resource "aws_api_gateway_method_response" "TBT01_submit" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id = "${aws_api_gateway_resource.TBT01_submit.id}"
  http_method = "${aws_api_gateway_method.TBT01_submit.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}

# IBC Vehicle ppvinfo
resource "aws_api_gateway_resource" "TBT01_ppvinfo" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.TBT01_IBC.id}"
  path_part   = "TBT01_ppvinfo"
}

resource "aws_api_gateway_method" "TBT01_ppvinfo" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id   = "${aws_api_gateway_resource.TBT01_ppvinfo.id}"
  http_method   = "GET"
  authorization = "CUSTOM"
  authorizer_id = "${aws_api_gateway_authorizer.NonProd_Auth0_Vehicle.id}"
  request_parameters = {
    "method.request.querystring.rateGroupPublicationYear"= true
    "method.request.querystring.carCode"= true
    "method.request.querystring.modelYear"= true
  }
}

resource "aws_api_gateway_integration" "TBT01_ppvinfo" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id             = "${aws_api_gateway_resource.TBT01_ppvinfo.id}"
  http_method             = "${aws_api_gateway_method.TBT01_ppvinfo.http_method}"
  integration_http_method = "GET"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-tbt01.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.tbt01.gore.insurcloud.ca/vehicle-info-service/v1/ppvinfo"
}

resource "aws_api_gateway_method_response" "TBT01_ppvinfo" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id = "${aws_api_gateway_resource.TBT01_ppvinfo.id}"
  http_method = "${aws_api_gateway_method.TBT01_ppvinfo.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}

# IBC Vehicle recinfo
resource "aws_api_gateway_resource" "TBT01_recinfo" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.TBT01_IBC.id}"
  path_part   = "TBT01_recinfo"
}

resource "aws_api_gateway_method" "TBT01_recinfo" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id   = "${aws_api_gateway_resource.TBT01_recinfo.id}"
  http_method   = "GET"
  authorization = "CUSTOM"
  authorizer_id = "${aws_api_gateway_authorizer.NonProd_Auth0_Vehicle.id}"
  request_parameters = {
    "method.request.querystring.publicationYear"= true
    "method.request.querystring.vehicleType"= true
    "method.request.querystring.vehicleMake"= true
    "method.request.querystring.vehicleModel"= true
    "method.request.querystring.modelYear"= true
 }
}

resource "aws_api_gateway_integration" "TBT01_recinfo" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id             = "${aws_api_gateway_resource.TBT01_recinfo.id}"
  http_method             = "${aws_api_gateway_method.TBT01_recinfo.http_method}"
  integration_http_method = "GET"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-tbt01.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.tbt01.gore.insurcloud.ca/vehicle-info-service/v1/recinfo"
}

resource "aws_api_gateway_method_response" "TBT01_recinfo" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id = "${aws_api_gateway_resource.TBT01_recinfo.id}"
  http_method = "${aws_api_gateway_method.TBT01_recinfo.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}
######### RESOURCE GROUPS ##############
#########TBT02#########
resource "aws_api_gateway_resource" "TBT02_AUTH" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_rest_api.GORE_AUTH0.root_resource_id}"
  path_part   = "TBT02_AUTH"
}
#####Address Validation#####
resource "aws_api_gateway_resource" "TBT02_Address" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.TBT02_AUTH.id}"
  path_part   = "TBT02_Address"
}

# Address Outbound GW -API Gateway - ARMS
resource "aws_api_gateway_method" "TBT02_Address" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id   = "${aws_api_gateway_resource.TBT02_Address.id}"
  http_method   = "POST"
  authorization = "CUSTOM"
  authorizer_id = "${aws_api_gateway_authorizer.NonProd_Auth0_AddressValidation.id}"
}

resource "aws_api_gateway_integration" "TBT02_Address" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id             = "${aws_api_gateway_resource.TBT02_Address.id}"
  http_method             = "${aws_api_gateway_method.TBT02_Address.http_method}"
  integration_http_method = "POST"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-tbt02.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.tbt02.gore.insurcloud.ca/address-validation-service/microservices/addressvalidation/ws"
}

resource "aws_api_gateway_method_response" "TBT02_Address" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id = "${aws_api_gateway_resource.TBT02_Address.id}"
  http_method = "${aws_api_gateway_method.TBT02_Address.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}
###HCAI###

resource "aws_api_gateway_resource" "TBT02_HCAISecurity" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.TBT02_AUTH.id}"
  path_part   = "TBT02_HCAISecurity"
}

# HCAI Outbound GW -API Gateway - ARMS
resource "aws_api_gateway_method" "TBT02_HCAISecurity" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id   = "${aws_api_gateway_resource.TBT02_HCAISecurity.id}"
  http_method   = "POST"
  authorization = "CUSTOM"
  authorizer_id = "${aws_api_gateway_authorizer.NonProd_Auth0_HCAI.id}"
}

resource "aws_api_gateway_integration" "TBT02_HCAISecurity" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id             = "${aws_api_gateway_resource.TBT02_HCAISecurity.id}"
  http_method             = "${aws_api_gateway_method.TBT02_HCAISecurity.http_method}"
  integration_http_method = "POST"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-tbt02.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.tbt02.gore.insurcloud.ca/soap-security-service/v1/proxy"
}

resource "aws_api_gateway_method_response" "TBT02_HCAISecurity" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id = "${aws_api_gateway_resource.TBT02_HCAISecurity.id}"
  http_method = "${aws_api_gateway_method.TBT02_HCAISecurity.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}

###Cheque Printing#####
resource "aws_api_gateway_resource" "TBT02_ChequePrinting" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.TBT02_AUTH.id}"
  path_part   = "TBT02_ChequePrinting"
}
# #Cheque Outbound#####
resource "aws_api_gateway_resource" "TBT02_SaveCheque" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.TBT02_ChequePrinting.id}"
  path_part   = "TBT02_SaveCheque"
}

resource "aws_api_gateway_method" "TBT02_SaveCheque" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id   = "${aws_api_gateway_resource.TBT02_SaveCheque.id}"
  http_method   = "POST"
  authorization = "CUSTOM"
  authorizer_id = "${aws_api_gateway_authorizer.NonProd_Auth0_ChequePrinting.id}"
}

resource "aws_api_gateway_integration" "TBT02_SaveCheque" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id             = "${aws_api_gateway_resource.TBT02_SaveCheque.id}"
  http_method             = "${aws_api_gateway_method.TBT02_SaveCheque.http_method}"
  integration_http_method = "POST"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-tbt02.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.tbt02.gore.insurcloud.ca/cheque-service/v1/cheque/print"
}

resource "aws_api_gateway_method_response" "TBT02_SaveCheque" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id = "${aws_api_gateway_resource.TBT02_SaveCheque.id}"
  http_method = "${aws_api_gateway_method.TBT02_SaveCheque.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}
# #Cheque --> F & 0 --> MS#####
resource "aws_api_gateway_resource" "TBT02_ChequeUpdateNumbers" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.TBT02_ChequePrinting.id}"
  path_part   = "TBT02_ChequeUpdateNumbers"
}

resource "aws_api_gateway_method" "TBT02_ChequeUpdateNumbers" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id   = "${aws_api_gateway_resource.TBT02_ChequeUpdateNumbers.id}"
  http_method   = "POST"
  authorization = "CUSTOM"
  authorizer_id = "${aws_api_gateway_authorizer.NonProd_Auth0_ChequePrinting.id}"
}

resource "aws_api_gateway_integration" "TBT02_ChequeUpdateNumbers" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id             = "${aws_api_gateway_resource.TBT02_ChequeUpdateNumbers.id}"
  http_method             = "${aws_api_gateway_method.TBT02_ChequeUpdateNumbers.http_method}"
  integration_http_method = "POST"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-tbt02.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.tbt02.gore.insurcloud.ca/cheque-service/v1/cheque/updateChequeNumbers"
}

resource "aws_api_gateway_method_response" "TBT02_ChequeUpdateNumbers" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id = "${aws_api_gateway_resource.TBT02_ChequeUpdateNumbers.id}"
  http_method = "${aws_api_gateway_method.TBT02_ChequeUpdateNumbers.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}
# #MVR Service#####
resource "aws_api_gateway_resource" "TBT02_MVRDriverInfo" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.TBT02_AUTH.id}"
  path_part   = "TBT02_MVRDriverInfo"
}

resource "aws_api_gateway_method" "TBT02_MVRDriverInfo" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id   = "${aws_api_gateway_resource.TBT02_MVRDriverInfo.id}"
  http_method   = "POST"
  authorization = "CUSTOM"
  authorizer_id = "${aws_api_gateway_authorizer.NonProd_Auth0_MVR.id}"
}

resource "aws_api_gateway_integration" "TBT02_MVRDriverInfo" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id             = "${aws_api_gateway_resource.TBT02_MVRDriverInfo.id}"
  http_method             = "${aws_api_gateway_method.TBT02_MVRDriverInfo.http_method}"
  integration_http_method = "POST"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-tbt02.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.tbt02.gore.insurcloud.ca/mvr-service/v1/driver/info"
}

resource "aws_api_gateway_method_response" "TBT02_MVRDriverInfo" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id = "${aws_api_gateway_resource.TBT02_MVRDriverInfo.id}"
  http_method = "${aws_api_gateway_method.TBT02_MVRDriverInfo.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}

#######Download service#####
resource "aws_api_gateway_resource" "TBT02_BMSDownload" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.TBT02_AUTH.id}"
  path_part   = "TBT02_BMSDownload"
}

# #Download Service for PC payload#####
resource "aws_api_gateway_resource" "TBT02_BMSDownload_PC" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.TBT02_AUTH.id}"
  path_part   = "TBT02_BMSDownload_PC"
}

resource "aws_api_gateway_method" "TBT02_BMSDownload_PC" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id   = "${aws_api_gateway_resource.TBT02_BMSDownload_PC.id}"
  http_method   = "POST"
  authorization = "CUSTOM"
  authorizer_id = "${aws_api_gateway_authorizer.NonProd_Auth0_MVR.id}"
}

resource "aws_api_gateway_integration" "TBT02_BMSDownload_PC" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id             = "${aws_api_gateway_resource.TBT02_BMSDownload_PC.id}"
  http_method             = "${aws_api_gateway_method.TBT02_BMSDownload_PC.http_method}"
  integration_http_method = "POST"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-tbt02.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.tbt02.gore.insurcloud.ca/bms-download-service/v1/bmsdownload/pcpayload"
}

resource "aws_api_gateway_method_response" "TBT02_BMSDownload_PC" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id = "${aws_api_gateway_resource.TBT02_BMSDownload_PC.id}"
  http_method = "${aws_api_gateway_method.TBT02_BMSDownload_PC.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}

# #Download Service for BC payload#####
resource "aws_api_gateway_resource" "TBT02_BMSDownload_BC" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.TBT02_BMSDownload.id}"
  path_part   = "TBT02_BMSDownload_BC"
}

resource "aws_api_gateway_method" "TBT02_BMSDownload_BC" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id   = "${aws_api_gateway_resource.TBT02_BMSDownload_BC.id}"
  http_method   = "POST"
  authorization = "CUSTOM"
  authorizer_id = "${aws_api_gateway_authorizer.NonProd_Auth0_MVR.id}"
}

resource "aws_api_gateway_integration" "TBT02_BMSDownload_BC" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id             = "${aws_api_gateway_resource.TBT02_BMSDownload_BC.id}"
  http_method             = "${aws_api_gateway_method.TBT02_BMSDownload_BC.http_method}"
  integration_http_method = "POST"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-tbt02.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.tbt02.gore.insurcloud.ca/bms-download-service/v1/bmsdownload/bcpayload"
}

resource "aws_api_gateway_method_response" "TBT02_BMSDownload_BC" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id = "${aws_api_gateway_resource.TBT02_BMSDownload_BC.id}"
  http_method = "${aws_api_gateway_method.TBT02_BMSDownload_BC.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}

###Document Service###

resource "aws_api_gateway_resource" "TBT02_Document" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.TBT02_AUTH.id}"
  path_part   = "TBT02_Document"
}

# Document Search
resource "aws_api_gateway_resource" "TBT02_Document_Search" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.TBT02_Document.id}"
  path_part   = "TBT02_Document_Search"
}

resource "aws_api_gateway_method" "TBT02_Document_Search" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id   = "${aws_api_gateway_resource.TBT02_Document_Search.id}"
  http_method   = "POST"
  authorization = "CUSTOM"
  authorizer_id = "${aws_api_gateway_authorizer.NonProd_Auth0_HCAI.id}"
}

resource "aws_api_gateway_integration" "TBT02_Document_Search" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id             = "${aws_api_gateway_resource.TBT02_Document_Search.id}"
  http_method             = "${aws_api_gateway_method.TBT02_Document_Search.http_method}"
  integration_http_method = "POST"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-tbt02.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.tbt02.gore.insurcloud.ca/document-service/v1/document/gore/summaries/search/metadata"
}

resource "aws_api_gateway_method_response" "TBT02_Document_Search" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id = "${aws_api_gateway_resource.TBT02_Document_Search.id}"
  http_method = "${aws_api_gateway_method.TBT02_Document_Search.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}
# Document Retrieve
resource "aws_api_gateway_resource" "TBT02_Document_Retrieve" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.TBT02_Document.id}"
  path_part   = "TBT02_Document_Retrieve"
}

resource "aws_api_gateway_method" "TBT02_Document_Retrieve" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id   = "${aws_api_gateway_resource.TBT02_Document_Retrieve.id}"
  http_method   = "POST"
  authorization = "CUSTOM"
  authorizer_id = "${aws_api_gateway_authorizer.NonProd_Auth0_HCAI.id}"
}

resource "aws_api_gateway_integration" "TBT02_Document_Retrieve" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id             = "${aws_api_gateway_resource.TBT02_Document_Retrieve.id}"
  http_method             = "${aws_api_gateway_method.TBT02_Document_Retrieve.http_method}"
  integration_http_method = "POST"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-tbt02.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.tbt02.gore.insurcloud.ca/document-service/v1/document/gore/retrieve/key"
}

resource "aws_api_gateway_method_response" "TBT02_Document_Retrieve" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id = "${aws_api_gateway_resource.TBT02_Document_Retrieve.id}"
  http_method = "${aws_api_gateway_method.TBT02_Document_Retrieve.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}

# Document Upload1
resource "aws_api_gateway_resource" "TBT02_Document_Upload1" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.TBT02_Document.id}"
  path_part   = "TBT02_Document_Upload1"
}

resource "aws_api_gateway_method" "TBT02_Document_Upload1" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id   = "${aws_api_gateway_resource.TBT02_Document_Upload1.id}"
  http_method   = "POST"
  authorization = "CUSTOM"
  authorizer_id = "${aws_api_gateway_authorizer.NonProd_Auth0_HCAI.id}"
}

resource "aws_api_gateway_integration" "TBT02_Document_Upload1" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id             = "${aws_api_gateway_resource.TBT02_Document_Upload1.id}"
  http_method             = "${aws_api_gateway_method.TBT02_Document_Upload1.http_method}"
  integration_http_method = "POST"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-tbt02.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.tbt02.gore.insurcloud.ca/document-service/v1/document/gore/base64/compose"
}

resource "aws_api_gateway_method_response" "TBT02_Document_Upload1" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id = "${aws_api_gateway_resource.TBT02_Document_Upload1.id}"
  http_method = "${aws_api_gateway_method.TBT02_Document_Upload1.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}

# Document Upload2
resource "aws_api_gateway_resource" "TBT02_Document_Upload2" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.TBT02_Document.id}"
  path_part   = "TBT02_Document_Upload2"
}

resource "aws_api_gateway_method" "TBT02_Document_Upload2" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id   = "${aws_api_gateway_resource.TBT02_Document_Upload2.id}"
  http_method   = "POST"
  authorization = "CUSTOM"
  authorizer_id = "${aws_api_gateway_authorizer.NonProd_Auth0_HCAI.id}"
}

resource "aws_api_gateway_integration" "TBT02_Document_Upload2" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id             = "${aws_api_gateway_resource.TBT02_Document_Upload2.id}"
  http_method             = "${aws_api_gateway_method.TBT02_Document_Upload2.http_method}"
  integration_http_method = "POST"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-tbt02.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.tbt02.gore.insurcloud.ca/document-service/v1/document/gore/save/document/payload"
}

resource "aws_api_gateway_method_response" "TBT02_Document_Upload2" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id = "${aws_api_gateway_resource.TBT02_Document_Upload2.id}"
  http_method = "${aws_api_gateway_method.TBT02_Document_Upload2.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}
###CRC###

resource "aws_api_gateway_resource" "TBT02_CRC" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.TBT02_AUTH.id}"
  path_part   = "TBT02_CRC"
}
resource "aws_api_gateway_resource" "TBT02_CRCCreateFNOL" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.TBT02_CRC.id}"
  path_part   = "TBT02_CRCCreateFNOL"
}


#CRC --> Guidewire
resource "aws_api_gateway_method" "TBT02_CRCCreateFNOL" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id   = "${aws_api_gateway_resource.TBT02_CRCCreateFNOL.id}"
  http_method   = "POST"
  authorization = "CUSTOM"
  authorizer_id = "${aws_api_gateway_authorizer.NonProd_Auth0_CRC.id}"
}

resource "aws_api_gateway_integration" "TBT02_CRCCreateFNOL" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id             = "${aws_api_gateway_resource.TBT02_CRCCreateFNOL.id}"
  http_method             = "${aws_api_gateway_method.TBT02_CRCCreateFNOL.http_method}"
  integration_http_method = "POST"
  connection_type         = "INTERNET"
  type                    = "HTTP_PROXY"
  uri                     = "https://tbt2.goredev.guidewire.net/cc/ws/ic/integration/crc/CollisionReportingCenterAPI"
}

resource "aws_api_gateway_method_response" "TBT02_CRCCreateFNOL" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id = "${aws_api_gateway_resource.TBT02_CRCCreateFNOL.id}"
  http_method = "${aws_api_gateway_method.TBT02_CRCCreateFNOL.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}

#CRC --> MS 

resource "aws_api_gateway_resource" "TBT02_CRCCollisionDocumentsMS" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.TBT02_CRC.id}"
  path_part   = "TBT02_CRCCollisionDocumentsMS"
}

resource "aws_api_gateway_method" "TBT02_CRCCollisionDocumentsMS" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id   = "${aws_api_gateway_resource.TBT02_CRCCollisionDocumentsMS.id}"
  http_method   = "POST"
  authorization = "CUSTOM"
  authorizer_id = "${aws_api_gateway_authorizer.NonProd_Auth0_CRC.id}"
}

resource "aws_api_gateway_integration" "TBT02_CRCCollisionDocumentsMS" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id             = "${aws_api_gateway_resource.TBT02_CRCCollisionDocumentsMS.id}"
  http_method             = "${aws_api_gateway_method.TBT02_CRCCollisionDocumentsMS.http_method}"
  integration_http_method = "POST"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-tbt02.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.tbt02.gore.insurcloud.ca/crc-service/microservices/crc/ws/"
}

resource "aws_api_gateway_method_response" "TBT02_CRCCollisionDocumentsMS" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id = "${aws_api_gateway_resource.TBT02_CRCCollisionDocumentsMS.id}"
  http_method = "${aws_api_gateway_method.TBT02_CRCCollisionDocumentsMS.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}

####Audatex###

# Audatex Inbount Acknowledgment - ARMS

resource "aws_api_gateway_resource" "TBT02_AudatexINB" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.TBT02_AUTH.id}"
  path_part   = "TBT02_AudatexINB"
}
resource "aws_api_gateway_resource" "TBT02_AudatexAssignmentAck" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.TBT02_AudatexINB.id}"
  path_part   = "TBT02_AudatexAssignmentAck"
}

resource "aws_api_gateway_method" "TBT02_AudatexAssignmentAck" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id   = "${aws_api_gateway_resource.TBT02_AudatexAssignmentAck.id}"
  http_method   = "POST"
  authorization = "CUSTOM"
  authorizer_id = "${aws_api_gateway_authorizer.NonProd_Auth0_Audatex.id}"
}

resource "aws_api_gateway_integration" "TBT02_AudatexAssignmentAck" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id             = "${aws_api_gateway_resource.TBT02_AudatexAssignmentAck.id}"
  http_method             = "${aws_api_gateway_method.TBT02_AudatexAssignmentAck.http_method}"
  integration_http_method = "POST"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-tbt02.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.tbt02.gore.insurcloud.ca/audatex-service/v1/assignment/ack"
}

resource "aws_api_gateway_method_response" "TBT02_AudatexAssignmentAck" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id = "${aws_api_gateway_resource.TBT02_AudatexAssignmentAck.id}"
  http_method = "${aws_api_gateway_method.TBT02_AudatexAssignmentAck.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}

##Audatex Inbound estimate
resource "aws_api_gateway_resource" "TBT02_AudatexEstimate" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.TBT02_AudatexINB.id}"
  path_part   = "TBT02_AudatexEstimate"
}

resource "aws_api_gateway_method" "TBT02_AudatexEstimate" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id   = "${aws_api_gateway_resource.TBT02_AudatexEstimate.id}"
  http_method   = "POST"
  authorization = "CUSTOM"
  authorizer_id = "${aws_api_gateway_authorizer.NonProd_Auth0_Audatex.id}"
}


resource "aws_api_gateway_integration" "TBT02_AudatexEstimate" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id             = "${aws_api_gateway_resource.TBT02_AudatexEstimate.id}"
  http_method             = "${aws_api_gateway_method.TBT02_AudatexEstimate.http_method}"
  integration_http_method = "POST"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-tbt02.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.tbt02.gore.insurcloud.ca/audatex-service/v1/estimate"
}

resource "aws_api_gateway_method_response" "TBT02_AudatexEstimate" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id = "${aws_api_gateway_resource.TBT02_AudatexEstimate.id}"
  http_method = "${aws_api_gateway_method.TBT02_AudatexEstimate.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}
####end of audatex#####


# ###Uniban###
resource "aws_api_gateway_resource" "TBT02_Uniban_Get" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.TBT02_AUTH.id}"
  path_part   = "TBT02_Uniban_Get"
}

##Uniban --> API
resource "aws_api_gateway_method" "TBT02_Uniban_Get" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id   = "${aws_api_gateway_resource.TBT02_Uniban_Get.id}"
  http_method   = "GET"
  authorization = "CUSTOM"
  authorizer_id = "${aws_api_gateway_authorizer.NonProd_Auth0_UnibanService.id}"
  request_parameters = {
    "method.request.querystring.policyNumber"= true
    "method.request.querystring.dateOfLoss"= true
    "method.request.querystring.vin"= true
  }
}

resource "aws_api_gateway_integration" "TBT02_Uniban_Get" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id             = "${aws_api_gateway_resource.TBT02_Uniban_Get.id}"
  http_method             = "${aws_api_gateway_method.TBT02_Uniban_Get.http_method}"
  integration_http_method = "GET"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-tbt02.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.tbt02.gore.insurcloud.ca/uniban-service/v1/retrievePolicy"
}

resource "aws_api_gateway_method_response" "TBT02_Uniban_Get" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id = "${aws_api_gateway_resource.TBT02_Uniban_Get.id}"
  http_method = "${aws_api_gateway_method.TBT02_Uniban_Get.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}

###IBC Vehicle MicroService###

resource "aws_api_gateway_resource" "TBT02_IBC" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.TBT02_AUTH.id}"
  path_part   = "TBT02_IBC"
}

### IBC Vehicle Endpoint 1
resource "aws_api_gateway_resource" "TBT02_vininfo" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.TBT02_IBC.id}"
  path_part   = "TBT02_vininfo"
}

resource "aws_api_gateway_method" "TBT02_vininfo" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id   = "${aws_api_gateway_resource.TBT02_vininfo.id}"
  http_method   = "GET"
  authorization = "CUSTOM"
  authorizer_id = "${aws_api_gateway_authorizer.NonProd_Auth0_Vehicle.id}"
  request_parameters = {
    "method.request.querystring.vin"= true
  }
}

resource "aws_api_gateway_integration" "TBT02_vininfo" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id             = "${aws_api_gateway_resource.TBT02_vininfo.id}"
  http_method             = "${aws_api_gateway_method.TBT02_vininfo.http_method}"
  integration_http_method = "GET"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-tbt02.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.tbt02.gore.insurcloud.ca/vehicle-info-service/v1/vininfo"
}

resource "aws_api_gateway_method_response" "TBT02_vininfo" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id = "${aws_api_gateway_resource.TBT02_vininfo.id}"
  http_method = "${aws_api_gateway_method.TBT02_vininfo.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}
# IBC Vehicle Endpoint 2####
resource "aws_api_gateway_resource" "TBT02_submit" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.TBT02_IBC.id}"
  path_part   = "TBT02_submit"
}

resource "aws_api_gateway_method" "TBT02_submit" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id   = "${aws_api_gateway_resource.TBT02_submit.id}"
  http_method   = "POST"
  authorization = "CUSTOM"
  authorizer_id = "${aws_api_gateway_authorizer.NonProd_Auth0_Vehicle.id}"
}

resource "aws_api_gateway_integration" "TBT02_submit" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id             = "${aws_api_gateway_resource.TBT02_submit.id}"
  http_method             = "${aws_api_gateway_method.TBT02_submit.http_method}"
  integration_http_method = "POST"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-tbt02.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.tbt02.gore.insurcloud.ca/vehicle-info-service/v1/product/download/submit"
}

resource "aws_api_gateway_method_response" "TBT02_submit" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id = "${aws_api_gateway_resource.TBT02_submit.id}"
  http_method = "${aws_api_gateway_method.TBT02_submit.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}

# IBC Vehicle ppvinfo
resource "aws_api_gateway_resource" "TBT02_ppvinfo" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.TBT02_IBC.id}"
  path_part   = "TBT02_ppvinfo"
}

resource "aws_api_gateway_method" "TBT02_ppvinfo" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id   = "${aws_api_gateway_resource.TBT02_ppvinfo.id}"
  http_method   = "GET"
  authorization = "CUSTOM"
  authorizer_id = "${aws_api_gateway_authorizer.NonProd_Auth0_Vehicle.id}"
  request_parameters = {
    "method.request.querystring.rateGroupPublicationYear"= true
    "method.request.querystring.carCode"= true
    "method.request.querystring.modelYear"= true
  }
}

resource "aws_api_gateway_integration" "TBT02_ppvinfo" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id             = "${aws_api_gateway_resource.TBT02_ppvinfo.id}"
  http_method             = "${aws_api_gateway_method.TBT02_ppvinfo.http_method}"
  integration_http_method = "GET"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-tbt02.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.tbt02.gore.insurcloud.ca/vehicle-info-service/v1/ppvinfo"
}

resource "aws_api_gateway_method_response" "TBT02_ppvinfo" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id = "${aws_api_gateway_resource.TBT02_ppvinfo.id}"
  http_method = "${aws_api_gateway_method.TBT02_ppvinfo.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}

# IBC Vehicle recinfo
resource "aws_api_gateway_resource" "TBT02_recinfo" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.TBT02_IBC.id}"
  path_part   = "TBT02_recinfo"
}

resource "aws_api_gateway_method" "TBT02_recinfo" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id   = "${aws_api_gateway_resource.TBT02_recinfo.id}"
  http_method   = "GET"
  authorization = "CUSTOM"
  authorizer_id = "${aws_api_gateway_authorizer.NonProd_Auth0_Vehicle.id}"
  request_parameters = {
    "method.request.querystring.publicationYear"= true
    "method.request.querystring.vehicleType"= true
    "method.request.querystring.vehicleMake"= true
    "method.request.querystring.vehicleModel"= true
    "method.request.querystring.modelYear"= true
 }
}

resource "aws_api_gateway_integration" "TBT02_recinfo" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id             = "${aws_api_gateway_resource.TBT02_recinfo.id}"
  http_method             = "${aws_api_gateway_method.TBT02_recinfo.http_method}"
  integration_http_method = "GET"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-tbt02.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.tbt02.gore.insurcloud.ca/vehicle-info-service/v1/recinfo"
}

resource "aws_api_gateway_method_response" "TBT02_recinfo" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id = "${aws_api_gateway_resource.TBT02_recinfo.id}"
  http_method = "${aws_api_gateway_method.TBT02_recinfo.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}
######### RESOURCE GROUPS ##############
#########BAT#########
resource "aws_api_gateway_resource" "BAT_AUTH" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_rest_api.GORE_AUTH0.root_resource_id}"
  path_part   = "BAT_AUTH"
}
#####Address Validation#####
resource "aws_api_gateway_resource" "BAT_Address" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.BAT_AUTH.id}"
  path_part   = "BAT_Address"
}

# Address Outbound GW -API Gateway - ARMS
resource "aws_api_gateway_method" "BAT_Address" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id   = "${aws_api_gateway_resource.BAT_Address.id}"
  http_method   = "POST"
  authorization = "CUSTOM"
  authorizer_id = "${aws_api_gateway_authorizer.NonProd_Auth0_AddressValidation.id}"
}

resource "aws_api_gateway_integration" "BAT_Address" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id             = "${aws_api_gateway_resource.BAT_Address.id}"
  http_method             = "${aws_api_gateway_method.BAT_Address.http_method}"
  integration_http_method = "POST"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-bat.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.bat.gore.insurcloud.ca/address-validation-service/microservices/addressvalidation/ws"
}

resource "aws_api_gateway_method_response" "BAT_Address" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id = "${aws_api_gateway_resource.BAT_Address.id}"
  http_method = "${aws_api_gateway_method.BAT_Address.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}
###HCAI###

resource "aws_api_gateway_resource" "BAT_HCAISecurity" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.BAT_AUTH.id}"
  path_part   = "BAT_HCAISecurity"
}

# HCAI Outbound GW -API Gateway - ARMS
resource "aws_api_gateway_method" "BAT_HCAISecurity" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id   = "${aws_api_gateway_resource.BAT_HCAISecurity.id}"
  http_method   = "POST"
  authorization = "CUSTOM"
  authorizer_id = "${aws_api_gateway_authorizer.NonProd_Auth0_HCAI.id}"
}

resource "aws_api_gateway_integration" "BAT_HCAISecurity" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id             = "${aws_api_gateway_resource.BAT_HCAISecurity.id}"
  http_method             = "${aws_api_gateway_method.BAT_HCAISecurity.http_method}"
  integration_http_method = "POST"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-bat.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.bat.gore.insurcloud.ca/soap-security-service/v1/proxy"
}

resource "aws_api_gateway_method_response" "BAT_HCAISecurity" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id = "${aws_api_gateway_resource.BAT_HCAISecurity.id}"
  http_method = "${aws_api_gateway_method.BAT_HCAISecurity.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}

###Cheque Printing#####
resource "aws_api_gateway_resource" "BAT_ChequePrinting" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.BAT_AUTH.id}"
  path_part   = "BAT_ChequePrinting"
}
# #Cheque Outbound#####
resource "aws_api_gateway_resource" "BAT_SaveCheque" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.BAT_ChequePrinting.id}"
  path_part   = "BAT_SaveCheque"
}

resource "aws_api_gateway_method" "BAT_SaveCheque" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id   = "${aws_api_gateway_resource.BAT_SaveCheque.id}"
  http_method   = "POST"
  authorization = "CUSTOM"
  authorizer_id = "${aws_api_gateway_authorizer.NonProd_Auth0_ChequePrinting.id}"
}

resource "aws_api_gateway_integration" "BAT_SaveCheque" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id             = "${aws_api_gateway_resource.BAT_SaveCheque.id}"
  http_method             = "${aws_api_gateway_method.BAT_SaveCheque.http_method}"
  integration_http_method = "POST"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-bat.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.bat.gore.insurcloud.ca/cheque-service/v1/cheque/print"
}

resource "aws_api_gateway_method_response" "BAT_SaveCheque" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id = "${aws_api_gateway_resource.BAT_SaveCheque.id}"
  http_method = "${aws_api_gateway_method.BAT_SaveCheque.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}
# #Cheque --> F & 0 --> MS#####
resource "aws_api_gateway_resource" "BAT_ChequeUpdateNumbers" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.BAT_ChequePrinting.id}"
  path_part   = "BAT_ChequeUpdateNumbers"
}

resource "aws_api_gateway_method" "BAT_ChequeUpdateNumbers" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id   = "${aws_api_gateway_resource.BAT_ChequeUpdateNumbers.id}"
  http_method   = "POST"
  authorization = "CUSTOM"
  authorizer_id = "${aws_api_gateway_authorizer.NonProd_Auth0_ChequePrinting.id}"
}

resource "aws_api_gateway_integration" "BAT_ChequeUpdateNumbers" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id             = "${aws_api_gateway_resource.BAT_ChequeUpdateNumbers.id}"
  http_method             = "${aws_api_gateway_method.BAT_ChequeUpdateNumbers.http_method}"
  integration_http_method = "POST"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-bat.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.bat.gore.insurcloud.ca/cheque-service/v1/cheque/updateChequeNumbers"
}

resource "aws_api_gateway_method_response" "BAT_ChequeUpdateNumbers" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id = "${aws_api_gateway_resource.BAT_ChequeUpdateNumbers.id}"
  http_method = "${aws_api_gateway_method.BAT_ChequeUpdateNumbers.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}

# #MVR Service#####
resource "aws_api_gateway_resource" "BAT_MVRDriverInfo" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.BAT_AUTH.id}"
  path_part   = "BAT_MVRDriverInfo"
}

resource "aws_api_gateway_method" "BAT_MVRDriverInfo" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id   = "${aws_api_gateway_resource.BAT_MVRDriverInfo.id}"
  http_method   = "POST"
  authorization = "CUSTOM"
  authorizer_id = "${aws_api_gateway_authorizer.NonProd_Auth0_MVR.id}"
}

resource "aws_api_gateway_integration" "BAT_MVRDriverInfo" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id             = "${aws_api_gateway_resource.BAT_MVRDriverInfo.id}"
  http_method             = "${aws_api_gateway_method.BAT_MVRDriverInfo.http_method}"
  integration_http_method = "POST"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-bat.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.bat.gore.insurcloud.ca/mvr-service/v1/driver/info"
}

resource "aws_api_gateway_method_response" "BAT_MVRDriverInfo" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id = "${aws_api_gateway_resource.BAT_MVRDriverInfo.id}"
  http_method = "${aws_api_gateway_method.BAT_MVRDriverInfo.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}

#######Download service#####
resource "aws_api_gateway_resource" "BAT_BMSDownload" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.BAT_AUTH.id}"
  path_part   = "BAT_BMSDownload"
}

# #Download Service for PC payload#####
resource "aws_api_gateway_resource" "BAT_BMSDownload_PC" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.BAT_AUTH.id}"
  path_part   = "BAT_BMSDownload_PC"
}

resource "aws_api_gateway_method" "BAT_BMSDownload_PC" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id   = "${aws_api_gateway_resource.BAT_BMSDownload_PC.id}"
  http_method   = "POST"
  authorization = "CUSTOM"
  authorizer_id = "${aws_api_gateway_authorizer.NonProd_Auth0_MVR.id}"
}

resource "aws_api_gateway_integration" "BAT_BMSDownload_PC" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id             = "${aws_api_gateway_resource.BAT_BMSDownload_PC.id}"
  http_method             = "${aws_api_gateway_method.BAT_BMSDownload_PC.http_method}"
  integration_http_method = "POST"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-bat.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.bat.gore.insurcloud.ca/bms-download-service/v1/bmsdownload/pcpayload"
}

resource "aws_api_gateway_method_response" "BAT_BMSDownload_PC" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id = "${aws_api_gateway_resource.BAT_BMSDownload_PC.id}"
  http_method = "${aws_api_gateway_method.BAT_BMSDownload_PC.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}

# #Download Service for BC payload#####
resource "aws_api_gateway_resource" "BAT_BMSDownload_BC" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.BAT_BMSDownload.id}"
  path_part   = "BAT_BMSDownload_BC"
}

resource "aws_api_gateway_method" "BAT_BMSDownload_BC" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id   = "${aws_api_gateway_resource.BAT_BMSDownload_BC.id}"
  http_method   = "POST"
  authorization = "CUSTOM"
  authorizer_id = "${aws_api_gateway_authorizer.NonProd_Auth0_MVR.id}"
}

resource "aws_api_gateway_integration" "BAT_BMSDownload_BC" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id             = "${aws_api_gateway_resource.BAT_BMSDownload_BC.id}"
  http_method             = "${aws_api_gateway_method.BAT_BMSDownload_BC.http_method}"
  integration_http_method = "POST"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-bat.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.bat.gore.insurcloud.ca/bms-download-service/v1/bmsdownload/bcpayload"
}

resource "aws_api_gateway_method_response" "BAT_BMSDownload_BC" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id = "${aws_api_gateway_resource.BAT_BMSDownload_BC.id}"
  http_method = "${aws_api_gateway_method.BAT_BMSDownload_BC.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}

###Document Service###

resource "aws_api_gateway_resource" "BAT_Document" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.BAT_AUTH.id}"
  path_part   = "BAT_Document"
}

# Document Search
resource "aws_api_gateway_resource" "BAT_Document_Search" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.BAT_Document.id}"
  path_part   = "BAT_Document_Search"
}

resource "aws_api_gateway_method" "BAT_Document_Search" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id   = "${aws_api_gateway_resource.BAT_Document_Search.id}"
  http_method   = "POST"
  authorization = "CUSTOM"
  authorizer_id = "${aws_api_gateway_authorizer.NonProd_Auth0_HCAI.id}"
}

resource "aws_api_gateway_integration" "BAT_Document_Search" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id             = "${aws_api_gateway_resource.BAT_Document_Search.id}"
  http_method             = "${aws_api_gateway_method.BAT_Document_Search.http_method}"
  integration_http_method = "POST"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-bat.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.bat.gore.insurcloud.ca/document-service/v1/document/gore/summaries/search/metadata"
}

resource "aws_api_gateway_method_response" "BAT_Document_Search" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id = "${aws_api_gateway_resource.BAT_Document_Search.id}"
  http_method = "${aws_api_gateway_method.BAT_Document_Search.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}
# Document Retrieve
resource "aws_api_gateway_resource" "BAT_Document_Retrieve" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.BAT_Document.id}"
  path_part   = "BAT_Document_Retrieve"
}

resource "aws_api_gateway_method" "BAT_Document_Retrieve" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id   = "${aws_api_gateway_resource.BAT_Document_Retrieve.id}"
  http_method   = "POST"
  authorization = "CUSTOM"
  authorizer_id = "${aws_api_gateway_authorizer.NonProd_Auth0_HCAI.id}"
}

resource "aws_api_gateway_integration" "BAT_Document_Retrieve" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id             = "${aws_api_gateway_resource.BAT_Document_Retrieve.id}"
  http_method             = "${aws_api_gateway_method.BAT_Document_Retrieve.http_method}"
  integration_http_method = "POST"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-bat.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.bat.gore.insurcloud.ca/document-service/v1/document/gore/retrieve/key"
}

resource "aws_api_gateway_method_response" "BAT_Document_Retrieve" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id = "${aws_api_gateway_resource.BAT_Document_Retrieve.id}"
  http_method = "${aws_api_gateway_method.BAT_Document_Retrieve.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}

# Document Upload1
resource "aws_api_gateway_resource" "BAT_Document_Upload1" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.BAT_Document.id}"
  path_part   = "BAT_Document_Upload1"
}

resource "aws_api_gateway_method" "BAT_Document_Upload1" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id   = "${aws_api_gateway_resource.BAT_Document_Upload1.id}"
  http_method   = "POST"
  authorization = "CUSTOM"
  authorizer_id = "${aws_api_gateway_authorizer.NonProd_Auth0_HCAI.id}"
}

resource "aws_api_gateway_integration" "BAT_Document_Upload1" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id             = "${aws_api_gateway_resource.BAT_Document_Upload1.id}"
  http_method             = "${aws_api_gateway_method.BAT_Document_Upload1.http_method}"
  integration_http_method = "POST"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-bat.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.bat.gore.insurcloud.ca/document-service/v1/document/gore/base64/compose"
}

resource "aws_api_gateway_method_response" "BAT_Document_Upload1" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id = "${aws_api_gateway_resource.BAT_Document_Upload1.id}"
  http_method = "${aws_api_gateway_method.BAT_Document_Upload1.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}

# Document Upload2
resource "aws_api_gateway_resource" "BAT_Document_Upload2" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.BAT_Document.id}"
  path_part   = "BAT_Document_Upload2"
}

resource "aws_api_gateway_method" "BAT_Document_Upload2" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id   = "${aws_api_gateway_resource.BAT_Document_Upload2.id}"
  http_method   = "POST"
  authorization = "CUSTOM"
  authorizer_id = "${aws_api_gateway_authorizer.NonProd_Auth0_HCAI.id}"
}

resource "aws_api_gateway_integration" "BAT_Document_Upload2" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id             = "${aws_api_gateway_resource.BAT_Document_Upload2.id}"
  http_method             = "${aws_api_gateway_method.BAT_Document_Upload2.http_method}"
  integration_http_method = "POST"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-bat.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.bat.gore.insurcloud.ca/document-service/v1/document/gore/save/document/payload"
}

resource "aws_api_gateway_method_response" "BAT_Document_Upload2" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id = "${aws_api_gateway_resource.BAT_Document_Upload2.id}"
  http_method = "${aws_api_gateway_method.BAT_Document_Upload2.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}
###CRC###

resource "aws_api_gateway_resource" "BAT_CRC" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.BAT_AUTH.id}"
  path_part   = "BAT_CRC"
}
resource "aws_api_gateway_resource" "BAT_CRCCreateFNOL" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.BAT_CRC.id}"
  path_part   = "BAT_CRCCreateFNOL"
}


#CRC --> Guidewire
resource "aws_api_gateway_method" "BAT_CRCCreateFNOL" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id   = "${aws_api_gateway_resource.BAT_CRCCreateFNOL.id}"
  http_method   = "POST"
  authorization = "CUSTOM"
  authorizer_id = "${aws_api_gateway_authorizer.NonProd_Auth0_CRC.id}"
}

resource "aws_api_gateway_integration" "BAT_CRCCreateFNOL" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id             = "${aws_api_gateway_resource.BAT_CRCCreateFNOL.id}"
  http_method             = "${aws_api_gateway_method.BAT_CRCCreateFNOL.http_method}"
  integration_http_method = "POST"
  connection_type         = "INTERNET"
  type                    = "HTTP_PROXY"
  uri                     = "https://bat.goredev.guidewire.net/cc/ws/ic/integration/crc/CollisionReportingCenterAPI"
}

resource "aws_api_gateway_method_response" "BAT_CRCCreateFNOL" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id = "${aws_api_gateway_resource.BAT_CRCCreateFNOL.id}"
  http_method = "${aws_api_gateway_method.BAT_CRCCreateFNOL.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}

#CRC --> MS 

resource "aws_api_gateway_resource" "BAT_CRCCollisionDocumentsMS" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.BAT_CRC.id}"
  path_part   = "BAT_CRCCollisionDocumentsMS"
}

resource "aws_api_gateway_method" "BAT_CRCCollisionDocumentsMS" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id   = "${aws_api_gateway_resource.BAT_CRCCollisionDocumentsMS.id}"
  http_method   = "POST"
  authorization = "CUSTOM"
  authorizer_id = "${aws_api_gateway_authorizer.NonProd_Auth0_CRC.id}"
}

resource "aws_api_gateway_integration" "BAT_CRCCollisionDocumentsMS" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id             = "${aws_api_gateway_resource.BAT_CRCCollisionDocumentsMS.id}"
  http_method             = "${aws_api_gateway_method.BAT_CRCCollisionDocumentsMS.http_method}"
  integration_http_method = "POST"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-bat.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.bat.gore.insurcloud.ca/crc-service/microservices/crc/ws/"
}

resource "aws_api_gateway_method_response" "BAT_CRCCollisionDocumentsMS" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id = "${aws_api_gateway_resource.BAT_CRCCollisionDocumentsMS.id}"
  http_method = "${aws_api_gateway_method.BAT_CRCCollisionDocumentsMS.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}

####Audatex###

# Audatex Inbount Acknowledgment - ARMS

resource "aws_api_gateway_resource" "BAT_AudatexINB" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.BAT_AUTH.id}"
  path_part   = "BAT_AudatexINB"
}
resource "aws_api_gateway_resource" "BAT_AudatexAssignmentAck" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.BAT_AudatexINB.id}"
  path_part   = "BAT_AudatexAssignmentAck"
}

resource "aws_api_gateway_method" "BAT_AudatexAssignmentAck" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id   = "${aws_api_gateway_resource.BAT_AudatexAssignmentAck.id}"
  http_method   = "POST"
  authorization = "CUSTOM"
  authorizer_id = "${aws_api_gateway_authorizer.NonProd_Auth0_Audatex.id}"
}

resource "aws_api_gateway_integration" "BAT_AudatexAssignmentAck" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id             = "${aws_api_gateway_resource.BAT_AudatexAssignmentAck.id}"
  http_method             = "${aws_api_gateway_method.BAT_AudatexAssignmentAck.http_method}"
  integration_http_method = "POST"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-bat.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.bat.gore.insurcloud.ca/audatex-service/v1/assignment/ack"
}

resource "aws_api_gateway_method_response" "BAT_AudatexAssignmentAck" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id = "${aws_api_gateway_resource.BAT_AudatexAssignmentAck.id}"
  http_method = "${aws_api_gateway_method.BAT_AudatexAssignmentAck.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}

##Audatex Inbound estimate
resource "aws_api_gateway_resource" "BAT_AudatexEstimate" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.BAT_AudatexINB.id}"
  path_part   = "BAT_AudatexEstimate"
}

resource "aws_api_gateway_method" "BAT_AudatexEstimate" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id   = "${aws_api_gateway_resource.BAT_AudatexEstimate.id}"
  http_method   = "POST"
  authorization = "CUSTOM"
  authorizer_id = "${aws_api_gateway_authorizer.NonProd_Auth0_Audatex.id}"
}


resource "aws_api_gateway_integration" "BAT_AudatexEstimate" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id             = "${aws_api_gateway_resource.BAT_AudatexEstimate.id}"
  http_method             = "${aws_api_gateway_method.BAT_AudatexEstimate.http_method}"
  integration_http_method = "POST"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-bat.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.bat.gore.insurcloud.ca/audatex-service/v1/estimate"
}

resource "aws_api_gateway_method_response" "BAT_AudatexEstimate" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id = "${aws_api_gateway_resource.BAT_AudatexEstimate.id}"
  http_method = "${aws_api_gateway_method.BAT_AudatexEstimate.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}
####end of audatex#####


# ###Uniban###
resource "aws_api_gateway_resource" "BAT_Uniban_Get" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.BAT_AUTH.id}"
  path_part   = "BAT_Uniban_Get"
}

##Uniban --> API
resource "aws_api_gateway_method" "BAT_Uniban_Get" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id   = "${aws_api_gateway_resource.BAT_Uniban_Get.id}"
  http_method   = "GET"
  authorization = "CUSTOM"
  authorizer_id = "${aws_api_gateway_authorizer.NonProd_Auth0_UnibanService.id}"
  request_parameters = {
    "method.request.querystring.policyNumber"= true
    "method.request.querystring.dateOfLoss"= true
    "method.request.querystring.vin"= true
  }
}

resource "aws_api_gateway_integration" "BAT_Uniban_Get" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id             = "${aws_api_gateway_resource.BAT_Uniban_Get.id}"
  http_method             = "${aws_api_gateway_method.BAT_Uniban_Get.http_method}"
  integration_http_method = "GET"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-bat.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.bat.gore.insurcloud.ca/uniban-service/v1/retrievePolicy"
}

resource "aws_api_gateway_method_response" "BAT_Uniban_Get" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id = "${aws_api_gateway_resource.BAT_Uniban_Get.id}"
  http_method = "${aws_api_gateway_method.BAT_Uniban_Get.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}

###IBC Vehicle MicroService###

resource "aws_api_gateway_resource" "BAT_IBC" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.BAT_AUTH.id}"
  path_part   = "BAT_IBC"
}

### IBC Vehicle Endpoint 1
resource "aws_api_gateway_resource" "BAT_vininfo" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.BAT_IBC.id}"
  path_part   = "BAT_vininfo"
}

resource "aws_api_gateway_method" "BAT_vininfo" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id   = "${aws_api_gateway_resource.BAT_vininfo.id}"
  http_method   = "GET"
  authorization = "CUSTOM"
  authorizer_id = "${aws_api_gateway_authorizer.NonProd_Auth0_Vehicle.id}"
  request_parameters = {
    "method.request.querystring.vin"= true
  }
}

resource "aws_api_gateway_integration" "BAT_vininfo" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id             = "${aws_api_gateway_resource.BAT_vininfo.id}"
  http_method             = "${aws_api_gateway_method.BAT_vininfo.http_method}"
  integration_http_method = "GET"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-bat.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.bat.gore.insurcloud.ca/vehicle-info-service/v1/vininfo"
}

resource "aws_api_gateway_method_response" "BAT_vininfo" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id = "${aws_api_gateway_resource.BAT_vininfo.id}"
  http_method = "${aws_api_gateway_method.BAT_vininfo.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}
# IBC Vehicle Endpoint 2####
resource "aws_api_gateway_resource" "BAT_submit" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.BAT_IBC.id}"
  path_part   = "BAT_submit"
}

resource "aws_api_gateway_method" "BAT_submit" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id   = "${aws_api_gateway_resource.BAT_submit.id}"
  http_method   = "POST"
  authorization = "CUSTOM"
  authorizer_id = "${aws_api_gateway_authorizer.NonProd_Auth0_Vehicle.id}"
}

resource "aws_api_gateway_integration" "BAT_submit" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id             = "${aws_api_gateway_resource.BAT_submit.id}"
  http_method             = "${aws_api_gateway_method.BAT_submit.http_method}"
  integration_http_method = "POST"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-bat.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.bat.gore.insurcloud.ca/vehicle-info-service/v1/product/download/submit"
}

resource "aws_api_gateway_method_response" "BAT_submit" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id = "${aws_api_gateway_resource.BAT_submit.id}"
  http_method = "${aws_api_gateway_method.BAT_submit.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}

# IBC Vehicle ppvinfo
resource "aws_api_gateway_resource" "BAT_ppvinfo" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.BAT_IBC.id}"
  path_part   = "BAT_ppvinfo"
}

resource "aws_api_gateway_method" "BAT_ppvinfo" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id   = "${aws_api_gateway_resource.BAT_ppvinfo.id}"
  http_method   = "GET"
  authorization = "CUSTOM"
  authorizer_id = "${aws_api_gateway_authorizer.NonProd_Auth0_Vehicle.id}"
  request_parameters = {
    "method.request.querystring.rateGroupPublicationYear"= true
    "method.request.querystring.carCode"= true
    "method.request.querystring.modelYear"= true
  }
}

resource "aws_api_gateway_integration" "BAT_ppvinfo" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id             = "${aws_api_gateway_resource.BAT_ppvinfo.id}"
  http_method             = "${aws_api_gateway_method.BAT_ppvinfo.http_method}"
  integration_http_method = "GET"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-bat.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.bat.gore.insurcloud.ca/vehicle-info-service/v1/ppvinfo"
}

resource "aws_api_gateway_method_response" "BAT_ppvinfo" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id = "${aws_api_gateway_resource.BAT_ppvinfo.id}"
  http_method = "${aws_api_gateway_method.BAT_ppvinfo.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}

# IBC Vehicle recinfo
resource "aws_api_gateway_resource" "BAT_recinfo" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.BAT_IBC.id}"
  path_part   = "BAT_recinfo"
}

resource "aws_api_gateway_method" "BAT_recinfo" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id   = "${aws_api_gateway_resource.BAT_recinfo.id}"
  http_method   = "GET"
  authorization = "CUSTOM"
  authorizer_id = "${aws_api_gateway_authorizer.NonProd_Auth0_Vehicle.id}"
  request_parameters = {
    "method.request.querystring.publicationYear"= true
    "method.request.querystring.vehicleType"= true
    "method.request.querystring.vehicleMake"= true
    "method.request.querystring.vehicleModel"= true
    "method.request.querystring.modelYear"= true
 }
}

resource "aws_api_gateway_integration" "BAT_recinfo" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id             = "${aws_api_gateway_resource.BAT_recinfo.id}"
  http_method             = "${aws_api_gateway_method.BAT_recinfo.http_method}"
  integration_http_method = "GET"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-bat.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.bat.gore.insurcloud.ca/vehicle-info-service/v1/recinfo"
}

resource "aws_api_gateway_method_response" "BAT_recinfo" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id = "${aws_api_gateway_resource.BAT_recinfo.id}"
  http_method = "${aws_api_gateway_method.BAT_recinfo.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}

###########NO AUTHORIZATION API GATEWAYS############
resource "aws_api_gateway_rest_api" "GORE_IP" {
  name        = "GORE_IP"
  description = "This is the API gateway for NO AUTH (AUTHORIZATION SET TO NONE) Gateway Collection."
  endpoint_configuration {
    types = ["REGIONAL"]
  }
}
#########DEV01#########
resource "aws_api_gateway_resource" "DEV01_IP" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_IP.id}"
  parent_id   = "${aws_api_gateway_rest_api.GORE_IP.root_resource_id}"
  path_part   = "DEV01_IP"
}


###Impact###
resource "aws_api_gateway_resource" "DEV01_Impact_GW" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_IP.id}"
  parent_id   = "${aws_api_gateway_resource.DEV01_IP.id}"
  path_part   = "DEV01_Impact_GW"
}

resource "aws_api_gateway_method" "DEV01_Impact_GW" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_IP.id}"
  resource_id   = "${aws_api_gateway_resource.DEV01_Impact_GW.id}"
  http_method   = "POST"
  authorization = "NONE"
}
resource "aws_api_gateway_integration" "DEV01_Impact_GW" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE_IP.id}"
  resource_id             = "${aws_api_gateway_resource.DEV01_Impact_GW.id}"
  http_method             = "${aws_api_gateway_method.DEV01_Impact_GW.http_method}"
  integration_http_method = "POST"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-dev01.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.dev01.gore.insurcloud.ca/genericassignmentservice.svc"
}

resource "aws_api_gateway_method_response" "DEV01_Impact_GW" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_IP.id}"
  resource_id = "${aws_api_gateway_resource.DEV01_Impact_GW.id}"
  http_method = "${aws_api_gateway_method.DEV01_Impact_GW.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}
#Enterprise --> API gateway --> Enterprise MS --> GW & Document MS
resource "aws_api_gateway_resource" "DEV01_EnterpriseINB" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_IP.id}"
  parent_id   = "${aws_api_gateway_resource.DEV01_IP.id}"
  path_part   = "DEV01_EnterpriseINB"
}

resource "aws_api_gateway_resource" "DEV01_SendInvoiceNotification" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_IP.id}"
  parent_id   = "${aws_api_gateway_resource.DEV01_EnterpriseINB.id}"
  path_part   = "DEV01_SendInvoiceNotification"
}

#Update Notification request
resource "aws_api_gateway_method" "DEV01_SendInvoiceNotification" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE_IP.id}"
  resource_id   = "${aws_api_gateway_resource.DEV01_SendInvoiceNotification.id}"
  http_method   = "POST"
  authorization = "NONE"
}

resource "aws_api_gateway_integration" "DEV01_SendInvoiceNotification" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE_IP.id}"
  resource_id             = "${aws_api_gateway_resource.DEV01_SendInvoiceNotification.id}"
  http_method             = "${aws_api_gateway_method.DEV01_SendInvoiceNotification.http_method}"
  integration_http_method = "POST"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-dev01.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.dev01.gore.insurcloud.ca/car-rental-service/v1/rental/sendInvoiceNotificationRequest"
}

resource "aws_api_gateway_method_response" "DEV01_SendInvoiceNotification" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_IP.id}"
  resource_id = "${aws_api_gateway_resource.DEV01_SendInvoiceNotification.id}"
  http_method = "${aws_api_gateway_method.DEV01_SendInvoiceNotification.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}
# #DREAM PAYMENTS#####
resource "aws_api_gateway_resource" "DEV01_DreamPayStatusUpdate" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_IP.id}"
  parent_id   = "${aws_api_gateway_resource.DEV01_IP.id}"
  path_part   = "DEV01_DreamPayStatusUpdate"
}

resource "aws_api_gateway_method" "DEV01_DreamPayStatusUpdate" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE_IP.id}"
  resource_id   = "${aws_api_gateway_resource.DEV01_DreamPayStatusUpdate.id}"
  http_method   = "POST"
  authorization = "NONE"
}

resource "aws_api_gateway_integration" "DEV01_DreamPayStatusUpdate" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE_IP.id}"
  resource_id             = "${aws_api_gateway_resource.DEV01_DreamPayStatusUpdate.id}"
  http_method             = "${aws_api_gateway_method.DEV01_DreamPayStatusUpdate.http_method}"
  integration_http_method = "POST"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-dev01.id}"
  type                    = "HTTP_PROXY"
  uri                     = "https://dev.goredev.guidewire.net/cc/rest/check/updateDreams"
}

resource "aws_api_gateway_method_response" "DEV01_DreamPayStatusUpdate" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_IP.id}"
  resource_id = "${aws_api_gateway_resource.DEV01_DreamPayStatusUpdate.id}"
  http_method = "${aws_api_gateway_method.DEV01_DreamPayStatusUpdate.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}
#########DEV02#########
resource "aws_api_gateway_resource" "DEV02_IP" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_IP.id}"
  parent_id   = "${aws_api_gateway_rest_api.GORE_IP.root_resource_id}"
  path_part   = "DEV02_IP"
}
###Impact###
resource "aws_api_gateway_resource" "DEV02_Impact_GW" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_IP.id}"
  parent_id   = "${aws_api_gateway_resource.DEV02_IP.id}"
  path_part   = "DEV02_Impact_GW"
}
resource "aws_api_gateway_method" "DEV02_Impact_GW" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_IP.id}"
  resource_id   = "${aws_api_gateway_resource.DEV02_Impact_GW.id}"
  http_method   = "POST"
  authorization = "NONE"
}
resource "aws_api_gateway_integration" "DEV02_Impact_GW" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE_IP.id}"
  resource_id             = "${aws_api_gateway_resource.DEV02_Impact_GW.id}"
  http_method             = "${aws_api_gateway_method.DEV02_Impact_GW.http_method}"
  integration_http_method = "POST"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-dev02.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.dev02.gore.insurcloud.ca/genericassignmentservice.svc"
}

resource "aws_api_gateway_method_response" "DEV02_Impact_GW" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_IP.id}"
  resource_id = "${aws_api_gateway_resource.DEV02_Impact_GW.id}"
  http_method = "${aws_api_gateway_method.DEV02_Impact_GW.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}
#Enterprise --> API gateway --> Enterprise MS --> GW & Document MS
resource "aws_api_gateway_resource" "DEV02_EnterpriseINB" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_IP.id}"
  parent_id   = "${aws_api_gateway_resource.DEV02_IP.id}"
  path_part   = "DEV02_EnterpriseINB"
}

resource "aws_api_gateway_resource" "DEV02_SendInvoiceNotification" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_IP.id}"
  parent_id   = "${aws_api_gateway_resource.DEV02_EnterpriseINB.id}"
  path_part   = "DEV02_SendInvoiceNotification"
}

#Update Notification request
resource "aws_api_gateway_method" "DEV02_SendInvoiceNotification" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE_IP.id}"
  resource_id   = "${aws_api_gateway_resource.DEV02_SendInvoiceNotification.id}"
  http_method   = "POST"
  authorization = "NONE"
}

resource "aws_api_gateway_integration" "DEV02_SendInvoiceNotification" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE_IP.id}"
  resource_id             = "${aws_api_gateway_resource.DEV02_SendInvoiceNotification.id}"
  http_method             = "${aws_api_gateway_method.DEV02_SendInvoiceNotification.http_method}"
  integration_http_method = "POST"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-dev02.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.dev02.gore.insurcloud.ca/car-rental-service/v1/rental/sendInvoiceNotificationRequest"
}

resource "aws_api_gateway_method_response" "DEV02_SendInvoiceNotification" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_IP.id}"
  resource_id = "${aws_api_gateway_resource.DEV02_SendInvoiceNotification.id}"
  http_method = "${aws_api_gateway_method.DEV02_SendInvoiceNotification.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}
# #DREAM PAYMENTS#####
resource "aws_api_gateway_resource" "DEV02_DreamPayStatusUpdate" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_IP.id}"
  parent_id   = "${aws_api_gateway_resource.DEV02_IP.id}"
  path_part   = "DEV02_DreamPayStatusUpdate"
}

resource "aws_api_gateway_method" "DEV02_DreamPayStatusUpdate" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE_IP.id}"
  resource_id   = "${aws_api_gateway_resource.DEV02_DreamPayStatusUpdate.id}"
  http_method   = "POST"
  authorization = "NONE"
}

resource "aws_api_gateway_integration" "DEV02_DreamPayStatusUpdate" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE_IP.id}"
  resource_id             = "${aws_api_gateway_resource.DEV02_DreamPayStatusUpdate.id}"
  http_method             = "${aws_api_gateway_method.DEV02_DreamPayStatusUpdate.http_method}"
  integration_http_method = "POST"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-dev02.id}"
  type                    = "HTTP_PROXY"
  uri                     = "https://dev2.goredev.guidewire.net/cc/rest/check/updateDreams"
}

resource "aws_api_gateway_method_response" "DEV02_DreamPayStatusUpdate" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_IP.id}"
  resource_id = "${aws_api_gateway_resource.DEV02_DreamPayStatusUpdate.id}"
  http_method = "${aws_api_gateway_method.DEV02_DreamPayStatusUpdate.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}
#########QA01#########
resource "aws_api_gateway_resource" "QA01_IP" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_IP.id}"
  parent_id   = "${aws_api_gateway_rest_api.GORE_IP.root_resource_id}"
  path_part   = "QA01_IP"
}
###Impact###
resource "aws_api_gateway_resource" "QA01_Impact_GW" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_IP.id}"
  parent_id   = "${aws_api_gateway_resource.QA01_IP.id}"
  path_part   = "QA01_Impact_GW"
}
resource "aws_api_gateway_method" "QA01_Impact_GW" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_IP.id}"
  resource_id   = "${aws_api_gateway_resource.QA01_Impact_GW.id}"
  http_method   = "POST"
  authorization = "NONE"
}
resource "aws_api_gateway_integration" "QA01_Impact_GW" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE_IP.id}"
  resource_id             = "${aws_api_gateway_resource.QA01_Impact_GW.id}"
  http_method             = "${aws_api_gateway_method.QA01_Impact_GW.http_method}"
  integration_http_method = "POST"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-qa01.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.qa01.gore.insurcloud.ca/genericassignmentservice.svc"
}

resource "aws_api_gateway_method_response" "QA01_Impact_GW" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_IP.id}"
  resource_id = "${aws_api_gateway_resource.QA01_Impact_GW.id}"
  http_method = "${aws_api_gateway_method.QA01_Impact_GW.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}
#Enterprise --> API gateway --> Enterprise MS --> GW & Document MS

resource "aws_api_gateway_resource" "QA01_EnterpriseINB" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_IP.id}"
  parent_id   = "${aws_api_gateway_resource.QA01_IP.id}"
  path_part   = "QA01_EnterpriseINB"
}
resource "aws_api_gateway_resource" "QA01_SendInvoiceNotification" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_IP.id}"
  parent_id   = "${aws_api_gateway_resource.QA01_EnterpriseINB.id}"
  path_part   = "QA01_SendInvoiceNotification"
}

#Update Notification request
resource "aws_api_gateway_method" "QA01_SendInvoiceNotification" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE_IP.id}"
  resource_id   = "${aws_api_gateway_resource.QA01_SendInvoiceNotification.id}"
  http_method   = "POST"
  authorization = "NONE"
}

resource "aws_api_gateway_integration" "QA01_SendInvoiceNotification" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE_IP.id}"
  resource_id             = "${aws_api_gateway_resource.QA01_SendInvoiceNotification.id}"
  http_method             = "${aws_api_gateway_method.QA01_SendInvoiceNotification.http_method}"
  integration_http_method = "POST"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-qa01.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.qa01.gore.insurcloud.ca/car-rental-service/v1/rental/sendInvoiceNotificationRequest"
}

resource "aws_api_gateway_method_response" "QA01_SendInvoiceNotification" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_IP.id}"
  resource_id = "${aws_api_gateway_resource.QA01_SendInvoiceNotification.id}"
  http_method = "${aws_api_gateway_method.QA01_SendInvoiceNotification.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}
# #DREAM PAYMENTS#####
resource "aws_api_gateway_resource" "QA01_DreamPayStatusUpdate" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_IP.id}"
  parent_id   = "${aws_api_gateway_resource.QA01_IP.id}"
  path_part   = "QA01_DreamPayStatusUpdate"
}

resource "aws_api_gateway_method" "QA01_DreamPayStatusUpdate" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE_IP.id}"
  resource_id   = "${aws_api_gateway_resource.QA01_DreamPayStatusUpdate.id}"
  http_method   = "POST"
  authorization = "NONE"
}

resource "aws_api_gateway_integration" "QA01_DreamPayStatusUpdate" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE_IP.id}"
  resource_id             = "${aws_api_gateway_resource.QA01_DreamPayStatusUpdate.id}"
  http_method             = "${aws_api_gateway_method.QA01_DreamPayStatusUpdate.http_method}"
  integration_http_method = "POST"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-qa01.id}"
  type                    = "HTTP_PROXY"
  uri                     = "https://qa.goredev.guidewire.net/cc/rest/check/updateDreams"
}

resource "aws_api_gateway_method_response" "QA01_DreamPayStatusUpdate" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_IP.id}"
  resource_id = "${aws_api_gateway_resource.QA01_DreamPayStatusUpdate.id}"
  http_method = "${aws_api_gateway_method.QA01_DreamPayStatusUpdate.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}
########DEV03#########
resource "aws_api_gateway_resource" "DEV03_IP" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_IP.id}"
  parent_id   = "${aws_api_gateway_rest_api.GORE_IP.root_resource_id}"
  path_part   = "DEV03_IP"
}


###Impact###
resource "aws_api_gateway_resource" "DEV03_Impact_GW" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_IP.id}"
  parent_id   = "${aws_api_gateway_resource.DEV03_IP.id}"
  path_part   = "DEV03_Impact_GW"
}

resource "aws_api_gateway_method" "DEV03_Impact_GW" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_IP.id}"
  resource_id   = "${aws_api_gateway_resource.DEV03_Impact_GW.id}"
  http_method   = "POST"
  authorization = "NONE"
}
resource "aws_api_gateway_integration" "DEV03_Impact_GW" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE_IP.id}"
  resource_id             = "${aws_api_gateway_resource.DEV03_Impact_GW.id}"
  http_method             = "${aws_api_gateway_method.DEV03_Impact_GW.http_method}"
  integration_http_method = "POST"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-dev03.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.dev03.gore.insurcloud.ca/genericassignmentservice.svc"
}

resource "aws_api_gateway_method_response" "DEV03_Impact_GW" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_IP.id}"
  resource_id = "${aws_api_gateway_resource.DEV03_Impact_GW.id}"
  http_method = "${aws_api_gateway_method.DEV03_Impact_GW.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}
#Enterprise --> API gateway --> Enterprise MS --> GW & Document MS
resource "aws_api_gateway_resource" "DEV03_EnterpriseINB" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_IP.id}"
  parent_id   = "${aws_api_gateway_resource.DEV03_IP.id}"
  path_part   = "DEV03_EnterpriseINB"
}

resource "aws_api_gateway_resource" "DEV03_SendInvoiceNotification" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_IP.id}"
  parent_id   = "${aws_api_gateway_resource.DEV03_EnterpriseINB.id}"
  path_part   = "DEV03_SendInvoiceNotification"
}

#Update Notification request
resource "aws_api_gateway_method" "DEV03_SendInvoiceNotification" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE_IP.id}"
  resource_id   = "${aws_api_gateway_resource.DEV03_SendInvoiceNotification.id}"
  http_method   = "POST"
  authorization = "NONE"
}

resource "aws_api_gateway_integration" "DEV03_SendInvoiceNotification" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE_IP.id}"
  resource_id             = "${aws_api_gateway_resource.DEV03_SendInvoiceNotification.id}"
  http_method             = "${aws_api_gateway_method.DEV03_SendInvoiceNotification.http_method}"
  integration_http_method = "POST"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-dev03.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.dev03.gore.insurcloud.ca/car-rental-service/v1/rental/sendInvoiceNotificationRequest"
}

resource "aws_api_gateway_method_response" "DEV03_SendInvoiceNotification" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_IP.id}"
  resource_id = "${aws_api_gateway_resource.DEV03_SendInvoiceNotification.id}"
  http_method = "${aws_api_gateway_method.DEV03_SendInvoiceNotification.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}
# #DREAM PAYMENTS#####
resource "aws_api_gateway_resource" "DEV03_DreamPayStatusUpdate" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_IP.id}"
  parent_id   = "${aws_api_gateway_resource.DEV03_IP.id}"
  path_part   = "DEV03_DreamPayStatusUpdate"
}

resource "aws_api_gateway_method" "DEV03_DreamPayStatusUpdate" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE_IP.id}"
  resource_id   = "${aws_api_gateway_resource.DEV03_DreamPayStatusUpdate.id}"
  http_method   = "POST"
  authorization = "NONE"
}

resource "aws_api_gateway_integration" "DEV03_DreamPayStatusUpdate" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE_IP.id}"
  resource_id             = "${aws_api_gateway_resource.DEV03_DreamPayStatusUpdate.id}"
  http_method             = "${aws_api_gateway_method.DEV03_DreamPayStatusUpdate.http_method}"
  integration_http_method = "POST"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-dev03.id}"
  type                    = "HTTP_PROXY"
  uri                     = "https://dev3.goredev.guidewire.net/cc/rest/check/updateDreams"
}

resource "aws_api_gateway_method_response" "DEV03_DreamPayStatusUpdate" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_IP.id}"
  resource_id = "${aws_api_gateway_resource.DEV03_DreamPayStatusUpdate.id}"
  http_method = "${aws_api_gateway_method.DEV03_DreamPayStatusUpdate.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}

#########QA02#########
resource "aws_api_gateway_resource" "QA02_IP" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_IP.id}"
  parent_id   = "${aws_api_gateway_rest_api.GORE_IP.root_resource_id}"
  path_part   = "QA02_IP"
}
###Impact###
resource "aws_api_gateway_resource" "QA02_Impact_GW" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_IP.id}"
  parent_id   = "${aws_api_gateway_resource.QA02_IP.id}"
  path_part   = "QA02_Impact_GW"
}
resource "aws_api_gateway_method" "QA02_Impact_GW" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_IP.id}"
  resource_id   = "${aws_api_gateway_resource.QA02_Impact_GW.id}"
  http_method   = "POST"
  authorization = "NONE"
}
resource "aws_api_gateway_integration" "QA02_Impact_GW" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE_IP.id}"
  resource_id             = "${aws_api_gateway_resource.QA02_Impact_GW.id}"
  http_method             = "${aws_api_gateway_method.QA02_Impact_GW.http_method}"
  integration_http_method = "POST"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-qa02.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.qa02.gore.insurcloud.ca/genericassignmentservice.svc"
}

resource "aws_api_gateway_method_response" "QA02_Impact_GW" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_IP.id}"
  resource_id = "${aws_api_gateway_resource.QA02_Impact_GW.id}"
  http_method = "${aws_api_gateway_method.QA02_Impact_GW.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}
#Enterprise --> API gateway --> Enterprise MS --> GW & Document MS

resource "aws_api_gateway_resource" "QA02_EnterpriseINB" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_IP.id}"
  parent_id   = "${aws_api_gateway_resource.QA02_IP.id}"
  path_part   = "QA02_EnterpriseINB"
}
resource "aws_api_gateway_resource" "QA02_SendInvoiceNotification" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_IP.id}"
  parent_id   = "${aws_api_gateway_resource.QA02_EnterpriseINB.id}"
  path_part   = "QA02_SendInvoiceNotification"
}

#Update Notification request
resource "aws_api_gateway_method" "QA02_SendInvoiceNotification" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE_IP.id}"
  resource_id   = "${aws_api_gateway_resource.QA02_SendInvoiceNotification.id}"
  http_method   = "POST"
  authorization = "NONE"
}

resource "aws_api_gateway_integration" "QA02_SendInvoiceNotification" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE_IP.id}"
  resource_id             = "${aws_api_gateway_resource.QA02_SendInvoiceNotification.id}"
  http_method             = "${aws_api_gateway_method.QA02_SendInvoiceNotification.http_method}"
  integration_http_method = "POST"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-qa02.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.qa02.gore.insurcloud.ca/car-rental-service/v1/rental/sendInvoiceNotificationRequest"
}

resource "aws_api_gateway_method_response" "QA02_SendInvoiceNotification" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_IP.id}"
  resource_id = "${aws_api_gateway_resource.QA02_SendInvoiceNotification.id}"
  http_method = "${aws_api_gateway_method.QA02_SendInvoiceNotification.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}
# #DREAM PAYMENTS#####
resource "aws_api_gateway_resource" "QA02_DreamPayStatusUpdate" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_IP.id}"
  parent_id   = "${aws_api_gateway_resource.QA02_IP.id}"
  path_part   = "QA02_DreamPayStatusUpdate"
}

resource "aws_api_gateway_method" "QA02_DreamPayStatusUpdate" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE_IP.id}"
  resource_id   = "${aws_api_gateway_resource.QA02_DreamPayStatusUpdate.id}"
  http_method   = "POST"
  authorization = "NONE"
}

resource "aws_api_gateway_integration" "QA02_DreamPayStatusUpdate" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE_IP.id}"
  resource_id             = "${aws_api_gateway_resource.QA02_DreamPayStatusUpdate.id}"
  http_method             = "${aws_api_gateway_method.QA02_DreamPayStatusUpdate.http_method}"
  integration_http_method = "POST"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-qa02.id}"
  type                    = "HTTP_PROXY"
  uri                     = "https://qa2.goredev.guidewire.net/cc/rest/check/updateDreams"
}

resource "aws_api_gateway_method_response" "QA02_DreamPayStatusUpdate" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_IP.id}"
  resource_id = "${aws_api_gateway_resource.QA02_DreamPayStatusUpdate.id}"
  http_method = "${aws_api_gateway_method.QA02_DreamPayStatusUpdate.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}

########TBT01#########
resource "aws_api_gateway_resource" "TBT01_IP" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_IP.id}"
  parent_id   = "${aws_api_gateway_rest_api.GORE_IP.root_resource_id}"
  path_part   = "TBT01_IP"
}


###Impact###
resource "aws_api_gateway_resource" "TBT01_Impact_GW" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_IP.id}"
  parent_id   = "${aws_api_gateway_resource.TBT01_IP.id}"
  path_part   = "TBT01_Impact_GW"
}

resource "aws_api_gateway_method" "TBT01_Impact_GW" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_IP.id}"
  resource_id   = "${aws_api_gateway_resource.TBT01_Impact_GW.id}"
  http_method   = "POST"
  authorization = "NONE"
}
resource "aws_api_gateway_integration" "TBT01_Impact_GW" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE_IP.id}"
  resource_id             = "${aws_api_gateway_resource.TBT01_Impact_GW.id}"
  http_method             = "${aws_api_gateway_method.TBT01_Impact_GW.http_method}"
  integration_http_method = "POST"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-tbt01.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.tbt01.gore.insurcloud.ca/genericassignmentservice.svc"
}

resource "aws_api_gateway_method_response" "TBT01_Impact_GW" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_IP.id}"
  resource_id = "${aws_api_gateway_resource.TBT01_Impact_GW.id}"
  http_method = "${aws_api_gateway_method.TBT01_Impact_GW.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}
#Enterprise --> API gateway --> Enterprise MS --> GW & Document MS
resource "aws_api_gateway_resource" "TBT01_EnterpriseINB" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_IP.id}"
  parent_id   = "${aws_api_gateway_resource.TBT01_IP.id}"
  path_part   = "TBT01_EnterpriseINB"
}

resource "aws_api_gateway_resource" "TBT01_SendInvoiceNotification" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_IP.id}"
  parent_id   = "${aws_api_gateway_resource.TBT01_EnterpriseINB.id}"
  path_part   = "TBT01_SendInvoiceNotification"
}

#Update Notification request
resource "aws_api_gateway_method" "TBT01_SendInvoiceNotification" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE_IP.id}"
  resource_id   = "${aws_api_gateway_resource.TBT01_SendInvoiceNotification.id}"
  http_method   = "POST"
  authorization = "NONE"
}

resource "aws_api_gateway_integration" "TBT01_SendInvoiceNotification" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE_IP.id}"
  resource_id             = "${aws_api_gateway_resource.TBT01_SendInvoiceNotification.id}"
  http_method             = "${aws_api_gateway_method.TBT01_SendInvoiceNotification.http_method}"
  integration_http_method = "POST"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-tbt01.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.tbt01.gore.insurcloud.ca/car-rental-service/v1/rental/sendInvoiceNotificationRequest"
}

resource "aws_api_gateway_method_response" "TBT01_SendInvoiceNotification" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_IP.id}"
  resource_id = "${aws_api_gateway_resource.TBT01_SendInvoiceNotification.id}"
  http_method = "${aws_api_gateway_method.TBT01_SendInvoiceNotification.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}
# #DREAM PAYMENTS#####
resource "aws_api_gateway_resource" "TBT01_DreamPayStatusUpdate" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_IP.id}"
  parent_id   = "${aws_api_gateway_resource.TBT01_IP.id}"
  path_part   = "TBT01_DreamPayStatusUpdate"
}

resource "aws_api_gateway_method" "TBT01_DreamPayStatusUpdate" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE_IP.id}"
  resource_id   = "${aws_api_gateway_resource.TBT01_DreamPayStatusUpdate.id}"
  http_method   = "POST"
  authorization = "NONE"
}

resource "aws_api_gateway_integration" "TBT01_DreamPayStatusUpdate" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE_IP.id}"
  resource_id             = "${aws_api_gateway_resource.TBT01_DreamPayStatusUpdate.id}"
  http_method             = "${aws_api_gateway_method.TBT01_DreamPayStatusUpdate.http_method}"
  integration_http_method = "POST"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-tbt01.id}"
  type                    = "HTTP_PROXY"
  uri                     = "https://tbt1.goredev.guidewire.net/cc/rest/check/updateDreams"
}

resource "aws_api_gateway_method_response" "TBT01_DreamPayStatusUpdate" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_IP.id}"
  resource_id = "${aws_api_gateway_resource.TBT01_DreamPayStatusUpdate.id}"
  http_method = "${aws_api_gateway_method.TBT01_DreamPayStatusUpdate.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}
########TBT02#########
resource "aws_api_gateway_resource" "TBT02_IP" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_IP.id}"
  parent_id   = "${aws_api_gateway_rest_api.GORE_IP.root_resource_id}"
  path_part   = "TBT02_IP"
}


###Impact###
resource "aws_api_gateway_resource" "TBT02_Impact_GW" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_IP.id}"
  parent_id   = "${aws_api_gateway_resource.TBT02_IP.id}"
  path_part   = "TBT02_Impact_GW"
}

resource "aws_api_gateway_method" "TBT02_Impact_GW" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_IP.id}"
  resource_id   = "${aws_api_gateway_resource.TBT02_Impact_GW.id}"
  http_method   = "POST"
  authorization = "NONE"
}
resource "aws_api_gateway_integration" "TBT02_Impact_GW" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE_IP.id}"
  resource_id             = "${aws_api_gateway_resource.TBT02_Impact_GW.id}"
  http_method             = "${aws_api_gateway_method.TBT02_Impact_GW.http_method}"
  integration_http_method = "POST"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-tbt02.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.tbt02.gore.insurcloud.ca/genericassignmentservice.svc"
}

resource "aws_api_gateway_method_response" "TBT02_Impact_GW" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_IP.id}"
  resource_id = "${aws_api_gateway_resource.TBT02_Impact_GW.id}"
  http_method = "${aws_api_gateway_method.TBT02_Impact_GW.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}
#Enterprise --> API gateway --> Enterprise MS --> GW & Document MS
resource "aws_api_gateway_resource" "TBT02_EnterpriseINB" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_IP.id}"
  parent_id   = "${aws_api_gateway_resource.TBT02_IP.id}"
  path_part   = "TBT02_EnterpriseINB"
}

resource "aws_api_gateway_resource" "TBT02_SendInvoiceNotification" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_IP.id}"
  parent_id   = "${aws_api_gateway_resource.TBT02_EnterpriseINB.id}"
  path_part   = "TBT02_SendInvoiceNotification"
}

#Update Notification request
resource "aws_api_gateway_method" "TBT02_SendInvoiceNotification" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE_IP.id}"
  resource_id   = "${aws_api_gateway_resource.TBT02_SendInvoiceNotification.id}"
  http_method   = "POST"
  authorization = "NONE"
}

resource "aws_api_gateway_integration" "TBT02_SendInvoiceNotification" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE_IP.id}"
  resource_id             = "${aws_api_gateway_resource.TBT02_SendInvoiceNotification.id}"
  http_method             = "${aws_api_gateway_method.TBT02_SendInvoiceNotification.http_method}"
  integration_http_method = "POST"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-tbt02.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.tbt02.gore.insurcloud.ca/car-rental-service/v1/rental/sendInvoiceNotificationRequest"
}

resource "aws_api_gateway_method_response" "TBT02_SendInvoiceNotification" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_IP.id}"
  resource_id = "${aws_api_gateway_resource.TBT02_SendInvoiceNotification.id}"
  http_method = "${aws_api_gateway_method.TBT02_SendInvoiceNotification.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}
# #DREAM PAYMENTS#####
resource "aws_api_gateway_resource" "TBT02_DreamPayStatusUpdate" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_IP.id}"
  parent_id   = "${aws_api_gateway_resource.TBT02_IP.id}"
  path_part   = "TBT02_DreamPayStatusUpdate"
}

resource "aws_api_gateway_method" "TBT02_DreamPayStatusUpdate" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE_IP.id}"
  resource_id   = "${aws_api_gateway_resource.TBT02_DreamPayStatusUpdate.id}"
  http_method   = "POST"
  authorization = "NONE"
}

resource "aws_api_gateway_integration" "TBT02_DreamPayStatusUpdate" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE_IP.id}"
  resource_id             = "${aws_api_gateway_resource.TBT02_DreamPayStatusUpdate.id}"
  http_method             = "${aws_api_gateway_method.TBT02_DreamPayStatusUpdate.http_method}"
  integration_http_method = "POST"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-tbt02.id}"
  type                    = "HTTP_PROXY"
  uri                     = "https://tbt2.goredev.guidewire.net/cc/rest/check/updateDreams"
}

resource "aws_api_gateway_method_response" "TBT02_DreamPayStatusUpdate" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_IP.id}"
  resource_id = "${aws_api_gateway_resource.TBT02_DreamPayStatusUpdate.id}"
  http_method = "${aws_api_gateway_method.TBT02_DreamPayStatusUpdate.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}
########BAT#########
resource "aws_api_gateway_resource" "BAT_IP" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_IP.id}"
  parent_id   = "${aws_api_gateway_rest_api.GORE_IP.root_resource_id}"
  path_part   = "BAT_IP"
}


###Impact###
resource "aws_api_gateway_resource" "BAT_Impact_GW" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_IP.id}"
  parent_id   = "${aws_api_gateway_resource.BAT_IP.id}"
  path_part   = "BAT_Impact_GW"
}

resource "aws_api_gateway_method" "BAT_Impact_GW" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_IP.id}"
  resource_id   = "${aws_api_gateway_resource.BAT_Impact_GW.id}"
  http_method   = "POST"
  authorization = "NONE"
}
resource "aws_api_gateway_integration" "BAT_Impact_GW" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE_IP.id}"
  resource_id             = "${aws_api_gateway_resource.BAT_Impact_GW.id}"
  http_method             = "${aws_api_gateway_method.BAT_Impact_GW.http_method}"
  integration_http_method = "POST"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-bat.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.bat.gore.insurcloud.ca/genericassignmentservice.svc"
}

resource "aws_api_gateway_method_response" "BAT_Impact_GW" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_IP.id}"
  resource_id = "${aws_api_gateway_resource.BAT_Impact_GW.id}"
  http_method = "${aws_api_gateway_method.BAT_Impact_GW.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}
#Enterprise --> API gateway --> Enterprise MS --> GW & Document MS
resource "aws_api_gateway_resource" "BAT_EnterpriseINB" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_IP.id}"
  parent_id   = "${aws_api_gateway_resource.BAT_IP.id}"
  path_part   = "BAT_EnterpriseINB"
}

resource "aws_api_gateway_resource" "BAT_SendInvoiceNotification" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_IP.id}"
  parent_id   = "${aws_api_gateway_resource.BAT_EnterpriseINB.id}"
  path_part   = "BAT_SendInvoiceNotification"
}

#Update Notification request
resource "aws_api_gateway_method" "BAT_SendInvoiceNotification" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE_IP.id}"
  resource_id   = "${aws_api_gateway_resource.BAT_SendInvoiceNotification.id}"
  http_method   = "POST"
  authorization = "NONE"
}

resource "aws_api_gateway_integration" "BAT_SendInvoiceNotification" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE_IP.id}"
  resource_id             = "${aws_api_gateway_resource.BAT_SendInvoiceNotification.id}"
  http_method             = "${aws_api_gateway_method.BAT_SendInvoiceNotification.http_method}"
  integration_http_method = "POST"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-bat.id}"
  type                    = "HTTP_PROXY"
  uri                     = "http://ms.bat.gore.insurcloud.ca/car-rental-service/v1/rental/sendInvoiceNotificationRequest"
}

resource "aws_api_gateway_method_response" "BAT_SendInvoiceNotification" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_IP.id}"
  resource_id = "${aws_api_gateway_resource.BAT_SendInvoiceNotification.id}"
  http_method = "${aws_api_gateway_method.BAT_SendInvoiceNotification.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}
# #DREAM PAYMENTS#####
resource "aws_api_gateway_resource" "BAT_DreamPayStatusUpdate" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_IP.id}"
  parent_id   = "${aws_api_gateway_resource.BAT_IP.id}"
  path_part   = "BAT_DreamPayStatusUpdate"
}

resource "aws_api_gateway_method" "BAT_DreamPayStatusUpdate" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE_IP.id}"
  resource_id   = "${aws_api_gateway_resource.BAT_DreamPayStatusUpdate.id}"
  http_method   = "POST"
  authorization = "NONE"
}

resource "aws_api_gateway_integration" "BAT_DreamPayStatusUpdate" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE_IP.id}"
  resource_id             = "${aws_api_gateway_resource.BAT_DreamPayStatusUpdate.id}"
  http_method             = "${aws_api_gateway_method.BAT_DreamPayStatusUpdate.http_method}"
  integration_http_method = "POST"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-bat.id}"
  type                    = "HTTP_PROXY"
  uri                     = "https://bat.goredev.guidewire.net/cc/rest/check/updateDreams"
}

resource "aws_api_gateway_method_response" "BAT_DreamPayStatusUpdate" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_IP.id}"
  resource_id = "${aws_api_gateway_resource.BAT_DreamPayStatusUpdate.id}"
  http_method = "${aws_api_gateway_method.BAT_DreamPayStatusUpdate.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}

###RESOURCE GROUPS###	
###Preprod####
resource "aws_api_gateway_resource" "PREPROD_AUTH" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_rest_api.GORE_AUTH0.root_resource_id}"
  path_part   = "PREPROD_AUTH"
}

# GET Earnix -API Gateway - ARMS
resource "aws_api_gateway_resource" "Earnix_Get" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.PREPROD_AUTH.id}"
  path_part   = "Earnix_Get"
}

resource "aws_api_gateway_method" "Earnix_Get" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id   = "${aws_api_gateway_resource.Earnix_Get.id}"
  http_method   = "GET"
  authorization = "NONE"
}

#to-do - update the proxy server to the one in perf
resource "aws_api_gateway_integration" "Earnix_Get" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id             = "${aws_api_gateway_resource.Earnix_Get.id}"
  http_method             = "${aws_api_gateway_method.Earnix_Get.http_method}"
  integration_http_method = "GET"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-dev01.id}"
  type                    = "HTTP_PROXY"
  uri                     = "https://goremutual-staging.earnix.com:443/earnix/online/v2/product/89490/schema?encounter=1"
}

resource "aws_api_gateway_method_response" "Earnix_Get" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id = "${aws_api_gateway_resource.Earnix_Get.id}"
  http_method = "${aws_api_gateway_method.Earnix_Get.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}

# #Post Earnix --> API gateway --> GW
resource "aws_api_gateway_resource" "Earnix_Post" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  parent_id   = "${aws_api_gateway_resource.PREPROD_AUTH.id}"
  path_part   = "Earnix_Post"
}

resource "aws_api_gateway_method" "Earnix_Post" {
  rest_api_id   = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id   = "${aws_api_gateway_resource.Earnix_Post.id}"
  http_method   = "POST"
  authorization = "NONE"
}

#to-do - update the proxy server to the one in perf
resource "aws_api_gateway_integration" "Earnix_Post" {
  rest_api_id             = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id             = "${aws_api_gateway_resource.Earnix_Post.id}"
  http_method             = "${aws_api_gateway_method.Earnix_Post.http_method}"
  integration_http_method = "POST"
  connection_type         = "VPC_LINK"
  connection_id           = "${aws_api_gateway_vpc_link.gore-insurcloud-dev01.id}"
  type                    = "HTTP_PROXY"
  uri                     = "https://goremutual-staging.earnix.com:443/earnix/online/v2/product/89490/profilesResult?encounter=0"
}

resource "aws_api_gateway_method_response" "Earnix_Post" {
  rest_api_id = "${aws_api_gateway_rest_api.GORE_AUTH0.id}"
  resource_id = "${aws_api_gateway_resource.Earnix_Post.id}"
  http_method = "${aws_api_gateway_method.Earnix_Post.http_method}"
  status_code = "200"
  response_models = {
         "application/json" = "Empty"
    }
}