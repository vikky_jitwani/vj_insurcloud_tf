#Envrionment specific variables
client_name = "IC-Portage"
environment_name = "HUB"
environment_type = "NP"
client_number = "A005"
#ad-dc1 = "10.10.4.10"
#ad-dc2 = "10.10.6.10"
volume_key = "arn:aws:kms:ca-central-1:342339668068:key/ba4a2825-3b94-4aaa-95c8-2f2d68637cd4"
ami_id_windows2012sql14enterprise = "ami-0b7e5050eee245b99"
ami_id_windows2019container = "ami-013453540e1aa0d48"
ami_id_windows2019base = "ami-06b8f69a907cf6bb2"
ami_id_windows2016base20200806 = "ami-0388b8d3cb35753f8"
ami_id_windows2016base = "ami-0bce38ba7ac166aa9"
ami_id_windows2012R2base = "ami-08f7dedaab95e4621"
ami_id_windows2008base = "ami-0446e08d2d99fc8c9"
ami_id_redhatlinux75 = "ami-05c33d286020a47f9"
ami_id_ubuntu1804baseimage = "ami-032172cc5f34b32fc"
ami_id_ubuntu1604baseimage = "ami-04f40cca01b3186ea"
ami_id_amazonlinux201712base = "ami-0a777bbf8ef3f43bf"
ami_id_amazonlinux201709base = "ami-058dba934995c5468"
ami_id_amazonlinux2base = "ami-071594d49d11fcb38"
ami_id_amazonlinux201803base = "ami-06829694f407e15c1"
ami_id_amazonlinuxbase20200806 = "ami-04978aa25eaba28b7"

#VPC Specific variables
vpc_id = "vpc-05cfa2b3f4a5adeb2"
vpc_cidr = "10.171.0.0/22"
#sg_cidr = "XXX"

portage-hub-database-1a-subnet = "subnet-014dd44a745ebb6e9"
portage-hub-database-1b-subnet = "subnet-05a16d39ae5f85a3d"
portage-hub-private-1a-subnet = "subnet-07abb9c7c3663aaf8"
portage-hub-private-1b-subnet = "subnet-0cf91c73f248d7ab6"
portage-hub-public-1a-subnet = "subnet-071b785ed3f541f4a"
portage-hub-public-1b-subnet = "subnet-0e2d7d4f779172f07"

#R53 
zone_id = "Z04739621X26ZH83WJ79G"

#Accounts
portage-dev-account = "091072499599"
portage-prod-account = "625725589452"
portage-hub-account = "342339668068"
