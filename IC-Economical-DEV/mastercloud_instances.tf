# #=======================================================================
# # DEV01
# #=======================================================================
######### GUIDEWIRE ##############
resource "aws_instance" "ICECONOMICAL_DEV01_GUIDEWIRE_SUITE" {
  ami = "${var.ami_id_amazonlinux2base}"
  vpc_security_group_ids = [
    "${aws_security_group.SG_GUIDEWIRE_DEV.id}",
    "${aws_security_group.SG_JENKINS_GUIDEWIRE_HEALTH_CHECK.id}",
    "${aws_security_group.SG_AVIATRIX_SSH.id}",
    "${aws_security_group.SG_JENKINS_CLIENT_SSH.id}",
    "${aws_security_group.SG_MSSQL_DB.id}"
  ]
  subnet_id = "${var.economical-dev-private-1a-subnet}"
  instance_type = "t3a.large"
  key_name = "EconomicalGuidewireNPRD"
  associate_public_ip_address = false
  iam_instance_profile = "CMS-SSMInstanceRole"
  ebs_optimized = "true"

  root_block_device {
    volume_size = "100"
  }

  lifecycle {
    prevent_destroy = false
    ignore_changes = [
      # FYI THERE IS A BUG IN TERRAFORM, I REALLY WANT TO USE VARIABLES BUT WE NEED THIS DEFECT FIXED: https://github.com/hashicorp/terraform/issues/3116
      "tags.%",
      "tags.BILLINGCODE",
      "tags.BILLINGCONTACT",
      "tags.CLIENT",
      "tags.COUNTRY",
      "tags.CSCLASS",
      "tags.CSQUAL",
      "tags.CSTYPE",
      "tags.ENVIRONMENT",
      "tags.FUNCTION",
      "tags.GROUPCONTACT",
      "tags.MEMBERFIRM",
      "tags.PRIMARYCONTACT",
      "tags.SECONDARYCONTACT",
      "volume_tags.%",
      "volume_tags.AccountID",
      "volume_tags.ApplicationName",
      "volume_tags.BILLINGCODE",
      "volume_tags.BILLINGCONTACT",
      "volume_tags.CLIENT",
      "volume_tags.COUNTRY",
      "volume_tags.CSCLASS",
      "volume_tags.CSQUAL",
      "volume_tags.CSTYPE",
      "volume_tags.DataClassification",
      "volume_tags.Docker",
      "volume_tags.ENVIRONMENT",
      "volume_tags.Environment",
      "volume_tags.FUNCTION",
      "volume_tags.GROUPCONTACT",
      "volume_tags.MEMBERFIRM",
      "volume_tags.ManagedBy",
      "volume_tags.Name",
      "volume_tags.Owner",
      "volume_tags.PRIMARYCONTACT",
      "volume_tags.PatchGroup",
      "volume_tags.ResourceSeverity",
      "volume_tags.ResourceType",
      "volume_tags.Role",
      "volume_tags.SECONDARYCONTACT",
      "volume_tags.SolutionName",
      "volume_tags.VendorManaged"
    ]
  }

  volume_tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "MANAGED_SERVICES"
  }

  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "MANAGED_SERVICES"
    PatchGroup = "AmazonLinux2-AzA-Group1"
    Owner = "robhatnagar@deloitte.ca"
    ManagedBy = "CMS-TF"
    SolutionName = "InsurCloud Economical"
    Environment = "Dev"
    ApplicationName = "guidewire, mssql, docker"
    DataClassification = "PII, PHI"
    DLM-Backup = "Yes"
    DLM-Retention = "90"
    VendorManaged = "False"
    ResourceType = "Client"
    ResourceSeverity = "Normal"
    Name = "A-MC-CAC-D-A003-ECONOMICAL-DEV01-GW-SUITE-LNX-EC2"
    Role = "Container"
  }

  provisioner "local-exec" {
    command = "echo ${aws_instance.ICECONOMICAL_DEV01_GUIDEWIRE_SUITE.private_ip} >> dev_hosts.txt"
  }
}

output "ICECONOMICAL_DEV01_GUIDEWIRE_SUITE_PRIVATEIP" {
  value = "${aws_instance.ICECONOMICAL_DEV01_GUIDEWIRE_SUITE.private_ip}"
}

######### MICROSERVICES ###########
resource "aws_instance" "ICECONOMICAL_DEV01_MICROSERVICES" {
  ami = "${var.ami_id_amazonlinux2base}"
  vpc_security_group_ids = [
    "${aws_security_group.SG_MICROSERVICES_DEV.id}",
    "${aws_security_group.SG_MONITORING.id}",
    "${aws_security_group.SG_AVIATRIX_SSH.id}",
    "${aws_security_group.SG_JENKINS_CLIENT_SSH.id}"
  ]
  subnet_id = "${var.economical-dev-private-1a-subnet}"
  instance_type = "t3a.xlarge"
  key_name = "EconomicalMicroservicesNPRD"
  associate_public_ip_address = false
  iam_instance_profile = "CMS-SSMInstanceRole"
  ebs_optimized = "true"

  root_block_device {
    volume_size = "200"
  }

  lifecycle {
    prevent_destroy = false
    ignore_changes = [
      "tags.%",
      "tags.BILLINGCODE",
      "tags.BILLINGCONTACT",
      "tags.CLIENT",
      "tags.COUNTRY",
      "tags.CSCLASS",
      "tags.CSQUAL",
      "tags.CSTYPE",
      "tags.ENVIRONMENT",
      "tags.FUNCTION",
      "tags.GROUPCONTACT",
      "tags.MEMBERFIRM",
      "tags.PRIMARYCONTACT",
      "tags.SECONDARYCONTACT",
      "volume_tags.%",
      "volume_tags.AccountID",
      "volume_tags.ApplicationName",
      "volume_tags.BILLINGCODE",
      "volume_tags.BILLINGCONTACT",
      "volume_tags.CLIENT",
      "volume_tags.COUNTRY",
      "volume_tags.CSCLASS",
      "volume_tags.CSQUAL",
      "volume_tags.CSTYPE",
      "volume_tags.DataClassification",
      "volume_tags.Docker",
      "volume_tags.ENVIRONMENT",
      "volume_tags.Environment",
      "volume_tags.FUNCTION",
      "volume_tags.GROUPCONTACT",
      "volume_tags.MEMBERFIRM",
      "volume_tags.ManagedBy",
      "volume_tags.Name",
      "volume_tags.Owner",
      "volume_tags.PRIMARYCONTACT",
      "volume_tags.PatchGroup",
      "volume_tags.ResourceSeverity",
      "volume_tags.ResourceType",
      "volume_tags.Role",
      "volume_tags.SECONDARYCONTACT",
      "volume_tags.SolutionName",
      "volume_tags.VendorManaged"
    ]
  }

  volume_tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "MANAGED_SERVICES"
  }

  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "MANAGED_SERVICES"
    PatchGroup = "AmazonLinux2-AzA-Group1"
    Owner = "robhatnagar@deloitte.ca"
    ManagedBy = "CMS-TF"
    SolutionName = "InsurCloud Economical"
    Environment = "Dev"
    ApplicationName = "docker, microservices"
    DataClassification = "PII, PHI"
    DLM-Backup = "Yes"
    DLM-Retention = "90"
    VendorManaged = "False"
    ResourceType = "Client"
    ResourceSeverity = "Normal"
    Name = "A-MC-CAC-D-A003-ECONOMICAL-DEV01-MICROSERVICES-LNX-EC2"
    Role = "Container"
  }

  provisioner "local-exec" {
    command = "echo ${aws_instance.ICECONOMICAL_DEV01_MICROSERVICES.private_ip} >> dev_hosts.txt"
  }
}

output "ICECONOMICAL_DEV01_MICROSERVICES_PRIVATEIP" {
  value = "${aws_instance.ICECONOMICAL_DEV01_MICROSERVICES.private_ip}"
}

######### RPA ###########
data "template_file" "ECO_DOMAIN_JOIN_1" {
  template = "${file("IC-Economical-DEV/windowsdata.tpl")}"

  vars = {
    computer_name = "RPA-APP-WIN"
    region = "${var.region}"
    domain = "${var.ad_domain}"
    dns1 = "${var.ad_dns1}"
    dns2 = "${var.ad_dns2}"
    dns3 = "${var.ad_dns3}"
    ouselection = "${var.ou_name}"
    pwordparameter = "${var.pwordparameter}"
    unameparameter = "${var.unameparameter}"
  }
}

resource "aws_instance" "ICECONOMICAL_DEV01_RPA_APP" {
  ami = "${var.ami_id_windows2016base}"
  vpc_security_group_ids = [
    "${aws_security_group.SG_AVIATRIX_RDP.id}",
    "${aws_security_group.SG_RPA_APP.id}",
    "${aws_security_group.SG_RPA_APP_DEV.id}"
  ]
  subnet_id = "${var.economical-dev-private-1a-subnet}"
  instance_type = "t3a.medium"
  key_name = "EconomicalRPANPRD"
  associate_public_ip_address = false
  iam_instance_profile = "CMS-SSMInstanceRole"
  ebs_optimized = "true"

  root_block_device {
    volume_size = "50"
  }

  user_data = "${data.template_file.ECO_DOMAIN_JOIN_1.rendered}"

  lifecycle {
    prevent_destroy = false
    ignore_changes = [
      "tags.%",
      "tags.BILLINGCODE",
      "tags.BILLINGCONTACT",
      "tags.CLIENT",
      "tags.COUNTRY",
      "tags.CSCLASS",
      "tags.CSQUAL",
      "tags.CSTYPE",
      "tags.ENVIRONMENT",
      "tags.FUNCTION",
      "tags.GROUPCONTACT",
      "tags.MEMBERFIRM",
      "tags.PRIMARYCONTACT",
      "tags.SECONDARYCONTACT",
      "volume_tags.%",
      "volume_tags.AccountID",
      "volume_tags.ApplicationName",
      "volume_tags.BILLINGCODE",
      "volume_tags.BILLINGCONTACT",
      "volume_tags.CLIENT",
      "volume_tags.COUNTRY",
      "volume_tags.CSCLASS",
      "volume_tags.CSQUAL",
      "volume_tags.CSTYPE",
      "volume_tags.DataClassification",
      "volume_tags.Docker",
      "volume_tags.ENVIRONMENT",
      "volume_tags.Environment",
      "volume_tags.FUNCTION",
      "volume_tags.GROUPCONTACT",
      "volume_tags.MEMBERFIRM",
      "volume_tags.ManagedBy",
      "volume_tags.Name",
      "volume_tags.Owner",
      "volume_tags.PRIMARYCONTACT",
      "volume_tags.PatchGroup",
      "volume_tags.ResourceSeverity",
      "volume_tags.ResourceType",
      "volume_tags.Role",
      "volume_tags.SECONDARYCONTACT",
      "volume_tags.SolutionName",
      "volume_tags.VendorManaged"
    ]
  }

  volume_tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "MANAGED_SERVICES"
  }

  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "MANAGED_SERVICES"
    PatchGroup = "Windows-AzA-Group1"
    Owner = "robhatnagar@deloitte.ca"
    ManagedBy = "CMS-TF"
    SolutionName = "InsurCloud Economical"
    Environment = "Dev"
    ApplicationName = "rpa, blueprism"
    DataClassification = "PII, PHI"
    DLM-Backup = "Yes"
    DLM-Retention = "90"
    VendorManaged = "False"
    ResourceType = "Client"
    ResourceSeverity = "Normal"
    Name = "A-MC-CAC-D-A003-ECONOMICAL-DEV01-RPA-APP-WIN-EC2"
  }
}

output "ICECONOMICAL_DEV01_RPA_APP_PRIVATEIP" {
  value = "${aws_instance.ICECONOMICAL_DEV01_RPA_APP.private_ip}"
}

data "template_file" "ECO_DOMAIN_JOIN_2" {
  template = "${file("IC-Economical-DEV/windowsdata.tpl")}"

  vars = {
    computer_name = "RPA-CLT01-WIN"
    region = "${var.region}"
    domain = "${var.ad_domain}"
    dns1 = "${var.ad_dns1}"
    dns2 = "${var.ad_dns2}"
    dns3 = "${var.ad_dns3}"
    ouselection = "${var.ou_name}"
    pwordparameter = "${var.pwordparameter}"
    unameparameter = "${var.unameparameter}"
  }
}

resource "aws_instance" "ICECONOMICAL_DEV01_RPA_CLIENT01" {
  ami = "${var.ami_id_windows2016base}"
  vpc_security_group_ids = [
    "${aws_security_group.SG_AVIATRIX_RDP.id}",
    "${aws_security_group.SG_RPA_CLIENT.id}",
    "${aws_security_group.SG_RPA_CLIENT_DEV.id}"
  ]
  subnet_id = "${var.economical-dev-private-1a-subnet}"
  instance_type = "t3a.medium"
  key_name = "EconomicalRPANPRD"
  associate_public_ip_address = false
  iam_instance_profile = "CMS-SSMInstanceRole"
  ebs_optimized = "true"

  root_block_device {
    volume_size = "70"
  }

  user_data = "${data.template_file.ECO_DOMAIN_JOIN_2.rendered}"

  lifecycle {
    prevent_destroy = false
    ignore_changes = [
      "tags.%",
      "tags.BILLINGCODE",
      "tags.BILLINGCONTACT",
      "tags.CLIENT",
      "tags.COUNTRY",
      "tags.CSCLASS",
      "tags.CSQUAL",
      "tags.CSTYPE",
      "tags.ENVIRONMENT",
      "tags.FUNCTION",
      "tags.GROUPCONTACT",
      "tags.MEMBERFIRM",
      "tags.PRIMARYCONTACT",
      "tags.SECONDARYCONTACT",
      "volume_tags.%",
      "volume_tags.AccountID",
      "volume_tags.ApplicationName",
      "volume_tags.BILLINGCODE",
      "volume_tags.BILLINGCONTACT",
      "volume_tags.CLIENT",
      "volume_tags.COUNTRY",
      "volume_tags.CSCLASS",
      "volume_tags.CSQUAL",
      "volume_tags.CSTYPE",
      "volume_tags.DataClassification",
      "volume_tags.Docker",
      "volume_tags.ENVIRONMENT",
      "volume_tags.Environment",
      "volume_tags.FUNCTION",
      "volume_tags.GROUPCONTACT",
      "volume_tags.MEMBERFIRM",
      "volume_tags.ManagedBy",
      "volume_tags.Name",
      "volume_tags.Owner",
      "volume_tags.PRIMARYCONTACT",
      "volume_tags.PatchGroup",
      "volume_tags.ResourceSeverity",
      "volume_tags.ResourceType",
      "volume_tags.Role",
      "volume_tags.SECONDARYCONTACT",
      "volume_tags.SolutionName",
      "volume_tags.VendorManaged"
    ]
  }

  volume_tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "MANAGED_SERVICES"
  }

  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "MANAGED_SERVICES"
    PatchGroup = "Windows-AzA-Group1"
    Owner = "robhatnagar@deloitte.ca"
    ManagedBy = "CMS-TF"
    SolutionName = "InsurCloud Economical"
    Environment = "Dev"
    ApplicationName = "rpa, blueprism"
    DataClassification = "PII, PHI"
    DLM-Backup = "Yes"
    DLM-Retention = "90"
    VendorManaged = "False"
    ResourceType = "Client"
    ResourceSeverity = "Normal"
    Name = "A-MC-CAC-D-A003-ECONOMICAL-DEV01-RPA-CLIENT01-WIN-EC2"
  }
}

output "ICECONOMICAL_DEV01_RPA_CLIENT01_PRIVATEIP" {
  value = "${aws_instance.ICECONOMICAL_DEV01_RPA_CLIENT01.private_ip}"
}

data "template_file" "ECO_DOMAIN_JOIN_3" {
  template = "${file("IC-Economical-DEV/windowsdata.tpl")}"

  vars = {
    computer_name = "RPA-CLT02-WIN"
    region = "${var.region}"
    domain = "${var.ad_domain}"
    dns1 = "${var.ad_dns1}"
    dns2 = "${var.ad_dns2}"
    dns3 = "${var.ad_dns3}"
    ouselection = "${var.ou_name}"
    pwordparameter = "${var.pwordparameter}"
    unameparameter = "${var.unameparameter}"
  }
}

resource "aws_instance" "ICECONOMICAL_DEV01_RPA_CLIENT02" {
  ami = "${var.ami_id_windows2016base}"
  vpc_security_group_ids = [
    "${aws_security_group.SG_AVIATRIX_RDP.id}",
    "${aws_security_group.SG_RPA_CLIENT.id}",
    "${aws_security_group.SG_RPA_CLIENT_DEV.id}"
  ]
  subnet_id = "${var.economical-dev-private-1a-subnet}"
  instance_type = "t3a.medium"
  key_name = "EconomicalRPANPRD"
  associate_public_ip_address = false
  iam_instance_profile = "CMS-SSMInstanceRole"
  ebs_optimized = "true"

  root_block_device {
    volume_size = "50"
  }

  user_data = "${data.template_file.ECO_DOMAIN_JOIN_3.rendered}"

  lifecycle {
    prevent_destroy = false
    ignore_changes = [
      "tags.%",
      "tags.BILLINGCODE",
      "tags.BILLINGCONTACT",
      "tags.CLIENT",
      "tags.COUNTRY",
      "tags.CSCLASS",
      "tags.CSQUAL",
      "tags.CSTYPE",
      "tags.ENVIRONMENT",
      "tags.FUNCTION",
      "tags.GROUPCONTACT",
      "tags.MEMBERFIRM",
      "tags.PRIMARYCONTACT",
      "tags.SECONDARYCONTACT",
      "volume_tags.%",
      "volume_tags.AccountID",
      "volume_tags.ApplicationName",
      "volume_tags.BILLINGCODE",
      "volume_tags.BILLINGCONTACT",
      "volume_tags.CLIENT",
      "volume_tags.COUNTRY",
      "volume_tags.CSCLASS",
      "volume_tags.CSQUAL",
      "volume_tags.CSTYPE",
      "volume_tags.DataClassification",
      "volume_tags.Docker",
      "volume_tags.ENVIRONMENT",
      "volume_tags.Environment",
      "volume_tags.FUNCTION",
      "volume_tags.GROUPCONTACT",
      "volume_tags.MEMBERFIRM",
      "volume_tags.ManagedBy",
      "volume_tags.Name",
      "volume_tags.Owner",
      "volume_tags.PRIMARYCONTACT",
      "volume_tags.PatchGroup",
      "volume_tags.ResourceSeverity",
      "volume_tags.ResourceType",
      "volume_tags.Role",
      "volume_tags.SECONDARYCONTACT",
      "volume_tags.SolutionName",
      "volume_tags.VendorManaged"
    ]
  }

  volume_tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "MANAGED_SERVICES"
  }

  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "MANAGED_SERVICES"
    PatchGroup = "Windows-AzA-Group1"
    Owner = "robhatnagar@deloitte.ca"
    ManagedBy = "CMS-TF"
    SolutionName = "InsurCloud Economical"
    Environment = "Dev"
    ApplicationName = "rpa, blueprism"
    DataClassification = "PII, PHI"
    DLM-Backup = "Yes"
    DLM-Retention = "90"
    VendorManaged = "False"
    ResourceType = "Client"
    ResourceSeverity = "Normal"
    Name = "A-MC-CAC-D-A003-ECONOMICAL-DEV01-RPA-CLIENT02-WIN-EC2"
  }
}

output "ICECONOMICAL_DEV01_RPA_CLIENT02_PRIVATEIP" {
  value = "${aws_instance.ICECONOMICAL_DEV01_RPA_CLIENT02.private_ip}"
}

######### HAPROXY ##############
resource "aws_instance" "ICECONOMICAL_DEV01_INTERNAL_HAPROXY" {
  ami = "${var.ami_id_amazonlinux2base}"
  vpc_security_group_ids = [
    "${aws_security_group.SG_INTERNAL_HAPROXY_DEV.id}",
    "${aws_security_group.SG_AVIATRIX_SSH.id}",
    "${aws_security_group.SG_JENKINS_CLIENT_SSH.id}"
  ]
  subnet_id = "${var.economical-dev-private-1a-subnet}"
  instance_type = "t3a.small"
  key_name = "EconomicalHAProxyNPRD"
  associate_public_ip_address = false
  iam_instance_profile = "CMS-SSMInstanceRole"
  ebs_optimized = "true"

  root_block_device {
    volume_size = "40"
  }

  lifecycle {
    prevent_destroy = false
    ignore_changes = [
      "tags.%",
      "tags.BILLINGCODE",
      "tags.BILLINGCONTACT",
      "tags.CLIENT",
      "tags.COUNTRY",
      "tags.CSCLASS",
      "tags.CSQUAL",
      "tags.CSTYPE",
      "tags.ENVIRONMENT",
      "tags.FUNCTION",
      "tags.GROUPCONTACT",
      "tags.MEMBERFIRM",
      "tags.PRIMARYCONTACT",
      "tags.SECONDARYCONTACT",
      "volume_tags.%",
      "volume_tags.AccountID",
      "volume_tags.ApplicationName",
      "volume_tags.BILLINGCODE",
      "volume_tags.BILLINGCONTACT",
      "volume_tags.CLIENT",
      "volume_tags.COUNTRY",
      "volume_tags.CSCLASS",
      "volume_tags.CSQUAL",
      "volume_tags.CSTYPE",
      "volume_tags.DataClassification",
      "volume_tags.Docker",
      "volume_tags.ENVIRONMENT",
      "volume_tags.Environment",
      "volume_tags.FUNCTION",
      "volume_tags.GROUPCONTACT",
      "volume_tags.MEMBERFIRM",
      "volume_tags.ManagedBy",
      "volume_tags.Name",
      "volume_tags.Owner",
      "volume_tags.PRIMARYCONTACT",
      "volume_tags.PatchGroup",
      "volume_tags.ResourceSeverity",
      "volume_tags.ResourceType",
      "volume_tags.Role",
      "volume_tags.SECONDARYCONTACT",
      "volume_tags.SolutionName",
      "volume_tags.VendorManaged"
    ]
  }

  volume_tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "MANAGED_SERVICES"
  }

  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "MANAGED_SERVICES"
    PatchGroup = "AmazonLinux2-AzA-Group1"
    Owner = "robhatnagar@deloitte.ca"
    ManagedBy = "CMS-TF"
    SolutionName = "InsurCloud Economical"
    Environment = "Dev"
    ApplicationName = "docker, haproxy"
    DataClassification = "PII, PHI"
    DLM-Backup = "Yes"
    DLM-Retention = "90"
    VendorManaged = "False"
    ResourceType = "Client"
    ResourceSeverity = "Normal"
    Name = "A-MC-CAC-D-A003-ECONOMICAL-DEV01-INTERNAL-HAPROXY-EC2"
    Role = "Container"
  }

  provisioner "local-exec" {
    command = "echo ${aws_instance.ICECONOMICAL_DEV01_INTERNAL_HAPROXY.private_ip} >> dev_hosts.txt"
  }
}

output "ICECONOMICAL_DEV01_INTERNAL_HAPROXY_PRIVATEIP" {
  value = "${aws_instance.ICECONOMICAL_DEV01_INTERNAL_HAPROXY.private_ip}"
}

resource "aws_instance" "ICECONOMICAL_DEV01_EXTERNAL_HAPROXY" {
  ami = "${var.ami_id_amazonlinux2base}"
  vpc_security_group_ids = [
    "${aws_security_group.SG_EXTERNAL_HAPROXY_DEV.id}",
    "${aws_security_group.SG_AVIATRIX_SSH.id}",
    "${aws_security_group.SG_JENKINS_CLIENT_SSH.id}"
  ]
  subnet_id = "${var.economical-dev-private-1a-subnet}"
  instance_type = "t3a.small"
  key_name = "EconomicalHAProxyNPRD"
  associate_public_ip_address = false
  iam_instance_profile = "CMS-SSMInstanceRole"
  ebs_optimized = "true"

  root_block_device {
    volume_size = "40"
  }

  lifecycle {
    prevent_destroy = false
    ignore_changes = [
      "tags.%",
      "tags.BILLINGCODE",
      "tags.BILLINGCONTACT",
      "tags.CLIENT",
      "tags.COUNTRY",
      "tags.CSCLASS",
      "tags.CSQUAL",
      "tags.CSTYPE",
      "tags.ENVIRONMENT",
      "tags.FUNCTION",
      "tags.GROUPCONTACT",
      "tags.MEMBERFIRM",
      "tags.PRIMARYCONTACT",
      "tags.SECONDARYCONTACT",
      "volume_tags.%",
      "volume_tags.AccountID",
      "volume_tags.ApplicationName",
      "volume_tags.BILLINGCODE",
      "volume_tags.BILLINGCONTACT",
      "volume_tags.CLIENT",
      "volume_tags.COUNTRY",
      "volume_tags.CSCLASS",
      "volume_tags.CSQUAL",
      "volume_tags.CSTYPE",
      "volume_tags.DataClassification",
      "volume_tags.Docker",
      "volume_tags.ENVIRONMENT",
      "volume_tags.Environment",
      "volume_tags.FUNCTION",
      "volume_tags.GROUPCONTACT",
      "volume_tags.MEMBERFIRM",
      "volume_tags.ManagedBy",
      "volume_tags.Name",
      "volume_tags.Owner",
      "volume_tags.PRIMARYCONTACT",
      "volume_tags.PatchGroup",
      "volume_tags.ResourceSeverity",
      "volume_tags.ResourceType",
      "volume_tags.Role",
      "volume_tags.SECONDARYCONTACT",
      "volume_tags.SolutionName",
      "volume_tags.VendorManaged"
    ]
  }

  volume_tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "MANAGED_SERVICES"
  }

  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "MANAGED_SERVICES"
    PatchGroup = "AmazonLinux2-AzA-Group1"
    Owner = "robhatnagar@deloitte.ca"
    ManagedBy = "CMS-TF"
    SolutionName = "InsurCloud Economical"
    Environment = "Dev"
    ApplicationName = "docker, haproxy"
    DataClassification = "PII, PHI"
    DLM-Backup = "Yes"
    DLM-Retention = "90"
    VendorManaged = "False"
    ResourceType = "Client"
    ResourceSeverity = "Normal"
    Name = "A-MC-CAC-D-A003-ECONOMICAL-DEV01-EXTERNAL-HAPROXY-EC2"
    Role = "Container"
  }

  provisioner "local-exec" {
    command = "echo ${aws_instance.ICECONOMICAL_DEV01_EXTERNAL_HAPROXY.private_ip} >> dev_hosts.txt"
  }
}

output "ICECONOMICAL_DEV01_EXTERNAL_HAPROXY_PRIVATEIP" {
  value = "${aws_instance.ICECONOMICAL_DEV01_EXTERNAL_HAPROXY.private_ip}"
}


# #=======================================================================
# # DEV02
# #=======================================================================
######### GUIDEWIRE ##############
resource "aws_instance" "ICECONOMICAL_DEV02_GUIDEWIRE_SUITE" {
  ami = "${var.ami_id_amazonlinux2base}"
  vpc_security_group_ids = [
    "${aws_security_group.SG_GUIDEWIRE_DEV.id}",
    "${aws_security_group.SG_JENKINS_GUIDEWIRE_HEALTH_CHECK.id}",
    "${aws_security_group.SG_AVIATRIX_SSH.id}",
    "${aws_security_group.SG_JENKINS_CLIENT_SSH.id}",
    "${aws_security_group.SG_MSSQL_DB.id}"
  ]
  subnet_id = "${var.economical-dev-private-1a-subnet}"
  instance_type = "t3a.large"
  key_name = "EconomicalGuidewireNPRD"
  associate_public_ip_address = false
  iam_instance_profile = "CMS-SSMInstanceRole"
  ebs_optimized = "true"

  root_block_device {
    volume_size = "100"
  }

  lifecycle {
    prevent_destroy = false
    ignore_changes = [
      "tags.%",
      "tags.BILLINGCODE",
      "tags.BILLINGCONTACT",
      "tags.CLIENT",
      "tags.COUNTRY",
      "tags.CSCLASS",
      "tags.CSQUAL",
      "tags.CSTYPE",
      "tags.ENVIRONMENT",
      "tags.FUNCTION",
      "tags.GROUPCONTACT",
      "tags.MEMBERFIRM",
      "tags.PRIMARYCONTACT",
      "tags.SECONDARYCONTACT",
      "volume_tags.%",
      "volume_tags.AccountID",
      "volume_tags.ApplicationName",
      "volume_tags.BILLINGCODE",
      "volume_tags.BILLINGCONTACT",
      "volume_tags.CLIENT",
      "volume_tags.COUNTRY",
      "volume_tags.CSCLASS",
      "volume_tags.CSQUAL",
      "volume_tags.CSTYPE",
      "volume_tags.DataClassification",
      "volume_tags.Docker",
      "volume_tags.ENVIRONMENT",
      "volume_tags.Environment",
      "volume_tags.FUNCTION",
      "volume_tags.GROUPCONTACT",
      "volume_tags.MEMBERFIRM",
      "volume_tags.ManagedBy",
      "volume_tags.Name",
      "volume_tags.Owner",
      "volume_tags.PRIMARYCONTACT",
      "volume_tags.PatchGroup",
      "volume_tags.ResourceSeverity",
      "volume_tags.ResourceType",
      "volume_tags.Role",
      "volume_tags.SECONDARYCONTACT",
      "volume_tags.SolutionName",
      "volume_tags.VendorManaged"
    ]
  }

  volume_tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "PROJECT"
  }

  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "PROJECT"
    PatchGroup = "AmazonLinux2-AzA-Group1"
    Owner = "robhatnagar@deloitte.ca"
    ManagedBy = "CMS-TF"
    SolutionName = "InsurCloud Economical"
    Environment = "Dev"
    ApplicationName = "guidewire, mssql, docker"
    DataClassification = "PII, PHI"
    DLM-Backup = "Yes"
    DLM-Retention = "90"
    ResourceType = "Client"
    ResourceSeverity = "Normal"
    Name = "A-MC-CAC-D-A003-ECONOMICAL-DEV02-GW-SUITE-LNX-EC2"
    Role = "Container"
  }

  provisioner "local-exec" {
    command = "echo ${aws_instance.ICECONOMICAL_DEV02_GUIDEWIRE_SUITE.private_ip} >> dev_hosts.txt"
  }
}

output "ICECONOMICAL_DEV02_GUIDEWIRE_SUITE_PRIVATEIP" {
  value = "${aws_instance.ICECONOMICAL_DEV02_GUIDEWIRE_SUITE.private_ip}"
}

######### MICROSERVICES ###########
resource "aws_instance" "ICECONOMICAL_DEV02_MICROSERVICES" {
  ami = "${var.ami_id_amazonlinux2base}"
  vpc_security_group_ids = [
    "${aws_security_group.SG_MICROSERVICES_DEV.id}",
    "${aws_security_group.SG_MONITORING.id}",
    "${aws_security_group.SG_AVIATRIX_SSH.id}",
    "${aws_security_group.SG_JENKINS_CLIENT_SSH.id}"
  ]
  subnet_id = "${var.economical-dev-private-1a-subnet}"
  instance_type = "t3a.xlarge"
  key_name = "EconomicalMicroservicesNPRD"
  associate_public_ip_address = false
  iam_instance_profile = "CMS-SSMInstanceRole"
  ebs_optimized = "true"

  root_block_device {
    volume_size = "200"
  }

  lifecycle {
    prevent_destroy = false
    ignore_changes = [
      "tags.%",
      "tags.BILLINGCODE",
      "tags.BILLINGCONTACT",
      "tags.CLIENT",
      "tags.COUNTRY",
      "tags.CSCLASS",
      "tags.CSQUAL",
      "tags.CSTYPE",
      "tags.ENVIRONMENT",
      "tags.FUNCTION",
      "tags.GROUPCONTACT",
      "tags.MEMBERFIRM",
      "tags.PRIMARYCONTACT",
      "tags.SECONDARYCONTACT",
      "volume_tags.%",
      "volume_tags.AccountID",
      "volume_tags.ApplicationName",
      "volume_tags.BILLINGCODE",
      "volume_tags.BILLINGCONTACT",
      "volume_tags.CLIENT",
      "volume_tags.COUNTRY",
      "volume_tags.CSCLASS",
      "volume_tags.CSQUAL",
      "volume_tags.CSTYPE",
      "volume_tags.DataClassification",
      "volume_tags.Docker",
      "volume_tags.ENVIRONMENT",
      "volume_tags.Environment",
      "volume_tags.FUNCTION",
      "volume_tags.GROUPCONTACT",
      "volume_tags.MEMBERFIRM",
      "volume_tags.ManagedBy",
      "volume_tags.Name",
      "volume_tags.Owner",
      "volume_tags.PRIMARYCONTACT",
      "volume_tags.PatchGroup",
      "volume_tags.ResourceSeverity",
      "volume_tags.ResourceType",
      "volume_tags.Role",
      "volume_tags.SECONDARYCONTACT",
      "volume_tags.SolutionName",
      "volume_tags.VendorManaged"
    ]
  }

  volume_tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "PROJECT"
  }

  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "PROJECT"
    PatchGroup = "AmazonLinux2-AzA-Group1"
    Owner = "robhatnagar@deloitte.ca"
    ManagedBy = "CMS-TF"
    SolutionName = "InsurCloud Economical"
    Environment = "Dev"
    ApplicationName = "docker, microservices"
    DataClassification = "PII, PHI"
    DLM-Backup = "Yes"
    DLM-Retention = "90"
    ResourceType = "Client"
    ResourceSeverity = "Normal"
    Name = "A-MC-CAC-D-A003-ECONOMICAL-DEV02-MICROSERVICES-LNX-EC2"
    Role = "Container"
  }

  provisioner "local-exec" {
    command = "echo ${aws_instance.ICECONOMICAL_DEV02_MICROSERVICES.private_ip} >> dev_hosts.txt"
  }
}

output "ICECONOMICAL_DEV02_MICROSERVICES_PRIVATEIP" {
  value = "${aws_instance.ICECONOMICAL_DEV02_MICROSERVICES.private_ip}"
}

######### RPA ###########
data "template_file" "ECO_DEV02_DOMAIN_JOIN_1" {
  template = "${file("IC-Economical-DEV/windowsdata.tpl")}"

  vars = {
    computer_name = "D02-RPA-APP-WIN"
    region = "${var.region}"
    domain = "${var.ad_domain}"
    dns1 = "${var.ad_dns1}"
    dns2 = "${var.ad_dns2}"
    dns3 = "${var.ad_dns3}"
    ouselection = "${var.ou_name}"
    pwordparameter = "${var.pwordparameter}"
    unameparameter = "${var.unameparameter}"
  }
}

resource "aws_instance" "ICECONOMICAL_DEV02_RPA_APP" {
  ami = "${var.ami_id_windows2016base}"
  vpc_security_group_ids = [
    "${aws_security_group.SG_AVIATRIX_RDP.id}",
    "${aws_security_group.SG_RPA_APP.id}",
    "${aws_security_group.SG_RPA_APP_DEV.id}"
  ]
  subnet_id = "${var.economical-dev-private-1a-subnet}"
  instance_type = "t3a.medium"
  key_name = "EconomicalRPANPRD"
  associate_public_ip_address = false
  iam_instance_profile = "CMS-SSMInstanceRole"
  ebs_optimized = "true"

  root_block_device {
    volume_size = "50"
  }

  user_data = "${data.template_file.ECO_DEV02_DOMAIN_JOIN_1.rendered}"

  lifecycle {
    prevent_destroy = false
    ignore_changes = [
      "tags.%",
      "tags.BILLINGCODE",
      "tags.BILLINGCONTACT",
      "tags.CLIENT",
      "tags.COUNTRY",
      "tags.CSCLASS",
      "tags.CSQUAL",
      "tags.CSTYPE",
      "tags.ENVIRONMENT",
      "tags.FUNCTION",
      "tags.GROUPCONTACT",
      "tags.MEMBERFIRM",
      "tags.PRIMARYCONTACT",
      "tags.SECONDARYCONTACT",
      "volume_tags.%",
      "volume_tags.AccountID",
      "volume_tags.ApplicationName",
      "volume_tags.BILLINGCODE",
      "volume_tags.BILLINGCONTACT",
      "volume_tags.CLIENT",
      "volume_tags.COUNTRY",
      "volume_tags.CSCLASS",
      "volume_tags.CSQUAL",
      "volume_tags.CSTYPE",
      "volume_tags.DataClassification",
      "volume_tags.Docker",
      "volume_tags.ENVIRONMENT",
      "volume_tags.Environment",
      "volume_tags.FUNCTION",
      "volume_tags.GROUPCONTACT",
      "volume_tags.MEMBERFIRM",
      "volume_tags.ManagedBy",
      "volume_tags.Name",
      "volume_tags.Owner",
      "volume_tags.PRIMARYCONTACT",
      "volume_tags.PatchGroup",
      "volume_tags.ResourceSeverity",
      "volume_tags.ResourceType",
      "volume_tags.Role",
      "volume_tags.SECONDARYCONTACT",
      "volume_tags.SolutionName",
      "volume_tags.VendorManaged"
    ]
  }

  volume_tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "PROJECT"
  }

  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "PROJECT"
    PatchGroup = "Windows-AzA-Group1"
    Owner = "robhatnagar@deloitte.ca"
    ManagedBy = "CMS-TF"
    SolutionName = "InsurCloud Economical"
    Environment = "Dev"
    ApplicationName = "rpa, blueprism"
    DataClassification = "PII, PHI"
    DLM-Backup = "Yes"
    DLM-Retention = "90"
    ResourceType = "Client"
    ResourceSeverity = "Normal"
    Name = "A-MC-CAC-D-A003-ECONOMICAL-DEV02-RPA-APP-WIN-EC2"
  }
}

output "ICECONOMICAL_DEV02_RPA_APP_PRIVATEIP" {
  value = "${aws_instance.ICECONOMICAL_DEV02_RPA_APP.private_ip}"
}

data "template_file" "ECO_DEV02_DOMAIN_JOIN_2" {
  template = "${file("IC-Economical-DEV/windowsdata.tpl")}"

  vars = {
    computer_name = "D02-RPA-C01-WIN"
    region = "${var.region}"
    domain = "${var.ad_domain}"
    dns1 = "${var.ad_dns1}"
    dns2 = "${var.ad_dns2}"
    dns3 = "${var.ad_dns3}"
    ouselection = "${var.ou_name}"
    pwordparameter = "${var.pwordparameter}"
    unameparameter = "${var.unameparameter}"
  }
}

resource "aws_instance" "ICECONOMICAL_DEV02_RPA_CLIENT01" {
  ami = "${var.ami_id_windows2016base}"
  vpc_security_group_ids = [
    "${aws_security_group.SG_AVIATRIX_RDP.id}",
    "${aws_security_group.SG_RPA_CLIENT.id}",
    "${aws_security_group.SG_RPA_CLIENT_DEV.id}"
  ]
  subnet_id = "${var.economical-dev-private-1a-subnet}"
  instance_type = "t3a.medium"
  key_name = "EconomicalRPANPRD"
  associate_public_ip_address = false
  iam_instance_profile = "CMS-SSMInstanceRole"
  ebs_optimized = "true"

  root_block_device {
    volume_size = "50"
  }

  user_data = "${data.template_file.ECO_DEV02_DOMAIN_JOIN_2.rendered}"

  lifecycle {
    prevent_destroy = false
    ignore_changes = [
      "tags.%",
      "tags.BILLINGCODE",
      "tags.BILLINGCONTACT",
      "tags.CLIENT",
      "tags.COUNTRY",
      "tags.CSCLASS",
      "tags.CSQUAL",
      "tags.CSTYPE",
      "tags.ENVIRONMENT",
      "tags.FUNCTION",
      "tags.GROUPCONTACT",
      "tags.MEMBERFIRM",
      "tags.PRIMARYCONTACT",
      "tags.SECONDARYCONTACT",
      "volume_tags.%",
      "volume_tags.AccountID",
      "volume_tags.ApplicationName",
      "volume_tags.BILLINGCODE",
      "volume_tags.BILLINGCONTACT",
      "volume_tags.CLIENT",
      "volume_tags.COUNTRY",
      "volume_tags.CSCLASS",
      "volume_tags.CSQUAL",
      "volume_tags.CSTYPE",
      "volume_tags.DataClassification",
      "volume_tags.Docker",
      "volume_tags.ENVIRONMENT",
      "volume_tags.Environment",
      "volume_tags.FUNCTION",
      "volume_tags.GROUPCONTACT",
      "volume_tags.MEMBERFIRM",
      "volume_tags.ManagedBy",
      "volume_tags.Name",
      "volume_tags.Owner",
      "volume_tags.PRIMARYCONTACT",
      "volume_tags.PatchGroup",
      "volume_tags.ResourceSeverity",
      "volume_tags.ResourceType",
      "volume_tags.Role",
      "volume_tags.SECONDARYCONTACT",
      "volume_tags.SolutionName",
      "volume_tags.VendorManaged"
    ]
  }

  volume_tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "PROJECT"
  }

  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "PROJECT"
    PatchGroup = "Windows-AzA-Group1"
    Owner = "robhatnagar@deloitte.ca"
    ManagedBy = "CMS-TF"
    SolutionName = "InsurCloud Economical"
    Environment = "Dev"
    ApplicationName = "rpa, blueprism"
    DataClassification = "PII, PHI"
    DLM-Backup = "Yes"
    DLM-Retention = "90"
    ResourceType = "Client"
    ResourceSeverity = "Normal"
    Name = "A-MC-CAC-D-A003-ECONOMICAL-DEV02-RPA-CLIENT01-WIN-EC2"
  }
}

output "ICECONOMICAL_DEV02_RPA_CLIENT01_PRIVATEIP" {
  value = "${aws_instance.ICECONOMICAL_DEV02_RPA_CLIENT01.private_ip}"
}

data "template_file" "ECO_DEV02_DOMAIN_JOIN_3" {
  template = "${file("IC-Economical-DEV/windowsdata.tpl")}"

  vars = {
    computer_name = "D02-RPA-C02-WIN"
    region = "${var.region}"
    domain = "${var.ad_domain}"
    dns1 = "${var.ad_dns1}"
    dns2 = "${var.ad_dns2}"
    dns3 = "${var.ad_dns3}"
    ouselection = "${var.ou_name}"
    pwordparameter = "${var.pwordparameter}"
    unameparameter = "${var.unameparameter}"
  }
}

resource "aws_instance" "ICECONOMICAL_DEV02_RPA_CLIENT02" {
  ami = "${var.ami_id_windows2016base}"
  vpc_security_group_ids = [
    "${aws_security_group.SG_AVIATRIX_RDP.id}",
    "${aws_security_group.SG_RPA_CLIENT.id}",
    "${aws_security_group.SG_RPA_CLIENT_DEV.id}"
  ]
  subnet_id = "${var.economical-dev-private-1a-subnet}"
  instance_type = "t3a.medium"
  key_name = "EconomicalRPANPRD"
  associate_public_ip_address = false
  iam_instance_profile = "CMS-SSMInstanceRole"
  ebs_optimized = "true"

  root_block_device {
    volume_size = "50"
  }

  user_data = "${data.template_file.ECO_DEV02_DOMAIN_JOIN_3.rendered}"

  lifecycle {
    prevent_destroy = false
    ignore_changes = [
      "tags.%",
      "tags.BILLINGCODE",
      "tags.BILLINGCONTACT",
      "tags.CLIENT",
      "tags.COUNTRY",
      "tags.CSCLASS",
      "tags.CSQUAL",
      "tags.CSTYPE",
      "tags.ENVIRONMENT",
      "tags.FUNCTION",
      "tags.GROUPCONTACT",
      "tags.MEMBERFIRM",
      "tags.PRIMARYCONTACT",
      "tags.SECONDARYCONTACT",
      "volume_tags.%",
      "volume_tags.AccountID",
      "volume_tags.ApplicationName",
      "volume_tags.BILLINGCODE",
      "volume_tags.BILLINGCONTACT",
      "volume_tags.CLIENT",
      "volume_tags.COUNTRY",
      "volume_tags.CSCLASS",
      "volume_tags.CSQUAL",
      "volume_tags.CSTYPE",
      "volume_tags.DataClassification",
      "volume_tags.Docker",
      "volume_tags.ENVIRONMENT",
      "volume_tags.Environment",
      "volume_tags.FUNCTION",
      "volume_tags.GROUPCONTACT",
      "volume_tags.MEMBERFIRM",
      "volume_tags.ManagedBy",
      "volume_tags.Name",
      "volume_tags.Owner",
      "volume_tags.PRIMARYCONTACT",
      "volume_tags.PatchGroup",
      "volume_tags.ResourceSeverity",
      "volume_tags.ResourceType",
      "volume_tags.Role",
      "volume_tags.SECONDARYCONTACT",
      "volume_tags.SolutionName",
      "volume_tags.VendorManaged"
    ]
  }

  volume_tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "PROJECT"
  }

  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "PROJECT"
    PatchGroup = "Windows-AzA-Group1"
    Owner = "robhatnagar@deloitte.ca"
    ManagedBy = "CMS-TF"
    SolutionName = "InsurCloud Economical"
    Environment = "Dev"
    ApplicationName = "rpa, blueprism"
    DataClassification = "PII, PHI"
    DLM-Backup = "Yes"
    DLM-Retention = "90"
    ResourceType = "Client"
    ResourceSeverity = "Normal"
    Name = "A-MC-CAC-D-A003-ECONOMICAL-DEV02-RPA-CLIENT02-WIN-EC2"
  }
}

output "ICECONOMICAL_DEV02_RPA_CLIENT02_PRIVATEIP" {
  value = "${aws_instance.ICECONOMICAL_DEV02_RPA_CLIENT02.private_ip}"
}

######### HAPROXY ##############
resource "aws_instance" "ICECONOMICAL_DEV02_INTERNAL_HAPROXY" {
  ami = "${var.ami_id_amazonlinux2base}"
  vpc_security_group_ids = [
    "${aws_security_group.SG_INTERNAL_HAPROXY_DEV.id}",
    "${aws_security_group.SG_AVIATRIX_SSH.id}",
    "${aws_security_group.SG_JENKINS_CLIENT_SSH.id}"
  ]
  subnet_id = "${var.economical-dev-private-1a-subnet}"
  instance_type = "t3a.small"
  key_name = "EconomicalHAProxyNPRD"
  associate_public_ip_address = false
  iam_instance_profile = "CMS-SSMInstanceRole"
  ebs_optimized = "true"

  root_block_device {
    volume_size = "40"
  }

  lifecycle {
    prevent_destroy = false
    ignore_changes = [
      "tags.%",
      "tags.BILLINGCODE",
      "tags.BILLINGCONTACT",
      "tags.CLIENT",
      "tags.COUNTRY",
      "tags.CSCLASS",
      "tags.CSQUAL",
      "tags.CSTYPE",
      "tags.ENVIRONMENT",
      "tags.FUNCTION",
      "tags.GROUPCONTACT",
      "tags.MEMBERFIRM",
      "tags.PRIMARYCONTACT",
      "tags.SECONDARYCONTACT",
      "volume_tags.%",
      "volume_tags.AccountID",
      "volume_tags.ApplicationName",
      "volume_tags.BILLINGCODE",
      "volume_tags.BILLINGCONTACT",
      "volume_tags.CLIENT",
      "volume_tags.COUNTRY",
      "volume_tags.CSCLASS",
      "volume_tags.CSQUAL",
      "volume_tags.CSTYPE",
      "volume_tags.DataClassification",
      "volume_tags.Docker",
      "volume_tags.ENVIRONMENT",
      "volume_tags.Environment",
      "volume_tags.FUNCTION",
      "volume_tags.GROUPCONTACT",
      "volume_tags.MEMBERFIRM",
      "volume_tags.ManagedBy",
      "volume_tags.Name",
      "volume_tags.Owner",
      "volume_tags.PRIMARYCONTACT",
      "volume_tags.PatchGroup",
      "volume_tags.ResourceSeverity",
      "volume_tags.ResourceType",
      "volume_tags.Role",
      "volume_tags.SECONDARYCONTACT",
      "volume_tags.SolutionName",
      "volume_tags.VendorManaged"
    ]
  }

  volume_tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "PROJECT"
  }

  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "PROJECT"
    PatchGroup = "AmazonLinux2-AzA-Group1"
    Owner = "robhatnagar@deloitte.ca"
    ManagedBy = "CMS-TF"
    SolutionName = "InsurCloud Economical"
    Environment = "Dev"
    ApplicationName = "docker, haproxy"
    DataClassification = "PII, PHI"
    DLM-Backup = "Yes"
    DLM-Retention = "90"
    ResourceType = "Client"
    ResourceSeverity = "Normal"
    Name = "A-MC-CAC-D-A003-ECONOMICAL-DEV02-INTERNAL-HAPROXY-EC2"
    Role = "Container"
  }

  provisioner "local-exec" {
    command = "echo ${aws_instance.ICECONOMICAL_DEV02_INTERNAL_HAPROXY.private_ip} >> dev_hosts.txt"
  }
}

output "ICECONOMICAL_DEV02_INTERNAL_HAPROXY_PRIVATEIP" {
  value = "${aws_instance.ICECONOMICAL_DEV02_INTERNAL_HAPROXY.private_ip}"
}


# #=======================================================================
# # ST01
# #=======================================================================
######### GUIDEWIRE ##############
resource "aws_instance" "ICECONOMICAL_ST01_GUIDEWIRE_SUITE" {
  ami = "${var.ami_id_amazonlinux2base}"
  vpc_security_group_ids = [
    "${aws_security_group.SG_GUIDEWIRE_DEV.id}",
    "${aws_security_group.SG_JENKINS_GUIDEWIRE_HEALTH_CHECK.id}",
    "${aws_security_group.SG_AVIATRIX_SSH.id}",
    "${aws_security_group.SG_JENKINS_CLIENT_SSH.id}",
    "${aws_security_group.SG_MSSQL_DB.id}"
  ]
  subnet_id = "${var.economical-dev-private-1a-subnet}"
  instance_type = "t3a.xlarge"
  key_name = "EconomicalGuidewireNPRD"
  associate_public_ip_address = false
  iam_instance_profile = "CMS-SSMInstanceRole"
  ebs_optimized = "true"

  root_block_device {
    volume_size = "100"
  }

  lifecycle {
    prevent_destroy = false
    ignore_changes = [
      "tags.%",
      "tags.BILLINGCODE",
      "tags.BILLINGCONTACT",
      "tags.CLIENT",
      "tags.COUNTRY",
      "tags.CSCLASS",
      "tags.CSQUAL",
      "tags.CSTYPE",
      "tags.ENVIRONMENT",
      "tags.FUNCTION",
      "tags.GROUPCONTACT",
      "tags.MEMBERFIRM",
      "tags.PRIMARYCONTACT",
      "tags.SECONDARYCONTACT",
      "volume_tags.%",
      "volume_tags.AccountID",
      "volume_tags.ApplicationName",
      "volume_tags.BILLINGCODE",
      "volume_tags.BILLINGCONTACT",
      "volume_tags.CLIENT",
      "volume_tags.COUNTRY",
      "volume_tags.CSCLASS",
      "volume_tags.CSQUAL",
      "volume_tags.CSTYPE",
      "volume_tags.DataClassification",
      "volume_tags.Docker",
      "volume_tags.ENVIRONMENT",
      "volume_tags.Environment",
      "volume_tags.FUNCTION",
      "volume_tags.GROUPCONTACT",
      "volume_tags.MEMBERFIRM",
      "volume_tags.ManagedBy",
      "volume_tags.Name",
      "volume_tags.Owner",
      "volume_tags.PRIMARYCONTACT",
      "volume_tags.PatchGroup",
      "volume_tags.ResourceSeverity",
      "volume_tags.ResourceType",
      "volume_tags.Role",
      "volume_tags.SECONDARYCONTACT",
      "volume_tags.SolutionName",
      "volume_tags.VendorManaged"
    ]
  }

  volume_tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "PROJECT"
  }

  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "PROJECT"
    PatchGroup = "AmazonLinux2-AzA-Group1"
    Owner = "robhatnagar@deloitte.ca"
    ManagedBy = "CMS-TF"
    SolutionName = "InsurCloud Economical"
    Environment = "Dev"
    ApplicationName = "guidewire, mssql, docker"
    DataClassification = "PII, PHI"
    DLM-Backup = "Yes"
    DLM-Retention = "90"
    VendorManaged = "False"
    ResourceType = "Client"
    ResourceSeverity = "Normal"
    Name = "A-MC-CAC-D-A003-ECONOMICAL-ST01-GW-SUITE-LNX-EC2"
    Role = "Container"
  }

  provisioner "local-exec" {
    command = "echo ${aws_instance.ICECONOMICAL_ST01_GUIDEWIRE_SUITE.private_ip} >> dev_hosts.txt"
  }
}

output "ICECONOMICAL_ST01_GUIDEWIRE_SUITE_PRIVATEIP" {
  value = "${aws_instance.ICECONOMICAL_ST01_GUIDEWIRE_SUITE.private_ip}"
}

######### MICROSERVICES ###########
resource "aws_instance" "ICECONOMICAL_ST01_MICROSERVICES" {
  ami = "${var.ami_id_amazonlinux2base}"
  vpc_security_group_ids = [
    "${aws_security_group.SG_MICROSERVICES_DEV.id}",
    "${aws_security_group.SG_MONITORING.id}",
    "${aws_security_group.SG_AVIATRIX_SSH.id}",
    "${aws_security_group.SG_JENKINS_CLIENT_SSH.id}"
  ]
  subnet_id = "${var.economical-dev-private-1a-subnet}"
  instance_type = "t3a.xlarge"
  key_name = "EconomicalMicroservicesNPRD"
  associate_public_ip_address = false
  iam_instance_profile = "CMS-SSMInstanceRole"
  ebs_optimized = "true"

  root_block_device {
    volume_size = "200"
  }

  lifecycle {
    prevent_destroy = false
    ignore_changes = [
      "tags.%",
      "tags.BILLINGCODE",
      "tags.BILLINGCONTACT",
      "tags.CLIENT",
      "tags.COUNTRY",
      "tags.CSCLASS",
      "tags.CSQUAL",
      "tags.CSTYPE",
      "tags.ENVIRONMENT",
      "tags.FUNCTION",
      "tags.GROUPCONTACT",
      "tags.MEMBERFIRM",
      "tags.PRIMARYCONTACT",
      "tags.SECONDARYCONTACT",
      "volume_tags.%",
      "volume_tags.AccountID",
      "volume_tags.ApplicationName",
      "volume_tags.BILLINGCODE",
      "volume_tags.BILLINGCONTACT",
      "volume_tags.CLIENT",
      "volume_tags.COUNTRY",
      "volume_tags.CSCLASS",
      "volume_tags.CSQUAL",
      "volume_tags.CSTYPE",
      "volume_tags.DataClassification",
      "volume_tags.Docker",
      "volume_tags.ENVIRONMENT",
      "volume_tags.Environment",
      "volume_tags.FUNCTION",
      "volume_tags.GROUPCONTACT",
      "volume_tags.MEMBERFIRM",
      "volume_tags.ManagedBy",
      "volume_tags.Name",
      "volume_tags.Owner",
      "volume_tags.PRIMARYCONTACT",
      "volume_tags.PatchGroup",
      "volume_tags.ResourceSeverity",
      "volume_tags.ResourceType",
      "volume_tags.Role",
      "volume_tags.SECONDARYCONTACT",
      "volume_tags.SolutionName",
      "volume_tags.VendorManaged"
    ]
  }

  volume_tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "PROJECT"
  }

  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "PROJECT"
    PatchGroup = "AmazonLinux2-AzA-Group1"
    Owner = "robhatnagar@deloitte.ca"
    ManagedBy = "CMS-TF"
    SolutionName = "InsurCloud Economical"
    Environment = "Dev"
    ApplicationName = "docker, microservices"
    DataClassification = "PII, PHI"
    DLM-Backup = "Yes"
    DLM-Retention = "90"
    VendorManaged = "False"
    ResourceType = "Client"
    ResourceSeverity = "Normal"
    Name = "A-MC-CAC-D-A003-ECONOMICAL-ST01-MICROSERVICES-LNX-EC2"
    Role = "Container"
  }

  provisioner "local-exec" {
    command = "echo ${aws_instance.ICECONOMICAL_ST01_MICROSERVICES.private_ip} >> dev_hosts.txt"
  }
}

output "ICECONOMICAL_ST01_MICROSERVICES_PRIVATEIP" {
  value = "${aws_instance.ICECONOMICAL_ST01_MICROSERVICES.private_ip}"
}

######### RPA ###########
data "template_file" "ECO_ST01_DOMAIN_JOIN_1" {
  template = "${file("IC-Economical-DEV/windowsdata.tpl")}"

  vars = {
    computer_name = "S01-RPA-APP-WIN"
    region = "${var.region}"
    domain = "${var.ad_domain}"
    dns1 = "${var.ad_dns1}"
    dns2 = "${var.ad_dns2}"
    dns3 = "${var.ad_dns3}"
    ouselection = "${var.ou_name}"
    pwordparameter = "${var.pwordparameter}"
    unameparameter = "${var.unameparameter}"
  }
}

resource "aws_instance" "ICECONOMICAL_ST01_RPA_APP" {
  ami = "${var.ami_id_windows2016base}"
  vpc_security_group_ids = [
    "${aws_security_group.SG_AVIATRIX_RDP.id}",
    "${aws_security_group.SG_RPA_APP.id}",
    "${aws_security_group.SG_RPA_APP_DEV.id}"
  ]
  subnet_id = "${var.economical-dev-private-1a-subnet}"
  instance_type = "t3a.medium"
  key_name = "EconomicalRPANPRD"
  associate_public_ip_address = false
  iam_instance_profile = "CMS-SSMInstanceRole"
  ebs_optimized = "true"

  root_block_device {
    volume_size = "50"
  }

  user_data = "${data.template_file.ECO_ST01_DOMAIN_JOIN_1.rendered}"

  lifecycle {
    prevent_destroy = false
    ignore_changes = [
      "tags.%",
      "tags.BILLINGCODE",
      "tags.BILLINGCONTACT",
      "tags.CLIENT",
      "tags.COUNTRY",
      "tags.CSCLASS",
      "tags.CSQUAL",
      "tags.CSTYPE",
      "tags.ENVIRONMENT",
      "tags.FUNCTION",
      "tags.GROUPCONTACT",
      "tags.MEMBERFIRM",
      "tags.PRIMARYCONTACT",
      "tags.SECONDARYCONTACT",
      "volume_tags.%",
      "volume_tags.AccountID",
      "volume_tags.ApplicationName",
      "volume_tags.BILLINGCODE",
      "volume_tags.BILLINGCONTACT",
      "volume_tags.CLIENT",
      "volume_tags.COUNTRY",
      "volume_tags.CSCLASS",
      "volume_tags.CSQUAL",
      "volume_tags.CSTYPE",
      "volume_tags.DataClassification",
      "volume_tags.Docker",
      "volume_tags.ENVIRONMENT",
      "volume_tags.Environment",
      "volume_tags.FUNCTION",
      "volume_tags.GROUPCONTACT",
      "volume_tags.MEMBERFIRM",
      "volume_tags.ManagedBy",
      "volume_tags.Name",
      "volume_tags.Owner",
      "volume_tags.PRIMARYCONTACT",
      "volume_tags.PatchGroup",
      "volume_tags.ResourceSeverity",
      "volume_tags.ResourceType",
      "volume_tags.Role",
      "volume_tags.SECONDARYCONTACT",
      "volume_tags.SolutionName",
      "volume_tags.VendorManaged"
    ]
  }

  volume_tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "PROJECT"
  }

  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "PROJECT"
    PatchGroup = "Windows-AzA-Group1"
    Owner = "robhatnagar@deloitte.ca"
    ManagedBy = "CMS-TF"
    SolutionName = "InsurCloud Economical"
    Environment = "Dev"
    ApplicationName = "rpa, blueprism"
    DataClassification = "PII, PHI"
    DLM-Backup = "Yes"
    DLM-Retention = "90"
    VendorManaged = "False"
    ResourceType = "Client"
    ResourceSeverity = "Normal"
    Name = "A-MC-CAC-D-A003-ECONOMICAL-ST01-RPA-APP-WIN-EC2"
  }
}

output "ICECONOMICAL_ST01_RPA_APP_PRIVATEIP" {
  value = "${aws_instance.ICECONOMICAL_ST01_RPA_APP.private_ip}"
}

data "template_file" "ECO_ST01_DOMAIN_JOIN_2" {
  template = "${file("IC-Economical-DEV/windowsdata.tpl")}"

  vars = {
    computer_name = "S01-RPA-C01-WIN"
    region = "${var.region}"
    domain = "${var.ad_domain}"
    dns1 = "${var.ad_dns1}"
    dns2 = "${var.ad_dns2}"
    dns3 = "${var.ad_dns3}"
    ouselection = "${var.ou_name}"
    pwordparameter = "${var.pwordparameter}"
    unameparameter = "${var.unameparameter}"
  }
}

resource "aws_instance" "ICECONOMICAL_ST01_RPA_CLIENT01" {
  ami = "${var.ami_id_windows2016base}"
  vpc_security_group_ids = [
    "${aws_security_group.SG_AVIATRIX_RDP.id}",
    "${aws_security_group.SG_RPA_CLIENT.id}",
    "${aws_security_group.SG_RPA_CLIENT_DEV.id}"
  ]
  subnet_id = "${var.economical-dev-private-1a-subnet}"
  instance_type = "t3a.medium"
  key_name = "EconomicalRPANPRD"
  associate_public_ip_address = false
  iam_instance_profile = "CMS-SSMInstanceRole"
  ebs_optimized = "true"

  root_block_device {
    volume_size = "50"
  }

  user_data = "${data.template_file.ECO_ST01_DOMAIN_JOIN_2.rendered}"

  lifecycle {
    prevent_destroy = false
    ignore_changes = [
      "tags.%",
      "tags.BILLINGCODE",
      "tags.BILLINGCONTACT",
      "tags.CLIENT",
      "tags.COUNTRY",
      "tags.CSCLASS",
      "tags.CSQUAL",
      "tags.CSTYPE",
      "tags.ENVIRONMENT",
      "tags.FUNCTION",
      "tags.GROUPCONTACT",
      "tags.MEMBERFIRM",
      "tags.PRIMARYCONTACT",
      "tags.SECONDARYCONTACT",
      "volume_tags.%",
      "volume_tags.AccountID",
      "volume_tags.ApplicationName",
      "volume_tags.BILLINGCODE",
      "volume_tags.BILLINGCONTACT",
      "volume_tags.CLIENT",
      "volume_tags.COUNTRY",
      "volume_tags.CSCLASS",
      "volume_tags.CSQUAL",
      "volume_tags.CSTYPE",
      "volume_tags.DataClassification",
      "volume_tags.Docker",
      "volume_tags.ENVIRONMENT",
      "volume_tags.Environment",
      "volume_tags.FUNCTION",
      "volume_tags.GROUPCONTACT",
      "volume_tags.MEMBERFIRM",
      "volume_tags.ManagedBy",
      "volume_tags.Name",
      "volume_tags.Owner",
      "volume_tags.PRIMARYCONTACT",
      "volume_tags.PatchGroup",
      "volume_tags.ResourceSeverity",
      "volume_tags.ResourceType",
      "volume_tags.Role",
      "volume_tags.SECONDARYCONTACT",
      "volume_tags.SolutionName",
      "volume_tags.VendorManaged"
    ]
  }

  volume_tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "PROJECT"
  }

  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "PROJECT"
    PatchGroup = "Windows-AzA-Group1"
    Owner = "robhatnagar@deloitte.ca"
    ManagedBy = "CMS-TF"
    SolutionName = "InsurCloud Economical"
    Environment = "Dev"
    ApplicationName = "rpa, blueprism"
    DataClassification = "PII, PHI"
    DLM-Backup = "Yes"
    DLM-Retention = "90"
    VendorManaged = "False"
    ResourceType = "Client"
    ResourceSeverity = "Normal"
    Name = "A-MC-CAC-D-A003-ECONOMICAL-ST01-RPA-CLIENT01-WIN-EC2"
  }
}

output "ICECONOMICAL_ST01_RPA_CLIENT01_PRIVATEIP" {
  value = "${aws_instance.ICECONOMICAL_ST01_RPA_CLIENT01.private_ip}"
}

data "template_file" "ECO_ST01_DOMAIN_JOIN_3" {
  template = "${file("IC-Economical-DEV/windowsdata.tpl")}"

  vars = {
    computer_name = "S01-RPA-C02-WIN"
    region = "${var.region}"
    domain = "${var.ad_domain}"
    dns1 = "${var.ad_dns1}"
    dns2 = "${var.ad_dns2}"
    dns3 = "${var.ad_dns3}"
    ouselection = "${var.ou_name}"
    pwordparameter = "${var.pwordparameter}"
    unameparameter = "${var.unameparameter}"
  }
}

resource "aws_instance" "ICECONOMICAL_ST01_RPA_CLIENT02" {
  ami = "${var.ami_id_windows2016base20200826}"
  vpc_security_group_ids = [
    "${aws_security_group.SG_AVIATRIX_RDP.id}",
    "${aws_security_group.SG_RPA_CLIENT.id}",
    "${aws_security_group.SG_RPA_CLIENT_DEV.id}"
  ]
  subnet_id = "${var.economical-dev-private-1a-subnet}"
  instance_type = "t3a.medium"
  key_name = "EconomicalRPANPRD"
  associate_public_ip_address = false
  iam_instance_profile = "CMS-SSMInstanceRole"
  ebs_optimized = "true"

  root_block_device {
    volume_size = "50"
  }

  user_data = "${data.template_file.ECO_ST01_DOMAIN_JOIN_3.rendered}"

  lifecycle {
    prevent_destroy = false
    ignore_changes = [
      "tags.%",
      "tags.BILLINGCODE",
      "tags.BILLINGCONTACT",
      "tags.CLIENT",
      "tags.COUNTRY",
      "tags.CSCLASS",
      "tags.CSQUAL",
      "tags.CSTYPE",
      "tags.ENVIRONMENT",
      "tags.FUNCTION",
      "tags.GROUPCONTACT",
      "tags.MEMBERFIRM",
      "tags.PRIMARYCONTACT",
      "tags.SECONDARYCONTACT",
      "volume_tags.%",
      "volume_tags.AccountID",
      "volume_tags.ApplicationName",
      "volume_tags.BILLINGCODE",
      "volume_tags.BILLINGCONTACT",
      "volume_tags.CLIENT",
      "volume_tags.COUNTRY",
      "volume_tags.CSCLASS",
      "volume_tags.CSQUAL",
      "volume_tags.CSTYPE",
      "volume_tags.DataClassification",
      "volume_tags.Docker",
      "volume_tags.ENVIRONMENT",
      "volume_tags.Environment",
      "volume_tags.FUNCTION",
      "volume_tags.GROUPCONTACT",
      "volume_tags.MEMBERFIRM",
      "volume_tags.ManagedBy",
      "volume_tags.Name",
      "volume_tags.Owner",
      "volume_tags.PRIMARYCONTACT",
      "volume_tags.PatchGroup",
      "volume_tags.ResourceSeverity",
      "volume_tags.ResourceType",
      "volume_tags.Role",
      "volume_tags.SECONDARYCONTACT",
      "volume_tags.SolutionName",
      "volume_tags.VendorManaged"
    ]
  }

  volume_tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "PROJECT"
  }

  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "PROJECT"
    PatchGroup = "Windows-AzA-Group1"
    Owner = "robhatnagar@deloitte.ca"
    ManagedBy = "CMS-TF"
    SolutionName = "InsurCloud Economical"
    Environment = "Dev"
    ApplicationName = "rpa, blueprism"
    DataClassification = "PII, PHI"
    DLM-Backup = "Yes"
    DLM-Retention = "90"
    VendorManaged = "False"
    ResourceType = "Client"
    ResourceSeverity = "Normal"
    Name = "A-MC-CAC-D-A003-ECONOMICAL-ST01-RPA-CLIENT02-WIN-EC2"
  }
}

output "ICECONOMICAL_ST01_RPA_CLIENT02_PRIVATEIP" {
  value = "${aws_instance.ICECONOMICAL_ST01_RPA_CLIENT02.private_ip}"
}

######### HAPROXY ##############
resource "aws_instance" "ICECONOMICAL_ST01_INTERNAL_HAPROXY" {
  ami = "${var.ami_id_amazonlinux2base}"
  vpc_security_group_ids = [
    "${aws_security_group.SG_INTERNAL_HAPROXY_DEV.id}",
    "${aws_security_group.SG_AVIATRIX_SSH.id}",
    "${aws_security_group.SG_JENKINS_CLIENT_SSH.id}"
  ]
  subnet_id = "${var.economical-dev-private-1a-subnet}"
  instance_type = "t3a.small"
  key_name = "EconomicalHAProxyNPRD"
  associate_public_ip_address = false
  iam_instance_profile = "CMS-SSMInstanceRole"
  ebs_optimized = "true"

  root_block_device {
    volume_size = "40"
  }

  lifecycle {
    prevent_destroy = false
    ignore_changes = [
      "tags.%",
      "tags.BILLINGCODE",
      "tags.BILLINGCONTACT",
      "tags.CLIENT",
      "tags.COUNTRY",
      "tags.CSCLASS",
      "tags.CSQUAL",
      "tags.CSTYPE",
      "tags.ENVIRONMENT",
      "tags.FUNCTION",
      "tags.GROUPCONTACT",
      "tags.MEMBERFIRM",
      "tags.PRIMARYCONTACT",
      "tags.SECONDARYCONTACT",
      "volume_tags.%",
      "volume_tags.AccountID",
      "volume_tags.ApplicationName",
      "volume_tags.BILLINGCODE",
      "volume_tags.BILLINGCONTACT",
      "volume_tags.CLIENT",
      "volume_tags.COUNTRY",
      "volume_tags.CSCLASS",
      "volume_tags.CSQUAL",
      "volume_tags.CSTYPE",
      "volume_tags.DataClassification",
      "volume_tags.Docker",
      "volume_tags.ENVIRONMENT",
      "volume_tags.Environment",
      "volume_tags.FUNCTION",
      "volume_tags.GROUPCONTACT",
      "volume_tags.MEMBERFIRM",
      "volume_tags.ManagedBy",
      "volume_tags.Name",
      "volume_tags.Owner",
      "volume_tags.PRIMARYCONTACT",
      "volume_tags.PatchGroup",
      "volume_tags.ResourceSeverity",
      "volume_tags.ResourceType",
      "volume_tags.Role",
      "volume_tags.SECONDARYCONTACT",
      "volume_tags.SolutionName",
      "volume_tags.VendorManaged"
    ]
  }

  volume_tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "PROJECT"
  }

  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "PROJECT"
    PatchGroup = "AmazonLinux2-AzA-Group1"
    Owner = "robhatnagar@deloitte.ca"
    ManagedBy = "CMS-TF"
    SolutionName = "InsurCloud Economical"
    Environment = "Dev"
    ApplicationName = "docker, haproxy"
    DataClassification = "PII, PHI"
    DLM-Backup = "Yes"
    DLM-Retention = "90"
    VendorManaged = "False"
    ResourceType = "Client"
    ResourceSeverity = "Normal"
    Name = "A-MC-CAC-D-A003-ECONOMICAL-ST01-INTERNAL-HAPROXY-EC2"
    Role = "Container"
  }

  provisioner "local-exec" {
    command = "echo ${aws_instance.ICECONOMICAL_ST01_INTERNAL_HAPROXY.private_ip} >> dev_hosts.txt"
  }
}

output "ICECONOMICAL_ST01_INTERNAL_HAPROXY_PRIVATEIP" {
  value = "${aws_instance.ICECONOMICAL_ST01_INTERNAL_HAPROXY.private_ip}"
}


# #=======================================================================
# # UAT01
# #=======================================================================
######### GUIDEWIRE ##############
resource "aws_instance" "ICECONOMICAL_UAT01_GUIDEWIRE_SUITE" {
  ami = "${var.ami_id_amazonlinux2base}"
  vpc_security_group_ids = [
    "${aws_security_group.SG_GUIDEWIRE_DEV.id}",
    "${aws_security_group.SG_JENKINS_GUIDEWIRE_HEALTH_CHECK.id}",
    "${aws_security_group.SG_AVIATRIX_SSH.id}",
    "${aws_security_group.SG_JENKINS_CLIENT_SSH.id}",
    "${aws_security_group.SG_MSSQL_DB.id}"
  ]
  subnet_id = "${var.economical-dev-private-1a-subnet}"
  instance_type = "t3a.xlarge"
  key_name = "EconomicalGuidewireNPRD"
  associate_public_ip_address = false
  iam_instance_profile = "CMS-SSMInstanceRole"
  ebs_optimized = "true"

  root_block_device {
    volume_size = "100"
  }

  lifecycle {
    prevent_destroy = false
    ignore_changes = [
      "tags.%",
      "tags.BILLINGCODE",
      "tags.BILLINGCONTACT",
      "tags.CLIENT",
      "tags.COUNTRY",
      "tags.CSCLASS",
      "tags.CSQUAL",
      "tags.CSTYPE",
      "tags.ENVIRONMENT",
      "tags.FUNCTION",
      "tags.GROUPCONTACT",
      "tags.MEMBERFIRM",
      "tags.PRIMARYCONTACT",
      "tags.SECONDARYCONTACT",
      "volume_tags.%",
      "volume_tags.AccountID",
      "volume_tags.ApplicationName",
      "volume_tags.BILLINGCODE",
      "volume_tags.BILLINGCONTACT",
      "volume_tags.CLIENT",
      "volume_tags.COUNTRY",
      "volume_tags.CSCLASS",
      "volume_tags.CSQUAL",
      "volume_tags.CSTYPE",
      "volume_tags.DataClassification",
      "volume_tags.Docker",
      "volume_tags.ENVIRONMENT",
      "volume_tags.Environment",
      "volume_tags.FUNCTION",
      "volume_tags.GROUPCONTACT",
      "volume_tags.MEMBERFIRM",
      "volume_tags.ManagedBy",
      "volume_tags.Name",
      "volume_tags.Owner",
      "volume_tags.PRIMARYCONTACT",
      "volume_tags.PatchGroup",
      "volume_tags.ResourceSeverity",
      "volume_tags.ResourceType",
      "volume_tags.Role",
      "volume_tags.SECONDARYCONTACT",
      "volume_tags.SolutionName",
      "volume_tags.VendorManaged"
    ]
  }

  volume_tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "PROJECT"
  }

  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "PROJECT"
    PatchGroup = "AmazonLinux2-AzA-Group1"
    Owner = "robhatnagar@deloitte.ca"
    ManagedBy = "CMS-TF"
    SolutionName = "InsurCloud Economical"
    Environment = "Dev"
    ApplicationName = "guidewire, mssql, docker"
    DataClassification = "PII, PHI"
    DLM-Backup = "Yes"
    DLM-Retention = "90"
    VendorManaged = "False"
    ResourceType = "Client"
    ResourceSeverity = "Normal"
    Name = "A-MC-CAC-D-A003-ECONOMICAL-UAT01-GW-SUITE-LNX-EC2"
    Role = "Container"
  }

  provisioner "local-exec" {
    command = "echo ${aws_instance.ICECONOMICAL_UAT01_GUIDEWIRE_SUITE.private_ip} >> dev_hosts.txt"
  }
}

output "ICECONOMICAL_UAT01_GUIDEWIRE_SUITE_PRIVATEIP" {
  value = "${aws_instance.ICECONOMICAL_UAT01_GUIDEWIRE_SUITE.private_ip}"
}

######### MICROSERVICES ###########
resource "aws_instance" "ICECONOMICAL_UAT01_MICROSERVICES" {
  ami = "${var.ami_id_amazonlinux2base}"
  vpc_security_group_ids = [
    "${aws_security_group.SG_MICROSERVICES_DEV.id}",
    "${aws_security_group.SG_MONITORING.id}",
    "${aws_security_group.SG_AVIATRIX_SSH.id}",
    "${aws_security_group.SG_JENKINS_CLIENT_SSH.id}"
  ]
  subnet_id = "${var.economical-dev-private-1a-subnet}"
  instance_type = "t3a.xlarge"
  key_name = "EconomicalMicroservicesNPRD"
  associate_public_ip_address = false
  iam_instance_profile = "CMS-SSMInstanceRole"
  ebs_optimized = "true"

  root_block_device {
    volume_size = "200"
  }

  lifecycle {
    prevent_destroy = false
    ignore_changes = [
      "tags.%",
      "tags.BILLINGCODE",
      "tags.BILLINGCONTACT",
      "tags.CLIENT",
      "tags.COUNTRY",
      "tags.CSCLASS",
      "tags.CSQUAL",
      "tags.CSTYPE",
      "tags.ENVIRONMENT",
      "tags.FUNCTION",
      "tags.GROUPCONTACT",
      "tags.MEMBERFIRM",
      "tags.PRIMARYCONTACT",
      "tags.SECONDARYCONTACT",
      "volume_tags.%",
      "volume_tags.AccountID",
      "volume_tags.ApplicationName",
      "volume_tags.BILLINGCODE",
      "volume_tags.BILLINGCONTACT",
      "volume_tags.CLIENT",
      "volume_tags.COUNTRY",
      "volume_tags.CSCLASS",
      "volume_tags.CSQUAL",
      "volume_tags.CSTYPE",
      "volume_tags.DataClassification",
      "volume_tags.Docker",
      "volume_tags.ENVIRONMENT",
      "volume_tags.Environment",
      "volume_tags.FUNCTION",
      "volume_tags.GROUPCONTACT",
      "volume_tags.MEMBERFIRM",
      "volume_tags.ManagedBy",
      "volume_tags.Name",
      "volume_tags.Owner",
      "volume_tags.PRIMARYCONTACT",
      "volume_tags.PatchGroup",
      "volume_tags.ResourceSeverity",
      "volume_tags.ResourceType",
      "volume_tags.Role",
      "volume_tags.SECONDARYCONTACT",
      "volume_tags.SolutionName",
      "volume_tags.VendorManaged"
    ]
  }

  volume_tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "PROJECT"
  }

  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "PROJECT"
    PatchGroup = "AmazonLinux2-AzA-Group1"
    Owner = "robhatnagar@deloitte.ca"
    ManagedBy = "CMS-TF"
    SolutionName = "InsurCloud Economical"
    Environment = "Dev"
    ApplicationName = "docker, microservices"
    DataClassification = "PII, PHI"
    DLM-Backup = "Yes"
    DLM-Retention = "90"
    VendorManaged = "False"
    ResourceType = "Client"
    ResourceSeverity = "Normal"
    Name = "A-MC-CAC-D-A003-ECONOMICAL-UAT01-MICROSERVICES-LNX-EC2"
    Role = "Container"
  }

  provisioner "local-exec" {
    command = "echo ${aws_instance.ICECONOMICAL_UAT01_MICROSERVICES.private_ip} >> dev_hosts.txt"
  }
}

output "ICECONOMICAL_UAT01_MICROSERVICES_PRIVATEIP" {
  value = "${aws_instance.ICECONOMICAL_UAT01_MICROSERVICES.private_ip}"
}

######### RPA ###########
data "template_file" "ECO_UAT01_DOMAIN_JOIN_1" {
  template = "${file("IC-Economical-DEV/windowsdata.tpl")}"

  vars = {
    computer_name = "U01-RPA-APP-WIN"
    region = "${var.region}"
    domain = "${var.ad_domain}"
    dns1 = "${var.ad_dns1}"
    dns2 = "${var.ad_dns2}"
    dns3 = "${var.ad_dns3}"
    ouselection = "${var.ou_name}"
    pwordparameter = "${var.pwordparameter}"
    unameparameter = "${var.unameparameter}"
  }
}

resource "aws_instance" "ICECONOMICAL_UAT01_RPA_APP" {
  ami = "${var.ami_id_windows2016base}"
  vpc_security_group_ids = [
    "${aws_security_group.SG_AVIATRIX_RDP.id}",
    "${aws_security_group.SG_RPA_APP.id}",
    "${aws_security_group.SG_RPA_APP_DEV.id}"
  ]
  subnet_id = "${var.economical-dev-private-1a-subnet}"
  instance_type = "t3a.medium"
  key_name = "EconomicalRPANPRD"
  associate_public_ip_address = false
  iam_instance_profile = "CMS-SSMInstanceRole"
  ebs_optimized = "true"

  root_block_device {
    volume_size = "50"
  }

  user_data = "${data.template_file.ECO_UAT01_DOMAIN_JOIN_1.rendered}"

  lifecycle {
    prevent_destroy = false
    ignore_changes = [
      "tags.%",
      "tags.BILLINGCODE",
      "tags.BILLINGCONTACT",
      "tags.CLIENT",
      "tags.COUNTRY",
      "tags.CSCLASS",
      "tags.CSQUAL",
      "tags.CSTYPE",
      "tags.ENVIRONMENT",
      "tags.FUNCTION",
      "tags.GROUPCONTACT",
      "tags.MEMBERFIRM",
      "tags.PRIMARYCONTACT",
      "tags.SECONDARYCONTACT",
      "volume_tags.%",
      "volume_tags.AccountID",
      "volume_tags.ApplicationName",
      "volume_tags.BILLINGCODE",
      "volume_tags.BILLINGCONTACT",
      "volume_tags.CLIENT",
      "volume_tags.COUNTRY",
      "volume_tags.CSCLASS",
      "volume_tags.CSQUAL",
      "volume_tags.CSTYPE",
      "volume_tags.DataClassification",
      "volume_tags.Docker",
      "volume_tags.ENVIRONMENT",
      "volume_tags.Environment",
      "volume_tags.FUNCTION",
      "volume_tags.GROUPCONTACT",
      "volume_tags.MEMBERFIRM",
      "volume_tags.ManagedBy",
      "volume_tags.Name",
      "volume_tags.Owner",
      "volume_tags.PRIMARYCONTACT",
      "volume_tags.PatchGroup",
      "volume_tags.ResourceSeverity",
      "volume_tags.ResourceType",
      "volume_tags.Role",
      "volume_tags.SECONDARYCONTACT",
      "volume_tags.SolutionName",
      "volume_tags.VendorManaged"
    ]
  }

  volume_tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "PROJECT"
  }

  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "PROJECT"
    PatchGroup = "Windows-AzA-Group1"
    Owner = "robhatnagar@deloitte.ca"
    ManagedBy = "CMS-TF"
    SolutionName = "InsurCloud Economical"
    Environment = "Dev"
    ApplicationName = "rpa, blueprism"
    DataClassification = "PII, PHI"
    DLM-Backup = "Yes"
    DLM-Retention = "90"
    VendorManaged = "False"
    ResourceType = "Client"
    ResourceSeverity = "Normal"
    Name = "A-MC-CAC-D-A003-ECONOMICAL-UAT01-RPA-APP-WIN-EC2"
  }
}

output "ICECONOMICAL_UAT01_RPA_APP_PRIVATEIP" {
  value = "${aws_instance.ICECONOMICAL_UAT01_RPA_APP.private_ip}"
}

data "template_file" "ECO_UAT01_DOMAIN_JOIN_2" {
  template = "${file("IC-Economical-DEV/windowsdata.tpl")}"

  vars = {
    computer_name = "U01-RPA-C01-WIN"
    region = "${var.region}"
    domain = "${var.ad_domain}"
    dns1 = "${var.ad_dns1}"
    dns2 = "${var.ad_dns2}"
    dns3 = "${var.ad_dns3}"
    ouselection = "${var.ou_name}"
    pwordparameter = "${var.pwordparameter}"
    unameparameter = "${var.unameparameter}"
  }
}

resource "aws_instance" "ICECONOMICAL_UAT01_RPA_CLIENT01" {
  ami = "${var.ami_id_windows2016base}"
  vpc_security_group_ids = [
    "${aws_security_group.SG_AVIATRIX_RDP.id}",
    "${aws_security_group.SG_RPA_CLIENT.id}",
    "${aws_security_group.SG_RPA_CLIENT_DEV.id}"
  ]
  subnet_id = "${var.economical-dev-private-1a-subnet}"
  instance_type = "t3a.medium"
  key_name = "EconomicalRPANPRD"
  associate_public_ip_address = false
  iam_instance_profile = "CMS-SSMInstanceRole"
  ebs_optimized = "true"

  root_block_device {
    volume_size = "50"
  }

  user_data = "${data.template_file.ECO_UAT01_DOMAIN_JOIN_2.rendered}"

  lifecycle {
    prevent_destroy = false
    ignore_changes = [
      "tags.%",
      "tags.BILLINGCODE",
      "tags.BILLINGCONTACT",
      "tags.CLIENT",
      "tags.COUNTRY",
      "tags.CSCLASS",
      "tags.CSQUAL",
      "tags.CSTYPE",
      "tags.ENVIRONMENT",
      "tags.FUNCTION",
      "tags.GROUPCONTACT",
      "tags.MEMBERFIRM",
      "tags.PRIMARYCONTACT",
      "tags.SECONDARYCONTACT",
      "volume_tags.%",
      "volume_tags.AccountID",
      "volume_tags.ApplicationName",
      "volume_tags.BILLINGCODE",
      "volume_tags.BILLINGCONTACT",
      "volume_tags.CLIENT",
      "volume_tags.COUNTRY",
      "volume_tags.CSCLASS",
      "volume_tags.CSQUAL",
      "volume_tags.CSTYPE",
      "volume_tags.DataClassification",
      "volume_tags.Docker",
      "volume_tags.ENVIRONMENT",
      "volume_tags.Environment",
      "volume_tags.FUNCTION",
      "volume_tags.GROUPCONTACT",
      "volume_tags.MEMBERFIRM",
      "volume_tags.ManagedBy",
      "volume_tags.Name",
      "volume_tags.Owner",
      "volume_tags.PRIMARYCONTACT",
      "volume_tags.PatchGroup",
      "volume_tags.ResourceSeverity",
      "volume_tags.ResourceType",
      "volume_tags.Role",
      "volume_tags.SECONDARYCONTACT",
      "volume_tags.SolutionName",
      "volume_tags.VendorManaged"
    ]
  }

  volume_tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "PROJECT"
  }

  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "PROJECT"
    PatchGroup = "Windows-AzA-Group1"
    Owner = "robhatnagar@deloitte.ca"
    ManagedBy = "CMS-TF"
    SolutionName = "InsurCloud Economical"
    Environment = "Dev"
    ApplicationName = "rpa, blueprism"
    DataClassification = "PII, PHI"
    DLM-Backup = "Yes"
    DLM-Retention = "90"
    VendorManaged = "False"
    ResourceType = "Client"
    ResourceSeverity = "Normal"
    Name = "A-MC-CAC-D-A003-ECONOMICAL-UAT01-RPA-CLIENT01-WIN-EC2"
  }
}

output "ICECONOMICAL_UAT01_RPA_CLIENT01_PRIVATEIP" {
  value = "${aws_instance.ICECONOMICAL_UAT01_RPA_CLIENT01.private_ip}"
}

data "template_file" "ECO_UAT01_DOMAIN_JOIN_3" {
  template = "${file("IC-Economical-DEV/windowsdata.tpl")}"

  vars = {
    computer_name = "U01-RPA-C02-WIN"
    region = "${var.region}"
    domain = "${var.ad_domain}"
    dns1 = "${var.ad_dns1}"
    dns2 = "${var.ad_dns2}"
    dns3 = "${var.ad_dns3}"
    ouselection = "${var.ou_name}"
    pwordparameter = "${var.pwordparameter}"
    unameparameter = "${var.unameparameter}"
  }
}

resource "aws_instance" "ICECONOMICAL_UAT01_RPA_CLIENT02" {
  ami = "${var.ami_id_windows2016base}"
  vpc_security_group_ids = [
    "${aws_security_group.SG_AVIATRIX_RDP.id}",
    "${aws_security_group.SG_RPA_CLIENT.id}",
    "${aws_security_group.SG_RPA_CLIENT_DEV.id}"
  ]
  subnet_id = "${var.economical-dev-private-1a-subnet}"
  instance_type = "t3a.medium"
  key_name = "EconomicalRPANPRD"
  associate_public_ip_address = false
  iam_instance_profile = "CMS-SSMInstanceRole"
  ebs_optimized = "true"

  root_block_device {
    volume_size = "50"
  }

  user_data = "${data.template_file.ECO_UAT01_DOMAIN_JOIN_3.rendered}"

  lifecycle {
    prevent_destroy = false
    ignore_changes = [
      "tags.%",
      "tags.BILLINGCODE",
      "tags.BILLINGCONTACT",
      "tags.CLIENT",
      "tags.COUNTRY",
      "tags.CSCLASS",
      "tags.CSQUAL",
      "tags.CSTYPE",
      "tags.ENVIRONMENT",
      "tags.FUNCTION",
      "tags.GROUPCONTACT",
      "tags.MEMBERFIRM",
      "tags.PRIMARYCONTACT",
      "tags.SECONDARYCONTACT",
      "volume_tags.%",
      "volume_tags.AccountID",
      "volume_tags.ApplicationName",
      "volume_tags.BILLINGCODE",
      "volume_tags.BILLINGCONTACT",
      "volume_tags.CLIENT",
      "volume_tags.COUNTRY",
      "volume_tags.CSCLASS",
      "volume_tags.CSQUAL",
      "volume_tags.CSTYPE",
      "volume_tags.DataClassification",
      "volume_tags.Docker",
      "volume_tags.ENVIRONMENT",
      "volume_tags.Environment",
      "volume_tags.FUNCTION",
      "volume_tags.GROUPCONTACT",
      "volume_tags.MEMBERFIRM",
      "volume_tags.ManagedBy",
      "volume_tags.Name",
      "volume_tags.Owner",
      "volume_tags.PRIMARYCONTACT",
      "volume_tags.PatchGroup",
      "volume_tags.ResourceSeverity",
      "volume_tags.ResourceType",
      "volume_tags.Role",
      "volume_tags.SECONDARYCONTACT",
      "volume_tags.SolutionName",
      "volume_tags.VendorManaged"
    ]
  }

  volume_tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "PROJECT"
  }

  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "PROJECT"
    PatchGroup = "Windows-AzB-Group1"
    Owner = "robhatnagar@deloitte.ca"
    ManagedBy = "CMS-TF"
    SolutionName = "InsurCloud Economical"
    Environment = "Dev"
    ApplicationName = "rpa, blueprism"
    DataClassification = "PII, PHI"
    DLM-Backup = "Yes"
    DLM-Retention = "90"
    VendorManaged = "False"
    ResourceType = "Client"
    ResourceSeverity = "Normal"
    Name = "A-MC-CAC-D-A003-ECONOMICAL-UAT01-RPA-CLIENT02-WIN-EC2"
  }
}

output "ICECONOMICAL_UAT01_RPA_CLIENT02_PRIVATEIP" {
  value = "${aws_instance.ICECONOMICAL_UAT01_RPA_CLIENT02.private_ip}"
}

data "template_file" "ECO_UAT01_DOMAIN_JOIN_4" {
  template = "${file("IC-Economical-DEV/windowsdata.tpl")}"

  vars = {
    computer_name = "U01-RPA-C03-WIN"
    region = "${var.region}"
    domain = "${var.ad_domain}"
    dns1 = "${var.ad_dns1}"
    dns2 = "${var.ad_dns2}"
    dns3 = "${var.ad_dns3}"
    ouselection = "${var.ou_name}"
    pwordparameter = "${var.pwordparameter}"
    unameparameter = "${var.unameparameter}"
  }
}

resource "aws_instance" "ICECONOMICAL_UAT01_RPA_CLIENT03" {
  ami = "${var.ami_id_windows2016base}"
  vpc_security_group_ids = [
    "${aws_security_group.SG_AVIATRIX_RDP.id}",
    "${aws_security_group.SG_RPA_CLIENT.id}",
    "${aws_security_group.SG_RPA_CLIENT_DEV.id}"
  ]
  subnet_id = "${var.economical-dev-private-1b-subnet}"
  instance_type = "t3a.medium"
  key_name = "EconomicalRPANPRD"
  associate_public_ip_address = false
  iam_instance_profile = "CMS-SSMInstanceRole"
  ebs_optimized = "true"

  root_block_device {
    volume_size = "50"
  }

  user_data = "${data.template_file.ECO_UAT01_DOMAIN_JOIN_4.rendered}"

  lifecycle {
    prevent_destroy = false
    ignore_changes = [
      "tags.%",
      "tags.BILLINGCODE",
      "tags.BILLINGCONTACT",
      "tags.CLIENT",
      "tags.COUNTRY",
      "tags.CSCLASS",
      "tags.CSQUAL",
      "tags.CSTYPE",
      "tags.ENVIRONMENT",
      "tags.FUNCTION",
      "tags.GROUPCONTACT",
      "tags.MEMBERFIRM",
      "tags.PRIMARYCONTACT",
      "tags.SECONDARYCONTACT",
      "volume_tags.%",
      "volume_tags.AccountID",
      "volume_tags.ApplicationName",
      "volume_tags.BILLINGCODE",
      "volume_tags.BILLINGCONTACT",
      "volume_tags.CLIENT",
      "volume_tags.COUNTRY",
      "volume_tags.CSCLASS",
      "volume_tags.CSQUAL",
      "volume_tags.CSTYPE",
      "volume_tags.DataClassification",
      "volume_tags.Docker",
      "volume_tags.ENVIRONMENT",
      "volume_tags.Environment",
      "volume_tags.FUNCTION",
      "volume_tags.GROUPCONTACT",
      "volume_tags.MEMBERFIRM",
      "volume_tags.ManagedBy",
      "volume_tags.Name",
      "volume_tags.Owner",
      "volume_tags.PRIMARYCONTACT",
      "volume_tags.PatchGroup",
      "volume_tags.ResourceSeverity",
      "volume_tags.ResourceType",
      "volume_tags.Role",
      "volume_tags.SECONDARYCONTACT",
      "volume_tags.SolutionName",
      "volume_tags.VendorManaged"
    ]
  }

  volume_tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "PROJECT"
  }

  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "PROJECT"
    PatchGroup = "Windows-AzA-Group1"
    Owner = "robhatnagar@deloitte.ca"
    ManagedBy = "CMS-TF"
    SolutionName = "InsurCloud Economical"
    Environment = "Dev"
    ApplicationName = "rpa, blueprism"
    DataClassification = "PII, PHI"
    DLM-Backup = "Yes"
    DLM-Retention = "90"
    VendorManaged = "False"
    ResourceType = "Client"
    ResourceSeverity = "Normal"
    Name = "A-MC-CAC-D-A003-ECONOMICAL-UAT01-RPA-CLIENT03-WIN-EC2"
  }
}

output "ICECONOMICAL_UAT01_RPA_CLIENT03_PRIVATEIP" {
  value = "${aws_instance.ICECONOMICAL_UAT01_RPA_CLIENT03.private_ip}"
}

data "template_file" "ECO_UAT01_RPA_CLIENT04_DOMAIN_JOIN" {
  template = "${file("IC-Economical-DEV/windowsdata.tpl")}"

  vars = {
    computer_name = "U01-RPA-C04-WIN"
    region = "${var.region}"
    domain = "${var.ad_domain}"
    dns1 = "${var.ad_dns1}"
    dns2 = "${var.ad_dns2}"
    dns3 = "${var.ad_dns3}"
    ouselection = "${var.ou_name}"
    pwordparameter = "${var.pwordparameter}"
    unameparameter = "${var.unameparameter}"
  }
}

resource "aws_instance" "ICECONOMICAL_UAT01_RPA_CLIENT04" {
  ami = "${var.ami_id_windows2016base}"
  vpc_security_group_ids = [
    "${aws_security_group.SG_AVIATRIX_RDP.id}",
    "${aws_security_group.SG_RPA_CLIENT.id}",
    "${aws_security_group.SG_RPA_CLIENT_DEV.id}"
  ]
  subnet_id = "${var.economical-dev-private-1b-subnet}"
  instance_type = "t3a.medium"
  key_name = "EconomicalRPANPRD"
  associate_public_ip_address = false
  iam_instance_profile = "CMS-SSMInstanceRole"
  ebs_optimized = "true"

  root_block_device {
    volume_size = "50"
  }

  user_data = "${data.template_file.ECO_UAT01_RPA_CLIENT04_DOMAIN_JOIN.rendered}"

  lifecycle {
    prevent_destroy = false
    ignore_changes = [
      "tags.%",
      "tags.BILLINGCODE",
      "tags.BILLINGCONTACT",
      "tags.CLIENT",
      "tags.COUNTRY",
      "tags.CSCLASS",
      "tags.CSQUAL",
      "tags.CSTYPE",
      "tags.ENVIRONMENT",
      "tags.FUNCTION",
      "tags.GROUPCONTACT",
      "tags.MEMBERFIRM",
      "tags.PRIMARYCONTACT",
      "tags.SECONDARYCONTACT",
      "volume_tags.%",
      "volume_tags.AccountID",
      "volume_tags.ApplicationName",
      "volume_tags.BILLINGCODE",
      "volume_tags.BILLINGCONTACT",
      "volume_tags.CLIENT",
      "volume_tags.COUNTRY",
      "volume_tags.CSCLASS",
      "volume_tags.CSQUAL",
      "volume_tags.CSTYPE",
      "volume_tags.DataClassification",
      "volume_tags.Docker",
      "volume_tags.ENVIRONMENT",
      "volume_tags.Environment",
      "volume_tags.FUNCTION",
      "volume_tags.GROUPCONTACT",
      "volume_tags.MEMBERFIRM",
      "volume_tags.ManagedBy",
      "volume_tags.Name",
      "volume_tags.Owner",
      "volume_tags.PRIMARYCONTACT",
      "volume_tags.PatchGroup",
      "volume_tags.ResourceSeverity",
      "volume_tags.ResourceType",
      "volume_tags.Role",
      "volume_tags.SECONDARYCONTACT",
      "volume_tags.SolutionName",
      "volume_tags.VendorManaged"
    ]
  }

  volume_tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "PROJECT"
  }

  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "PROJECT"
    PatchGroup = "Windows-AzB-Group1"
    Owner = "robhatnagar@deloitte.ca"
    ManagedBy = "CMS-TF"
    SolutionName = "InsurCloud Economical"
    Environment = "Dev"
    ApplicationName = "rpa, blueprism"
    DataClassification = "PII, PHI"
    DLM-Backup = "Yes"
    DLM-Retention = "90"
    VendorManaged = "False"
    ResourceType = "Client"
    ResourceSeverity = "Normal"
    Name = "A-MC-CAC-D-A003-ECONOMICAL-UAT01-RPA-CLIENT04-WIN-EC2"
  }
}

output "ICECONOMICAL_UAT01_RPA_CLIENT04_PRIVATEIP" {
  value = "${aws_instance.ICECONOMICAL_UAT01_RPA_CLIENT04.private_ip}"
}

data "template_file" "ECO_UAT01_RPA_CLIENT05_DOMAIN_JOIN" {
  template = "${file("IC-Economical-DEV/windowsdata.tpl")}"

  vars = {
    computer_name = "U01-RPA-C05-WIN"
    region = "${var.region}"
    domain = "${var.ad_domain}"
    dns1 = "${var.ad_dns1}"
    dns2 = "${var.ad_dns2}"
    dns3 = "${var.ad_dns3}"
    ouselection = "${var.ou_name}"
    pwordparameter = "${var.pwordparameter}"
    unameparameter = "${var.unameparameter}"
  }
}

resource "aws_instance" "ICECONOMICAL_UAT01_RPA_CLIENT05" {
  ami = "${var.ami_id_windows2016base}"
  vpc_security_group_ids = [
    "${aws_security_group.SG_AVIATRIX_RDP.id}",
    "${aws_security_group.SG_RPA_CLIENT.id}",
    "${aws_security_group.SG_RPA_CLIENT_DEV.id}"
  ]
  subnet_id = "${var.economical-dev-private-1b-subnet}"
  instance_type = "t3a.medium"
  key_name = "EconomicalRPANPRD"
  associate_public_ip_address = false
  iam_instance_profile = "CMS-SSMInstanceRole"
  ebs_optimized = "true"

  root_block_device {
    volume_size = "50"
  }

  user_data = "${data.template_file.ECO_UAT01_RPA_CLIENT05_DOMAIN_JOIN.rendered}"

  lifecycle {
    prevent_destroy = false
    ignore_changes = [
      "tags.%",
      "tags.BILLINGCODE",
      "tags.BILLINGCONTACT",
      "tags.CLIENT",
      "tags.COUNTRY",
      "tags.CSCLASS",
      "tags.CSQUAL",
      "tags.CSTYPE",
      "tags.ENVIRONMENT",
      "tags.FUNCTION",
      "tags.GROUPCONTACT",
      "tags.MEMBERFIRM",
      "tags.PRIMARYCONTACT",
      "tags.SECONDARYCONTACT",
      "volume_tags.%",
      "volume_tags.AccountID",
      "volume_tags.ApplicationName",
      "volume_tags.BILLINGCODE",
      "volume_tags.BILLINGCONTACT",
      "volume_tags.CLIENT",
      "volume_tags.COUNTRY",
      "volume_tags.CSCLASS",
      "volume_tags.CSQUAL",
      "volume_tags.CSTYPE",
      "volume_tags.DataClassification",
      "volume_tags.Docker",
      "volume_tags.ENVIRONMENT",
      "volume_tags.Environment",
      "volume_tags.FUNCTION",
      "volume_tags.GROUPCONTACT",
      "volume_tags.MEMBERFIRM",
      "volume_tags.ManagedBy",
      "volume_tags.Name",
      "volume_tags.Owner",
      "volume_tags.PRIMARYCONTACT",
      "volume_tags.PatchGroup",
      "volume_tags.ResourceSeverity",
      "volume_tags.ResourceType",
      "volume_tags.Role",
      "volume_tags.SECONDARYCONTACT",
      "volume_tags.SolutionName",
      "volume_tags.VendorManaged"
    ]
  }

  volume_tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "PROJECT"
  }

  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "PROJECT"
    PatchGroup = "Windows-AzA-Group1"
    Owner = "robhatnagar@deloitte.ca"
    ManagedBy = "CMS-TF"
    SolutionName = "InsurCloud Economical"
    Environment = "Dev"
    ApplicationName = "rpa, blueprism"
    DataClassification = "PII, PHI"
    DLM-Backup = "Yes"
    DLM-Retention = "90"
    VendorManaged = "False"
    ResourceType = "Client"
    ResourceSeverity = "Normal"
    Name = "A-MC-CAC-D-A003-ECONOMICAL-UAT01-RPA-CLIENT05-WIN-EC2"
  }
}

output "ICECONOMICAL_UAT01_RPA_CLIENT05_PRIVATEIP" {
  value = "${aws_instance.ICECONOMICAL_UAT01_RPA_CLIENT05.private_ip}"
}

data "template_file" "ECO_UAT01_RPA_CLIENT06_DOMAIN_JOIN" {
  template = "${file("IC-Economical-DEV/windowsdata.tpl")}"

  vars = {
    computer_name = "U01-RPA-C06-WIN"
    region = "${var.region}"
    domain = "${var.ad_domain}"
    dns1 = "${var.ad_dns1}"
    dns2 = "${var.ad_dns2}"
    dns3 = "${var.ad_dns3}"
    ouselection = "${var.ou_name}"
    pwordparameter = "${var.pwordparameter}"
    unameparameter = "${var.unameparameter}"
  }
}

resource "aws_instance" "ICECONOMICAL_UAT01_RPA_CLIENT06" {
  ami = "${var.ami_id_windows2016base}"
  vpc_security_group_ids = [
    "${aws_security_group.SG_AVIATRIX_RDP.id}",
    "${aws_security_group.SG_RPA_CLIENT.id}",
    "${aws_security_group.SG_RPA_CLIENT_DEV.id}"
  ]
  subnet_id = "${var.economical-dev-private-1b-subnet}"
  instance_type = "t3a.medium"
  key_name = "EconomicalRPANPRD"
  associate_public_ip_address = false
  iam_instance_profile = "CMS-SSMInstanceRole"
  ebs_optimized = "true"

  root_block_device {
    volume_size = "50"
  }

  user_data = "${data.template_file.ECO_UAT01_RPA_CLIENT06_DOMAIN_JOIN.rendered}"

  lifecycle {
    prevent_destroy = false
    ignore_changes = [
      "tags.%",
      "tags.BILLINGCODE",
      "tags.BILLINGCONTACT",
      "tags.CLIENT",
      "tags.COUNTRY",
      "tags.CSCLASS",
      "tags.CSQUAL",
      "tags.CSTYPE",
      "tags.ENVIRONMENT",
      "tags.FUNCTION",
      "tags.GROUPCONTACT",
      "tags.MEMBERFIRM",
      "tags.PRIMARYCONTACT",
      "tags.SECONDARYCONTACT",
      "volume_tags.%",
      "volume_tags.AccountID",
      "volume_tags.ApplicationName",
      "volume_tags.BILLINGCODE",
      "volume_tags.BILLINGCONTACT",
      "volume_tags.CLIENT",
      "volume_tags.COUNTRY",
      "volume_tags.CSCLASS",
      "volume_tags.CSQUAL",
      "volume_tags.CSTYPE",
      "volume_tags.DataClassification",
      "volume_tags.Docker",
      "volume_tags.ENVIRONMENT",
      "volume_tags.Environment",
      "volume_tags.FUNCTION",
      "volume_tags.GROUPCONTACT",
      "volume_tags.MEMBERFIRM",
      "volume_tags.ManagedBy",
      "volume_tags.Name",
      "volume_tags.Owner",
      "volume_tags.PRIMARYCONTACT",
      "volume_tags.PatchGroup",
      "volume_tags.ResourceSeverity",
      "volume_tags.ResourceType",
      "volume_tags.Role",
      "volume_tags.SECONDARYCONTACT",
      "volume_tags.SolutionName",
      "volume_tags.VendorManaged"
    ]
  }

  volume_tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "PROJECT"
  }

  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "PROJECT"
    PatchGroup = "Windows-AzB-Group1"
    Owner = "robhatnagar@deloitte.ca"
    ManagedBy = "CMS-TF"
    SolutionName = "InsurCloud Economical"
    Environment = "Dev"
    ApplicationName = "rpa, blueprism"
    DataClassification = "PII, PHI"
    DLM-Backup = "Yes"
    DLM-Retention = "90"
    VendorManaged = "False"
    ResourceType = "Client"
    ResourceSeverity = "Normal"
    Name = "A-MC-CAC-D-A003-ECONOMICAL-UAT01-RPA-CLIENT06-WIN-EC2"
  }
}

output "ICECONOMICAL_UAT01_RPA_CLIENT06_PRIVATEIP" {
  value = "${aws_instance.ICECONOMICAL_UAT01_RPA_CLIENT06.private_ip}"
}

data "template_file" "ECO_UAT01_RPA_CLIENT07_DOMAIN_JOIN" {
  template = "${file("IC-Economical-DEV/windowsdata.tpl")}"

  vars = {
    computer_name = "U01-RPA-C07-WIN"
    region = "${var.region}"
    domain = "${var.ad_domain}"
    dns1 = "${var.ad_dns1}"
    dns2 = "${var.ad_dns2}"
    dns3 = "${var.ad_dns3}"
    ouselection = "${var.ou_name}"
    pwordparameter = "${var.pwordparameter}"
    unameparameter = "${var.unameparameter}"
  }
}


######### HAPROXY ##############
resource "aws_instance" "ICECONOMICAL_UAT01_INTERNAL_HAPROXY" {
  ami = "${var.ami_id_amazonlinux2base}"
  vpc_security_group_ids = [
    "${aws_security_group.SG_INTERNAL_HAPROXY_DEV.id}",
    "${aws_security_group.SG_AVIATRIX_SSH.id}",
    "${aws_security_group.SG_JENKINS_CLIENT_SSH.id}"
  ]
  subnet_id = "${var.economical-dev-private-1a-subnet}"
  instance_type = "t3a.small"
  key_name = "EconomicalHAProxyNPRD"
  associate_public_ip_address = false
  iam_instance_profile = "CMS-SSMInstanceRole"
  ebs_optimized = "true"

  root_block_device {
    volume_size = "40"
  }

  lifecycle {
    prevent_destroy = false
    ignore_changes = [
      "tags.%",
      "tags.BILLINGCODE",
      "tags.BILLINGCONTACT",
      "tags.CLIENT",
      "tags.COUNTRY",
      "tags.CSCLASS",
      "tags.CSQUAL",
      "tags.CSTYPE",
      "tags.ENVIRONMENT",
      "tags.FUNCTION",
      "tags.GROUPCONTACT",
      "tags.MEMBERFIRM",
      "tags.PRIMARYCONTACT",
      "tags.SECONDARYCONTACT",
      "volume_tags.%",
      "volume_tags.AccountID",
      "volume_tags.ApplicationName",
      "volume_tags.BILLINGCODE",
      "volume_tags.BILLINGCONTACT",
      "volume_tags.CLIENT",
      "volume_tags.COUNTRY",
      "volume_tags.CSCLASS",
      "volume_tags.CSQUAL",
      "volume_tags.CSTYPE",
      "volume_tags.DataClassification",
      "volume_tags.Docker",
      "volume_tags.ENVIRONMENT",
      "volume_tags.Environment",
      "volume_tags.FUNCTION",
      "volume_tags.GROUPCONTACT",
      "volume_tags.MEMBERFIRM",
      "volume_tags.ManagedBy",
      "volume_tags.Name",
      "volume_tags.Owner",
      "volume_tags.PRIMARYCONTACT",
      "volume_tags.PatchGroup",
      "volume_tags.ResourceSeverity",
      "volume_tags.ResourceType",
      "volume_tags.Role",
      "volume_tags.SECONDARYCONTACT",
      "volume_tags.SolutionName",
      "volume_tags.VendorManaged"
    ]
  }

  volume_tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "PROJECT"
  }

  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "PROJECT"
    PatchGroup = "AmazonLinux2-AzA-Group1"
    Owner = "robhatnagar@deloitte.ca"
    ManagedBy = "CMS-TF"
    SolutionName = "InsurCloud Economical"
    Environment = "Dev"
    ApplicationName = "docker, haproxy"
    DataClassification = "PII, PHI"
    DLM-Backup = "Yes"
    DLM-Retention = "90"
    VendorManaged = "False"
    ResourceType = "Client"
    ResourceSeverity = "Normal"
    Name = "A-MC-CAC-D-A003-ECONOMICAL-UAT01-INTERNAL-HAPROXY-EC2"
    Role = "Container"
  }

  provisioner "local-exec" {
    command = "echo ${aws_instance.ICECONOMICAL_UAT01_INTERNAL_HAPROXY.private_ip} >> dev_hosts.txt"
  }
}

output "ICECONOMICAL_UAT01_INTERNAL_HAPROXY_PRIVATEIP" {
  value = "${aws_instance.ICECONOMICAL_UAT01_INTERNAL_HAPROXY.private_ip}"
}


# #=======================================================================
# # PRF01
# #=======================================================================

######### LOADRUNNER ##############
data "template_file" "ECO_PRF01_DOMAIN_JOIN_LR" {
  template = "${file("IC-Economical-DEV/windowsdata.tpl")}"

  vars = {
    computer_name = "PRF01-LR-WIN"
    region = "${var.region}"
    domain = "${var.ad_domain}"
    dns1 = "${var.ad_dns1}"
    dns2 = "${var.ad_dns2}"
    dns3 = "${var.ad_dns3}"
    ouselection = "${var.ou_name}"
    pwordparameter = "${var.pwordparameter}"
    unameparameter = "${var.unameparameter}"
  }
}

resource "aws_instance" "ICECONOMICAL_PRF01_LOAD_RUNNER" {
  ami = "${var.ami_id_windows2016base}"
  vpc_security_group_ids = [
    "${aws_security_group.SG_AVIATRIX_RDP.id}",
    "${aws_security_group.SG_RPA_APP.id}",
    "${aws_security_group.SG_RPA_APP_DEV.id}"
  ]
  subnet_id = "${var.economical-dev-private-1a-subnet}"
  instance_type = "t3a.large"
  key_name = "EconomicalRPANPRD"
  associate_public_ip_address = false
  iam_instance_profile = "CMS-SSMInstanceRole"
  ebs_optimized = "true"

  root_block_device {
    volume_size = "200"
  }

  user_data = "${data.template_file.ECO_PRF01_DOMAIN_JOIN_LR.rendered}"

  lifecycle {
    prevent_destroy = false
    ignore_changes = [
      "tags.%",
      "tags.BILLINGCODE",
      "tags.BILLINGCONTACT",
      "tags.CLIENT",
      "tags.COUNTRY",
      "tags.CSCLASS",
      "tags.CSQUAL",
      "tags.CSTYPE",
      "tags.ENVIRONMENT",
      "tags.FUNCTION",
      "tags.GROUPCONTACT",
      "tags.MEMBERFIRM",
      "tags.PRIMARYCONTACT",
      "tags.SECONDARYCONTACT",
      "volume_tags.%",
      "volume_tags.AccountID",
      "volume_tags.ApplicationName",
      "volume_tags.BILLINGCODE",
      "volume_tags.BILLINGCONTACT",
      "volume_tags.CLIENT",
      "volume_tags.COUNTRY",
      "volume_tags.CSCLASS",
      "volume_tags.CSQUAL",
      "volume_tags.CSTYPE",
      "volume_tags.DataClassification",
      "volume_tags.Docker",
      "volume_tags.ENVIRONMENT",
      "volume_tags.Environment",
      "volume_tags.FUNCTION",
      "volume_tags.GROUPCONTACT",
      "volume_tags.MEMBERFIRM",
      "volume_tags.ManagedBy",
      "volume_tags.Name",
      "volume_tags.Owner",
      "volume_tags.PRIMARYCONTACT",
      "volume_tags.PatchGroup",
      "volume_tags.ResourceSeverity",
      "volume_tags.ResourceType",
      "volume_tags.Role",
      "volume_tags.SECONDARYCONTACT",
      "volume_tags.SolutionName",
      "volume_tags.VendorManaged"
    ]
  }

  volume_tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "PROJECT"
  }

  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "PROJECT"
    PatchGroup = "Windows-AzA-Group1"
    Owner = "robhatnagar@deloitte.ca"
    ManagedBy = "CMS-TF"
    SolutionName = "InsurCloud Economical"
    Environment = "Dev"
    ApplicationName = "loadrunner"
    DataClassification = "PII, PHI"
    DLM-Backup = "Yes"
    DLM-Retention = "90"
    VendorManaged = "False"
    ResourceType = "Client"
    ResourceSeverity = "Normal"
    Name = "A-MC-CAC-D-A003-ECONOMICAL-PRF01-LOAD-RUNNER-WIN-EC2"
    Role = "Container"
  }
}

output "ICECONOMICAL_PRF01_LOAD_RUNNER_PRIVATEIP" {
  value = "${aws_instance.ICECONOMICAL_PRF01_LOAD_RUNNER.private_ip}"
}

######### GUIDEWIRE ##############
resource "aws_instance" "ICECONOMICAL_PRF01_GUIDEWIRE01_SUITE" {
  ami = "${var.ami_id_amazonlinux2base}"
  vpc_security_group_ids = [
    "${aws_security_group.SG_GUIDEWIRE.id}",
    "${aws_security_group.SG_JENKINS_GUIDEWIRE_HEALTH_CHECK.id}",
    "${aws_security_group.SG_GUIDEWIRE_CLUSTER_NBIO_EVENT_QUEUE.id}",
    "${aws_security_group.SG_AVIATRIX_SSH.id}",
    "${aws_security_group.SG_JENKINS_CLIENT_SSH.id}"
  ]
  subnet_id = "${var.economical-dev-private-1a-subnet}"
  instance_type = "m5a.xlarge"
  key_name = "EconomicalGuidewireNPRD"
  associate_public_ip_address = false
  iam_instance_profile = "CMS-SSMInstanceRole"
  ebs_optimized = "true"

  root_block_device {
    volume_size = "100"
  }

  lifecycle {
    prevent_destroy = false
    ignore_changes = [
      "tags.%",
      "tags.BILLINGCODE",
      "tags.BILLINGCONTACT",
      "tags.CLIENT",
      "tags.COUNTRY",
      "tags.CSCLASS",
      "tags.CSQUAL",
      "tags.CSTYPE",
      "tags.ENVIRONMENT",
      "tags.FUNCTION",
      "tags.GROUPCONTACT",
      "tags.MEMBERFIRM",
      "tags.PRIMARYCONTACT",
      "tags.SECONDARYCONTACT",
      "volume_tags.%",
      "volume_tags.AccountID",
      "volume_tags.ApplicationName",
      "volume_tags.BILLINGCODE",
      "volume_tags.BILLINGCONTACT",
      "volume_tags.CLIENT",
      "volume_tags.COUNTRY",
      "volume_tags.CSCLASS",
      "volume_tags.CSQUAL",
      "volume_tags.CSTYPE",
      "volume_tags.DataClassification",
      "volume_tags.Docker",
      "volume_tags.ENVIRONMENT",
      "volume_tags.Environment",
      "volume_tags.FUNCTION",
      "volume_tags.GROUPCONTACT",
      "volume_tags.MEMBERFIRM",
      "volume_tags.ManagedBy",
      "volume_tags.Name",
      "volume_tags.Owner",
      "volume_tags.PRIMARYCONTACT",
      "volume_tags.PatchGroup",
      "volume_tags.ResourceSeverity",
      "volume_tags.ResourceType",
      "volume_tags.Role",
      "volume_tags.SECONDARYCONTACT",
      "volume_tags.SolutionName",
      "volume_tags.VendorManaged"
    ]
  }

  volume_tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "PROJECT"
  }

  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "PROJECT"
    PatchGroup = "AmazonLinux2-AzA-Group1"
    Owner = "robhatnagar@deloitte.ca"
    ManagedBy = "CMS-TF"
    SolutionName = "InsurCloud Economical"
    Environment = "Dev"
    ApplicationName = "guidewire, docker"
    DataClassification = "PII, PHI"
    DLM-Backup = "Yes"
    DLM-Retention = "90"
    VendorManaged = "False"
    ResourceType = "Client"
    ResourceSeverity = "Normal"
    Name = "A-MC-CAC-D-A003-ECONOMICAL-PRF01-GW01-SUITE-LNX-EC2"
    Role = "Container"
  }

  provisioner "local-exec" {
    command = "echo ${aws_instance.ICECONOMICAL_PRF01_GUIDEWIRE01_SUITE.private_ip} >> dev_hosts.txt"
  }
}

output "ICECONOMICAL_PRF01_GUIDEWIRE01_SUITE_PRIVATEIP" {
  value = "${aws_instance.ICECONOMICAL_PRF01_GUIDEWIRE01_SUITE.private_ip}"
}

resource "aws_instance" "ICECONOMICAL_PRF01_GUIDEWIRE02_SUITE" {
  ami = "${var.ami_id_amazonlinux2base}"
  vpc_security_group_ids = [
    "${aws_security_group.SG_GUIDEWIRE.id}",
    "${aws_security_group.SG_JENKINS_GUIDEWIRE_HEALTH_CHECK.id}",
    "${aws_security_group.SG_GUIDEWIRE_CLUSTER_NBIO_EVENT_QUEUE.id}",
    "${aws_security_group.SG_AVIATRIX_SSH.id}",
    "${aws_security_group.SG_JENKINS_CLIENT_SSH.id}"
  ]
  subnet_id = "${var.economical-dev-private-1b-subnet}"
  instance_type = "m5a.xlarge"
  key_name = "EconomicalGuidewireNPRD"
  associate_public_ip_address = false
  iam_instance_profile = "CMS-SSMInstanceRole"
  ebs_optimized = "true"

  root_block_device {
    volume_size = "100"
  }

  lifecycle {
    prevent_destroy = false
    ignore_changes = [
      "tags.%",
      "tags.BILLINGCODE",
      "tags.BILLINGCONTACT",
      "tags.CLIENT",
      "tags.COUNTRY",
      "tags.CSCLASS",
      "tags.CSQUAL",
      "tags.CSTYPE",
      "tags.ENVIRONMENT",
      "tags.FUNCTION",
      "tags.GROUPCONTACT",
      "tags.MEMBERFIRM",
      "tags.PRIMARYCONTACT",
      "tags.SECONDARYCONTACT",
      "volume_tags.%",
      "volume_tags.AccountID",
      "volume_tags.ApplicationName",
      "volume_tags.BILLINGCODE",
      "volume_tags.BILLINGCONTACT",
      "volume_tags.CLIENT",
      "volume_tags.COUNTRY",
      "volume_tags.CSCLASS",
      "volume_tags.CSQUAL",
      "volume_tags.CSTYPE",
      "volume_tags.DataClassification",
      "volume_tags.Docker",
      "volume_tags.ENVIRONMENT",
      "volume_tags.Environment",
      "volume_tags.FUNCTION",
      "volume_tags.GROUPCONTACT",
      "volume_tags.MEMBERFIRM",
      "volume_tags.ManagedBy",
      "volume_tags.Name",
      "volume_tags.Owner",
      "volume_tags.PRIMARYCONTACT",
      "volume_tags.PatchGroup",
      "volume_tags.ResourceSeverity",
      "volume_tags.ResourceType",
      "volume_tags.Role",
      "volume_tags.SECONDARYCONTACT",
      "volume_tags.SolutionName",
      "volume_tags.VendorManaged"
    ]
  }

  volume_tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "PROJECT"
  }

  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "PROJECT"
    PatchGroup = "AmazonLinux2-AzB-Group1"
    Owner = "robhatnagar@deloitte.ca"
    ManagedBy = "CMS-TF"
    SolutionName = "InsurCloud Economical"
    Environment = "Dev"
    ApplicationName = "guidewire, docker"
    DataClassification = "PII, PHI"
    DLM-Backup = "Yes"
    DLM-Retention = "90"
    VendorManaged = "False"
    ResourceType = "Client"
    ResourceSeverity = "Normal"
    Name = "A-MC-CAC-D-A003-ECONOMICAL-PRF01-GW02-SUITE-LNX-EC2"
    Role = "Container"
  }

  provisioner "local-exec" {
    command = "echo ${aws_instance.ICECONOMICAL_PRF01_GUIDEWIRE02_SUITE.private_ip} >> dev_hosts.txt"
  }
}

output "ICECONOMICAL_PRF01_GUIDEWIRE02_SUITE_PRIVATEIP" {
  value = "${aws_instance.ICECONOMICAL_PRF01_GUIDEWIRE02_SUITE.private_ip}"
}

resource "aws_instance" "ICECONOMICAL_PRF01_GUIDEWIRE03_SUITE" {
  ami = "${var.ami_id_amazonlinux2base}"
  vpc_security_group_ids = [
    "${aws_security_group.SG_GUIDEWIRE.id}",
    "${aws_security_group.SG_JENKINS_GUIDEWIRE_HEALTH_CHECK.id}",
    "${aws_security_group.SG_GUIDEWIRE_CLUSTER_NBIO_EVENT_QUEUE.id}",
    "${aws_security_group.SG_AVIATRIX_SSH.id}",
    "${aws_security_group.SG_JENKINS_CLIENT_SSH.id}"
  ]
  subnet_id = "${var.economical-dev-private-1a-subnet}"
  instance_type = "m5a.xlarge"
  key_name = "EconomicalGuidewireNPRD"
  associate_public_ip_address = false
  iam_instance_profile = "CMS-SSMInstanceRole"
  ebs_optimized = "true"

  root_block_device {
    volume_size = "100"
  }

  lifecycle {
    prevent_destroy = false
    ignore_changes = [
      "tags.%",
      "tags.BILLINGCODE",
      "tags.BILLINGCONTACT",
      "tags.CLIENT",
      "tags.COUNTRY",
      "tags.CSCLASS",
      "tags.CSQUAL",
      "tags.CSTYPE",
      "tags.ENVIRONMENT",
      "tags.FUNCTION",
      "tags.GROUPCONTACT",
      "tags.MEMBERFIRM",
      "tags.PRIMARYCONTACT",
      "tags.SECONDARYCONTACT",
      "volume_tags.%",
      "volume_tags.AccountID",
      "volume_tags.ApplicationName",
      "volume_tags.BILLINGCODE",
      "volume_tags.BILLINGCONTACT",
      "volume_tags.CLIENT",
      "volume_tags.COUNTRY",
      "volume_tags.CSCLASS",
      "volume_tags.CSQUAL",
      "volume_tags.CSTYPE",
      "volume_tags.DataClassification",
      "volume_tags.Docker",
      "volume_tags.ENVIRONMENT",
      "volume_tags.Environment",
      "volume_tags.FUNCTION",
      "volume_tags.GROUPCONTACT",
      "volume_tags.MEMBERFIRM",
      "volume_tags.ManagedBy",
      "volume_tags.Name",
      "volume_tags.Owner",
      "volume_tags.PRIMARYCONTACT",
      "volume_tags.PatchGroup",
      "volume_tags.ResourceSeverity",
      "volume_tags.ResourceType",
      "volume_tags.Role",
      "volume_tags.SECONDARYCONTACT",
      "volume_tags.SolutionName",
      "volume_tags.VendorManaged"
    ]
  }

  volume_tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "PROJECT"
  }

  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "PROJECT"
    PatchGroup = "AmazonLinux2-AzA-Group1"
    Owner = "robhatnagar@deloitte.ca"
    ManagedBy = "CMS-TF"
    SolutionName = "InsurCloud Economical"
    Environment = "Dev"
    ApplicationName = "guidewire, docker"
    DataClassification = "PII, PHI"
    DLM-Backup = "Yes"
    DLM-Retention = "90"
    VendorManaged = "False"
    ResourceType = "Client"
    ResourceSeverity = "Normal"
    Name = "A-MC-CAC-D-A003-ECONOMICAL-PRF01-GW03-SUITE-LNX-EC2"
    Role = "Container"
  }

  provisioner "local-exec" {
    command = "echo ${aws_instance.ICECONOMICAL_PRF01_GUIDEWIRE03_SUITE.private_ip} >> dev_hosts.txt"
  }
}

output "ICECONOMICAL_PRF01_GUIDEWIRE03_SUITE_PRIVATEIP" {
  value = "${aws_instance.ICECONOMICAL_PRF01_GUIDEWIRE03_SUITE.private_ip}"
}

resource "aws_instance" "ICECONOMICAL_PRF01_GUIDEWIRE04_SUITE" {
  ami = "${var.ami_id_amazonlinux2base}"
  vpc_security_group_ids = [
    "${aws_security_group.SG_GUIDEWIRE.id}",
    "${aws_security_group.SG_JENKINS_GUIDEWIRE_HEALTH_CHECK.id}",
    "${aws_security_group.SG_GUIDEWIRE_CLUSTER_NBIO_EVENT_QUEUE.id}",
    "${aws_security_group.SG_AVIATRIX_SSH.id}",
    "${aws_security_group.SG_JENKINS_CLIENT_SSH.id}"
  ]
  subnet_id = "${var.economical-dev-private-1b-subnet}"
  instance_type = "m5a.xlarge"
  key_name = "EconomicalGuidewireNPRD"
  associate_public_ip_address = false
  iam_instance_profile = "CMS-SSMInstanceRole"
  ebs_optimized = "true"

  root_block_device {
    volume_size = "100"
  }

  lifecycle {
    prevent_destroy = false
    ignore_changes = [
      "tags.%",
      "tags.BILLINGCODE",
      "tags.BILLINGCONTACT",
      "tags.CLIENT",
      "tags.COUNTRY",
      "tags.CSCLASS",
      "tags.CSQUAL",
      "tags.CSTYPE",
      "tags.ENVIRONMENT",
      "tags.FUNCTION",
      "tags.GROUPCONTACT",
      "tags.MEMBERFIRM",
      "tags.PRIMARYCONTACT",
      "tags.SECONDARYCONTACT",
      "volume_tags.%",
      "volume_tags.AccountID",
      "volume_tags.ApplicationName",
      "volume_tags.BILLINGCODE",
      "volume_tags.BILLINGCONTACT",
      "volume_tags.CLIENT",
      "volume_tags.COUNTRY",
      "volume_tags.CSCLASS",
      "volume_tags.CSQUAL",
      "volume_tags.CSTYPE",
      "volume_tags.DataClassification",
      "volume_tags.Docker",
      "volume_tags.ENVIRONMENT",
      "volume_tags.Environment",
      "volume_tags.FUNCTION",
      "volume_tags.GROUPCONTACT",
      "volume_tags.MEMBERFIRM",
      "volume_tags.ManagedBy",
      "volume_tags.Name",
      "volume_tags.Owner",
      "volume_tags.PRIMARYCONTACT",
      "volume_tags.PatchGroup",
      "volume_tags.ResourceSeverity",
      "volume_tags.ResourceType",
      "volume_tags.Role",
      "volume_tags.SECONDARYCONTACT",
      "volume_tags.SolutionName",
      "volume_tags.VendorManaged"
    ]
  }

  volume_tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "PROJECT"
  }

  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "PROJECT"
    PatchGroup = "AmazonLinux2-AzB-Group1"
    Owner = "robhatnagar@deloitte.ca"
    ManagedBy = "CMS-TF"
    SolutionName = "InsurCloud Economical"
    Environment = "Dev"
    ApplicationName = "guidewire, docker"
    DataClassification = "PII, PHI"
    DLM-Backup = "Yes"
    DLM-Retention = "90"
    VendorManaged = "False"
    ResourceType = "Client"
    ResourceSeverity = "Normal"
    Name = "A-MC-CAC-D-A003-ECONOMICAL-PRF01-GW04-SUITE-LNX-EC2"
    Role = "Container"
  }

  provisioner "local-exec" {
    command = "echo ${aws_instance.ICECONOMICAL_PRF01_GUIDEWIRE04_SUITE.private_ip} >> dev_hosts.txt"
  }
}

output "ICECONOMICAL_PRF01_GUIDEWIRE04_SUITE_PRIVATEIP" {
  value = "${aws_instance.ICECONOMICAL_PRF01_GUIDEWIRE04_SUITE.private_ip}"
}

data "template_file" "ECO_PRF01_DOMAIN_JOIN_GW_DB" {
  template = "${file("IC-Economical-DEV/windowsdata.tpl")}"

  vars = {
    computer_name = "PRF01-GW-DB-W"
    region = "${var.region}"
    domain = "${var.ad_domain}"
    dns1 = "${var.ad_dns1}"
    dns2 = "${var.ad_dns2}"
    dns3 = "${var.ad_dns3}"
    ouselection = "${var.ou_name}"
    pwordparameter = "${var.pwordparameter}"
    unameparameter = "${var.unameparameter}"
  }
}

resource "aws_instance" "ICECONOMICAL_PRF01_GUIDEWIRE_DB" {
  ami = "${var.ami_id_windows2019base}"
  vpc_security_group_ids = [
    "${aws_security_group.SG_AVIATRIX_RDP.id}",
    "${aws_security_group.SG_MSSQL_DB.id}"
  ]
  subnet_id = "${var.economical-dev-private-1a-subnet}"
  instance_type = "r5a.large"
  key_name = "EconomicalGuidewireNPRD"
  associate_public_ip_address = false
  iam_instance_profile = "CMS-SSMInstanceRole"
  ebs_optimized = "true"

  root_block_device {
    volume_size = "50"
  }

  user_data = "${data.template_file.ECO_PRF01_DOMAIN_JOIN_GW_DB.rendered}"

  lifecycle {
    prevent_destroy = false
    ignore_changes = [
      "tags.%",
      "tags.BILLINGCODE",
      "tags.BILLINGCONTACT",
      "tags.CLIENT",
      "tags.COUNTRY",
      "tags.CSCLASS",
      "tags.CSQUAL",
      "tags.CSTYPE",
      "tags.ENVIRONMENT",
      "tags.FUNCTION",
      "tags.GROUPCONTACT",
      "tags.MEMBERFIRM",
      "tags.PRIMARYCONTACT",
      "tags.SECONDARYCONTACT",
      "volume_tags.%",
      "volume_tags.AccountID",
      "volume_tags.ApplicationName",
      "volume_tags.BILLINGCODE",
      "volume_tags.BILLINGCONTACT",
      "volume_tags.CLIENT",
      "volume_tags.COUNTRY",
      "volume_tags.CSCLASS",
      "volume_tags.CSQUAL",
      "volume_tags.CSTYPE",
      "volume_tags.DataClassification",
      "volume_tags.Docker",
      "volume_tags.ENVIRONMENT",
      "volume_tags.Environment",
      "volume_tags.FUNCTION",
      "volume_tags.GROUPCONTACT",
      "volume_tags.MEMBERFIRM",
      "volume_tags.ManagedBy",
      "volume_tags.Name",
      "volume_tags.Owner",
      "volume_tags.PRIMARYCONTACT",
      "volume_tags.PatchGroup",
      "volume_tags.ResourceSeverity",
      "volume_tags.ResourceType",
      "volume_tags.Role",
      "volume_tags.SECONDARYCONTACT",
      "volume_tags.SolutionName",
      "volume_tags.VendorManaged"
    ]
  }

  volume_tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "PROJECT"
  }

  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "PROJECT"
    PatchGroup = "Windows-AzA-Group1"
    Owner = "robhatnagar@deloitte.ca"
    ManagedBy = "CMS-TF"
    SolutionName = "InsurCloud Economical"
    Environment = "Dev"
    ApplicationName = "mssql, guidewire"
    DataClassification = "PII, PHI"
    DLM-Backup = "Yes"
    DLM-Retention = "90"
    VendorManaged = "False"
    ResourceType = "Client"
    ResourceSeverity = "Normal"
    Name = "A-MC-CAC-D-A003-ECONOMICAL-PRF01-GW-MSSQL-DB01-WIN-EC2"
  }
}

output "ICECONOMICAL_PRF01_GUIDEWIRE_DB_PRIVATEIP" {
  value = "${aws_instance.ICECONOMICAL_PRF01_GUIDEWIRE_DB.private_ip}"
}

######### MICROSERVICES ###########
resource "aws_instance" "ICECONOMICAL_PRF01_MICROSERVICES_MANAGER01" {
  ami = "${var.ami_id_amazonlinuxbase20200506}"
  vpc_security_group_ids = [
    "${aws_security_group.SG_MICROSERVICES.id}",
    "${aws_security_group.SG_MONITORING.id}",
    "${aws_security_group.SG_AVIATRIX_SSH.id}",
    "${aws_security_group.SG_JENKINS_CLIENT_SSH.id}"
  ]
  subnet_id = "${var.economical-dev-private-1a-subnet}"
  instance_type = "t3a.medium"
  key_name = "EconomicalMicroservicesNPRD"
  associate_public_ip_address = false
  iam_instance_profile = "CMS-SSMInstanceRole"
  ebs_optimized = "true"

  root_block_device {
    volume_size = "16"
  }

  lifecycle {
    prevent_destroy = false
    ignore_changes = [
      "tags.%",
      "tags.BILLINGCODE",
      "tags.BILLINGCONTACT",
      "tags.CLIENT",
      "tags.COUNTRY",
      "tags.CSCLASS",
      "tags.CSQUAL",
      "tags.CSTYPE",
      "tags.ENVIRONMENT",
      "tags.FUNCTION",
      "tags.GROUPCONTACT",
      "tags.MEMBERFIRM",
      "tags.PRIMARYCONTACT",
      "tags.SECONDARYCONTACT",
      "volume_tags.%",
      "volume_tags.AccountID",
      "volume_tags.ApplicationName",
      "volume_tags.BILLINGCODE",
      "volume_tags.BILLINGCONTACT",
      "volume_tags.CLIENT",
      "volume_tags.COUNTRY",
      "volume_tags.CSCLASS",
      "volume_tags.CSQUAL",
      "volume_tags.CSTYPE",
      "volume_tags.DataClassification",
      "volume_tags.Docker",
      "volume_tags.ENVIRONMENT",
      "volume_tags.Environment",
      "volume_tags.FUNCTION",
      "volume_tags.GROUPCONTACT",
      "volume_tags.MEMBERFIRM",
      "volume_tags.ManagedBy",
      "volume_tags.Name",
      "volume_tags.Owner",
      "volume_tags.PRIMARYCONTACT",
      "volume_tags.PatchGroup",
      "volume_tags.ResourceSeverity",
      "volume_tags.ResourceType",
      "volume_tags.Role",
      "volume_tags.SECONDARYCONTACT",
      "volume_tags.SolutionName",
      "volume_tags.VendorManaged"
    ]
  }

  volume_tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "PROJECT"
  }

  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "PROJECT"
    PatchGroup = "AmazonLinux2-AzA-Group2"
    Owner = "robhatnagar@deloitte.ca"
    ManagedBy = "CMS-TF"
    SolutionName = "InsurCloud Economical"
    Environment = "Dev"
    ApplicationName = "docker, microservices"
    DataClassification = "PII, PHI"
    DLM-Backup = "Yes"
    DLM-Retention = "90"
    VendorManaged = "False"
    ResourceType = "Client"
    ResourceSeverity = "Normal"
    Name = "A-MC-CAC-D-A003-ECONOMICAL-PRF01-MS-MANAGER01-LNX-EC2"
    Role = "Container"
  }

  provisioner "local-exec" {
    command = "echo ${aws_instance.ICECONOMICAL_PRF01_MICROSERVICES_MANAGER01.private_ip} >> dev_hosts.txt"
  }
}

output "ICECONOMICAL_PRF01_MICROSERVICES_MANAGER01_PRIVATEIP" {
  value = "${aws_instance.ICECONOMICAL_PRF01_MICROSERVICES_MANAGER01.private_ip}"
}

resource "aws_instance" "ICECONOMICAL_PRF01_MICROSERVICES_MANAGER02" {
  ami = "${var.ami_id_amazonlinuxbase20200506}"
  vpc_security_group_ids = [
    "${aws_security_group.SG_MICROSERVICES.id}",
    "${aws_security_group.SG_MONITORING.id}",
    "${aws_security_group.SG_AVIATRIX_SSH.id}",
    "${aws_security_group.SG_JENKINS_CLIENT_SSH.id}"
  ]
  subnet_id = "${var.economical-dev-private-1a-subnet}"
  instance_type = "t3a.medium"
  key_name = "EconomicalMicroservicesNPRD"
  associate_public_ip_address = false
  iam_instance_profile = "CMS-SSMInstanceRole"
  ebs_optimized = "true"

  root_block_device {
    volume_size = "16"
  }

  lifecycle {
    prevent_destroy = false
    ignore_changes = [
      "tags.%",
      "tags.BILLINGCODE",
      "tags.BILLINGCONTACT",
      "tags.CLIENT",
      "tags.COUNTRY",
      "tags.CSCLASS",
      "tags.CSQUAL",
      "tags.CSTYPE",
      "tags.ENVIRONMENT",
      "tags.FUNCTION",
      "tags.GROUPCONTACT",
      "tags.MEMBERFIRM",
      "tags.PRIMARYCONTACT",
      "tags.SECONDARYCONTACT",
      "volume_tags.%",
      "volume_tags.AccountID",
      "volume_tags.ApplicationName",
      "volume_tags.BILLINGCODE",
      "volume_tags.BILLINGCONTACT",
      "volume_tags.CLIENT",
      "volume_tags.COUNTRY",
      "volume_tags.CSCLASS",
      "volume_tags.CSQUAL",
      "volume_tags.CSTYPE",
      "volume_tags.DataClassification",
      "volume_tags.Docker",
      "volume_tags.ENVIRONMENT",
      "volume_tags.Environment",
      "volume_tags.FUNCTION",
      "volume_tags.GROUPCONTACT",
      "volume_tags.MEMBERFIRM",
      "volume_tags.ManagedBy",
      "volume_tags.Name",
      "volume_tags.Owner",
      "volume_tags.PRIMARYCONTACT",
      "volume_tags.PatchGroup",
      "volume_tags.ResourceSeverity",
      "volume_tags.ResourceType",
      "volume_tags.Role",
      "volume_tags.SECONDARYCONTACT",
      "volume_tags.SolutionName",
      "volume_tags.VendorManaged"
    ]
  }

  volume_tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "PROJECT"
  }

  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "PROJECT"
    PatchGroup = "AmazonLinux2-AzB-Group2"
    Owner = "robhatnagar@deloitte.ca"
    ManagedBy = "CMS-TF"
    SolutionName = "InsurCloud Economical"
    Environment = "Dev"
    ApplicationName = "docker, microservices"
    DataClassification = "PII, PHI"
    DLM-Backup = "Yes"
    DLM-Retention = "90"
    VendorManaged = "False"
    ResourceType = "Client"
    ResourceSeverity = "Normal"
    Name = "A-MC-CAC-D-A003-ECONOMICAL-PRF01-MS-MANAGER02-LNX-EC2"
    Role = "Container"
  }

  provisioner "local-exec" {
    command = "echo ${aws_instance.ICECONOMICAL_PRF01_MICROSERVICES_MANAGER02.private_ip} >> dev_hosts.txt"
  }
}

output "ICECONOMICAL_PRF01_MICROSERVICES_MANAGER02_PRIVATEIP" {
  value = "${aws_instance.ICECONOMICAL_PRF01_MICROSERVICES_MANAGER02.private_ip}"
}

resource "aws_instance" "ICECONOMICAL_PRF01_MICROSERVICES_MANAGER03" {
  ami = "${var.ami_id_amazonlinuxbase20200506}"
  vpc_security_group_ids = [
    "${aws_security_group.SG_MICROSERVICES.id}",
    "${aws_security_group.SG_MONITORING.id}",
    "${aws_security_group.SG_AVIATRIX_SSH.id}",
    "${aws_security_group.SG_JENKINS_CLIENT_SSH.id}"
  ]
  subnet_id = "${var.economical-dev-private-1b-subnet}"
  instance_type = "t3a.medium"
  key_name = "EconomicalMicroservicesNPRD"
  associate_public_ip_address = false
  iam_instance_profile = "CMS-SSMInstanceRole"
  ebs_optimized = "true"

  root_block_device {
    volume_size = "16"
  }

  lifecycle {
    prevent_destroy = false
    ignore_changes = [
      "tags.%",
      "tags.BILLINGCODE",
      "tags.BILLINGCONTACT",
      "tags.CLIENT",
      "tags.COUNTRY",
      "tags.CSCLASS",
      "tags.CSQUAL",
      "tags.CSTYPE",
      "tags.ENVIRONMENT",
      "tags.FUNCTION",
      "tags.GROUPCONTACT",
      "tags.MEMBERFIRM",
      "tags.PRIMARYCONTACT",
      "tags.SECONDARYCONTACT",
      "volume_tags.%",
      "volume_tags.AccountID",
      "volume_tags.ApplicationName",
      "volume_tags.BILLINGCODE",
      "volume_tags.BILLINGCONTACT",
      "volume_tags.CLIENT",
      "volume_tags.COUNTRY",
      "volume_tags.CSCLASS",
      "volume_tags.CSQUAL",
      "volume_tags.CSTYPE",
      "volume_tags.DataClassification",
      "volume_tags.Docker",
      "volume_tags.ENVIRONMENT",
      "volume_tags.Environment",
      "volume_tags.FUNCTION",
      "volume_tags.GROUPCONTACT",
      "volume_tags.MEMBERFIRM",
      "volume_tags.ManagedBy",
      "volume_tags.Name",
      "volume_tags.Owner",
      "volume_tags.PRIMARYCONTACT",
      "volume_tags.PatchGroup",
      "volume_tags.ResourceSeverity",
      "volume_tags.ResourceType",
      "volume_tags.Role",
      "volume_tags.SECONDARYCONTACT",
      "volume_tags.SolutionName",
      "volume_tags.VendorManaged"
    ]
  }

  volume_tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "PROJECT"
  }

  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "PROJECT"
    PatchGroup = "AmazonLinux2-AzB-Group3"
    Owner = "robhatnagar@deloitte.ca"
    ManagedBy = "CMS-TF"
    SolutionName = "InsurCloud Economical"
    Environment = "Dev"
    ApplicationName = "docker, microservices"
    DataClassification = "PII, PHI"
    DLM-Backup = "Yes"
    DLM-Retention = "90"
    VendorManaged = "False"
    ResourceType = "Client"
    ResourceSeverity = "Normal"
    Name = "A-MC-CAC-D-A003-ECONOMICAL-PRF01-MS-MANAGER03-LNX-EC2"
    Role = "Container"
  }

  provisioner "local-exec" {
    command = "echo ${aws_instance.ICECONOMICAL_PRF01_MICROSERVICES_MANAGER03.private_ip} >> dev_hosts.txt"
  }
}

output "ICECONOMICAL_PRF01_MICROSERVICES_MANAGER03_PRIVATEIP" {
  value = "${aws_instance.ICECONOMICAL_PRF01_MICROSERVICES_MANAGER03.private_ip}"
}

resource "aws_instance" "ICECONOMICAL_PRF01_MICROSERVICES_APPLICATION01" {
  ami = "${var.ami_id_amazonlinuxbase20200506}"
  vpc_security_group_ids = [
    "${aws_security_group.SG_MICROSERVICES.id}",
    "${aws_security_group.SG_MICROSERVICES_DEV.id}",
    "${aws_security_group.SG_MONITORING.id}",
    "${aws_security_group.SG_AVIATRIX_SSH.id}",
    "${aws_security_group.SG_JENKINS_CLIENT_SSH.id}"
  ]
  subnet_id = "${var.economical-dev-private-1a-subnet}"
  instance_type = "t3a.xlarge"
  key_name = "EconomicalMicroservicesNPRD"
  associate_public_ip_address = false
  iam_instance_profile = "CMS-SSMInstanceRole"
  ebs_optimized = "true"

  root_block_device {
    volume_size = "100"
  }

  lifecycle {
    prevent_destroy = false
    ignore_changes = [
      "tags.%",
      "tags.BILLINGCODE",
      "tags.BILLINGCONTACT",
      "tags.CLIENT",
      "tags.COUNTRY",
      "tags.CSCLASS",
      "tags.CSQUAL",
      "tags.CSTYPE",
      "tags.ENVIRONMENT",
      "tags.FUNCTION",
      "tags.GROUPCONTACT",
      "tags.MEMBERFIRM",
      "tags.PRIMARYCONTACT",
      "tags.SECONDARYCONTACT",
      "volume_tags.%",
      "volume_tags.AccountID",
      "volume_tags.ApplicationName",
      "volume_tags.BILLINGCODE",
      "volume_tags.BILLINGCONTACT",
      "volume_tags.CLIENT",
      "volume_tags.COUNTRY",
      "volume_tags.CSCLASS",
      "volume_tags.CSQUAL",
      "volume_tags.CSTYPE",
      "volume_tags.DataClassification",
      "volume_tags.Docker",
      "volume_tags.ENVIRONMENT",
      "volume_tags.Environment",
      "volume_tags.FUNCTION",
      "volume_tags.GROUPCONTACT",
      "volume_tags.MEMBERFIRM",
      "volume_tags.ManagedBy",
      "volume_tags.Name",
      "volume_tags.Owner",
      "volume_tags.PRIMARYCONTACT",
      "volume_tags.PatchGroup",
      "volume_tags.ResourceSeverity",
      "volume_tags.ResourceType",
      "volume_tags.Role",
      "volume_tags.SECONDARYCONTACT",
      "volume_tags.SolutionName",
      "volume_tags.VendorManaged"
    ]
  }

  volume_tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "PROJECT"
  }

  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "PROJECT"
    PatchGroup = "AmazonLinux2-AzA-Group1"
    Owner = "robhatnagar@deloitte.ca"
    ManagedBy = "CMS-TF"
    SolutionName = "InsurCloud Economical"
    Environment = "Dev"
    ApplicationName = "docker, microservices"
    DataClassification = "PII, PHI"
    DLM-Backup = "Yes"
    DLM-Retention = "90"
    VendorManaged = "False"
    ResourceType = "Client"
    ResourceSeverity = "Normal"
    Name = "A-MC-CAC-D-A003-ECONOMICAL-PRF01-MS-APP01-LNX-EC2"
    Role = "Container"
  }

  provisioner "local-exec" {
    command = "echo ${aws_instance.ICECONOMICAL_PRF01_MICROSERVICES_APPLICATION01.private_ip} >> dev_hosts.txt"
  }
}

output "ICECONOMICAL_PRF01_MICROSERVICES_APPLICATION01_PRIVATEIP" {
  value = "${aws_instance.ICECONOMICAL_PRF01_MICROSERVICES_APPLICATION01.private_ip}"
}

resource "aws_instance" "ICECONOMICAL_PRF01_MICROSERVICES_APPLICATION02" {
  ami = "${var.ami_id_amazonlinuxbase20200506}"
  vpc_security_group_ids = [
    "${aws_security_group.SG_MICROSERVICES.id}",
    "${aws_security_group.SG_MICROSERVICES_DEV.id}",
    "${aws_security_group.SG_MONITORING.id}",
    "${aws_security_group.SG_AVIATRIX_SSH.id}",
    "${aws_security_group.SG_JENKINS_CLIENT_SSH.id}"
  ]
  subnet_id = "${var.economical-dev-private-1b-subnet}"
  instance_type = "t3a.xlarge"
  key_name = "EconomicalMicroservicesNPRD"
  associate_public_ip_address = false
  iam_instance_profile = "CMS-SSMInstanceRole"
  ebs_optimized = "true"

  root_block_device {
    volume_size = "100"
  }

  lifecycle {
    prevent_destroy = false
    ignore_changes = [
      "tags.%",
      "tags.BILLINGCODE",
      "tags.BILLINGCONTACT",
      "tags.CLIENT",
      "tags.COUNTRY",
      "tags.CSCLASS",
      "tags.CSQUAL",
      "tags.CSTYPE",
      "tags.ENVIRONMENT",
      "tags.FUNCTION",
      "tags.GROUPCONTACT",
      "tags.MEMBERFIRM",
      "tags.PRIMARYCONTACT",
      "tags.SECONDARYCONTACT",
      "volume_tags.%",
      "volume_tags.AccountID",
      "volume_tags.ApplicationName",
      "volume_tags.BILLINGCODE",
      "volume_tags.BILLINGCONTACT",
      "volume_tags.CLIENT",
      "volume_tags.COUNTRY",
      "volume_tags.CSCLASS",
      "volume_tags.CSQUAL",
      "volume_tags.CSTYPE",
      "volume_tags.DataClassification",
      "volume_tags.Docker",
      "volume_tags.ENVIRONMENT",
      "volume_tags.Environment",
      "volume_tags.FUNCTION",
      "volume_tags.GROUPCONTACT",
      "volume_tags.MEMBERFIRM",
      "volume_tags.ManagedBy",
      "volume_tags.Name",
      "volume_tags.Owner",
      "volume_tags.PRIMARYCONTACT",
      "volume_tags.PatchGroup",
      "volume_tags.ResourceSeverity",
      "volume_tags.ResourceType",
      "volume_tags.Role",
      "volume_tags.SECONDARYCONTACT",
      "volume_tags.SolutionName",
      "volume_tags.VendorManaged"
    ]
  }

  volume_tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "PROJECT"
  }

  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "PROJECT"
    PatchGroup = "AmazonLinux2-AzB-Group1"
    Owner = "robhatnagar@deloitte.ca"
    ManagedBy = "CMS-TF"
    SolutionName = "InsurCloud Economical"
    Environment = "Dev"
    ApplicationName = "docker, microservices"
    DataClassification = "PII, PHI"
    DLM-Backup = "Yes"
    DLM-Retention = "90"
    VendorManaged = "False"
    ResourceType = "Client"
    ResourceSeverity = "Normal"
    Name = "A-MC-CAC-D-A003-ECONOMICAL-PRF01-MS-APP02-LNX-EC2"
    Role = "Container"
  }

  provisioner "local-exec" {
    command = "echo ${aws_instance.ICECONOMICAL_PRF01_MICROSERVICES_APPLICATION02.private_ip} >> dev_hosts.txt"
  }
}

output "ICECONOMICAL_PRF01_MICROSERVICES_APPLICATION02_PRIVATEIP" {
  value = "${aws_instance.ICECONOMICAL_PRF01_MICROSERVICES_APPLICATION02.private_ip}"
}

######### RPA ###########
data "template_file" "ECO_PRF01_DOMAIN_JOIN_1" {
  template = "${file("IC-Economical-DEV/windowsdata.tpl")}"

  vars = {
    computer_name = "PRF01-RPA-APP"
    region = "${var.region}"
    domain = "${var.ad_domain}"
    dns1 = "${var.ad_dns1}"
    dns2 = "${var.ad_dns2}"
    dns3 = "${var.ad_dns3}"
    ouselection = "${var.ou_name}"
    pwordparameter = "${var.pwordparameter}"
    unameparameter = "${var.unameparameter}"
  }
}

resource "aws_instance" "ICECONOMICAL_PRF01_RPA_APP" {
  ami = "${var.ami_id_windows2016base}"
  vpc_security_group_ids = [
    "${aws_security_group.SG_AVIATRIX_RDP.id}",
    "${aws_security_group.SG_RPA_APP.id}",
    "${aws_security_group.SG_RPA_APP_DEV.id}"
  ]
  subnet_id = "${var.economical-dev-private-1a-subnet}"
  instance_type = "t3a.large"
  key_name = "EconomicalRPANPRD"
  associate_public_ip_address = false
  iam_instance_profile = "CMS-SSMInstanceRole"
  ebs_optimized = "true"

  root_block_device {
    volume_size = "50"
  }

  user_data = "${data.template_file.ECO_PRF01_DOMAIN_JOIN_1.rendered}"

  lifecycle {
    prevent_destroy = false
    ignore_changes = [
      "tags.%",
      "tags.BILLINGCODE",
      "tags.BILLINGCONTACT",
      "tags.CLIENT",
      "tags.COUNTRY",
      "tags.CSCLASS",
      "tags.CSQUAL",
      "tags.CSTYPE",
      "tags.ENVIRONMENT",
      "tags.FUNCTION",
      "tags.GROUPCONTACT",
      "tags.MEMBERFIRM",
      "tags.PRIMARYCONTACT",
      "tags.SECONDARYCONTACT",
      "volume_tags.%",
      "volume_tags.AccountID",
      "volume_tags.ApplicationName",
      "volume_tags.BILLINGCODE",
      "volume_tags.BILLINGCONTACT",
      "volume_tags.CLIENT",
      "volume_tags.COUNTRY",
      "volume_tags.CSCLASS",
      "volume_tags.CSQUAL",
      "volume_tags.CSTYPE",
      "volume_tags.DataClassification",
      "volume_tags.Docker",
      "volume_tags.ENVIRONMENT",
      "volume_tags.Environment",
      "volume_tags.FUNCTION",
      "volume_tags.GROUPCONTACT",
      "volume_tags.MEMBERFIRM",
      "volume_tags.ManagedBy",
      "volume_tags.Name",
      "volume_tags.Owner",
      "volume_tags.PRIMARYCONTACT",
      "volume_tags.PatchGroup",
      "volume_tags.ResourceSeverity",
      "volume_tags.ResourceType",
      "volume_tags.Role",
      "volume_tags.SECONDARYCONTACT",
      "volume_tags.SolutionName",
      "volume_tags.VendorManaged"
    ]
  }

  volume_tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "PROJECT"
  }

  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "PROJECT"
    PatchGroup = "Windows-AzA-Group1"
    Owner = "robhatnagar@deloitte.ca"
    ManagedBy = "CMS-TF"
    SolutionName = "InsurCloud Economical"
    Environment = "Dev"
    ApplicationName = "rpa, blueprism"
    DataClassification = "PII, PHI"
    DLM-Backup = "Yes"
    DLM-Retention = "90"
    VendorManaged = "False"
    ResourceType = "Client"
    ResourceSeverity = "Normal"
    Name = "A-MC-CAC-D-A003-ECONOMICAL-PRF01-RPA-APP-WIN-EC2"
  }
}

output "ICECONOMICAL_PRF01_RPA_APP_PRIVATEIP" {
  value = "${aws_instance.ICECONOMICAL_PRF01_RPA_APP.private_ip}"
}

data "template_file" "ECO_PRF01_DOMAIN_JOIN_INT01" {
  template = "${file("IC-Economical-DEV/windowsdata.tpl")}"

  vars = {
    computer_name = "PRF01-RPA-INT"
    region = "${var.region}"
    domain = "${var.ad_domain}"
    dns1 = "${var.ad_dns1}"
    dns2 = "${var.ad_dns2}"
    dns3 = "${var.ad_dns3}"
    ouselection = "${var.ou_name}"
    pwordparameter = "${var.pwordparameter}"
    unameparameter = "${var.unameparameter}"
  }
}

resource "aws_instance" "ICECONOMICAL_PRF01_RPA_INT01" {
  ami = "${var.ami_id_windows2016base}"
  vpc_security_group_ids = [
    "${aws_security_group.SG_AVIATRIX_RDP.id}",
    "${aws_security_group.SG_RPA_APP.id}",
    "${aws_security_group.SG_RPA_APP_DEV.id}"
  ]
  subnet_id = "${var.economical-dev-private-1a-subnet}"
  instance_type = "t3a.medium"
  key_name = "EconomicalRPANPRD"
  associate_public_ip_address = false
  iam_instance_profile = "CMS-SSMInstanceRole"
  ebs_optimized = "true"

  root_block_device {
    volume_size = "50"
  }

  user_data = "${data.template_file.ECO_PRF01_DOMAIN_JOIN_INT01.rendered}"

  lifecycle {
    prevent_destroy = false
    ignore_changes = [
      "tags.%",
      "tags.BILLINGCODE",
      "tags.BILLINGCONTACT",
      "tags.CLIENT",
      "tags.COUNTRY",
      "tags.CSCLASS",
      "tags.CSQUAL",
      "tags.CSTYPE",
      "tags.ENVIRONMENT",
      "tags.FUNCTION",
      "tags.GROUPCONTACT",
      "tags.MEMBERFIRM",
      "tags.PRIMARYCONTACT",
      "tags.SECONDARYCONTACT",
      "volume_tags.%",
      "volume_tags.AccountID",
      "volume_tags.ApplicationName",
      "volume_tags.BILLINGCODE",
      "volume_tags.BILLINGCONTACT",
      "volume_tags.CLIENT",
      "volume_tags.COUNTRY",
      "volume_tags.CSCLASS",
      "volume_tags.CSQUAL",
      "volume_tags.CSTYPE",
      "volume_tags.DataClassification",
      "volume_tags.Docker",
      "volume_tags.ENVIRONMENT",
      "volume_tags.Environment",
      "volume_tags.FUNCTION",
      "volume_tags.GROUPCONTACT",
      "volume_tags.MEMBERFIRM",
      "volume_tags.ManagedBy",
      "volume_tags.Name",
      "volume_tags.Owner",
      "volume_tags.PRIMARYCONTACT",
      "volume_tags.PatchGroup",
      "volume_tags.ResourceSeverity",
      "volume_tags.ResourceType",
      "volume_tags.Role",
      "volume_tags.SECONDARYCONTACT",
      "volume_tags.SolutionName",
      "volume_tags.VendorManaged"
    ]
  }

  volume_tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "PROJECT"
  }

  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "PROJECT"
    PatchGroup = "Windows-AzA-Group1"
    Owner = "robhatnagar@deloitte.ca"
    ManagedBy = "CMS-TF"
    SolutionName = "InsurCloud Economical"
    Environment = "Dev"
    ApplicationName = "rpa, blueprism"
    DataClassification = "PII, PHI"
    DLM-Backup = "Yes"
    DLM-Retention = "90"
    VendorManaged = "False"
    ResourceType = "Client"
    ResourceSeverity = "Normal"
    Name = "A-MC-CAC-D-A003-ECONOMICAL-PRF01-RPA-INT01-WIN-EC2"
  }
}

output "ICECONOMICAL_PRF01_RPA_INT01_PRIVATEIP" {
  value = "${aws_instance.ICECONOMICAL_PRF01_RPA_INT01.private_ip}"
}

data "template_file" "ECO_PRF01_DOMAIN_JOIN_2" {
  template = "${file("IC-Economical-DEV/windowsdata.tpl")}"

  vars = {
    computer_name = "PRF01-RPA-C01"
    region = "${var.region}"
    domain = "${var.ad_domain}"
    dns1 = "${var.ad_dns1}"
    dns2 = "${var.ad_dns2}"
    dns3 = "${var.ad_dns3}"
    ouselection = "${var.ou_name}"
    pwordparameter = "${var.pwordparameter}"
    unameparameter = "${var.unameparameter}"
  }
}

resource "aws_instance" "ICECONOMICAL_PRF01_RPA_CLIENT01" {
  ami = "${var.ami_id_windows2016base}"
  vpc_security_group_ids = [
    "${aws_security_group.SG_AVIATRIX_RDP.id}",
    "${aws_security_group.SG_RPA_CLIENT.id}",
    "${aws_security_group.SG_RPA_CLIENT_DEV.id}"
  ]
  subnet_id = "${var.economical-dev-private-1a-subnet}"
  instance_type = "t3a.medium"
  key_name = "EconomicalRPANPRD"
  associate_public_ip_address = false
  iam_instance_profile = "CMS-SSMInstanceRole"
  ebs_optimized = "true"

  root_block_device {
    volume_size = "50"
  }

  user_data = "${data.template_file.ECO_PRF01_DOMAIN_JOIN_2.rendered}"

  lifecycle {
    prevent_destroy = false
    ignore_changes = [
      "tags.%",
      "tags.BILLINGCODE",
      "tags.BILLINGCONTACT",
      "tags.CLIENT",
      "tags.COUNTRY",
      "tags.CSCLASS",
      "tags.CSQUAL",
      "tags.CSTYPE",
      "tags.ENVIRONMENT",
      "tags.FUNCTION",
      "tags.GROUPCONTACT",
      "tags.MEMBERFIRM",
      "tags.PRIMARYCONTACT",
      "tags.SECONDARYCONTACT",
      "volume_tags.%",
      "volume_tags.AccountID",
      "volume_tags.ApplicationName",
      "volume_tags.BILLINGCODE",
      "volume_tags.BILLINGCONTACT",
      "volume_tags.CLIENT",
      "volume_tags.COUNTRY",
      "volume_tags.CSCLASS",
      "volume_tags.CSQUAL",
      "volume_tags.CSTYPE",
      "volume_tags.DataClassification",
      "volume_tags.Docker",
      "volume_tags.ENVIRONMENT",
      "volume_tags.Environment",
      "volume_tags.FUNCTION",
      "volume_tags.GROUPCONTACT",
      "volume_tags.MEMBERFIRM",
      "volume_tags.ManagedBy",
      "volume_tags.Name",
      "volume_tags.Owner",
      "volume_tags.PRIMARYCONTACT",
      "volume_tags.PatchGroup",
      "volume_tags.ResourceSeverity",
      "volume_tags.ResourceType",
      "volume_tags.Role",
      "volume_tags.SECONDARYCONTACT",
      "volume_tags.SolutionName",
      "volume_tags.VendorManaged"
    ]
  }

  volume_tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "PROJECT"
  }

  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "PROJECT"
    PatchGroup = "Windows-AzA-Group1"
    Owner = "robhatnagar@deloitte.ca"
    ManagedBy = "CMS-TF"
    SolutionName = "InsurCloud Economical"
    Environment = "Dev"
    ApplicationName = "rpa, blueprism"
    DataClassification = "PII, PHI"
    DLM-Backup = "Yes"
    DLM-Retention = "90"
    VendorManaged = "False"
    ResourceType = "Client"
    ResourceSeverity = "Normal"
    Name = "A-MC-CAC-D-A003-ECONOMICAL-PRF01-RPA-CLIENT01-WIN-EC2"
  }
}

output "ICECONOMICAL_PRF01_RPA_CLIENT01_PRIVATEIP" {
  value = "${aws_instance.ICECONOMICAL_PRF01_RPA_CLIENT01.private_ip}"
}

data "template_file" "ECO_PRF01_DOMAIN_JOIN_3" {
  template = "${file("IC-Economical-DEV/windowsdata.tpl")}"

  vars = {
    computer_name = "PRF01-RPA-C02"
    region = "${var.region}"
    domain = "${var.ad_domain}"
    dns1 = "${var.ad_dns1}"
    dns2 = "${var.ad_dns2}"
    dns3 = "${var.ad_dns3}"
    ouselection = "${var.ou_name}"
    pwordparameter = "${var.pwordparameter}"
    unameparameter = "${var.unameparameter}"
  }
}

resource "aws_instance" "ICECONOMICAL_PRF01_RPA_CLIENT02" {
  ami = "${var.ami_id_windows2016base}"
  vpc_security_group_ids = [
    "${aws_security_group.SG_AVIATRIX_RDP.id}",
    "${aws_security_group.SG_RPA_CLIENT.id}",
    "${aws_security_group.SG_RPA_CLIENT_DEV.id}"
  ]
  subnet_id = "${var.economical-dev-private-1a-subnet}"
  instance_type = "t3a.medium"
  key_name = "EconomicalRPANPRD"
  associate_public_ip_address = false
  iam_instance_profile = "CMS-SSMInstanceRole"
  ebs_optimized = "true"

  root_block_device {
    volume_size = "50"
  }

  user_data = "${data.template_file.ECO_PRF01_DOMAIN_JOIN_3.rendered}"

  lifecycle {
    prevent_destroy = false
    ignore_changes = [
      "tags.%",
      "tags.BILLINGCODE",
      "tags.BILLINGCONTACT",
      "tags.CLIENT",
      "tags.COUNTRY",
      "tags.CSCLASS",
      "tags.CSQUAL",
      "tags.CSTYPE",
      "tags.ENVIRONMENT",
      "tags.FUNCTION",
      "tags.GROUPCONTACT",
      "tags.MEMBERFIRM",
      "tags.PRIMARYCONTACT",
      "tags.SECONDARYCONTACT",
      "volume_tags.%",
      "volume_tags.AccountID",
      "volume_tags.ApplicationName",
      "volume_tags.BILLINGCODE",
      "volume_tags.BILLINGCONTACT",
      "volume_tags.CLIENT",
      "volume_tags.COUNTRY",
      "volume_tags.CSCLASS",
      "volume_tags.CSQUAL",
      "volume_tags.CSTYPE",
      "volume_tags.DataClassification",
      "volume_tags.Docker",
      "volume_tags.ENVIRONMENT",
      "volume_tags.Environment",
      "volume_tags.FUNCTION",
      "volume_tags.GROUPCONTACT",
      "volume_tags.MEMBERFIRM",
      "volume_tags.ManagedBy",
      "volume_tags.Name",
      "volume_tags.Owner",
      "volume_tags.PRIMARYCONTACT",
      "volume_tags.PatchGroup",
      "volume_tags.ResourceSeverity",
      "volume_tags.ResourceType",
      "volume_tags.Role",
      "volume_tags.SECONDARYCONTACT",
      "volume_tags.SolutionName",
      "volume_tags.VendorManaged"
    ]
  }

  volume_tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "PROJECT"
  }

  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "PROJECT"
    PatchGroup = "Windows-AzB-Group1"
    Owner = "robhatnagar@deloitte.ca"
    ManagedBy = "CMS-TF"
    SolutionName = "InsurCloud Economical"
    Environment = "Dev"
    ApplicationName = "rpa, blueprism"
    DataClassification = "PII, PHI"
    DLM-Backup = "Yes"
    DLM-Retention = "90"
    VendorManaged = "False"
    ResourceType = "Client"
    ResourceSeverity = "Normal"
    Name = "A-MC-CAC-D-A003-ECONOMICAL-PRF01-RPA-CLIENT02-WIN-EC2"
  }
}

output "ICECONOMICAL_PRF01_RPA_CLIENT02_PRIVATEIP" {
  value = "${aws_instance.ICECONOMICAL_PRF01_RPA_CLIENT02.private_ip}"
}

data "template_file" "ECO_PRF01_DOMAIN_JOIN_4" {
  template = "${file("IC-Economical-DEV/windowsdata.tpl")}"

  vars = {
    computer_name = "PRF01-RPA-C03"
    region = "${var.region}"
    domain = "${var.ad_domain}"
    dns1 = "${var.ad_dns1}"
    dns2 = "${var.ad_dns2}"
    dns3 = "${var.ad_dns3}"
    ouselection = "${var.ou_name}"
    pwordparameter = "${var.pwordparameter}"
    unameparameter = "${var.unameparameter}"
  }
}

resource "aws_instance" "ICECONOMICAL_PRF01_RPA_CLIENT03" {
  ami = "${var.ami_id_windows2016base}"
  vpc_security_group_ids = [
    "${aws_security_group.SG_AVIATRIX_RDP.id}",
    "${aws_security_group.SG_RPA_CLIENT.id}",
    "${aws_security_group.SG_RPA_CLIENT_DEV.id}"
  ]
  subnet_id = "${var.economical-dev-private-1a-subnet}"
  instance_type = "t3a.medium"
  key_name = "EconomicalRPANPRD"
  associate_public_ip_address = false
  iam_instance_profile = "CMS-SSMInstanceRole"
  ebs_optimized = "true"

  root_block_device {
    volume_size = "50"
  }

  user_data = "${data.template_file.ECO_PRF01_DOMAIN_JOIN_4.rendered}"

  lifecycle {
    prevent_destroy = false
    ignore_changes = [
      "tags.%",
      "tags.BILLINGCODE",
      "tags.BILLINGCONTACT",
      "tags.CLIENT",
      "tags.COUNTRY",
      "tags.CSCLASS",
      "tags.CSQUAL",
      "tags.CSTYPE",
      "tags.ENVIRONMENT",
      "tags.FUNCTION",
      "tags.GROUPCONTACT",
      "tags.MEMBERFIRM",
      "tags.PRIMARYCONTACT",
      "tags.SECONDARYCONTACT",
      "volume_tags.%",
      "volume_tags.AccountID",
      "volume_tags.ApplicationName",
      "volume_tags.BILLINGCODE",
      "volume_tags.BILLINGCONTACT",
      "volume_tags.CLIENT",
      "volume_tags.COUNTRY",
      "volume_tags.CSCLASS",
      "volume_tags.CSQUAL",
      "volume_tags.CSTYPE",
      "volume_tags.DataClassification",
      "volume_tags.Docker",
      "volume_tags.ENVIRONMENT",
      "volume_tags.Environment",
      "volume_tags.FUNCTION",
      "volume_tags.GROUPCONTACT",
      "volume_tags.MEMBERFIRM",
      "volume_tags.ManagedBy",
      "volume_tags.Name",
      "volume_tags.Owner",
      "volume_tags.PRIMARYCONTACT",
      "volume_tags.PatchGroup",
      "volume_tags.ResourceSeverity",
      "volume_tags.ResourceType",
      "volume_tags.Role",
      "volume_tags.SECONDARYCONTACT",
      "volume_tags.SolutionName",
      "volume_tags.VendorManaged"
    ]
  }

  volume_tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "PROJECT"
  }

  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "PROJECT"
    PatchGroup = "Windows-AzA-Group1"
    Owner = "robhatnagar@deloitte.ca"
    ManagedBy = "CMS-TF"
    SolutionName = "InsurCloud Economical"
    Environment = "Dev"
    ApplicationName = "rpa, blueprism"
    DataClassification = "PII, PHI"
    DLM-Backup = "Yes"
    DLM-Retention = "90"
    VendorManaged = "False"
    ResourceType = "Client"
    ResourceSeverity = "Normal"
    Name = "A-MC-CAC-D-A003-ECONOMICAL-PRF01-RPA-CLIENT03-WIN-EC2"
  }
}

output "ICECONOMICAL_PRF01_RPA_CLIENT03_PRIVATEIP" {
  value = "${aws_instance.ICECONOMICAL_PRF01_RPA_CLIENT03.private_ip}"
}

data "template_file" "ECO_PRF01_RPA_CLIENT04_DOMAIN_JOIN" {
  template = "${file("IC-Economical-DEV/windowsdata.tpl")}"

  vars = {
    computer_name = "PRF01-RPA-C04"
    region = "${var.region}"
    domain = "${var.ad_domain}"
    dns1 = "${var.ad_dns1}"
    dns2 = "${var.ad_dns2}"
    dns3 = "${var.ad_dns3}"
    ouselection = "${var.ou_name}"
    pwordparameter = "${var.pwordparameter}"
    unameparameter = "${var.unameparameter}"
  }
}

resource "aws_instance" "ICECONOMICAL_PRF01_RPA_CLIENT04" {
  ami = "${var.ami_id_windows2016base}"
  vpc_security_group_ids = [
    "${aws_security_group.SG_AVIATRIX_RDP.id}",
    "${aws_security_group.SG_RPA_CLIENT.id}",
    "${aws_security_group.SG_RPA_CLIENT_DEV.id}"
  ]
  subnet_id = "${var.economical-dev-private-1b-subnet}"
  instance_type = "t3a.medium"
  key_name = "EconomicalRPANPRD"
  associate_public_ip_address = false
  iam_instance_profile = "CMS-SSMInstanceRole"
  ebs_optimized = "true"

  root_block_device {
    volume_size = "50"
  }

  user_data = "${data.template_file.ECO_PRF01_RPA_CLIENT04_DOMAIN_JOIN.rendered}"

  lifecycle {
    prevent_destroy = false
    ignore_changes = [
      "tags.%",
      "tags.BILLINGCODE",
      "tags.BILLINGCONTACT",
      "tags.CLIENT",
      "tags.COUNTRY",
      "tags.CSCLASS",
      "tags.CSQUAL",
      "tags.CSTYPE",
      "tags.ENVIRONMENT",
      "tags.FUNCTION",
      "tags.GROUPCONTACT",
      "tags.MEMBERFIRM",
      "tags.PRIMARYCONTACT",
      "tags.SECONDARYCONTACT",
      "volume_tags.%",
      "volume_tags.AccountID",
      "volume_tags.ApplicationName",
      "volume_tags.BILLINGCODE",
      "volume_tags.BILLINGCONTACT",
      "volume_tags.CLIENT",
      "volume_tags.COUNTRY",
      "volume_tags.CSCLASS",
      "volume_tags.CSQUAL",
      "volume_tags.CSTYPE",
      "volume_tags.DataClassification",
      "volume_tags.Docker",
      "volume_tags.ENVIRONMENT",
      "volume_tags.Environment",
      "volume_tags.FUNCTION",
      "volume_tags.GROUPCONTACT",
      "volume_tags.MEMBERFIRM",
      "volume_tags.ManagedBy",
      "volume_tags.Name",
      "volume_tags.Owner",
      "volume_tags.PRIMARYCONTACT",
      "volume_tags.PatchGroup",
      "volume_tags.ResourceSeverity",
      "volume_tags.ResourceType",
      "volume_tags.Role",
      "volume_tags.SECONDARYCONTACT",
      "volume_tags.SolutionName",
      "volume_tags.VendorManaged"
    ]
  }

  volume_tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "PROJECT"
  }

  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "PROJECT"
    PatchGroup = "Windows-AzB-Group1"
    Owner = "robhatnagar@deloitte.ca"
    ManagedBy = "CMS-TF"
    SolutionName = "InsurCloud Economical"
    Environment = "Dev"
    ApplicationName = "rpa, blueprism"
    DataClassification = "PII, PHI"
    DLM-Backup = "Yes"
    DLM-Retention = "90"
    VendorManaged = "False"
    ResourceType = "Client"
    ResourceSeverity = "Normal"
    Name = "A-MC-CAC-D-A003-ECONOMICAL-PRF01-RPA-CLIENT04-WIN-EC2"
  }
}

output "ICECONOMICAL_PRF01_RPA_CLIENT04_PRIVATEIP" {
  value = "${aws_instance.ICECONOMICAL_PRF01_RPA_CLIENT04.private_ip}"
}

data "template_file" "ECO_PRF01_RPA_CLIENT05_DOMAIN_JOIN" {
  template = "${file("IC-Economical-DEV/windowsdata.tpl")}"

  vars = {
    computer_name = "PRF01-RPA-C05"
    region = "${var.region}"
    domain = "${var.ad_domain}"
    dns1 = "${var.ad_dns1}"
    dns2 = "${var.ad_dns2}"
    dns3 = "${var.ad_dns3}"
    ouselection = "${var.ou_name}"
    pwordparameter = "${var.pwordparameter}"
    unameparameter = "${var.unameparameter}"
  }
}

resource "aws_instance" "ICECONOMICAL_PRF01_RPA_CLIENT05" {
  ami = "${var.ami_id_windows2016base}"
  vpc_security_group_ids = [
    "${aws_security_group.SG_AVIATRIX_RDP.id}",
    "${aws_security_group.SG_RPA_CLIENT.id}",
    "${aws_security_group.SG_RPA_CLIENT_DEV.id}"
  ]
  subnet_id = "${var.economical-dev-private-1b-subnet}"
  instance_type = "t3a.medium"
  key_name = "EconomicalRPANPRD"
  associate_public_ip_address = false
  iam_instance_profile = "CMS-SSMInstanceRole"
  ebs_optimized = "true"

  root_block_device {
    volume_size = "50"
  }

  user_data = "${data.template_file.ECO_PRF01_RPA_CLIENT05_DOMAIN_JOIN.rendered}"

  lifecycle {
    prevent_destroy = false
    ignore_changes = [
      "tags.%",
      "tags.BILLINGCODE",
      "tags.BILLINGCONTACT",
      "tags.CLIENT",
      "tags.COUNTRY",
      "tags.CSCLASS",
      "tags.CSQUAL",
      "tags.CSTYPE",
      "tags.ENVIRONMENT",
      "tags.FUNCTION",
      "tags.GROUPCONTACT",
      "tags.MEMBERFIRM",
      "tags.PRIMARYCONTACT",
      "tags.SECONDARYCONTACT",
      "volume_tags.%",
      "volume_tags.AccountID",
      "volume_tags.ApplicationName",
      "volume_tags.BILLINGCODE",
      "volume_tags.BILLINGCONTACT",
      "volume_tags.CLIENT",
      "volume_tags.COUNTRY",
      "volume_tags.CSCLASS",
      "volume_tags.CSQUAL",
      "volume_tags.CSTYPE",
      "volume_tags.DataClassification",
      "volume_tags.Docker",
      "volume_tags.ENVIRONMENT",
      "volume_tags.Environment",
      "volume_tags.FUNCTION",
      "volume_tags.GROUPCONTACT",
      "volume_tags.MEMBERFIRM",
      "volume_tags.ManagedBy",
      "volume_tags.Name",
      "volume_tags.Owner",
      "volume_tags.PRIMARYCONTACT",
      "volume_tags.PatchGroup",
      "volume_tags.ResourceSeverity",
      "volume_tags.ResourceType",
      "volume_tags.Role",
      "volume_tags.SECONDARYCONTACT",
      "volume_tags.SolutionName",
      "volume_tags.VendorManaged"
    ]
  }

  volume_tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "PROJECT"
  }

  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "PROJECT"
    PatchGroup = "Windows-AzA-Group1"
    Owner = "robhatnagar@deloitte.ca"
    ManagedBy = "CMS-TF"
    SolutionName = "InsurCloud Economical"
    Environment = "Dev"
    ApplicationName = "rpa, blueprism"
    DataClassification = "PII, PHI"
    DLM-Backup = "Yes"
    DLM-Retention = "90"
    VendorManaged = "False"
    ResourceType = "Client"
    ResourceSeverity = "Normal"
    Name = "A-MC-CAC-D-A003-ECONOMICAL-PRF01-RPA-CLIENT05-WIN-EC2"
  }
}

output "ICECONOMICAL_PRF01_RPA_CLIENT05_PRIVATEIP" {
  value = "${aws_instance.ICECONOMICAL_PRF01_RPA_CLIENT05.private_ip}"
}

data "template_file" "ECO_PRF01_RPA_CLIENT06_DOMAIN_JOIN" {
  template = "${file("IC-Economical-DEV/windowsdata.tpl")}"

  vars = {
    computer_name = "PRF01-RPA-C06"
    region = "${var.region}"
    domain = "${var.ad_domain}"
    dns1 = "${var.ad_dns1}"
    dns2 = "${var.ad_dns2}"
    dns3 = "${var.ad_dns3}"
    ouselection = "${var.ou_name}"
    pwordparameter = "${var.pwordparameter}"
    unameparameter = "${var.unameparameter}"
  }
}

resource "aws_instance" "ICECONOMICAL_PRF01_RPA_CLIENT06" {
  ami = "${var.ami_id_windows2016base}"
  vpc_security_group_ids = [
    "${aws_security_group.SG_AVIATRIX_RDP.id}",
    "${aws_security_group.SG_RPA_CLIENT.id}",
    "${aws_security_group.SG_RPA_CLIENT_DEV.id}"
  ]
  subnet_id = "${var.economical-dev-private-1b-subnet}"
  instance_type = "t3a.medium"
  key_name = "EconomicalRPANPRD"
  associate_public_ip_address = false
  iam_instance_profile = "CMS-SSMInstanceRole"
  ebs_optimized = "true"

  root_block_device {
    volume_size = "50"
  }

  user_data = "${data.template_file.ECO_PRF01_RPA_CLIENT06_DOMAIN_JOIN.rendered}"

  lifecycle {
    prevent_destroy = false
    ignore_changes = [
      "tags.%",
      "tags.BILLINGCODE",
      "tags.BILLINGCONTACT",
      "tags.CLIENT",
      "tags.COUNTRY",
      "tags.CSCLASS",
      "tags.CSQUAL",
      "tags.CSTYPE",
      "tags.ENVIRONMENT",
      "tags.FUNCTION",
      "tags.GROUPCONTACT",
      "tags.MEMBERFIRM",
      "tags.PRIMARYCONTACT",
      "tags.SECONDARYCONTACT",
      "volume_tags.%",
      "volume_tags.AccountID",
      "volume_tags.ApplicationName",
      "volume_tags.BILLINGCODE",
      "volume_tags.BILLINGCONTACT",
      "volume_tags.CLIENT",
      "volume_tags.COUNTRY",
      "volume_tags.CSCLASS",
      "volume_tags.CSQUAL",
      "volume_tags.CSTYPE",
      "volume_tags.DataClassification",
      "volume_tags.Docker",
      "volume_tags.ENVIRONMENT",
      "volume_tags.Environment",
      "volume_tags.FUNCTION",
      "volume_tags.GROUPCONTACT",
      "volume_tags.MEMBERFIRM",
      "volume_tags.ManagedBy",
      "volume_tags.Name",
      "volume_tags.Owner",
      "volume_tags.PRIMARYCONTACT",
      "volume_tags.PatchGroup",
      "volume_tags.ResourceSeverity",
      "volume_tags.ResourceType",
      "volume_tags.Role",
      "volume_tags.SECONDARYCONTACT",
      "volume_tags.SolutionName",
      "volume_tags.VendorManaged"
    ]
  }

  volume_tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "PROJECT"
  }

  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "PROJECT"
    PatchGroup = "Windows-AzB-Group1"
    Owner = "robhatnagar@deloitte.ca"
    ManagedBy = "CMS-TF"
    SolutionName = "InsurCloud Economical"
    Environment = "Dev"
    ApplicationName = "rpa, blueprism"
    DataClassification = "PII, PHI"
    DLM-Backup = "Yes"
    DLM-Retention = "90"
    VendorManaged = "False"
    ResourceType = "Client"
    ResourceSeverity = "Normal"
    Name = "A-MC-CAC-D-A003-ECONOMICAL-PRF01-RPA-CLIENT06-WIN-EC2"
  }
}

output "ICECONOMICAL_PRF01_RPA_CLIENT06_PRIVATEIP" {
  value = "${aws_instance.ICECONOMICAL_PRF01_RPA_CLIENT06.private_ip}"
}

data "template_file" "ECO_PRF01_RPA_CLIENT07_DOMAIN_JOIN" {
  template = "${file("IC-Economical-DEV/windowsdata.tpl")}"

  vars = {
    computer_name = "PRF01-RPA-C07"
    region = "${var.region}"
    domain = "${var.ad_domain}"
    dns1 = "${var.ad_dns1}"
    dns2 = "${var.ad_dns2}"
    dns3 = "${var.ad_dns3}"
    ouselection = "${var.ou_name}"
    pwordparameter = "${var.pwordparameter}"
    unameparameter = "${var.unameparameter}"
  }
}

######### HAPROXY ##############
resource "aws_instance" "ICECONOMICAL_PRF01_INTERNAL_HAPROXY01" {
  ami = "${var.ami_id_amazonlinuxbase20200506}"
  vpc_security_group_ids = [
    "${aws_security_group.SG_INTERNAL_HAPROXY_DEV.id}",
    "${aws_security_group.SG_AVIATRIX_SSH.id}",
    "${aws_security_group.SG_JENKINS_CLIENT_SSH.id}"
  ]
  subnet_id = "${var.economical-dev-private-1a-subnet}"
  instance_type = "t3a.medium"
  key_name = "EconomicalHAProxyNPRD"
  associate_public_ip_address = false
  iam_instance_profile = "CMS-SSMInstanceRole"
  ebs_optimized = "true"

  root_block_device {
    volume_size = "40"
  }

  lifecycle {
    prevent_destroy = false
    ignore_changes = [
      "tags.%",
      "tags.BILLINGCODE",
      "tags.BILLINGCONTACT",
      "tags.CLIENT",
      "tags.COUNTRY",
      "tags.CSCLASS",
      "tags.CSQUAL",
      "tags.CSTYPE",
      "tags.ENVIRONMENT",
      "tags.FUNCTION",
      "tags.GROUPCONTACT",
      "tags.MEMBERFIRM",
      "tags.PRIMARYCONTACT",
      "tags.SECONDARYCONTACT",
      "volume_tags.%",
      "volume_tags.AccountID",
      "volume_tags.ApplicationName",
      "volume_tags.BILLINGCODE",
      "volume_tags.BILLINGCONTACT",
      "volume_tags.CLIENT",
      "volume_tags.COUNTRY",
      "volume_tags.CSCLASS",
      "volume_tags.CSQUAL",
      "volume_tags.CSTYPE",
      "volume_tags.DataClassification",
      "volume_tags.Docker",
      "volume_tags.ENVIRONMENT",
      "volume_tags.Environment",
      "volume_tags.FUNCTION",
      "volume_tags.GROUPCONTACT",
      "volume_tags.MEMBERFIRM",
      "volume_tags.ManagedBy",
      "volume_tags.Name",
      "volume_tags.Owner",
      "volume_tags.PRIMARYCONTACT",
      "volume_tags.PatchGroup",
      "volume_tags.ResourceSeverity",
      "volume_tags.ResourceType",
      "volume_tags.Role",
      "volume_tags.SECONDARYCONTACT",
      "volume_tags.SolutionName",
      "volume_tags.VendorManaged"
    ]
  }

  volume_tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "PROJECT"
  }

  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "PROJECT"
    PatchGroup = "AmazonLinux2-AzA-Group1"
    Owner = "robhatnagar@deloitte.ca"
    ManagedBy = "CMS-TF"
    SolutionName = "InsurCloud Economical"
    Environment = "Dev"
    ApplicationName = "docker, haproxy"
    DataClassification = "PII, PHI"
    DLM-Backup = "Yes"
    DLM-Retention = "90"
    VendorManaged = "False"
    ResourceType = "Client"
    ResourceSeverity = "Normal"
    Name = "A-MC-CAC-D-A003-ECONOMICAL-PRF01-INTERNAL-HAPROXY01-EC2"
    Role = "Container"
  }

  provisioner "local-exec" {
    command = "echo ${aws_instance.ICECONOMICAL_PRF01_INTERNAL_HAPROXY01.private_ip} >> dev_hosts.txt"
  }
}

output "ICECONOMICAL_PRF01_INTERNAL_HAPROXY_PRIVATEIP01" {
  value = "${aws_instance.ICECONOMICAL_PRF01_INTERNAL_HAPROXY01.private_ip}"
}

resource "aws_instance" "ICECONOMICAL_PRF01_INTERNAL_HAPROXY02" {
  ami = "${var.ami_id_amazonlinuxbase20200506}"
  vpc_security_group_ids = [
    "${aws_security_group.SG_INTERNAL_HAPROXY_DEV.id}",
    "${aws_security_group.SG_AVIATRIX_SSH.id}",
    "${aws_security_group.SG_JENKINS_CLIENT_SSH.id}"
  ]
  subnet_id = "${var.economical-dev-private-1b-subnet}"
  instance_type = "t3a.medium"
  key_name = "EconomicalHAProxyNPRD"
  associate_public_ip_address = false
  iam_instance_profile = "CMS-SSMInstanceRole"
  ebs_optimized = "true"

  root_block_device {
    volume_size = "40"
  }

  lifecycle {
    prevent_destroy = false
    ignore_changes = [
      "tags.%",
      "tags.BILLINGCODE",
      "tags.BILLINGCONTACT",
      "tags.CLIENT",
      "tags.COUNTRY",
      "tags.CSCLASS",
      "tags.CSQUAL",
      "tags.CSTYPE",
      "tags.ENVIRONMENT",
      "tags.FUNCTION",
      "tags.GROUPCONTACT",
      "tags.MEMBERFIRM",
      "tags.PRIMARYCONTACT",
      "tags.SECONDARYCONTACT",
      "volume_tags.%",
      "volume_tags.AccountID",
      "volume_tags.ApplicationName",
      "volume_tags.BILLINGCODE",
      "volume_tags.BILLINGCONTACT",
      "volume_tags.CLIENT",
      "volume_tags.COUNTRY",
      "volume_tags.CSCLASS",
      "volume_tags.CSQUAL",
      "volume_tags.CSTYPE",
      "volume_tags.DataClassification",
      "volume_tags.Docker",
      "volume_tags.ENVIRONMENT",
      "volume_tags.Environment",
      "volume_tags.FUNCTION",
      "volume_tags.GROUPCONTACT",
      "volume_tags.MEMBERFIRM",
      "volume_tags.ManagedBy",
      "volume_tags.Name",
      "volume_tags.Owner",
      "volume_tags.PRIMARYCONTACT",
      "volume_tags.PatchGroup",
      "volume_tags.ResourceSeverity",
      "volume_tags.ResourceType",
      "volume_tags.Role",
      "volume_tags.SECONDARYCONTACT",
      "volume_tags.SolutionName",
      "volume_tags.VendorManaged"
    ]
  }

  volume_tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "PROJECT"
  }

  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "PROJECT"
    PatchGroup = "AmazonLinux2-AzB-Group1"
    Owner = "robhatnagar@deloitte.ca"
    ManagedBy = "CMS-TF"
    SolutionName = "InsurCloud Economical"
    Environment = "Dev"
    ApplicationName = "docker, haproxy"
    DataClassification = "PII, PHI"
    DLM-Backup = "Yes"
    DLM-Retention = "90"
    VendorManaged = "False"
    ResourceType = "Client"
    ResourceSeverity = "Normal"
    Name = "A-MC-CAC-D-A003-ECONOMICAL-PRF01-INTERNAL-HAPROXY02-EC2"
    Role = "Container"
  }

  provisioner "local-exec" {
    command = "echo ${aws_instance.ICECONOMICAL_PRF01_INTERNAL_HAPROXY02.private_ip} >> dev_hosts.txt"
  }
}

output "ICECONOMICAL_PRF01_INTERNAL_HAPROXY_PRIVATEIP02" {
  value = "${aws_instance.ICECONOMICAL_PRF01_INTERNAL_HAPROXY02.private_ip}"
}


# #=======================================================================
# # SIT01
# #=======================================================================

######### GUIDEWIRE ##############
resource "aws_instance" "ICECONOMICAL_SIT01_GUIDEWIRE01_SUITE" {
  ami = "${var.ami_id_amazonlinuxbase20200506}"
  vpc_security_group_ids = [
    "${aws_security_group.SG_GUIDEWIRE.id}",
    "${aws_security_group.SG_JENKINS_GUIDEWIRE_HEALTH_CHECK.id}",
    "${aws_security_group.SG_GUIDEWIRE_CLUSTER_NBIO_EVENT_QUEUE.id}",
    "${aws_security_group.SG_AVIATRIX_SSH.id}",
    "${aws_security_group.SG_JENKINS_CLIENT_SSH.id}"
  ]
  subnet_id = "${var.economical-dev-private-1a-subnet}"
  instance_type = "m5a.xlarge"
  key_name = "EconomicalGuidewireNPRD"
  associate_public_ip_address = false
  iam_instance_profile = "CMS-SSMInstanceRole"
  ebs_optimized = "true"

  root_block_device {
    volume_size = "100"
  }

  lifecycle {
    prevent_destroy = false
    ignore_changes = [
      "tags.%",
      "tags.BILLINGCODE",
      "tags.BILLINGCONTACT",
      "tags.CLIENT",
      "tags.COUNTRY",
      "tags.CSCLASS",
      "tags.CSQUAL",
      "tags.CSTYPE",
      "tags.ENVIRONMENT",
      "tags.FUNCTION",
      "tags.GROUPCONTACT",
      "tags.MEMBERFIRM",
      "tags.PRIMARYCONTACT",
      "tags.SECONDARYCONTACT",
      "volume_tags.%",
      "volume_tags.AccountID",
      "volume_tags.ApplicationName",
      "volume_tags.BILLINGCODE",
      "volume_tags.BILLINGCONTACT",
      "volume_tags.CLIENT",
      "volume_tags.COUNTRY",
      "volume_tags.CSCLASS",
      "volume_tags.CSQUAL",
      "volume_tags.CSTYPE",
      "volume_tags.DataClassification",
      "volume_tags.Docker",
      "volume_tags.ENVIRONMENT",
      "volume_tags.Environment",
      "volume_tags.FUNCTION",
      "volume_tags.GROUPCONTACT",
      "volume_tags.MEMBERFIRM",
      "volume_tags.ManagedBy",
      "volume_tags.Name",
      "volume_tags.Owner",
      "volume_tags.PRIMARYCONTACT",
      "volume_tags.PatchGroup",
      "volume_tags.ResourceSeverity",
      "volume_tags.ResourceType",
      "volume_tags.Role",
      "volume_tags.SECONDARYCONTACT",
      "volume_tags.SolutionName",
      "volume_tags.VendorManaged"
    ]
  }

  volume_tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "PROJECT"
  }

  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "PROJECT"
    PatchGroup = "AmazonLinux2-AzA-Group1"
    Owner = "robhatnagar@deloitte.ca"
    ManagedBy = "CMS-TF"
    SolutionName = "InsurCloud Economical"
    Environment = "Dev"
    ApplicationName = "guidewire, docker"
    DataClassification = "PII, PHI"
    DLM-Backup = "Yes"
    DLM-Retention = "90"
    VendorManaged = "False"
    ResourceType = "Client"
    ResourceSeverity = "Normal"
    Name = "A-MC-CAC-D-A003-ECONOMICAL-SIT01-GW01-SUITE-LNX-EC2"
    Role = "Container"
  }

  provisioner "local-exec" {
    command = "echo ${aws_instance.ICECONOMICAL_SIT01_GUIDEWIRE01_SUITE.private_ip} >> dev_hosts.txt"
  }
}

output "ICECONOMICAL_SIT01_GUIDEWIRE01_SUITE_PRIVATEIP" {
  value = "${aws_instance.ICECONOMICAL_SIT01_GUIDEWIRE01_SUITE.private_ip}"
}

resource "aws_instance" "ICECONOMICAL_SIT01_GUIDEWIRE02_SUITE" {
  ami = "${var.ami_id_amazonlinuxbase20200506}"
  vpc_security_group_ids = [
    "${aws_security_group.SG_GUIDEWIRE.id}",
    "${aws_security_group.SG_JENKINS_GUIDEWIRE_HEALTH_CHECK.id}",
    "${aws_security_group.SG_GUIDEWIRE_CLUSTER_NBIO_EVENT_QUEUE.id}",
    "${aws_security_group.SG_AVIATRIX_SSH.id}",
    "${aws_security_group.SG_JENKINS_CLIENT_SSH.id}"
  ]
  subnet_id = "${var.economical-dev-private-1b-subnet}"
  instance_type = "m5a.xlarge"
  key_name = "EconomicalGuidewireNPRD"
  associate_public_ip_address = false
  iam_instance_profile = "CMS-SSMInstanceRole"
  ebs_optimized = "true"

  root_block_device {
    volume_size = "100"
  }

  lifecycle {
    prevent_destroy = false
    ignore_changes = [
      "tags.%",
      "tags.BILLINGCODE",
      "tags.BILLINGCONTACT",
      "tags.CLIENT",
      "tags.COUNTRY",
      "tags.CSCLASS",
      "tags.CSQUAL",
      "tags.CSTYPE",
      "tags.ENVIRONMENT",
      "tags.FUNCTION",
      "tags.GROUPCONTACT",
      "tags.MEMBERFIRM",
      "tags.PRIMARYCONTACT",
      "tags.SECONDARYCONTACT",
      "volume_tags.%",
      "volume_tags.AccountID",
      "volume_tags.ApplicationName",
      "volume_tags.BILLINGCODE",
      "volume_tags.BILLINGCONTACT",
      "volume_tags.CLIENT",
      "volume_tags.COUNTRY",
      "volume_tags.CSCLASS",
      "volume_tags.CSQUAL",
      "volume_tags.CSTYPE",
      "volume_tags.DataClassification",
      "volume_tags.Docker",
      "volume_tags.ENVIRONMENT",
      "volume_tags.Environment",
      "volume_tags.FUNCTION",
      "volume_tags.GROUPCONTACT",
      "volume_tags.MEMBERFIRM",
      "volume_tags.ManagedBy",
      "volume_tags.Name",
      "volume_tags.Owner",
      "volume_tags.PRIMARYCONTACT",
      "volume_tags.PatchGroup",
      "volume_tags.ResourceSeverity",
      "volume_tags.ResourceType",
      "volume_tags.Role",
      "volume_tags.SECONDARYCONTACT",
      "volume_tags.SolutionName",
      "volume_tags.VendorManaged"
    ]
  }

  volume_tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "PROJECT"
  }

  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "PROJECT"
    PatchGroup = "AmazonLinux2-AzB-Group1"
    Owner = "robhatnagar@deloitte.ca"
    ManagedBy = "CMS-TF"
    SolutionName = "InsurCloud Economical"
    Environment = "Dev"
    ApplicationName = "guidewire, docker"
    DataClassification = "PII, PHI"
    DLM-Backup = "Yes"
    DLM-Retention = "90"
    VendorManaged = "False"
    ResourceType = "Client"
    ResourceSeverity = "Normal"
    Name = "A-MC-CAC-D-A003-ECONOMICAL-SIT01-GW02-SUITE-LNX-EC2"
    Role = "Container"
  }

  provisioner "local-exec" {
    command = "echo ${aws_instance.ICECONOMICAL_SIT01_GUIDEWIRE02_SUITE.private_ip} >> dev_hosts.txt"
  }
}

output "ICECONOMICAL_SIT01_GUIDEWIRE02_SUITE_PRIVATEIP" {
  value = "${aws_instance.ICECONOMICAL_SIT01_GUIDEWIRE02_SUITE.private_ip}"
}

resource "aws_instance" "ICECONOMICAL_SIT01_GUIDEWIRE03_SUITE" {
  ami = "${var.ami_id_amazonlinuxbase20200506}"
  vpc_security_group_ids = [
    "${aws_security_group.SG_GUIDEWIRE.id}",
    "${aws_security_group.SG_JENKINS_GUIDEWIRE_HEALTH_CHECK.id}",
    "${aws_security_group.SG_GUIDEWIRE_CLUSTER_NBIO_EVENT_QUEUE.id}",
    "${aws_security_group.SG_AVIATRIX_SSH.id}",
    "${aws_security_group.SG_JENKINS_CLIENT_SSH.id}"
  ]
  subnet_id = "${var.economical-dev-private-1a-subnet}"
  instance_type = "m5a.xlarge"
  key_name = "EconomicalGuidewireNPRD"
  associate_public_ip_address = false
  iam_instance_profile = "CMS-SSMInstanceRole"
  ebs_optimized = "true"

  root_block_device {
    volume_size = "100"
  }

  lifecycle {
    prevent_destroy = false
    ignore_changes = [
      "tags.%",
      "tags.BILLINGCODE",
      "tags.BILLINGCONTACT",
      "tags.CLIENT",
      "tags.COUNTRY",
      "tags.CSCLASS",
      "tags.CSQUAL",
      "tags.CSTYPE",
      "tags.ENVIRONMENT",
      "tags.FUNCTION",
      "tags.GROUPCONTACT",
      "tags.MEMBERFIRM",
      "tags.PRIMARYCONTACT",
      "tags.SECONDARYCONTACT",
      "volume_tags.%",
      "volume_tags.AccountID",
      "volume_tags.ApplicationName",
      "volume_tags.BILLINGCODE",
      "volume_tags.BILLINGCONTACT",
      "volume_tags.CLIENT",
      "volume_tags.COUNTRY",
      "volume_tags.CSCLASS",
      "volume_tags.CSQUAL",
      "volume_tags.CSTYPE",
      "volume_tags.DataClassification",
      "volume_tags.Docker",
      "volume_tags.ENVIRONMENT",
      "volume_tags.Environment",
      "volume_tags.FUNCTION",
      "volume_tags.GROUPCONTACT",
      "volume_tags.MEMBERFIRM",
      "volume_tags.ManagedBy",
      "volume_tags.Name",
      "volume_tags.Owner",
      "volume_tags.PRIMARYCONTACT",
      "volume_tags.PatchGroup",
      "volume_tags.ResourceSeverity",
      "volume_tags.ResourceType",
      "volume_tags.Role",
      "volume_tags.SECONDARYCONTACT",
      "volume_tags.SolutionName",
      "volume_tags.VendorManaged"
    ]
  }

  volume_tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "PROJECT"
  }

  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "PROJECT"
    PatchGroup = "AmazonLinux2-AzA-Group1"
    Owner = "robhatnagar@deloitte.ca"
    ManagedBy = "CMS-TF"
    SolutionName = "InsurCloud Economical"
    Environment = "Dev"
    ApplicationName = "guidewire, docker"
    DataClassification = "PII, PHI"
    DLM-Backup = "Yes"
    DLM-Retention = "90"
    VendorManaged = "False"
    ResourceType = "Client"
    ResourceSeverity = "Normal"
    Name = "A-MC-CAC-D-A003-ECONOMICAL-SIT01-GW03-SUITE-LNX-EC2"
    Role = "Container"
  }

  provisioner "local-exec" {
    command = "echo ${aws_instance.ICECONOMICAL_SIT01_GUIDEWIRE03_SUITE.private_ip} >> dev_hosts.txt"
  }
}

output "ICECONOMICAL_SIT01_GUIDEWIRE03_SUITE_PRIVATEIP" {
  value = "${aws_instance.ICECONOMICAL_SIT01_GUIDEWIRE03_SUITE.private_ip}"
}

resource "aws_instance" "ICECONOMICAL_SIT01_GUIDEWIRE04_SUITE" {
  ami = "${var.ami_id_amazonlinuxbase20200506}"
  vpc_security_group_ids = [
    "${aws_security_group.SG_GUIDEWIRE.id}",
    "${aws_security_group.SG_JENKINS_GUIDEWIRE_HEALTH_CHECK.id}",
    "${aws_security_group.SG_GUIDEWIRE_CLUSTER_NBIO_EVENT_QUEUE.id}",
    "${aws_security_group.SG_AVIATRIX_SSH.id}",
    "${aws_security_group.SG_JENKINS_CLIENT_SSH.id}"
  ]
  subnet_id = "${var.economical-dev-private-1b-subnet}"
  instance_type = "m5a.xlarge"
  key_name = "EconomicalGuidewireNPRD"
  associate_public_ip_address = false
  iam_instance_profile = "CMS-SSMInstanceRole"
  ebs_optimized = "true"

  root_block_device {
    volume_size = "100"
  }

  lifecycle {
    prevent_destroy = false
    ignore_changes = [
      "tags.%",
      "tags.BILLINGCODE",
      "tags.BILLINGCONTACT",
      "tags.CLIENT",
      "tags.COUNTRY",
      "tags.CSCLASS",
      "tags.CSQUAL",
      "tags.CSTYPE",
      "tags.ENVIRONMENT",
      "tags.FUNCTION",
      "tags.GROUPCONTACT",
      "tags.MEMBERFIRM",
      "tags.PRIMARYCONTACT",
      "tags.SECONDARYCONTACT",
      "volume_tags.%",
      "volume_tags.AccountID",
      "volume_tags.ApplicationName",
      "volume_tags.BILLINGCODE",
      "volume_tags.BILLINGCONTACT",
      "volume_tags.CLIENT",
      "volume_tags.COUNTRY",
      "volume_tags.CSCLASS",
      "volume_tags.CSQUAL",
      "volume_tags.CSTYPE",
      "volume_tags.DataClassification",
      "volume_tags.Docker",
      "volume_tags.ENVIRONMENT",
      "volume_tags.Environment",
      "volume_tags.FUNCTION",
      "volume_tags.GROUPCONTACT",
      "volume_tags.MEMBERFIRM",
      "volume_tags.ManagedBy",
      "volume_tags.Name",
      "volume_tags.Owner",
      "volume_tags.PRIMARYCONTACT",
      "volume_tags.PatchGroup",
      "volume_tags.ResourceSeverity",
      "volume_tags.ResourceType",
      "volume_tags.Role",
      "volume_tags.SECONDARYCONTACT",
      "volume_tags.SolutionName",
      "volume_tags.VendorManaged"
    ]
  }

  volume_tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "PROJECT"
  }

  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "PROJECT"
    PatchGroup = "AmazonLinux2-AzB-Group1"
    Owner = "robhatnagar@deloitte.ca"
    ManagedBy = "CMS-TF"
    SolutionName = "InsurCloud Economical"
    Environment = "Dev"
    ApplicationName = "guidewire, docker"
    DataClassification = "PII, PHI"
    DLM-Backup = "Yes"
    DLM-Retention = "90"
    VendorManaged = "False"
    ResourceType = "Client"
    ResourceSeverity = "Normal"
    Name = "A-MC-CAC-D-A003-ECONOMICAL-SIT01-GW04-SUITE-LNX-EC2"
    Role = "Container"
  }

  provisioner "local-exec" {
    command = "echo ${aws_instance.ICECONOMICAL_SIT01_GUIDEWIRE04_SUITE.private_ip} >> dev_hosts.txt"
  }
}

output "ICECONOMICAL_SIT01_GUIDEWIRE04_SUITE_PRIVATEIP" {
  value = "${aws_instance.ICECONOMICAL_SIT01_GUIDEWIRE04_SUITE.private_ip}"
}

data "template_file" "ECO_SIT01_DOMAIN_JOIN_GW_DB" {
  template = "${file("IC-Economical-DEV/windowsdata.tpl")}"

  vars = {
    computer_name = "SIT01-GW-DB-W"
    region = "${var.region}"
    domain = "${var.ad_domain}"
    dns1 = "${var.ad_dns1}"
    dns2 = "${var.ad_dns2}"
    dns3 = "${var.ad_dns3}"
    ouselection = "${var.ou_name}"
    pwordparameter = "${var.pwordparameter}"
    unameparameter = "${var.unameparameter}"
  }
}

resource "aws_instance" "ICECONOMICAL_SIT01_GUIDEWIRE_DB" {
  ami = "${var.ami_id_windows2019base}"
  vpc_security_group_ids = [
    "${aws_security_group.SG_AVIATRIX_RDP.id}",
    "${aws_security_group.SG_MSSQL_DB.id}"
  ]
  subnet_id = "${var.economical-dev-private-1b-subnet}"
  instance_type = "r5.large"
  key_name = "EconomicalGuidewireNPRD"
  associate_public_ip_address = false
  iam_instance_profile = "CMS-SSMInstanceRole"
  ebs_optimized = "true"

  root_block_device {
    volume_size = "50"
  }

  user_data = "${data.template_file.ECO_SIT01_DOMAIN_JOIN_GW_DB.rendered}"

  lifecycle {
    prevent_destroy = false
    ignore_changes = [
      "tags.%",
      "tags.BILLINGCODE",
      "tags.BILLINGCONTACT",
      "tags.CLIENT",
      "tags.COUNTRY",
      "tags.CSCLASS",
      "tags.CSQUAL",
      "tags.CSTYPE",
      "tags.ENVIRONMENT",
      "tags.FUNCTION",
      "tags.GROUPCONTACT",
      "tags.MEMBERFIRM",
      "tags.PRIMARYCONTACT",
      "tags.SECONDARYCONTACT",
      "volume_tags.%",
      "volume_tags.AccountID",
      "volume_tags.ApplicationName",
      "volume_tags.BILLINGCODE",
      "volume_tags.BILLINGCONTACT",
      "volume_tags.CLIENT",
      "volume_tags.COUNTRY",
      "volume_tags.CSCLASS",
      "volume_tags.CSQUAL",
      "volume_tags.CSTYPE",
      "volume_tags.DataClassification",
      "volume_tags.Docker",
      "volume_tags.ENVIRONMENT",
      "volume_tags.Environment",
      "volume_tags.FUNCTION",
      "volume_tags.GROUPCONTACT",
      "volume_tags.MEMBERFIRM",
      "volume_tags.ManagedBy",
      "volume_tags.Name",
      "volume_tags.Owner",
      "volume_tags.PRIMARYCONTACT",
      "volume_tags.PatchGroup",
      "volume_tags.ResourceSeverity",
      "volume_tags.ResourceType",
      "volume_tags.Role",
      "volume_tags.SECONDARYCONTACT",
      "volume_tags.SolutionName",
      "volume_tags.VendorManaged"
    ]
  }

  volume_tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "PROJECT"
  }

  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "PROJECT"
    PatchGroup = "Windows-AzB-Group1"
    Owner = "robhatnagar@deloitte.ca"
    ManagedBy = "CMS-TF"
    SolutionName = "InsurCloud Economical"
    Environment = "Dev"
    ApplicationName = "mssql, guidewire"
    DataClassification = "PII, PHI"
    DLM-Backup = "Yes"
    DLM-Retention = "90"
    VendorManaged = "False"
    ResourceType = "Client"
    ResourceSeverity = "Normal"
    Name = "A-MC-CAC-D-A003-ECONOMICAL-SIT01-GW-MSSQL-DB01-WIN-EC2"
  }
}

output "ICECONOMICAL_SIT01_GUIDEWIRE_DB_PRIVATEIP" {
  value = "${aws_instance.ICECONOMICAL_SIT01_GUIDEWIRE_DB.private_ip}"
}

######### MICROSERVICES ###########
resource "aws_instance" "ICECONOMICAL_SIT01_MICROSERVICES_MANAGER01" {
  ami = "${var.ami_id_amazonlinuxbase20200506}"
  vpc_security_group_ids = [
    "${aws_security_group.SG_MICROSERVICES.id}",
    "${aws_security_group.SG_MONITORING.id}",
    "${aws_security_group.SG_AVIATRIX_SSH.id}",
    "${aws_security_group.SG_JENKINS_CLIENT_SSH.id}"
  ]
  subnet_id = "${var.economical-dev-private-1a-subnet}"
  instance_type = "t3a.medium"
  key_name = "EconomicalMicroservicesNPRD"
  associate_public_ip_address = false
  iam_instance_profile = "CMS-SSMInstanceRole"
  ebs_optimized = "true"

  root_block_device {
    volume_size = "16"
  }

  lifecycle {
    prevent_destroy = false
    ignore_changes = [
      "tags.%",
      "tags.BILLINGCODE",
      "tags.BILLINGCONTACT",
      "tags.CLIENT",
      "tags.COUNTRY",
      "tags.CSCLASS",
      "tags.CSQUAL",
      "tags.CSTYPE",
      "tags.ENVIRONMENT",
      "tags.FUNCTION",
      "tags.GROUPCONTACT",
      "tags.MEMBERFIRM",
      "tags.PRIMARYCONTACT",
      "tags.SECONDARYCONTACT",
      "volume_tags.%",
      "volume_tags.AccountID",
      "volume_tags.ApplicationName",
      "volume_tags.BILLINGCODE",
      "volume_tags.BILLINGCONTACT",
      "volume_tags.CLIENT",
      "volume_tags.COUNTRY",
      "volume_tags.CSCLASS",
      "volume_tags.CSQUAL",
      "volume_tags.CSTYPE",
      "volume_tags.DataClassification",
      "volume_tags.Docker",
      "volume_tags.ENVIRONMENT",
      "volume_tags.Environment",
      "volume_tags.FUNCTION",
      "volume_tags.GROUPCONTACT",
      "volume_tags.MEMBERFIRM",
      "volume_tags.ManagedBy",
      "volume_tags.Name",
      "volume_tags.Owner",
      "volume_tags.PRIMARYCONTACT",
      "volume_tags.PatchGroup",
      "volume_tags.ResourceSeverity",
      "volume_tags.ResourceType",
      "volume_tags.Role",
      "volume_tags.SECONDARYCONTACT",
      "volume_tags.SolutionName",
      "volume_tags.VendorManaged"
    ]
  }

  volume_tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "PROJECT"
  }

  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "PROJECT"
    PatchGroup = "AmazonLinux2-AzA-Group2"
    Owner = "robhatnagar@deloitte.ca"
    ManagedBy = "CMS-TF"
    SolutionName = "InsurCloud Economical"
    Environment = "Dev"
    ApplicationName = "docker, microservices"
    DataClassification = "PII, PHI"
    DLM-Backup = "Yes"
    DLM-Retention = "90"
    VendorManaged = "False"
    ResourceType = "Client"
    ResourceSeverity = "Normal"
    Name = "A-MC-CAC-D-A003-ECONOMICAL-SIT01-MS-MANAGER01-LNX-EC2"
    Role = "Container"
  }

  provisioner "local-exec" {
    command = "echo ${aws_instance.ICECONOMICAL_SIT01_MICROSERVICES_MANAGER01.private_ip} >> dev_hosts.txt"
  }
}

output "ICECONOMICAL_SIT01_MICROSERVICES_MANAGER01_PRIVATEIP" {
  value = "${aws_instance.ICECONOMICAL_SIT01_MICROSERVICES_MANAGER01.private_ip}"
}

resource "aws_instance" "ICECONOMICAL_SIT01_MICROSERVICES_MANAGER02" {
  ami = "${var.ami_id_amazonlinuxbase20200506}"
  vpc_security_group_ids = [
    "${aws_security_group.SG_MICROSERVICES.id}",
    "${aws_security_group.SG_MONITORING.id}",
    "${aws_security_group.SG_AVIATRIX_SSH.id}",
    "${aws_security_group.SG_JENKINS_CLIENT_SSH.id}"
  ]
  subnet_id = "${var.economical-dev-private-1b-subnet}"
  instance_type = "t3a.medium"
  key_name = "EconomicalMicroservicesNPRD"
  associate_public_ip_address = false
  iam_instance_profile = "CMS-SSMInstanceRole"
  ebs_optimized = "true"

  root_block_device {
    volume_size = "16"
  }

  lifecycle {
    prevent_destroy = false
    ignore_changes = [
      "tags.%",
      "tags.BILLINGCODE",
      "tags.BILLINGCONTACT",
      "tags.CLIENT",
      "tags.COUNTRY",
      "tags.CSCLASS",
      "tags.CSQUAL",
      "tags.CSTYPE",
      "tags.ENVIRONMENT",
      "tags.FUNCTION",
      "tags.GROUPCONTACT",
      "tags.MEMBERFIRM",
      "tags.PRIMARYCONTACT",
      "tags.SECONDARYCONTACT",
      "volume_tags.%",
      "volume_tags.AccountID",
      "volume_tags.ApplicationName",
      "volume_tags.BILLINGCODE",
      "volume_tags.BILLINGCONTACT",
      "volume_tags.CLIENT",
      "volume_tags.COUNTRY",
      "volume_tags.CSCLASS",
      "volume_tags.CSQUAL",
      "volume_tags.CSTYPE",
      "volume_tags.DataClassification",
      "volume_tags.Docker",
      "volume_tags.ENVIRONMENT",
      "volume_tags.Environment",
      "volume_tags.FUNCTION",
      "volume_tags.GROUPCONTACT",
      "volume_tags.MEMBERFIRM",
      "volume_tags.ManagedBy",
      "volume_tags.Name",
      "volume_tags.Owner",
      "volume_tags.PRIMARYCONTACT",
      "volume_tags.PatchGroup",
      "volume_tags.ResourceSeverity",
      "volume_tags.ResourceType",
      "volume_tags.Role",
      "volume_tags.SECONDARYCONTACT",
      "volume_tags.SolutionName",
      "volume_tags.VendorManaged"
    ]
  }

  volume_tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "PROJECT"
  }

  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "PROJECT"
    PatchGroup = "AmazonLinux2-AzB-Group2"
    Owner = "robhatnagar@deloitte.ca"
    ManagedBy = "CMS-TF"
    SolutionName = "InsurCloud Economical"
    Environment = "Dev"
    ApplicationName = "docker, microservices"
    DataClassification = "PII, PHI"
    DLM-Backup = "Yes"
    DLM-Retention = "90"
    VendorManaged = "False"
    ResourceType = "Client"
    ResourceSeverity = "Normal"
    Name = "A-MC-CAC-D-A003-ECONOMICAL-SIT01-MS-MANAGER02-LNX-EC2"
    Role = "Container"
  }

  provisioner "local-exec" {
    command = "echo ${aws_instance.ICECONOMICAL_SIT01_MICROSERVICES_MANAGER02.private_ip} >> dev_hosts.txt"
  }
}

output "ICECONOMICAL_SIT01_MICROSERVICES_MANAGER02_PRIVATEIP" {
  value = "${aws_instance.ICECONOMICAL_SIT01_MICROSERVICES_MANAGER02.private_ip}"
}

resource "aws_instance" "ICECONOMICAL_SIT01_MICROSERVICES_MANAGER03" {
  ami = "${var.ami_id_amazonlinuxbase20200506}"
  vpc_security_group_ids = [
    "${aws_security_group.SG_MICROSERVICES.id}",
    "${aws_security_group.SG_MONITORING.id}",
    "${aws_security_group.SG_AVIATRIX_SSH.id}",
    "${aws_security_group.SG_JENKINS_CLIENT_SSH.id}"
  ]
  subnet_id = "${var.economical-dev-private-1b-subnet}"
  instance_type = "t3a.medium"
  key_name = "EconomicalMicroservicesNPRD"
  associate_public_ip_address = false
  iam_instance_profile = "CMS-SSMInstanceRole"
  ebs_optimized = "true"

  root_block_device {
    volume_size = "16"
  }

  lifecycle {
    prevent_destroy = false
    ignore_changes = [
      "tags.%",
      "tags.BILLINGCODE",
      "tags.BILLINGCONTACT",
      "tags.CLIENT",
      "tags.COUNTRY",
      "tags.CSCLASS",
      "tags.CSQUAL",
      "tags.CSTYPE",
      "tags.ENVIRONMENT",
      "tags.FUNCTION",
      "tags.GROUPCONTACT",
      "tags.MEMBERFIRM",
      "tags.PRIMARYCONTACT",
      "tags.SECONDARYCONTACT",
      "volume_tags.%",
      "volume_tags.AccountID",
      "volume_tags.ApplicationName",
      "volume_tags.BILLINGCODE",
      "volume_tags.BILLINGCONTACT",
      "volume_tags.CLIENT",
      "volume_tags.COUNTRY",
      "volume_tags.CSCLASS",
      "volume_tags.CSQUAL",
      "volume_tags.CSTYPE",
      "volume_tags.DataClassification",
      "volume_tags.Docker",
      "volume_tags.ENVIRONMENT",
      "volume_tags.Environment",
      "volume_tags.FUNCTION",
      "volume_tags.GROUPCONTACT",
      "volume_tags.MEMBERFIRM",
      "volume_tags.ManagedBy",
      "volume_tags.Name",
      "volume_tags.Owner",
      "volume_tags.PRIMARYCONTACT",
      "volume_tags.PatchGroup",
      "volume_tags.ResourceSeverity",
      "volume_tags.ResourceType",
      "volume_tags.Role",
      "volume_tags.SECONDARYCONTACT",
      "volume_tags.SolutionName",
      "volume_tags.VendorManaged"
    ]
  }

  volume_tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "PROJECT"
  }

  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "PROJECT"
    PatchGroup = "AmazonLinux2-AzB-Group3"
    Owner = "robhatnagar@deloitte.ca"
    ManagedBy = "CMS-TF"
    SolutionName = "InsurCloud Economical"
    Environment = "Dev"
    ApplicationName = "docker, microservices"
    DataClassification = "PII, PHI"
    DLM-Backup = "Yes"
    DLM-Retention = "90"
    VendorManaged = "False"
    ResourceType = "Client"
    ResourceSeverity = "Normal"
    Name = "A-MC-CAC-D-A003-ECONOMICAL-SIT01-MS-MANAGER03-LNX-EC2"
    Role = "Container"
  }

  provisioner "local-exec" {
    command = "echo ${aws_instance.ICECONOMICAL_SIT01_MICROSERVICES_MANAGER03.private_ip} >> dev_hosts.txt"
  }
}

output "ICECONOMICAL_SIT01_MICROSERVICES_MANAGER03_PRIVATEIP" {
  value = "${aws_instance.ICECONOMICAL_SIT01_MICROSERVICES_MANAGER03.private_ip}"
}

resource "aws_instance" "ICECONOMICAL_SIT01_MICROSERVICES_APPLICATION01" {
  ami = "${var.ami_id_amazonlinuxbase20200506}"
  vpc_security_group_ids = [
    "${aws_security_group.SG_MICROSERVICES.id}",
    "${aws_security_group.SG_MONITORING.id}",
    "${aws_security_group.SG_AVIATRIX_SSH.id}",
    "${aws_security_group.SG_JENKINS_CLIENT_SSH.id}"
  ]
  subnet_id = "${var.economical-dev-private-1a-subnet}"
  instance_type = "t3a.xlarge"
  key_name = "EconomicalMicroservicesNPRD"
  associate_public_ip_address = false
  iam_instance_profile = "CMS-SSMInstanceRole"
  ebs_optimized = "true"

  root_block_device {
    volume_size = "100"
  }

  lifecycle {
    prevent_destroy = false
    ignore_changes = [
      "tags.%",
      "tags.BILLINGCODE",
      "tags.BILLINGCONTACT",
      "tags.CLIENT",
      "tags.COUNTRY",
      "tags.CSCLASS",
      "tags.CSQUAL",
      "tags.CSTYPE",
      "tags.ENVIRONMENT",
      "tags.FUNCTION",
      "tags.GROUPCONTACT",
      "tags.MEMBERFIRM",
      "tags.PRIMARYCONTACT",
      "tags.SECONDARYCONTACT",
      "volume_tags.%",
      "volume_tags.AccountID",
      "volume_tags.ApplicationName",
      "volume_tags.BILLINGCODE",
      "volume_tags.BILLINGCONTACT",
      "volume_tags.CLIENT",
      "volume_tags.COUNTRY",
      "volume_tags.CSCLASS",
      "volume_tags.CSQUAL",
      "volume_tags.CSTYPE",
      "volume_tags.DataClassification",
      "volume_tags.Docker",
      "volume_tags.ENVIRONMENT",
      "volume_tags.Environment",
      "volume_tags.FUNCTION",
      "volume_tags.GROUPCONTACT",
      "volume_tags.MEMBERFIRM",
      "volume_tags.ManagedBy",
      "volume_tags.Name",
      "volume_tags.Owner",
      "volume_tags.PRIMARYCONTACT",
      "volume_tags.PatchGroup",
      "volume_tags.ResourceSeverity",
      "volume_tags.ResourceType",
      "volume_tags.Role",
      "volume_tags.SECONDARYCONTACT",
      "volume_tags.SolutionName",
      "volume_tags.VendorManaged"
    ]
  }

  volume_tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "PROJECT"
  }

  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "PROJECT"
    PatchGroup = "AmazonLinux2-AzA-Group1"
    Owner = "robhatnagar@deloitte.ca"
    ManagedBy = "CMS-TF"
    SolutionName = "InsurCloud Economical"
    Environment = "Dev"
    ApplicationName = "docker, microservices"
    DataClassification = "PII, PHI"
    DLM-Backup = "Yes"
    DLM-Retention = "90"
    VendorManaged = "False"
    ResourceType = "Client"
    ResourceSeverity = "Normal"
    Name = "A-MC-CAC-D-A003-ECONOMICAL-SIT01-MS-APP01-LNX-EC2"
    Role = "Container"
  }

  provisioner "local-exec" {
    command = "echo ${aws_instance.ICECONOMICAL_SIT01_MICROSERVICES_APPLICATION01.private_ip} >> dev_hosts.txt"
  }
}

output "ICECONOMICAL_SIT01_MICROSERVICES_APPLICATION01_PRIVATEIP" {
  value = "${aws_instance.ICECONOMICAL_SIT01_MICROSERVICES_APPLICATION01.private_ip}"
}

resource "aws_instance" "ICECONOMICAL_SIT01_MICROSERVICES_APPLICATION02" {
  ami = "${var.ami_id_amazonlinuxbase20200506}"
  vpc_security_group_ids = [
    "${aws_security_group.SG_MICROSERVICES.id}",
    "${aws_security_group.SG_MONITORING.id}",
    "${aws_security_group.SG_AVIATRIX_SSH.id}",
    "${aws_security_group.SG_JENKINS_CLIENT_SSH.id}"
  ]
  subnet_id = "${var.economical-dev-private-1b-subnet}"
  instance_type = "t3a.xlarge"
  key_name = "EconomicalMicroservicesNPRD"
  associate_public_ip_address = false
  iam_instance_profile = "CMS-SSMInstanceRole"
  ebs_optimized = "true"

  root_block_device {
    volume_size = "100"
  }

  lifecycle {
    prevent_destroy = false
    ignore_changes = [
      "tags.%",
      "tags.BILLINGCODE",
      "tags.BILLINGCONTACT",
      "tags.CLIENT",
      "tags.COUNTRY",
      "tags.CSCLASS",
      "tags.CSQUAL",
      "tags.CSTYPE",
      "tags.ENVIRONMENT",
      "tags.FUNCTION",
      "tags.GROUPCONTACT",
      "tags.MEMBERFIRM",
      "tags.PRIMARYCONTACT",
      "tags.SECONDARYCONTACT",
      "volume_tags.%",
      "volume_tags.AccountID",
      "volume_tags.ApplicationName",
      "volume_tags.BILLINGCODE",
      "volume_tags.BILLINGCONTACT",
      "volume_tags.CLIENT",
      "volume_tags.COUNTRY",
      "volume_tags.CSCLASS",
      "volume_tags.CSQUAL",
      "volume_tags.CSTYPE",
      "volume_tags.DataClassification",
      "volume_tags.Docker",
      "volume_tags.ENVIRONMENT",
      "volume_tags.Environment",
      "volume_tags.FUNCTION",
      "volume_tags.GROUPCONTACT",
      "volume_tags.MEMBERFIRM",
      "volume_tags.ManagedBy",
      "volume_tags.Name",
      "volume_tags.Owner",
      "volume_tags.PRIMARYCONTACT",
      "volume_tags.PatchGroup",
      "volume_tags.ResourceSeverity",
      "volume_tags.ResourceType",
      "volume_tags.Role",
      "volume_tags.SECONDARYCONTACT",
      "volume_tags.SolutionName",
      "volume_tags.VendorManaged"
    ]
  }

  volume_tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "PROJECT"
  }

  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "PROJECT"
    PatchGroup = "AmazonLinux2-AzB-Group1"
    Owner = "robhatnagar@deloitte.ca"
    ManagedBy = "CMS-TF"
    SolutionName = "InsurCloud Economical"
    Environment = "Dev"
    ApplicationName = "docker, microservices"
    DataClassification = "PII, PHI"
    DLM-Backup = "Yes"
    DLM-Retention = "90"
    VendorManaged = "False"
    ResourceType = "Client"
    ResourceSeverity = "Normal"
    Name = "A-MC-CAC-D-A003-ECONOMICAL-SIT01-MS-APP02-LNX-EC2"
    Role = "Container"
  }

  provisioner "local-exec" {
    command = "echo ${aws_instance.ICECONOMICAL_SIT01_MICROSERVICES_APPLICATION02.private_ip} >> dev_hosts.txt"
  }
}

output "ICECONOMICAL_SIT01_MICROSERVICES_APPLICATION02_PRIVATEIP" {
  value = "${aws_instance.ICECONOMICAL_SIT01_MICROSERVICES_APPLICATION02.private_ip}"
}

######### RPA ###########
data "template_file" "ICECONOMICAL_SIT01_DOMAIN_JOIN_RPA_APP01" {
  template = "${file("IC-Economical-DEV/windowsdata.tpl")}"

  vars = {
    computer_name = "SIT01-RPA-A01"
    region = "${var.region}"
    domain = "${var.ad_domain}"
    dns1 = "${var.ad_dns1}"
    dns2 = "${var.ad_dns2}"
    dns3 = "${var.ad_dns3}"
    ouselection = "${var.ou_name}"
    pwordparameter = "${var.pwordparameter}"
    unameparameter = "${var.unameparameter}"
  }
}

resource "aws_instance" "ICECONOMICAL_SIT01_RPA_APP01" {
  ami = "${var.ami_id_windows2016base}"
  vpc_security_group_ids = [
    "${aws_security_group.SG_AVIATRIX_RDP.id}",
    "${aws_security_group.SG_RPA_APP.id}"
  ]
  subnet_id = "${var.economical-dev-private-1a-subnet}"
  instance_type = "t3a.medium"
  key_name = "EconomicalRPANPRD"
  associate_public_ip_address = false
  iam_instance_profile = "CMS-SSMInstanceRole"
  ebs_optimized = "true"

  root_block_device {
    volume_size = "50"
  }

  user_data = "${data.template_file.ICECONOMICAL_SIT01_DOMAIN_JOIN_RPA_APP01.rendered}"

  lifecycle {
    prevent_destroy = false
    ignore_changes = [
      "tags.%",
      "tags.BILLINGCODE",
      "tags.BILLINGCONTACT",
      "tags.CLIENT",
      "tags.COUNTRY",
      "tags.CSCLASS",
      "tags.CSQUAL",
      "tags.CSTYPE",
      "tags.ENVIRONMENT",
      "tags.FUNCTION",
      "tags.GROUPCONTACT",
      "tags.MEMBERFIRM",
      "tags.PRIMARYCONTACT",
      "tags.SECONDARYCONTACT",
      "volume_tags.%",
      "volume_tags.AccountID",
      "volume_tags.ApplicationName",
      "volume_tags.BILLINGCODE",
      "volume_tags.BILLINGCONTACT",
      "volume_tags.CLIENT",
      "volume_tags.COUNTRY",
      "volume_tags.CSCLASS",
      "volume_tags.CSQUAL",
      "volume_tags.CSTYPE",
      "volume_tags.DataClassification",
      "volume_tags.Docker",
      "volume_tags.ENVIRONMENT",
      "volume_tags.Environment",
      "volume_tags.FUNCTION",
      "volume_tags.GROUPCONTACT",
      "volume_tags.MEMBERFIRM",
      "volume_tags.ManagedBy",
      "volume_tags.Name",
      "volume_tags.Owner",
      "volume_tags.PRIMARYCONTACT",
      "volume_tags.PatchGroup",
      "volume_tags.ResourceSeverity",
      "volume_tags.ResourceType",
      "volume_tags.Role",
      "volume_tags.SECONDARYCONTACT",
      "volume_tags.SolutionName",
      "volume_tags.VendorManaged"
    ]
  }

  volume_tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "PROJECT"
  }

  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "PROJECT"
    PatchGroup = "Windows-AzA-Group1"
    Owner = "robhatnagar@deloitte.ca"
    ManagedBy = "CMS-TF"
    SolutionName = "InsurCloud Economical"
    Environment = "Dev"
    ApplicationName = "rpa, blueprism"
    DataClassification = "PII, PHI"
    DLM-Backup = "Yes"
    DLM-Retention = "90"
    VendorManaged = "False"
    ResourceType = "Client"
    ResourceSeverity = "Normal"
    Name = "A-MC-CAC-D-A003-ECONOMICAL-SIT01-RPA-APP01-WIN-EC2"
  }
}

output "ICECONOMICAL_SIT01_RPA_APP01_PRIVATEIP" {
  value = "${aws_instance.ICECONOMICAL_SIT01_RPA_APP01.private_ip}"
}

data "template_file" "ICECONOMICAL_SIT01_DOMAIN_JOIN_RPA_APP02" {
  template = "${file("IC-Economical-DEV/windowsdata.tpl")}"

  vars = {
    computer_name = "SIT01-RPA-A02"
    region = "${var.region}"
    domain = "${var.ad_domain}"
    dns1 = "${var.ad_dns1}"
    dns2 = "${var.ad_dns2}"
    dns3 = "${var.ad_dns3}"
    ouselection = "${var.ou_name}"
    pwordparameter = "${var.pwordparameter}"
    unameparameter = "${var.unameparameter}"
  }
}

resource "aws_instance" "ICECONOMICAL_SIT01_RPA_APP02" {
  ami = "${var.ami_id_windows2016base}"
  vpc_security_group_ids = [
    "${aws_security_group.SG_AVIATRIX_RDP.id}",
    "${aws_security_group.SG_RPA_APP.id}"
  ]
  subnet_id = "${var.economical-dev-private-1b-subnet}"
  instance_type = "t3a.medium"
  key_name = "EconomicalRPANPRD"
  associate_public_ip_address = false
  iam_instance_profile = "CMS-SSMInstanceRole"
  ebs_optimized = "true"

  root_block_device {
    volume_size = "50"
  }

  user_data = "${data.template_file.ICECONOMICAL_SIT01_DOMAIN_JOIN_RPA_APP02.rendered}"

  lifecycle {
    prevent_destroy = false
    ignore_changes = [
      "tags.%",
      "tags.BILLINGCODE",
      "tags.BILLINGCONTACT",
      "tags.CLIENT",
      "tags.COUNTRY",
      "tags.CSCLASS",
      "tags.CSQUAL",
      "tags.CSTYPE",
      "tags.ENVIRONMENT",
      "tags.FUNCTION",
      "tags.GROUPCONTACT",
      "tags.MEMBERFIRM",
      "tags.PRIMARYCONTACT",
      "tags.SECONDARYCONTACT",
      "volume_tags.%",
      "volume_tags.AccountID",
      "volume_tags.ApplicationName",
      "volume_tags.BILLINGCODE",
      "volume_tags.BILLINGCONTACT",
      "volume_tags.CLIENT",
      "volume_tags.COUNTRY",
      "volume_tags.CSCLASS",
      "volume_tags.CSQUAL",
      "volume_tags.CSTYPE",
      "volume_tags.DataClassification",
      "volume_tags.Docker",
      "volume_tags.ENVIRONMENT",
      "volume_tags.Environment",
      "volume_tags.FUNCTION",
      "volume_tags.GROUPCONTACT",
      "volume_tags.MEMBERFIRM",
      "volume_tags.ManagedBy",
      "volume_tags.Name",
      "volume_tags.Owner",
      "volume_tags.PRIMARYCONTACT",
      "volume_tags.PatchGroup",
      "volume_tags.ResourceSeverity",
      "volume_tags.ResourceType",
      "volume_tags.Role",
      "volume_tags.SECONDARYCONTACT",
      "volume_tags.SolutionName",
      "volume_tags.VendorManaged"
    ]
  }

  volume_tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "PROJECT"
  }

  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "PROJECT"
    PatchGroup = "Windows-AzB-Group1"
    Owner = "robhatnagar@deloitte.ca"
    ManagedBy = "CMS-TF"
    SolutionName = "InsurCloud Economical"
    Environment = "Dev"
    ApplicationName = "rpa, blueprism"
    DataClassification = "PII, PHI"
    DLM-Backup = "Yes"
    DLM-Retention = "90"
    VendorManaged = "False"
    ResourceType = "Client"
    ResourceSeverity = "Normal"
    Name = "A-MC-CAC-D-A003-ECONOMICAL-SIT01-RPA-APP02-WIN-EC2"
  }
}

output "ICECONOMICAL_SIT01_RPA_APP02_PRIVATEIP" {
  value = "${aws_instance.ICECONOMICAL_SIT01_RPA_APP02.private_ip}"
}

data "template_file" "ECO_SIT01_DOMAIN_JOIN_INT01" {
  template = "${file("IC-Economical-DEV/windowsdata.tpl")}"

  vars = {
    computer_name = "SIT01-RPA-I01"
    region = "${var.region}"
    domain = "${var.ad_domain}"
    dns1 = "${var.ad_dns1}"
    dns2 = "${var.ad_dns2}"
    dns3 = "${var.ad_dns3}"
    ouselection = "${var.ou_name}"
    pwordparameter = "${var.pwordparameter}"
    unameparameter = "${var.unameparameter}"
  }
}

resource "aws_instance" "ICECONOMICAL_SIT01_RPA_INT01" {
  ami = "${var.ami_id_windows2016base}"
  vpc_security_group_ids = [
    "${aws_security_group.SG_AVIATRIX_RDP.id}",
    "${aws_security_group.SG_RPA_APP.id}"
  ]
  subnet_id = "${var.economical-dev-private-1a-subnet}"
  instance_type = "t3a.medium"
  key_name = "EconomicalRPANPRD"
  associate_public_ip_address = false
  iam_instance_profile = "CMS-SSMInstanceRole"
  ebs_optimized = "true"

  root_block_device {
    volume_size = "50"
  }

  user_data = "${data.template_file.ECO_SIT01_DOMAIN_JOIN_INT01.rendered}"

  lifecycle {
    prevent_destroy = false
    ignore_changes = [
      "tags.%",
      "tags.BILLINGCODE",
      "tags.BILLINGCONTACT",
      "tags.CLIENT",
      "tags.COUNTRY",
      "tags.CSCLASS",
      "tags.CSQUAL",
      "tags.CSTYPE",
      "tags.ENVIRONMENT",
      "tags.FUNCTION",
      "tags.GROUPCONTACT",
      "tags.MEMBERFIRM",
      "tags.PRIMARYCONTACT",
      "tags.SECONDARYCONTACT",
      "volume_tags.%",
      "volume_tags.AccountID",
      "volume_tags.ApplicationName",
      "volume_tags.BILLINGCODE",
      "volume_tags.BILLINGCONTACT",
      "volume_tags.CLIENT",
      "volume_tags.COUNTRY",
      "volume_tags.CSCLASS",
      "volume_tags.CSQUAL",
      "volume_tags.CSTYPE",
      "volume_tags.DataClassification",
      "volume_tags.Docker",
      "volume_tags.ENVIRONMENT",
      "volume_tags.Environment",
      "volume_tags.FUNCTION",
      "volume_tags.GROUPCONTACT",
      "volume_tags.MEMBERFIRM",
      "volume_tags.ManagedBy",
      "volume_tags.Name",
      "volume_tags.Owner",
      "volume_tags.PRIMARYCONTACT",
      "volume_tags.PatchGroup",
      "volume_tags.ResourceSeverity",
      "volume_tags.ResourceType",
      "volume_tags.Role",
      "volume_tags.SECONDARYCONTACT",
      "volume_tags.SolutionName",
      "volume_tags.VendorManaged"
    ]
  }

  volume_tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "PROJECT"
  }

  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "PROJECT"
    PatchGroup = "Windows-AzA-Group1"
    Owner = "robhatnagar@deloitte.ca"
    ManagedBy = "CMS-TF"
    SolutionName = "InsurCloud Economical"
    Environment = "Dev"
    ApplicationName = "rpa, blueprism"
    DataClassification = "PII, PHI"
    DLM-Backup = "Yes"
    DLM-Retention = "90"
    VendorManaged = "False"
    ResourceType = "Client"
    ResourceSeverity = "Normal"
    Name = "A-MC-CAC-D-A003-ECONOMICAL-SIT01-RPA-INT01-WIN-EC2"
  }
}

output "ICECONOMICAL_SIT01_RPA_INT01_PRIVATEIP" {
  value = "${aws_instance.ICECONOMICAL_SIT01_RPA_INT01.private_ip}"
}

data "template_file" "ECO_SIT01_DOMAIN_JOIN_RPA_C01" {
  template = "${file("IC-Economical-DEV/windowsdata.tpl")}"

  vars = {
    computer_name = "SIT01-RPA-C01"
    region = "${var.region}"
    domain = "${var.ad_domain}"
    dns1 = "${var.ad_dns1}"
    dns2 = "${var.ad_dns2}"
    dns3 = "${var.ad_dns3}"
    ouselection = "${var.ou_name}"
    pwordparameter = "${var.pwordparameter}"
    unameparameter = "${var.unameparameter}"
  }
}

resource "aws_instance" "ICECONOMICAL_SIT01_RPA_CLIENT01" {
  ami = "${var.ami_id_windows2016base}"
  vpc_security_group_ids = [
    "${aws_security_group.SG_AVIATRIX_RDP.id}",
    "${aws_security_group.SG_RPA_CLIENT.id}"
  ]
  subnet_id = "${var.economical-dev-private-1a-subnet}"
  instance_type = "t3a.medium"
  key_name = "EconomicalRPANPRD"
  associate_public_ip_address = false
  iam_instance_profile = "CMS-SSMInstanceRole"
  ebs_optimized = "true"

  root_block_device {
    volume_size = "50"
  }

  user_data = "${data.template_file.ECO_SIT01_DOMAIN_JOIN_RPA_C01.rendered}"

  lifecycle {
    prevent_destroy = false
    ignore_changes = [
      "tags.%",
      "tags.BILLINGCODE",
      "tags.BILLINGCONTACT",
      "tags.CLIENT",
      "tags.COUNTRY",
      "tags.CSCLASS",
      "tags.CSQUAL",
      "tags.CSTYPE",
      "tags.ENVIRONMENT",
      "tags.FUNCTION",
      "tags.GROUPCONTACT",
      "tags.MEMBERFIRM",
      "tags.PRIMARYCONTACT",
      "tags.SECONDARYCONTACT",
      "volume_tags.%",
      "volume_tags.AccountID",
      "volume_tags.ApplicationName",
      "volume_tags.BILLINGCODE",
      "volume_tags.BILLINGCONTACT",
      "volume_tags.CLIENT",
      "volume_tags.COUNTRY",
      "volume_tags.CSCLASS",
      "volume_tags.CSQUAL",
      "volume_tags.CSTYPE",
      "volume_tags.DataClassification",
      "volume_tags.Docker",
      "volume_tags.ENVIRONMENT",
      "volume_tags.Environment",
      "volume_tags.FUNCTION",
      "volume_tags.GROUPCONTACT",
      "volume_tags.MEMBERFIRM",
      "volume_tags.ManagedBy",
      "volume_tags.Name",
      "volume_tags.Owner",
      "volume_tags.PRIMARYCONTACT",
      "volume_tags.PatchGroup",
      "volume_tags.ResourceSeverity",
      "volume_tags.ResourceType",
      "volume_tags.Role",
      "volume_tags.SECONDARYCONTACT",
      "volume_tags.SolutionName",
      "volume_tags.VendorManaged"
    ]
  }

  volume_tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "PROJECT"
  }

  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "PROJECT"
    PatchGroup = "Windows-AzA-Group1"
    Owner = "robhatnagar@deloitte.ca"
    ManagedBy = "CMS-TF"
    SolutionName = "InsurCloud Economical"
    Environment = "Dev"
    ApplicationName = "rpa, blueprism"
    DataClassification = "PII, PHI"
    DLM-Backup = "Yes"
    DLM-Retention = "90"
    VendorManaged = "False"
    ResourceType = "Client"
    ResourceSeverity = "Normal"
    Name = "A-MC-CAC-D-A003-ECONOMICAL-SIT01-RPA-CLIENT01-WIN-EC2"
  }
}

output "ICECONOMICAL_SIT01_RPA_CLIENT01_PRIVATEIP" {
  value = "${aws_instance.ICECONOMICAL_SIT01_RPA_CLIENT01.private_ip}"
}

data "template_file" "ECO_SIT01_DOMAIN_JOIN_RPA_C02" {
  template = "${file("IC-Economical-DEV/windowsdata.tpl")}"

  vars = {
    computer_name = "SIT01-RPA-C02"
    region = "${var.region}"
    domain = "${var.ad_domain}"
    dns1 = "${var.ad_dns1}"
    dns2 = "${var.ad_dns2}"
    dns3 = "${var.ad_dns3}"
    ouselection = "${var.ou_name}"
    pwordparameter = "${var.pwordparameter}"
    unameparameter = "${var.unameparameter}"
  }
}

resource "aws_instance" "ICECONOMICAL_SIT01_RPA_CLIENT02" {
  ami = "${var.ami_id_windows2016base}"
  vpc_security_group_ids = [
    "${aws_security_group.SG_AVIATRIX_RDP.id}",
    "${aws_security_group.SG_RPA_CLIENT.id}"
  ]
  subnet_id = "${var.economical-dev-private-1b-subnet}"
  instance_type = "t3a.medium"
  key_name = "EconomicalRPANPRD"
  associate_public_ip_address = false
  iam_instance_profile = "CMS-SSMInstanceRole"
  ebs_optimized = "true"

  root_block_device {
    volume_size = "50"
  }

  user_data = "${data.template_file.ECO_SIT01_DOMAIN_JOIN_RPA_C02.rendered}"

  lifecycle {
    prevent_destroy = false
    ignore_changes = [
      "tags.%",
      "tags.BILLINGCODE",
      "tags.BILLINGCONTACT",
      "tags.CLIENT",
      "tags.COUNTRY",
      "tags.CSCLASS",
      "tags.CSQUAL",
      "tags.CSTYPE",
      "tags.ENVIRONMENT",
      "tags.FUNCTION",
      "tags.GROUPCONTACT",
      "tags.MEMBERFIRM",
      "tags.PRIMARYCONTACT",
      "tags.SECONDARYCONTACT",
      "volume_tags.%",
      "volume_tags.AccountID",
      "volume_tags.ApplicationName",
      "volume_tags.BILLINGCODE",
      "volume_tags.BILLINGCONTACT",
      "volume_tags.CLIENT",
      "volume_tags.COUNTRY",
      "volume_tags.CSCLASS",
      "volume_tags.CSQUAL",
      "volume_tags.CSTYPE",
      "volume_tags.DataClassification",
      "volume_tags.Docker",
      "volume_tags.ENVIRONMENT",
      "volume_tags.Environment",
      "volume_tags.FUNCTION",
      "volume_tags.GROUPCONTACT",
      "volume_tags.MEMBERFIRM",
      "volume_tags.ManagedBy",
      "volume_tags.Name",
      "volume_tags.Owner",
      "volume_tags.PRIMARYCONTACT",
      "volume_tags.PatchGroup",
      "volume_tags.ResourceSeverity",
      "volume_tags.ResourceType",
      "volume_tags.Role",
      "volume_tags.SECONDARYCONTACT",
      "volume_tags.SolutionName",
      "volume_tags.VendorManaged"
    ]
  }

  volume_tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "PROJECT"
  }

  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "PROJECT"
    PatchGroup = "Windows-AzB-Group1"
    Owner = "robhatnagar@deloitte.ca"
    ManagedBy = "CMS-TF"
    SolutionName = "InsurCloud Economical"
    Environment = "Dev"
    ApplicationName = "rpa, blueprism"
    DataClassification = "PII, PHI"
    DLM-Backup = "Yes"
    DLM-Retention = "90"
    VendorManaged = "False"
    ResourceType = "Client"
    ResourceSeverity = "Normal"
    Name = "A-MC-CAC-D-A003-ECONOMICAL-SIT01-RPA-CLIENT02-WIN-EC2"
  }
}

output "ICECONOMICAL_SIT01_RPA_CLIENT02_PRIVATEIP" {
  value = "${aws_instance.ICECONOMICAL_SIT01_RPA_CLIENT02.private_ip}"
}

data "template_file" "ECO_SIT01_DOMAIN_JOIN_RPA_C03" {
  template = "${file("IC-Economical-DEV/windowsdata.tpl")}"

  vars = {
    computer_name = "SIT01-RPA-C03"
    region = "${var.region}"
    domain = "${var.ad_domain}"
    dns1 = "${var.ad_dns1}"
    dns2 = "${var.ad_dns2}"
    dns3 = "${var.ad_dns3}"
    ouselection = "${var.ou_name}"
    pwordparameter = "${var.pwordparameter}"
    unameparameter = "${var.unameparameter}"
  }
}

resource "aws_instance" "ICECONOMICAL_SIT01_RPA_CLIENT03" {
  ami = "${var.ami_id_windows2016base}"
  vpc_security_group_ids = [
    "${aws_security_group.SG_AVIATRIX_RDP.id}",
    "${aws_security_group.SG_RPA_CLIENT.id}"
  ]
  subnet_id = "${var.economical-dev-private-1b-subnet}"
  instance_type = "t3a.medium"
  key_name = "EconomicalRPANPRD"
  associate_public_ip_address = false
  iam_instance_profile = "CMS-SSMInstanceRole"
  ebs_optimized = "true"

  root_block_device {
    volume_size = "50"
  }

  user_data = "${data.template_file.ECO_SIT01_DOMAIN_JOIN_RPA_C03.rendered}"

  lifecycle {
    prevent_destroy = false
    ignore_changes = [
      "tags.%",
      "tags.BILLINGCODE",
      "tags.BILLINGCONTACT",
      "tags.CLIENT",
      "tags.COUNTRY",
      "tags.CSCLASS",
      "tags.CSQUAL",
      "tags.CSTYPE",
      "tags.ENVIRONMENT",
      "tags.FUNCTION",
      "tags.GROUPCONTACT",
      "tags.MEMBERFIRM",
      "tags.PRIMARYCONTACT",
      "tags.SECONDARYCONTACT",
      "volume_tags.%",
      "volume_tags.AccountID",
      "volume_tags.ApplicationName",
      "volume_tags.BILLINGCODE",
      "volume_tags.BILLINGCONTACT",
      "volume_tags.CLIENT",
      "volume_tags.COUNTRY",
      "volume_tags.CSCLASS",
      "volume_tags.CSQUAL",
      "volume_tags.CSTYPE",
      "volume_tags.DataClassification",
      "volume_tags.Docker",
      "volume_tags.ENVIRONMENT",
      "volume_tags.Environment",
      "volume_tags.FUNCTION",
      "volume_tags.GROUPCONTACT",
      "volume_tags.MEMBERFIRM",
      "volume_tags.ManagedBy",
      "volume_tags.Name",
      "volume_tags.Owner",
      "volume_tags.PRIMARYCONTACT",
      "volume_tags.PatchGroup",
      "volume_tags.ResourceSeverity",
      "volume_tags.ResourceType",
      "volume_tags.Role",
      "volume_tags.SECONDARYCONTACT",
      "volume_tags.SolutionName",
      "volume_tags.VendorManaged"
    ]
  }

  volume_tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "PROJECT"
  }

  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "PROJECT"
    PatchGroup = "Windows-AzA-Group1"
    Owner = "robhatnagar@deloitte.ca"
    ManagedBy = "CMS-TF"
    SolutionName = "InsurCloud Economical"
    Environment = "Dev"
    ApplicationName = "rpa, blueprism"
    DataClassification = "PII, PHI"
    DLM-Backup = "Yes"
    DLM-Retention = "90"
    VendorManaged = "False"
    ResourceType = "Client"
    ResourceSeverity = "Normal"
    Name = "A-MC-CAC-D-A003-ECONOMICAL-SIT01-RPA-CLIENT03-WIN-EC2"
  }
}

output "ICECONOMICAL_SIT01_RPA_CLIENT03_PRIVATEIP" {
  value = "${aws_instance.ICECONOMICAL_SIT01_RPA_CLIENT03.private_ip}"
}

data "template_file" "ECO_SIT01_DOMAIN_JOIN_RPA_C04" {
  template = "${file("IC-Economical-DEV/windowsdata.tpl")}"

  vars = {
    computer_name = "SIT01-RPA-C04"
    region = "${var.region}"
    domain = "${var.ad_domain}"
    dns1 = "${var.ad_dns1}"
    dns2 = "${var.ad_dns2}"
    dns3 = "${var.ad_dns3}"
    ouselection = "${var.ou_name}"
    pwordparameter = "${var.pwordparameter}"
    unameparameter = "${var.unameparameter}"
  }
}

resource "aws_instance" "ICECONOMICAL_SIT01_RPA_CLIENT04" {
  ami = "${var.ami_id_windows2016base}"
  vpc_security_group_ids = [
    "${aws_security_group.SG_AVIATRIX_RDP.id}",
    "${aws_security_group.SG_RPA_CLIENT.id}"
  ]
  subnet_id = "${var.economical-dev-private-1b-subnet}"
  instance_type = "t3a.medium"
  key_name = "EconomicalRPANPRD"
  associate_public_ip_address = false
  iam_instance_profile = "CMS-SSMInstanceRole"
  ebs_optimized = "true"

  root_block_device {
    volume_size = "50"
  }

  user_data = "${data.template_file.ECO_SIT01_DOMAIN_JOIN_RPA_C04.rendered}"

  lifecycle {
    prevent_destroy = false
    ignore_changes = [
      "tags.%",
      "tags.BILLINGCODE",
      "tags.BILLINGCONTACT",
      "tags.CLIENT",
      "tags.COUNTRY",
      "tags.CSCLASS",
      "tags.CSQUAL",
      "tags.CSTYPE",
      "tags.ENVIRONMENT",
      "tags.FUNCTION",
      "tags.GROUPCONTACT",
      "tags.MEMBERFIRM",
      "tags.PRIMARYCONTACT",
      "tags.SECONDARYCONTACT",
      "volume_tags.%",
      "volume_tags.AccountID",
      "volume_tags.ApplicationName",
      "volume_tags.BILLINGCODE",
      "volume_tags.BILLINGCONTACT",
      "volume_tags.CLIENT",
      "volume_tags.COUNTRY",
      "volume_tags.CSCLASS",
      "volume_tags.CSQUAL",
      "volume_tags.CSTYPE",
      "volume_tags.DataClassification",
      "volume_tags.Docker",
      "volume_tags.ENVIRONMENT",
      "volume_tags.Environment",
      "volume_tags.FUNCTION",
      "volume_tags.GROUPCONTACT",
      "volume_tags.MEMBERFIRM",
      "volume_tags.ManagedBy",
      "volume_tags.Name",
      "volume_tags.Owner",
      "volume_tags.PRIMARYCONTACT",
      "volume_tags.PatchGroup",
      "volume_tags.ResourceSeverity",
      "volume_tags.ResourceType",
      "volume_tags.Role",
      "volume_tags.SECONDARYCONTACT",
      "volume_tags.SolutionName",
      "volume_tags.VendorManaged"
    ]
  }

  volume_tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "PROJECT"
  }

  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "PROJECT"
    PatchGroup = "Windows-AzB-Group1"
    Owner = "robhatnagar@deloitte.ca"
    ManagedBy = "CMS-TF"
    SolutionName = "InsurCloud Economical"
    Environment = "Dev"
    ApplicationName = "rpa, blueprism"
    DataClassification = "PII, PHI"
    DLM-Backup = "Yes"
    DLM-Retention = "90"
    VendorManaged = "False"
    ResourceType = "Client"
    ResourceSeverity = "Normal"
    Name = "A-MC-CAC-D-A003-ECONOMICAL-SIT01-RPA-CLIENT04-WIN-EC2"
  }
}

output "ICECONOMICAL_SIT01_RPA_CLIENT04_PRIVATEIP" {
  value = "${aws_instance.ICECONOMICAL_SIT01_RPA_CLIENT04.private_ip}"
}

data "template_file" "ECO_SIT01_DOMAIN_JOIN_RPA_C05" {
  template = "${file("IC-Economical-DEV/windowsdata.tpl")}"

  vars = {
    computer_name = "SIT01-RPA-C05"
    region = "${var.region}"
    domain = "${var.ad_domain}"
    dns1 = "${var.ad_dns1}"
    dns2 = "${var.ad_dns2}"
    dns3 = "${var.ad_dns3}"
    ouselection = "${var.ou_name}"
    pwordparameter = "${var.pwordparameter}"
    unameparameter = "${var.unameparameter}"
  }
}

resource "aws_instance" "ICECONOMICAL_SIT01_RPA_CLIENT05" {
  ami = "${var.ami_id_windows2016base}"
  vpc_security_group_ids = [
    "${aws_security_group.SG_AVIATRIX_RDP.id}",
    "${aws_security_group.SG_RPA_CLIENT.id}"
  ]
  subnet_id = "${var.economical-dev-private-1b-subnet}"
  instance_type = "t3a.medium"
  key_name = "EconomicalRPANPRD"
  associate_public_ip_address = false
  iam_instance_profile = "CMS-SSMInstanceRole"
  ebs_optimized = "true"

  root_block_device {
    volume_size = "50"
  }

  user_data = "${data.template_file.ECO_SIT01_DOMAIN_JOIN_RPA_C05.rendered}"

  lifecycle {
    prevent_destroy = false
    ignore_changes = [
      "tags.%",
      "tags.BILLINGCODE",
      "tags.BILLINGCONTACT",
      "tags.CLIENT",
      "tags.COUNTRY",
      "tags.CSCLASS",
      "tags.CSQUAL",
      "tags.CSTYPE",
      "tags.ENVIRONMENT",
      "tags.FUNCTION",
      "tags.GROUPCONTACT",
      "tags.MEMBERFIRM",
      "tags.PRIMARYCONTACT",
      "tags.SECONDARYCONTACT",
      "volume_tags.%",
      "volume_tags.AccountID",
      "volume_tags.ApplicationName",
      "volume_tags.BILLINGCODE",
      "volume_tags.BILLINGCONTACT",
      "volume_tags.CLIENT",
      "volume_tags.COUNTRY",
      "volume_tags.CSCLASS",
      "volume_tags.CSQUAL",
      "volume_tags.CSTYPE",
      "volume_tags.DataClassification",
      "volume_tags.Docker",
      "volume_tags.ENVIRONMENT",
      "volume_tags.Environment",
      "volume_tags.FUNCTION",
      "volume_tags.GROUPCONTACT",
      "volume_tags.MEMBERFIRM",
      "volume_tags.ManagedBy",
      "volume_tags.Name",
      "volume_tags.Owner",
      "volume_tags.PRIMARYCONTACT",
      "volume_tags.PatchGroup",
      "volume_tags.ResourceSeverity",
      "volume_tags.ResourceType",
      "volume_tags.Role",
      "volume_tags.SECONDARYCONTACT",
      "volume_tags.SolutionName",
      "volume_tags.VendorManaged"
    ]
  }

  volume_tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "PROJECT"
  }

  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "PROJECT"
    PatchGroup = "Windows-AzA-Group1"
    Owner = "robhatnagar@deloitte.ca"
    ManagedBy = "CMS-TF"
    SolutionName = "InsurCloud Economical"
    Environment = "Dev"
    ApplicationName = "rpa, blueprism"
    DataClassification = "PII, PHI"
    DLM-Backup = "Yes"
    DLM-Retention = "90"
    VendorManaged = "False"
    ResourceType = "Client"
    ResourceSeverity = "Normal"
    Name = "A-MC-CAC-D-A003-ECONOMICAL-SIT01-RPA-CLIENT05-WIN-EC2"
  }
}

output "ICECONOMICAL_SIT01_RPA_CLIENT05_PRIVATEIP" {
  value = "${aws_instance.ICECONOMICAL_SIT01_RPA_CLIENT05.private_ip}"
}

data "template_file" "ECO_SIT01_DOMAIN_JOIN_RPA_C06" {
  template = "${file("IC-Economical-DEV/windowsdata.tpl")}"

  vars = {
    computer_name = "SIT01-RPA-C06"
    region = "${var.region}"
    domain = "${var.ad_domain}"
    dns1 = "${var.ad_dns1}"
    dns2 = "${var.ad_dns2}"
    dns3 = "${var.ad_dns3}"
    ouselection = "${var.ou_name}"
    pwordparameter = "${var.pwordparameter}"
    unameparameter = "${var.unameparameter}"
  }
}

resource "aws_instance" "ICECONOMICAL_SIT01_RPA_CLIENT06" {
  ami = "${var.ami_id_windows2016base}"
  vpc_security_group_ids = [
    "${aws_security_group.SG_AVIATRIX_RDP.id}",
    "${aws_security_group.SG_RPA_CLIENT.id}"
  ]
  subnet_id = "${var.economical-dev-private-1b-subnet}"
  instance_type = "t3a.medium"
  key_name = "EconomicalRPANPRD"
  associate_public_ip_address = false
  iam_instance_profile = "CMS-SSMInstanceRole"
  ebs_optimized = "true"

  root_block_device {
    volume_size = "50"
  }

  user_data = "${data.template_file.ECO_SIT01_DOMAIN_JOIN_RPA_C06.rendered}"

  lifecycle {
    prevent_destroy = false
    ignore_changes = [
      "tags.%",
      "tags.BILLINGCODE",
      "tags.BILLINGCONTACT",
      "tags.CLIENT",
      "tags.COUNTRY",
      "tags.CSCLASS",
      "tags.CSQUAL",
      "tags.CSTYPE",
      "tags.ENVIRONMENT",
      "tags.FUNCTION",
      "tags.GROUPCONTACT",
      "tags.MEMBERFIRM",
      "tags.PRIMARYCONTACT",
      "tags.SECONDARYCONTACT",
      "volume_tags.%",
      "volume_tags.AccountID",
      "volume_tags.ApplicationName",
      "volume_tags.BILLINGCODE",
      "volume_tags.BILLINGCONTACT",
      "volume_tags.CLIENT",
      "volume_tags.COUNTRY",
      "volume_tags.CSCLASS",
      "volume_tags.CSQUAL",
      "volume_tags.CSTYPE",
      "volume_tags.DataClassification",
      "volume_tags.Docker",
      "volume_tags.ENVIRONMENT",
      "volume_tags.Environment",
      "volume_tags.FUNCTION",
      "volume_tags.GROUPCONTACT",
      "volume_tags.MEMBERFIRM",
      "volume_tags.ManagedBy",
      "volume_tags.Name",
      "volume_tags.Owner",
      "volume_tags.PRIMARYCONTACT",
      "volume_tags.PatchGroup",
      "volume_tags.ResourceSeverity",
      "volume_tags.ResourceType",
      "volume_tags.Role",
      "volume_tags.SECONDARYCONTACT",
      "volume_tags.SolutionName",
      "volume_tags.VendorManaged"
    ]
  }

  volume_tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "PROJECT"
  }

  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "PROJECT"
    PatchGroup = "Windows-AzB-Group1"
    Owner = "robhatnagar@deloitte.ca"
    ManagedBy = "CMS-TF"
    SolutionName = "InsurCloud Economical"
    Environment = "Dev"
    ApplicationName = "rpa, blueprism"
    DataClassification = "PII, PHI"
    DLM-Backup = "Yes"
    DLM-Retention = "90"
    VendorManaged = "False"
    ResourceType = "Client"
    ResourceSeverity = "Normal"
    Name = "A-MC-CAC-D-A003-ECONOMICAL-SIT01-RPA-CLIENT06-WIN-EC2"
  }
}

output "ICECONOMICAL_SIT01_RPA_CLIENT06_PRIVATEIP" {
  value = "${aws_instance.ICECONOMICAL_SIT01_RPA_CLIENT06.private_ip}"
}

data "template_file" "ECO_SIT01_DOMAIN_JOIN_RPA_C07" {
  template = "${file("IC-Economical-DEV/windowsdata.tpl")}"

  vars = {
    computer_name = "SIT01-RPA-C07"
    region = "${var.region}"
    domain = "${var.ad_domain}"
    dns1 = "${var.ad_dns1}"
    dns2 = "${var.ad_dns2}"
    dns3 = "${var.ad_dns3}"
    ouselection = "${var.ou_name}"
    pwordparameter = "${var.pwordparameter}"
    unameparameter = "${var.unameparameter}"
  }
}

######### HAPROXY ##############
resource "aws_instance" "ICECONOMICAL_SIT01_INTERNAL_HAPROXY01" {
  ami = "${var.ami_id_amazonlinuxbase20200506}"
  vpc_security_group_ids = [
    "${aws_security_group.SG_INTERNAL_HAPROXY.id}",
    "${aws_security_group.SG_AVIATRIX_SSH.id}",
    "${aws_security_group.SG_JENKINS_CLIENT_SSH.id}"
  ]
  subnet_id = "${var.economical-dev-private-1a-subnet}"
  instance_type = "t3a.medium"
  key_name = "EconomicalHAProxyNPRD"
  associate_public_ip_address = false
  iam_instance_profile = "CMS-SSMInstanceRole"
  ebs_optimized = "true"

  root_block_device {
    volume_size = "40"
  }

  lifecycle {
    prevent_destroy = false
    ignore_changes = [
      "tags.%",
      "tags.BILLINGCODE",
      "tags.BILLINGCONTACT",
      "tags.CLIENT",
      "tags.COUNTRY",
      "tags.CSCLASS",
      "tags.CSQUAL",
      "tags.CSTYPE",
      "tags.ENVIRONMENT",
      "tags.FUNCTION",
      "tags.GROUPCONTACT",
      "tags.MEMBERFIRM",
      "tags.PRIMARYCONTACT",
      "tags.SECONDARYCONTACT",
      "volume_tags.%",
      "volume_tags.AccountID",
      "volume_tags.ApplicationName",
      "volume_tags.BILLINGCODE",
      "volume_tags.BILLINGCONTACT",
      "volume_tags.CLIENT",
      "volume_tags.COUNTRY",
      "volume_tags.CSCLASS",
      "volume_tags.CSQUAL",
      "volume_tags.CSTYPE",
      "volume_tags.DataClassification",
      "volume_tags.Docker",
      "volume_tags.ENVIRONMENT",
      "volume_tags.Environment",
      "volume_tags.FUNCTION",
      "volume_tags.GROUPCONTACT",
      "volume_tags.MEMBERFIRM",
      "volume_tags.ManagedBy",
      "volume_tags.Name",
      "volume_tags.Owner",
      "volume_tags.PRIMARYCONTACT",
      "volume_tags.PatchGroup",
      "volume_tags.ResourceSeverity",
      "volume_tags.ResourceType",
      "volume_tags.Role",
      "volume_tags.SECONDARYCONTACT",
      "volume_tags.SolutionName",
      "volume_tags.VendorManaged"
    ]
  }

  volume_tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "PROJECT"
  }

  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "PROJECT"
    PatchGroup = "AmazonLinux2-AzA-Group1"
    Owner = "robhatnagar@deloitte.ca"
    ManagedBy = "CMS-TF"
    SolutionName = "InsurCloud Economical"
    Environment = "Dev"
    ApplicationName = "docker, haproxy"
    DataClassification = "PII, PHI"
    DLM-Backup = "Yes"
    DLM-Retention = "90"
    VendorManaged = "False"
    ResourceType = "Client"
    ResourceSeverity = "Normal"
    Name = "A-MC-CAC-D-A003-ECONOMICAL-SIT01-INTERNAL-HAPROXY01-EC2"
    Role = "Container"
  }

  provisioner "local-exec" {
    command = "echo ${aws_instance.ICECONOMICAL_SIT01_INTERNAL_HAPROXY01.private_ip} >> dev_hosts.txt"
  }
}

output "ICECONOMICAL_SIT01_INTERNAL_HAPROXY_PRIVATEIP01" {
  value = "${aws_instance.ICECONOMICAL_SIT01_INTERNAL_HAPROXY01.private_ip}"
}

resource "aws_instance" "ICECONOMICAL_SIT01_INTERNAL_HAPROXY02" {
  ami = "${var.ami_id_amazonlinuxbase20200506}"
  vpc_security_group_ids = [
    "${aws_security_group.SG_INTERNAL_HAPROXY.id}",
    "${aws_security_group.SG_AVIATRIX_SSH.id}",
    "${aws_security_group.SG_JENKINS_CLIENT_SSH.id}"
  ]
  subnet_id = "${var.economical-dev-private-1b-subnet}"
  instance_type = "t3a.small"
  key_name = "EconomicalHAProxyNPRD"
  associate_public_ip_address = false
  iam_instance_profile = "CMS-SSMInstanceRole"
  ebs_optimized = "true"

  root_block_device {
    volume_size = "40"
  }

  lifecycle {
    prevent_destroy = false
    ignore_changes = [
      "tags.%",
      "tags.BILLINGCODE",
      "tags.BILLINGCONTACT",
      "tags.CLIENT",
      "tags.COUNTRY",
      "tags.CSCLASS",
      "tags.CSQUAL",
      "tags.CSTYPE",
      "tags.ENVIRONMENT",
      "tags.FUNCTION",
      "tags.GROUPCONTACT",
      "tags.MEMBERFIRM",
      "tags.PRIMARYCONTACT",
      "tags.SECONDARYCONTACT",
      "volume_tags.%",
      "volume_tags.AccountID",
      "volume_tags.ApplicationName",
      "volume_tags.BILLINGCODE",
      "volume_tags.BILLINGCONTACT",
      "volume_tags.CLIENT",
      "volume_tags.COUNTRY",
      "volume_tags.CSCLASS",
      "volume_tags.CSQUAL",
      "volume_tags.CSTYPE",
      "volume_tags.DataClassification",
      "volume_tags.Docker",
      "volume_tags.ENVIRONMENT",
      "volume_tags.Environment",
      "volume_tags.FUNCTION",
      "volume_tags.GROUPCONTACT",
      "volume_tags.MEMBERFIRM",
      "volume_tags.ManagedBy",
      "volume_tags.Name",
      "volume_tags.Owner",
      "volume_tags.PRIMARYCONTACT",
      "volume_tags.PatchGroup",
      "volume_tags.ResourceSeverity",
      "volume_tags.ResourceType",
      "volume_tags.Role",
      "volume_tags.SECONDARYCONTACT",
      "volume_tags.SolutionName",
      "volume_tags.VendorManaged"
    ]
  }

  volume_tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "PROJECT"
  }

  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "PROJECT"
    PatchGroup = "AmazonLinux2-AzB-Group1"
    Owner = "robhatnagar@deloitte.ca"
    ManagedBy = "CMS-TF"
    SolutionName = "InsurCloud Economical"
    Environment = "Dev"
    ApplicationName = "docker, haproxy"
    DataClassification = "PII, PHI"
    DLM-Backup = "Yes"
    DLM-Retention = "90"
    VendorManaged = "False"
    ResourceType = "Client"
    ResourceSeverity = "Normal"
    Name = "A-MC-CAC-D-A003-ECONOMICAL-SIT01-INTERNAL-HAPROXY02-EC2"
    Role = "Container"
  }

  provisioner "local-exec" {
    command = "echo ${aws_instance.ICECONOMICAL_SIT01_INTERNAL_HAPROXY02.private_ip} >> dev_hosts.txt"
  }
}

output "ICECONOMICAL_SIT01_INTERNAL_HAPROXY_PRIVATEIP02" {
  value = "${aws_instance.ICECONOMICAL_SIT01_INTERNAL_HAPROXY02.private_ip}"
}


# #=======================================================================
# # PPS01
# #=======================================================================

######### GUIDEWIRE ##############
resource "aws_instance" "ICECONOMICAL_PPS01_GUIDEWIRE01_SUITE" {
  ami = "${var.ami_id_amazonlinuxbase20200506}"
  vpc_security_group_ids = [
    "${aws_security_group.SG_GUIDEWIRE.id}",
    "${aws_security_group.SG_JENKINS_GUIDEWIRE_HEALTH_CHECK.id}",
    "${aws_security_group.SG_GUIDEWIRE_CLUSTER_NBIO_EVENT_QUEUE.id}",
    "${aws_security_group.SG_AVIATRIX_SSH.id}",
    "${aws_security_group.SG_JENKINS_CLIENT_SSH.id}"
  ]
  subnet_id = "${var.economical-dev-private-1a-subnet}"
  instance_type = "m5a.large"
  key_name = "EconomicalGuidewireNPRD"
  associate_public_ip_address = false
  iam_instance_profile = "CMS-SSMInstanceRole"
  ebs_optimized = "true"

  root_block_device {
    volume_size = "100"
  }

  lifecycle {
    prevent_destroy = false
    ignore_changes = [
      "tags.%",
      "tags.BILLINGCODE",
      "tags.BILLINGCONTACT",
      "tags.CLIENT",
      "tags.COUNTRY",
      "tags.CSCLASS",
      "tags.CSQUAL",
      "tags.CSTYPE",
      "tags.ENVIRONMENT",
      "tags.FUNCTION",
      "tags.GROUPCONTACT",
      "tags.MEMBERFIRM",
      "tags.PRIMARYCONTACT",
      "tags.SECONDARYCONTACT",
      "volume_tags.%",
      "volume_tags.AccountID",
      "volume_tags.ApplicationName",
      "volume_tags.BILLINGCODE",
      "volume_tags.BILLINGCONTACT",
      "volume_tags.CLIENT",
      "volume_tags.COUNTRY",
      "volume_tags.CSCLASS",
      "volume_tags.CSQUAL",
      "volume_tags.CSTYPE",
      "volume_tags.DataClassification",
      "volume_tags.Docker",
      "volume_tags.ENVIRONMENT",
      "volume_tags.Environment",
      "volume_tags.FUNCTION",
      "volume_tags.GROUPCONTACT",
      "volume_tags.MEMBERFIRM",
      "volume_tags.ManagedBy",
      "volume_tags.Name",
      "volume_tags.Owner",
      "volume_tags.PRIMARYCONTACT",
      "volume_tags.PatchGroup",
      "volume_tags.ResourceSeverity",
      "volume_tags.ResourceType",
      "volume_tags.Role",
      "volume_tags.SECONDARYCONTACT",
      "volume_tags.SolutionName",
      "volume_tags.VendorManaged"
    ]
  }

  volume_tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "MANAGED_SERVICES"
  }

  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "MANAGED_SERVICES"
    PatchGroup = "AmazonLinux2-AzA-Group1"
    Owner = "robhatnagar@deloitte.ca"
    ManagedBy = "CMS-TF"
    SolutionName = "InsurCloud Economical"
    Environment = "Dev"
    ApplicationName = "guidewire, docker"
    DataClassification = "PII, PHI"
    DLM-Backup = "Yes"
    DLM-Retention = "90"
    VendorManaged = "False"
    ResourceType = "Client"
    ResourceSeverity = "Normal"
    Name = "A-MC-CAC-D-A003-ECONOMICAL-PPS01-GW01-SUITE-LNX-EC2"
    Role = "Container"
  }

  provisioner "local-exec" {
    command = "echo ${aws_instance.ICECONOMICAL_PPS01_GUIDEWIRE01_SUITE.private_ip} >> dev_hosts.txt"
  }
}

output "ICECONOMICAL_PPS01_GUIDEWIRE01_SUITE_PRIVATEIP" {
  value = "${aws_instance.ICECONOMICAL_PPS01_GUIDEWIRE01_SUITE.private_ip}"
}

resource "aws_instance" "ICECONOMICAL_PPS01_GUIDEWIRE02_SUITE" {
  ami = "${var.ami_id_amazonlinuxbase20200506}"
  vpc_security_group_ids = [
    "${aws_security_group.SG_GUIDEWIRE.id}",
    "${aws_security_group.SG_JENKINS_GUIDEWIRE_HEALTH_CHECK.id}",
    "${aws_security_group.SG_GUIDEWIRE_CLUSTER_NBIO_EVENT_QUEUE.id}",
    "${aws_security_group.SG_AVIATRIX_SSH.id}",
    "${aws_security_group.SG_JENKINS_CLIENT_SSH.id}"
  ]
  subnet_id = "${var.economical-dev-private-1b-subnet}"
  instance_type = "m5a.large"
  key_name = "EconomicalGuidewireNPRD"
  associate_public_ip_address = false
  iam_instance_profile = "CMS-SSMInstanceRole"
  ebs_optimized = "true"

  root_block_device {
    volume_size = "100"
  }

  lifecycle {
    prevent_destroy = false
    ignore_changes = [
      "tags.%",
      "tags.BILLINGCODE",
      "tags.BILLINGCONTACT",
      "tags.CLIENT",
      "tags.COUNTRY",
      "tags.CSCLASS",
      "tags.CSQUAL",
      "tags.CSTYPE",
      "tags.ENVIRONMENT",
      "tags.FUNCTION",
      "tags.GROUPCONTACT",
      "tags.MEMBERFIRM",
      "tags.PRIMARYCONTACT",
      "tags.SECONDARYCONTACT",
      "volume_tags.%",
      "volume_tags.AccountID",
      "volume_tags.ApplicationName",
      "volume_tags.BILLINGCODE",
      "volume_tags.BILLINGCONTACT",
      "volume_tags.CLIENT",
      "volume_tags.COUNTRY",
      "volume_tags.CSCLASS",
      "volume_tags.CSQUAL",
      "volume_tags.CSTYPE",
      "volume_tags.DataClassification",
      "volume_tags.Docker",
      "volume_tags.ENVIRONMENT",
      "volume_tags.Environment",
      "volume_tags.FUNCTION",
      "volume_tags.GROUPCONTACT",
      "volume_tags.MEMBERFIRM",
      "volume_tags.ManagedBy",
      "volume_tags.Name",
      "volume_tags.Owner",
      "volume_tags.PRIMARYCONTACT",
      "volume_tags.PatchGroup",
      "volume_tags.ResourceSeverity",
      "volume_tags.ResourceType",
      "volume_tags.Role",
      "volume_tags.SECONDARYCONTACT",
      "volume_tags.SolutionName",
      "volume_tags.VendorManaged"
    ]
  }

  volume_tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "MANAGED_SERVICES"
  }

  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "MANAGED_SERVICES"
    PatchGroup = "AmazonLinux2-AzB-Group1"
    Owner = "robhatnagar@deloitte.ca"
    ManagedBy = "CMS-TF"
    SolutionName = "InsurCloud Economical"
    Environment = "Dev"
    ApplicationName = "guidewire, docker"
    DataClassification = "PII, PHI"
    DLM-Backup = "Yes"
    DLM-Retention = "90"
    VendorManaged = "False"
    ResourceType = "Client"
    ResourceSeverity = "Normal"
    Name = "A-MC-CAC-D-A003-ECONOMICAL-PPS01-GW02-SUITE-LNX-EC2"
    Role = "Container"
  }

  provisioner "local-exec" {
    command = "echo ${aws_instance.ICECONOMICAL_PPS01_GUIDEWIRE02_SUITE.private_ip} >> dev_hosts.txt"
  }
}

output "ICECONOMICAL_PPS01_GUIDEWIRE02_SUITE_PRIVATEIP" {
  value = "${aws_instance.ICECONOMICAL_PPS01_GUIDEWIRE02_SUITE.private_ip}"
}

resource "aws_instance" "ICECONOMICAL_PPS01_GUIDEWIRE03_SUITE" {
  ami = "${var.ami_id_amazonlinuxbase20200506}"
  vpc_security_group_ids = [
    "${aws_security_group.SG_GUIDEWIRE.id}",
    "${aws_security_group.SG_JENKINS_GUIDEWIRE_HEALTH_CHECK.id}",
    "${aws_security_group.SG_GUIDEWIRE_CLUSTER_NBIO_EVENT_QUEUE.id}",
    "${aws_security_group.SG_AVIATRIX_SSH.id}",
    "${aws_security_group.SG_JENKINS_CLIENT_SSH.id}"
  ]
  subnet_id = "${var.economical-dev-private-1a-subnet}"
  instance_type = "m5a.large"
  key_name = "EconomicalGuidewireNPRD"
  associate_public_ip_address = false
  iam_instance_profile = "CMS-SSMInstanceRole"
  ebs_optimized = "true"

  root_block_device {
    volume_size = "100"
  }

  lifecycle {
    prevent_destroy = false
    ignore_changes = [
      "tags.%",
      "tags.BILLINGCODE",
      "tags.BILLINGCONTACT",
      "tags.CLIENT",
      "tags.COUNTRY",
      "tags.CSCLASS",
      "tags.CSQUAL",
      "tags.CSTYPE",
      "tags.ENVIRONMENT",
      "tags.FUNCTION",
      "tags.GROUPCONTACT",
      "tags.MEMBERFIRM",
      "tags.PRIMARYCONTACT",
      "tags.SECONDARYCONTACT",
      "volume_tags.%",
      "volume_tags.AccountID",
      "volume_tags.ApplicationName",
      "volume_tags.BILLINGCODE",
      "volume_tags.BILLINGCONTACT",
      "volume_tags.CLIENT",
      "volume_tags.COUNTRY",
      "volume_tags.CSCLASS",
      "volume_tags.CSQUAL",
      "volume_tags.CSTYPE",
      "volume_tags.DataClassification",
      "volume_tags.Docker",
      "volume_tags.ENVIRONMENT",
      "volume_tags.Environment",
      "volume_tags.FUNCTION",
      "volume_tags.GROUPCONTACT",
      "volume_tags.MEMBERFIRM",
      "volume_tags.ManagedBy",
      "volume_tags.Name",
      "volume_tags.Owner",
      "volume_tags.PRIMARYCONTACT",
      "volume_tags.PatchGroup",
      "volume_tags.ResourceSeverity",
      "volume_tags.ResourceType",
      "volume_tags.Role",
      "volume_tags.SECONDARYCONTACT",
      "volume_tags.SolutionName",
      "volume_tags.VendorManaged"
    ]
  }

  volume_tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "MANAGED_SERVICES"
  }

  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "MANAGED_SERVICES"
    PatchGroup = "AmazonLinux2-AzA-Group1"
    Owner = "robhatnagar@deloitte.ca"
    ManagedBy = "CMS-TF"
    SolutionName = "InsurCloud Economical"
    Environment = "Dev"
    ApplicationName = "guidewire, docker"
    DataClassification = "PII, PHI"
    DLM-Backup = "Yes"
    DLM-Retention = "90"
    VendorManaged = "False"
    ResourceType = "Client"
    ResourceSeverity = "Normal"
    Name = "A-MC-CAC-D-A003-ECONOMICAL-PPS01-GW03-SUITE-LNX-EC2"
    Role = "Container"
  }

  provisioner "local-exec" {
    command = "echo ${aws_instance.ICECONOMICAL_PPS01_GUIDEWIRE03_SUITE.private_ip} >> dev_hosts.txt"
  }
}

output "ICECONOMICAL_PPS01_GUIDEWIRE03_SUITE_PRIVATEIP" {
  value = "${aws_instance.ICECONOMICAL_PPS01_GUIDEWIRE03_SUITE.private_ip}"
}

resource "aws_instance" "ICECONOMICAL_PPS01_GUIDEWIRE04_SUITE" {
  ami = "${var.ami_id_amazonlinuxbase20200506}"
  vpc_security_group_ids = [
    "${aws_security_group.SG_GUIDEWIRE.id}",
    "${aws_security_group.SG_JENKINS_GUIDEWIRE_HEALTH_CHECK.id}",
    "${aws_security_group.SG_GUIDEWIRE_CLUSTER_NBIO_EVENT_QUEUE.id}",
    "${aws_security_group.SG_AVIATRIX_SSH.id}",
    "${aws_security_group.SG_JENKINS_CLIENT_SSH.id}"
  ]
  subnet_id = "${var.economical-dev-private-1b-subnet}"
  instance_type = "m5a.large"
  key_name = "EconomicalGuidewireNPRD"
  associate_public_ip_address = false
  iam_instance_profile = "CMS-SSMInstanceRole"
  ebs_optimized = "true"

  root_block_device {
    volume_size = "100"
  }

  lifecycle {
    prevent_destroy = false
    ignore_changes = [
      "tags.%",
      "tags.BILLINGCODE",
      "tags.BILLINGCONTACT",
      "tags.CLIENT",
      "tags.COUNTRY",
      "tags.CSCLASS",
      "tags.CSQUAL",
      "tags.CSTYPE",
      "tags.ENVIRONMENT",
      "tags.FUNCTION",
      "tags.GROUPCONTACT",
      "tags.MEMBERFIRM",
      "tags.PRIMARYCONTACT",
      "tags.SECONDARYCONTACT",
      "volume_tags.%",
      "volume_tags.AccountID",
      "volume_tags.ApplicationName",
      "volume_tags.BILLINGCODE",
      "volume_tags.BILLINGCONTACT",
      "volume_tags.CLIENT",
      "volume_tags.COUNTRY",
      "volume_tags.CSCLASS",
      "volume_tags.CSQUAL",
      "volume_tags.CSTYPE",
      "volume_tags.DataClassification",
      "volume_tags.Docker",
      "volume_tags.ENVIRONMENT",
      "volume_tags.Environment",
      "volume_tags.FUNCTION",
      "volume_tags.GROUPCONTACT",
      "volume_tags.MEMBERFIRM",
      "volume_tags.ManagedBy",
      "volume_tags.Name",
      "volume_tags.Owner",
      "volume_tags.PRIMARYCONTACT",
      "volume_tags.PatchGroup",
      "volume_tags.ResourceSeverity",
      "volume_tags.ResourceType",
      "volume_tags.Role",
      "volume_tags.SECONDARYCONTACT",
      "volume_tags.SolutionName",
      "volume_tags.VendorManaged"
    ]
  }

  volume_tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "MANAGED_SERVICES"
  }

  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "MANAGED_SERVICES"
    PatchGroup = "AmazonLinux2-AzB-Group1"
    Owner = "robhatnagar@deloitte.ca"
    ManagedBy = "CMS-TF"
    SolutionName = "InsurCloud Economical"
    Environment = "Dev"
    ApplicationName = "guidewire, docker"
    DataClassification = "PII, PHI"
    DLM-Backup = "Yes"
    DLM-Retention = "90"
    VendorManaged = "False"
    ResourceType = "Client"
    ResourceSeverity = "Normal"
    Name = "A-MC-CAC-D-A003-ECONOMICAL-PPS01-GW04-SUITE-LNX-EC2"
    Role = "Container"
  }

  provisioner "local-exec" {
    command = "echo ${aws_instance.ICECONOMICAL_PPS01_GUIDEWIRE04_SUITE.private_ip} >> dev_hosts.txt"
  }
}

output "ICECONOMICAL_PPS01_GUIDEWIRE04_SUITE_PRIVATEIP" {
  value = "${aws_instance.ICECONOMICAL_PPS01_GUIDEWIRE04_SUITE.private_ip}"
}

data "template_file" "ECO_PPS01_DOMAIN_JOIN_GW_DB" {
  template = "${file("IC-Economical-DEV/windowsdata.tpl")}"

  vars = {
    computer_name = "PPS01-GW-DB-W"
    region = "${var.region}"
    domain = "${var.ad_domain}"
    dns1 = "${var.ad_dns1}"
    dns2 = "${var.ad_dns2}"
    dns3 = "${var.ad_dns3}"
    ouselection = "${var.ou_name}"
    pwordparameter = "${var.pwordparameter}"
    unameparameter = "${var.unameparameter}"
  }
}

resource "aws_instance" "ICECONOMICAL_PPS01_GUIDEWIRE_DB" {
  ami = "${var.ami_id_windows2019base}"
  vpc_security_group_ids = [
    "${aws_security_group.SG_AVIATRIX_RDP.id}",
    "${aws_security_group.SG_MSSQL_DB.id}"
  ]
  subnet_id = "${var.economical-dev-private-1b-subnet}"
  instance_type = "r5a.large"
  key_name = "EconomicalGuidewireNPRD"
  associate_public_ip_address = false
  iam_instance_profile = "CMS-SSMInstanceRole"
  ebs_optimized = "true"

  root_block_device {
    volume_size = "50"
  }

  user_data = "${data.template_file.ECO_PPS01_DOMAIN_JOIN_GW_DB.rendered}"

  lifecycle {
    prevent_destroy = false
    ignore_changes = [
      "tags.%",
      "tags.BILLINGCODE",
      "tags.BILLINGCONTACT",
      "tags.CLIENT",
      "tags.COUNTRY",
      "tags.CSCLASS",
      "tags.CSQUAL",
      "tags.CSTYPE",
      "tags.ENVIRONMENT",
      "tags.FUNCTION",
      "tags.GROUPCONTACT",
      "tags.MEMBERFIRM",
      "tags.PRIMARYCONTACT",
      "tags.SECONDARYCONTACT",
      "volume_tags.%",
      "volume_tags.AccountID",
      "volume_tags.ApplicationName",
      "volume_tags.BILLINGCODE",
      "volume_tags.BILLINGCONTACT",
      "volume_tags.CLIENT",
      "volume_tags.COUNTRY",
      "volume_tags.CSCLASS",
      "volume_tags.CSQUAL",
      "volume_tags.CSTYPE",
      "volume_tags.DataClassification",
      "volume_tags.Docker",
      "volume_tags.ENVIRONMENT",
      "volume_tags.Environment",
      "volume_tags.FUNCTION",
      "volume_tags.GROUPCONTACT",
      "volume_tags.MEMBERFIRM",
      "volume_tags.ManagedBy",
      "volume_tags.Name",
      "volume_tags.Owner",
      "volume_tags.PRIMARYCONTACT",
      "volume_tags.PatchGroup",
      "volume_tags.ResourceSeverity",
      "volume_tags.ResourceType",
      "volume_tags.Role",
      "volume_tags.SECONDARYCONTACT",
      "volume_tags.SolutionName",
      "volume_tags.VendorManaged"
    ]
  }

  volume_tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "MANAGED_SERVICES"
  }

  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "MANAGED_SERVICES"
    PatchGroup = "Windows-AzB-Group1"
    Owner = "robhatnagar@deloitte.ca"
    ManagedBy = "CMS-TF"
    SolutionName = "InsurCloud Economical"
    Environment = "Dev"
    ApplicationName = "mssql, guidewire"
    DataClassification = "PII, PHI"
    DLM-Backup = "Yes"
    DLM-Retention = "90"
    VendorManaged = "False"
    ResourceType = "Client"
    ResourceSeverity = "Normal"
    Name = "A-MC-CAC-D-A003-ECONOMICAL-PPS01-GW-MSSQL-DB01-WIN-EC2"
  }
}

output "ICECONOMICAL_PPS01_GUIDEWIRE_DB_PRIVATEIP" {
  value = "${aws_instance.ICECONOMICAL_PPS01_GUIDEWIRE_DB.private_ip}"
}

######### MICROSERVICES ###########
resource "aws_instance" "ICECONOMICAL_PPS01_MICROSERVICES_MANAGER01" {
  ami = "${var.ami_id_amazonlinuxbase20200506}"
  vpc_security_group_ids = [
    "${aws_security_group.SG_MICROSERVICES.id}",
    "${aws_security_group.SG_MONITORING.id}",
    "${aws_security_group.SG_AVIATRIX_SSH.id}",
    "${aws_security_group.SG_JENKINS_CLIENT_SSH.id}"
  ]
  subnet_id = "${var.economical-dev-private-1a-subnet}"
  instance_type = "t3a.small"
  key_name = "EconomicalMicroservicesNPRD"
  associate_public_ip_address = false
  iam_instance_profile = "CMS-SSMInstanceRole"
  ebs_optimized = "true"

  root_block_device {
    volume_size = "16"
  }

  lifecycle {
    prevent_destroy = false
    ignore_changes = [
      "tags.%",
      "tags.BILLINGCODE",
      "tags.BILLINGCONTACT",
      "tags.CLIENT",
      "tags.COUNTRY",
      "tags.CSCLASS",
      "tags.CSQUAL",
      "tags.CSTYPE",
      "tags.ENVIRONMENT",
      "tags.FUNCTION",
      "tags.GROUPCONTACT",
      "tags.MEMBERFIRM",
      "tags.PRIMARYCONTACT",
      "tags.SECONDARYCONTACT",
      "volume_tags.%",
      "volume_tags.AccountID",
      "volume_tags.ApplicationName",
      "volume_tags.BILLINGCODE",
      "volume_tags.BILLINGCONTACT",
      "volume_tags.CLIENT",
      "volume_tags.COUNTRY",
      "volume_tags.CSCLASS",
      "volume_tags.CSQUAL",
      "volume_tags.CSTYPE",
      "volume_tags.DataClassification",
      "volume_tags.Docker",
      "volume_tags.ENVIRONMENT",
      "volume_tags.Environment",
      "volume_tags.FUNCTION",
      "volume_tags.GROUPCONTACT",
      "volume_tags.MEMBERFIRM",
      "volume_tags.ManagedBy",
      "volume_tags.Name",
      "volume_tags.Owner",
      "volume_tags.PRIMARYCONTACT",
      "volume_tags.PatchGroup",
      "volume_tags.ResourceSeverity",
      "volume_tags.ResourceType",
      "volume_tags.Role",
      "volume_tags.SECONDARYCONTACT",
      "volume_tags.SolutionName",
      "volume_tags.VendorManaged"
    ]
  }

  volume_tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "MANAGED_SERVICES"
  }

  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "MANAGED_SERVICES"
    PatchGroup = "AmazonLinux2-AzA-Group2"
    Owner = "robhatnagar@deloitte.ca"
    ManagedBy = "CMS-TF"
    SolutionName = "InsurCloud Economical"
    Environment = "Dev"
    ApplicationName = "docker, microservices"
    DataClassification = "PII, PHI"
    DLM-Backup = "Yes"
    DLM-Retention = "90"
    VendorManaged = "False"
    ResourceType = "Client"
    ResourceSeverity = "Normal"
    Name = "A-MC-CAC-D-A003-ECONOMICAL-PPS01-MS-MANAGER01-LNX-EC2"
    Role = "Container"
  }

  provisioner "local-exec" {
    command = "echo ${aws_instance.ICECONOMICAL_PPS01_MICROSERVICES_MANAGER01.private_ip} >> dev_hosts.txt"
  }
}

output "ICECONOMICAL_PPS01_MICROSERVICES_MANAGER01_PRIVATEIP" {
  value = "${aws_instance.ICECONOMICAL_PPS01_MICROSERVICES_MANAGER01.private_ip}"
}

resource "aws_instance" "ICECONOMICAL_PPS01_MICROSERVICES_MANAGER02" {
  ami = "${var.ami_id_amazonlinuxbase20200506}"
  vpc_security_group_ids = [
    "${aws_security_group.SG_MICROSERVICES.id}",
    "${aws_security_group.SG_MONITORING.id}",
    "${aws_security_group.SG_AVIATRIX_SSH.id}",
    "${aws_security_group.SG_JENKINS_CLIENT_SSH.id}"
  ]
  subnet_id = "${var.economical-dev-private-1b-subnet}"
  instance_type = "t3a.small"
  key_name = "EconomicalMicroservicesNPRD"
  associate_public_ip_address = false
  iam_instance_profile = "CMS-SSMInstanceRole"
  ebs_optimized = "true"

  root_block_device {
    volume_size = "16"
  }

  lifecycle {
    prevent_destroy = false
    ignore_changes = [
      "tags.%",
      "tags.BILLINGCODE",
      "tags.BILLINGCONTACT",
      "tags.CLIENT",
      "tags.COUNTRY",
      "tags.CSCLASS",
      "tags.CSQUAL",
      "tags.CSTYPE",
      "tags.ENVIRONMENT",
      "tags.FUNCTION",
      "tags.GROUPCONTACT",
      "tags.MEMBERFIRM",
      "tags.PRIMARYCONTACT",
      "tags.SECONDARYCONTACT",
      "volume_tags.%",
      "volume_tags.AccountID",
      "volume_tags.ApplicationName",
      "volume_tags.BILLINGCODE",
      "volume_tags.BILLINGCONTACT",
      "volume_tags.CLIENT",
      "volume_tags.COUNTRY",
      "volume_tags.CSCLASS",
      "volume_tags.CSQUAL",
      "volume_tags.CSTYPE",
      "volume_tags.DataClassification",
      "volume_tags.Docker",
      "volume_tags.ENVIRONMENT",
      "volume_tags.Environment",
      "volume_tags.FUNCTION",
      "volume_tags.GROUPCONTACT",
      "volume_tags.MEMBERFIRM",
      "volume_tags.ManagedBy",
      "volume_tags.Name",
      "volume_tags.Owner",
      "volume_tags.PRIMARYCONTACT",
      "volume_tags.PatchGroup",
      "volume_tags.ResourceSeverity",
      "volume_tags.ResourceType",
      "volume_tags.Role",
      "volume_tags.SECONDARYCONTACT",
      "volume_tags.SolutionName",
      "volume_tags.VendorManaged"
    ]
  }

  volume_tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "MANAGED_SERVICES"
  }

  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "MANAGED_SERVICES"
    PatchGroup = "AmazonLinux2-AzB-Group2"
    Owner = "robhatnagar@deloitte.ca"
    ManagedBy = "CMS-TF"
    SolutionName = "InsurCloud Economical"
    Environment = "Dev"
    ApplicationName = "docker, microservices"
    DataClassification = "PII, PHI"
    DLM-Backup = "Yes"
    DLM-Retention = "90"
    VendorManaged = "False"
    ResourceType = "Client"
    ResourceSeverity = "Normal"
    Name = "A-MC-CAC-D-A003-ECONOMICAL-PPS01-MS-MANAGER02-LNX-EC2"
    Role = "Container"
  }

  provisioner "local-exec" {
    command = "echo ${aws_instance.ICECONOMICAL_PPS01_MICROSERVICES_MANAGER02.private_ip} >> dev_hosts.txt"
  }
}

output "ICECONOMICAL_PPS01_MICROSERVICES_MANAGER02_PRIVATEIP" {
  value = "${aws_instance.ICECONOMICAL_PPS01_MICROSERVICES_MANAGER02.private_ip}"
}

resource "aws_instance" "ICECONOMICAL_PPS01_MICROSERVICES_MANAGER03" {
  ami = "${var.ami_id_amazonlinuxbase20200506}"
  vpc_security_group_ids = [
    "${aws_security_group.SG_MICROSERVICES.id}",
    "${aws_security_group.SG_MONITORING.id}",
    "${aws_security_group.SG_AVIATRIX_SSH.id}",
    "${aws_security_group.SG_JENKINS_CLIENT_SSH.id}"
  ]
  subnet_id = "${var.economical-dev-private-1b-subnet}"
  instance_type = "t3a.small"
  key_name = "EconomicalMicroservicesNPRD"
  associate_public_ip_address = false
  iam_instance_profile = "CMS-SSMInstanceRole"
  ebs_optimized = "true"

  root_block_device {
    volume_size = "16"
  }

  lifecycle {
    prevent_destroy = false
    ignore_changes = [
      "tags.%",
      "tags.BILLINGCODE",
      "tags.BILLINGCONTACT",
      "tags.CLIENT",
      "tags.COUNTRY",
      "tags.CSCLASS",
      "tags.CSQUAL",
      "tags.CSTYPE",
      "tags.ENVIRONMENT",
      "tags.FUNCTION",
      "tags.GROUPCONTACT",
      "tags.MEMBERFIRM",
      "tags.PRIMARYCONTACT",
      "tags.SECONDARYCONTACT",
      "volume_tags.%",
      "volume_tags.AccountID",
      "volume_tags.ApplicationName",
      "volume_tags.BILLINGCODE",
      "volume_tags.BILLINGCONTACT",
      "volume_tags.CLIENT",
      "volume_tags.COUNTRY",
      "volume_tags.CSCLASS",
      "volume_tags.CSQUAL",
      "volume_tags.CSTYPE",
      "volume_tags.DataClassification",
      "volume_tags.Docker",
      "volume_tags.ENVIRONMENT",
      "volume_tags.Environment",
      "volume_tags.FUNCTION",
      "volume_tags.GROUPCONTACT",
      "volume_tags.MEMBERFIRM",
      "volume_tags.ManagedBy",
      "volume_tags.Name",
      "volume_tags.Owner",
      "volume_tags.PRIMARYCONTACT",
      "volume_tags.PatchGroup",
      "volume_tags.ResourceSeverity",
      "volume_tags.ResourceType",
      "volume_tags.Role",
      "volume_tags.SECONDARYCONTACT",
      "volume_tags.SolutionName",
      "volume_tags.VendorManaged"
    ]
  }

  volume_tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "MANAGED_SERVICES"
  }

  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "MANAGED_SERVICES"
    PatchGroup = "AmazonLinux2-AzB-Group3"
    Owner = "robhatnagar@deloitte.ca"
    ManagedBy = "CMS-TF"
    SolutionName = "InsurCloud Economical"
    Environment = "Dev"
    ApplicationName = "docker, microservices"
    DataClassification = "PII, PHI"
    DLM-Backup = "Yes"
    DLM-Retention = "90"
    VendorManaged = "False"
    ResourceType = "Client"
    ResourceSeverity = "Normal"
    Name = "A-MC-CAC-D-A003-ECONOMICAL-PPS01-MS-MANAGER03-LNX-EC2"
    Role = "Container"
  }

  provisioner "local-exec" {
    command = "echo ${aws_instance.ICECONOMICAL_PPS01_MICROSERVICES_MANAGER03.private_ip} >> dev_hosts.txt"
  }
}

output "ICECONOMICAL_PPS01_MICROSERVICES_MANAGER03_PRIVATEIP" {
  value = "${aws_instance.ICECONOMICAL_PPS01_MICROSERVICES_MANAGER03.private_ip}"
}

resource "aws_instance" "ICECONOMICAL_PPS01_MICROSERVICES_APPLICATION01" {
  ami = "${var.ami_id_amazonlinuxbase20200506}"
  vpc_security_group_ids = [
    "${aws_security_group.SG_MICROSERVICES.id}",
    "${aws_security_group.SG_MONITORING.id}",
    "${aws_security_group.SG_AVIATRIX_SSH.id}",
    "${aws_security_group.SG_JENKINS_CLIENT_SSH.id}"
  ]
  subnet_id = "${var.economical-dev-private-1a-subnet}"
  instance_type = "t3a.xlarge"
  key_name = "EconomicalMicroservicesNPRD"
  associate_public_ip_address = false
  iam_instance_profile = "CMS-SSMInstanceRole"
  ebs_optimized = "true"

  root_block_device {
    volume_size = "100"
  }

  lifecycle {
    prevent_destroy = false
    ignore_changes = [
      "tags.%",
      "tags.BILLINGCODE",
      "tags.BILLINGCONTACT",
      "tags.CLIENT",
      "tags.COUNTRY",
      "tags.CSCLASS",
      "tags.CSQUAL",
      "tags.CSTYPE",
      "tags.ENVIRONMENT",
      "tags.FUNCTION",
      "tags.GROUPCONTACT",
      "tags.MEMBERFIRM",
      "tags.PRIMARYCONTACT",
      "tags.SECONDARYCONTACT",
      "volume_tags.%",
      "volume_tags.AccountID",
      "volume_tags.ApplicationName",
      "volume_tags.BILLINGCODE",
      "volume_tags.BILLINGCONTACT",
      "volume_tags.CLIENT",
      "volume_tags.COUNTRY",
      "volume_tags.CSCLASS",
      "volume_tags.CSQUAL",
      "volume_tags.CSTYPE",
      "volume_tags.DataClassification",
      "volume_tags.Docker",
      "volume_tags.ENVIRONMENT",
      "volume_tags.Environment",
      "volume_tags.FUNCTION",
      "volume_tags.GROUPCONTACT",
      "volume_tags.MEMBERFIRM",
      "volume_tags.ManagedBy",
      "volume_tags.Name",
      "volume_tags.Owner",
      "volume_tags.PRIMARYCONTACT",
      "volume_tags.PatchGroup",
      "volume_tags.ResourceSeverity",
      "volume_tags.ResourceType",
      "volume_tags.Role",
      "volume_tags.SECONDARYCONTACT",
      "volume_tags.SolutionName",
      "volume_tags.VendorManaged"
    ]
  }

  volume_tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "MANAGED_SERVICES"
  }

  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "MANAGED_SERVICES"
    PatchGroup = "AmazonLinux2-AzA-Group1"
    Owner = "robhatnagar@deloitte.ca"
    ManagedBy = "CMS-TF"
    SolutionName = "InsurCloud Economical"
    Environment = "Dev"
    ApplicationName = "docker, microservices"
    DataClassification = "PII, PHI"
    DLM-Backup = "Yes"
    DLM-Retention = "90"
    VendorManaged = "False"
    ResourceType = "Client"
    ResourceSeverity = "Normal"
    Name = "A-MC-CAC-D-A003-ECONOMICAL-PPS01-MS-APP01-LNX-EC2"
    Role = "Container"
  }

  provisioner "local-exec" {
    command = "echo ${aws_instance.ICECONOMICAL_PPS01_MICROSERVICES_APPLICATION01.private_ip} >> dev_hosts.txt"
  }
}

output "ICECONOMICAL_PPS01_MICROSERVICES_APPLICATION01_PRIVATEIP" {
  value = "${aws_instance.ICECONOMICAL_PPS01_MICROSERVICES_APPLICATION01.private_ip}"
}

resource "aws_instance" "ICECONOMICAL_PPS01_MICROSERVICES_APPLICATION02" {
  ami = "${var.ami_id_amazonlinuxbase20200506}"
  vpc_security_group_ids = [
    "${aws_security_group.SG_MICROSERVICES.id}",
    "${aws_security_group.SG_MONITORING.id}",
    "${aws_security_group.SG_AVIATRIX_SSH.id}",
    "${aws_security_group.SG_JENKINS_CLIENT_SSH.id}"
  ]
  subnet_id = "${var.economical-dev-private-1b-subnet}"
  instance_type = "t3a.xlarge"
  key_name = "EconomicalMicroservicesNPRD"
  associate_public_ip_address = false
  iam_instance_profile = "CMS-SSMInstanceRole"
  ebs_optimized = "true"

  root_block_device {
    volume_size = "100"
  }

  lifecycle {
    prevent_destroy = false
    ignore_changes = [
      "tags.%",
      "tags.BILLINGCODE",
      "tags.BILLINGCONTACT",
      "tags.CLIENT",
      "tags.COUNTRY",
      "tags.CSCLASS",
      "tags.CSQUAL",
      "tags.CSTYPE",
      "tags.ENVIRONMENT",
      "tags.FUNCTION",
      "tags.GROUPCONTACT",
      "tags.MEMBERFIRM",
      "tags.PRIMARYCONTACT",
      "tags.SECONDARYCONTACT",
      "volume_tags.%",
      "volume_tags.AccountID",
      "volume_tags.ApplicationName",
      "volume_tags.BILLINGCODE",
      "volume_tags.BILLINGCONTACT",
      "volume_tags.CLIENT",
      "volume_tags.COUNTRY",
      "volume_tags.CSCLASS",
      "volume_tags.CSQUAL",
      "volume_tags.CSTYPE",
      "volume_tags.DataClassification",
      "volume_tags.Docker",
      "volume_tags.ENVIRONMENT",
      "volume_tags.Environment",
      "volume_tags.FUNCTION",
      "volume_tags.GROUPCONTACT",
      "volume_tags.MEMBERFIRM",
      "volume_tags.ManagedBy",
      "volume_tags.Name",
      "volume_tags.Owner",
      "volume_tags.PRIMARYCONTACT",
      "volume_tags.PatchGroup",
      "volume_tags.ResourceSeverity",
      "volume_tags.ResourceType",
      "volume_tags.Role",
      "volume_tags.SECONDARYCONTACT",
      "volume_tags.SolutionName",
      "volume_tags.VendorManaged"
    ]
  }

  volume_tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "MANAGED_SERVICES"
  }

  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "MANAGED_SERVICES"
    PatchGroup = "AmazonLinux2-AzB-Group1"
    Owner = "robhatnagar@deloitte.ca"
    ManagedBy = "CMS-TF"
    SolutionName = "InsurCloud Economical"
    Environment = "Dev"
    ApplicationName = "docker, microservices"
    DataClassification = "PII, PHI"
    DLM-Backup = "Yes"
    DLM-Retention = "90"
    VendorManaged = "False"
    ResourceType = "Client"
    ResourceSeverity = "Normal"
    Name = "A-MC-CAC-D-A003-ECONOMICAL-PPS01-MS-APP02-LNX-EC2"
    Role = "Container"
  }

  provisioner "local-exec" {
    command = "echo ${aws_instance.ICECONOMICAL_PPS01_MICROSERVICES_APPLICATION02.private_ip} >> dev_hosts.txt"
  }
}

output "ICECONOMICAL_PPS01_MICROSERVICES_APPLICATION02_PRIVATEIP" {
  value = "${aws_instance.ICECONOMICAL_PPS01_MICROSERVICES_APPLICATION02.private_ip}"
}

######### RPA ###########
data "template_file" "ICECONOMICAL_PPS01_DOMAIN_JOIN_RPA_APP01" {
  template = "${file("IC-Economical-DEV/windowsdata.tpl")}"

  vars = {
    computer_name = "PPS01-RPA-A01"
    region = "${var.region}"
    domain = "${var.ad_domain}"
    dns1 = "${var.ad_dns1}"
    dns2 = "${var.ad_dns2}"
    dns3 = "${var.ad_dns3}"
    ouselection = "${var.ou_name}"
    pwordparameter = "${var.pwordparameter}"
    unameparameter = "${var.unameparameter}"
  }
}

resource "aws_instance" "ICECONOMICAL_PPS01_RPA_APP01" {
  ami = "${var.ami_id_windows2016base}"
  vpc_security_group_ids = [
    "${aws_security_group.SG_AVIATRIX_RDP.id}",
    "${aws_security_group.SG_RPA_APP.id}"
  ]
  subnet_id = "${var.economical-dev-private-1a-subnet}"
  instance_type = "t3a.medium"
  key_name = "EconomicalRPANPRD"
  associate_public_ip_address = false
  iam_instance_profile = "CMS-SSMInstanceRole"
  ebs_optimized = "true"

  root_block_device {
    volume_size = "50"
  }

  user_data = "${data.template_file.ICECONOMICAL_PPS01_DOMAIN_JOIN_RPA_APP01.rendered}"

  lifecycle {
    prevent_destroy = false
    ignore_changes = [
      "tags.%",
      "tags.BILLINGCODE",
      "tags.BILLINGCONTACT",
      "tags.CLIENT",
      "tags.COUNTRY",
      "tags.CSCLASS",
      "tags.CSQUAL",
      "tags.CSTYPE",
      "tags.ENVIRONMENT",
      "tags.FUNCTION",
      "tags.GROUPCONTACT",
      "tags.MEMBERFIRM",
      "tags.PRIMARYCONTACT",
      "tags.SECONDARYCONTACT",
      "volume_tags.%",
      "volume_tags.AccountID",
      "volume_tags.ApplicationName",
      "volume_tags.BILLINGCODE",
      "volume_tags.BILLINGCONTACT",
      "volume_tags.CLIENT",
      "volume_tags.COUNTRY",
      "volume_tags.CSCLASS",
      "volume_tags.CSQUAL",
      "volume_tags.CSTYPE",
      "volume_tags.DataClassification",
      "volume_tags.Docker",
      "volume_tags.ENVIRONMENT",
      "volume_tags.Environment",
      "volume_tags.FUNCTION",
      "volume_tags.GROUPCONTACT",
      "volume_tags.MEMBERFIRM",
      "volume_tags.ManagedBy",
      "volume_tags.Name",
      "volume_tags.Owner",
      "volume_tags.PRIMARYCONTACT",
      "volume_tags.PatchGroup",
      "volume_tags.ResourceSeverity",
      "volume_tags.ResourceType",
      "volume_tags.Role",
      "volume_tags.SECONDARYCONTACT",
      "volume_tags.SolutionName",
      "volume_tags.VendorManaged"
    ]
  }

  volume_tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "MANAGED_SERVICES"
  }

  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "MANAGED_SERVICES"
    PatchGroup = "Windows-AzA-Group1"
    Owner = "robhatnagar@deloitte.ca"
    ManagedBy = "CMS-TF"
    SolutionName = "InsurCloud Economical"
    Environment = "Dev"
    ApplicationName = "rpa, blueprism"
    DataClassification = "PII, PHI"
    DLM-Backup = "Yes"
    DLM-Retention = "90"
    VendorManaged = "False"
    ResourceType = "Client"
    ResourceSeverity = "Normal"
    Name = "A-MC-CAC-D-A003-ECONOMICAL-PPS01-RPA-APP01-WIN-EC2"
  }
}

output "ICECONOMICAL_PPS01_RPA_APP01_PRIVATEIP" {
  value = "${aws_instance.ICECONOMICAL_PPS01_RPA_APP01.private_ip}"
}

data "template_file" "ICECONOMICAL_PPS01_DOMAIN_JOIN_RPA_APP02" {
  template = "${file("IC-Economical-DEV/windowsdata.tpl")}"

  vars = {
    computer_name = "PPS01-RPA-A02"
    region = "${var.region}"
    domain = "${var.ad_domain}"
    dns1 = "${var.ad_dns1}"
    dns2 = "${var.ad_dns2}"
    dns3 = "${var.ad_dns3}"
    ouselection = "${var.ou_name}"
    pwordparameter = "${var.pwordparameter}"
    unameparameter = "${var.unameparameter}"
  }
}

resource "aws_instance" "ICECONOMICAL_PPS01_RPA_APP02" {
  ami = "${var.ami_id_windows2016base}"
  vpc_security_group_ids = [
    "${aws_security_group.SG_AVIATRIX_RDP.id}",
    "${aws_security_group.SG_RPA_APP.id}"
  ]
  subnet_id = "${var.economical-dev-private-1b-subnet}"
  instance_type = "t3a.medium"
  key_name = "EconomicalRPANPRD"
  associate_public_ip_address = false
  iam_instance_profile = "CMS-SSMInstanceRole"
  ebs_optimized = "true"

  root_block_device {
    volume_size = "50"
  }

  user_data = "${data.template_file.ICECONOMICAL_PPS01_DOMAIN_JOIN_RPA_APP02.rendered}"

  lifecycle {
    prevent_destroy = false
    ignore_changes = [
      "tags.%",
      "tags.BILLINGCODE",
      "tags.BILLINGCONTACT",
      "tags.CLIENT",
      "tags.COUNTRY",
      "tags.CSCLASS",
      "tags.CSQUAL",
      "tags.CSTYPE",
      "tags.ENVIRONMENT",
      "tags.FUNCTION",
      "tags.GROUPCONTACT",
      "tags.MEMBERFIRM",
      "tags.PRIMARYCONTACT",
      "tags.SECONDARYCONTACT",
      "volume_tags.%",
      "volume_tags.AccountID",
      "volume_tags.ApplicationName",
      "volume_tags.BILLINGCODE",
      "volume_tags.BILLINGCONTACT",
      "volume_tags.CLIENT",
      "volume_tags.COUNTRY",
      "volume_tags.CSCLASS",
      "volume_tags.CSQUAL",
      "volume_tags.CSTYPE",
      "volume_tags.DataClassification",
      "volume_tags.Docker",
      "volume_tags.ENVIRONMENT",
      "volume_tags.Environment",
      "volume_tags.FUNCTION",
      "volume_tags.GROUPCONTACT",
      "volume_tags.MEMBERFIRM",
      "volume_tags.ManagedBy",
      "volume_tags.Name",
      "volume_tags.Owner",
      "volume_tags.PRIMARYCONTACT",
      "volume_tags.PatchGroup",
      "volume_tags.ResourceSeverity",
      "volume_tags.ResourceType",
      "volume_tags.Role",
      "volume_tags.SECONDARYCONTACT",
      "volume_tags.SolutionName",
      "volume_tags.VendorManaged"
    ]
  }

  volume_tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "MANAGED_SERVICES"
  }

  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "MANAGED_SERVICES"
    PatchGroup = "Windows-AzB-Group1"
    Owner = "robhatnagar@deloitte.ca"
    ManagedBy = "CMS-TF"
    SolutionName = "InsurCloud Economical"
    Environment = "Dev"
    ApplicationName = "rpa, blueprism"
    DataClassification = "PII, PHI"
    DLM-Backup = "Yes"
    DLM-Retention = "90"
    VendorManaged = "False"
    ResourceType = "Client"
    ResourceSeverity = "Normal"
    Name = "A-MC-CAC-D-A003-ECONOMICAL-PPS01-RPA-APP02-WIN-EC2"
  }
}

output "ICECONOMICAL_PPS01_RPA_APP02_PRIVATEIP" {
  value = "${aws_instance.ICECONOMICAL_PPS01_RPA_APP02.private_ip}"
}

data "template_file" "ECO_PPS01_DOMAIN_JOIN_INT01" {
  template = "${file("IC-Economical-DEV/windowsdata.tpl")}"

  vars = {
    computer_name = "PPS01-RPA-I01"
    region = "${var.region}"
    domain = "${var.ad_domain}"
    dns1 = "${var.ad_dns1}"
    dns2 = "${var.ad_dns2}"
    dns3 = "${var.ad_dns3}"
    ouselection = "${var.ou_name}"
    pwordparameter = "${var.pwordparameter}"
    unameparameter = "${var.unameparameter}"
  }
}

resource "aws_instance" "ICECONOMICAL_PPS01_RPA_INT01" {
  ami = "${var.ami_id_windows2016base}"
  vpc_security_group_ids = [
    "${aws_security_group.SG_AVIATRIX_RDP.id}",
    "${aws_security_group.SG_RPA_APP.id}"
  ]
  subnet_id = "${var.economical-dev-private-1a-subnet}"
  instance_type = "t3a.medium"
  key_name = "EconomicalRPANPRD"
  associate_public_ip_address = false
  iam_instance_profile = "CMS-SSMInstanceRole"
  ebs_optimized = "true"

  root_block_device {
    volume_size = "50"
  }

  user_data = "${data.template_file.ECO_PPS01_DOMAIN_JOIN_INT01.rendered}"

  lifecycle {
    prevent_destroy = false
    ignore_changes = [
      "tags.%",
      "tags.BILLINGCODE",
      "tags.BILLINGCONTACT",
      "tags.CLIENT",
      "tags.COUNTRY",
      "tags.CSCLASS",
      "tags.CSQUAL",
      "tags.CSTYPE",
      "tags.ENVIRONMENT",
      "tags.FUNCTION",
      "tags.GROUPCONTACT",
      "tags.MEMBERFIRM",
      "tags.PRIMARYCONTACT",
      "tags.SECONDARYCONTACT",
      "volume_tags.%",
      "volume_tags.AccountID",
      "volume_tags.ApplicationName",
      "volume_tags.BILLINGCODE",
      "volume_tags.BILLINGCONTACT",
      "volume_tags.CLIENT",
      "volume_tags.COUNTRY",
      "volume_tags.CSCLASS",
      "volume_tags.CSQUAL",
      "volume_tags.CSTYPE",
      "volume_tags.DataClassification",
      "volume_tags.Docker",
      "volume_tags.ENVIRONMENT",
      "volume_tags.Environment",
      "volume_tags.FUNCTION",
      "volume_tags.GROUPCONTACT",
      "volume_tags.MEMBERFIRM",
      "volume_tags.ManagedBy",
      "volume_tags.Name",
      "volume_tags.Owner",
      "volume_tags.PRIMARYCONTACT",
      "volume_tags.PatchGroup",
      "volume_tags.ResourceSeverity",
      "volume_tags.ResourceType",
      "volume_tags.Role",
      "volume_tags.SECONDARYCONTACT",
      "volume_tags.SolutionName",
      "volume_tags.VendorManaged"
    ]
  }

  volume_tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "MANAGED_SERVICES"
  }

  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "MANAGED_SERVICES"
    PatchGroup = "Windows-AzA-Group1"
    Owner = "robhatnagar@deloitte.ca"
    ManagedBy = "CMS-TF"
    SolutionName = "InsurCloud Economical"
    Environment = "Dev"
    ApplicationName = "rpa, blueprism"
    DataClassification = "PII, PHI"
    DLM-Backup = "Yes"
    DLM-Retention = "90"
    VendorManaged = "False"
    ResourceType = "Client"
    ResourceSeverity = "Normal"
    Name = "A-MC-CAC-D-A003-ECONOMICAL-PPS01-RPA-INT01-WIN-EC2"
  }
}

output "ICECONOMICAL_PPS01_RPA_INT01_PRIVATEIP" {
  value = "${aws_instance.ICECONOMICAL_PPS01_RPA_INT01.private_ip}"
}

data "template_file" "ECO_PPS01_DOMAIN_JOIN_RPA_C01" {
  template = "${file("IC-Economical-DEV/windowsdata.tpl")}"

  vars = {
    computer_name = "PPS01-RPA-C01"
    region = "${var.region}"
    domain = "${var.ad_domain}"
    dns1 = "${var.ad_dns1}"
    dns2 = "${var.ad_dns2}"
    dns3 = "${var.ad_dns3}"
    ouselection = "${var.ou_name}"
    pwordparameter = "${var.pwordparameter}"
    unameparameter = "${var.unameparameter}"
  }
}

resource "aws_instance" "ICECONOMICAL_PPS01_RPA_CLIENT01" {
  ami = "${var.ami_id_windows2016base}"
  vpc_security_group_ids = [
    "${aws_security_group.SG_AVIATRIX_RDP.id}",
    "${aws_security_group.SG_RPA_CLIENT.id}"
  ]
  subnet_id = "${var.economical-dev-private-1a-subnet}"
  instance_type = "t3a.medium"
  key_name = "EconomicalRPANPRD"
  associate_public_ip_address = false
  iam_instance_profile = "CMS-SSMInstanceRole"
  ebs_optimized = "true"

  root_block_device {
    volume_size = "50"
  }

  user_data = "${data.template_file.ECO_PPS01_DOMAIN_JOIN_RPA_C01.rendered}"

  lifecycle {
    prevent_destroy = false
    ignore_changes = [
      "tags.%",
      "tags.BILLINGCODE",
      "tags.BILLINGCONTACT",
      "tags.CLIENT",
      "tags.COUNTRY",
      "tags.CSCLASS",
      "tags.CSQUAL",
      "tags.CSTYPE",
      "tags.ENVIRONMENT",
      "tags.FUNCTION",
      "tags.GROUPCONTACT",
      "tags.MEMBERFIRM",
      "tags.PRIMARYCONTACT",
      "tags.SECONDARYCONTACT",
      "volume_tags.%",
      "volume_tags.AccountID",
      "volume_tags.ApplicationName",
      "volume_tags.BILLINGCODE",
      "volume_tags.BILLINGCONTACT",
      "volume_tags.CLIENT",
      "volume_tags.COUNTRY",
      "volume_tags.CSCLASS",
      "volume_tags.CSQUAL",
      "volume_tags.CSTYPE",
      "volume_tags.DataClassification",
      "volume_tags.Docker",
      "volume_tags.ENVIRONMENT",
      "volume_tags.Environment",
      "volume_tags.FUNCTION",
      "volume_tags.GROUPCONTACT",
      "volume_tags.MEMBERFIRM",
      "volume_tags.ManagedBy",
      "volume_tags.Name",
      "volume_tags.Owner",
      "volume_tags.PRIMARYCONTACT",
      "volume_tags.PatchGroup",
      "volume_tags.ResourceSeverity",
      "volume_tags.ResourceType",
      "volume_tags.Role",
      "volume_tags.SECONDARYCONTACT",
      "volume_tags.SolutionName",
      "volume_tags.VendorManaged"
    ]
  }

  volume_tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "MANAGED_SERVICES"
  }

  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "MANAGED_SERVICES"
    PatchGroup = "Windows-AzA-Group1"
    Owner = "robhatnagar@deloitte.ca"
    ManagedBy = "CMS-TF"
    SolutionName = "InsurCloud Economical"
    Environment = "Dev"
    ApplicationName = "rpa, blueprism"
    DataClassification = "PII, PHI"
    DLM-Backup = "Yes"
    DLM-Retention = "90"
    VendorManaged = "False"
    ResourceType = "Client"
    ResourceSeverity = "Normal"
    Name = "A-MC-CAC-D-A003-ECONOMICAL-PPS01-RPA-CLIENT01-WIN-EC2"
  }
}

output "ICECONOMICAL_PPS01_RPA_CLIENT01_PRIVATEIP" {
  value = "${aws_instance.ICECONOMICAL_PPS01_RPA_CLIENT01.private_ip}"
}

data "template_file" "ECO_PPS01_DOMAIN_JOIN_RPA_C02" {
  template = "${file("IC-Economical-DEV/windowsdata.tpl")}"

  vars = {
    computer_name = "PPS01-RPA-C02"
    region = "${var.region}"
    domain = "${var.ad_domain}"
    dns1 = "${var.ad_dns1}"
    dns2 = "${var.ad_dns2}"
    dns3 = "${var.ad_dns3}"
    ouselection = "${var.ou_name}"
    pwordparameter = "${var.pwordparameter}"
    unameparameter = "${var.unameparameter}"
  }
}

resource "aws_instance" "ICECONOMICAL_PPS01_RPA_CLIENT02" {
  ami = "${var.ami_id_windows2016base}"
  vpc_security_group_ids = [
    "${aws_security_group.SG_AVIATRIX_RDP.id}",
    "${aws_security_group.SG_RPA_CLIENT.id}"
  ]
  subnet_id = "${var.economical-dev-private-1b-subnet}"
  instance_type = "t3a.medium"
  key_name = "EconomicalRPANPRD"
  associate_public_ip_address = false
  iam_instance_profile = "CMS-SSMInstanceRole"
  ebs_optimized = "true"

  root_block_device {
    volume_size = "50"
  }

  user_data = "${data.template_file.ECO_PPS01_DOMAIN_JOIN_RPA_C02.rendered}"

  lifecycle {
    prevent_destroy = false
    ignore_changes = [
      "tags.%",
      "tags.BILLINGCODE",
      "tags.BILLINGCONTACT",
      "tags.CLIENT",
      "tags.COUNTRY",
      "tags.CSCLASS",
      "tags.CSQUAL",
      "tags.CSTYPE",
      "tags.ENVIRONMENT",
      "tags.FUNCTION",
      "tags.GROUPCONTACT",
      "tags.MEMBERFIRM",
      "tags.PRIMARYCONTACT",
      "tags.SECONDARYCONTACT",
      "volume_tags.%",
      "volume_tags.AccountID",
      "volume_tags.ApplicationName",
      "volume_tags.BILLINGCODE",
      "volume_tags.BILLINGCONTACT",
      "volume_tags.CLIENT",
      "volume_tags.COUNTRY",
      "volume_tags.CSCLASS",
      "volume_tags.CSQUAL",
      "volume_tags.CSTYPE",
      "volume_tags.DataClassification",
      "volume_tags.Docker",
      "volume_tags.ENVIRONMENT",
      "volume_tags.Environment",
      "volume_tags.FUNCTION",
      "volume_tags.GROUPCONTACT",
      "volume_tags.MEMBERFIRM",
      "volume_tags.ManagedBy",
      "volume_tags.Name",
      "volume_tags.Owner",
      "volume_tags.PRIMARYCONTACT",
      "volume_tags.PatchGroup",
      "volume_tags.ResourceSeverity",
      "volume_tags.ResourceType",
      "volume_tags.Role",
      "volume_tags.SECONDARYCONTACT",
      "volume_tags.SolutionName",
      "volume_tags.VendorManaged"
    ]
  }

  volume_tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "MANAGED_SERVICES"
  }

  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "MANAGED_SERVICES"
    PatchGroup = "Windows-AzB-Group1"
    Owner = "robhatnagar@deloitte.ca"
    ManagedBy = "CMS-TF"
    SolutionName = "InsurCloud Economical"
    Environment = "Dev"
    ApplicationName = "rpa, blueprism"
    DataClassification = "PII, PHI"
    DLM-Backup = "Yes"
    DLM-Retention = "90"
    VendorManaged = "False"
    ResourceType = "Client"
    ResourceSeverity = "Normal"
    Name = "A-MC-CAC-D-A003-ECONOMICAL-PPS01-RPA-CLIENT02-WIN-EC2"
  }
}

output "ICECONOMICAL_PPS01_RPA_CLIENT02_PRIVATEIP" {
  value = "${aws_instance.ICECONOMICAL_PPS01_RPA_CLIENT02.private_ip}"
}

data "template_file" "ECO_PPS01_DOMAIN_JOIN_RPA_C03" {
  template = "${file("IC-Economical-DEV/windowsdata.tpl")}"

  vars = {
    computer_name = "PPS01-RPA-C03"
    region = "${var.region}"
    domain = "${var.ad_domain}"
    dns1 = "${var.ad_dns1}"
    dns2 = "${var.ad_dns2}"
    dns3 = "${var.ad_dns3}"
    ouselection = "${var.ou_name}"
    pwordparameter = "${var.pwordparameter}"
    unameparameter = "${var.unameparameter}"
  }
}

resource "aws_instance" "ICECONOMICAL_PPS01_RPA_CLIENT03" {
  ami = "${var.ami_id_windows2016base}"
  vpc_security_group_ids = [
    "${aws_security_group.SG_AVIATRIX_RDP.id}",
    "${aws_security_group.SG_RPA_CLIENT.id}"
  ]
  subnet_id = "${var.economical-dev-private-1b-subnet}"
  instance_type = "t3a.medium"
  key_name = "EconomicalRPANPRD"
  associate_public_ip_address = false
  iam_instance_profile = "CMS-SSMInstanceRole"
  ebs_optimized = "true"

  root_block_device {
    volume_size = "50"
  }

  user_data = "${data.template_file.ECO_PPS01_DOMAIN_JOIN_RPA_C03.rendered}"

  lifecycle {
    prevent_destroy = false
    ignore_changes = [
      "tags.%",
      "tags.BILLINGCODE",
      "tags.BILLINGCONTACT",
      "tags.CLIENT",
      "tags.COUNTRY",
      "tags.CSCLASS",
      "tags.CSQUAL",
      "tags.CSTYPE",
      "tags.ENVIRONMENT",
      "tags.FUNCTION",
      "tags.GROUPCONTACT",
      "tags.MEMBERFIRM",
      "tags.PRIMARYCONTACT",
      "tags.SECONDARYCONTACT",
      "volume_tags.%",
      "volume_tags.AccountID",
      "volume_tags.ApplicationName",
      "volume_tags.BILLINGCODE",
      "volume_tags.BILLINGCONTACT",
      "volume_tags.CLIENT",
      "volume_tags.COUNTRY",
      "volume_tags.CSCLASS",
      "volume_tags.CSQUAL",
      "volume_tags.CSTYPE",
      "volume_tags.DataClassification",
      "volume_tags.Docker",
      "volume_tags.ENVIRONMENT",
      "volume_tags.Environment",
      "volume_tags.FUNCTION",
      "volume_tags.GROUPCONTACT",
      "volume_tags.MEMBERFIRM",
      "volume_tags.ManagedBy",
      "volume_tags.Name",
      "volume_tags.Owner",
      "volume_tags.PRIMARYCONTACT",
      "volume_tags.PatchGroup",
      "volume_tags.ResourceSeverity",
      "volume_tags.ResourceType",
      "volume_tags.Role",
      "volume_tags.SECONDARYCONTACT",
      "volume_tags.SolutionName",
      "volume_tags.VendorManaged"
    ]
  }

  volume_tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "MANAGED_SERVICES"
  }

  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "MANAGED_SERVICES"
    PatchGroup = "Windows-AzA-Group1"
    Owner = "robhatnagar@deloitte.ca"
    ManagedBy = "CMS-TF"
    SolutionName = "InsurCloud Economical"
    Environment = "Dev"
    ApplicationName = "rpa, blueprism"
    DataClassification = "PII, PHI"
    DLM-Backup = "Yes"
    DLM-Retention = "90"
    VendorManaged = "False"
    ResourceType = "Client"
    ResourceSeverity = "Normal"
    Name = "A-MC-CAC-D-A003-ECONOMICAL-PPS01-RPA-CLIENT03-WIN-EC2"
  }
}

output "ICECONOMICAL_PPS01_RPA_CLIENT03_PRIVATEIP" {
  value = "${aws_instance.ICECONOMICAL_PPS01_RPA_CLIENT03.private_ip}"
}

data "template_file" "ECO_PPS01_DOMAIN_JOIN_RPA_C04" {
  template = "${file("IC-Economical-DEV/windowsdata.tpl")}"

  vars = {
    computer_name = "PPS01-RPA-C04"
    region = "${var.region}"
    domain = "${var.ad_domain}"
    dns1 = "${var.ad_dns1}"
    dns2 = "${var.ad_dns2}"
    dns3 = "${var.ad_dns3}"
    ouselection = "${var.ou_name}"
    pwordparameter = "${var.pwordparameter}"
    unameparameter = "${var.unameparameter}"
  }
}

resource "aws_instance" "ICECONOMICAL_PPS01_RPA_CLIENT04" {
  ami = "${var.ami_id_windows2016base}"
  vpc_security_group_ids = [
    "${aws_security_group.SG_AVIATRIX_RDP.id}",
    "${aws_security_group.SG_RPA_CLIENT.id}"
  ]
  subnet_id = "${var.economical-dev-private-1b-subnet}"
  instance_type = "t3a.medium"
  key_name = "EconomicalRPANPRD"
  associate_public_ip_address = false
  iam_instance_profile = "CMS-SSMInstanceRole"
  ebs_optimized = "true"

  root_block_device {
    volume_size = "50"
  }

  user_data = "${data.template_file.ECO_PPS01_DOMAIN_JOIN_RPA_C04.rendered}"

  lifecycle {
    prevent_destroy = false
    ignore_changes = [
      "tags.%",
      "tags.BILLINGCODE",
      "tags.BILLINGCONTACT",
      "tags.CLIENT",
      "tags.COUNTRY",
      "tags.CSCLASS",
      "tags.CSQUAL",
      "tags.CSTYPE",
      "tags.ENVIRONMENT",
      "tags.FUNCTION",
      "tags.GROUPCONTACT",
      "tags.MEMBERFIRM",
      "tags.PRIMARYCONTACT",
      "tags.SECONDARYCONTACT",
      "volume_tags.%",
      "volume_tags.AccountID",
      "volume_tags.ApplicationName",
      "volume_tags.BILLINGCODE",
      "volume_tags.BILLINGCONTACT",
      "volume_tags.CLIENT",
      "volume_tags.COUNTRY",
      "volume_tags.CSCLASS",
      "volume_tags.CSQUAL",
      "volume_tags.CSTYPE",
      "volume_tags.DataClassification",
      "volume_tags.Docker",
      "volume_tags.ENVIRONMENT",
      "volume_tags.Environment",
      "volume_tags.FUNCTION",
      "volume_tags.GROUPCONTACT",
      "volume_tags.MEMBERFIRM",
      "volume_tags.ManagedBy",
      "volume_tags.Name",
      "volume_tags.Owner",
      "volume_tags.PRIMARYCONTACT",
      "volume_tags.PatchGroup",
      "volume_tags.ResourceSeverity",
      "volume_tags.ResourceType",
      "volume_tags.Role",
      "volume_tags.SECONDARYCONTACT",
      "volume_tags.SolutionName",
      "volume_tags.VendorManaged"
    ]
  }

  volume_tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "MANAGED_SERVICES"
  }

  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "MANAGED_SERVICES"
    PatchGroup = "Windows-AzB-Group1"
    Owner = "robhatnagar@deloitte.ca"
    ManagedBy = "CMS-TF"
    SolutionName = "InsurCloud Economical"
    Environment = "Dev"
    ApplicationName = "rpa, blueprism"
    DataClassification = "PII, PHI"
    DLM-Backup = "Yes"
    DLM-Retention = "90"
    VendorManaged = "False"
    ResourceType = "Client"
    ResourceSeverity = "Normal"
    Name = "A-MC-CAC-D-A003-ECONOMICAL-PPS01-RPA-CLIENT04-WIN-EC2"
  }
}

output "ICECONOMICAL_PPS01_RPA_CLIENT04_PRIVATEIP" {
  value = "${aws_instance.ICECONOMICAL_PPS01_RPA_CLIENT04.private_ip}"
}

data "template_file" "ECO_PPS01_DOMAIN_JOIN_RPA_C05" {
  template = "${file("IC-Economical-DEV/windowsdata.tpl")}"

  vars = {
    computer_name = "PPS01-RPA-C05"
    region = "${var.region}"
    domain = "${var.ad_domain}"
    dns1 = "${var.ad_dns1}"
    dns2 = "${var.ad_dns2}"
    dns3 = "${var.ad_dns3}"
    ouselection = "${var.ou_name}"
    pwordparameter = "${var.pwordparameter}"
    unameparameter = "${var.unameparameter}"
  }
}

resource "aws_instance" "ICECONOMICAL_PPS01_RPA_CLIENT05" {
  ami = "${var.ami_id_windows2016base}"
  vpc_security_group_ids = [
    "${aws_security_group.SG_AVIATRIX_RDP.id}",
    "${aws_security_group.SG_RPA_CLIENT.id}"
  ]
  subnet_id = "${var.economical-dev-private-1b-subnet}"
  instance_type = "t3a.medium"
  key_name = "EconomicalRPANPRD"
  associate_public_ip_address = false
  iam_instance_profile = "CMS-SSMInstanceRole"
  ebs_optimized = "true"

  root_block_device {
    volume_size = "50"
  }

  user_data = "${data.template_file.ECO_PPS01_DOMAIN_JOIN_RPA_C05.rendered}"

  lifecycle {
    prevent_destroy = false
    ignore_changes = [
      "tags.%",
      "tags.BILLINGCODE",
      "tags.BILLINGCONTACT",
      "tags.CLIENT",
      "tags.COUNTRY",
      "tags.CSCLASS",
      "tags.CSQUAL",
      "tags.CSTYPE",
      "tags.ENVIRONMENT",
      "tags.FUNCTION",
      "tags.GROUPCONTACT",
      "tags.MEMBERFIRM",
      "tags.PRIMARYCONTACT",
      "tags.SECONDARYCONTACT",
      "volume_tags.%",
      "volume_tags.AccountID",
      "volume_tags.ApplicationName",
      "volume_tags.BILLINGCODE",
      "volume_tags.BILLINGCONTACT",
      "volume_tags.CLIENT",
      "volume_tags.COUNTRY",
      "volume_tags.CSCLASS",
      "volume_tags.CSQUAL",
      "volume_tags.CSTYPE",
      "volume_tags.DataClassification",
      "volume_tags.Docker",
      "volume_tags.ENVIRONMENT",
      "volume_tags.Environment",
      "volume_tags.FUNCTION",
      "volume_tags.GROUPCONTACT",
      "volume_tags.MEMBERFIRM",
      "volume_tags.ManagedBy",
      "volume_tags.Name",
      "volume_tags.Owner",
      "volume_tags.PRIMARYCONTACT",
      "volume_tags.PatchGroup",
      "volume_tags.ResourceSeverity",
      "volume_tags.ResourceType",
      "volume_tags.Role",
      "volume_tags.SECONDARYCONTACT",
      "volume_tags.SolutionName",
      "volume_tags.VendorManaged"
    ]
  }

  volume_tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "MANAGED_SERVICES"
  }

  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "MANAGED_SERVICES"
    PatchGroup = "Windows-AzA-Group1"
    Owner = "robhatnagar@deloitte.ca"
    ManagedBy = "CMS-TF"
    SolutionName = "InsurCloud Economical"
    Environment = "Dev"
    ApplicationName = "rpa, blueprism"
    DataClassification = "PII, PHI"
    DLM-Backup = "Yes"
    DLM-Retention = "90"
    VendorManaged = "False"
    ResourceType = "Client"
    ResourceSeverity = "Normal"
    Name = "A-MC-CAC-D-A003-ECONOMICAL-PPS01-RPA-CLIENT05-WIN-EC2"
  }
}

output "ICECONOMICAL_PPS01_RPA_CLIENT05_PRIVATEIP" {
  value = "${aws_instance.ICECONOMICAL_PPS01_RPA_CLIENT05.private_ip}"
}

data "template_file" "ECO_PPS01_DOMAIN_JOIN_RPA_C06" {
  template = "${file("IC-Economical-DEV/windowsdata.tpl")}"

  vars = {
    computer_name = "PPS01-RPA-C06"
    region = "${var.region}"
    domain = "${var.ad_domain}"
    dns1 = "${var.ad_dns1}"
    dns2 = "${var.ad_dns2}"
    dns3 = "${var.ad_dns3}"
    ouselection = "${var.ou_name}"
    pwordparameter = "${var.pwordparameter}"
    unameparameter = "${var.unameparameter}"
  }
}

resource "aws_instance" "ICECONOMICAL_PPS01_RPA_CLIENT06" {
  ami = "${var.ami_id_windows2016base}"
  vpc_security_group_ids = [
    "${aws_security_group.SG_AVIATRIX_RDP.id}",
    "${aws_security_group.SG_RPA_CLIENT.id}"
  ]
  subnet_id = "${var.economical-dev-private-1b-subnet}"
  instance_type = "t3a.medium"
  key_name = "EconomicalRPANPRD"
  associate_public_ip_address = false
  iam_instance_profile = "CMS-SSMInstanceRole"
  ebs_optimized = "true"

  root_block_device {
    volume_size = "50"
  }

  user_data = "${data.template_file.ECO_PPS01_DOMAIN_JOIN_RPA_C06.rendered}"

  lifecycle {
    prevent_destroy = false
    ignore_changes = [
      "tags.%",
      "tags.BILLINGCODE",
      "tags.BILLINGCONTACT",
      "tags.CLIENT",
      "tags.COUNTRY",
      "tags.CSCLASS",
      "tags.CSQUAL",
      "tags.CSTYPE",
      "tags.ENVIRONMENT",
      "tags.FUNCTION",
      "tags.GROUPCONTACT",
      "tags.MEMBERFIRM",
      "tags.PRIMARYCONTACT",
      "tags.SECONDARYCONTACT",
      "volume_tags.%",
      "volume_tags.AccountID",
      "volume_tags.ApplicationName",
      "volume_tags.BILLINGCODE",
      "volume_tags.BILLINGCONTACT",
      "volume_tags.CLIENT",
      "volume_tags.COUNTRY",
      "volume_tags.CSCLASS",
      "volume_tags.CSQUAL",
      "volume_tags.CSTYPE",
      "volume_tags.DataClassification",
      "volume_tags.Docker",
      "volume_tags.ENVIRONMENT",
      "volume_tags.Environment",
      "volume_tags.FUNCTION",
      "volume_tags.GROUPCONTACT",
      "volume_tags.MEMBERFIRM",
      "volume_tags.ManagedBy",
      "volume_tags.Name",
      "volume_tags.Owner",
      "volume_tags.PRIMARYCONTACT",
      "volume_tags.PatchGroup",
      "volume_tags.ResourceSeverity",
      "volume_tags.ResourceType",
      "volume_tags.Role",
      "volume_tags.SECONDARYCONTACT",
      "volume_tags.SolutionName",
      "volume_tags.VendorManaged"
    ]
  }

  volume_tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "MANAGED_SERVICES"
  }

  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "MANAGED_SERVICES"
    PatchGroup = "Windows-AzB-Group1"
    Owner = "robhatnagar@deloitte.ca"
    ManagedBy = "CMS-TF"
    SolutionName = "InsurCloud Economical"
    Environment = "Dev"
    ApplicationName = "rpa, blueprism"
    DataClassification = "PII, PHI"
    DLM-Backup = "Yes"
    DLM-Retention = "90"
    VendorManaged = "False"
    ResourceType = "Client"
    ResourceSeverity = "Normal"
    Name = "A-MC-CAC-D-A003-ECONOMICAL-PPS01-RPA-CLIENT06-WIN-EC2"
  }
}

output "ICECONOMICAL_PPS01_RPA_CLIENT06_PRIVATEIP" {
  value = "${aws_instance.ICECONOMICAL_PPS01_RPA_CLIENT06.private_ip}"
}

data "template_file" "ECO_PPS01_DOMAIN_JOIN_RPA_C07" {
  template = "${file("IC-Economical-DEV/windowsdata.tpl")}"

  vars = {
    computer_name = "PPS01-RPA-C07"
    region = "${var.region}"
    domain = "${var.ad_domain}"
    dns1 = "${var.ad_dns1}"
    dns2 = "${var.ad_dns2}"
    dns3 = "${var.ad_dns3}"
    ouselection = "${var.ou_name}"
    pwordparameter = "${var.pwordparameter}"
    unameparameter = "${var.unameparameter}"
  }
}

resource "aws_instance" "ICECONOMICAL_PPS01_RPA_CLIENT07" {
  ami = "${var.ami_id_windows2016base}"
  vpc_security_group_ids = [
    "${aws_security_group.SG_AVIATRIX_RDP.id}",
    "${aws_security_group.SG_RPA_CLIENT.id}"
  ]
  subnet_id = "${var.economical-dev-private-1b-subnet}"
  instance_type = "t3a.medium"
  key_name = "EconomicalRPANPRD"
  associate_public_ip_address = false
  iam_instance_profile = "CMS-SSMInstanceRole"
  ebs_optimized = "true"

  root_block_device {
    volume_size = "50"
  }

  user_data = "${data.template_file.ECO_PPS01_DOMAIN_JOIN_RPA_C07.rendered}"

  lifecycle {
    prevent_destroy = false
    ignore_changes = [
      "tags.%",
      "tags.BILLINGCODE",
      "tags.BILLINGCONTACT",
      "tags.CLIENT",
      "tags.COUNTRY",
      "tags.CSCLASS",
      "tags.CSQUAL",
      "tags.CSTYPE",
      "tags.ENVIRONMENT",
      "tags.FUNCTION",
      "tags.GROUPCONTACT",
      "tags.MEMBERFIRM",
      "tags.PRIMARYCONTACT",
      "tags.SECONDARYCONTACT",
      "volume_tags.%",
      "volume_tags.AccountID",
      "volume_tags.ApplicationName",
      "volume_tags.BILLINGCODE",
      "volume_tags.BILLINGCONTACT",
      "volume_tags.CLIENT",
      "volume_tags.COUNTRY",
      "volume_tags.CSCLASS",
      "volume_tags.CSQUAL",
      "volume_tags.CSTYPE",
      "volume_tags.DataClassification",
      "volume_tags.Docker",
      "volume_tags.ENVIRONMENT",
      "volume_tags.Environment",
      "volume_tags.FUNCTION",
      "volume_tags.GROUPCONTACT",
      "volume_tags.MEMBERFIRM",
      "volume_tags.ManagedBy",
      "volume_tags.Name",
      "volume_tags.Owner",
      "volume_tags.PRIMARYCONTACT",
      "volume_tags.PatchGroup",
      "volume_tags.ResourceSeverity",
      "volume_tags.ResourceType",
      "volume_tags.Role",
      "volume_tags.SECONDARYCONTACT",
      "volume_tags.SolutionName",
      "volume_tags.VendorManaged"
    ]
  }

  volume_tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "MANAGED_SERVICES"
  }

  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "MANAGED_SERVICES"
    PatchGroup = "Windows-AzA-Group1"
    Owner = "robhatnagar@deloitte.ca"
    ManagedBy = "CMS-TF"
    SolutionName = "InsurCloud Economical"
    Environment = "Dev"
    ApplicationName = "rpa, blueprism"
    DataClassification = "PII, PHI"
    DLM-Backup = "Yes"
    DLM-Retention = "90"
    VendorManaged = "False"
    ResourceType = "Client"
    ResourceSeverity = "Normal"
    Name = "A-MC-CAC-D-A003-ECONOMICAL-PPS01-RPA-CLIENT07-WIN-EC2"
  }
}

output "ICECONOMICAL_PPS01_RPA_CLIENT07_PRIVATEIP" {
  value = "${aws_instance.ICECONOMICAL_PPS01_RPA_CLIENT07.private_ip}"
}

data "template_file" "ECO_PPS01_DOMAIN_JOIN_RPA_C08" {
  template = "${file("IC-Economical-DEV/windowsdata.tpl")}"

  vars = {
    computer_name = "PPS01-RPA-C08"
    region = "${var.region}"
    domain = "${var.ad_domain}"
    dns1 = "${var.ad_dns1}"
    dns2 = "${var.ad_dns2}"
    dns3 = "${var.ad_dns3}"
    ouselection = "${var.ou_name}"
    pwordparameter = "${var.pwordparameter}"
    unameparameter = "${var.unameparameter}"
  }
}

resource "aws_instance" "ICECONOMICAL_PPS01_RPA_CLIENT08" {
  ami = "${var.ami_id_windows2016base}"
  vpc_security_group_ids = [
    "${aws_security_group.SG_AVIATRIX_RDP.id}",
    "${aws_security_group.SG_RPA_CLIENT.id}"
  ]
  subnet_id = "${var.economical-dev-private-1b-subnet}"
  instance_type = "t3a.medium"
  key_name = "EconomicalRPANPRD"
  associate_public_ip_address = false
  iam_instance_profile = "CMS-SSMInstanceRole"
  ebs_optimized = "true"

  root_block_device {
    volume_size = "50"
  }

  user_data = "${data.template_file.ECO_PPS01_DOMAIN_JOIN_RPA_C08.rendered}"

  lifecycle {
    prevent_destroy = false
    ignore_changes = [
      "tags.%",
      "tags.BILLINGCODE",
      "tags.BILLINGCONTACT",
      "tags.CLIENT",
      "tags.COUNTRY",
      "tags.CSCLASS",
      "tags.CSQUAL",
      "tags.CSTYPE",
      "tags.ENVIRONMENT",
      "tags.FUNCTION",
      "tags.GROUPCONTACT",
      "tags.MEMBERFIRM",
      "tags.PRIMARYCONTACT",
      "tags.SECONDARYCONTACT",
      "volume_tags.%",
      "volume_tags.AccountID",
      "volume_tags.ApplicationName",
      "volume_tags.BILLINGCODE",
      "volume_tags.BILLINGCONTACT",
      "volume_tags.CLIENT",
      "volume_tags.COUNTRY",
      "volume_tags.CSCLASS",
      "volume_tags.CSQUAL",
      "volume_tags.CSTYPE",
      "volume_tags.DataClassification",
      "volume_tags.Docker",
      "volume_tags.ENVIRONMENT",
      "volume_tags.Environment",
      "volume_tags.FUNCTION",
      "volume_tags.GROUPCONTACT",
      "volume_tags.MEMBERFIRM",
      "volume_tags.ManagedBy",
      "volume_tags.Name",
      "volume_tags.Owner",
      "volume_tags.PRIMARYCONTACT",
      "volume_tags.PatchGroup",
      "volume_tags.ResourceSeverity",
      "volume_tags.ResourceType",
      "volume_tags.Role",
      "volume_tags.SECONDARYCONTACT",
      "volume_tags.SolutionName",
      "volume_tags.VendorManaged"
    ]
  }

  volume_tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "MANAGED_SERVICES"
  }

  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "MANAGED_SERVICES"
    PatchGroup = "Windows-AzB-Group1"
    Owner = "robhatnagar@deloitte.ca"
    ManagedBy = "CMS-TF"
    SolutionName = "InsurCloud Economical"
    Environment = "Dev"
    ApplicationName = "rpa, blueprism"
    DataClassification = "PII, PHI"
    DLM-Backup = "Yes"
    DLM-Retention = "90"
    VendorManaged = "False"
    ResourceType = "Client"
    ResourceSeverity = "Normal"
    Name = "A-MC-CAC-D-A003-ECONOMICAL-PPS01-RPA-CLIENT08-WIN-EC2"
  }
}

output "ICECONOMICAL_PPS01_RPA_CLIENT08_PRIVATEIP" {
  value = "${aws_instance.ICECONOMICAL_PPS01_RPA_CLIENT08.private_ip}"
}

data "template_file" "ECO_PPS01_DOMAIN_JOIN_RPA_C09" {
  template = "${file("IC-Economical-DEV/windowsdata.tpl")}"

  vars = {
    computer_name = "PPS01-RPA-C09"
    region = "${var.region}"
    domain = "${var.ad_domain}"
    dns1 = "${var.ad_dns1}"
    dns2 = "${var.ad_dns2}"
    dns3 = "${var.ad_dns3}"
    ouselection = "${var.ou_name}"
    pwordparameter = "${var.pwordparameter}"
    unameparameter = "${var.unameparameter}"
  }
}

######### HAPROXY ##############
resource "aws_instance" "ICECONOMICAL_PPS01_INTERNAL_HAPROXY01" {
  ami = "${var.ami_id_amazonlinuxbase20200506}"
  vpc_security_group_ids = [
    "${aws_security_group.SG_INTERNAL_HAPROXY.id}",
    "${aws_security_group.SG_AVIATRIX_SSH.id}",
    "${aws_security_group.SG_JENKINS_CLIENT_SSH.id}"
  ]
  subnet_id = "${var.economical-dev-private-1a-subnet}"
  instance_type = "t3a.small"
  key_name = "EconomicalHAProxyNPRD"
  associate_public_ip_address = false
  iam_instance_profile = "CMS-SSMInstanceRole"
  ebs_optimized = "true"

  root_block_device {
    volume_size = "40"
  }

  lifecycle {
    prevent_destroy = false
    ignore_changes = [
      "tags.%",
      "tags.BILLINGCODE",
      "tags.BILLINGCONTACT",
      "tags.CLIENT",
      "tags.COUNTRY",
      "tags.CSCLASS",
      "tags.CSQUAL",
      "tags.CSTYPE",
      "tags.ENVIRONMENT",
      "tags.FUNCTION",
      "tags.GROUPCONTACT",
      "tags.MEMBERFIRM",
      "tags.PRIMARYCONTACT",
      "tags.SECONDARYCONTACT",
      "volume_tags.%",
      "volume_tags.AccountID",
      "volume_tags.ApplicationName",
      "volume_tags.BILLINGCODE",
      "volume_tags.BILLINGCONTACT",
      "volume_tags.CLIENT",
      "volume_tags.COUNTRY",
      "volume_tags.CSCLASS",
      "volume_tags.CSQUAL",
      "volume_tags.CSTYPE",
      "volume_tags.DataClassification",
      "volume_tags.Docker",
      "volume_tags.ENVIRONMENT",
      "volume_tags.Environment",
      "volume_tags.FUNCTION",
      "volume_tags.GROUPCONTACT",
      "volume_tags.MEMBERFIRM",
      "volume_tags.ManagedBy",
      "volume_tags.Name",
      "volume_tags.Owner",
      "volume_tags.PRIMARYCONTACT",
      "volume_tags.PatchGroup",
      "volume_tags.ResourceSeverity",
      "volume_tags.ResourceType",
      "volume_tags.Role",
      "volume_tags.SECONDARYCONTACT",
      "volume_tags.SolutionName",
      "volume_tags.VendorManaged"
    ]
  }

  volume_tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "MANAGED_SERVICES"
  }

  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "MANAGED_SERVICES"
    PatchGroup = "AmazonLinux2-AzA-Group1"
    Owner = "robhatnagar@deloitte.ca"
    ManagedBy = "CMS-TF"
    SolutionName = "InsurCloud Economical"
    Environment = "Dev"
    ApplicationName = "docker, haproxy"
    DataClassification = "PII, PHI"
    DLM-Backup = "Yes"
    DLM-Retention = "90"
    VendorManaged = "False"
    ResourceType = "Client"
    ResourceSeverity = "Normal"
    Name = "A-MC-CAC-D-A003-ECONOMICAL-PPS01-INTERNAL-HAPROXY01-EC2"
    Role = "Container"
  }

  provisioner "local-exec" {
    command = "echo ${aws_instance.ICECONOMICAL_PPS01_INTERNAL_HAPROXY01.private_ip} >> dev_hosts.txt"
  }
}

output "ICECONOMICAL_PPS01_INTERNAL_HAPROXY_PRIVATEIP01" {
  value = "${aws_instance.ICECONOMICAL_PPS01_INTERNAL_HAPROXY01.private_ip}"
}

resource "aws_instance" "ICECONOMICAL_PPS01_INTERNAL_HAPROXY02" {
  ami = "${var.ami_id_amazonlinuxbase20200506}"
  vpc_security_group_ids = [
    "${aws_security_group.SG_INTERNAL_HAPROXY.id}",
    "${aws_security_group.SG_AVIATRIX_SSH.id}",
    "${aws_security_group.SG_JENKINS_CLIENT_SSH.id}"
  ]
  subnet_id = "${var.economical-dev-private-1b-subnet}"
  instance_type = "t3a.small"
  key_name = "EconomicalHAProxyNPRD"
  associate_public_ip_address = false
  iam_instance_profile = "CMS-SSMInstanceRole"
  ebs_optimized = "true"

  root_block_device {
    volume_size = "40"
  }

  lifecycle {
    prevent_destroy = false
    ignore_changes = [
      "tags.%",
      "tags.BILLINGCODE",
      "tags.BILLINGCONTACT",
      "tags.CLIENT",
      "tags.COUNTRY",
      "tags.CSCLASS",
      "tags.CSQUAL",
      "tags.CSTYPE",
      "tags.ENVIRONMENT",
      "tags.FUNCTION",
      "tags.GROUPCONTACT",
      "tags.MEMBERFIRM",
      "tags.PRIMARYCONTACT",
      "tags.SECONDARYCONTACT",
      "volume_tags.%",
      "volume_tags.AccountID",
      "volume_tags.ApplicationName",
      "volume_tags.BILLINGCODE",
      "volume_tags.BILLINGCONTACT",
      "volume_tags.CLIENT",
      "volume_tags.COUNTRY",
      "volume_tags.CSCLASS",
      "volume_tags.CSQUAL",
      "volume_tags.CSTYPE",
      "volume_tags.DataClassification",
      "volume_tags.Docker",
      "volume_tags.ENVIRONMENT",
      "volume_tags.Environment",
      "volume_tags.FUNCTION",
      "volume_tags.GROUPCONTACT",
      "volume_tags.MEMBERFIRM",
      "volume_tags.ManagedBy",
      "volume_tags.Name",
      "volume_tags.Owner",
      "volume_tags.PRIMARYCONTACT",
      "volume_tags.PatchGroup",
      "volume_tags.ResourceSeverity",
      "volume_tags.ResourceType",
      "volume_tags.Role",
      "volume_tags.SECONDARYCONTACT",
      "volume_tags.SolutionName",
      "volume_tags.VendorManaged"
    ]
  }

  volume_tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "MANAGED_SERVICES"
  }

  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "MANAGED_SERVICES"
    PatchGroup = "AmazonLinux2-AzB-Group1"
    Owner = "robhatnagar@deloitte.ca"
    ManagedBy = "CMS-TF"
    SolutionName = "InsurCloud Economical"
    Environment = "Dev"
    ApplicationName = "docker, haproxy"
    DataClassification = "PII, PHI"
    DLM-Backup = "Yes"
    DLM-Retention = "90"
    VendorManaged = "False"
    ResourceType = "Client"
    ResourceSeverity = "Normal"
    Name = "A-MC-CAC-D-A003-ECONOMICAL-PPS01-INTERNAL-HAPROXY02-EC2"
    Role = "Container"
  }

  provisioner "local-exec" {
    command = "echo ${aws_instance.ICECONOMICAL_PPS01_INTERNAL_HAPROXY02.private_ip} >> dev_hosts.txt"
  }
}

output "ICECONOMICAL_PPS01_INTERNAL_HAPROXY_PRIVATEIP02" {
  value = "${aws_instance.ICECONOMICAL_PPS01_INTERNAL_HAPROXY02.private_ip}"
}


# #=======================================================================
# # DEV
# #=======================================================================
############# JENKINS CLIENT #################
resource "aws_instance" "ICECONOMICAL_DEV_JENKINS_CLIENT" {
  ami = "${var.ami_id_amazonlinux2base}"
  vpc_security_group_ids = [
    "${aws_security_group.SG_JENKINS_CLIENT.id}"]
  subnet_id = "${var.economical-dev-private-1a-subnet}"
  instance_type = "r5a.xlarge"
  key_name = "EconomicalJenkinsNPRD"
  associate_public_ip_address = false
  iam_instance_profile = "CMS-SSMInstanceRole"
  ebs_optimized = "true"

  root_block_device {
    volume_size = "250"
  }

  lifecycle {
    prevent_destroy = false
    ignore_changes = [
      "tags.%",
      "tags.BILLINGCODE",
      "tags.BILLINGCONTACT",
      "tags.CLIENT",
      "tags.COUNTRY",
      "tags.CSCLASS",
      "tags.CSQUAL",
      "tags.CSTYPE",
      "tags.ENVIRONMENT",
      "tags.FUNCTION",
      "tags.GROUPCONTACT",
      "tags.MEMBERFIRM",
      "tags.PRIMARYCONTACT",
      "tags.SECONDARYCONTACT",
      "volume_tags.%",
      "volume_tags.AccountID",
      "volume_tags.ApplicationName",
      "volume_tags.BILLINGCODE",
      "volume_tags.BILLINGCONTACT",
      "volume_tags.CLIENT",
      "volume_tags.COUNTRY",
      "volume_tags.CSCLASS",
      "volume_tags.CSQUAL",
      "volume_tags.CSTYPE",
      "volume_tags.DataClassification",
      "volume_tags.Docker",
      "volume_tags.ENVIRONMENT",
      "volume_tags.Environment",
      "volume_tags.FUNCTION",
      "volume_tags.GROUPCONTACT",
      "volume_tags.MEMBERFIRM",
      "volume_tags.ManagedBy",
      "volume_tags.Name",
      "volume_tags.Owner",
      "volume_tags.PRIMARYCONTACT",
      "volume_tags.PatchGroup",
      "volume_tags.ResourceSeverity",
      "volume_tags.ResourceType",
      "volume_tags.Role",
      "volume_tags.SECONDARYCONTACT",
      "volume_tags.SolutionName",
      "volume_tags.VendorManaged"
    ]
  }

  volume_tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "MANAGED_SERVICES"
  }

  tags = {
    AccountID = "${var.economical-dev-account}"
    Client = "Economical"
    Contract = "MANAGED_SERVICES"
    PatchGroup = "AmazonLinux2-AzA-Group1"
    Owner = "robhatnagar@deloitte.ca"
    ManagedBy = "CMS-TF"
    SolutionName = "InsurCloud Economical"
    Environment = "Dev"
    ApplicationName = "jenkins, docker"
    DataClassification = "NA"
    DLM-Backup = "Yes"
    DLM-Retention = "90"
    VendorManaged = "False"
    ResourceType = "Client"
    ResourceSeverity = "Normal"
    Name = "A-MC-CAC-D-A003-ECONOMICAL-DEV-JENKINS-CLIENT-EC2"
    Role = "Container"
  }
}

output "ICECONOMICAL_DEV_JENKINS_CLIENT_PRIVATEIP" {
  value = "${aws_instance.ICECONOMICAL_DEV_JENKINS_CLIENT.private_ip}"
}

######################################
