# #=======================================================================
# # Shared Resource 
# #=======================================================================


############# JENKINS CLIENT #################
resource "aws_instance" "ICGORE_DEV_JENKINS_CLIENT" {
    ami = "${var.ami_id_amazonlinux2base}"
    vpc_security_group_ids = [
        "${aws_security_group.SG_JENKINS_CLIENT.id}",
        "${aws_security_group.SG_AVIATRIX_SSH.id}"
    ]
	subnet_id = "${var.gore-dev-private-1a-subnet}"
    instance_type = "r5.xlarge"
    key_name    = "GoreJenkinsDev"
    associate_public_ip_address = false
    iam_instance_profile = "CMS-SSMInstanceRole"
	ebs_optimized = "true"

	lifecycle {
       prevent_destroy = false
	}

	root_block_device {
       volume_size = "250"
	}

	tags {
        Client = "GoreMutual"
        Account = "GORE-MUTUAL-dev"
        Owner = "robhatnagar@deloitte.ca"
        ManagedBy = "CMS-TF"
        SolutionName = "Insurcloud Gore"
        Environment = "Dev"
		ApplicationName = "jenkins- docker"
		DataClassification = "NA"
		DLM-Backup = "Yes"
		DLM-Retention = "90"
		PatchGroup = "AmazonLinux2"
        VendorManaged = "False"
		ResourceType = "Client"
		ResourceSeverity = "Critical"
		Name = "A-MC-CAC-D-A003-GORE-DEV-JENKINS-CLIENT-EC2"
		AccountID = "${data.aws_caller_identity.current.account_id}"
		Role = "Container"
	}
}

output "ICPEEL_DEV_JENKINS_CLIENT_PRIVATEIP" {
  value = "${aws_instance.ICGORE_DEV_JENKINS_CLIENT.private_ip}"
}

######################################

# #=======================================================================
# # DEV01
# #=======================================================================

######### MICROSERVICES ###########
resource "aws_instance" "ICGORE_DEV01_MICROSERVICES" {
    ami = "${var.ami_id_amazonlinux2base}"
    vpc_security_group_ids = [
        "${aws_security_group.SG_MICROSERVICES_DEV.id}",
        "${aws_security_group.SG_MONITORING.id}",
        "${aws_security_group.SG_AVIATRIX_SSH.id}",
        "${aws_security_group.SG_JENKINS_CLIENT_SSH.id}",
        "${aws_security_group.SG_MYSQL_DB.id}"
    ]
    subnet_id = "${var.gore-dev-private-1a-subnet}"
    instance_type = "r5a.2xlarge"
    key_name    = "GoreMicroservicesDev"
    associate_public_ip_address = false
    iam_instance_profile = "CMS-SSMInstanceRole"
	ebs_optimized = "true"

    root_block_device {
       volume_size = "200"
    }

    tags {
        Client = "GoreMutual"
        Account = "GORE-MUTUAL-dev"
        Owner = "robhatnagar@deloitte.ca"
        ManagedBy = "CMS-TF"
        SolutionName = "Insurcloud Gore"
        Environment = "Dev"
        ApplicationName = "docker, microservices, mysql"
        DataClassification = "PII, PHI"
        DLM-Backup = "Yes"
        DLM-Retention = "90"
        PatchGroup = "AmazonLinux2"
        VendorManaged = "False"
        ResourceType = "Client"
        ResourceSeverity = "Normal"
        Name = "A-MC-CAC-D-A003-GORE-DEV01-MICROSERVICES-LNX-EC2"
        AccountID = "${data.aws_caller_identity.current.account_id}"
        Role = "Container"
    }

    provisioner "local-exec" {
        command = "echo ${aws_instance.ICGORE_DEV01_MICROSERVICES.private_ip} >> dev_hosts.txt"
    }
}

output "ICGORE_DEV01_MICROSERVICES_PRIVATEIP" {
    value = "${aws_instance.ICGORE_DEV01_MICROSERVICES.private_ip}"
}

######### HAPROXY ##############
resource "aws_instance" "ICGORE_DEV01_INTERNAL_HAPROXY" {
    ami = "${var.ami_id_amazonlinux2base}"
    vpc_security_group_ids = [
        "${aws_security_group.SG_INTERNAL_HAPROXY_DEV.id}",
        "${aws_security_group.SG_AVIATRIX_SSH.id}",
        "${aws_security_group.SG_JENKINS_CLIENT_SSH.id}",
        "${aws_security_group.SG_CLIENT_PING_TEST.id}"
    ]
    subnet_id = "${var.gore-dev-private-1a-subnet}"
    instance_type = "t3.small"
    key_name    = "GoreHAProxyDev"
    associate_public_ip_address = false
    iam_instance_profile = "CMS-SSMInstanceRole"
	ebs_optimized = "true"

    root_block_device {
       volume_size = "40"
    }

    tags {
        Client = "GoreMutual"
        Account = "GORE-MUTUAL-dev"
        Owner = "robhatnagar@deloitte.ca"
        ManagedBy = "CMS-TF"
        SolutionName = "Insurcloud Gore"
        Environment = "Dev"
        ApplicationName = "docker- haproxy"
        DataClassification = "PII, PHI"
        DLM-Backup = "Yes"
        DLM-Retention = "90"
        PatchGroup = "AmazonLinux2"
        VendorManaged = "False"
        ResourceType = "Client"
        ResourceSeverity = "Normal"
        Name = "A-MC-CAC-D-A003-GORE-DEV01-INTERNAL-HAPROXY-EC2"
        AccountID = "${data.aws_caller_identity.current.account_id}"
        Role = "Container"
    }
   
    provisioner "local-exec" {
        command = "echo ${aws_instance.ICGORE_DEV01_INTERNAL_HAPROXY.private_ip} >> dev_hosts.txt"
    }
}

output "ICGORE_DEV01_INTERNAL_HAPROXY_PRIVATEIP" {
    value = "${aws_instance.ICGORE_DEV01_INTERNAL_HAPROXY.private_ip}"
}


# #=======================================================================
# # DEV02
# #=======================================================================

######### MICROSERVICES ###########
resource "aws_instance" "ICGORE_DEV02_MICROSERVICES" {
    ami = "${var.ami_id_amazonlinux2base}"
    vpc_security_group_ids = [
        "${aws_security_group.SG_MICROSERVICES_DEV.id}",
        "${aws_security_group.SG_MONITORING.id}",
        "${aws_security_group.SG_AVIATRIX_SSH.id}",
        "${aws_security_group.SG_JENKINS_CLIENT_SSH.id}",
        "${aws_security_group.SG_MYSQL_DB.id}"
    ]
    subnet_id = "${var.gore-dev-private-1a-subnet}"
    instance_type = "r5a.2xlarge"
    key_name    = "GoreMicroservicesDev"
    associate_public_ip_address = false
    iam_instance_profile = "CMS-SSMInstanceRole"
	ebs_optimized = "true"

    root_block_device {
       volume_size = "200"
    }

    tags {
        Client = "GoreMutual"
        Account = "GORE-MUTUAL-dev"
        Owner = "robhatnagar@deloitte.ca"
        ManagedBy = "CMS-TF"
        SolutionName = "Insurcloud Gore"
        Environment = "Dev"
        ApplicationName = "docker- microservices- mysql"
        DataClassification = "PII, PHI"
        DLM-Backup = "Yes"
        DLM-Retention = "90"
        PatchGroup = "AmazonLinux2"
        VendorManaged = "False"
        ResourceType = "Client"
        ResourceSeverity = "Normal"
        Name = "A-MC-CAC-D-A003-GORE-DEV02-MICROSERVICES-LNX-EC2"
        AccountID = "${data.aws_caller_identity.current.account_id}"
        Role = "Container"
    }

    provisioner "local-exec" {
        command = "echo ${aws_instance.ICGORE_DEV02_MICROSERVICES.private_ip} >> dev_hosts.txt"
    }
}

output "ICGORE_DEV02_MICROSERVICES_PRIVATEIP" {
    value = "${aws_instance.ICGORE_DEV02_MICROSERVICES.private_ip}"
}

######### HAPROXY ##############
resource "aws_instance" "ICGORE_DEV02_INTERNAL_HAPROXY" {
    ami = "${var.ami_id_amazonlinux2base}"
    vpc_security_group_ids = [
        "${aws_security_group.SG_INTERNAL_HAPROXY_DEV.id}",
        "${aws_security_group.SG_AVIATRIX_SSH.id}",
        "${aws_security_group.SG_JENKINS_CLIENT_SSH.id}",
        "${aws_security_group.SG_CLIENT_PING_TEST.id}"
    ]
    subnet_id = "${var.gore-dev-private-1a-subnet}"
    instance_type = "t3.small"
    key_name    = "GoreHAProxyDev"
    associate_public_ip_address = false
    iam_instance_profile = "CMS-SSMInstanceRole"
	ebs_optimized = "true"

    root_block_device {
       volume_size = "40"
    }

    tags {
        Client = "GoreMutual"
        Account = "GORE-MUTUAL-dev"
        Owner = "robhatnagar@deloitte.ca"
        ManagedBy = "CMS-TF"
        SolutionName = "Insurcloud Gore"
        Environment = "Dev"
        ApplicationName = "docker- haproxy"
        DataClassification = "PII, PHI"
        DLM-Backup = "Yes"
        DLM-Retention = "90"
        PatchGroup = "AmazonLinux2"
        VendorManaged = "False"
        ResourceType = "Client"
        ResourceSeverity = "Normal"
        Name = "A-MC-CAC-D-A003-GORE-DEV02-INTERNAL-HAPROXY-EC2"
        AccountID = "${data.aws_caller_identity.current.account_id}"
        Role = "Container"
    }
   
    provisioner "local-exec" {
        command = "echo ${aws_instance.ICGORE_DEV02_INTERNAL_HAPROXY.private_ip} >> dev_hosts.txt"
    }
}

output "ICGORE_DEV02_INTERNAL_HAPROXY_PRIVATEIP" {
    value = "${aws_instance.ICGORE_DEV02_INTERNAL_HAPROXY.private_ip}"
}



# #=======================================================================
# # DEV03
# #=======================================================================

######### MICROSERVICES ###########
resource "aws_instance" "ICGORE_DEV03_MICROSERVICES" {
    ami = "${var.ami_id_amazonlinux2base}"
    vpc_security_group_ids = [
        "${aws_security_group.SG_MICROSERVICES_DEV.id}",
        "${aws_security_group.SG_MONITORING.id}",
        "${aws_security_group.SG_AVIATRIX_SSH.id}",
        "${aws_security_group.SG_JENKINS_CLIENT_SSH.id}",
        "${aws_security_group.SG_MYSQL_DB.id}"
    ]
    subnet_id = "${var.gore-dev-private-1a-subnet}"
    instance_type = "r5a.2xlarge"
    key_name    = "GoreMicroservicesDev"
    associate_public_ip_address = false
    iam_instance_profile = "CMS-SSMInstanceRole"
	ebs_optimized = "true"

    root_block_device {
       volume_size = "200"
    }

    tags {
        Client = "GoreMutual"
        Account = "GORE-MUTUAL-dev"
        Owner = "robhatnagar@deloitte.ca"
        ManagedBy = "CMS-TF"
        SolutionName = "Insurcloud Gore"
        Environment = "Dev"
        ApplicationName = "docker- microservices- mysql"
        DataClassification = "PII, PHI"
        DLM-Backup = "Yes"
        DLM-Retention = "90"
        PatchGroup = "AmazonLinux2"
        VendorManaged = "False"
        ResourceType = "Client"
        ResourceSeverity = "Normal"
        Name = "A-MC-CAC-D-A003-GORE-DEV03-MICROSERVICES-LNX-EC2"
        AccountID = "${data.aws_caller_identity.current.account_id}"
        Role = "Container"
    }

    provisioner "local-exec" {
        command = "echo ${aws_instance.ICGORE_DEV03_MICROSERVICES.private_ip} >> dev_hosts.txt"
    }
}

output "ICGORE_DEV03_MICROSERVICES_PRIVATEIP" {
    value = "${aws_instance.ICGORE_DEV03_MICROSERVICES.private_ip}"
}

######### HAPROXY ##############
resource "aws_instance" "ICGORE_DEV03_INTERNAL_HAPROXY" {
    ami = "${var.ami_id_amazonlinux2base}"
    vpc_security_group_ids = [
        "${aws_security_group.SG_INTERNAL_HAPROXY_DEV.id}",
        "${aws_security_group.SG_AVIATRIX_SSH.id}",
        "${aws_security_group.SG_JENKINS_CLIENT_SSH.id}",
        "${aws_security_group.SG_CLIENT_PING_TEST.id}"
    ]
    subnet_id = "${var.gore-dev-private-1a-subnet}"
    instance_type = "t3.small"
    key_name    = "GoreHAProxyDev"
    associate_public_ip_address = false
    iam_instance_profile = "CMS-SSMInstanceRole"
	ebs_optimized = "true"

    root_block_device {
       volume_size = "40"
    }

    tags {
        Client = "GoreMutual"
        Account = "GORE-MUTUAL-dev"
        Owner = "robhatnagar@deloitte.ca"
        ManagedBy = "CMS-TF"
        SolutionName = "Insurcloud Gore"
        Environment = "Dev"
        ApplicationName = "docker- haproxy"
        DataClassification = "PII, PHI"
        DLM-Backup = "Yes"
        DLM-Retention = "90"
        PatchGroup = "AmazonLinux2"
        VendorManaged = "False"
        ResourceType = "Client"
        ResourceSeverity = "Normal"
        Name = "A-MC-CAC-D-A003-GORE-DEV03-INTERNAL-HAPROXY-EC2"
        AccountID = "${data.aws_caller_identity.current.account_id}"
        Role = "Container"
    }
   
    provisioner "local-exec" {
        command = "echo ${aws_instance.ICGORE_DEV03_INTERNAL_HAPROXY.private_ip} >> dev_hosts.txt"
    }
}

output "ICGORE_DEV03_INTERNAL_HAPROXY_PRIVATEIP" {
    value = "${aws_instance.ICGORE_DEV03_INTERNAL_HAPROXY.private_ip}"
}


# #=======================================================================
# # TBT01
# #=======================================================================

######### MICROSERVICES ###########
resource "aws_instance" "ICGORE_TBT01_MICROSERVICES" {
    ami = "${var.ami_id_amazonlinux2base}"
    vpc_security_group_ids = [
        "${aws_security_group.SG_MICROSERVICES_DEV.id}",
        "${aws_security_group.SG_MONITORING.id}",
        "${aws_security_group.SG_AVIATRIX_SSH.id}",
        "${aws_security_group.SG_JENKINS_CLIENT_SSH.id}",
        "${aws_security_group.SG_MYSQL_DB.id}"
    ]
    subnet_id = "${var.gore-dev-private-1a-subnet}"
    instance_type = "r5a.2xlarge"
    key_name    = "GoreMicroservicesDev"
    associate_public_ip_address = false
    iam_instance_profile = "CMS-SSMInstanceRole"
	ebs_optimized = "true"

    root_block_device {
       volume_size = "200"
    }

    tags {
        Client = "GoreMutual"
        Account = "GORE-MUTUAL-dev"
        Owner = "robhatnagar@deloitte.ca"
        ManagedBy = "CMS-TF"
        SolutionName = "Insurcloud Gore"
        Environment = "Dev"
        ApplicationName = "docker- microservices- mysql"
        DataClassification = "PII, PHI"
        DLM-Backup = "Yes"
        DLM-Retention = "90"
        PatchGroup = "AmazonLinux2"
        VendorManaged = "False"
        ResourceType = "Client"
        ResourceSeverity = "Normal"
        Name = "A-MC-CAC-D-A003-GORE-TBT01-MICROSERVICES-LNX-EC2"
        AccountID = "${data.aws_caller_identity.current.account_id}"
        Role = "Container"
    }

    provisioner "local-exec" {
        command = "echo ${aws_instance.ICGORE_TBT01_MICROSERVICES.private_ip} >> dev_hosts.txt"
    }
}

output "ICGORE_TBT01_MICROSERVICES_PRIVATEIP" {
    value = "${aws_instance.ICGORE_TBT01_MICROSERVICES.private_ip}"
}
######### HAPROXY ##############
resource "aws_instance" "ICGORE_TBT01_INTERNAL_HAPROXY" {
    ami = "${var.ami_id_amazonlinux2base}"
    vpc_security_group_ids = [
        "${aws_security_group.SG_INTERNAL_HAPROXY_DEV.id}",
        "${aws_security_group.SG_AVIATRIX_SSH.id}",
        "${aws_security_group.SG_JENKINS_CLIENT_SSH.id}",
        "${aws_security_group.SG_CLIENT_PING_TEST.id}"
    ]
    subnet_id = "${var.gore-dev-private-1a-subnet}"
    instance_type = "t3.small"
    key_name    = "GoreHAProxyDev"
    associate_public_ip_address = false
    iam_instance_profile = "CMS-SSMInstanceRole"
	ebs_optimized = "true"

    root_block_device {
       volume_size = "40"
    }

    tags {
        Client = "GoreMutual"
        Account = "GORE-MUTUAL-dev"
        Owner = "robhatnagar@deloitte.ca"
        ManagedBy = "CMS-TF"
        SolutionName = "Insurcloud Gore"
        Environment = "Dev"
        ApplicationName = "docker- haproxy"
        DataClassification = "PII, PHI"
        DLM-Backup = "Yes"
        DLM-Retention = "90"
        PatchGroup = "AmazonLinux2"
        VendorManaged = "False"
        ResourceType = "Client"
        ResourceSeverity = "Normal"
        Name = "A-MC-CAC-D-A003-GORE-TBT01-INTERNAL-HAPROXY-EC2"
        AccountID = "${data.aws_caller_identity.current.account_id}"
        Role = "Container"
    }
   
    provisioner "local-exec" {
        command = "echo ${aws_instance.ICGORE_TBT01_INTERNAL_HAPROXY.private_ip} >> dev_hosts.txt"
    }
}

output "ICGORE_TBT01_INTERNAL_HAPROXY_PRIVATEIP" {
    value = "${aws_instance.ICGORE_TBT01_INTERNAL_HAPROXY.private_ip}"
}



# #=======================================================================
# # QA01
# #=======================================================================

######### MICROSERVICES ###########
resource "aws_instance" "ICGORE_QA01_MICROSERVICES" {
    ami = "${var.ami_id_amazonlinux2base}"
    vpc_security_group_ids = [
        "${aws_security_group.SG_MICROSERVICES_DEV.id}",
        "${aws_security_group.SG_MONITORING.id}",
        "${aws_security_group.SG_AVIATRIX_SSH.id}",
        "${aws_security_group.SG_JENKINS_CLIENT_SSH.id}",
        "${aws_security_group.SG_MYSQL_DB.id}"
    ]
    subnet_id = "${var.gore-dev-private-1a-subnet}"
    instance_type = "r5a.2xlarge"
    key_name    = "GoreMicroservicesDev"
    associate_public_ip_address = false
    iam_instance_profile = "CMS-SSMInstanceRole"
	ebs_optimized = "true"

    root_block_device {
       volume_size = "200"
    }

    tags {
        Client = "GoreMutual"
        Account = "GORE-MUTUAL-dev"
        Owner = "robhatnagar@deloitte.ca"
        ManagedBy = "CMS-TF"
        SolutionName = "Insurcloud Gore"
        Environment = "Dev"
        ApplicationName = "docker- microservices- mysql"
        DataClassification = "PII, PHI"
        DLM-Backup = "Yes"
        DLM-Retention = "90"
        PatchGroup = "AmazonLinux2"
        VendorManaged = "False"
        ResourceType = "Client"
        ResourceSeverity = "Normal"
        Name = "A-MC-CAC-D-A003-GORE-QA01-MICROSERVICES-LNX-EC2"
        AccountID = "${data.aws_caller_identity.current.account_id}"
        Role = "Container"
    }

    provisioner "local-exec" {
        command = "echo ${aws_instance.ICGORE_QA01_MICROSERVICES.private_ip} >> dev_hosts.txt"
    }
}

output "ICGORE_QA01_MICROSERVICES_PRIVATEIP" {
    value = "${aws_instance.ICGORE_QA01_MICROSERVICES.private_ip}"
}

######### HAPROXY ##############
resource "aws_instance" "ICGORE_QA01_INTERNAL_HAPROXY" {
    ami = "${var.ami_id_amazonlinux2base}"
    vpc_security_group_ids = [
        "${aws_security_group.SG_INTERNAL_HAPROXY_DEV.id}",
        "${aws_security_group.SG_AVIATRIX_SSH.id}",
        "${aws_security_group.SG_JENKINS_CLIENT_SSH.id}",
        "${aws_security_group.SG_CLIENT_PING_TEST.id}"
    ]
    subnet_id = "${var.gore-dev-private-1a-subnet}"
    instance_type = "t3.small"
    key_name    = "GoreHAProxyDev"
    associate_public_ip_address = false
    iam_instance_profile = "CMS-SSMInstanceRole"
	ebs_optimized = "true"

    root_block_device {
       volume_size = "40"
    }

    tags {
        Client = "GoreMutual"
        Account = "GORE-MUTUAL-dev"
        Owner = "robhatnagar@deloitte.ca"
        ManagedBy = "CMS-TF"
        SolutionName = "Insurcloud Gore"
        Environment = "Dev"
        ApplicationName = "docker- haproxy"
        DataClassification = "PII, PHI"
        DLM-Backup = "Yes"
        DLM-Retention = "90"
        PatchGroup = "AmazonLinux2"
        VendorManaged = "False"
        ResourceType = "Client"
        ResourceSeverity = "Normal"
        Name = "A-MC-CAC-D-A003-GORE-QA01-INTERNAL-HAPROXY-EC2"
        AccountID = "${data.aws_caller_identity.current.account_id}"
        Role = "Container"
    }
   
    provisioner "local-exec" {
        command = "echo ${aws_instance.ICGORE_QA01_INTERNAL_HAPROXY.private_ip} >> dev_hosts.txt"
    }
}

output "ICGORE_QA01_INTERNAL_HAPROXY_PRIVATEIP" {
    value = "${aws_instance.ICGORE_QA01_INTERNAL_HAPROXY.private_ip}"
}



# #=======================================================================
# # QA02
# #=======================================================================

######### MICROSERVICES ###########
resource "aws_instance" "ICGORE_QA02_MICROSERVICES" {
    ami = "${var.ami_id_amazonlinux2base}"
    vpc_security_group_ids = [
        "${aws_security_group.SG_MICROSERVICES_DEV.id}",
        "${aws_security_group.SG_MONITORING.id}",
        "${aws_security_group.SG_AVIATRIX_SSH.id}",
        "${aws_security_group.SG_JENKINS_CLIENT_SSH.id}",
        "${aws_security_group.SG_MYSQL_DB.id}"
    ]
    subnet_id = "${var.gore-dev-private-1a-subnet}"
    instance_type = "r5a.2xlarge"
    key_name    = "GoreMicroservicesDev"
    associate_public_ip_address = false
    iam_instance_profile = "CMS-SSMInstanceRole"
	ebs_optimized = "true"

    root_block_device {
       volume_size = "200"
    }

    tags {
        Client = "GoreMutual"
        Account = "GORE-MUTUAL-dev"
        Owner = "robhatnagar@deloitte.ca"
        ManagedBy = "CMS-TF"
        SolutionName = "Insurcloud Gore"
        Environment = "Dev"
        ApplicationName = "docker- microservices- mysql"
        DataClassification = "PII, PHI"
        DLM-Backup = "Yes"
        DLM-Retention = "90"
        PatchGroup = "AmazonLinux2"
        VendorManaged = "False"
        ResourceType = "Client"
        ResourceSeverity = "Normal"
        Name = "A-MC-CAC-D-A003-GORE-QA02-MICROSERVICES-LNX-EC2"
        AccountID = "${data.aws_caller_identity.current.account_id}"
        Role = "Container"
    }

    provisioner "local-exec" {
        command = "echo ${aws_instance.ICGORE_QA02_MICROSERVICES.private_ip} >> dev_hosts.txt"
    }
}

output "ICGORE_QA02_MICROSERVICES_PRIVATEIP" {
    value = "${aws_instance.ICGORE_QA02_MICROSERVICES.private_ip}"
}

######### HAPROXY ##############
resource "aws_instance" "ICGORE_QA02_INTERNAL_HAPROXY" {
    ami = "${var.ami_id_amazonlinux2base}"
    vpc_security_group_ids = [
        "${aws_security_group.SG_INTERNAL_HAPROXY_DEV.id}",
        "${aws_security_group.SG_AVIATRIX_SSH.id}",
        "${aws_security_group.SG_JENKINS_CLIENT_SSH.id}",
        "${aws_security_group.SG_CLIENT_PING_TEST.id}"
    ]
    subnet_id = "${var.gore-dev-private-1a-subnet}"
    instance_type = "t3.small"
    key_name    = "GoreHAProxyDev"
    associate_public_ip_address = false
    iam_instance_profile = "CMS-SSMInstanceRole"
	ebs_optimized = "true"

    root_block_device {
       volume_size = "40"
    }

    tags {
        Client = "GoreMutual"
        Account = "GORE-MUTUAL-dev"
        Owner = "robhatnagar@deloitte.ca"
        ManagedBy = "CMS-TF"
        SolutionName = "Insurcloud Gore"
        Environment = "Dev"
        ApplicationName = "docker- haproxy"
        DataClassification = "PII, PHI"
        DLM-Backup = "Yes"
        DLM-Retention = "90"
        PatchGroup = "AmazonLinux2"
        VendorManaged = "False"
        ResourceType = "Client"
        ResourceSeverity = "Normal"
        Name = "A-MC-CAC-D-A003-GORE-QA02-INTERNAL-HAPROXY-EC2"
        AccountID = "${data.aws_caller_identity.current.account_id}"
        Role = "Container"
    }
   
    provisioner "local-exec" {
        command = "echo ${aws_instance.ICGORE_QA02_INTERNAL_HAPROXY.private_ip} >> dev_hosts.txt"
    }
}

output "ICGORE_QA02_INTERNAL_HAPROXY_PRIVATEIP" {
    value = "${aws_instance.ICGORE_QA02_INTERNAL_HAPROXY.private_ip}"
}



# #=======================================================================
# # TBT02
# #=======================================================================

######### MICROSERVICES ###########
resource "aws_instance" "ICGORE_TBT02_MICROSERVICES" {
    ami = "${var.ami_id_amazonlinux2base}"
    vpc_security_group_ids = [
        "${aws_security_group.SG_MICROSERVICES_DEV.id}",
        "${aws_security_group.SG_MONITORING.id}",
        "${aws_security_group.SG_AVIATRIX_SSH.id}",
        "${aws_security_group.SG_JENKINS_CLIENT_SSH.id}",
        "${aws_security_group.SG_MYSQL_DB.id}"
    ]
    subnet_id = "${var.gore-dev-private-1a-subnet}"
    instance_type = "r5a.2xlarge"
    key_name    = "GoreMicroservicesDev"
    associate_public_ip_address = false
    iam_instance_profile = "CMS-SSMInstanceRole"
	ebs_optimized = "true"

    root_block_device {
       volume_size = "200"
    }

    tags {
        Client = "GoreMutual"
        Account = "GORE-MUTUAL-dev"
        Owner = "robhatnagar@deloitte.ca"
        ManagedBy = "CMS-TF"
        SolutionName = "Insurcloud Gore"
        Environment = "Dev"
        ApplicationName = "docker- microservices- mysql"
        DataClassification = "PII, PHI"
        DLM-Backup = "Yes"
        DLM-Retention = "90"
        PatchGroup = "AmazonLinux2"
        VendorManaged = "False"
        ResourceType = "Client"
        ResourceSeverity = "Normal"
        Name = "A-MC-CAC-D-A003-GORE-TBT02-MICROSERVICES-LNX-EC2"
        AccountID = "${data.aws_caller_identity.current.account_id}"
        Role = "Container"
    }

    provisioner "local-exec" {
        command = "echo ${aws_instance.ICGORE_TBT02_MICROSERVICES.private_ip} >> dev_hosts.txt"
    }
}

output "ICGORE_TBT02_MICROSERVICES_PRIVATEIP" {
    value = "${aws_instance.ICGORE_TBT02_MICROSERVICES.private_ip}"
}

######### HAPROXY ##############
resource "aws_instance" "ICGORE_TBT02_INTERNAL_HAPROXY" {
    ami = "${var.ami_id_amazonlinux2base}"
    vpc_security_group_ids = [
        "${aws_security_group.SG_INTERNAL_HAPROXY_DEV.id}",
        "${aws_security_group.SG_AVIATRIX_SSH.id}",
        "${aws_security_group.SG_JENKINS_CLIENT_SSH.id}",
        "${aws_security_group.SG_CLIENT_PING_TEST.id}"
    ]
    subnet_id = "${var.gore-dev-private-1a-subnet}"
    instance_type = "t3a.small"
    key_name    = "GoreHAProxyDev"
    associate_public_ip_address = false
    iam_instance_profile = "CMS-SSMInstanceRole"
	ebs_optimized = "true"

    root_block_device {
       volume_size = "40"
    }

    tags {
        Client = "GoreMutual"
        Account = "GORE-MUTUAL-dev"
        Owner = "robhatnagar@deloitte.ca"
        ManagedBy = "CMS-TF"
        SolutionName = "Insurcloud Gore"
        Environment = "Dev"
        ApplicationName = "docker- haproxy"
        DataClassification = "PII, PHI"
        DLM-Backup = "Yes"
        DLM-Retention = "90"
        PatchGroup = "AmazonLinux2"
        VendorManaged = "False"
        ResourceType = "Client"
        ResourceSeverity = "Normal"
        Name = "A-MC-CAC-D-A003-GORE-TBT02-INTERNAL-HAPROXY-EC2"
        AccountID = "${data.aws_caller_identity.current.account_id}"
        Role = "Container"
    }
   
    provisioner "local-exec" {
        command = "echo ${aws_instance.ICGORE_TBT02_INTERNAL_HAPROXY.private_ip} >> dev_hosts.txt"
    }
}

output "ICGORE_TBT02_INTERNAL_HAPROXY_PRIVATEIP" {
    value = "${aws_instance.ICGORE_TBT02_INTERNAL_HAPROXY.private_ip}"
}


# #=======================================================================
# # BAT
# #=======================================================================

######### MICROSERVICES ###########
resource "aws_instance" "ICGORE_BAT_MICROSERVICES" {
    ami = "${var.ami_id_amazonlinux2base}"
    vpc_security_group_ids = [
        "${aws_security_group.SG_MICROSERVICES_DEV.id}",
        "${aws_security_group.SG_MONITORING.id}",
        "${aws_security_group.SG_AVIATRIX_SSH.id}",
        "${aws_security_group.SG_JENKINS_CLIENT_SSH.id}",
        "${aws_security_group.SG_MYSQL_DB.id}"
    ]
    subnet_id = "${var.gore-dev-private-1a-subnet}"
    instance_type = "r5a.2xlarge"
    key_name    = "GoreMicroservicesDev"
    associate_public_ip_address = false
    iam_instance_profile = "CMS-SSMInstanceRole"
	ebs_optimized = "true"

    root_block_device {
       volume_size = "200"
    }

    tags {
        Client = "GoreMutual"
        Account = "GORE-MUTUAL-dev"
        Owner = "robhatnagar@deloitte.ca"
        ManagedBy = "CMS-TF"
        SolutionName = "Insurcloud Gore"
        Environment = "Dev"
        ApplicationName = "docker- microservices- mysql"
        DataClassification = "PII, PHI"
        DLM-Backup = "Yes"
        DLM-Retention = "90"
        PatchGroup = "AmazonLinux2"
        VendorManaged = "False"
        ResourceType = "Client"
        ResourceSeverity = "Normal"
        Name = "A-MC-CAC-D-A003-GORE-BAT-MICROSERVICES-LNX-EC2"
        AccountID = "${data.aws_caller_identity.current.account_id}"
        Role = "Container"
    }

    provisioner "local-exec" {
        command = "echo ${aws_instance.ICGORE_BAT_MICROSERVICES.private_ip} >> dev_hosts.txt"
    }
}

output "ICGORE_BAT_MICROSERVICES_PRIVATEIP" {
    value = "${aws_instance.ICGORE_BAT_MICROSERVICES.private_ip}"
}

######### HAPROXY ##############
resource "aws_instance" "ICGORE_BAT_INTERNAL_HAPROXY" {
    ami = "${var.ami_id_amazonlinux2base}"
    vpc_security_group_ids = [
        "${aws_security_group.SG_INTERNAL_HAPROXY_DEV.id}",
        "${aws_security_group.SG_AVIATRIX_SSH.id}",
        "${aws_security_group.SG_JENKINS_CLIENT_SSH.id}",
        "${aws_security_group.SG_CLIENT_PING_TEST.id}"
    ]
    subnet_id = "${var.gore-dev-private-1a-subnet}"
    instance_type = "t3a.small"
    key_name    = "GoreHAProxyDev"
    associate_public_ip_address = false
    iam_instance_profile = "CMS-SSMInstanceRole"
	ebs_optimized = "true"

    root_block_device {
       volume_size = "40"
    }

    tags {
        Client = "GoreMutual"
        Account = "GORE-MUTUAL-dev"
        Owner = "robhatnagar@deloitte.ca"
        ManagedBy = "CMS-TF"
        SolutionName = "Insurcloud Gore"
        Environment = "Dev"
        ApplicationName = "docker- haproxy"
        DataClassification = "PII, PHI"
        DLM-Backup = "Yes"
        DLM-Retention = "90"
        PatchGroup = "AmazonLinux2"
        VendorManaged = "False"
        ResourceType = "Client"
        ResourceSeverity = "Normal"
        Name = "A-MC-CAC-D-A003-GORE-BAT-INTERNAL-HAPROXY-EC2"
        AccountID = "${data.aws_caller_identity.current.account_id}"
        Role = "Container"
    }
   
    provisioner "local-exec" {
        command = "echo ${aws_instance.ICGORE_BAT_INTERNAL_HAPROXY.private_ip} >> dev_hosts.txt"
    }
}

output "ICGORE_BAT_INTERNAL_HAPROXY_PRIVATEIP" {
    value = "${aws_instance.ICGORE_BAT_INTERNAL_HAPROXY.private_ip}"
}


######### Perf ###########
resource "aws_instance" "ICGORE_PERF_MICROSERVICES_MANAGER_1" {
    ami = "${var.ami_id_amazonlinux2base}"
    vpc_security_group_ids      = [
        "${aws_security_group.SG_MICROSERVICES.id}",
        "${aws_security_group.SG_AVIATRIX_SSH.id}",
        "${aws_security_group.SG_JENKINS_CLIENT_SSH.id}"
    ]
    subnet_id = "${var.gore-dev-private-1a-subnet}"
    instance_type = "t3a.small"
    key_name    = "GoreMicroservicesDev"
    associate_public_ip_address = false
    iam_instance_profile = "CMS-SSMInstanceRole"
	ebs_optimized = "true"

    root_block_device {
       volume_size = "16"
    }

    tags {
        Owner = "robhatnagar@deloitte.ca"
        ManagedBy = "CMS-TF"
        SolutionName = "Insurcloud Gore"
        Environment = "Dev"
        ApplicationName = "docker, microservices"
        DataClassification = "PII, PHI"
        DLM-Backup = "Daily-1"
        DLM-Retention = "90"
        PatchGroup = "AmazonLinux2-Microsvc1"
        VendorManaged = "False"
        ResourceType = "Client"
        ResourceSeverity = "Normal"
        Name = "A-MC-CAC-D-A003-GORE-PERF-MICROSERVICES-MANAGER-1-LNX-EC2"
        AccountID = "${data.aws_caller_identity.current.account_id}"
        Role = "Container"
        CLIENT = "InsurCloud GORE"
		Account = "GORE-dev"
		Client = "Gore"
		KeyName = "GoreMicroservicesDev"
    }

    provisioner "local-exec" {
        command = "echo ${aws_instance.ICGORE_PERF_MICROSERVICES_MANAGER_1.private_ip} >> dev_hosts.txt"
    }
}

output "ICGORE_PERF_MICROSERVICES_MANAGER_1_PRIVATEIP" {
    value = "${aws_instance.ICGORE_PERF_MICROSERVICES_MANAGER_1.private_ip}"
}

resource "aws_instance" "ICGORE_PERF_MICROSERVICES_MANAGER_2" {
    ami = "${var.ami_id_amazonlinux2base}"
    vpc_security_group_ids      = [
        "${aws_security_group.SG_MICROSERVICES.id}",
        "${aws_security_group.SG_AVIATRIX_SSH.id}",
        "${aws_security_group.SG_JENKINS_CLIENT_SSH.id}"
    ]
    subnet_id = "${var.gore-dev-private-1b-subnet}"
    instance_type = "t3a.small"
    key_name    = "GoreMicroservicesDev"
    associate_public_ip_address = false
    iam_instance_profile = "CMS-SSMInstanceRole"
	ebs_optimized = "true"

    root_block_device {
       volume_size = "16"
    }

    tags {
        Owner = "robhatnagar@deloitte.ca"
        ManagedBy = "CMS-TF"
        SolutionName = "Insurcloud Gore"
        Environment = "Dev"
        ApplicationName = "docker, microservices"
        DataClassification = "PII, PHI"
        DLM-Backup = "Daily-1"
        DLM-Retention = "90"
        PatchGroup = "AmazonLinux2-Microsvc2"
        VendorManaged = "False"
        ResourceType = "Client"
        ResourceSeverity = "Normal"
        Name = "A-MC-CAC-D-A003-GORE-PERF-MICROSERVICES-MANAGER-2-LNX-EC2"
        AccountID = "${data.aws_caller_identity.current.account_id}"
        Role = "Container"
        CLIENT = "InsurCloud GORE"
		Account = "GORE-dev"
		Client = "Gore"
		KeyName = "GoreMicroservicesDev"
    }

    provisioner "local-exec" {
        command = "echo ${aws_instance.ICGORE_PERF_MICROSERVICES_MANAGER_2.private_ip} >> dev_hosts.txt"
    }
}

output "ICGORE_PERF_MICROSERVICES_MANAGER_2_PRIVATEIP" {
    value = "${aws_instance.ICGORE_PERF_MICROSERVICES_MANAGER_2.private_ip}"
}

resource "aws_instance" "ICGORE_PERF_MICROSERVICES_MANAGER_3" {
    ami = "${var.ami_id_amazonlinux2base}"
    vpc_security_group_ids      = [
        "${aws_security_group.SG_MICROSERVICES.id}",
        "${aws_security_group.SG_AVIATRIX_SSH.id}",
        "${aws_security_group.SG_JENKINS_CLIENT_SSH.id}"
    ]
    subnet_id = "${var.gore-dev-private-1a-subnet}"
    instance_type = "t3a.small"
    key_name    = "GoreMicroservicesDev"
    associate_public_ip_address = false
    iam_instance_profile = "CMS-SSMInstanceRole"
	ebs_optimized = "true"

    root_block_device {
       volume_size = "16"
    }

    tags {
        Owner = "robhatnagar@deloitte.ca"
        ManagedBy = "CMS-TF"
        SolutionName = "Insurcloud Gore"
        Environment = "Dev"
        ApplicationName = "docker, microservices"
        DataClassification = "PII, PHI"
        DLM-Backup = "Daily-1"
        DLM-Retention = "90"
        PatchGroup = "AmazonLinux2-Microsvc3"
        VendorManaged = "False"
        ResourceType = "Client"
        ResourceSeverity = "Normal"
        Name = "A-MC-CAC-D-A003-GORE-PERF-MICROSERVICES-MANAGER-3-LNX-EC2"
        AccountID = "${data.aws_caller_identity.current.account_id}"
        Role = "Container"
        CLIENT = "InsurCloud GORE"
		Account = "GORE-dev"
		Client = "Gore"
		KeyName = "GoreMicroservicesDev"
    }

    provisioner "local-exec" {
        command = "echo ${aws_instance.ICGORE_PERF_MICROSERVICES_MANAGER_3.private_ip} >> dev_hosts.txt"
    }
}

output "ICGORE_PERF_MICROSERVICES_MANAGER_3_PRIVATEIP" {
    value = "${aws_instance.ICGORE_PERF_MICROSERVICES_MANAGER_3.private_ip}"
}

resource "aws_instance" "ICGORE_PERF_MICROSERVICES_APPLICATION_1" {
    ami = "${var.ami_id_amazonlinux2base}"
    vpc_security_group_ids = [
        "${aws_security_group.SG_MICROSERVICES.id}",
        "${aws_security_group.SG_MONITORING.id}",
        "${aws_security_group.SG_AVIATRIX_SSH.id}",
        "${aws_security_group.SG_JENKINS_CLIENT_SSH.id}"
    ]
    subnet_id = "${var.gore-dev-private-1a-subnet}"
    instance_type = "t3a.2xlarge"
    key_name    = "GoreMicroservicesDev"
    associate_public_ip_address = false
    iam_instance_profile = "CMS-SSMInstanceRole"
	ebs_optimized = "true"

    root_block_device {
       volume_size = "100"
    }

    tags {
        Owner = "robhatnagar@deloitte.ca"
        ManagedBy = "CMS-TF"
        SolutionName = "Insurcloud Gore"
        Environment = "Dev"
        ApplicationName = "docker, microservices"
        DataClassification = "PII, PHI"
        DLM-Backup = "Daily-7"
        DLM-Retention = "90"
        PatchGroup = "AmazonLinux2-Microsvc4"
        VendorManaged = "False"
        ResourceType = "Client"
        ResourceSeverity = "Normal"
        Name = "A-MC-CAC-D-A003-GORE-PERF-MICROSERVICES-APP-1-LNX-EC2"
        AccountID = "${data.aws_caller_identity.current.account_id}"
        Role = "Container"
        CLIENT = "InsurCloud GORE"
		Account = "GORE-dev"
		Client = "Gore"
		KeyName = "GoreMicroservicesDev"
        BILLINGCODE = "FPE01903-CO-TE-TO-1000"
        BILLINGCONTACT = "marrojas@deloitte.ca"
        COUNTRY = "CA"
        CSCLASS = "Confidential"
        CSQUAL = "CI/PI Data"
        CSTYPE = "External"
        ENVIRONMENT = "NPD"
        FUNCTION = "CON"
        MEMBERFIRM = "CA"
        PRIMARYCONTACT = "marrojas@deloitte.ca"
        SECONDARYCONTACT = "Samir Modh"
    }

    provisioner "local-exec" {
        command = "echo ${aws_instance.ICGORE_PERF_MICROSERVICES_APPLICATION_1.private_ip} >> dev_hosts.txt"
    }
}

output "ICGORE_PERF_MICROSERVICES_APPLICATION_1_PRIVATEIP" {
    value = "${aws_instance.ICGORE_PERF_MICROSERVICES_APPLICATION_1.private_ip}"
}

resource "aws_instance" "ICGORE_PERF_MICROSERVICES_APPLICATION_2" {
    ami = "${var.ami_id_amazonlinux2base}"
    vpc_security_group_ids = [
        "${aws_security_group.SG_MICROSERVICES.id}",
        "${aws_security_group.SG_MONITORING.id}",
        "${aws_security_group.SG_AVIATRIX_SSH.id}",
        "${aws_security_group.SG_JENKINS_CLIENT_SSH.id}"
    ]
    subnet_id = "${var.gore-dev-private-1b-subnet}"
    instance_type = "t3a.2xlarge"
    key_name    = "GoreMicroservicesDev"
    associate_public_ip_address = false
    iam_instance_profile = "CMS-SSMInstanceRole"
	ebs_optimized = "true"

    root_block_device {
       volume_size = "100"
    }
    
    tags {
        Owner = "robhatnagar@deloitte.ca"
        ManagedBy = "CMS-TF"
        SolutionName = "Insurcloud Gore"
        Environment = "Dev"
        ApplicationName = "docker, microservices"
        DataClassification = "PII, PHI"
        DLM-Backup = "Daily-7"
        DLM-Retention = "90"
        PatchGroup = "AmazonLinux2-Microsvc5"
        VendorManaged = "False"
        ResourceType = "Client"
        ResourceSeverity = "Normal"
        Name = "A-MC-CAC-D-A003-GORE-PERF-MICROSERVICES-APP-2-LNX-EC2"
        AccountID = "${data.aws_caller_identity.current.account_id}"
        Role = "Container"
        CLIENT = "InsurCloud GORE"
		Account = "GORE-dev"
		Client = "Gore"
		KeyName = "GoreMicroservicesDev"
        SECONDARYCONTACT = "Samir Modh"
        BILLINGCODE = "FPE01903-CO-TE-TO-1000"
        BILLINGCONTACT = "marrojas@deloitte.ca"
        COUNTRY = "CA"
        CSCLASS = "Confidential"
        CSQUAL = "CI/PI Data"
        CSTYPE = "External"
        ENVIRONMENT = "NPD"
        FUNCTION = "CON"
        MEMBERFIRM = "CA"
        PRIMARYCONTACT = "marrojas@deloitte.ca"       
    }

    provisioner "local-exec" {
        command = "echo ${aws_instance.ICGORE_PERF_MICROSERVICES_APPLICATION_2.private_ip} >> dev_hosts.txt"
    }
}

output "ICGORE_PERF_MICROSERVICES_APPLICATION_2_PRIVATEIP" {
    value = "${aws_instance.ICGORE_PERF_MICROSERVICES_APPLICATION_2.private_ip}"
}

######### HAPROXY #############
# resource "aws_instance" "ICGORE_PERF_EXTERNAL_HAPROXY_1" {
#     ami = "${var.ami_id_amazonlinux2base}"
#     vpc_security_group_ids = [
#         "${aws_security_group.SG_EXTERNAL_HAPROXY.id}",
#         "${aws_security_group.SG_HAPROXY_EXTERNAL_PEERING.id}",
#         "${aws_security_group.SG_AVIATRIX_SSH.id}",
#         "${aws_security_group.SG_JENKINS_CLIENT_SSH.id}"
#     ]
#     subnet_id = "${var.gore-dev-private-1a-subnet}"
#     instance_type = "t3a.small"
#     key_name    = "GoreHAProxy"
#     associate_public_ip_address = false
#     iam_instance_profile = "CMS-SSMInstanceRole"
# 	ebs_optimized = "true"

#     root_block_device {
#        volume_size = "20"
#     }

#     tags {
#         Owner = "robhatnagar@deloitte.ca"
#         ManagedBy = "CMS-TF"
#         SolutionName = "Insurcloud Gore"
#         Environment = "Dev"
#         ApplicationName = "docker, haproxy"
#         DataClassification = "PII, PHI"
#         DLM-Backup = "Daily-1"
#         DLM-Retention = "90"
#         PatchGroup = "AmazonLinux2"
#         VendorManaged = "False"
#         ResourceType = "Client"
#         ResourceSeverity = "Normal"
#         Name = "A-MC-CAC-D-A003-GORE-PERF-EXTERNAL-HAPROXY-1-EC2"
#         AccountID = "${data.aws_caller_identity.current.account_id}"
#         Role = "Container"
#         CLIENT = "InsurCloud GORE"
# 		Account = "GORE-dev"
# 		Client = "Gore"
# 		KeyName = "GoreHAProxy"
#     }
   
#     provisioner "local-exec" {
#         command = "echo ${aws_instance.ICGORE_PERF_EXTERNAL_HAPROXY_1.private_ip} >> dev_hosts.txt"
#     }
# }

# output "ICGORE_PERF_EXTERNAL_HAPROXY_1_PRIVATEIP" {
#     value = "${aws_instance.ICGORE_PERF_EXTERNAL_HAPROXY_1.private_ip}"
# }

# resource "aws_instance" "ICGORE_PERF_EXTERNAL_HAPROXY_2" {
#     ami = "${var.ami_id_amazonlinux2base}"
#     vpc_security_group_ids = [
#         "${aws_security_group.SG_EXTERNAL_HAPROXY.id}",
#         "${aws_security_group.SG_HAPROXY_EXTERNAL_PEERING.id}",
#         "${aws_security_group.SG_AVIATRIX_SSH.id}",
#         "${aws_security_group.SG_JENKINS_CLIENT_SSH.id}"
#     ]
#     subnet_id = "${var.gore-dev-private-1b-subnet}"
#     instance_type = "t3a.small"
#     key_name    = "GoreHAProxy"
#     associate_public_ip_address = false
#     iam_instance_profile = "CMS-SSMInstanceRole"
# 	ebs_optimized = "true"

#     root_block_device {
#        volume_size = "20"
#     }

#     tags {
#         Owner = "robhatnagar@deloitte.ca"
#         ManagedBy = "CMS-TF"
#         SolutionName = "Insurcloud Gore"
#         Environment = "Dev"
#         ApplicationName = "docker, haproxy"
#         DataClassification = "NA"
#         DLM-Backup = "Daily-1"
#         DLM-Retention = "90"
#         PatchGroup = "AmazonLinux2"
#         VendorManaged = "False"
#         ResourceType = "Client"
#         ResourceSeverity = "Normal"
#         Name = "A-MC-CAC-D-A003-GORE-PERF-EXTERNAL-HAPROXY-2-EC2"
#         AccountID = "${data.aws_caller_identity.current.account_id}"
#         Role = "Container"
#         CLIENT = "InsurCloud GORE"
# 		Account = "GORE-dev"
# 		Client = "Gore"
# 		KeyName = "GoreHAProxy"
#     }

#     provisioner "local-exec" {
#         command = "echo ${aws_instance.ICGORE_PERF_EXTERNAL_HAPROXY_2.private_ip} >> dev_hosts.txt"
#     }
# }

# output "ICGORE_PERF_EXTERNAL_HAPROXY_2_PRIVATEIP" {
#     value = "${aws_instance.ICGORE_PERF_EXTERNAL_HAPROXY_2.private_ip}"
#}

resource "aws_instance" "ICGORE_PERF_INTERNAL_HAPROXY_1" {
    ami = "${var.ami_id_amazonlinux2base}"
    vpc_security_group_ids = [
        "${aws_security_group.SG_INTERNAL_HAPROXY.id}",
        "${aws_security_group.SG_HAPROXY_INTERNAL_PEERING.id}",
        "${aws_security_group.SG_AVIATRIX_SSH.id}",
        "${aws_security_group.SG_JENKINS_CLIENT_SSH.id}",
        "${aws_security_group.SG_CLIENT_PING_TEST.id}"
    ]
    subnet_id = "${var.gore-dev-private-1a-subnet}"
    instance_type = "t3a.small"
    key_name    = "GoreHAProxyDev"
    associate_public_ip_address = false
    iam_instance_profile = "CMS-SSMInstanceRole"
	ebs_optimized = "true"

    root_block_device {
       volume_size = "20"
    }

    tags {
        Owner = "robhatnagar@deloitte.ca"
        ManagedBy = "CMS-TF"
        SolutionName = "Insurcloud Gore"
        Environment = "Dev"
        ApplicationName = "docker, haproxy"
        DataClassification = "PII, PHI"
        DLM-Backup = "Daily-1"
        DLM-Retention = "90"
        PatchGroup = "AmazonLinux2"
        VendorManaged = "False"
        ResourceType = "Client"
        ResourceSeverity = "Normal"
        Name = "A-MC-CAC-D-A003-GORE-PERF-INTERNAL-HAPROXY-1-EC2"
        AccountID = "${data.aws_caller_identity.current.account_id}"
        Role = "Container"
        CLIENT = "InsurCloud GORE"
		Account = "GORE-dev"
		Client = "Gore"
		KeyName = "GoreHAProxyDev"
    }

    provisioner "local-exec" {
        command = "echo ${aws_instance.ICGORE_PERF_INTERNAL_HAPROXY_1.private_ip} >> dev_hosts.txt"
    }
}

output "ICGORE_PERF_INTERNAL_HAPROXY_1_PRIVATEIP" {
    value = "${aws_instance.ICGORE_PERF_INTERNAL_HAPROXY_1.private_ip}"
}

resource "aws_instance" "ICGORE_PERF_INTERNAL_HAPROXY_2" {
    ami = "${var.ami_id_amazonlinux2base}"
    vpc_security_group_ids = [
        "${aws_security_group.SG_INTERNAL_HAPROXY.id}",
        "${aws_security_group.SG_HAPROXY_INTERNAL_PEERING.id}",
        "${aws_security_group.SG_AVIATRIX_SSH.id}",
        "${aws_security_group.SG_JENKINS_CLIENT_SSH.id}",
        "${aws_security_group.SG_CLIENT_PING_TEST.id}"
    ]
    subnet_id = "${var.gore-dev-private-1b-subnet}"
    instance_type = "t3a.small"
    key_name    = "GoreHAProxyDev"
    associate_public_ip_address = false
    iam_instance_profile = "CMS-SSMInstanceRole"
	ebs_optimized = "true"

    root_block_device {
       volume_size = "20"
    }

    tags {
        Owner = "robhatnagar@deloitte.ca"
        ManagedBy = "CMS-TF"
        SolutionName = "Insurcloud Gore"
        Environment = "Dev"
        ApplicationName = "docker, haproxy"
        DataClassification = "NA"
        DLM-Backup = "Daily-1"
        DLM-Retention = "90"
        PatchGroup = "AmazonLinux2"
        VendorManaged = "False"
        ResourceType = "Client"
        ResourceSeverity = "Normal"
        Name = "A-MC-CAC-D-A003-GORE-PERF-INTERNAL-HAPROXY-2-EC2"
        AccountID = "${data.aws_caller_identity.current.account_id}"
        Role = "Container"
        CLIENT = "InsurCloud GORE"
		Account = "GORE-dev"
		Client = "Gore"
		KeyName = "GoreHAProxyDev"
    }

    provisioner "local-exec" {
        command = "echo ${aws_instance.ICGORE_PERF_INTERNAL_HAPROXY_2.private_ip} >> dev_hosts.txt"
    }
}

output "ICGORE_PERF_INTERNAL_HAPROXY_2_PRIVATEIP" {
    value = "${aws_instance.ICGORE_PERF_INTERNAL_HAPROXY_2.private_ip}"
}


# #=======================================================================
# # Jobscheduler 
# #=======================================================================
resource "aws_instance" "ICGORE_DEV_JOB_SCHEDULER" {
    ami = "${var.ami_id_amazonlinux2base}"
    vpc_security_group_ids      = [
        "${aws_security_group.SG_JOB_SCHEDULER.id}",
        "${aws_security_group.SG_AVIATRIX_SSH.id}",
        "${aws_security_group.SG_JENKINS_CLIENT_SSH.id}"
    ]
    subnet_id = "${var.gore-dev-private-1a-subnet}"
    instance_type = "t3a.medium"
    key_name    = "GoreOtherDev"
    associate_public_ip_address = false
    iam_instance_profile = "CMS-SSMInstanceRole"
	ebs_optimized = "true"

    root_block_device {
       volume_size = "40"
       delete_on_termination = "false"
    }

    tags {
        Client = "GoreMutual"
        Account = "GORE-MUTUAL-dev"
        Owner = "robhatnagar@deloitte.ca"
        ManagedBy = "CMS-TF"
        SolutionName = "Insurcloud Gore"
        Environment = "Dev"
        ApplicationName = "jobscheduler"
        DataClassification = "PII, PHI"
        DLM-Backup = "Yes"
        DLM-Retention = "90"
        PatchGroup = "AmazonLinux2"
        VendorManaged = "False"
        ResourceType = "Client"
        ResourceSeverity = "Normal"
        Name = "A-MC-CAC-D-A003-GORE-DEV-JOB-SCHEDULER-LNX-EC2"
        AccountID = "${data.aws_caller_identity.current.account_id}"
        Role = "Container"
        Role = "Container"
    }
}



# #=======================================================================
# # OpenText 
# #=======================================================================
######### OPENTEXT ##############
data "template_file" "GORE_DOMAIN_JOIN_1" {
    template = "${file("IC-GoreMutual-DEV/windowsdata.tpl")}"

    vars {
        computer_name = "OTCS-COMM-WIN"
        region              = "${var.region}"
        domain              = "${var.ad_domain}"
        dns1                = "${var.ad_dns1}"
        dns2                = "${var.ad_dns2}"
        dns3                = "${var.ad_dns3}"
        ouselection         = "${var.ou_name}"
        pwordparameter      = "${var.pwordparameter}"
        unameparameter      = "${var.unameparameter}"
    }
}

resource "aws_instance" "ICGORE_DEV_OPENTEXT_COM_SERVER" {
    ami                         = "${var.ami_id_windows2019base}"
    vpc_security_group_ids      = [
        "${aws_security_group.SG_OPENTEXT.id}",
        "${aws_security_group.SG_AVIATRIX_RDP.id}"
    ]
    subnet_id                   = "${var.gore-dev-private-1a-subnet}"
    instance_type               = "i3.2xlarge"
    key_name                    = "GoreApplicationsDev"
    associate_public_ip_address = false
    iam_instance_profile        = "CMS-SSMInstanceRole"
	ebs_optimized = "true"

    lifecycle {
        prevent_destroy = false
    }

    root_block_device {
        volume_size = "600"
    }

    user_data = "${data.template_file.GORE_DOMAIN_JOIN_1.rendered}"

    tags {
        Client = "GoreMutual"
        Account = "GORE-MUTUAL-dev"
        Owner = "robhatnagar@deloitte.ca"
        ManagedBy = "CMS-TF"
        SolutionName = "Insurcloud Gore"
        Environment = "Dev"
        ApplicationName = "opentext, exstream"
        DataClassification = "PII, PHI"
        DLM-Backup = "Yes"
        DLM-Retention = "90"
        PatchGroup = "Windows"
        VendorManaged = "False"
        OSName = "Windows"
        ResourceType = "Client"
        ResourceSeverity = "Normal"
        Name = "A-MC-CAC-D-A003-GORE-DEV-OPENTEXT-COMMUNICATIONS-WIN-EC2"
        AccountID = "${data.aws_caller_identity.current.account_id}"
        Role = ""
    }
}

output "ICGORE_DEV_OPENTEXT_COM_SERVER_PRIVATEIP" {
    value = "${aws_instance.ICGORE_DEV_OPENTEXT_COM_SERVER.private_ip}"
}
