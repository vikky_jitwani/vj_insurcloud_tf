// #=======================================================================
// # HUB
// #=======================================================================

resource "aws_route53_record" "ICECONOMICAL_NEXUS_REG_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "nexus.economical.insurcloud.ca"
    type    = "A"

    alias {
        name                   = "internal-cicd-internal-alb-2023433918.ca-central-1.elb.amazonaws.com"
        zone_id                = "ZQSVJUPU6J1EY"
        evaluate_target_health = false
    }
}

resource "aws_route53_record" "ICECONOMICAL_NEXUS_PROD_REG_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "nexus-prod.economical.insurcloud.ca"
    type    = "A"

    alias {
        name                   = "internal-cicd-internal-alb-2023433918.ca-central-1.elb.amazonaws.com"
        zone_id                = "ZQSVJUPU6J1EY"
        evaluate_target_health = false
    }
}

resource "aws_route53_record" "ICECONOMICAL_NEXUS_UI_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "nexus-ui.economical.insurcloud.ca"
    type    = "A"

    alias {
        name                   = "internal-cicd-internal-alb-2023433918.ca-central-1.elb.amazonaws.com"
        zone_id                = "ZQSVJUPU6J1EY"
        evaluate_target_health = false
    }
}

resource "aws_route53_record" "ICECONOMICAL_SONARQUBE_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "sonarqube.economical.insurcloud.ca"
    type    = "A"

    alias {
        name                   = "internal-cicd-internal-alb-2023433918.ca-central-1.elb.amazonaws.com"
        zone_id                = "ZQSVJUPU6J1EY"
        evaluate_target_health = false
    }
}

resource "aws_route53_record" "ICECONOMICAL_CONSUL_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "consul.economical.insurcloud.ca"
    type    = "A"

    alias {
        name                   = "internal-cicd-internal-alb-2023433918.ca-central-1.elb.amazonaws.com"
        zone_id                = "ZQSVJUPU6J1EY"
        evaluate_target_health = false
    }
}

resource "aws_route53_record" "ICECONOMICAL_JENKINS_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "jenkins.economical.insurcloud.ca"
    type    = "A"

    alias {
        name                   = "${aws_lb.ICECO_INTERNAL_ALB_ICHUB.dns_name}"
        zone_id                = "${aws_lb.ICECO_INTERNAL_ALB_ICHUB.zone_id}"
        evaluate_target_health = false
    }
}

resource "aws_route53_record" "ICECONOMICAL_VAULT_DEV_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "vault-dev.economical.insurcloud.ca"
    type    = "A"

    alias {
        name                   = "${aws_lb.ICECO_INTERNAL_ALB_ICHUB.dns_name}"
        zone_id                = "${aws_lb.ICECO_INTERNAL_ALB_ICHUB.zone_id}"
        evaluate_target_health = false
    }
}

resource "aws_route53_record" "ICECONOMICAL_VAULT_PROD_RECORD" {
    zone_id = "${var.zone_id}"
    name    = "vault-prod.economical.insurcloud.ca"
    type    = "A"

    alias {
        name                   = "${aws_lb.ICECO_INTERNAL_ALB_ICHUB.dns_name}"
        zone_id                = "${aws_lb.ICECO_INTERNAL_ALB_ICHUB.zone_id}"
        evaluate_target_health = false
    }
}

resource "aws_route53_record" "ICECONOMICAL_VAULT_PROD_RECORD2" {
    zone_id = "${var.zone_id}"
    name    = "vault.economical.insurcloud.ca"
    type    = "A"

    alias {
        name                   = "${aws_lb.ICECO_INTERNAL_ALB_ICHUB.dns_name}"
        zone_id                = "${aws_lb.ICECO_INTERNAL_ALB_ICHUB.zone_id}"
        evaluate_target_health = false
    }
}
