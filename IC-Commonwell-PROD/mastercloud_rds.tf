
resource "aws_db_subnet_group" "MSSQL_DB_SUBNET" {
    name        = "mssql-db-subnet"
    description = "RDS subnet group"
    subnet_ids  = ["${var.commonwell-prod-database-1a-subnet}", "${var.commonwell-prod-database-1b-subnet}"]
}

resource "aws_db_subnet_group" "MYSQL_DB_SUBNET" {
    name        = "mysql-db-subnet"
    description = "RDS subnet group"
    subnet_ids  = ["${var.commonwell-prod-database-1a-subnet}", "${var.commonwell-prod-database-1b-subnet}"]
}

resource "aws_db_subnet_group" "POSTGRES_DB_SUBNET" {
    name        = "postgres-db-subnet"
    description = "RDS subnet group"
    subnet_ids  = ["${var.commonwell-prod-database-1a-subnet}", "${var.commonwell-prod-database-1b-subnet}"]
}

# Microservices Database
resource "aws_db_instance" "mssqldb" {

    license_model     = "license-included"
    engine            = "sqlserver-se"
    engine_version    = "14.00.3049.1.v1"
    instance_class    = "db.t3.xlarge"
    allocated_storage = 500
    storage_encrypted = true
    identifier        = "cmigprod01msdb"
    username          = "sa"
    password          = "${var.mssql-rds-password}" # password
    port              = 1433
    timezone          = "Eastern Standard Time"

    maintenance_window = "Mon:00:00-Mon:03:00"
    backup_window      = "03:00-06:00"
    deletion_protection = true
    // enabled_cloudwatch_logs_exports = ["general"] #engine version does not support this

    db_subnet_group_name  = "${aws_db_subnet_group.MSSQL_DB_SUBNET.name}"
    parameter_group_name  = "default.sqlserver-se-14.0"
    multi_az              = true # set to true to have high availability: 2 instances synchronized with each other
    vpc_security_group_ids  = ["${aws_security_group.SG_MSSQL_DB.id}"]
    storage_type            = "gp2"
    backup_retention_period = 30 # how long you’re going to keep your backups
    kms_key_id = "arn:aws:kms:ca-central-1:942574907772:key/54cbeb6a-1ccf-42df-8af9-cc129aae0ffd"

    tags = {
        Name = "cmig-prod01-msdb"
        Environment = "PROD"
    }
}

# JobScheduler Database
resource "aws_db_parameter_group" "jobscheduler" {
  name   = "cmigjobscheduler"
  family = "mysql8.0"

  parameter {
    name  = "time_zone"
    value = "US/Eastern" # MySQL has no America/Toronto time zone
  }

  parameter {
    name = "log_output"
    value = "FILE"
  }

  parameter {
    name = "general_log"
    value = "1"
  }

  parameter {
    name = "slow_query_log"
    value = "1"
  }

  parameter {
    name = "long_query_time"
    value = "2"
  }
}

resource "aws_db_instance" "mysqldb" {

    engine            = "mysql"
    engine_version    = "8.0.17"
    instance_class    = "db.t3.large"
    allocated_storage = 100
    storage_encrypted = true
    identifier        = "cmigjobscheduler"
    name              = "cmigjobscheduler"
    username          = "sa"
    password          = "${var.mysql-rds-password}" # password
    port              = 3306

    maintenance_window = "Mon:00:00-Mon:03:00"
    backup_window      = "03:00-06:01"
    deletion_protection = true
    enabled_cloudwatch_logs_exports = ["error", "general", "slowquery"]
    apply_immediately  = true

    db_subnet_group_name  = "${aws_db_subnet_group.MYSQL_DB_SUBNET.name}"
    parameter_group_name  = "${aws_db_parameter_group.jobscheduler.id}"
    multi_az              = true # set to true to have high availability: 2 instances synchronized with each other
    vpc_security_group_ids  = ["${aws_security_group.SG_MYSQL_DB.id}"]
    storage_type            = "gp2"
    backup_retention_period = 30 # how long you’re going to keep your backups
    kms_key_id = "arn:aws:kms:ca-central-1:942574907772:key/54cbeb6a-1ccf-42df-8af9-cc129aae0ffd"

    tags = {
        Name = "cmigjobscheduler"
        Environment = "PROD"
    }
}

# Hubio Database
resource "aws_db_parameter_group" "hubio" {
  name   = "cmigprod01hubio"
  family = "postgres10"

  parameter {
    name  = "timezone"
    value = "America/Toronto"
  }
}

resource "aws_db_instance" "postgresdb" {

    engine            = "postgres"
    engine_version    = "10.13"
    instance_class    = "db.t3.medium"
    allocated_storage = 50
    storage_encrypted = true
    identifier        = "cmigprod01hubio"
    name              = "cmigprod01hubio"
    username          = "sa"
    password          = "${var.postgres-rds-password}" # password
    port              = 5432

    maintenance_window = "Mon:00:00-Mon:03:00"
    backup_window      = "03:00-06:00"
    deletion_protection = true
    enabled_cloudwatch_logs_exports = ["postgresql", "upgrade"]

    db_subnet_group_name  = "${aws_db_subnet_group.POSTGRES_DB_SUBNET.name}"
    parameter_group_name  = "${aws_db_parameter_group.hubio.id}"
    multi_az              = true # set to true to have high availability: 2 instances synchronized with each other
    vpc_security_group_ids  = ["${aws_security_group.SG_POSTGRES_DB.id}"]
    storage_type            = "gp2"
    backup_retention_period = 30 # how long you’re going to keep your backups
    kms_key_id = "arn:aws:kms:ca-central-1:942574907772:key/54cbeb6a-1ccf-42df-8af9-cc129aae0ffd"

    tags = {
        Name = "cmig-prod01-hubio"
        Environment = "PROD"
    }
}

