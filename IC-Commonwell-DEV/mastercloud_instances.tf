
#=======================================================================
# Guidewire
#=======================================================================
resource "aws_instance" "ICCW_GUIDEWIRE_APPLICATION_1" {
    ami = "${var.ami_id_amazonlinux2base}"
    vpc_security_group_ids = [
		"${aws_security_group.SG_GUIDEWIRE.id}",
		"${aws_security_group.SG_GUIDEWIRE_CLUSTER_NBIO_EVENT_QUEUE.id}",
		"${aws_security_group.SG_JENKINS_GUIDEWIRE_HEALTH_CHECK.id}",
		"${aws_security_group.SG_AVIATRIX_SSH.id}",
		"${aws_security_group.SG_COMMONWELL_JENKINS_CLIENT_SSH.id}"
	]
	subnet_id = "${var.commonwell-non-prod-guidewire-containers-1a-subnet}"
    instance_type = "r5a.2xlarge"
    key_name    = "CommonwellGuidewire"
    associate_public_ip_address = false
	private_ip     = "10.10.65.56"
	iam_instance_profile = "CmigServiceRole"

	root_block_device {
       volume_size = "100"
	}

	tags {
		Owner = "robhatnagar@deloitte.ca"
		ManagedBy = "CMS-TF"
		SolutionName = "Insurcloud Commonwell"
		Environment = "Dev"
		ApplicationName = "Guidewire, docker"
		DataClassification = "PII, PHI"
		DLM-Backup = "Daily-1"
		DLM-Retention = "90"
		PatchGroup = "AmazonLinux2"
		VendorManaged = "False"
		ResourceType = "Client"
		ResourceSeverity = "Normal"
		Name = "A-MC-CAC-D-A003-COMMONWELL-PPS-GW-APP-1-LNX-EC2"
		AccountID = "${data.aws_caller_identity.current.account_id}"
		Role = "Container"
		NexusUse = "False"
		Account = "COMMONWELL-dev"
		Client = "cmig"
		KeyName = "CommonwellGuidewire"
	}	

	provisioner "local-exec" {
    	command = "echo ${aws_instance.ICCW_GUIDEWIRE_APPLICATION_1.private_ip} >> dev_hosts.txt"
  	}

}

output "ICCW_GUIDEWIRE_APPLICATION_1_PRIVATEIP" {
  value = "${aws_instance.ICCW_GUIDEWIRE_APPLICATION_1.private_ip}"
}

resource "aws_instance" "ICCW_GUIDEWIRE_APPLICATION_2" {
    ami = "${var.ami_id_amazonlinux2base}"
    vpc_security_group_ids = [
		"${aws_security_group.SG_GUIDEWIRE.id}",
		"${aws_security_group.SG_GUIDEWIRE_CLUSTER_NBIO_EVENT_QUEUE.id}",
		"${aws_security_group.SG_JENKINS_GUIDEWIRE_HEALTH_CHECK.id}",
		"${aws_security_group.SG_AVIATRIX_SSH.id}",
		"${aws_security_group.SG_COMMONWELL_JENKINS_CLIENT_SSH.id}"
	]
	subnet_id = "${var.commonwell-non-prod-guidewire-containers-1b-subnet}"
    instance_type = "r5a.2xlarge"
    key_name    = "CommonwellGuidewire"
    associate_public_ip_address = false
	iam_instance_profile = "CmigServiceRole"

	root_block_device {
       volume_size = "100"
	}

	tags {
		Owner = "robhatnagar@deloitte.ca"
		ManagedBy = "CMS-TF"
		SolutionName = "Insurcloud Commonwell"
		Environment = "Dev"
		ApplicationName = "Guidewire, docker"
		DataClassification = "PII, PHI"
		DLM-Backup = "Daily-1"
		DLM-Retention = "90"
		PatchGroup = "AmazonLinux2"
		VendorManaged = "False"
		ResourceType = "Client"
		ResourceSeverity = "Normal"
		Name = "A-MC-CAC-D-A003-COMMONWELL-PPS-GW-APP-2-LNX-EC2"
		AccountID = "${data.aws_caller_identity.current.account_id}"
		Role = "Container"
		NexusUse = "False"
		Account = "COMMONWELL-dev"
		Client = "cmig"
		KeyName = "CommonwellGuidewire"
	}	

	provisioner "local-exec" {
    	command = "echo ${aws_instance.ICCW_GUIDEWIRE_APPLICATION_2.private_ip} >> dev_hosts.txt"
  	}

}

output "ICCW_GUIDEWIRE_APPLICATION_2_PRIVATEIP" {
  value = "${aws_instance.ICCW_GUIDEWIRE_APPLICATION_2.private_ip}"
}

resource "aws_instance" "ICCW_GUIDEWIRE_DATABASE" {
    ami = "${var.ami_id_windows2012base}"
	vpc_security_group_ids      = [
		"${aws_security_group.SG_MSSQL_DB.id}",
		"${aws_security_group.SG_AVIATRIX_RDP.id}",
		"${aws_security_group.SG_AVIATRIX_SSH.id}",
		"${aws_security_group.SG_COMMONWELL_JENKINS_CLIENT_SSH.id}"

	]
	subnet_id = "${var.commonwell-non-prod-guidewire-database-1a-subnet}"
    instance_type = "r5a.large"
    key_name    = "CommonwellDatabase"
    associate_public_ip_address = false
	private_ip     = "10.10.68.148"
	iam_instance_profile = "CmigServiceRole"

	root_block_device {
       volume_size = "800"
	   delete_on_termination = false
	}

	tags {
		Owner = "robhatnagar@deloitte.ca"
		ManagedBy = "CMS-TF"
		SolutionName = "Insurcloud Commonwell"
		Environment = "Dev"
		ApplicationName = "MSSQL"
		DataClassification = "PII, PHI"
		DLM-Backup = "Daily-7"
		DLM-Retention = "90"
		PatchGroup = "Windows"
		VendorManaged = "false"
		ResourceType = "Client"
		ResourceSeverity = "Normal"
		Name = "A-MC-CAC-D-A003-COMMONWELL-PPS-GW-DB-WIN-EC2"
		AccountID = "${data.aws_caller_identity.current.account_id}"
		Role = "Container"
		NexusUse = "False"
		Account = "COMMONWELL-dev"
		Client = "cmig"
		KeyName = "CommonwellDatabase"
	}	

	provisioner "local-exec" {
    	command = "echo ${aws_instance.ICCW_GUIDEWIRE_DATABASE.private_ip} >> dev_hosts.txt"
  	}
	  
}

output "ICCW_GUIDEWIRE_DATABASE_PRIVATEIP" {
  value = "${aws_instance.ICCW_GUIDEWIRE_DATABASE.private_ip}"
}

#=======================================================================
# Microservices
#=======================================================================
resource "aws_instance" "ICCW_MICROSERVICES_MANAGER_1" {
    ami = "ami-0fb08d3473db719ce"
	vpc_security_group_ids      = [
		"${aws_security_group.SG_COMMONWELL_MICROSERVICES.id}",
		"${aws_security_group.SG_AVIATRIX_SSH.id}",
		"${aws_security_group.SG_COMMONWELL_JENKINS_CLIENT_SSH.id}"
	]
	subnet_id = "${var.commonwell-non-prod-spring-microservices-1a-subnet}"
    instance_type = "t3a.small"
    key_name    = "CommonwellMicroservices"
    associate_public_ip_address = false
	iam_instance_profile = "CmigServiceRole"
	ebs_optimized = true

	root_block_device {
       volume_size = "16"
	}

	tags {
		Owner = "robhatnagar@deloitte.ca"
		ManagedBy = "CMS-TF"
		SolutionName = "Insurcloud Commonwell"
		Environment = "Dev"
		ApplicationName = "docker, microservices"
		DataClassification = "PII, PHI"
		DLM-Backup = "Daily-1"
		DLM-Retention = "90"
		PatchGroup = "AmazonLinux2-Microsvc1"
		VendorManaged = "False"
		ResourceType = "Client"
		ResourceSeverity = "Normal"
		Name = "A-MC-CAC-D-A003-COMMONWELL-PPS-MICROSERVICES-MANAGER-1-LNX-EC2"
		AccountID = "${data.aws_caller_identity.current.account_id}"
		Role = "Container"
		NexusUse = "False"
		Account = "COMMONWELL-dev"
		Client = "cmig"
		KeyName = "CommonwellMicroservices"
	}

	provisioner "local-exec" {
    	command = "echo ${aws_instance.ICCW_MICROSERVICES_MANAGER_1.private_ip} >> dev_hosts.txt"
  	}
}

output "ICCW_MICROSERVICES_MANAGER_1_PRIVATEIP" {
  value = "${aws_instance.ICCW_MICROSERVICES_MANAGER_1.private_ip}"
}

resource "aws_instance" "ICCW_MICROSERVICES_MANAGER_2" {
    ami = "ami-052a8e1df57c60ca3"
	vpc_security_group_ids      = [
		"${aws_security_group.SG_COMMONWELL_MICROSERVICES.id}",
		"${aws_security_group.SG_AVIATRIX_SSH.id}",
		"${aws_security_group.SG_COMMONWELL_JENKINS_CLIENT_SSH.id}"
	]
	subnet_id = "${var.commonwell-non-prod-spring-microservices-1b-subnet}"
    instance_type = "t3a.small"
    key_name    = "CommonwellMicroservices"
    associate_public_ip_address = false
	iam_instance_profile = "CmigServiceRole"
	ebs_optimized = true

	root_block_device {
       volume_size = "16"
	}

	tags {
		Owner = "robhatnagar@deloitte.ca"
		ManagedBy = "CMS-TF"
		SolutionName = "Insurcloud Commonwell"
		Environment = "Dev"
		ApplicationName = "docker, microservices"
		DataClassification = "PII, PHI"
		DLM-Backup = "Daily-1"
		DLM-Retention = "90"
		PatchGroup = "AmazonLinux2-Microsvc2"
		VendorManaged = "False"
		ResourceType = "Client"
		ResourceSeverity = "Normal"
		Name = "A-MC-CAC-D-A003-COMMONWELL-PPS-MICROSERVICES-MANAGER-2-LNX-EC2"
		AccountID = "${data.aws_caller_identity.current.account_id}"
		Role = "Container"
		NexusUse = "True"
		Account = "COMMONWELL-dev"
		Client = "cmig"
		KeyName = "CommonwellMicroservices"
	}

	provisioner "local-exec" {
    	command = "echo ${aws_instance.ICCW_MICROSERVICES_MANAGER_2.private_ip} >> dev_hosts.txt"
  	}
}

output "ICCW_MICROSERVICES_MANAGER_2_PRIVATEIP" {
  value = "${aws_instance.ICCW_MICROSERVICES_MANAGER_2.private_ip}"
}

resource "aws_instance" "ICCW_MICROSERVICES_MANAGER_3" {
    ami = "ami-08299469cc55ecae3"
    vpc_security_group_ids      = [
		"${aws_security_group.SG_COMMONWELL_MICROSERVICES.id}",
		"${aws_security_group.SG_AVIATRIX_SSH.id}",
		"${aws_security_group.SG_COMMONWELL_JENKINS_CLIENT_SSH.id}"
	]
	subnet_id = "${var.commonwell-non-prod-spring-microservices-1a-subnet}"
    instance_type = "t3a.small"
    key_name    = "CommonwellMicroservices"
    associate_public_ip_address = false
	iam_instance_profile = "CmigServiceRole"
	ebs_optimized = true

	root_block_device {
       volume_size = "16"
	}

	tags {
		Owner = "robhatnagar@deloitte.ca"
		ManagedBy = "CMS-TF"
		SolutionName = "Insurcloud Commonwell"
		Environment = "Dev"
		ApplicationName = "docker, microservices"
		DataClassification = "PII, PHI"
		DLM-Backup = "Daily-1"
		DLM-Retention = "90"
		PatchGroup = "AmazonLinux2-Microsvc3"
		VendorManaged = "False"
		ResourceType = "Client"
		ResourceSeverity = "Normal"
		Name = "A-MC-CAC-D-A003-COMMONWELL-PPS-MICROSERVICES-MANAGER-3-LNX-EC2"
		AccountID = "${data.aws_caller_identity.current.account_id}"
		Role = "Container"
		NexusUse = "True"
		Account = "COMMONWELL-dev"
		Client = "cmig"
		KeyName = "CommonwellMicroservices"
	}

	provisioner "local-exec" {
    	command = "echo ${aws_instance.ICCW_MICROSERVICES_MANAGER_3.private_ip} >> dev_hosts.txt"
  	}
}

output "ICCW_MICROSERVICES_MANAGER_3_PRIVATEIP" {
  value = "${aws_instance.ICCW_MICROSERVICES_MANAGER_3.private_ip}"
}

resource "aws_instance" "ICCW_MICROSERVICES_APPLICATION_1" {
    ami = "${var.ami_id_amazonlinux2base}"
    vpc_security_group_ids = [
		"${aws_security_group.SG_COMMONWELL_MICROSERVICES.id}",
		"${aws_security_group.SG_COMMONWELL_PROMETHEUS_GRAFANA.id}",
		"${aws_security_group.SG_AVIATRIX_SSH.id}",
		"${aws_security_group.SG_COMMONWELL_JENKINS_CLIENT_SSH.id}"
	]
	subnet_id = "${var.commonwell-non-prod-spring-microservices-1a-subnet}"
    instance_type = "r5a.2xlarge"
    key_name    = "CommonwellMicroservices"
    associate_public_ip_address = false
	private_ip = "10.10.67.98"
	iam_instance_profile = "CmigServiceRole"
	ebs_optimized = true

	root_block_device {
       volume_size = "50"
	}

	tags {
		Owner = "robhatnagar@deloitte.ca"
		ManagedBy = "CMS-TF"
		SolutionName = "Insurcloud Commonwell"
		Environment = "Dev"
		ApplicationName = "docker, microservices"
		DataClassification = "PII, PHI"
		DLM-Backup = "Daily-7"
		DLM-Retention = "90"
		PatchGroup = "AmazonLinux2-Microsvc4"
		VendorManaged = "False"
		ResourceType = "Client"
		ResourceSeverity = "Normal"
		Name = "A-MC-CAC-D-A003-COMMONWELL-PPS-MICROSERVICES-APP-1-LNX-EC2"
		AccountID = "${data.aws_caller_identity.current.account_id}"
		Role = "Container"
		NexusUse = "True"
		Account = "COMMONWELL-dev"
		Client = "cmig"
		KeyName = "CommonwellMicroservices"
	}

	provisioner "local-exec" {
    	command = "echo ${aws_instance.ICCW_MICROSERVICES_APPLICATION_1.private_ip} >> dev_hosts.txt"
  	}
}

output "ICCW_MICROSERVICES_APPLICATION_1_PRIVATEIP" {
  value = "${aws_instance.ICCW_MICROSERVICES_APPLICATION_1.private_ip}"
}

resource "aws_instance" "ICCW_MICROSERVICES_APPLICATION_2" {
    ami = "${var.ami_id_amazonlinux2base}"
    vpc_security_group_ids = [
		"${aws_security_group.SG_COMMONWELL_MICROSERVICES.id}",
		"${aws_security_group.SG_COMMONWELL_PROMETHEUS_GRAFANA.id}",
		"${aws_security_group.SG_AVIATRIX_SSH.id}",
		"${aws_security_group.SG_COMMONWELL_JENKINS_CLIENT_SSH.id}"
	]
	subnet_id = "${var.commonwell-non-prod-spring-microservices-1b-subnet}"
    instance_type = "r5a.2xlarge"
    key_name    = "CommonwellMicroservices"
    associate_public_ip_address = false
	iam_instance_profile = "CmigServiceRole"
	ebs_optimized = true

	root_block_device {
       volume_size = "100"
	}
	
	tags {
		Owner = "robhatnagar@deloitte.ca"
		ManagedBy = "CMS-TF"
		SolutionName = "Insurcloud Commonwell"
		Environment = "Dev"
		ApplicationName = "docker, microservices"
		DataClassification = "PII, PHI"
		DLM-Backup = "Daily-7"
		DLM-Retention = "90"
		PatchGroup = "AmazonLinux2-Microsvc5"
		VendorManaged = "False"
		ResourceType = "Client"
		ResourceSeverity = "Normal"
		Name = "A-MC-CAC-D-A003-COMMONWELL-PPS-MICROSERVICES-APP-2-LNX-EC2"
		AccountID = "${data.aws_caller_identity.current.account_id}"
		Role = "Container"
		NexusUse = "True"
		Account = "COMMONWELL-dev"
		Client = "cmig"
		KeyName = "CommonwellMicroservices"
	}

	provisioner "local-exec" {
    	command = "echo ${aws_instance.ICCW_MICROSERVICES_APPLICATION_2.private_ip} >> dev_hosts.txt"
  	}
}

output "ICCW_MICROSERVICES_APPLICATION_2_PRIVATEIP" {
  value = "${aws_instance.ICCW_MICROSERVICES_APPLICATION_2.private_ip}"
}

#=======================================================================
# HAProxies
#=======================================================================
resource "aws_instance" "ICCW_EXTERNAL_HA_PROXY_N1" {
    ami = "${var.ami_id_amazonlinux2base}"
    vpc_security_group_ids = [
		"${aws_security_group.SG_EXTERNAL_HAPROXY.id}",
		"${aws_security_group.SG_HAPROXY_EXTERNAL_PEERING.id}",
		"${aws_security_group.SG_AVIATRIX_SSH.id}",
		"${aws_security_group.SG_COMMONWELL_JENKINS_CLIENT_SSH.id}"
	]
	subnet_id = "${var.commonwell-non-prod-private-1a-subnet}"
    instance_type = "t3a.small"
    key_name    = "CommonwellHAProxy"
    associate_public_ip_address = false
	private_ip     = "10.10.70.10"
	iam_instance_profile = "CmigServiceRole"

	root_block_device {
       volume_size = "10"
	}

	tags {
		Owner = "robhatnagar@deloitte.ca"
		ManagedBy = "CMS-TF"
		SolutionName = "Insurcloud Commonwell"
		Environment = "Dev"
		ApplicationName = "docker, microservices"
		DataClassification = "PII, PHI"
		DLM-Backup = "Daily-1"
		DLM-Retention = "90"
		PatchGroup = "AmazonLinux2"
		VendorManaged = "False"
		ResourceType = "Client"
		ResourceSeverity = "Normal"
		Name = "A-MC-CAC-D-A003-COMMONWELL-PPS-EXTERNAL-HA-PROXY-N1-EC2"
		AccountID = "${data.aws_caller_identity.current.account_id}"
		Role = "Container"
		NexusUse = "True"
		Account = "COMMONWELL-dev"
		Client = "cmig"
		KeyName = "CommonwellHAProxy"
	}
   
	provisioner "local-exec" {
    	command = "echo ${aws_instance.ICCW_EXTERNAL_HA_PROXY_N1.private_ip} >> dev_hosts.txt"
  	}
}

output "ICCW_INTERNAL_HA_PROXY_N1_PRIVATEIP" {
  value = "${aws_instance.ICCW_INTERNAL_HA_PROXY_N1.private_ip}"
}

resource "aws_instance" "ICCW_EXTERNAL_HA_PROXY_N2" {
    ami = "${var.ami_id_amazonlinux2base}"
    vpc_security_group_ids = [
		"${aws_security_group.SG_EXTERNAL_HAPROXY.id}",
		"${aws_security_group.SG_HAPROXY_EXTERNAL_PEERING.id}",
		"${aws_security_group.SG_AVIATRIX_SSH.id}",
		"${aws_security_group.SG_COMMONWELL_JENKINS_CLIENT_SSH.id}"
	]
	subnet_id = "${var.commonwell-non-prod-private-1b-subnet}"
    instance_type = "t3a.small"
    key_name    = "CommonwellHAProxy"
    associate_public_ip_address = false
	iam_instance_profile = "CmigServiceRole"

	root_block_device {
       volume_size = "10"
	}

	tags {
		Owner = "robhatnagar@deloitte.ca"
		ManagedBy = "CMS-TF"
		SolutionName = "Insurcloud Commonwell"
		Environment = "Dev"
		ApplicationName = "docker, haproxy"
		DataClassification = "NA"
		DLM-Backup = "Daily-1"
		DLM-Retention = "90"
		PatchGroup = "AmazonLinux2"
		VendorManaged = "False"
		ResourceType = "Client"
		ResourceSeverity = "Normal"
		Name = "A-MC-CAC-D-A003-COMMONWELL-PPS-EXTERNAL-HA-PROXY-N2-EC2"
		AccountID = "${data.aws_caller_identity.current.account_id}"
		Role = "Container"
		NexusUse = "True"
		Account = "COMMONWELL-dev"
		Client = "cmig"
		KeyName = "CommonwellHAProxy"
	}

	provisioner "local-exec" {
    	command = "echo ${aws_instance.ICCW_EXTERNAL_HA_PROXY_N2.private_ip} >> dev_hosts.txt"
  	}
}

output "ICCW_INTERNAL_HA_PROXY_N2_PRIVATEIP" {
  value = "${aws_instance.ICCW_INTERNAL_HA_PROXY_N2.private_ip}"
}

resource "aws_instance" "ICCW_INTERNAL_HA_PROXY_N1" {
    ami = "${var.ami_id_amazonlinux2base}"
    vpc_security_group_ids = [
		"${aws_security_group.SG_INTERNAL_HAPROXY.id}",
		"${aws_security_group.SG_HAPROXY_INTERNAL_PEERING.id}",
		"${aws_security_group.SG_AVIATRIX_SSH.id}",
		"${aws_security_group.SG_COMMONWELL_JENKINS_CLIENT_SSH.id}"
	]
	subnet_id = "${var.commonwell-non-prod-private-1a-subnet}"
    instance_type = "t3a.small"
    key_name    = "CommonwellHAProxy"
    associate_public_ip_address = false
	private_ip     = "10.10.70.247"
	iam_instance_profile = "CmigServiceRole"

	root_block_device {
       volume_size = "10"
	}

	tags {
		Owner = "robhatnagar@deloitte.ca"
		ManagedBy = "CMS-TF"
		SolutionName = "Insurcloud Commonwell"
		Environment = "Dev"
		ApplicationName = "docker, haproxy"
		DataClassification = "PII, PHI"
		DLM-Backup = "Daily-1"
		DLM-Retention = "90"
		PatchGroup = "AmazonLinux2"
		VendorManaged = "False"
		ResourceType = "Client"
		ResourceSeverity = "Normal"
		Name = "A-MC-CAC-D-A003-COMMONWELL-PPS-INTERNAL-HA-PROXY-N1-EC2"
		AccountID = "${data.aws_caller_identity.current.account_id}"
		Role = "Container"
		NexusUse = "True"
		Account = "COMMONWELL-dev"
		Client = "cmig"
		KeyName = "CommonwellHAProxy"
	}

	provisioner "local-exec" {
    	command = "echo ${aws_instance.ICCW_INTERNAL_HA_PROXY_N1.private_ip} >> dev_hosts.txt"
  	}
}

output "ICCW_EXTERNAL_HA_PROXY_N1_PRIVATEIP" {
  value = "${aws_instance.ICCW_EXTERNAL_HA_PROXY_N1.private_ip}"
}

resource "aws_instance" "ICCW_INTERNAL_HA_PROXY_N2" {
    ami = "${var.ami_id_amazonlinux2base}"
    vpc_security_group_ids = [
		"${aws_security_group.SG_INTERNAL_HAPROXY.id}",
		"${aws_security_group.SG_HAPROXY_INTERNAL_PEERING.id}",
		"${aws_security_group.SG_AVIATRIX_SSH.id}",
		"${aws_security_group.SG_COMMONWELL_JENKINS_CLIENT_SSH.id}"
	]
	subnet_id = "${var.commonwell-non-prod-private-1b-subnet}"
    instance_type = "t3a.small"
    key_name    = "CommonwellHAProxy"
    associate_public_ip_address = false
	iam_instance_profile = "CmigServiceRole"

	root_block_device {
       volume_size = "10"
	}

	tags {
		Owner = "robhatnagar@deloitte.ca"
		ManagedBy = "CMS-TF"
		SolutionName = "Insurcloud Commonwell"
		Environment = "Dev"
		ApplicationName = "docker, haproxy"
		DataClassification = "NA"
		DLM-Backup = "Daily-1"
		DLM-Retention = "90"
		PatchGroup = "AmazonLinux2"
		VendorManaged = "false"
		ResourceType = "Client"
		ResourceSeverity = "Normal"
		Name = "A-MC-CAC-D-A003-COMMONWELL-PPS-INTERNAL-HA-PROXY-N2-EC2"
		AccountID = "${data.aws_caller_identity.current.account_id}"
		Role = "Container"
		NexusUse = "True"
		Account = "COMMONWELL-dev"
		Client = "cmig"
		KeyName = "CommonwellHAProxy"
	}

	provisioner "local-exec" {
    	command = "echo ${aws_instance.ICCW_INTERNAL_HA_PROXY_N2.private_ip} >> dev_hosts.txt"
  	}
}

output "ICCW_EXTERNAL_HA_PROXY_N2_PRIVATEIP" {
  value = "${aws_instance.ICCW_EXTERNAL_HA_PROXY_N2.private_ip}"
}

#=======================================================================
# Jobscheduler
#=======================================================================
resource "aws_instance" "ICCW_JOB_SCHEDULER" {
    ami = "${var.ami_id_amazonlinux2base}"
	vpc_security_group_ids      = [
		"${aws_security_group.SG_JOB_SCHEDULER.id}",
		"${aws_security_group.SG_AVIATRIX_SSH.id}",
		"${aws_security_group.SG_COMMONWELL_JENKINS_CLIENT_SSH.id}"
	]
	subnet_id = "${var.commonwell-non-prod-private-1a-subnet}"
    instance_type = "t3a.medium"
    key_name    = "CommonwellSAP"
    associate_public_ip_address = false
	iam_instance_profile = "CmigServiceRole"

	root_block_device {
       volume_size = "40"
	}

	tags {
		Owner = "robhatnagar@deloitte.ca"
		ManagedBy = "CMS-TF"
		SolutionName = "Insurcloud Commonwell"
		Environment = "Dev"
		ApplicationName = "Guidewire, docker"
		DataClassification = "PII, PHI"
		DLM-Backup = "Daily-1"
		DLM-Retention = "90"
		PatchGroup = "AmazonLinux2"
		VendorManaged = "False"
		ResourceType = "Client"
		ResourceSeverity = "Normal"
		Name = "A-MC-CAC-D-A003-COMMONWELL-DEV-JOB-SCHEDULER-LNX-EC2"
		AccountID = "${data.aws_caller_identity.current.account_id}"
		Role = "Container"
		NexusUse = "True"
		Account = "COMMONWELL-dev"
		Client = "cmig"
		KeyName = "CommonwellSAP"
	}

	provisioner "local-exec" {
    	command = "echo ${aws_instance.ICCW_JOB_SCHEDULER.private_ip} >> dev_hosts.txt"
  	}

}

output "ICCW_JOB_SCHEDULER_PRIVATEIP" {
  value = "${aws_instance.ICCW_JOB_SCHEDULER.private_ip}"
}

#=======================================================================
# OpenText
#=======================================================================
resource "aws_instance" "ICCW_OT_DATABASE" {
    ami = "${var.ami_id_windows2012_otdb}"
    vpc_security_group_ids      = [
        "${aws_security_group.SG_OT_MSSQL_DB.id}",
        "${aws_security_group.SG_AVIATRIX_RDP.id}"
	]
    subnet_id = "${var.commonwell-non-prod-database-1a-subnet}"
    instance_type = "r5a.xlarge"
    key_name    = "CommonwellDatabase"
    associate_public_ip_address = false
    iam_instance_profile = "CmigServiceRole"
    ebs_optimized = true

    root_block_device {
        volume_size = "170"
        delete_on_termination = false
    }

    tags {
        Owner = "robhatnagar@deloitte.ca"
        ManagedBy = "CMS-TF"
        SolutionName = "Insurcloud Commonwell"
        Environment = "Dev"
        ApplicationName = "MSSQL"
        DataClassification = "PII, PHI"
        DLM-Backup = "Daily-7"
        DLM-Retention = "90"
        PatchGroup = "Windows"
        VendorManaged = "false"
        ResourceType = "Client"
        ResourceSeverity = "Normal"
        Name = "A-MC-CAC-D-A003-COMMONWELL-DEV-OT-DB-WIN-EC2"
        AccountID = "${data.aws_caller_identity.current.account_id}"
        Role = "Container"
        NexusUse = "False"
		Account = "COMMONWELL-dev"
		Client = "cmig"
		KeyName = "CommonwellDatabase"
    }

    provisioner "local-exec" {
        command = "echo ${aws_instance.ICCW_OT_DATABASE.private_ip} >> dev_hosts.txt"
  	}	  
}

output "ICCW_OT_DATABASE_PRIVATEIP" {
    value = "${aws_instance.ICCW_OT_DATABASE.private_ip}"
}

resource "aws_instance" "ICCW_EXSTREAM_COM_SERVER" {
	ami                         = "${var.ami_id_windows2016base}"
	vpc_security_group_ids      = [
		"${aws_security_group.SG_COMMONWELL_EXSTREAM.id}",
		"${aws_security_group.SG_FORTICLIENT_RDP.id}",
		"sg-09069a280a760f119", # CW RDS Access
		"${aws_security_group.SG_AVIATRIX_RDP.id}"
	]
	subnet_id                   = "${var.commonwell-non-prod-private-1a-subnet}"
	instance_type               = "r5a.large"
	key_name                    = "CommonwellExstream"
	associate_public_ip_address = false
	iam_instance_profile        = "CmigServiceRole"

	lifecycle {
		prevent_destroy = true
	}

	root_block_device {
		volume_size = "600"
	}

	tags {
		Owner = "robhatnagar@deloitte.ca"
		ManagedBy = "CMS-TF"
		SolutionName = "Insurcloud Commonwell"
		Environment = "Dev"
		ApplicationName = "EXSTREAM COM"
		DataClassification = "PII, PHI"
		DLM-Backup = "Daily-1"
		DLM-Retention = "90"
		PatchGroup = "Windows"
		VendorManaged = "False"
		ResourceType = "Client"
		ResourceSeverity = "Normal"
		Name = "A-MC-CAC-D-A003-COMMONWELL-DEV-EXSTREAM-COMMUNICATIONS-WIN-EC2"
		AccountID = "${data.aws_caller_identity.current.account_id}"
		Role = "Container"
		NexusUse = "False"
		Account = "COMMONWELL-dev"
		Client = "cmig"
		KeyName = "CommonwellExstream"
	}
}

output "ICCW_EXSTREAM_COM_SERVER_PRIVATEIP" {
  value = "${aws_instance.ICCW_EXSTREAM_COM_SERVER.private_ip}"
}

resource "aws_instance" "ICCW_EXSTREAM_CONTENT_BRAVA_SERVER" {
	ami                         = "${var.ami_id_windows2016base}"
	vpc_security_group_ids      = [
		"${aws_security_group.SG_COMMONWELL_EXSTREAM.id}",
		"${aws_security_group.SG_AVIATRIX_RDP.id}"
	]
	subnet_id                   = "${var.commonwell-non-prod-private-1a-subnet}"
	instance_type               = "r5a.large"
	key_name                    = "CommonwellExstream"
	associate_public_ip_address = false
	iam_instance_profile        = "CmigServiceRole"

	lifecycle {
		prevent_destroy = true
	}

	root_block_device {
		volume_size = "560"
	}

	tags {
		Owner = "robhatnagar@deloitte.ca"
		ManagedBy = "CMS-TF"
		SolutionName = "Insurcloud Commonwell"
		Environment = "Dev"
		ApplicationName = "EXSTREAM BRAVA"
		DataClassification = "PII, PHI"
		DLM-Backup = "Daily-7"
		DLM-Retention = "90"
		PatchGroup = "Windows"
		VendorManaged = "False"
		ResourceType = "Client"
		ResourceSeverity = "Normal"
		Name  = "A-MC-CAC-D-A003-COMMONWELL-DEV-OT-CONTENT-SUITE-BRAVA-WIN-EC2"
		AccountID = "${data.aws_caller_identity.current.account_id}"
		Role = "Container"
		NexusUse = "False"
		Account = "COMMONWELL-dev"
		Client = "cmig"
		KeyName = "CommonwellExstream"
	}
}

output "ICCW_OT_CONTENT_SUITE_BRAVA_SERVER_PRIVATEIP" {
  value = "${aws_instance.ICCW_EXSTREAM_CONTENT_BRAVA_SERVER.private_ip}"
}

resource "aws_instance" "ICCW_EXSTREAM_CONTENT_INDEX_SERVER" {
	ami                         = "${var.ami_id_windows2016base}"
	vpc_security_group_ids      = [
		"${aws_security_group.SG_COMMONWELL_EXSTREAM.id}",
		"${aws_security_group.SG_AVIATRIX_RDP.id}"
	]
	subnet_id                   = "${var.commonwell-non-prod-private-1a-subnet}"
	instance_type               = "t3a.large"
	key_name                    = "CommonwellExstream"
	associate_public_ip_address = false
	iam_instance_profile        = "CmigServiceRole"

	lifecycle {
		prevent_destroy = true
	}

	root_block_device {
		volume_size = "560"
	}

	tags {
		Owner = "robhatnagar@deloitte.ca"
		ManagedBy = "CMS-TF"
		SolutionName = "Insurcloud Commonwell"
		Environment = "Dev"
		ApplicationName = "EXSTREAM INDEX"
		DataClassification = "PII, PHI"
		DLM-Backup = "Daily-7"
		DLM-Retention = "90"
		PatchGroup = "Windows"
		VendorManaged = "False"
		ResourceType = "Client"
		ResourceSeverity = "Normal"
		Name  = "A-MC-CAC-D-A003-COMMONWELL-DEV-OT-CONTENT-SUITE-INDEX-WIN-EC2"
		AccountID = "${data.aws_caller_identity.current.account_id}"
		Role = "Container"
		NexusUse = "False"
		Account = "COMMONWELL-dev"
		Client = "cmig"
		KeyName = "CommonwellExstream"
	}
}

output "ICCW_OT_CONTENT_SUITE_INDEX_SERVER_PRIVATEIP" {
  value = "${aws_instance.ICCW_EXSTREAM_CONTENT_INDEX_SERVER.private_ip}"
}

#=======================================================================
# Hubio
#=======================================================================
resource "aws_instance" "ICCW_HUBIO_SERVER" {
    ami = "${var.ami_id_windows2019cont}"
	vpc_security_group_ids      = [
		"${aws_security_group.SG_HUBIO.id}",
		"${aws_security_group.SG_COMMONWELL_JOBSCHEDULER_SSH.id}",
		"${aws_security_group.SG_AVIATRIX_RDP.id}"
	]
	subnet_id = "${var.commonwell-non-prod-private-1a-subnet}"
    instance_type = "t3a.xlarge"
    key_name    = "CommonwellApplications"
    associate_public_ip_address = false
	iam_instance_profile = "CmigServiceRole"

	lifecycle {
    	prevent_destroy = true
  	}

	root_block_device {
       	volume_size = "500"
	}

	tags {
		Owner = "robhatnagar@deloitte.ca"
		ManagedBy = "CMS-TF"
		SolutionName = "Insurcloud Commonwell"
		Environment = "Dev"
		ApplicationName = "Hubio"
		DataClassification = "PII, PHI"
		DLM-Backup = "Daily-7"
		DLM-Retention = "90"
		PatchGroup = "Windows"
		VendorManaged = "False"
		ResourceType = "Client"
		ResourceSeverity = "Normal"
		Name = "A-MC-CAC-D-A003-COMMONWELL-PPS-HUBIO-SAP-WIN-EC2"
		AccountID = "${data.aws_caller_identity.current.account_id}"
		Role = "Container"
		NexusUse = "False"
		Account = "COMMONWELL-dev"
		Client = "cmig"
		KeyName = "CommonwellApplications"
	}

	provisioner "local-exec" {
    	command = "echo ${aws_instance.ICCW_HUBIO_SERVER.private_ip} >> dev_hosts.txt"
  	}
}

output "ICCW_HUBIO_SERVER_PRIVATEIP" {
  value = "${aws_instance.ICCW_HUBIO_SERVER.private_ip}"
}

#######################DEV/QA#######################

resource "aws_instance" "ICCW_EXTERNAL_HA_PROXY_DEV01" {
    ami = "${var.ami_id_amazonlinux2base}"
    vpc_security_group_ids = [
		"${aws_security_group.SG_EXTERNAL_HAPROXY_DEV.id}",
		"${aws_security_group.SG_AVIATRIX_SSH.id}",
		"${aws_security_group.SG_COMMONWELL_JENKINS_CLIENT_SSH.id}"
	]
	subnet_id = "${var.commonwell-non-prod-private-1a-subnet}"
    instance_type = "t3a.small"
    key_name    = "CommonwellHAProxy"
    associate_public_ip_address = false
	iam_instance_profile = "CmigServiceRole"

	root_block_device {
       volume_size = "10"
	}

	tags {
		Owner = "robhatnagar@deloitte.ca"
		ManagedBy = "CMS-TF"
		SolutionName = "Insurcloud Commonwell"
		Environment = "Dev"
		ApplicationName = "docker, microservices"
		DataClassification = "PII, PHI"
		DLM-Backup = "Daily-1"
		DLM-Retention = "90"
		PatchGroup = "AmazonLinux2"
		VendorManaged = "False"
		ResourceType = "Client"
		ResourceSeverity = "Normal"
		Name = "A-MC-CAC-D-A003-COMMONWELL-DEV01-EXTERNAL-HA-PROXY-EC2"
		AccountID = "${data.aws_caller_identity.current.account_id}"
		Role = "Container"
		NexusUse = "True"
		Account = "COMMONWELL-dev"
		Client = "cmig"
		KeyName = "CommonwellHAProxy"
	}
}

output "ICCW_INTERNAL_HA_PROXY_DEV01_PRIVATEIP" {
  value = "${aws_instance.ICCW_INTERNAL_HA_PROXY_DEV01.private_ip}"
}

resource "aws_instance" "ICCW_INTERNAL_HA_PROXY_DEV01" {
    ami = "${var.ami_id_amazonlinux2base}"
    vpc_security_group_ids = [
		"${aws_security_group.SG_INTERNAL_HAPROXY_DEV.id}",
		"${aws_security_group.SG_AVIATRIX_SSH.id}",
		"${aws_security_group.SG_COMMONWELL_JENKINS_CLIENT_SSH.id}"
	]
	subnet_id = "${var.commonwell-non-prod-private-1a-subnet}"
    instance_type = "t3a.small"
    key_name    = "CommonwellHAProxy"
    associate_public_ip_address = false
	iam_instance_profile = "CmigServiceRole"

	root_block_device {
       volume_size = "10"
	   delete_on_termination = "false"
	}

	tags {
		Owner = "robhatnagar@deloitte.ca"
		ManagedBy = "CMS-TF"
		SolutionName = "Insurcloud Commonwell"
		Environment = "Dev"
		ApplicationName = "docker, haproxy"
		DataClassification = "PII, PHI"
		DLM-Backup = "Daily-1"
		DLM-Retention = "90"
		PatchGroup = "AmazonLinux2"
		VendorManaged = "False"
		ResourceType = "Client"
		ResourceSeverity = "Normal"
		Name = "A-MC-CAC-D-A003-COMMONWELL-DEV01-INTERNAL-HA-PROXY-EC2"
		AccountID = "${data.aws_caller_identity.current.account_id}"
		Role = "Container"
		NexusUse = "True"
		Account = "COMMONWELL-dev"
		Client = "cmig"
		KeyName = "CommonwellHAProxy"
	}

}

output "ICCW_EXTERNAL_HA_PROXY_DEV01_PRIVATEIP" {
  value = "${aws_instance.ICCW_EXTERNAL_HA_PROXY_DEV01.private_ip}"
}

resource "aws_instance" "ICCW_GUIDEWIRE_DEV01" {
    ami = "${var.ami_id_amazonlinux2base}"
    vpc_security_group_ids = [
		"${aws_security_group.COMMONWELL-GW-DEV.id}",
		"${aws_security_group.SG_MSSQL_DB.id}",
		"${aws_security_group.SG_JENKINS_GUIDEWIRE_HEALTH_CHECK.id}",
		"${aws_security_group.SG_AVIATRIX_SSH.id}",
		"${aws_security_group.SG_COMMONWELL_JENKINS_CLIENT_SSH.id}"
	]
	subnet_id = "${var.commonwell-non-prod-guidewire-containers-1a-subnet}"
    instance_type = "r5a.xlarge"
    key_name    = "CommonwellGuidewire"
    associate_public_ip_address = false
	iam_instance_profile = "CmigServiceRole"

	root_block_device {
       volume_size = "100"
	   delete_on_termination = "false"
	}

	tags {
		Owner = "robhatnagar@deloitte.ca"
		ManagedBy = "CMS-TF"
		SolutionName = "Insurcloud Commonwell"
		Environment = "Dev"
		ApplicationName = "Guidewire, docker"
		DataClassification = "PII, PHI"
		DLM-Backup = "Daily-1"
		DLM-Retention = "90"
		PatchGroup = "AmazonLinux2"
		VendorManaged = "False"
		ResourceType = "Client"
		ResourceSeverity = "Normal"
		Name = "A-MC-CAC-D-A003-COMMONWELL-DEV01-GW-LNX-EC2"
		AccountID = "${data.aws_caller_identity.current.account_id}"
		Role = "Container"
		NexusUse = "False"
		Account = "COMMONWELL-dev"
		Client = "cmig"
		KeyName = "CommonwellGuidewire"
	}	

}

output "ICCW_GUIDEWIRE_DEV01_PRIVATEIP" {
  value = "${aws_instance.ICCW_GUIDEWIRE_DEV01.private_ip}"
}

resource "aws_instance" "ICCW_MICROSERVICES_DEV01" {
    ami = "${var.ami_id_amazonlinux2base}"
    vpc_security_group_ids = [
		"${aws_security_group.SG_COMMONWELL_MICROSERVICES_DEV.id}",
		"${aws_security_group.SG_MSSQL_DB.id}",
		"${aws_security_group.SG_COMMONWELL_PROMETHEUS_GRAFANA.id}",
		"${aws_security_group.SG_AVIATRIX_SSH.id}",
		"${aws_security_group.SG_COMMONWELL_JENKINS_CLIENT_SSH.id}"
	]
	subnet_id = "${var.commonwell-non-prod-spring-microservices-1a-subnet}"
    instance_type = "r5a.2xlarge"
    key_name    = "CommonwellMicroservices"
    associate_public_ip_address = false
	iam_instance_profile = "CmigServiceRole"

	root_block_device {
       volume_size = "150"
	}

	tags {
		Owner = "robhatnagar@deloitte.ca"
		ManagedBy = "CMS-TF"
		SolutionName = "Insurcloud Commonwell"
		Environment = "Dev"
		ApplicationName = "docker, microservices"
		DataClassification = "PII, PHI"
		DLM-Backup = "Daily-1"
		DLM-Retention = "90"
		PatchGroup = "AmazonLinux2-Microsvc4"
		VendorManaged = "False"
		ResourceType = "Client"
		ResourceSeverity = "Normal"
		Name = "A-MC-CAC-D-A003-COMMONWELL-DEV01-MICROSERVICES-LNX-EC2"
		AccountID = "${data.aws_caller_identity.current.account_id}"
		Role = "Container"
		NexusUse = "True"
		Account = "COMMONWELL-dev"
		Client = "cmig"
		KeyName = "CommonwellMicroservices"
	}
}

output "ICCW_MICROSERVICES_DEV01_PRIVATEIP" {
  value = "${aws_instance.ICCW_MICROSERVICES_DEV01.private_ip}"
}
