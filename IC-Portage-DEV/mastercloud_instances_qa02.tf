
######### GUIDEWIRE ##############
resource "aws_instance" "ICPORTAGE_QA02_GUIDEWIRE_SUITE" {
  ami = "${var.ami_id_amazonlinuxbase20200806}"

  vpc_security_group_ids = [
    "${aws_security_group.SG_GUIDEWIRE_DEV.id}",
    "${aws_security_group.SG_JENKINS_GUIDEWIRE_HEALTH_CHECK.id}",
    "${aws_security_group.SG_AVIATRIX_SSH.id}",
    "${aws_security_group.SG_JENKINS_CLIENT_SSH.id}",
    "${aws_security_group.SG_MSSQL_DB.id}",
  ]

  subnet_id                   = "${var.portage-dev-private-1a-subnet}"
  instance_type               = "r5a.xlarge"
  key_name                    = "PortageGuidewireDev"
  associate_public_ip_address = false
  iam_instance_profile        = "CMS-SSMInstanceRole"
  ebs_optimized               = "true"

  root_block_device {
    volume_size = "35"
  }

  tags {
    Owner              = "robhatnagar@deloitte.ca"
    ManagedBy          = "CMS-TF"
    SolutionName       = "Insurcloud Portage"
    Environment        = "Dev"
    ApplicationName    = "guidewire, mssql, docker"
    DataClassification = "PII, PHI"
    DLM-Backup         = "Yes"
    DLM-Retention      = "90"
    PatchGroup         = "AmazonLinux2"
    VendorManaged      = "False"
    ResourceType       = "Client"
    ResourceSeverity   = "Normal"
    Name               = "A-MC-CAC-D-A005-PORTAGE-QA02-GW-SUITE-LNX-EC2"
    AccountID          = "${data.aws_caller_identity.current.account_id}"
    Role               = "Container"
    CLIENT             = "Portage"
  }

  provisioner "local-exec" {
    command = "echo ${aws_instance.ICPORTAGE_QA02_GUIDEWIRE_SUITE.private_ip} >> dev_hosts.txt"
  }
}

output "ICPORTAGE_QA02_GUIDEWIRE_SUITE_PRIVATEIP" {
  value = "${aws_instance.ICPORTAGE_QA02_GUIDEWIRE_SUITE.private_ip}"
}

######### MICROSERVICES ###########
resource "aws_instance" "ICPORTAGE_QA02_MICROSERVICES" {
  ami = "${var.ami_id_amazonlinuxbase20200806}"

  vpc_security_group_ids = [
    "${aws_security_group.SG_MICROSERVICES_DEV.id}",
    "${aws_security_group.SG_MONITORING.id}",
    "${aws_security_group.SG_AVIATRIX_SSH.id}",
    "${aws_security_group.SG_JENKINS_CLIENT_SSH.id}",
    "${aws_security_group.SG_MYSQL_DB.id}",
  ]

  subnet_id                   = "${var.portage-dev-private-1a-subnet}"
  instance_type               = "r5a.xlarge"
  key_name                    = "PortageMicroservicesDev"
  associate_public_ip_address = false
  iam_instance_profile        = "CMS-SSMInstanceRole"
  ebs_optimized               = "true"

  root_block_device {
    volume_size = "50"
  }

  tags {
    Owner              = "robhatnagar@deloitte.ca"
    ManagedBy          = "CMS-TF"
    SolutionName       = "Insurcloud Portage"
    Environment        = "Dev"
    ApplicationName    = "docker, microservices, mysql"
    DataClassification = "PII, PHI"
    DLM-Backup         = "Yes"
    DLM-Retention      = "90"
    PatchGroup         = "AmazonLinux2"
    VendorManaged      = "False"
    ResourceType       = "Client"
    ResourceSeverity   = "Normal"
    Name               = "A-MC-CAC-D-A005-PORTAGE-QA02-MICROSERVICES-LNX-EC2"
    AccountID          = "${data.aws_caller_identity.current.account_id}"
    Role               = "Container"
    CLIENT             = "Portage"
  }

  provisioner "local-exec" {
    command = "echo ${aws_instance.ICPORTAGE_QA02_MICROSERVICES.private_ip} >> dev_hosts.txt"
  }
}

output "ICPORTAGE_QA02_MICROSERVICES_PRIVATEIP" {
  value = "${aws_instance.ICPORTAGE_QA02_MICROSERVICES.private_ip}"
}

######### MSSQL ##############
data "template_file" "PORTAGE_DOMAIN_JOIN_9" {
  template = "${file("IC-Portage-DEV/windowsdata.tpl")}"

  vars {
    computer_name  = "QA02DHIC-DB"
    region         = "${var.region}"
    domain         = "${var.ad_domain}"
    dns1           = "${var.ad_dns1}"
    dns2           = "${var.ad_dns2}"
    dns3           = "${var.ad_dns3}"
    ouselection    = "${var.ou_name}"
    pwordparameter = "${var.pwordparameter}"
    unameparameter = "${var.unameparameter}"
  }
}

resource "aws_instance" "ICPORTAGE_QA02_DHIC_MSSQL_DATABASE" {
  ami = "${var.ami_id_windows_mssql_base}"

  vpc_security_group_ids = [
    "${aws_security_group.SG_MSSQL_DB.id}",
    "${aws_security_group.SG_PORTAGE_MSSQL_DB.id}",
    "${aws_security_group.SG_AVIATRIX_RDP.id}",
  ]

  subnet_id                   = "${var.portage-dev-database-1a-subnet}"
  instance_type               = "r5a.large"
  key_name                    = "PortageDatabaseDev"
  associate_public_ip_address = false
  iam_instance_profile        = "CMS-SSMInstanceRole"
  ebs_optimized               = "true"

  lifecycle {
    prevent_destroy = false
  }

  root_block_device {
    volume_size = "256"
  }

  user_data = "${data.template_file.PORTAGE_DOMAIN_JOIN_9.rendered}"

  tags {
    Owner              = "robhatnagar@deloitte.ca"
    ManagedBy          = "CMS-TF"
    SolutionName       = "Insurcloud Portage"
    Environment        = "Dev"
    ApplicationName    = "database, mssql, sap, ssis"
    DataClassification = "PII, PHI"
    DLM-Backup         = "Yes"
    DLM-Retention      = "90"
    PatchGroup         = "Windows"
    VendorManaged      = "False"
    OSName             = "Windows"
    ResourceType       = "Client"
    ResourceSeverity   = "Normal"
    Name               = "A-MC-CAC-D-A005-PORTAGE-QA02-DHIC-DATABASE-MSSQL-WIN-EC2"
    AccountID          = "${data.aws_caller_identity.current.account_id}"
    Role               = "Container"
    CLIENT             = "Portage"
  }

  provisioner "local-exec" {
    command = "echo ${aws_instance.ICPORTAGE_QA02_DHIC_MSSQL_DATABASE.private_ip} >> dev_hosts.txt"
  }
}

output "ICPORTAGE_QA02_DHIC_MSSQL_DATABASE_PRIVATEIP" {
  value = "${aws_instance.ICPORTAGE_QA02_DHIC_MSSQL_DATABASE.private_ip}"
}

######### WINDOWS ##############
data "template_file" "PORTAGE_DOMAIN_JOIN_10" {
  template = "${file("IC-Portage-DEV/windowsdata.tpl")}"

  vars {
    computer_name  = "QA02DHIC"
    region         = "${var.region}"
    domain         = "${var.ad_domain}"
    dns1           = "${var.ad_dns1}"
    dns2           = "${var.ad_dns2}"
    dns3           = "${var.ad_dns3}"
    ouselection    = "${var.ou_name}"
    pwordparameter = "${var.pwordparameter}"
    unameparameter = "${var.unameparameter}"
  }
}

resource "aws_instance" "ICPORTAGE_QA02_DHIC_SERVER" {
  ami = "${var.ami_id_windows_base}"

  vpc_security_group_ids = [
    "${aws_security_group.SG_DHIC.id}",
    "${aws_security_group.SG_AVIATRIX_RDP.id}",
    "${aws_security_group.SG_WORKSPACES.id}"
  ]

  subnet_id                   = "${var.portage-dev-private-1a-subnet}"
  instance_type               = "t3a.xlarge"
  key_name                    = "PortageDataDev"
  associate_public_ip_address = false
  iam_instance_profile        = "CMS-SSMInstanceRole"
  ebs_optimized               = "true"
  lifecycle {
    prevent_destroy = false
  }

  root_block_device {
    volume_size = "100"
  }

  user_data = "${data.template_file.PORTAGE_DOMAIN_JOIN_10.rendered}"

  tags {
    Owner              = "robhatnagar@deloitte.ca"
    ManagedBy          = "CMS-TF"
    SolutionName       = "Insurcloud Portage"
    Environment        = "Dev"
    ApplicationName    = "sap"
    DataClassification = "PII, PHI"
    DLM-Backup         = "Yes"
    DLM-Retention      = "90"
    PatchGroup         = "Windows"
    VendorManaged      = "False"
    OSName             = "Windows"
    ResourceType       = "Client"
    ResourceSeverity   = "Normal"
    Name               = "A-MC-CAC-D-A005-PORTAGE-QA02-DHIC-SERVER-WIN-EC2"
    AccountID          = "${data.aws_caller_identity.current.account_id}"
    Role               = "Container"
    CLIENT             = "Portage"
  }
  provisioner "local-exec" {
    command = "echo ${aws_instance.ICPORTAGE_QA02_DHIC_SERVER.private_ip} >> dev_hosts.txt"
  }
}

output "ICPORTAGE_QA02_DHIC_SERVER_PRIVATEIP" {
  value = "${aws_instance.ICPORTAGE_QA02_DHIC_SERVER.private_ip}"
}
