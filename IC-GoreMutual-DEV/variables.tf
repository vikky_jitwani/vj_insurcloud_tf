variable "client_name" {
  description = "AWS client name for resources (Ex: MASTERCLOUD)"
  default = "GORE"
}

variable "environment_name" {
  description = "AWS environment name for resources (Ex: NON-PROD)"
  default = ""
}

variable "environment_type" {
  description = "AWS environment type for resources (Ex: P=PROD/HUB, D=NON-PROD)"
  default = ""
}

variable "client_number" {
  description = "AWS client number for tagging (Ex: A001)"
  default = "A005"
}

variable "region" {
  description = "AWS region for hosting our your network"
  default = "ca-central-1"
}

variable "volume_key" {
  description = "KMS volume key hub account"
  default = ""
}

variable "ami_id_amazonlinux201709base" {
  description = "Amazon Linux Encypted Root Volume AMI"
  default = ""
}

variable "ami_id_redhatlinux75" {
  description = "Redhat Linux Encypted Root Volume AMI"
  default = ""
}

variable "ami_id_amazonlinux201803base" {
  description = "Amazon Linux 2018 03 Encypted Root Volume AMI"
  default = ""
}

variable "ami_id_amazonlinux2base" {
  description = "Amazon Linux 2 Base Encypted Root Volume AMI"
  default = ""
}

variable "ami_id_windows2008base" {
  description = "Windows 2008 Base Encypted Root Volume AMI"
  default = ""
}

variable "ami_id_amazonlinux201712base" {
  description = "Amazon Linux 2017 12 Base Encypted Root Volume AMI"
  default = ""
}

variable "ami_id_windows2012base" {
  description = "Windows 2012 Base Encypted Root Volume AMI with SQL 2014 Express"
  default = ""
}


variable "ami_id_windows2012R2base" {
  description = "Windows 2012 R2 Base Encypted Root Volume AMI"
  default = ""
}


variable "ami_id_windows2012SQLEnterprisebase" {
  description = "Windows 2012 RTM Base Encypted Root Volume AMI with SQL 2014 Enterprise"
  default = ""
}

variable "ami_id_windows2012R2SQLExpressbase" {
  description = "Windows 2012 R2 RTM Base Encypted Root Volume AMI with SQL 2014 Express"
  default = ""
}

variable "ami_id_windows2016base" {
  description = "Windows 2016 Base Encypted Root Volume AMI"
  default = ""
}

variable "ami_id_windows2019base" {
  description = "Windows 2019 Base Encypted Root Volume AMI"
  default = ""
}

variable "ami_id_windows2019container" {
  description = "Windows 2019 Base with containers Encypted Root Volume AMI"
  default = ""
}

variable "ami_id_ubuntu1604baseimage" {
  description = "Ubuntu 16 04 Base Encypted Root Volume AMI"
  default = ""
}

variable "ami_id_ubuntu1804baseimage" {
  description = "Ubuntu 18 04 Base Encypted Root Volume AMI"
  default = ""
}

variable "ami_id_windows_dev_infocenter_datahub_server" {
  description = "InfoCenter & Datahub Base Image for Dev. Based on ami_id_windows2016base"
  default = ""
}

variable "ami_id_" {
  description = "Ubuntu 18 04 Base Encypted Root Volume AMI"
  default = ""
}

variable "vpc_id" {
  description = "VPC id for DEV account"
  default = "vpc-08958975360ac71be"
}

variable "vpc_cidr" {
  description = "VPC cidr dev account"
  default = "192.168.8.0/22"
}

variable "gore-dev-database-1a-subnet" {
  description = "Database subnet one"
  default = ""
}

variable "gore-dev-database-1b-subnet" {
  description = "Database subnet two"
  default = ""
}

variable "gore-dev-private-1a-subnet" {
  description = "Private Subnet One"
  default = ""
}

variable "gore-dev-private-1b-subnet" {
  description = "Private Subnet Two"
  default = ""
}

variable "default_cidr" {
  description = "default cidr used"
  default = ["10.0.0.0/8"]
}

variable "gore_cidr" {
    default = "10.10.10.0/23"
}

# variable "peel_watchguard_cidr" {
#     default = "192.168.113.0/24"
# }

# variable "peel_dmz_cidr" {
#     default = "192.168.100.0/24"
# }

# variable "peel_wireless_cidr" {
#     default = "192.168.101.0/24"
# }

variable "mssql-rds-password" {
    default = "2020SeaLion"
}
variable "mysql-rds-password" {
    default = "2020SeaLion"
}

variable "postgres-rds-password" {
    default = "2020SeaLion"
}

variable "zone_id" {
  description = "default zone id"
  default = ""
}
variable "gore_dev_cidr" {
 description = "default cidr used"
 type = "list"
 default = ["192.168.28.0/22"]
}
 
variable "gore_prod_cidr" {
 description = "default cidr used"
 type = "list"
 default = ["192.168.24.0/22"]
}
 
variable "gore_hub_cidr" {
 description = "default cidr used"
 type = "list"
 default = ["192.168.20.0/22"]
}
variable "gore_hub_private_cidr" {
  description = "default cidr used"
  type = "list"
  default = ["192.168.20.0/24", "192.168.21.0/24"]
}
variable "gore_aviatrix_vpn" {
    default = "192.168.22.19/32"
}

variable "ad_dns1" {
    default = "192.168.20.32"
}
variable "ad_dns2" {
    default = "192.168.21.188"
}
variable "ad_dns3" {
    default = "192.168.20.2"
}
variable "ou_name" {
    default = "dev"
}
variable "pwordparameter" {
    default = "domain_password"
}
variable "unameparameter" {
    default = "domain_username"
}
variable "ad_domain" {
    default = "internal.gore.insurcloud.ca"
}
data "aws_caller_identity" "current" {}

variable "gore-dev-account" {
  default = "780193124257"
}

variable "gore-prod-account" {
  default = "948423377884"
}

variable "gore-hub-account" {
  default = "963473599983"
}

