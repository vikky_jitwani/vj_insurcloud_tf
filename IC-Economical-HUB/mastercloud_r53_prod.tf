# #=======================================================================
# # PROD01
# #=======================================================================

######### GUIDEWIRE ##############
resource "aws_route53_record" "ICECONOMICAL_PROD01_GUIDEWIRE01_SUITE_RECORD" {
  zone_id = "${var.zone_id}"
  name    = "gw1.economical.insurcloud.ca"
  ttl = "300"
  type    = "A"
  records = ["${data.terraform_remote_state.prod_remote_state.ICECONOMICAL_PROD01_GUIDEWIRE01_SUITE_PRIVATEIP}"]
}

resource "aws_route53_record" "ICECONOMICAL_PROD01_GUIDEWIRE02_SUITE_RECORD" {
  zone_id = "${var.zone_id}"
  name    = "gw2.economical.insurcloud.ca"
  ttl = "300"
  type    = "A"
  records = ["${data.terraform_remote_state.prod_remote_state.ICECONOMICAL_PROD01_GUIDEWIRE02_SUITE_PRIVATEIP}"]
}

resource "aws_route53_record" "ICECONOMICAL_PROD01_GUIDEWIRE03_SUITE_RECORD" {
  zone_id = "${var.zone_id}"
  name    = "gw3.economical.insurcloud.ca"
  ttl = "300"
  type    = "A"
  records = ["${data.terraform_remote_state.prod_remote_state.ICECONOMICAL_PROD01_GUIDEWIRE03_SUITE_PRIVATEIP}"]
}

resource "aws_route53_record" "ICECONOMICAL_PROD01_GUIDEWIRE04_SUITE_RECORD" {
  zone_id = "${var.zone_id}"
  name    = "gw4.economical.insurcloud.ca"
  ttl = "300"
  type    = "A"
  records = ["${data.terraform_remote_state.prod_remote_state.ICECONOMICAL_PROD01_GUIDEWIRE04_SUITE_PRIVATEIP}"]
}

resource "aws_route53_record" "ICECONOMICAL_PROD01_GUIDEWIRE_DB01_RECORD" {
  zone_id = "${var.zone_id}"
  name    = "gwdb01.economical.insurcloud.ca"
  ttl     = "300"
  type    = "A"
  records = ["${data.terraform_remote_state.prod_remote_state.ICECONOMICAL_PROD01_GUIDEWIRE_DATABASE01_PRIVATEIP}"]
}

resource "aws_route53_record" "ICECONOMICAL_PROD01_GUIDEWIRE_DB02_RECORD" {
  zone_id = "${var.zone_id}"
  name    = "gwdb02.economical.insurcloud.ca"
  ttl     = "300"
  type    = "A"
  records = ["${data.terraform_remote_state.prod_remote_state.ICECONOMICAL_PROD01_GUIDEWIRE_DATABASE02_PRIVATEIP}"]
}

resource "aws_route53_record" "ICECONOMICAL_PROD01_INTERNAL_NLB_GUIDEWIRE_RECORD_1" {
  zone_id = "${var.zone_id}"
  name    = "gw.economical.insurcloud.ca"
  type    = "A"
  alias {
    name                   = "${data.terraform_remote_state.prod_remote_state.ICECONOMICAL_PROD01_INTERNAL_NLB_DNS_NAME}"
    zone_id                = "${data.terraform_remote_state.prod_remote_state.ICECONOMICAL_PROD01_INTERNAL_NLB_ZONE_ID}"
    evaluate_target_health = false
  }
}

######### MICROSERVICES ###########
resource "aws_route53_record" "ICECONOMICAL_PROD01_MICROSERVICES_MANAGER01_RECORD" {
  zone_id = "${var.zone_id}"
  name    = "msman1.economical.insurcloud.ca"
  ttl     = "300"
  type    = "A"
  records = ["${data.terraform_remote_state.prod_remote_state.ICECONOMICAL_PROD01_MICROSERVICES_MANAGER01_PRIVATEIP}"]
}

resource "aws_route53_record" "ICECONOMICAL_PROD01_MICROSERVICES_MANAGER02_RECORD" {
  zone_id = "${var.zone_id}"
  name    = "msman2.economical.insurcloud.ca"
  ttl     = "300"
  type    = "A"
  records = ["${data.terraform_remote_state.prod_remote_state.ICECONOMICAL_PROD01_MICROSERVICES_MANAGER02_PRIVATEIP}"]
}

resource "aws_route53_record" "ICECONOMICAL_PROD01_MICROSERVICES_MANAGER03_RECORD" {
  zone_id = "${var.zone_id}"
  name    = "msman3.economical.insurcloud.ca"
  ttl     = "300"
  type    = "A"
  records = ["${data.terraform_remote_state.prod_remote_state.ICECONOMICAL_PROD01_MICROSERVICES_MANAGER03_PRIVATEIP}"]
}

resource "aws_route53_record" "ICECONOMICAL_PROD01_MICROSERVICES_APPLICATION01_RECORD" {
  zone_id = "${var.zone_id}"
  name    = "ms1.economical.insurcloud.ca"
  ttl     = "300"
  type    = "A"
  records = ["${data.terraform_remote_state.prod_remote_state.ICECONOMICAL_PROD01_MICROSERVICES_APPLICATION01_PRIVATEIP}"]
}

resource "aws_route53_record" "ICECONOMICAL_PROD01_MICROSERVICES_APPLICATION02_RECORD" {
  zone_id = "${var.zone_id}"
  name    = "ms2.economical.insurcloud.ca"
  ttl     = "300"
  type    = "A"
  records = ["${data.terraform_remote_state.prod_remote_state.ICECONOMICAL_PROD01_MICROSERVICES_APPLICATION02_PRIVATEIP}"]
}

resource "aws_route53_record" "ICECONOMICAL_PROD01_INTERNAL_NLB_MICROSERVICES_RECORD_1" {
  zone_id = "${var.zone_id}"
  name    = "ms.economical.insurcloud.ca"
  type    = "A"
  alias {
    name                   = "${data.terraform_remote_state.prod_remote_state.ICECONOMICAL_PROD01_INTERNAL_NLB_DNS_NAME}"
    zone_id                = "${data.terraform_remote_state.prod_remote_state.ICECONOMICAL_PROD01_INTERNAL_NLB_ZONE_ID}"
    evaluate_target_health = false
  }
}

######### RPA ##############
resource "aws_route53_record" "ICECONOMICAL_PROD01_RPA_WS_RECORD" {
  zone_id = "${var.zone_id}"
  name    = "rpa.economical.insurcloud.ca"
  type    = "A"
  alias {
    name                   = "${data.terraform_remote_state.prod_remote_state.ICECONOMICAL_PROD01_RPA_NLB_DNS_NAME}"
    zone_id                = "${data.terraform_remote_state.prod_remote_state.ICECONOMICAL_PROD01_RPA_NLB_ZONE_ID}"
    evaluate_target_health = false
  }
}

resource "aws_route53_record" "ICECONOMICAL_PROD01_RPA_APP01_RECORD" {
  zone_id = "${var.zone_id}"
  name    = "rpa-app01.economical.insurcloud.ca"
  ttl     = "300"
  type    = "A"
  records = ["${data.terraform_remote_state.prod_remote_state.ICECONOMICAL_PROD01_RPA_APP01_PRIVATEIP}"]
}

resource "aws_route53_record" "ICECONOMICAL_PROD01_RPA_APP02_RECORD" {
  zone_id = "${var.zone_id}"
  name    = "rpa-app02.economical.insurcloud.ca"
  ttl     = "300"
  type    = "A"
  records = ["${data.terraform_remote_state.prod_remote_state.ICECONOMICAL_PROD01_RPA_APP02_PRIVATEIP}"]
}

resource "aws_route53_record" "ICECONOMICAL_PROD01_RPA_DB_RECORD" {
  zone_id = "${var.zone_id}"
  name    = "rpa-db.economical.insurcloud.ca"
  ttl     = "300"
  type    = "CNAME"
  records = ["${data.terraform_remote_state.prod_remote_state.ICECONOMICAL_PROD01_RPA_DB_PRIVATE_DNS}"]
}

resource "aws_route53_record" "ICECONOMICAL_PROD01_RPA_INT01_RECORD" {
  zone_id = "${var.zone_id}"
  name    = "rpa-int01.economical.insurcloud.ca"
  ttl     = "300"
  type    = "A"
  records = ["${data.terraform_remote_state.prod_remote_state.ICECONOMICAL_PROD01_RPA_INT01_PRIVATEIP}"]
}

resource "aws_route53_record" "ICECONOMICAL_PROD01_RPA_CLIENT01_RECORD" {
  zone_id = "${var.zone_id}"
  name    = "rpa-client01.economical.insurcloud.ca"
  ttl     = "300"
  type    = "A"
  records = ["${data.terraform_remote_state.prod_remote_state.ICECONOMICAL_PROD01_RPA_CLIENT01_PRIVATEIP}"]
}

resource "aws_route53_record" "ICECONOMICAL_PROD01_RPA_CLIENT02_RECORD" {
  zone_id = "${var.zone_id}"
  name    = "rpa-client02.economical.insurcloud.ca"
  ttl     = "300"
  type    = "A"
  records = ["${data.terraform_remote_state.prod_remote_state.ICECONOMICAL_PROD01_RPA_CLIENT02_PRIVATEIP}"]
}

resource "aws_route53_record" "ICECONOMICAL_PROD01_RPA_CLIENT03_RECORD" {
  zone_id = "${var.zone_id}"
  name    = "rpa-client03.economical.insurcloud.ca"
  ttl     = "300"
  type    = "A"
  records = ["${data.terraform_remote_state.prod_remote_state.ICECONOMICAL_PROD01_RPA_CLIENT03_PRIVATEIP}"]
}

resource "aws_route53_record" "ICECONOMICAL_PROD01_RPA_CLIENT04_RECORD" {
  zone_id = "${var.zone_id}"
  name    = "rpa-client04.economical.insurcloud.ca"
  ttl     = "300"
  type    = "A"
  records = ["${data.terraform_remote_state.prod_remote_state.ICECONOMICAL_PROD01_RPA_CLIENT04_PRIVATEIP}"]
}

resource "aws_route53_record" "ICECONOMICAL_PROD01_RPA_CLIENT05_RECORD" {
  zone_id = "${var.zone_id}"
  name    = "rpa-client05.economical.insurcloud.ca"
  ttl     = "300"
  type    = "A"
  records = ["${data.terraform_remote_state.prod_remote_state.ICECONOMICAL_PROD01_RPA_CLIENT05_PRIVATEIP}"]
}

resource "aws_route53_record" "ICECONOMICAL_PROD01_RPA_CLIENT06_RECORD" {
  zone_id = "${var.zone_id}"
  name    = "rpa-client06.economical.insurcloud.ca"
  ttl     = "300"
  type    = "A"
  records = ["${data.terraform_remote_state.prod_remote_state.ICECONOMICAL_PROD01_RPA_CLIENT06_PRIVATEIP}"]
}

resource "aws_route53_record" "ICECONOMICAL_PROD01_RPA_CLIENT07_RECORD" {
  zone_id = "${var.zone_id}"
  name    = "rpa-client07.economical.insurcloud.ca"
  ttl     = "300"
  type    = "A"
  records = ["${data.terraform_remote_state.prod_remote_state.ICECONOMICAL_PROD01_RPA_CLIENT07_PRIVATEIP}"]
}

resource "aws_route53_record" "ICECONOMICAL_PROD01_RPA_CLIENT08_RECORD" {
  zone_id = "${var.zone_id}"
  name    = "rpa-client08.economical.insurcloud.ca"
  ttl     = "300"
  type    = "A"
  records = ["${data.terraform_remote_state.prod_remote_state.ICECONOMICAL_PROD01_RPA_CLIENT08_PRIVATEIP}"]
}

resource "aws_route53_record" "ICECONOMICAL_PROD01_RPA_CLIENT09_RECORD" {
  zone_id = "${var.zone_id}"
  name    = "rpa-client09.economical.insurcloud.ca"
  ttl     = "300"
  type    = "A"
  records = ["${data.terraform_remote_state.prod_remote_state.ICECONOMICAL_PROD01_RPA_CLIENT09_PRIVATEIP}"]
}

resource "aws_route53_record" "ICECONOMICAL_PROD01_RPA_CLIENT10_RECORD" {
  zone_id = "${var.zone_id}"
  name    = "rpa-client10.economical.insurcloud.ca"
  ttl     = "300"
  type    = "A"
  records = ["${data.terraform_remote_state.prod_remote_state.ICECONOMICAL_PROD01_RPA_CLIENT10_PRIVATEIP}"]
}

resource "aws_route53_record" "ICECONOMICAL_PROD01_RPA_CLIENT11_RECORD" {
  zone_id = "${var.zone_id}"
  name    = "rpa-client11.economical.insurcloud.ca"
  ttl     = "300"
  type    = "A"
  records = ["${data.terraform_remote_state.prod_remote_state.ICECONOMICAL_PROD01_RPA_CLIENT11_PRIVATEIP}"]
}

resource "aws_route53_record" "ICECONOMICAL_PROD01_RPA_CLIENT12_RECORD" {
  zone_id = "${var.zone_id}"
  name    = "rpa-client12.economical.insurcloud.ca"
  ttl     = "300"
  type    = "A"
  records = ["${data.terraform_remote_state.prod_remote_state.ICECONOMICAL_PROD01_RPA_CLIENT12_PRIVATEIP}"]
}

resource "aws_route53_record" "ICECONOMICAL_PROD01_RPA_CLIENT13_RECORD" {
  zone_id = "${var.zone_id}"
  name    = "rpa-client13.economical.insurcloud.ca"
  ttl     = "300"
  type    = "A"
  records = ["${data.terraform_remote_state.prod_remote_state.ICECONOMICAL_PROD01_RPA_CLIENT13_PRIVATEIP}"]
}

resource "aws_route53_record" "ICECONOMICAL_PROD01_RPA_CLIENT14_RECORD" {
  zone_id = "${var.zone_id}"
  name    = "rpa-client14.economical.insurcloud.ca"
  ttl     = "300"
  type    = "A"
  records = ["${data.terraform_remote_state.prod_remote_state.ICECONOMICAL_PROD01_RPA_CLIENT14_PRIVATEIP}"]
}

resource "aws_route53_record" "ICECONOMICAL_PROD01_RPA_CLIENT15_RECORD" {
  zone_id = "${var.zone_id}"
  name    = "rpa-client15.economical.insurcloud.ca"
  ttl     = "300"
  type    = "A"
  records = ["${data.terraform_remote_state.prod_remote_state.ICECONOMICAL_PROD01_RPA_CLIENT15_PRIVATEIP}"]
}

resource "aws_route53_record" "ICECONOMICAL_PROD01_RPA_CLIENT16_RECORD" {
  zone_id = "${var.zone_id}"
  name    = "rpa-client16.economical.insurcloud.ca"
  ttl     = "300"
  type    = "A"
  records = ["${data.terraform_remote_state.prod_remote_state.ICECONOMICAL_PROD01_RPA_CLIENT16_PRIVATEIP}"]
}

resource "aws_route53_record" "ICECONOMICAL_PROD01_RPA_CLIENT17_RECORD" {
  zone_id = "${var.zone_id}"
  name    = "rpa-client17.economical.insurcloud.ca"
  ttl     = "300"
  type    = "A"
  records = ["${data.terraform_remote_state.prod_remote_state.ICECONOMICAL_PROD01_RPA_CLIENT17_PRIVATEIP}"]
}

resource "aws_route53_record" "ICECONOMICAL_PROD01_RPA_CLIENT18_RECORD" {
  zone_id = "${var.zone_id}"
  name    = "rpa-client18.economical.insurcloud.ca"
  ttl     = "300"
  type    = "A"
  records = ["${data.terraform_remote_state.prod_remote_state.ICECONOMICAL_PROD01_RPA_CLIENT18_PRIVATEIP}"]
}

resource "aws_route53_record" "ICECONOMICAL_PROD01_RPA_CLIENT19_RECORD" {
  zone_id = "${var.zone_id}"
  name    = "rpa-client19.economical.insurcloud.ca"
  ttl     = "300"
  type    = "A"
  records = ["${data.terraform_remote_state.prod_remote_state.ICECONOMICAL_PROD01_RPA_CLIENT19_PRIVATEIP}"]
}

resource "aws_route53_record" "ICECONOMICAL_PROD01_RPA_CLIENT20_RECORD" {
  zone_id = "${var.zone_id}"
  name    = "rpa-client20.economical.insurcloud.ca"
  ttl     = "300"
  type    = "A"
  records = ["${data.terraform_remote_state.prod_remote_state.ICECONOMICAL_PROD01_RPA_CLIENT20_PRIVATEIP}"]
}

resource "aws_route53_record" "ICECONOMICAL_PROD01_RPA_CLIENT21_RECORD" {
  zone_id = "${var.zone_id}"
  name    = "rpa-client21.economical.insurcloud.ca"
  ttl     = "300"
  type    = "A"
  records = ["${data.terraform_remote_state.prod_remote_state.ICECONOMICAL_PROD01_RPA_CLIENT21_PRIVATEIP}"]
}

resource "aws_route53_record" "ICECONOMICAL_PROD01_RPA_CLIENT22_RECORD" {
  zone_id = "${var.zone_id}"
  name    = "rpa-client22.economical.insurcloud.ca"
  ttl     = "300"
  type    = "A"
  records = ["${data.terraform_remote_state.prod_remote_state.ICECONOMICAL_PROD01_RPA_CLIENT22_PRIVATEIP}"]
}

resource "aws_route53_record" "ICECONOMICAL_PROD01_RPA_CLIENT23_RECORD" {
  zone_id = "${var.zone_id}"
  name    = "rpa-client23.economical.insurcloud.ca"
  ttl     = "300"
  type    = "A"
  records = ["${data.terraform_remote_state.prod_remote_state.ICECONOMICAL_PROD01_RPA_CLIENT23_PRIVATEIP}"]
}

resource "aws_route53_record" "ICECONOMICAL_PROD01_RPA_CLIENT24_RECORD" {
  zone_id = "${var.zone_id}"
  name    = "rpa-client24.economical.insurcloud.ca"
  ttl     = "300"
  type    = "A"
  records = ["${data.terraform_remote_state.prod_remote_state.ICECONOMICAL_PROD01_RPA_CLIENT24_PRIVATEIP}"]
}

resource "aws_route53_record" "ICECONOMICAL_PROD01_RPA_CLIENT25_RECORD" {
  zone_id = "${var.zone_id}"
  name    = "rpa-client25.economical.insurcloud.ca"
  ttl     = "300"
  type    = "A"
  records = ["${data.terraform_remote_state.prod_remote_state.ICECONOMICAL_PROD01_RPA_CLIENT25_PRIVATEIP}"]
}

resource "aws_route53_record" "ICECONOMICAL_PROD01_RPA_CLIENT26_RECORD" {
  zone_id = "${var.zone_id}"
  name    = "rpa-client26.economical.insurcloud.ca"
  ttl     = "300"
  type    = "A"
  records = ["${data.terraform_remote_state.prod_remote_state.ICECONOMICAL_PROD01_RPA_CLIENT26_PRIVATEIP}"]
}

resource "aws_route53_record" "ICECONOMICAL_PROD01_RPA_CLIENT27_RECORD" {
  zone_id = "${var.zone_id}"
  name    = "rpa-client27.economical.insurcloud.ca"
  ttl     = "300"
  type    = "A"
  records = ["${data.terraform_remote_state.prod_remote_state.ICECONOMICAL_PROD01_RPA_CLIENT27_PRIVATEIP}"]
}

resource "aws_route53_record" "ICECONOMICAL_PROD01_RPA_CLIENT28_RECORD" {
  zone_id = "${var.zone_id}"
  name    = "rpa-client28.economical.insurcloud.ca"
  ttl     = "300"
  type    = "A"
  records = ["${data.terraform_remote_state.prod_remote_state.ICECONOMICAL_PROD01_RPA_CLIENT28_PRIVATEIP}"]
}

resource "aws_route53_record" "ICECONOMICAL_PROD01_RPA_CLIENT29_RECORD" {
  zone_id = "${var.zone_id}"
  name    = "rpa-client29.economical.insurcloud.ca"
  ttl     = "300"
  type    = "A"
  records = ["${data.terraform_remote_state.prod_remote_state.ICECONOMICAL_PROD01_RPA_CLIENT29_PRIVATEIP}"]
}

resource "aws_route53_record" "ICECONOMICAL_PROD01_RPA_CLIENT30_RECORD" {
  zone_id = "${var.zone_id}"
  name    = "rpa-client30.economical.insurcloud.ca"
  ttl     = "300"
  type    = "A"
  records = ["${data.terraform_remote_state.prod_remote_state.ICECONOMICAL_PROD01_RPA_CLIENT30_PRIVATEIP}"]
}

resource "aws_route53_record" "ICECONOMICAL_PROD01_RPA_CLIENT31_RECORD" {
  zone_id = "${var.zone_id}"
  name    = "rpa-client31.economical.insurcloud.ca"
  ttl     = "300"
  type    = "A"
  records = ["${data.terraform_remote_state.prod_remote_state.ICECONOMICAL_PROD01_RPA_CLIENT31_PRIVATEIP}"]
}

resource "aws_route53_record" "ICECONOMICAL_PROD01_RPA_CLIENT32_RECORD" {
  zone_id = "${var.zone_id}"
  name    = "rpa-client32.economical.insurcloud.ca"
  ttl     = "300"
  type    = "A"
  records = ["${data.terraform_remote_state.prod_remote_state.ICECONOMICAL_PROD01_RPA_CLIENT32_PRIVATEIP}"]
}
resource "aws_route53_record" "ICECONOMICAL_PROD01_RPA_CLIENT33_RECORD" {
  zone_id = "${var.zone_id}"
  name    = "rpa-client33.economical.insurcloud.ca"
  ttl     = "300"
  type    = "A"
  records = ["${data.terraform_remote_state.prod_remote_state.ICECONOMICAL_PROD01_RPA_CLIENT33_PRIVATEIP}"]
}

resource "aws_route53_record" "ICECONOMICAL_PROD01_RPA_CLIENT34_RECORD" {
  zone_id = "${var.zone_id}"
  name    = "rpa-client34.economical.insurcloud.ca"
  ttl     = "300"
  type    = "A"
  records = ["${data.terraform_remote_state.prod_remote_state.ICECONOMICAL_PROD01_RPA_CLIENT34_PRIVATEIP}"]
}

resource "aws_route53_record" "ICECONOMICAL_PROD01_RPA_CLIENT35_RECORD" {
  zone_id = "${var.zone_id}"
  name    = "rpa-client35.economical.insurcloud.ca"
  ttl     = "300"
  type    = "A"
  records = ["${data.terraform_remote_state.prod_remote_state.ICECONOMICAL_PROD01_RPA_CLIENT35_PRIVATEIP}"]
}

resource "aws_route53_record" "ICECONOMICAL_PROD01_RPA_CLIENT36_RECORD" {
  zone_id = "${var.zone_id}"
  name    = "rpa-client36.economical.insurcloud.ca"
  ttl     = "300"
  type    = "A"
  records = ["${data.terraform_remote_state.prod_remote_state.ICECONOMICAL_PROD01_RPA_CLIENT36_PRIVATEIP}"]
}

resource "aws_route53_record" "ICECONOMICAL_PROD01_RPA_CLIENT37_RECORD" {
  zone_id = "${var.zone_id}"
  name    = "rpa-client37.economical.insurcloud.ca"
  ttl     = "300"
  type    = "A"
  records = ["${data.terraform_remote_state.prod_remote_state.ICECONOMICAL_PROD01_RPA_CLIENT37_PRIVATEIP}"]
}

resource "aws_route53_record" "ICECONOMICAL_PROD01_RPA_CLIENT38_RECORD" {
  zone_id = "${var.zone_id}"
  name    = "rpa-client38.economical.insurcloud.ca"
  ttl     = "300"
  type    = "A"
  records = ["${data.terraform_remote_state.prod_remote_state.ICECONOMICAL_PROD01_RPA_CLIENT38_PRIVATEIP}"]
}

resource "aws_route53_record" "ICECONOMICAL_PROD01_RPA_CLIENT39_RECORD" {
  zone_id = "${var.zone_id}"
  name    = "rpa-client39.economical.insurcloud.ca"
  ttl     = "300"
  type    = "A"
  records = ["${data.terraform_remote_state.prod_remote_state.ICECONOMICAL_PROD01_RPA_CLIENT39_PRIVATEIP}"]
}

resource "aws_route53_record" "ICECONOMICAL_PROD01_RPA_CLIENT40_RECORD" {
  zone_id = "${var.zone_id}"
  name    = "rpa-client40.economical.insurcloud.ca"
  ttl     = "300"
  type    = "A"
  records = ["${data.terraform_remote_state.prod_remote_state.ICECONOMICAL_PROD01_RPA_CLIENT40_PRIVATEIP}"]
}

resource "aws_route53_record" "ICECONOMICAL_PROD01_RPA_CLIENT41_RECORD" {
  zone_id = "${var.zone_id}"
  name    = "rpa-client41.economical.insurcloud.ca"
  ttl     = "300"
  type    = "A"
  records = ["${data.terraform_remote_state.prod_remote_state.ICECONOMICAL_PROD01_RPA_CLIENT41_PRIVATEIP}"]
}

resource "aws_route53_record" "ICECONOMICAL_PROD01_RPA_CLIENT42_RECORD" {
  zone_id = "${var.zone_id}"
  name    = "rpa-client42.economical.insurcloud.ca"
  ttl     = "300"
  type    = "A"
  records = ["${data.terraform_remote_state.prod_remote_state.ICECONOMICAL_PROD01_RPA_CLIENT42_PRIVATEIP}"]
}

resource "aws_route53_record" "ICECONOMICAL_PROD01_RPA_CLIENT43_RECORD" {
  zone_id = "${var.zone_id}"
  name    = "rpa-client43.economical.insurcloud.ca"
  ttl     = "300"
  type    = "A"
  records = ["${data.terraform_remote_state.prod_remote_state.ICECONOMICAL_PROD01_RPA_CLIENT43_PRIVATEIP}"]
}

resource "aws_route53_record" "ICECONOMICAL_PROD01_RPA_CLIENT44_RECORD" {
  zone_id = "${var.zone_id}"
  name    = "rpa-client44.economical.insurcloud.ca"
  ttl     = "300"
  type    = "A"
  records = ["${data.terraform_remote_state.prod_remote_state.ICECONOMICAL_PROD01_RPA_CLIENT44_PRIVATEIP}"]
}

resource "aws_route53_record" "ICECONOMICAL_PROD01_RPA_CLIENT45_RECORD" {
  zone_id = "${var.zone_id}"
  name    = "rpa-client45.economical.insurcloud.ca"
  ttl     = "300"
  type    = "A"
  records = ["${data.terraform_remote_state.prod_remote_state.ICECONOMICAL_PROD01_RPA_CLIENT45_PRIVATEIP}"]
}

resource "aws_route53_record" "ICECONOMICAL_PROD01_RPA_CLIENT46_RECORD" {
  zone_id = "${var.zone_id}"
  name    = "rpa-client46.economical.insurcloud.ca"
  ttl     = "300"
  type    = "A"
  records = ["${data.terraform_remote_state.prod_remote_state.ICECONOMICAL_PROD01_RPA_CLIENT46_PRIVATEIP}"]
}

######### HAPROXY ##############
resource "aws_route53_record" "ICECONOMICAL_PROD01_INTERNAL_HAPROXY_RECORD" { /* TODO: REMOVE THIS LATER */
  zone_id = "${var.zone_id}"
  name    = "inthap.economical.insurcloud.ca"
  ttl = "300"
  type    = "A"
  records = ["${data.terraform_remote_state.prod_remote_state.ICECONOMICAL_PROD01_INTERNAL_HAPROXY_PRIVATEIP01}"]
}

resource "aws_route53_record" "ICECONOMICAL_PROD01_INTERNAL_HAPROXY_RECORD01" {
  zone_id = "${var.zone_id}"
  name    = "inthap01.economical.insurcloud.ca"
  ttl = "300"
  type    = "A"
  records = ["${data.terraform_remote_state.prod_remote_state.ICECONOMICAL_PROD01_INTERNAL_HAPROXY_PRIVATEIP01}"]
}

resource "aws_route53_record" "ICECONOMICAL_PROD01_INTERNAL_HAPROXY_RECORD02" {
  zone_id = "${var.zone_id}"
  name    = "inthap02.economical.insurcloud.ca"
  ttl = "300"
  type    = "A"
  records = ["${data.terraform_remote_state.prod_remote_state.ICECONOMICAL_PROD01_INTERNAL_HAPROXY_PRIVATEIP02}"]
}
