# #=======================================================================
# # PROD01
# #=======================================================================

####### GW VOLUMES #############
resource "aws_volume_attachment" "ICECONOMICAL_PROD01_GUIDEWIRE01_SUITE_DOCKER_EBS_ATTACHMENT" {
  device_name = "/dev/sdh"
  volume_id = "${aws_ebs_volume.ICECONOMICAL_PROD01_GUIDEWIRE01_SUITE_DOCKER_EBS.id}"
  instance_id = "${aws_instance.ICECONOMICAL_PROD01_GUIDEWIRE01_SUITE.id}"
}

resource "aws_ebs_volume" "ICECONOMICAL_PROD01_GUIDEWIRE01_SUITE_DOCKER_EBS" {
  availability_zone = "ca-central-1a"
  size = 100
  type = "gp3"
  encrypted = true
  kms_key_id = "arn:aws:kms:ca-central-1:852868701745:key/21cbfbdb-fa63-43fd-8f85-440e73a52e56"
  tags = {
    Contract = "MANAGED_SERVICES"
    Name = "ICECONOMICAL_PROD01_GUIDEWIRE01_SUITE_DOCKER_EBS"
    Docker = "True"
  }
}

resource "aws_volume_attachment" "ICECONOMICAL_PROD01_GUIDEWIRE01_SUITE_LOGS_EBS_ATTACHMENT" {
  device_name = "/dev/sdi"
  volume_id = "${aws_ebs_volume.ICECONOMICAL_PROD01_GUIDEWIRE01_SUITE_LOGS_EBS.id}"
  instance_id = "${aws_instance.ICECONOMICAL_PROD01_GUIDEWIRE01_SUITE.id}"
}

resource "aws_ebs_volume" "ICECONOMICAL_PROD01_GUIDEWIRE01_SUITE_LOGS_EBS" {
  availability_zone = "ca-central-1a"
  size = 50
  type = "gp3"
  encrypted = true
  kms_key_id = "arn:aws:kms:ca-central-1:852868701745:key/21cbfbdb-fa63-43fd-8f85-440e73a52e56"
  tags = {
    Contract = "MANAGED_SERVICES"
    Name = "ICECONOMICAL_PROD01_GUIDEWIRE01_SUITE_LOGS_EBS"
    Docker = "True"
  }
}

resource "aws_volume_attachment" "ICECONOMICAL_PROD01_GUIDEWIRE02_SUITE_DOCKER_EBS_ATTACHMENT" {
  device_name = "/dev/sdh"
  volume_id = "${aws_ebs_volume.ICECONOMICAL_PROD01_GUIDEWIRE02_SUITE_DOCKER_EBS.id}"
  instance_id = "${aws_instance.ICECONOMICAL_PROD01_GUIDEWIRE02_SUITE.id}"
}

resource "aws_ebs_volume" "ICECONOMICAL_PROD01_GUIDEWIRE02_SUITE_DOCKER_EBS" {
  availability_zone = "ca-central-1b"
  size = 100
  type = "gp3"
  encrypted = true
  kms_key_id = "arn:aws:kms:ca-central-1:852868701745:key/21cbfbdb-fa63-43fd-8f85-440e73a52e56"
  tags = {
    Contract = "MANAGED_SERVICES"
    Name = "ICECONOMICAL_PROD01_GUIDEWIRE02_SUITE_DOCKER_EBS"
    Docker = "True"
  }
}

resource "aws_volume_attachment" "ICECONOMICAL_PROD01_GUIDEWIRE02_SUITE_LOGS_EBS_ATTACHMENT" {
  device_name = "/dev/sdi"
  volume_id = "${aws_ebs_volume.ICECONOMICAL_PROD01_GUIDEWIRE02_SUITE_LOGS_EBS.id}"
  instance_id = "${aws_instance.ICECONOMICAL_PROD01_GUIDEWIRE02_SUITE.id}"
}

resource "aws_ebs_volume" "ICECONOMICAL_PROD01_GUIDEWIRE02_SUITE_LOGS_EBS" {
  availability_zone = "ca-central-1b"
  size = 50
  type = "gp3"
  encrypted = true
  kms_key_id = "arn:aws:kms:ca-central-1:852868701745:key/21cbfbdb-fa63-43fd-8f85-440e73a52e56"
  tags = {
    Contract = "MANAGED_SERVICES"
    Name = "ICECONOMICAL_PROD01_GUIDEWIRE02_SUITE_LOGS_EBS"
    Docker = "True"
  }
}

resource "aws_volume_attachment" "ICECONOMICAL_PROD01_GUIDEWIRE03_SUITE_DOCKER_EBS_ATTACHMENT" {
  device_name = "/dev/sdh"
  volume_id = "${aws_ebs_volume.ICECONOMICAL_PROD01_GUIDEWIRE03_SUITE_DOCKER_EBS.id}"
  instance_id = "${aws_instance.ICECONOMICAL_PROD01_GUIDEWIRE03_SUITE.id}"
}

resource "aws_ebs_volume" "ICECONOMICAL_PROD01_GUIDEWIRE03_SUITE_DOCKER_EBS" {
  availability_zone = "ca-central-1a"
  size = 100
  type = "gp3"
  encrypted = true
  kms_key_id = "arn:aws:kms:ca-central-1:852868701745:key/21cbfbdb-fa63-43fd-8f85-440e73a52e56"
  tags = {
    Contract = "MANAGED_SERVICES"
    Name = "ICECONOMICAL_PROD01_GUIDEWIRE03_SUITE_DOCKER_EBS"
    Docker = "True"
  }
}

resource "aws_volume_attachment" "ICECONOMICAL_PROD01_GUIDEWIRE03_SUITE_LOGS_EBS_ATTACHMENT" {
  device_name = "/dev/sdi"
  volume_id = "${aws_ebs_volume.ICECONOMICAL_PROD01_GUIDEWIRE03_SUITE_LOGS_EBS.id}"
  instance_id = "${aws_instance.ICECONOMICAL_PROD01_GUIDEWIRE03_SUITE.id}"
}

resource "aws_ebs_volume" "ICECONOMICAL_PROD01_GUIDEWIRE03_SUITE_LOGS_EBS" {
  availability_zone = "ca-central-1a"
  size = 50
  type = "gp3"
  encrypted = true
  kms_key_id = "arn:aws:kms:ca-central-1:852868701745:key/21cbfbdb-fa63-43fd-8f85-440e73a52e56"
  tags = {
    Contract = "MANAGED_SERVICES"
    Name = "ICECONOMICAL_PROD01_GUIDEWIRE03_SUITE_LOGS_EBS"
    Docker = "True"
  }
}

resource "aws_volume_attachment" "ICECONOMICAL_PROD01_GUIDEWIRE04_SUITE_DOCKER_EBS_ATTACHMENT" {
  device_name = "/dev/sdh"
  volume_id = "${aws_ebs_volume.ICECONOMICAL_PROD01_GUIDEWIRE04_SUITE_DOCKER_EBS.id}"
  instance_id = "${aws_instance.ICECONOMICAL_PROD01_GUIDEWIRE04_SUITE.id}"
}

resource "aws_ebs_volume" "ICECONOMICAL_PROD01_GUIDEWIRE04_SUITE_DOCKER_EBS" {
  availability_zone = "ca-central-1b"
  size = 100
  type = "gp3"
  encrypted = true
  kms_key_id = "arn:aws:kms:ca-central-1:852868701745:key/21cbfbdb-fa63-43fd-8f85-440e73a52e56"
  tags = {
    Contract = "MANAGED_SERVICES"
    Name = "ICECONOMICAL_PROD01_GUIDEWIRE04_SUITE_DOCKER_EBS"
    Docker = "True"
  }
}

resource "aws_volume_attachment" "ICECONOMICAL_PROD01_GUIDEWIRE04_SUITE_LOGS_EBS_ATTACHMENT" {
  device_name = "/dev/sdi"
  volume_id = "${aws_ebs_volume.ICECONOMICAL_PROD01_GUIDEWIRE04_SUITE_LOGS_EBS.id}"
  instance_id = "${aws_instance.ICECONOMICAL_PROD01_GUIDEWIRE04_SUITE.id}"
}

resource "aws_ebs_volume" "ICECONOMICAL_PROD01_GUIDEWIRE04_SUITE_LOGS_EBS" {
  availability_zone = "ca-central-1b"
  size = 50
  type = "gp3"
  encrypted = true
  kms_key_id = "arn:aws:kms:ca-central-1:852868701745:key/21cbfbdb-fa63-43fd-8f85-440e73a52e56"
  tags = {
    Contract = "MANAGED_SERVICES"
    Name = "ICECONOMICAL_PROD01_GUIDEWIRE04_SUITE_LOGS_EBS"
    Docker = "True"
  }
}

############ Microservices Volumes ##################
resource "aws_volume_attachment" "ICECONOMICAL_PROD01_MICROSERVICES01_DOCKER_EBS_ATTACHMENT" {
  device_name = "/dev/sdh"
  volume_id = "${aws_ebs_volume.ICECONOMICAL_PROD01_MICROSERVICES01_DOCKER_EBS.id}"
  instance_id = "${aws_instance.ICECONOMICAL_PROD01_MICROSERVICES_APPLICATION01.id}"
}

resource "aws_ebs_volume" "ICECONOMICAL_PROD01_MICROSERVICES01_DOCKER_EBS" {
  availability_zone = "ca-central-1a"
  size = 50
  encrypted = true
  kms_key_id = "arn:aws:kms:ca-central-1:852868701745:key/21cbfbdb-fa63-43fd-8f85-440e73a52e56"
  tags = {
    Contract = "MANAGED_SERVICES"
    Name = "ICECONOMICAL_PROD01_MICROSERVICES01_DOCKER_EBS"
    Docker = "True"
  }
}

resource "aws_volume_attachment" "ICECONOMICAL_PROD01_MICROSERVICES02_DOCKER_EBS_ATTACHMENT" {
  device_name = "/dev/sdh"
  volume_id = "${aws_ebs_volume.ICECONOMICAL_PROD01_MICROSERVICES02_DOCKER_EBS.id}"
  instance_id = "${aws_instance.ICECONOMICAL_PROD01_MICROSERVICES_APPLICATION02.id}"
}

resource "aws_ebs_volume" "ICECONOMICAL_PROD01_MICROSERVICES02_DOCKER_EBS" {
  availability_zone = "ca-central-1b"
  size = 50
  encrypted = true
  kms_key_id = "arn:aws:kms:ca-central-1:852868701745:key/21cbfbdb-fa63-43fd-8f85-440e73a52e56"
  tags = {
    Contract = "MANAGED_SERVICES"
    Name = "ICECONOMICAL_PROD01_MICROSERVICES02_DOCKER_EBS"
    Docker = "True"
  }
}

############ Internal HAProxy Volumes ##################
resource "aws_volume_attachment" "ICECONOMICAL_PROD01_INTERNAL_HAPROXY_DOCKER_EBS_ATTACHMENT01" {
  device_name = "/dev/sdh"
  volume_id = "${aws_ebs_volume.ICECONOMICAL_PROD01_INTERNAL_HAPROXY_DOCKER_EBS01.id}"
  instance_id = "${aws_instance.ICECONOMICAL_PROD01_INTERNAL_HAPROXY01.id}"
}

resource "aws_ebs_volume" "ICECONOMICAL_PROD01_INTERNAL_HAPROXY_DOCKER_EBS01" {
  availability_zone = "ca-central-1a"
  size = 20
  encrypted = true
  kms_key_id = "arn:aws:kms:ca-central-1:852868701745:key/21cbfbdb-fa63-43fd-8f85-440e73a52e56"
  tags = {
    Contract = "MANAGED_SERVICES"
    Name = "ICECONOMICAL_PROD01_INTERNAL_HAPROXY_DOCKER_EBS01"
    Docker = "True"
  }
}

resource "aws_volume_attachment" "ICECONOMICAL_PROD01_INTERNAL_HAPROXY_DOCKER_EBS_ATTACHMENT02" {
  device_name = "/dev/sdh"
  volume_id = "${aws_ebs_volume.ICECONOMICAL_PROD01_INTERNAL_HAPROXY_DOCKER_EBS02.id}"
  instance_id = "${aws_instance.ICECONOMICAL_PROD01_INTERNAL_HAPROXY02.id}"
}

resource "aws_ebs_volume" "ICECONOMICAL_PROD01_INTERNAL_HAPROXY_DOCKER_EBS02" {
  availability_zone = "ca-central-1b"
  size = 20
  encrypted = true
  kms_key_id = "arn:aws:kms:ca-central-1:852868701745:key/21cbfbdb-fa63-43fd-8f85-440e73a52e56"
  tags = {
    Contract = "MANAGED_SERVICES"
    Name = "ICECONOMICAL_PROD01_INTERNAL_HAPROXY_DOCKER_EBS02"
    Docker = "True"
  }
}

############ DB Volumes ##################
resource "aws_volume_attachment" "ICECONOMICAL_PROD_GUIDEWIRE_DB01_WIN_EC2_EBS_ATTACHMENT" {
  device_name = "/dev/sdh"
  volume_id = "${aws_ebs_volume.ICECONOMICAL_PROD_GUIDEWIRE_DB01_WIN_EC2_EBS.id}"
  instance_id = "${aws_instance.ICECONOMICAL_PROD01_GUIDEWIRE_MSSQL_DATABASE01.id}"
}

resource "aws_ebs_volume" "ICECONOMICAL_PROD_GUIDEWIRE_DB01_WIN_EC2_EBS" {
  availability_zone = "ca-central-1a"
  size = 200
  encrypted = true
  kms_key_id = "arn:aws:kms:ca-central-1:852868701745:key/21cbfbdb-fa63-43fd-8f85-440e73a52e56"
  tags = {
    Contract = "MANAGED_SERVICES"
    Name = "ICECONOMICAL_PROD_GUIDEWIRE_DB01_WIN_EC2_EBS"
  }
}

resource "aws_volume_attachment" "ICECONOMICAL_PROD_GUIDEWIRE_DB02_WIN_EC2_EBS_ATTACHMENT" {
  device_name = "/dev/sdh"
  volume_id = "${aws_ebs_volume.ICECONOMICAL_PROD_GUIDEWIRE_DB02_WIN_EC2_EBS.id}"
  instance_id = "${aws_instance.ICECONOMICAL_PROD01_GUIDEWIRE_MSSQL_DATABASE02.id}"
}

resource "aws_ebs_volume" "ICECONOMICAL_PROD_GUIDEWIRE_DB02_WIN_EC2_EBS" {
  availability_zone = "ca-central-1b"
  size = 200
  encrypted = true
  kms_key_id = "arn:aws:kms:ca-central-1:852868701745:key/21cbfbdb-fa63-43fd-8f85-440e73a52e56"
  tags = {
    Contract = "MANAGED_SERVICES"
    Name = "ICECONOMICAL_PROD_GUIDEWIRE_DB02_WIN_EC2_EBS"
  }
}


# #=======================================================================
# # PROD shared
# #=======================================================================
############ Jenkins Client ##################
resource "aws_volume_attachment" "ICECONOMICAL_PROD_JENKINS_CLIENT_DOCKER_EBS_ATTACHMENT" {
  device_name = "/dev/sdh"
  volume_id = "${aws_ebs_volume.ICECONOMICAL_PROD_JENKINS_CLIENT_DOCKER_EBS.id}"
  instance_id = "${aws_instance.ICECONOMICAL_PROD_JENKINS_CLIENT.id}"
}

resource "aws_ebs_volume" "ICECONOMICAL_PROD_JENKINS_CLIENT_DOCKER_EBS" {
  availability_zone = "${aws_instance.ICECONOMICAL_PROD_JENKINS_CLIENT.availability_zone}"
  size = 50
  type = "gp3"
  encrypted = true
  kms_key_id = "arn:aws:kms:ca-central-1:852868701745:key/21cbfbdb-fa63-43fd-8f85-440e73a52e56"

  tags = {
    Contract = "MANAGED_SERVICES"
    Name = "ICECONOMICAL_PROD_JENKINS_CLIENT_DOCKER_EBS"
    Docker = "True"
  }
}
