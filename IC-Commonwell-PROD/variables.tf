variable "client_name" {
  description = "AWS client name for resources (Ex: MASTERCLOUD)"
  default = ""
}

variable "enviornment_name" {
  description = "AWS enviornment name for resources (Ex: NON-PROD)"
  default = ""
}

variable "enviornment_type" {
  description = "AWS enviornment type for resources (Ex: P=PROD/HUB, D=NON-PROD)"
  default = ""
}

variable "client_number" {
  description = "AWS client number for tagging (Ex: A001)"
  default = "A003"
}

variable "region" {
  description = "AWS region for hosting our your network"
  default = "ca-central-1"
}

variable "volume_key" {
  description = "KMS volume key prod account"
  default = ""
}

variable "timezone" {
  description = "(Optional) Time zone of the DB instance. timezone is currently only supported by Microsoft SQL Server. The timezone can only be set on creation. See MSSQL User Guide for more information."
  default     = "Eastern"
}

variable "ami_id_amazonlinux201709base" {
  description = "Amazon Linux Encypted Root Volume AMI"
  default = ""
}

variable "ami_id_redhatlinux75" {
  description = "Redhat Linux Encypted Root Volume AMI"
  default = ""
}

variable "ami_id_amazonlinux201803base" {
  description = "Amazon Linux 2018 03 Encypted Root Volume AMI"
  default = ""
}

variable "ami_id_amazonlinux2base" {
  description = "Amazon Linux 2 Base Encypted Root Volume AMI"
  default = ""
}

variable "ami_id_amazonlinux2base20200506" {
  description = "Amazon Linux 2 Base Encypted Root Volume AMI 2020-05-06"
  default = ""
}

variable "ami_id_windows2008base" {
  description = "Windows 2008 Base Encypted Root Volume AMI"
  default = ""
}

variable "ami_id_amazonlinux201712base" {
  description = "Amazon Linux 2017 12 Base Encypted Root Volume AMI"
  default = ""
}

variable "ami_id_windows2012R2base" {
  description = "Windows 2012 R2 Base Encypted Root Volume AMI"
  default = ""
}

variable "ami_id_windows2012SQLEnterprisebase" {
  description = "Windows 2012 R2 Base Encypted Root Volume AMI with SQL 2014 Enterprise from Marketplace"
  default = ""
}
variable "ami_id_windows2016base" {
  description = "Windows 2016 Base Encypted Root Volume AMI"
  default = ""
}

variable "ami_id_windows2019base" {
  description = "Windows 2019 Base Encypted Root Volume AMI"
  default = ""
}

variable "ami_id_windows2019cont" {
  description = "Windows 2019 Cont Encypted Root Volume AMI"
  default = ""
}

variable "ami_id_ubuntu1604baseimage" {
  description = "Ubuntu 16 04 Base Encypted Root Volume AMI"
  default = ""
}

variable "ami_id_ubuntu1804baseimage" {
  description = "Ubuntu 18 04 Base Encypted Root Volume AMI"
  default = ""
}

variable "vpc_id" {
  description = "VPC id for PROD account"
  default = "vpc-056e018cb30996b4e"
}

variable "vpc_cidr" {
  description = "VPC cidr prod account"
  default = ""
}

variable "commonwell-prod-database-1a-subnet" {
  description = "Database subnet one"
  default = ""
}

variable "commonwell-prod-database-1b-subnet" {
  description = "Database subnet two"
  default = ""
}

variable "commonwell-prod-guidewire-containers-1a-subnet" {
  description = "Guidewire containers Subnet One"
  default = ""
}

variable "commonwell-prod-guidewire-containers-1b-subnet" {
  description = "Guidewire Containers Subnet Two"
  default = ""
}

variable "commonwell-prod-guidewire-database-1a-subnet" {
  description = "Guidewire Database Subnet One"
  default = ""
}

variable "commonwell-prod-guidewire-database-1b-subnet" {
  description = "Guidewire Database Subnet two"
  default = ""
}

variable "commonwell-prod-private-1a-subnet" {
  description = "Private Subnet one"
  default = ""
}

variable "commonwell-prod-private-1b-subnet" {
  description = "Private Subnet Two"
  default = ""

}

variable "commonwell-prod-spring-microservices-1a-subnet" {
  description = "Spring Microservices Subnet One"
  default = ""
}

variable "commonwell-prod-spring-microservices-1b-subnet" {
  description = "Spring Microservices Subnet Two"
  default = ""
}

variable "default_cidr" {
  description = "default cidr used"
  default = ["10.0.0.0/8"]
}

variable "hub_cidr" {
  default = "10.10.4.0/22"
}

variable "deloitte_cidr" {
    default = "10.10.48.0/20"
}

variable "commonwell_aws_cidr" {
    default = "172.31.0.0/16"
}

variable "commonwell_lindsay_cidr" {
    default = "10.19.0.0/16"
}

variable "commonwell_whitby_cidr" {
    default = "10.20.0.0/16"
}

variable "mssql-rds-password" {
    default = "Canada@123"
}
variable "mysql-rds-password" {
    default = "Canada@123"
}

variable "postgres-rds-password" {
    default = "Canada@123"
}

data "aws_caller_identity" "current" {}

variable "commonwell_aviatrix_vpn_1a" {
    default = "10.10.5.104/32"
}
variable "commonwell_aviatrix_vpn_1b" {
    default = "10.10.7.78/32"
}
variable "commonwell_omnia_cidr" {
  default = "10.42.48.0/20"
}
variable "cmig-prod-account"{
  default = ""
}