# #=======================================================================
# # DEV01
# #=======================================================================
##### ALBs #####

#Dont need this as we only have one instance. 
# resource "aws_lb" "ICGORE_DEV01_INTERNAL_ALB" {
#     name = "ICGORE-DEV01-INTERNAL-ALB"
#     internal           = true
#     load_balancer_type = "application"
#     enable_cross_zone_load_balancing = false
#     subnets            = [
#         "${var.gore-dev-private-1a-subnet}",
#         "${var.gore-dev-private-1b-subnet}"
#     ]

#     tags = {
#         Name = "ICGORE-DEV01-INTERNAL-ALB"
#     }
# }

# resource "aws_lb_target_group" "ICGORE_DEV01_INTERNAL_ALB_HAPROXY_TARGET_GROUP" {
#     name     = "ICGORE-DEV01-HAPROXY-INT-ALB"
#     port     = 80
#     protocol = "TCP"
#     vpc_id   = "${var.vpc_id}"

#     health_check {
#         protocol = "TCP"
#         port = 8444
#     }

#     tags = {
#         Name = "ICGORE-DEV01-HAPROXY-INTERNAL-ALB-TARGET-GROUP"
#     }
# }

# resource "aws_lb_listener" "ICGORE_DEV01_INTERNAL_ALB_HAPROXY_LISTENER" {
#     load_balancer_arn = "${aws_lb.ICGORE_DEV01_INTERNAL_ALB.arn}"
#     port              = "80"
#     protocol          = "TCP"

#     default_action {
#         type             = "forward"
#         target_group_arn = "${aws_lb_target_group.ICGORE_DEV01_INTERNAL_ALB_HAPROXY_TARGET_GROUP.arn}"
#     }
# }


# resource "aws_lb_target_group_attachment" "ICGORE_DEV01_INTERNAL_ALB_TARGET_GROUP_ATTACHMENT_HAPROXY" {
#     target_group_arn = "${aws_lb_target_group.ICGORE_DEV01_INTERNAL_ALB_HAPROXY_TARGET_GROUP.arn}"
#     target_id        = "${aws_instance.ICGORE_DEV01_INTERNAL_HAPROXY.id}"
#     port             = 80
# }

# output "ICGORE_DEV01_INTERNAL_ALB_DNS_NAME" {
#     value = "${aws_lb.ICGORE_DEV01_INTERNAL_ALB.dns_name}"
# }

# output "ICGORE_DEV01_INTERNAL_ALB_ZONE_ID" {
#     value = "${aws_lb.ICGORE_DEV01_INTERNAL_ALB.zone_id}"
# }

##### NLBs ####################
########## Internal ###########
########## DEV01 ###########
resource "aws_lb" "ICGORE_DEV01_INTERNAL_NLB" {
    name = "ICGORE-DEV01-INTERNAL-NLB"
    internal           = true
    load_balancer_type = "network"
    enable_cross_zone_load_balancing = false
    subnets            = [
        "${var.gore-dev-private-1a-subnet}",
        "${var.gore-dev-private-1b-subnet}"
    ]

    tags = {
        Name = "ICGORE-DEV01-INTERNAL-NLB"
    }
}

resource "aws_lb_target_group" "ICGORE_DEV01_INTERNAL_NLB_HAPROXY_TARGET_GROUP" {
    name     = "ICGORE-DEV01-HAPROXY-INT-NLB"
    port     = 80
    protocol = "TCP"
    vpc_id   = "${var.vpc_id}"

    health_check {
        protocol = "TCP"
        port = 8444
    }

    tags = {
        Name = "ICGORE-DEV01-HAPROXY-INTERNAL-NLB-TARGET-GROUP"
    }
}

resource "aws_lb_listener" "ICGORE_DEV01_INTERNAL_NLB_HAPROXY_LISTENER" {
    load_balancer_arn = "${aws_lb.ICGORE_DEV01_INTERNAL_NLB.arn}"
    port              = "80"
    protocol          = "TCP"

    default_action {
        type             = "forward"
        target_group_arn = "${aws_lb_target_group.ICGORE_DEV01_INTERNAL_NLB_HAPROXY_TARGET_GROUP.arn}"
    }
}


resource "aws_lb_target_group_attachment" "ICGORE_DEV01_INTERNAL_NLB_TARGET_GROUP_ATTACHMENT_HAPROXY" {
    target_group_arn = "${aws_lb_target_group.ICGORE_DEV01_INTERNAL_NLB_HAPROXY_TARGET_GROUP.arn}"
    target_id        = "${aws_instance.ICGORE_DEV01_INTERNAL_HAPROXY.id}"
    port             = 80
}

output "ICGORE_DEV01_INTERNAL_NLB_DNS_NAME" {
    value = "${aws_lb.ICGORE_DEV01_INTERNAL_NLB.dns_name}"
}

output "ICGORE_DEV01_INTERNAL_NLB_ZONE_ID" {
    value = "${aws_lb.ICGORE_DEV01_INTERNAL_NLB.zone_id}"
}

########## DEV02 ###########
resource "aws_lb" "ICGORE_DEV02_INTERNAL_NLB" {
    name = "ICGORE-DEV02-INTERNAL-NLB"
    internal           = true
    load_balancer_type = "network"
    enable_cross_zone_load_balancing = false
    subnets            = [
        "${var.gore-dev-private-1a-subnet}",
        "${var.gore-dev-private-1b-subnet}"
    ]

    tags = {
        Name = "ICGORE-DEV02-INTERNAL-NLB"
    }
}

resource "aws_lb_target_group" "ICGORE_DEV02_INTERNAL_NLB_HAPROXY_TARGET_GROUP" {
    name     = "ICGORE-DEV02-HAPROXY-INT-NLB"
    port     = 80
    protocol = "TCP"
    vpc_id   = "${var.vpc_id}"

    health_check {
        protocol = "TCP"
        port = 8444
    }

    tags = {
        Name = "ICGORE-DEV02-HAPROXY-INTERNAL-NLB-TARGET-GROUP"
    }
}

resource "aws_lb_listener" "ICGORE_DEV02_INTERNAL_NLB_HAPROXY_LISTENER" {
    load_balancer_arn = "${aws_lb.ICGORE_DEV02_INTERNAL_NLB.arn}"
    port              = "80"
    protocol          = "TCP"

    default_action {
        type             = "forward"
        target_group_arn = "${aws_lb_target_group.ICGORE_DEV02_INTERNAL_NLB_HAPROXY_TARGET_GROUP.arn}"
    }
}


resource "aws_lb_target_group_attachment" "ICGORE_DEV02_INTERNAL_NLB_TARGET_GROUP_ATTACHMENT_HAPROXY" {
    target_group_arn = "${aws_lb_target_group.ICGORE_DEV02_INTERNAL_NLB_HAPROXY_TARGET_GROUP.arn}"
    target_id        = "${aws_instance.ICGORE_DEV02_INTERNAL_HAPROXY.id}"
    port             = 80
}

output "ICGORE_DEV02_INTERNAL_NLB_DNS_NAME" {
    value = "${aws_lb.ICGORE_DEV02_INTERNAL_NLB.dns_name}"
}

output "ICGORE_DEV02_INTERNAL_NLB_ZONE_ID" {
    value = "${aws_lb.ICGORE_DEV02_INTERNAL_NLB.zone_id}"
}

########## DEV03 ###########
resource "aws_lb" "ICGORE_DEV03_INTERNAL_NLB" {
    name = "ICGORE-DEV03-INTERNAL-NLB"
    internal           = true
    load_balancer_type = "network"
    enable_cross_zone_load_balancing = false
    subnets            = [
        "${var.gore-dev-private-1a-subnet}",
        "${var.gore-dev-private-1b-subnet}"
    ]

    tags = {
        Name = "ICGORE-DEV03-INTERNAL-NLB"
    }
}

resource "aws_lb_target_group" "ICGORE_DEV03_INTERNAL_NLB_HAPROXY_TARGET_GROUP" {
    name     = "ICGORE-DEV03-HAPROXY-INT-NLB"
    port     = 80
    protocol = "TCP"
    vpc_id   = "${var.vpc_id}"

    health_check {
        protocol = "TCP"
        port = 8444
    }

    tags = {
        Name = "ICGORE-DEV03-HAPROXY-INTERNAL-NLB-TARGET-GROUP"
    }
}

resource "aws_lb_listener" "ICGORE_DEV03_INTERNAL_NLB_HAPROXY_LISTENER" {
    load_balancer_arn = "${aws_lb.ICGORE_DEV03_INTERNAL_NLB.arn}"
    port              = "80"
    protocol          = "TCP"

    default_action {
        type             = "forward"
        target_group_arn = "${aws_lb_target_group.ICGORE_DEV03_INTERNAL_NLB_HAPROXY_TARGET_GROUP.arn}"
    }
}


resource "aws_lb_target_group_attachment" "ICGORE_DEV03_INTERNAL_NLB_TARGET_GROUP_ATTACHMENT_HAPROXY" {
    target_group_arn = "${aws_lb_target_group.ICGORE_DEV03_INTERNAL_NLB_HAPROXY_TARGET_GROUP.arn}"
    target_id        = "${aws_instance.ICGORE_DEV03_INTERNAL_HAPROXY.id}"
    port             = 80
}

output "ICGORE_DEV03_INTERNAL_NLB_DNS_NAME" {
    value = "${aws_lb.ICGORE_DEV03_INTERNAL_NLB.dns_name}"
}

output "ICGORE_DEV03_INTERNAL_NLB_ZONE_ID" {
    value = "${aws_lb.ICGORE_DEV03_INTERNAL_NLB.zone_id}"
}



########## TBT01 ###########
resource "aws_lb" "ICGORE_TBT01_INTERNAL_NLB" {
    name = "ICGORE-TBT01-INTERNAL-NLB"
    internal           = true
    load_balancer_type = "network"
    enable_cross_zone_load_balancing = false
    subnets            = [
        "${var.gore-dev-private-1a-subnet}",
        "${var.gore-dev-private-1b-subnet}"
    ]

    tags = {
        Name = "ICGORE-TBT01-INTERNAL-NLB"
    }
}

resource "aws_lb_target_group" "ICGORE_TBT01_INTERNAL_NLB_HAPROXY_TARGET_GROUP" {
    name     = "ICGORE-TBT01-HAPROXY-INT-NLB"
    port     = 80
    protocol = "TCP"
    vpc_id   = "${var.vpc_id}"

    health_check {
        protocol = "TCP"
        port = 8444
    }

    tags = {
        Name = "ICGORE-TBT01-HAPROXY-INTERNAL-NLB-TARGET-GROUP"
    }
}

resource "aws_lb_listener" "ICGORE_TBT01_INTERNAL_NLB_HAPROXY_LISTENER" {
    load_balancer_arn = "${aws_lb.ICGORE_TBT01_INTERNAL_NLB.arn}"
    port              = "80"
    protocol          = "TCP"

    default_action {
        type             = "forward"
        target_group_arn = "${aws_lb_target_group.ICGORE_TBT01_INTERNAL_NLB_HAPROXY_TARGET_GROUP.arn}"
    }
}

resource "aws_lb_target_group_attachment" "ICGORE_TBT01_INTERNAL_NLB_TARGET_GROUP_ATTACHMENT_HAPROXY" {
    target_group_arn = "${aws_lb_target_group.ICGORE_TBT01_INTERNAL_NLB_HAPROXY_TARGET_GROUP.arn}"
    target_id        = "${aws_instance.ICGORE_TBT01_INTERNAL_HAPROXY.id}"
    port             = 80
}

output "ICGORE_TBT01_INTERNAL_NLB_DNS_NAME" {
    value = "${aws_lb.ICGORE_TBT01_INTERNAL_NLB.dns_name}"
}

output "ICGORE_TBT01_INTERNAL_NLB_ZONE_ID" {
    value = "${aws_lb.ICGORE_TBT01_INTERNAL_NLB.zone_id}"
}


########## QA01 ###########
resource "aws_lb" "ICGORE_QA01_INTERNAL_NLB" {
    name = "ICGORE-QA01-INTERNAL-NLB"
    internal           = true
    load_balancer_type = "network"
    enable_cross_zone_load_balancing = false
    subnets            = [
        "${var.gore-dev-private-1a-subnet}",
        "${var.gore-dev-private-1b-subnet}"
    ]

    tags = {
        Name = "ICGORE-QA01-INTERNAL-NLB"
    }
}

resource "aws_lb_target_group" "ICGORE_QA01_INTERNAL_NLB_HAPROXY_TARGET_GROUP" {
    name     = "ICGORE-QA01-HAPROXY-INT-NLB"
    port     = 80
    protocol = "TCP"
    vpc_id   = "${var.vpc_id}"

    health_check {
        protocol = "TCP"
        port = 8444
    }

    tags = {
        Name = "ICGORE-QA01-HAPROXY-INTERNAL-NLB-TARGET-GROUP"
    }
}

resource "aws_lb_listener" "ICGORE_QA01_INTERNAL_NLB_HAPROXY_LISTENER" {
    load_balancer_arn = "${aws_lb.ICGORE_QA01_INTERNAL_NLB.arn}"
    port              = "80"
    protocol          = "TCP"

    default_action {
        type             = "forward"
        target_group_arn = "${aws_lb_target_group.ICGORE_QA01_INTERNAL_NLB_HAPROXY_TARGET_GROUP.arn}"
    }
}


resource "aws_lb_target_group_attachment" "ICGORE_QA01_INTERNAL_NLB_TARGET_GROUP_ATTACHMENT_HAPROXY" {
    target_group_arn = "${aws_lb_target_group.ICGORE_QA01_INTERNAL_NLB_HAPROXY_TARGET_GROUP.arn}"
    target_id        = "${aws_instance.ICGORE_QA01_INTERNAL_HAPROXY.id}"
    port             = 80
}

output "ICGORE_QA01_INTERNAL_NLB_DNS_NAME" {
    value = "${aws_lb.ICGORE_QA01_INTERNAL_NLB.dns_name}"
}

output "ICGORE_QA01_INTERNAL_NLB_ZONE_ID" {
    value = "${aws_lb.ICGORE_QA01_INTERNAL_NLB.zone_id}"
}



########## QA02 ###########
resource "aws_lb" "ICGORE_QA02_INTERNAL_NLB" {
    name = "ICGORE-QA02-INTERNAL-NLB"
    internal           = true
    load_balancer_type = "network"
    enable_cross_zone_load_balancing = false
    subnets            = [
        "${var.gore-dev-private-1a-subnet}",
        "${var.gore-dev-private-1b-subnet}"
    ]

    tags = {
        Name = "ICGORE-QA02-INTERNAL-NLB"
    }
}

resource "aws_lb_target_group" "ICGORE_QA02_INTERNAL_NLB_HAPROXY_TARGET_GROUP" {
    name     = "ICGORE-QA02-HAPROXY-INT-NLB"
    port     = 80
    protocol = "TCP"
    vpc_id   = "${var.vpc_id}"

    health_check {
        protocol = "TCP"
        port = 8444
    }

    tags = {
        Name = "ICGORE-QA02-HAPROXY-INTERNAL-NLB-TARGET-GROUP"
    }
}

resource "aws_lb_listener" "ICGORE_QA02_INTERNAL_NLB_HAPROXY_LISTENER" {
    load_balancer_arn = "${aws_lb.ICGORE_QA02_INTERNAL_NLB.arn}"
    port              = "80"
    protocol          = "TCP"

    default_action {
        type             = "forward"
        target_group_arn = "${aws_lb_target_group.ICGORE_QA02_INTERNAL_NLB_HAPROXY_TARGET_GROUP.arn}"
    }
}


resource "aws_lb_target_group_attachment" "ICGORE_QA02_INTERNAL_NLB_TARGET_GROUP_ATTACHMENT_HAPROXY" {
    target_group_arn = "${aws_lb_target_group.ICGORE_QA02_INTERNAL_NLB_HAPROXY_TARGET_GROUP.arn}"
    target_id        = "${aws_instance.ICGORE_QA02_INTERNAL_HAPROXY.id}"
    port             = 80
}

output "ICGORE_QA02_INTERNAL_NLB_DNS_NAME" {
    value = "${aws_lb.ICGORE_QA02_INTERNAL_NLB.dns_name}"
}

output "ICGORE_QA02_INTERNAL_NLB_ZONE_ID" {
    value = "${aws_lb.ICGORE_QA02_INTERNAL_NLB.zone_id}"
}

########## TBT02 ###########
resource "aws_lb" "ICGORE_TBT02_INTERNAL_NLB" {
    name = "ICGORE-TBT02-INTERNAL-NLB"
    internal           = true
    load_balancer_type = "network"
    enable_cross_zone_load_balancing = false
    subnets            = [
        "${var.gore-dev-private-1a-subnet}",
        "${var.gore-dev-private-1b-subnet}"
    ]

    tags = {
        Name = "ICGORE-TBT02-INTERNAL-NLB"
    }
}

resource "aws_lb_target_group" "ICGORE_TBT02_INTERNAL_NLB_HAPROXY_TARGET_GROUP" {
    name     = "ICGORE-TBT02-HAPROXY-INT-NLB"
    port     = 80
    protocol = "TCP"
    vpc_id   = "${var.vpc_id}"

    health_check {
        protocol = "TCP"
        port = 8444
    }

    tags = {
        Name = "ICGORE-TBT02-HAPROXY-INTERNAL-NLB-TARGET-GROUP"
    }
}

resource "aws_lb_listener" "ICGORE_TBT02_INTERNAL_NLB_HAPROXY_LISTENER" {
    load_balancer_arn = "${aws_lb.ICGORE_TBT02_INTERNAL_NLB.arn}"
    port              = "80"
    protocol          = "TCP"

    default_action {
        type             = "forward"
        target_group_arn = "${aws_lb_target_group.ICGORE_TBT02_INTERNAL_NLB_HAPROXY_TARGET_GROUP.arn}"
    }
}


resource "aws_lb_target_group_attachment" "ICGORE_TBT02_INTERNAL_NLB_TARGET_GROUP_ATTACHMENT_HAPROXY" {
    target_group_arn = "${aws_lb_target_group.ICGORE_TBT02_INTERNAL_NLB_HAPROXY_TARGET_GROUP.arn}"
    target_id        = "${aws_instance.ICGORE_TBT02_INTERNAL_HAPROXY.id}"
    port             = 80
}

output "ICGORE_TBT02_INTERNAL_NLB_DNS_NAME" {
    value = "${aws_lb.ICGORE_TBT02_INTERNAL_NLB.dns_name}"
}

output "ICGORE_TBT02_INTERNAL_NLB_ZONE_ID" {
    value = "${aws_lb.ICGORE_TBT02_INTERNAL_NLB.zone_id}"
}

########## BAT ###########
resource "aws_lb" "ICGORE_BAT_INTERNAL_NLB" {
    name = "ICGORE-BAT-INTERNAL-NLB"
    internal           = true
    load_balancer_type = "network"
    enable_cross_zone_load_balancing = false
    subnets            = [
        "${var.gore-dev-private-1a-subnet}",
        "${var.gore-dev-private-1b-subnet}"
    ]

    tags = {
        Name = "ICGORE-BAT-INTERNAL-NLB"
    }
}

resource "aws_lb_target_group" "ICGORE_BAT_INTERNAL_NLB_HAPROXY_TARGET_GROUP" {
    name     = "ICGORE-BAT-HAPROXY-INT-NLB"
    port     = 80
    protocol = "TCP"
    vpc_id   = "${var.vpc_id}"

    health_check {
        protocol = "TCP"
        port = 8444
    }

    tags = {
        Name = "ICGORE-BAT-HAPROXY-INTERNAL-NLB-TARGET-GROUP"
    }
}

resource "aws_lb_listener" "ICGORE_BAT_INTERNAL_NLB_HAPROXY_LISTENER" {
    load_balancer_arn = "${aws_lb.ICGORE_BAT_INTERNAL_NLB.arn}"
    port              = "80"
    protocol          = "TCP"

    default_action {
        type             = "forward"
        target_group_arn = "${aws_lb_target_group.ICGORE_BAT_INTERNAL_NLB_HAPROXY_TARGET_GROUP.arn}"
    }
}


resource "aws_lb_target_group_attachment" "ICGORE_BAT_INTERNAL_NLB_TARGET_GROUP_ATTACHMENT_HAPROXY" {
    target_group_arn = "${aws_lb_target_group.ICGORE_BAT_INTERNAL_NLB_HAPROXY_TARGET_GROUP.arn}"
    target_id        = "${aws_instance.ICGORE_BAT_INTERNAL_HAPROXY.id}"
    port             = 80
}

output "ICGORE_BAT_INTERNAL_NLB_DNS_NAME" {
    value = "${aws_lb.ICGORE_BAT_INTERNAL_NLB.dns_name}"
}

output "ICGORE_BAT_INTERNAL_NLB_ZONE_ID" {
    value = "${aws_lb.ICGORE_BAT_INTERNAL_NLB.zone_id}"
}

########## PERF ###########
resource "aws_lb" "ICGORE_PERF_INTERNAL_NLB" {
    name = "ICGORE-PERF-INTERNAL-NLB"
    internal           = true
    load_balancer_type = "network"
    enable_cross_zone_load_balancing = false
    subnets            = [
        "${var.gore-dev-private-1a-subnet}",
        "${var.gore-dev-private-1b-subnet}"
    ]

    tags = {
        Name = "ICGORE-PERF-INTERNAL-NLB"
    }
}

resource "aws_lb_target_group" "ICGORE_PERF_INTERNAL_NLB_HAPROXY_TARGET_GROUP" {
    name     = "ICGORE-PERF-HAPROXY-INT-NLB"
    port     = 80
    protocol = "TCP"
    vpc_id   = "${var.vpc_id}"

    health_check {
        protocol = "TCP"
        port = 8444
    }

    tags = {
        Name = "ICGORE-PERF-HAPROXY-INTERNAL-NLB-TARGET-GROUP"
    }
}

resource "aws_lb_listener" "ICGORE_PERF_INTERNAL_NLB_HAPROXY_LISTENER" {
    load_balancer_arn = "${aws_lb.ICGORE_PERF_INTERNAL_NLB.arn}"
    port              = "80"
    protocol          = "TCP"

    default_action {
        type             = "forward"
        target_group_arn = "${aws_lb_target_group.ICGORE_PERF_INTERNAL_NLB_HAPROXY_TARGET_GROUP.arn}"
    }
}


resource "aws_lb_target_group_attachment" "ICGORE_PERF_INTERNAL_NLB_TARGET_GROUP_ATTACHMENT_HAPROXY_1" {
    target_group_arn = "${aws_lb_target_group.ICGORE_PERF_INTERNAL_NLB_HAPROXY_TARGET_GROUP.arn}"
    target_id        = "${aws_instance.ICGORE_PERF_INTERNAL_HAPROXY_1.id}"
    port             = 80
}

resource "aws_lb_target_group_attachment" "ICGORE_PERF_INTERNAL_NLB_TARGET_GROUP_ATTACHMENT_HAPROXY_2" {
    target_group_arn = "${aws_lb_target_group.ICGORE_PERF_INTERNAL_NLB_HAPROXY_TARGET_GROUP.arn}"
    target_id        = "${aws_instance.ICGORE_PERF_INTERNAL_HAPROXY_2.id}"
    port             = 80
}

output "ICGORE_PERF_INTERNAL_NLB_DNS_NAME" {
    value = "${aws_lb.ICGORE_PERF_INTERNAL_NLB.dns_name}"
}

output "ICGORE_PERF_INTERNAL_NLB_ZONE_ID" {
    value = "${aws_lb.ICGORE_PERF_INTERNAL_NLB.zone_id}"
}