provider "aws" {
  region     = "ca-central-1"
  version    = "~> 2.63.0" #Temporary until there is a fix for https://github.com/terraform-providers/terraform-provider-aws/issues/13549
  assume_role = {
    role_arn = "arn:aws:iam::020266858005:role/admin_ops_auto"
  }
}

terraform {
 backend "s3" {
    bucket     = "aws-cms-terraformstate"
    key        = "ICHUB/hub.tfstate"
    region     = "ca-central-1"
    role_arn       = "arn:aws:iam::393766723611:role/admin_ops_auto"
	  #encrypt    = true
	  #kms_key_id = "arn:aws:kms:ca-central-1::key/"
  }
}

data "terraform_remote_state" "peel_dev_remote_state" {
  backend = "s3"
  config = {
    role_arn   = "arn:aws:iam::393766723611:role/admin_ops_auto"
    bucket     = "aws-cms-terraformstate"
    key        = "ICPeel/dev.tfstate"
    region     = "ca-central-1"
  }
}

data "terraform_remote_state" "peel_prod_remote_state" {
  backend = "s3"
  config = {
    role_arn   = "arn:aws:iam::393766723611:role/admin_ops_auto"
    bucket     = "aws-cms-terraformstate"
    key        = "ICPeel/prod.tfstate"
    region     = "ca-central-1"
  }
}