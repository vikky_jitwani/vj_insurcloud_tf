# ---------------------------------------------------------------------------------------------------------------------
# ENVIRONMENT VARIABLES
# Define these secrets as environment variables
# ---------------------------------------------------------------------------------------------------------------------

# AWS_ACCESS_KEY_ID
# AWS_SECRET_ACCESS_KEY
# AWS_DEFAULT_REGION

# ---------------------------------------------------------------------------------------------------------------------
# REQUIRED PARAMETERS
# These parameters must be set per environment
# ---------------------------------------------------------------------------------------------------------------------

variable "vpc_name_filter" {
  description = "Values for VPC filtering by tag Name"
  type = "list"
}

variable "subnet_name_filter" {
  description = "Values for SubNet filtering by tag Name"
  type = "list"
}

variable "ssh_key_name" {
  description = "The name of an EC2 Key Pair that can be used to SSH to the EC2 Instances in this cluster. Set to an empty string to not associate a Key Pair."
}

variable "allowed_inbound_cidr_blocks" {
  description = "The name of an EC2 Key Pair that can be used to SSH to the EC2 Instances in this cluster. Set to an empty string to not associate a Key Pair."
  type = "list"
}

variable "allowed_ssh_cidr_blocks" {
  description = "The name of an EC2 Key Pair that can be used to SSH to the EC2 Instances in this cluster. Set to an empty string to not associate a Key Pair."
  type = "list"
}

variable "environment" {
  description = "Target environment name"
}

variable "cluster_name_prefix" {
  description = "Prefix to name the Consul cluster"
}

variable "consul_cluster_tag_key" {
  description = "The tag the Consul EC2 Instances will look for to automatically discover each other and form a cluster."
}

variable "consul_cluster_tag_value" {
  description = "What to name the Consul server cluster and all of its associated resources"
}

variable "cross_account_assumed_role" {
  description = "ARN for the Assumed Role using Cross Account Access to Vault"
  type        = "list"
}

variable "patching_policy_arn" {
  description = "Patching policy to be attached to Vault IAM Role"
}


# ---------------------------------------------------------------------------------------------------------------------
# OPTIONAL PARAMETERS
# ---------------------------------------------------------------------------------------------------------------------

variable "permissions_boundary" {
  description = "IAM Role permissions boundary"
  default     = ""
}

variable "create_dns_entry" {
  description = "If set to true, this module will create a Route 53 DNS A record for the ELB in the var.hosted_zone_id hosted zone with the domain name in var.domain_name."
  default = "1"
}

variable "hosted_zone_domain_name" {
  description = "The domain name of the Route 53 Hosted Zone in which to add a DNS entry for Vault (e.g. example.com). Only used if var.create_dns_entry is true."
  default = "insurcloud.ca"
}

variable "vault_instance_type" {
  description = "The type of EC2 Instance to run in the Vault ASG"
  default     = "t3.small"
}

variable "vault_cluster_size" {
  description = "The number of Vault server nodes to deploy. We strongly recommend using 3 or 5."
  default     = 3
}

variable "consul_ca_path" {
  description = "Path to the directory of CA files used to verify outgoing connections."
  default     = "/opt/consul/tls/ca"
}

variable "consul_cert_file_path" {
  description = "Path to the certificate file used to verify incoming connections."
  default     = "/opt/consul/tls/consul.crt.pem"
}

variable "consul_key_file_path" {
  description = "Path to the certificate key used to verify incoming connections."
  default     = "/opt/consul/tls/consul.key.pem"
}

variable "api_protocol" {
  description = "The domain name to use in the DNS A record for the consul ELB (e.g. consul.example.com). Make sure that a) this is a domain within the var.hosted_zone_domain_name hosted zone and b) this is the same domain name you used in the TLS certificates for consul. Only used if var.create_dns_entry is true."
  default = "HTTPS"
}

variable "api_port" {
  description = "The domain name to use in the DNS A record for the consul ELB (e.g. consul.example.com). Make sure that a) this is a domain within the var.hosted_zone_domain_name hosted zone and b) this is the same domain name you used in the TLS certificates for consul. Only used if var.create_dns_entry is true."
  default = 8200
}

variable "lb_port" {
  description = "The domain name to use in the DNS A record for the consul ELB (e.g. consul.example.com). Make sure that a) this is a domain within the var.hosted_zone_domain_name hosted zone and b) this is the same domain name you used in the TLS certificates for consul. Only used if var.create_dns_entry is true."
  default = 443
}
